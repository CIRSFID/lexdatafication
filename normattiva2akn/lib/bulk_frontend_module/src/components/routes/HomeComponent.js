import React, { Component } from 'react';
import axios, { post, get } from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import MyPaginationActionsTable from '../ui/Table/MyPaginationActionsTable';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const formControlStyle = {  
  minWidth: 180,
  maxWidth: 360,
};
const cardButtonStyle = {
  marginLeft: "calc(50% - 65px)"
};

const types = ["DECRETO", "COMUNICATO", "DECRETO LEGGE", "LEGGE"];

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      startDate:null,
      endDate:null,
      types: [],      
      bulkSets: []
    };

  }
  componentDidMount(){
    let fakeData = [
      { 
        types: ["LEGGE", "DECRETO"], 
        range: {
          from:'12-12-2012',
          to:'12-12-2015'
        },
        ts: new Date().getTime().toString()
      },
      {
        types: ["DECRETO", "COMUNICATO", "DECRETO LEGGE", "LEGGE"],
        range: {
          from: '12-12-1990',
          to: '12-12-2000'
        },
        ts: new Date().getTime().toString()
      },
      {
        types: ["LEGGE", "DECRETO"],
        range: {
          from: '12-12-2013',
          to: '12-08-2019'
        },
        ts: new Date().getTime().toString()
      }
    ]
    this.setState({bulkSets: fakeData});
  }
  
  handleStartDateChange(e){
    this.setState({startDate: e.target.value});
  }
  
  handleEndDateChange(e) {    
    this.setState({ endDate: e.target.value });
  }

  handleTypeChange(e){
    this.setState({ types: e.target.value });
  }

  render() {
    return (    
        <div>
          <Card>
            <CardContent>
            <FormControl style={formControlStyle}>
              <TextField
                id="startDate"
                label="Data Inizio"
                type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                value={this.state.startDate}
                onChange={this.handleStartDateChange.bind(this)}
                />
            </FormControl>
            <FormControl style={formControlStyle}>
              <TextField
                id="endDate"
                label="Data Fine"
                type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                value={this.state.endDate}
                onChange={this.handleEndDateChange.bind(this)}
                />
            </FormControl>  
            </CardContent>
            <CardContent>
            <FormControl style={formControlStyle}>
              <InputLabel htmlFor="select-multiple-checkbox">Tipologia</InputLabel>
              <Select
                multiple
                value={this.state.types}
                onChange={this.handleTypeChange.bind(this)}
                input={<Input id="select-multiple-checkbox" />}
                renderValue={selected => selected.join(', ')}
                MenuProps={MenuProps}
              >
                {types.map(type => (
                  <MenuItem key={type} value={type}>
                    <Checkbox checked={this.state.types.indexOf(type) > -1} />
                    <ListItemText primary={type} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </CardContent>
            <CardActions>
              <Button style={cardButtonStyle} size="small">
                Reset
              </Button>
              <Button size="small">
                Avvia Bulk
              </Button>
            </CardActions>
        </Card>  
        <Card>  
          <CardContent>
            <MyPaginationActionsTable
              data={this.state.bulkSets}          
            />
          </CardContent>
        </Card>  
      </div>
    );
  }
}

export default App;
