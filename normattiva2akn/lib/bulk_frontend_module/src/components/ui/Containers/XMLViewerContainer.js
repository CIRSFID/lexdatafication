import React, { Component } from 'react';
import { Row, Col, Modal, Button } from 'react-bootstrap';
import XMLViewer from '../XMLViewer/XMLViewer.js'

class XMLViewerContainer extends Component {  
  constructor(props) {
    super(props);
    this.state = {
      xmlContent: '',
      showModal:false
    };

  }
  componentDidMount() {
    this.setState({ xmlContent: this.props.xmlContent });
    this.setState({ showModal: this.props.showViewer });    
  }
  
  handleShowViewer(e){
    e.preventDefault(e);
    this.setState({ showModal: true});
    this.props.handleCloseXMLViewer(true);
  }
  handleCloseViewer(e) {
    e.preventDefault();
    this.setState({ showModal: false });    
    this.props.handleCloseXMLViewer(false);
  }
  
  handleXmlContent(xmlContent){
    this.setState({ xmlContent: xmlContent });    
    this.props.handleCloseXMLViewer();
  } 

  render() {
    
    return ( 
      <div>
        <Modal show={this.props.showViewer} onHide={this.handleCloseViewer.bind(this)} bsSize="large">
          <Modal.Header closeButton>
            <Modal.Title>AKN Conversion</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
                <Col xs={12} sm={12} md={12}>
                  <XMLViewer 
                    xmlContent={this.props.xmlContent}
                    />
                </Col>
            </Row>   
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.handleCloseXMLViewer} className="btn btn-primary">Close</Button>
          </Modal.Footer>
        </Modal>      
      </div>     
    );
  }
}

export default XMLViewerContainer;
