import React, { Component } from 'react';

class FooterContainer extends Component {   

  render() {
      
    let displayAttribute = '';
    if (this.props.show) {
      displayAttribute = { 'display': 'block' }
    } else {
      displayAttribute = { 'display': 'none' }
    }
    return ( 
      <div style={displayAttribute}>
            
                <p>
                    <span>CC-by 4.0 CIRSFID- University of Bologna</span>
                    <span>Autori: CIRSFID, Università di Bologna</span>
                </p>
                <p>
                    <span>Sviluppatori: Monica Palmirani, Luca Cervone, Matteo Nardi</span>
                </p>
                <p>
                    <span>Contatti: <a href="mailto:monica.palmirani@unibo.it">monica.palmirani@unibo.it</a></span>
                </p>

            
      </div>     
    );
  }
}

export default FooterContainer;
