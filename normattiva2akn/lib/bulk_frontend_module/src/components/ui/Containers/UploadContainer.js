import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import TabsContainer from '../Tabs/TabsContainer.js';

class UploadContainer extends Component {

  render() {
    let displayAttribute = '';
    if (this.props.show) {
      displayAttribute = { 'display': 'block' }
    } else {
      displayAttribute = { 'display': 'none' }
    }
    return (
      <div style={displayAttribute}>
        <Row>
          <Col xs={12} sm={12} md={12}>
            <TabsContainer
              onFormFileSubmit={this.props.onFormFileSubmit}
              onFormXMLTextSubmit={this.props.onFormXMLTextSubmit}
							onJSONTextSubmit={this.props.onJSONTextSubmit}
							onDateRangeSubmit={this.props.onDateRangeSubmit}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default UploadContainer;
