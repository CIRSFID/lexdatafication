import React, { Component } from 'react';

class HomeTextContainer extends Component {   

  render() {
      
    let displayAttribute = '';
    if (this.props.show) {
      displayAttribute = { 'display': 'block' }
    } else {
      displayAttribute = { 'display': 'none' }
    }
    return ( 
      <div style={displayAttribute}>
        <div className="container text-left">
            <h1>Normattiva2Akn - Bulk</h1>
            <section>
              <p>
                Selezionando un'intervallo di date è possibile convertire in bulk una lista di atti.
              </p>
            </section>
        </div>  
      </div>     
    );
  }
}

export default HomeTextContainer;
