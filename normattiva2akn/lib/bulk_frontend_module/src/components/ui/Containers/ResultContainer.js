import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import TableContainer from '../Table/TableContainer.js';
import XMLViewerContainer from './XMLViewerContainer.js';

class ResultContainer extends Component {  
 
  handleBackClick(e){
      e.preventDefault();
      this.props.backToUploadPahse();
  }
  handleDownloadZip(e) {
    e.preventDefault();
    this.props.downloadResultZip();
  }
  render() {
      
    let displayAttribute = '';
    if (this.props.show) {
      displayAttribute = { 'display': 'block' }
    } else {
      displayAttribute = { 'display': 'none' }
    }
    return ( 
      <div style={displayAttribute}>
        <div className="text-left">
          <h1>Converted files</h1>
        </div>
        <br />
        <Row>
            <Col xs={12} sm={12} md={12}>
                <TableContainer 
                    resultData={this.props.resultData}
                    fileDownload={this.props.fileDownload}
                    fileDirectDownload={this.props.fileDirectDownload}
                    fileView={this.props.fileView}  
                    fileViewContent={this.props.fileViewContent}  
                    viewModalContent={this.props.viewModalContent}  
                />
            </Col>
        </Row>
        <button type="submit" onClick={this.handleBackClick.bind(this)} className="btn btn-primary">Back to Upload</button>
        <button type="submit" onClick={this.handleDownloadZip.bind(this)} className="btn btn-primary">Download Zip</button>
      </div>     
    );
  }
}

export default ResultContainer;
