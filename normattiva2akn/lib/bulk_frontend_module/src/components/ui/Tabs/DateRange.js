import React, { Component } from 'react';
import { DateRangePicker } from 'react-dates';

class DateRange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			value: '',
			startDate: null,
			endDate: null,
			focusedInput: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

	handleSubmit(e) {
		e.preventDefault(e);
		let startDt = this.state.startDate.toISOString().split('T')[0];
		let endDt = this.state.endDate.toISOString().split('T')[0];
		this.props.onDateRangeSubmit(startDt, endDt);
  }

  render() {
    return (
			<div>
        <h2>{this.props.title}</h2>
				<DateRangePicker
					startDate={this.state.startDate} // momentPropTypes.momentObj or null,
					startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
					endDate={this.state.endDate} // momentPropTypes.momentObj or null,
					endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
					onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
					focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
					onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
					block={true}
					isOutsideRange={() => false}					
					displayFormat={'DD-MM-YYYY'}
				/>
				<br />
				<button className="btn btn-primary pull-center" onClick={this.handleSubmit.bind(this)}>Submit</button>
			</div>
    );
  }
}

export default DateRange;
