import React from 'react'
// Import React FilePond
import { FilePond, File } from 'react-filepond';

// Import FilePond styles
import 'filepond/dist/filepond.min.css';
class FileUploader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            file: null,
            files: []
        }
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.onChange = this.onChange.bind(this)        
    }
    onFormSubmit(e) {
        e.preventDefault();
        this.props.onFormFileSubmit(this.state.file);
    }
    onChange(e) {
        this.setState({ file: e.target.files[0] })
    }

    handleUpload(fieldName, file, metadata, load, error, progress, abort){
        this.props.onFormFileSubmit(file);
    }

    render() {
        return (
            <div>
                <h2>{this.props.title}</h2>
                <FilePond 
                    allowMultiple={false}
                    instantUpload={false}
                    server={{
                        process: this.handleUpload.bind(this)
                    }}>                    
                    {this.state.files.map(file => (
                        <File key={file} source={file} />
                    ))}
                </FilePond>
            </div>
        )
    }
}

export default FileUploader;
