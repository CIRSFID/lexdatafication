import React, { Component } from 'react';
import { Tab, Col, Row, Nav, NavItem } from 'react-bootstrap';
import TextArea from './TextArea.js';
import FileUploader from './FileUploader.js';
import DateRange from './DateRange.js';

class TabsContainer extends Component {

  render() {
    return (
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <Row className="clearfix">
              <Col sm={4}>
            <br /><br />
                <Nav bsStyle="pills" stacked>
                  <NavItem eventKey="first">BULK RANGE</NavItem>
							<NavItem eventKey="second">DOCS</NavItem>
                </Nav>
              </Col>
              <Col sm={8}>
                <Tab.Content animation>
									<Tab.Pane eventKey="first">
										<DateRange
												title="Bulk Range"
												onDateRangeSubmit={this.props.onDateRangeSubmit}
											/>
										</Tab.Pane>
									<Tab.Pane eventKey="second">
											<h2>Docs</h2>
											<div className="text-center">
												<p>Viene esposta l'api in get con intervallo di date "api-prefix/:startDate/:endDate"</p>
											</div>
									</Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
    );
  }
}

export default TabsContainer;
