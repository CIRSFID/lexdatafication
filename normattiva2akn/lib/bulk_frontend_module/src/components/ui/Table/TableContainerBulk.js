import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import ActionContainer from './ActionContainer.js';

class TableContainerBulk extends Component {  

  render() {
    return ( 
        <Table responsive striped bordered condensed hover>
            <thead className="table-header">
                <tr>
                    <th>Num</th>
                    <th>Types</th>
                    <th>From Date</th>                    
                    <th>To Date</th>                    
                    <th>Executed</th>                    
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {this.props.resultData.map((row, i) =>{                                  
                    return (<tr key={i}>
                        <td>{i + 1}</td>
                        <td>{row.types.join(', ')}</td>
                        <td>{row.range.from}</td>
                        <td>{row.range.to}</td>
                        <td>{row.ts}</td>
                        <td>
                            <ActionContainer
                                rowData={row}                                
                            />
                        </td>
                    </tr>)
                }
                    
                    
                )}              
            </tbody>
        </Table>
    );
  }
}

export default TableContainerBulk;
