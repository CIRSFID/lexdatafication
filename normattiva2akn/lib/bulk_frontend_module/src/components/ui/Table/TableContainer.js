import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import ActionContainer from './ActionContainer.js';

class TableContainer extends Component {  

  render() {
    return ( 
        <Table responsive striped bordered condensed hover>
            <thead className="table-header">
                <tr>
                    <th>Num</th>
                    <th>File</th>
                    <th>Validazione</th>                    
                    <th>Azioni</th>
                </tr>
            </thead>
            <tbody>
                {this.props.resultData.map((row, i) =>{
                    let validationStr = "NOT VALID";
                    if (row.validationObj.success) {
                        validationStr ="VALID";
                    }
                    return (<tr key={i}>
                        <td>{i + 1}</td>
                        <td>{row.fileName}</td>
                        <td>{validationStr}</td>
                        <td>
                            <ActionContainer
                                rowData={row}
                                fileDownload={this.props.fileDownload}
                                fileDirectDownload={this.props.fileDirectDownload}
                                fileView={this.props.fileView}
                                fileViewContent={this.props.fileViewContent}
                                viewModalContent={this.props.viewModalContent}
                            />
                        </td>
                    </tr>)
                }
                    
                    
                )}              
            </tbody>
        </Table>
    );
  }
}

export default TableContainer;
