import React, { Component } from 'react';
import { ButtonToolbar, ButtonGroup, Button, Glyphicon } from 'react-bootstrap';

class ActionContainer extends Component {  
  
    handleFileDownloadClick(e){
        e.preventDefault();
        console.log(this.props.rowData);        
        this.props.fileDirectDownload(this.props.rowData);
    }

    handleWiewFileClick(e) {
        e.preventDefault();
        console.log(this.props.rowData);
        this.props.fileViewContent(this.props.rowData.xmlContent);
    }

    handleViewValidationsClick(e) {
        e.preventDefault();
        let customString = 'No validation errors';
        if(!this.props.rowData.validationObj.success){
            customString = "-----------------------------------------\n";
            this.props.rowData.validationObj.errors.forEach(element => {                
                element.error.split(',').forEach(element => {
                    customString += element + '\n';
                });
                customString += "-----------------------------------------\n";
            });
        }
        this.props.fileViewContent(customString);
    }

    render() {
        return ( 
            <div>
                <ButtonToolbar>
                    <ButtonGroup>
                        <Button bsSize="xsmall" onClick={this.handleViewValidationsClick.bind(this)}>
                            <Glyphicon glyph="wrench" />
                        </Button>
                        <Button bsSize="xsmall" onClick={this.handleFileDownloadClick.bind(this)}>
                            <Glyphicon glyph="download-alt" />
                        </Button>
                        <Button bsSize="xsmall" onClick={this.handleWiewFileClick.bind(this)}>
                            <Glyphicon glyph="search" />
                        </Button>  
                    </ButtonGroup>
                </ButtonToolbar>
            </div>
        );
    }
}

export default ActionContainer;
