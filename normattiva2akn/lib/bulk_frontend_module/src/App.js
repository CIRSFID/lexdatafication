import React, { Component } from 'react';
import xml2js from 'xml2js';
import logo from './logo.svg';
import logoUnibo from './logoUnibo.png';
import cirfidTestata from './cirsfid_testata.gif';
import axios, { post, get } from 'axios';
import { Image, Tab, Col, Row, Nav, NavItem } from 'react-bootstrap';
import FileDownload from 'react-file-download';
import { ToastContainer, toast } from 'react-toastify';
import JSZip from 'jszip';
import UploadContainer from './components/ui/Containers/UploadContainer.js';
import ResultContainer from './components/ui/Containers/ResultContainer.js';
import XMLViewerContainer from './components/ui/Containers/XMLViewerContainer.js';
import HomeTextContainer from './components/ui/Containers/HomeTextContainer.js';
import FooterContainer from './components/ui/Containers/FooterContainer.js';
import SpinnerLoader from './components/ui/SpinnerLoader/SpinnerLoader.js';
import './App.css';

//to do remove var Loader = require('react-loaders').Loader;

//ES6 Axios polyfill IExploer compatibility
require('es6-promise').polyfill()

const uploadApi = '/nir2akn/api/upload-and-convert';
const downloadApi = '/nir2akn/api/download';
const jsonConvertApi = 'convert';
const jsonUploadApi = 'upload-and-convert';
const bulkApi = 'bulk-act-address';

let container;

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      resultData: [],
      isFetchingData:false,
      showResult:false,
      showViewer:false,
      xmlContent:'',
      modalContent:''
    };

  }

  fileDownload(requestObj) {
    let url = downloadApi;
    this.setState({ isFetchingData: true })
    post(url,
      {
        fileName: requestObj.fileName,
        destDir: requestObj.destDir
      }).then((response) => {
          this.setState({ isFetchingData: false })
          FileDownload(response.data, requestObj.fileName);
      }).catch((err)=>{
        this.setState({ isFetchingData: false })
        console.error(err)
      })
  }

  fileDirectDownload(fileObj){
    FileDownload(fileObj.xmlContent, fileObj.fileName);
  }

  fileView(requestObj) {
    let url = downloadApi;
    this.setState({ isFetchingData: true })
    post(url,
      {
        fileName: requestObj.fileName,
        destDir: requestObj.destDir
      }).then((response) => {
        this.setState({ isFetchingData: false })
        this.setState({ xmlContent: response.data })
        this.setState({ showViewer: true });
      }).catch((err) => {
        this.setState({ isFetchingData: false })
        console.error(err)
      })
  }

  fileViewContent(content) {
    this.setState({ xmlContent: content })
    this.setState({ showViewer: true });
  }
  
  viewModalContent(content) {
    this.setState({ modalContent: content })
    this.setState({ showViewer: true });
  }

  downloadResultZip(){
    let zipFile = new JSZip();
    this.state.resultData.forEach((el) => {
      zipFile.file(el.fileName, el.xmlContent);
    });
    zipFile.generateAsync({ type: "blob" })
      .then(function (content) {
        // see FileSaver.js
        FileDownload(content, "Result.zip");
      });
  }

  handleOpenXMLViewer() {
    this.setState({ showViewer: true });
  }

  handleCloseXMLViewer() {
    this.setState({ showViewer: false });
  }

  fileUpload(file) {
    let url = jsonUploadApi;
    let formData = new FormData();
    formData.append('file', file)
    let config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return post(url, formData, config)
  }

  xmlTextUpload(xmlText) {
    let url = uploadApi;
    let formData = new FormData();
    formData.append('textarea', xmlText)
    let config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return post(url, formData, config)
  }

  jsonContentUpload(jsonContent) {
    let url = jsonConvertApi;
    let content = {content: JSON.parse(jsonContent)};
    let config = {
      headers: {
        'content-type': 'application/json'
      }
    }
    return post(url, content, config)
	}

	getDateRangeDocs(startDate, endDate){
		let url = bulkApi;
		let api = url + '/' + startDate + '/' + endDate;
		return get(api)
	}

  onFormXMLTextSubmit(xmlText) {
    this.setState({ isFetchingData: true})
    this.xmlTextUpload(xmlText).then((response) => {
      this.setState({ isFetchingData: false })
      this.setState({ showResult: true })
      this.setState({ resultData: response.data.content })
    }).catch((err)=>{
      this.setState({ isFetchingData: false })
      toast.error(err.response.data)
      console.error("->", err.response.data)
    })
  }

  onJSONTextSubmit(jsonText) {
    this.setState({ isFetchingData: true })
    try{
      this.jsonContentUpload(jsonText).then((response) => {
        this.setState({ isFetchingData: false })
        this.setState({ xmlContent: response.data })
        this.setState({ showViewer: true });
      }).catch((err) => {
        this.setState({ isFetchingData: false })
        toast.error(err.response.data)
        console.error("->", err.response.data)
      })
    }catch(err){
      toast.error(err.message)
      this.setState({ isFetchingData: false })
    }

  }

  onFormFileSubmit(file) {
    this.setState({ isFetchingData: true })
    this.fileUpload(file).then((response) => {
      this.setState({ isFetchingData: false })
      this.setState({ showResult: true })
      this.setState({ resultData: response.data.content })
    }).catch((err) => {
      this.setState({ isFetchingData: false })
      toast.error(err.response.data)
      console.error("->", err.response.data)
    })
	}

	onDateRangeSubmit(startDate, endDate) {
		this.setState({ isFetchingData: true })
		this.getDateRangeDocs(startDate, endDate).then((response) => {
			this.setState({ isFetchingData: false })
			this.setState({ showResult: true })
			this.setState({ resultData: response.data.content })
		}).catch((err) => {
			this.setState({ isFetchingData: false })
			//toast.error(err.response.data)
			console.error("->", err.response.data)
		})
	}

  backToUploadPahse(){
      this.setState({ showResult: false })
  }

  render() {

    return (
      <div className="App">
        <ToastContainer />
        <header className="App-header">

          <Image src={cirfidTestata} responsive />

        </header>
        <div className="App-sub-header" />

        <div className="main-app-content">
          <SpinnerLoader isLoading={this.state.isFetchingData}/>
            <div>
              <HomeTextContainer
                show={!this.state.isFetchingData && !this.state.showResult}
                />

              <UploadContainer
                show={!this.state.isFetchingData && !this.state.showResult}
                onFormFileSubmit={this.onFormFileSubmit.bind(this)}
                onFormXMLTextSubmit={this.onFormXMLTextSubmit.bind(this)}
								onJSONTextSubmit={this.onJSONTextSubmit.bind(this)}
								onDateRangeSubmit={this.onDateRangeSubmit.bind(this)}
                />

              <ResultContainer
                show={!this.state.isFetchingData && this.state.showResult}
                backToUploadPahse={this.backToUploadPahse.bind(this)}
                resultData={this.state.resultData}
                fileDownload={this.fileDownload.bind(this)}
                downloadResultZip={this.downloadResultZip.bind(this)}
                fileDirectDownload={this.fileDirectDownload.bind(this)}
                fileView={this.fileView.bind(this)}
                fileViewContent={this.fileViewContent.bind(this)}
                viewModalContent={this.viewModalContent.bind(this)}
                />

              <XMLViewerContainer
                showViewer={this.state.showViewer}
                xmlContent={this.state.xmlContent}
                handleCloseXMLViewer={this.handleCloseXMLViewer.bind(this)}
                handleOpenXMLViewer={this.handleOpenXMLViewer.bind(this)}
                />
            </div>

          </div>
      </div>
    );
  }
}

export default App;
