/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import sax from 'sax';
import * as R from 'ramda';
import xmlbuilder from 'xmlbuilder';
// import xmldom from 'xmldom';

class XmlMarker {
	constructor(xmlString) {
		this.xmlString = xmlString || '';
		this.elements = [];
		this.text = '';
		// this.xmlDom = null;
		// try {
		// 	this.xmlDom = new xmldom.DOMParser().parseFromString(this.xmlString);
		// } catch (error) {
		// 	console.log(error);
		// 	throw error;
		// }
		// console.log(new xmldom.XMLSerializer().serializeToString(this.xmlDom));
		this.unmark();
	}

	unmark() {
		let elements = [];
		let text = '';
		let openTags = [{
			id: 0,
			elChildren: []
		}];
		const parser = sax.parser(true, {lowercase: false});
		parser.onerror = function (e) {
			throw e;
		};
		parser.ontext = function (str) {
			text += str;
		};
		parser.onopentag = function (node) {
			const currentParent = openTags[openTags.length-1];
			let element = {
				name: node.name,
				id: elements.length+openTags.length,
				parent: currentParent.id,
				start: text.length,
				attributes: node.attributes,
				elChildren: []
			};
			currentParent.elChildren.push(element);
			openTags.push(element);
		};
		parser.onclosetag = function (name) {
			let element = openTags.pop();
			if (element.name != name)
				throw new Error('Mismatching closing tag', name);

			elements.push(element);
			element.end = text.length;
		};
		parser.write(this.xmlString).close();
		this.elements = elements;
		this.text = text;
	}

	unmarkDom() {
		let elements = [];
		let text = '';
		function recUnmark(node, parent) {
			let element = {
				name: node.nodeName,
				type: node.nodeType,
				id: elements.length,
				parent: parent,
				start: text.length
			};
			if (element.name == '#text') {
				element.text = node.text;
				text += element.text;
			}
			elements.push(element);
			// console.log(element);
			if (element.type == node.DOCUMENT_NODE ||
				element.type == node.ELEMENT_NODE) {
				for (let i = 0; i < node.childNodes.length; i++) {
					recUnmark(node.childNodes[i], element.id);
				}
			}
			element.end = text.length;
		}
		recUnmark(this.xmlDom, 0);
		// console.log(elements);
		// console.log(text);
		this.elements = elements;
		this.text = text;
	}

	getText() {
		return this.text;
	}

	getXml() {
		let tree = this.buildTree();
		// console.log(JSON.stringify(tree, null, 4));
		let xml = this.treeToXml(tree);
		return xml;
	}

	buildTree() {
		let tree = this.elements[this.elements.length-1];
		return this.buildRec(tree);
	}

	buildRec(el) {
		let node = el;
		node.children = [];
		let pos = node.start;
		node.elChildren
			.forEach((iterEl) => {
				// Push eventual text content
				if (iterEl.start > pos) {
					node.children.push(
						this.text.substring(pos, iterEl.start)
					);
				}
				node.children.push(this.buildRec(iterEl));
				pos = iterEl.end;
			});
		// Check if there is other text after last children
		if (pos < node.end) {
			node.children.push(
				this.text.substring(pos, el.end)
			);
		}
		return node;
	}

	treeToXml(tree) {
		const root = xmlbuilder.begin();
		this.elToXml(tree, root);
		const xml = root.end({ pretty: false });
		return xml;
	}

	elToXml(el, parent) {
		const type = typeof el;
		if (parent && type === 'string') {
			parent.txt(el);
		} else if (type === 'object') {
			let node = parent.ele(el.name);
			for (let attr in el.attributes) {
				node.att(attr, el.attributes[attr]);
			}
			if (el.children) {
				el.children.forEach((el) => this.elToXml(el, node));
			}
		} else {
			throw new Error('Unknown element type: ' + type);
		}
	}

	markup(el) {
		let element = {
			name: el.name,
			start: el.start,
			end: el.end,
			attributes: R.clone(el.attributes),
			elChildren: []
		};

		let parent = this.findPossibleParent(el);
		if (!parent) {
			throw new Error('Impossibile to find offset position');
		}
		this.insertElement(element, parent);
	}

	findPossibleParent(el) {
		let root = this.elements[this.elements.length-1];
		let parents = [];
		function search(node) {
			if(el.start >= node.start && el.end <= node.end) {
				parents.push(node);
				node.elChildren.forEach(search);
			}
		}
		search(root);
		return parents.pop();
	}

	insertElement(el, parent) {
		if (parent.elChildren.length == 0) {
			return parent.elChildren.push(el);
		}
		let pos = 0;
		let childrenToMovePos = [];
		for(let i = 0; i < parent.elChildren.length; i++) {
			let child = parent.elChildren[i];
			if (el.start >= child.start) {
				// console.log(el.start, el.end, child.start, child.end);
				pos = i+1;
			}
			if (el.start <= child.start && child.end <= el.end) {
				childrenToMovePos.push(i);
			}
			// Overlap detection
			// TODO: test overlap, empty child, empty el
			if (el.start != el.end && (
				(el.start <= child.start && el.end > child.start && el.end <= child.end) ||
				(el.start >= child.start && el.start < child.end  && el.end >= child.end)
				)) {
				throw new Error('Overlap at: '+ child.name);
			}
		}

		let childrenToMove = childrenToMovePos.map((i) => parent.elChildren[i]);
		let lastChildPos = childrenToMovePos.pop();
		let afterPos = (lastChildPos !== undefined && lastChildPos+1) || pos;
		parent.elChildren = parent.elChildren.slice(0, pos)
							.concat(el)
							.concat(parent.elChildren.slice(afterPos));

		el.elChildren = childrenToMove;
	}
}

export default XmlMarker;
