import { expect } from 'chai';
import path from 'path';
import fs from 'fs';
import XmlMarker from '../';

var getContent = function(file) {
	return fs.readFileSync(path.join(__dirname,  file), { encoding: 'utf-8' });
}

describe('Should handle simple example', () => {
	it('should translate without ns', () => {
		var akn = getContent('xmlSample.xml').trim();
		var marker = new XmlMarker(akn);
		var text = marker.getText();
		expect(text.length).to.equal(246);
		var xmlNew = marker.getXml();
		expect(xmlNew).to.equal(akn);
	});
	it('should translate including ns', () => {
		var akn = getContent('xmlSampleWithNs.xml').trim();
		var marker = new XmlMarker(akn);
		var xmlNew = marker.getXml();
		expect(xmlNew).to.equal(akn);
	});
	it('should translate without indentation', () => {
		var akn = getContent('xmlSampleWithoutIndentation.xml').trim();
		var marker = new XmlMarker(akn);
		var xmlNew = marker.getXml();
		expect(xmlNew).to.equal(akn);
	});
	it('should translate including empty tags', () => {
		var akn = getContent('xmlSampleWithoutText.xml').trim();
		var marker = new XmlMarker(akn);
		var text = marker.getText();
		expect(text).to.equal('');
		var xmlNew = marker.getXml();
		expect(xmlNew).to.equal(akn);
	});
});

describe('Should handle full akomantoso example', () => {
	it('should translate akn file', () => {
		var akn = getContent('aknSample.xml').trim();
		var marker = new XmlMarker(akn);
		var xmlNew = marker.getXml();
		expect(xmlNew).to.equal(akn);
	});
	it('should translate big akn file', () => {
		var akn = getContent('bigAknSample.xml').trim();
		var marker = new XmlMarker(akn);
		var xmlNew = marker.getXml();
		expect(xmlNew).to.equal(akn);
	});
});

describe('Should handle simple tag markup', () => {
	// it('TODO: should markup empty tag', () => {
	// 	var akn = getContent('xmlSample.xml').trim();
	// 	var marker = new XmlMarker(akn);
	// 	var text = marker.getText();
	// 	marker.markup({name: 'span', start: 0, end: 0});
	// 	marker.markup({name: 'span2', start: 5, end: 6});
	// 	expect(text.length).to.equal(246);
	// 	// var xmlNew = marker.getXml();
	// 	// expect(xmlNew).to.equal(akn);
	// });
	var akn = getContent('xmlSample.xml').trim();
	it('should markup tag into empty tag', () => {
		var marker = new XmlMarker(akn);
		var text = marker.getText();
		marker.markup({name: 'span', start: 5, end: 17});
		marker.markup({name: 'span2', start: 210, end: 210});
		marker.markup({name: 'span3', start: 229, end: 235});
		var text2 = marker.getText();
		expect(text).to.equal(text2);
		var xmlNew = marker.getXml();
		expect(xmlNew.includes('<docType><span>')).to.equal(true);
		expect(xmlNew.includes('</span></docType>')).to.equal(true);
		expect(xmlNew.includes('<docNumber><span2/>')).to.equal(true);
		expect(xmlNew.includes('<span3>maggio</span3>')).to.equal(true);
	});
	it('should markup tag wrapping all child', () => {
		var marker = new XmlMarker(akn);
		marker.markup({name: 'div', start: 2, end: 245});
		var xmlNew = marker.getXml();
		expect(xmlNew.includes('<p><div>')).to.equal(true);
		expect(xmlNew.includes('</div></p>')).to.equal(true);
	});
	it('should markup tag within children', () => {
		var marker = new XmlMarker(akn);
		marker.markup({name: 'div', start: 19, end: 20});
		marker.markup({name: 'div', start: 208, end: 210});
		var xmlNew = marker.getXml();
		expect(xmlNew.includes('<div>	</div><docTitle>')).to.equal(true);
		expect(xmlNew.includes('<div>		</div><docNumber>')).to.equal(true);
	});
	it('should markup tag appending child', () => {
		var marker = new XmlMarker(akn);
		marker.markup({name: 'div', start: 244, end: 245});
		var xmlNew = marker.getXml();
		expect(xmlNew.includes('<div>	</div></p>')).to.equal(true);
	});
	it('should throw overlap error', () => {
		var marker = new XmlMarker(akn);
		// Start in p and end up in docType
		expect(() => marker.markup({name: 'div', start: 4, end: 7})
		).to.throw('Overlap at: docType');
		// Start in docTitle and end up in p
		expect(() => marker.markup({name: 'div', start: 25, end: 208})
		).to.throw('Overlap at: docTitle');
		// Start in docNumber and end up in docDate
		expect(() => marker.markup({name: 'div', start: 211, end: 225})
		).to.throw('Overlap at: docNumber');
		// Start in p and end up in docDate
		expect(() => marker.markup({name: 'div', start: 4, end: 225})
		).to.throw('Overlap at: docDate');
	});
});

describe('Should handle simple tag markup on big file', () => {
	var akn = getContent('bigAknSample.xml').trim();
	it('should markup some tags', () => {
		var marker = new XmlMarker(akn);
		// var text = marker.getText();
		// console.log(text.substring(3543162, 3543269));
		// console.log(text.lastIndexOf("il decreto del Ministro dello sviluppo economico 28 aprile 2008, n. 99, pubblicato nella Gazzetta Ufficiale 5 giugno 2008, n. 130.))"));
		marker.markup({name: 'spanTest', start: 1070914, end: 1070963});
		marker.markup({name: 'spanTest2', start: 3543162, end: 3543269});
		var xmlNew = marker.getXml();
		// console.log(xmlNew);
		expect(xmlNew.includes('<spanTest>articolo 13 del decreto-legge 6 luglio 2012, n. 9</spanTest>')).to.equal(true);
		expect(xmlNew.includes('<spanTest2>il decreto del Ministro dello sviluppo economico 28 aprile 2008, n. 99, pubblicato nella Gazzetta Ufficiale</spanTest2>')).to.equal(true);
	});
});
