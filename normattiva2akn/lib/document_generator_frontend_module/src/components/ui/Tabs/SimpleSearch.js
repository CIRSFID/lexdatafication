import React, { Component } from 'react';
import { SingleDatePicker } from 'react-dates';

class SimpleSearch extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
            value: '',
            docDate: null,
            vigenzaDate: null,
            focusedDocDate: false,
            focusedVigDate: false,
            numRedazionale: null
        };
        
        this.handleNumRedazionaleChange = this.handleNumRedazionaleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleNumRedazionaleChange(event) {        
        this.setState({ numRedazionale: event.target.value });
	}

	handleSubmit(e) {
        e.preventDefault(e);   
        let docDate, vigDate;        
        if(this.state.docDate){
            docDate = this.state.docDate.toISOString().split('T')[0]
        }
        if (this.state.vigDate) {
            vigDate = this.state.vigDate.toISOString().split('T')[0]
        }                
        return this.props.onDocSearchSubmit(
            this.state.numRedazionale,
            docDate,
            vigDate);		
    }
    
	handleClear(e) {
		e.preventDefault(e);
        this.setState({ 
            vigenzaDate: null,
            docDate: null,
            numRedazionale: ''
         });
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<h2>{this.props.title}</h2>
                <div> 
                    <input type="text" value={this.state.numRedazionale} onChange={this.handleNumRedazionaleChange} placeholder="Num. Redazionale"/>                                                                             
                </div>
                <div>
                    <SingleDatePicker
                        date={this.state.docDate} // momentPropTypes.momentObj or null
                        onDateChange={date => this.setState({ docDate: date })} // PropTypes.func.isRequired
                        focused={this.state.focusedDocDate} // PropTypes.bool
                        onFocusChange={({ focused }) => this.setState({ focusedDocDate: focused })} // PropTypes.func.isRequired
                        isOutsideRange={() => false}
                        id="docDate_id" // PropTypes.string.isRequired,
                        placeholder="Data publ."
                        displayFormat={'DD-MM-YYYY'}
                    />                                
                    <SingleDatePicker
                        date={this.state.vigenzaDate} // momentPropTypes.momentObj or null
                        onDateChange={date => this.setState({ vigenzaDate: date })} // PropTypes.func.isRequired
                        focused={this.state.focusedVigDate} // PropTypes.bool
                        onFocusChange={({ focused }) => this.setState({ focusedVigDate: focused })} // PropTypes.func.isRequired
                        isOutsideRange={() => false}
                        id="vigDate_id" // PropTypes.string.isRequired,
                        placeholder="Data Vigenza"
                        displayFormat={'DD-MM-YYYY'}
                    /> 
                </div> 
                <div className="text-center">
                    <input type="submit" className="btn btn-primary" value="Convert" />
                    <button className="btn btn-primary" onClick={this.handleClear.bind(this)}>Clear</button>
                </div>
			</form>
		);
	}
}

export default SimpleSearch;
