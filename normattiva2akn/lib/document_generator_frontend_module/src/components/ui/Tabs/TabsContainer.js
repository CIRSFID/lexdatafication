import React, { Component } from 'react';
import { Tab, Col, Row, Nav, NavItem } from 'react-bootstrap';
import TextArea from './TextArea.js';
import FileUploader from './FileUploader.js';
import DateRange from './DateRange.js';
import SimpleSearch from './SimpleSearch.js';

class TabsContainer extends Component {

  render() {
    return (
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <Row className="clearfix">
              <Col sm={4}>
            <br /><br />
                <Nav bsStyle="pills" stacked>
                  <NavItem eventKey="first">JSON</NavItem>
							    <NavItem eventKey="second">UPLOAD JSON/ZIP</NavItem>
                  <NavItem eventKey="third">CONVERT DOCUMENT</NavItem>
                  <NavItem eventKey="fourth">DOCS</NavItem>
                </Nav>
              </Col>
              <Col sm={8}>
                <Tab.Content animation>
									<Tab.Pane eventKey="first">
                    <TextArea
												title="JSON (copy and paste)"
                        onJSONTextSubmit={this.props.onJSONTextSubmit}
											/>
									</Tab.Pane>
                  <Tab.Pane eventKey="second">
                    <FileUploader
                      title="JSON/ZIP UPLOAD"
                      onFormFileSubmit={this.props.onFormFileSubmit}
                    />
                  </Tab.Pane>
                  <Tab.Pane eventKey="third">
                  <SimpleSearch
                      title="CONVERTI DOCUMENTO"                      
                      onDocSearchSubmit={this.props.onDocSearchSubmit}
                    />
                  </Tab.Pane>                  
                  <Tab.Pane eventKey="fourth">
											<h2>DOCS</h2>
											<div className="text-left">
                    <p>Viene esposta l'api in POST <code>'/convert'</code> che prende come parametro un json e restituisce un conentuo Akoma Ntoso</p>
                    <p>Viene esposta l'api in MULTIPART <code>'/upload-and-convert'</code> che converte un file json o un file zip contentente dei json in una lista di file Akoma Ntoso</p>
											</div>
									</Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
    );
  }
}

export default TabsContainer;
