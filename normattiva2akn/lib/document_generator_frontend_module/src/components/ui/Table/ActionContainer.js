import React, { Component } from 'react';
import { ButtonToolbar, ButtonGroup, Button, Glyphicon } from 'react-bootstrap';

class ActionContainer extends Component {  
  
    handleFileDownloadClick(e){
        e.preventDefault();
        console.log(this.props.rowData);        
        this.props.fileDirectDownload(this.props.rowData);
    }

    handleWiewFileClick(e) {
        e.preventDefault();
        this.props.fileViewContent(this.props.rowData.xmlPath, 'xml');
    }

    handleViewValidationsClick(e) {
        e.preventDefault();        
        this.props.fileViewContent(this.props.rowData.validationPath, 'vJson');
    }

    render() {
        return ( 
            <div>
                <ButtonToolbar>
                    <ButtonGroup>
                        <Button bsSize="xsmall" onClick={this.handleViewValidationsClick.bind(this)}>
                            <Glyphicon glyph="wrench" />
                        </Button>
                        <Button bsSize="xsmall" onClick={this.handleFileDownloadClick.bind(this)}>
                            <Glyphicon glyph="download-alt" />
                        </Button>
                        <Button bsSize="xsmall" onClick={this.handleWiewFileClick.bind(this)}>
                            <Glyphicon glyph="search" />
                        </Button>  
                    </ButtonGroup>
                </ButtonToolbar>
            </div>
        );
    }
}

export default ActionContainer;
