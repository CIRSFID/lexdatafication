
const mapping = (data) => {
    return data.map((el) => {
        return {
            tipoDoc: el.docType            
        }
    });
}

exports.map = mapping;