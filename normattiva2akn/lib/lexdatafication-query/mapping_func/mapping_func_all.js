
const mapping = (data) => {
    return data.map((el) => {                    
        return {
            titoloDoc: el.docTitle,
            tipoDoc: el.docType,
            fase: el.otherReferences
        }
    });
}

exports.map = mapping;