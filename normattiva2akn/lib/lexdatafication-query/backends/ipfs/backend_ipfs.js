import http from 'http';
import VError from 'verror';
import sax from 'sax';
import { basename } from 'path';
import {PassThrough} from 'stream';
import FormData from 'form-data';

import config from './config.json';

const backend_ipfs = {
	// Call callback with err or list of files inside dir.

	getDir : function (path, callback) {
		var resource = encode(config.rest + config.baseCollection + path);
		console.log('GET DIR', resource);
		http.get({
			host: config.host,
			port: config.port,
			auth: config.auth,
			path: resource
		}, function (res) {
			if (res.statusCode == 404) return callback(null, []);

			// Convert ipfs format to an array of file/dir names.
			var saxStream = sax.createStream(false, {}),
				fileList = [];
			saxStream.on("opentag", function (tag) {
				var name = tag.attributes.NAME,
					isDir = (tag.name == 'EXIST:COLLECTION'),
					isFile = (tag.name == 'EXIST:RESOURCE');
				if (isDir || isFile)
					fileList.push(path + basename(name) + (isDir ? '/' : ''));
			});
			saxStream.on('error', function (err) {
				res.unpipe(saxStream);
				callback(new VError(err, 'Exist error getting file list'));
			});
			saxStream.on('end', function () {
				fileList.shift(); // Remove our collection name
				console.log('filelist', fileList);
				if (res.statusCode != 200)
					callback(new Error('Exist GET DIR request has status code ' + res.statusCode));
				else callback(undefined, fileList.map(decode_ls));
			});
			res.pipe(saxStream);
		});
	},

	// Read file from path and write it to output stream.
	// Call callback on success or error (VError object or 404 int).

	// -----------> curl "http://localhost:5001/api/v0/cat?arg=<ipfs-path>"
	getFile : function (output, path, file, callback) {
		var resource = encode(config.rest + config.baseCollection + path + file) + '?_xsl=no'; // ?_xsl=no disable the xml stylesheet process
		console.log('GET FILE', resource);
		http.get({
			host: config.host,
			port: config.port,
			auth: config.auth,
			path: resource
		}, function (res) {
			if (res.statusCode == 404)
				return callback(404);
			else if (res.statusCode != 200)
				return callback(new Error('Exist GET request has status code ' + res.statusCode));
			res.on('error', function (err) {
				res.unpipe(output);
				return callback(new VError(err, 'Error getting file'));
			});
			res.on('end', function () {
				return callback();
			});
			res.pipe(output);
		});
	},


	checkIfExist: function (path, file, callback) {
		var stream = new PassThrough();
		backend_ipfs.getFile(stream,path,file,(err)=>{
			if (err == 404)
				return callback(false);
			else if (err != undefined)
				return callback(null, err)
			else
				return callback(true);
		})
		stream.on("data",()=>{});
	},


	/**
	 * Save input stream to file in path, creating directory
	 * if it does not exist. Call callback on success or error.
	 * @param {Stream} input- the stream of the file to be pushed.
	 * @param {String} path - the path in which we want to save the file.
	 * @param {String} file - the file name.
	 * @param {Function} callback - the callback function used to pass evental errors to the caller
	 *
	 */
	 //-----------> curl -F file=@myfile "http://localhost:5001/api/v0/block/put?format=v0&mhtype=sha2-256&mhlen=-1"
	putFile : function (input, path, file, callback) {
		var resource = encode_write( config.api_ipfs + "add" );
		console.log('PUT FILE', resource);
		var form = new FormData();
		form.append('file', input)

		var request = http.request({
			method: 'POST',
			host: config.host,
			port: config.port,
			path: resource,
			headers: form.getHeaders()
		}, function(res){
			console.log(res.statusCode);
			res.setEncoding('utf8');
			res.on('data', function(d) {
			  console.log(d);
			});
		});

		form.pipe(request);


	},
	/*
	// Delete the passed file
	// Call callback on success or error
	// TODO: remove empty directories
	exports.deleteFile = function (path, file, callback, existConfig) {
		existConfig = R.merge(config, existConfig);
		var resource = encode_write(existConfig.rest + existConfig.baseCollection + path + '/' + file);
		console.log('DELETE FILE', resource);
		http.request({
			method: "DELETE",
			host: existConfig.host,
			port: existConfig.port,
			auth: existConfig.auth,
			path: resource
		}, function (res) {
			if(res.statusCode == 404) return callback(404);
			else if (res.statusCode != 200)
				return callback(new Error('Exist DELETE request has status code ' + res.statusCode));

			callback();
			// Delete empty collections, don't care about the output
			deleteEmptyCollections(config.baseCollection, function() {});
		}).end();
	};
	*/
	// Notes:
	// Exist handling of special charactes is really bad.
	// We must do double URI encoding when retrieving files
	// https://github.com/eXist-db/exist/issues/44

}
function encode_write(str) {
		return str.split('/').map(encodeURIComponent).join('/');
    // return (
    //     str
    //         .replace('%', '%25')
    //         .replace('#', '%23')
    //         .replace(':', '%3A')
    // );
}
/*
function decode_ls(str) {
	console.log('str', str);
	return decodeUR
		str.replace(encodedRegex, replaceEncoded)
	);
	// .replace('%23', '#')
	// .replace('%3A', ':')
	// .replace('%25', '%');
}
*/
function encode(str) {
	return encodeURI(str);
	// Uncomment for eXist 2.2
	// .replace(specialRegex, replaceSpecial)
	// // .replace('#', '%23')
	// // .replace(':', '%3A')
	// // .replace(/%(?!27|28|29|2A)/g, '%25')
	// .replace(/%(?!20)/g, '%25')
	// ;
}
/*
var specialMap =
	{
		// "!": "%21",
		"#": "%23",
		"$": "%24",
		"&": "%26",
		// "'": "%27",
		// "(": "%28",
		// ")": "%29",
		// "*": "%2A",
		"+": "%2B",
		",": "%2C",
		":": "%3A",
		";": "%3B",
		"=": "%3D",
		"?": "%3F",
		"@": "%40",
		"[": "%5B",
		"]": "%5D"
	};

var specialRegex = new RegExp('([' + (Object.keys(specialMap).map(R.concat('\\')).join('')) + '])', 'g');
var replaceSpecial = function (special) { return specialMap[special]; };
var encodedMap = R.invertObj({
	"!": "%21",
	"#": "%23",
	"$": "%24",
	"&": "%26",
	"'": "%27",
	"(": "%28",
	")": "%29",
	"*": "%2A",
	"+": "%2B",
	",": "%2C",
	// "/": "%2F",
	":": "%3A",
	";": "%3B",
	"=": "%3D",
	"?": "%3F",
	"@": "%40",
	"[": "%5B",
	"]": "%5D"
});
var encodedRegex = new RegExp('(' + (Object.keys(encodedMap).join('|')) + ')', 'g');
var replaceEncoded = function (encoded) { return encodedMap[encoded]; };
// console.logpecialRegex)
// console.log(encodedRegex)
//
// console.log('!__#__$__&__\'__(__)__*__+__,__/__:__;__=__?__@__[__]__è');
// console.log(encode('!__#__$__&__\'__(__)__*__+__,__/__:__;__=__?__@__[__]__è'));
// console.log(decode_ls(encode('!__#__$__&__\'__(__)__*__+__,__/__:__;__=__?__@__[__]__è')));
*/
export default backend_ipfs
