/* eslint-disable */

var webpack = require('webpack');
var uglifyJsPlugin = require('uglifyjs-webpack-plugin');
var path = require('path');

module.exports = {
    entry: path.resolve(__dirname, './server.js'),

    output: {
        path: path.resolve(__dirname, 'dist'),
		filename: 'lexdatafication-api-server.min.js'
    },

    target: 'node',

    plugins: [
		new uglifyJsPlugin()
    ],

    resolve: {
        extensions: ['.js'],
        modules: [
            'node_modules',
        ],
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'es2016', 'es2017'],
                    plugins: ['transform-runtime']
                },
            }, {
                test: /\.json$/,
                use: 'json-loader',
            },
        ],
    }
};
