import express from 'express';
import dateUtils from '../utils/date_utils';
import requestUtils from '../utils/request_utils';
import config from '../configs/config.json';
import * as R from 'ramda';

const router = express.Router();
const routerAll = express.Router();
const routerReport = express.Router();

router.use('/all', routerAll);
router.use('/report', routerReport);

routerAll.get('/:n?', (request, response) => {
    if (!request.params.n || isNaN(request.params.n)){
        response.writeHead(500, { "Content-Type": "application/json" });
        return response.end(JSON.stringify({
            "error": "Richiesta non valida"
        }));
    }
    let interval = dateUtils.getMonthsIntervalFromNow(request.params.n);
    let url = config.exist.intervalQueryPath + 'start=' + interval.startDate + '&end=' + interval.endDate;
    return requestUtils.performRequest(url, response, R.identity);
});	

routerAll.get('/:startDate/:endDate', (request, response) => {
    if (!request.params.startDate || !request.params.endDate) {
        response.writeHead(500, { "Content-Type": "application/json" });
        return response.end(JSON.stringify({
            "error": "Richiesta non valida"
        }));
    }
    let interval = {
        startDate: request.params.startDate,
        endDate: request.params.endDate
    };    
    let url = config.exist.intervalQueryPath + 'start=' + interval.startDate + '&end=' + interval.endDate;
    return requestUtils.performRequest(url, response, R.identity);
});

routerReport.get('*', (request, response) => {
    let url = config.exist.reportQueryPath;
    response.json(normalizeDocTypes({
        "total": "9469",
        "valid": "8089",
        "notValid": "1380",
        "docTypes": [
            {
                "docType": "DECRETO",
                "total": "1890",
                "valid": "1504",
                "notValid": "386"
            },
            {
                "docType": "DECRETO-LEGGE",
                "total": "2171",
                "valid": "1897",
                "notValid": "274"
            },
            {
                "docType": "LEGGE",
                "total": "3677",
                "valid": "3349",
                "notValid": "328"
            },
            {
                "docType": "DECRETO LEGISLATIVO",
                "total": "1731",
                "valid": "1339",
                "notValid": "392"
            }
        ]
    }));
    // requestUtils.performRequestRaw(url, (err, data) => {
    //     if (err) {
    //         response.writeHead(500, { "Content-Type": "application/json" });
    //         return response.end(JSON.stringify({
    //             "error": err
    //         }));
    //     }
    //     if (data && data.docTypes) {
    //         data.docTypes = data.docTypes.reduce((resObj, obj) => {
    //             resObj[obj.docType] = obj;
    //             return resObj;
    //         }, {});
    //     }
    //     return response.json(data);
    // });
});

function normalizeDocTypes(data) {
    if (data && data.docTypes) {
        data.docTypes = data.docTypes.reduce((resObj, obj) => {
            resObj[obj.docType] = obj;
            return resObj;
        }, {});
    }
    return data;
}

exports.router = router;
