import express from 'express';
import dateUtils from '../utils/date_utils';
import requestUtils from '../utils/request_utils';
import config from '../configs/config.json';
import * as R from 'ramda';

const router = express.Router();
const routerAll = express.Router();

router.use('/all', routerAll);

routerAll.get('/:n?', (request, response) => {
    if (!request.params.n || isNaN(request.params.n)){
        response.writeHead(500, { "Content-Type": "application/json" });
        return response.end(JSON.stringify({
            "error": "Richiesta non valida"
        }));
    }
    let interval = dateUtils.getMonthsIntervalFromNow(request.params.n);
    let url = config.exist.intervalQueryPath + 'start=' + interval.startDate + '&end=' + interval.endDate;
    return requestUtils.performRequest(url, response, R.identity);
});	

routerAll.get('/:startDate/:endDate', (request, response) => {
    if (!request.params.startDate || !request.params.endDate) {
        response.writeHead(500, { "Content-Type": "application/json" });
        return response.end(JSON.stringify({
            "error": "Richiesta non valida"
        }));
    }
    let interval = {
        startDate: request.params.startDate,
        endDate: request.params.endDate
    };    
    let url = config.exist.intervalQueryPath + 'start=' + interval.startDate + '&end=' + interval.endDate;
    return requestUtils.performRequest(url, response, R.identity);
});

exports.router = router;
