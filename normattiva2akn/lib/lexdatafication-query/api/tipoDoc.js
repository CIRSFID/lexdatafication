import express from 'express';
import dateUtils from '../utils/date_utils';
import requestUtils from '../utils/request_utils';
import mapping from '../mapping_func/mapping_func_tipoDoc';
import config from '../configs/config.json';

const router = express.Router();

router.get('/:n?', (request, response) => {
    if (!request.params.n || isNaN(request.params.n)) {
        response.writeHead(500, { "Content-Type": "application/json" });
        return response.end(JSON.stringify({
            "error": "Richiesta non valida"
        }));
    }
    let interval = dateUtils.getMonthsIntervalFromNow(request.params.n);
    let url = config.exist.intervalQueryPath + 'start=' + interval.startDate + '&end=' + interval.endDate;
    return requestUtils.performRequest(url, response, mapping.map);
});

router.get('/:startDate/:endDate', (request, response) => {
    if (!request.params.startDate || !request.params.endDate) {
        response.writeHead(500, { "Content-Type": "application/json" });
        return response.end(JSON.stringify({
            "error": "Richiesta non valida"
        }));
    }
    let interval = {
        startDate: request.params.startDate,
        endDate: request.params.endDate
    };
    let url = config.exist.intervalQueryPath + 'start=' + interval.startDate + '&end=' + interval.endDate;
    return requestUtils.performRequest(url, response, mapping.map);
});

exports.router = router;
