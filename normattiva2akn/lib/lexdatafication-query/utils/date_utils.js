import moment from 'moment';

const getMonthsIntervalFromNow = (numMonths) => {
    let endDate = moment();
    let startDate = moment().subtract(numMonths, 'months');
    endDate = endDate.format("YYYY-MM-DD");
    startDate = startDate.format("YYYY-MM-DD");
    return {
        endDate,
        startDate
    };
};

exports.getMonthsIntervalFromNow = getMonthsIntervalFromNow;