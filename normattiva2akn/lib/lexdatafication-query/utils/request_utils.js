import http from 'http';

const performRequest = (url, response, mappingFunc) => {    
    http.get(url, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            try {
                if (data === 'null') {
                    console.log("No documents")
                    response.writeHead(200, { "Content-Type": "application/json" });
                    return response.end(JSON.stringify({
                        "documents": []
                    }));
                }
                let resData = JSON.parse(data);
                if(mappingFunc){
                    resData = mappingFunc(resData.documents);
                }                
                response.writeHead(200, { "Content-Type": "application/json" });
                response.end(JSON.stringify({ documents: resData }));
                //prepare responce
            } catch (err) {
                console.log("Error: " + err);
                response.writeHead(500, { "Content-Type": "application/json" });
                return response.end(JSON.stringify({
                    "error": "Richiesta non valida"
                }));
            }
        });
    }).on("error", (err) => {
        console.log("Error: " + err.message);
        response.writeHead(500, { "Content-Type": "application/json" });
        return response.end(JSON.stringify({
            "errors": err.message
        }));
    });
};

const performRequestRaw= (url, cb) => {    
    http.get(url, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            if (data === 'null') {
                return cb();
            }
            return cb(null, JSON.parse(data));
        });
    }).on("error", (err) => {
        return cb(err);
    });
};

exports.performRequest = performRequest;
exports.performRequestRaw = performRequestRaw;