var fs = require('fs');
var path = require('path');

var distFileName = 'nmt.api.min.js';
var srcPath = '/dist/' + distFileName;
var moduleName = 'document_generator';
var deps = ['bulk_server_module'];

function copyFile(source, target, cb) {
	var cbCalled = false;

	var rd = fs.createReadStream(source);
	rd.on("error", function (err) {
		done(err);
	});
	var wr = fs.createWriteStream(target);
	wr.on("error", function (err) {
		done(err);
	});
	wr.on("close", function (ex) {
		done();
	});
	rd.pipe(wr);

	function done(err) {
		if (!cbCalled) {
			cb(err);
			cbCalled = true;
		}
	}
}
console.log("Coping to dependecies folder...");
deps.forEach(function (currentModule) {
	var distPath = '../' + currentModule + '/lib/' + moduleName + '/' + distFileName;
	copyFile(path.join(__dirname, srcPath), path.join(__dirname, distPath), function () {
		console.log("Updated module: ", currentModule);
	});
})
