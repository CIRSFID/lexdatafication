/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import camelCase from 'camelcase';
import * as R from 'ramda';
import traverse from 'traverse';
import _ from 'underscore';
import AknUri from '../utils/AknUri';

class MetadataBuilder {
	constructor(allData, refsData) {
		this.data = allData.metadati;
		this.allData = allData;
		this.refsData = refsData;
		this.invalidDatesTypes = ['0-00-00'];
		this.tipoDocRefinings = [
			{
				name: 'DECRETO DEL PRESIDENTE DELLA REPUBBLICA',
				ref: 'dpr'
			}
		];
	}

	checkDates() {
		if (!this.data.dataDoc || this.invalidDatesTypes.indexOf(this.data.dataDoc) !== -1) {
			this.data.dataDoc = '9999-09-09';
		}
		if (!this.data.dataVigoreOriginale || this.invalidDatesTypes.indexOf(this.data.dataVigoreOriginale) !== -1) {
			this.data.dataVigoreOriginale = '';
		}
		if (!this.data.dataPubblicazione || this.invalidDatesTypes.indexOf(this.data.dataPubblicazione) !== -1) {
			this.data.dataPubblicazione = '9999-09-09';
		}
		if (!this.data.tipoPubblicazione || this.invalidDatesTypes.indexOf(this.data.tipoPubblicazione) !== -1) {
			this.data.tipoPubblicazione = '9999-09-09';
		}
	}

	refineTipoDoc(stringIn) {
		let refinedTipoDoc = stringIn;
		this.tipoDocRefinings.forEach((el) => {
			if (el.name === stringIn) {
				refinedTipoDoc = el.ref;
			}
		});
		//refinedTipoDoc = refinedTipoDoc.replace(' ','-');
		refinedTipoDoc = camelCase(refinedTipoDoc);
		return refinedTipoDoc;
	}

	createContent() {
		this.checkDates();
		const workDate = this.data.dataDoc;
		const versionData = this.data.dataVigoreOriginale || workDate;
		const manifestationDate = new Date().toISOString().substr(0, 10);
		const pubDate = this.data.dataPubblicazione;
		const pubName = this.data.tipoPubblicazione;
		const pubNumber = this.data.numeroPubblicazione;
		const urnNir = this.data.urn;
		const eliNir = this.data.eli;
		const uri = this.buildUri();

		this.applyUriModification();

		let metaStr = `<meta>
				<identification source="#source">
					<FRBRWork>
						<FRBRthis value="${uri.work()}"/>
						<FRBRuri value="${uri.work(true)}"/>
						<FRBRalias value="${urnNir}" name="urn:nir"/>
						<FRBRalias value="${eliNir}" name="eli"/>
						<FRBRdate name="" date="${workDate}"/>
						<FRBRauthor as="#author" href=""/>
						<FRBRcountry value="it"/>
					</FRBRWork>
					<FRBRExpression>
						<FRBRthis value="${uri.expression()}"/>
						<FRBRuri value="${uri.expression(true)}"/>
						<FRBRdate name="" date="${versionData}"/>
						<FRBRauthor as="#author" href=""/>
						<FRBRlanguage language="ita"/>
					</FRBRExpression>
					<FRBRManifestation>
						<FRBRthis value="${uri.manifestation()}"/>
						<FRBRuri value="${uri.manifestation(true)}"/>
						<FRBRdate name="" date="${manifestationDate}"/>
						<FRBRauthor as="#author" href=""/>
					</FRBRManifestation>
				</identification>
				<publication date="${pubDate}" name="${pubName}" showAs="" number="${pubNumber}"/>
				<lifecycle source="#palmirani">
					${this.generateLifecycleEvents()}
				</lifecycle>
				<analysis source="#palmirani">
					${this.generatePassiveModifications()}
					${this.generateOtherReferenceMeta()}
				</analysis>
				<references source="#source">
					<TLCPerson showAs="Somebody" href="/akn/ontology/person/somebody" eId="source"/>
					<TLCRole showAs="Author" href="/akn/ontology/role/author" eId="author"/>
					<TLCConcept eId="concept_1" href="/akn/it/ontology/concept/ipzs/lavoriPreparatori" showAs="Lavori Preparatori"/>
					${this.generateRolesMeta()}
					${this.generatePersonsMeta()}
				</references>
		</meta>`;
		metaStr = metaStr.replace(/[\t\n]+/g, '');
		return metaStr;
	}

	generatePersonsMeta() {
		const persons = this.refsData.filter(el => el.kind === 'person');
		const personsTag = [];
		_.uniq(persons, ['other_data', 'refers_to']).forEach((el) => {
			personsTag.push(`<TLCPerson eId="${el.other_data.refers_to}" href="/akn/ontology/person/it/${el.other_data.refers_to}" showAs="${el.other_data.full_name}"/>`);
		});
		return personsTag.join('');
	}

	generateRolesMeta() {
		//TO-DO: Add roles for all possible roles kind
		const roles = this.refsData.filter(el => ['ruolo_istituzionale'].indexOf(el.kind) >= 0);
		const rolesTag = [];
		_.uniq(roles, ['value']).forEach((el) => {
			//TO-DO: wait for the json format from poldo
			rolesTag.push(`<TLCRole eId="${camelCase(el.value)}" href="/akn/ontology/role/it/${camelCase(el.value)}" showAs="${el.value}"/>`)
		});
		return rolesTag.join('');
	}

	generateOtherReferenceMeta() {
		const references = this.refsData.filter(el =>
			(el.kind === 'atto_giudiziario' && el.akn_uri.indexOf('/cost/') > 0) ||
				(el.kind === 'lavori_preparatori'));
		if (references.length > 0) {
			const referencesTag = [];
			referencesTag.push('<otherReferences source="#cirsfid">');
			references.forEach((el) => {
				//TO-DO: wait for the json format from poldo
				if (el.kind === 'atto_giudiziario') {
					referencesTag.push(`<alternativeReference for="${el.akn_uri}" href="${el.url}" refersTo="#URL"/>`);
				} else if (el.kind === 'lavori_preparatori') {
					referencesTag.push(`<alternativeReference for="${el.url}" href="${el.url}" refersTo="#lavoriPreparatori"/>`);
				}
			});
			referencesTag.push('</otherReferences>');
			return referencesTag.join('');
		}
		return '';
	}

	generatePassiveModifications() {
		const passiveModTag = [];
		passiveModTag.push('<passiveModifications>');
		passiveModTag.push('</passiveModifications>');
		return passiveModTag.join('');
	}

	buildUri() {
		const uri = AknUri.empty();
		uri.country = 'it';
		uri.type = 'act';
		uri.subtype = this.refineTipoDoc(this.data.tipoDoc);
		uri.author = 'stato';
		uri.date = this.data.dataDoc;
		uri.language = 'ita';
		uri.version = this.data.dataVigoreOriginale;
		uri.component = 'main';
		if (this.data.numDoc === '0') {
			uri.name = this.data.redazione;
		} else {
			uri.name = this.data.numDoc;
		}
		return uri;
	}

	convertModType(splitCont) {
		let strToReturn = splitCont;
		const assosiations = {
			art: 'art_',
			capo: 'chp_',
			cap: 'chp_',
			sezione: 'sec_',
			sez: 'sec_',
			heading: 'heading',
			let: 'num_',
			com: 'para_'
		};
		Object.keys(assosiations).map((key) => {
			if (strToReturn.indexOf(key) !== -1) {
				strToReturn = strToReturn.replace(key.toString(), assosiations[key]);
			}
		});
		return strToReturn;
	}

	applyUriModification() {
		const convertMod = (uriMod) => {
			const splitCont = uriMod.split('-');
			let finalStr = '';
			let artStr, paraStr, numStr;
			if (splitCont[0]) {
				artStr = this.convertModType(splitCont[0]);
				finalStr += artStr;
			}
			if (splitCont[1]) {
				paraStr = this.convertModType(splitCont[1]);
				finalStr += `__${paraStr}`;
			}
			if (splitCont[2]) {
				numStr = this.convertModType(splitCont[2]);
				finalStr += `__${numStr}`;
			}
			return finalStr;
		};
	}

	generateLifecycleEvents() {
		const dateSort = (a, b) => (a > b ? 1 : -1);
		return R.sort(
			dateSort,
			R.uniq(traverse(this.allData)
				.reduce((acc, el) => {
					if (el && el.inizioVigore) { acc.push(el.inizioVigore); }
					return acc;
				}, []))
		)
			.filter(el => el && el.length === 10)
			.map((date, i) => {
				const source = i ? `#rp${i}` : '#ro1'; // Original event
				return `<eventRef eId="eventRef_${i}" date="${date}" source="${source}"/>`;
			})
			.join('');
	}
}

module.exports = MetadataBuilder;
