/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import promiseSequential from 'promise-sequential';
import ArticleBuilder from './ArticleBuilder.js';

import appConfig from '../../configs/configs.json';

const bodyParserUrl = appConfig.parser.phpServiceUrl+'/paragraph/';

class GenericBuilder {
	constructor(elementsArray) {
		this.elementsArray = elementsArray || [];
		this.articlesArray = [];
		this.articleBuilder = new ArticleBuilder(bodyParserUrl, 'ita', null, 'act_cad');
		this.getAllArticlesInHierarchy(this.elementsArray);
	}

	createContent() {
		return new Promise((resolve, reject) => {
			this.createParsedContent(this.articlesArray)
				.then((res) => {
					this.matchAllArticlesInHierarchy(res);
					resolve(this.elementsArray);
				}).catch((rjReason) => {
					console.log(rjReason);
					reject(rjReason);
				});
		});
	}

	createParsedContent(articlesArray) {
		const promiseArray = [];

		return new Promise((resolve, reject) => {
			if (articlesArray.length === 0){
				return resolve([]);
			}
			for (let k = 0; k < articlesArray.length; k++) {
				//NB: considering direct down to paragraph node in response
				promiseArray.push(this.getTranslateFunction(articlesArray[k], 'paragraph'));
			}
			console.log("calling remote parser")
			promiseSequential(promiseArray)
				.then((parsedArticles) => {
					console.log('---- Article parser resolved promise ----');
					//parsedArticles.forEach(el => console.log(el));
					for (let k = 0; k < parsedArticles.length; k++) {
						for (let z = 0; z < articlesArray.length; z++) {
							//TO-DO:Check compare properties miss rubricaNir
							if (parsedArticles[k].idInterno === articlesArray[k].idInterno &&
								parsedArticles[k].nomeNir === articlesArray[k].nomeNir &&
								parsedArticles[k].idNir === articlesArray[k].idNir &&
								parsedArticles[k].numNir === articlesArray[k].numNir) {
								//TO-DO:Check field names es. testo
								articlesArray[k].testo = parsedArticles[k].testo;
							}
						}
					}
					resolve(articlesArray);
				}).catch((rjReason) => {
					console.log(rjReason);
					reject(rjReason);
				});
		});
	}

	getAllArticlesInHierarchy(elementsArray) {
		for (let k = 0; k < elementsArray.length; k++) {
			if (elementsArray[k].elementoArticolato
				&& elementsArray[k].elementoArticolato.nomeNir !== 'articolo') {
					this.getAllArticlesInHierarchy(elementsArray[k].elementoArticolato.elementi);
			} else {
				this.articlesArray.push(elementsArray[k].elementoArticolato);
			}
		}
	}

	matchAllArticlesInHierarchy(parsedArticles) {
		for (let k = 0; k < this.elementsArray.length; k++) {
			if (this.elementsArray[k].elementoArticolato.elementi) {
				this.getAllArticlesInHierarchy(this.elementsArray[k].elementoArticolato.elementi);
			} else if (this.elementsArray[k].elementoArticolato.nomeNir === 'articolo') {
				for (let z = 0; z < parsedArticles.length; z++) {
					if (parsedArticles[z].idInterno === this.elementsArray[k].elementoArticolato.idInterno &&
						parsedArticles[z].nomeNir === this.elementsArray[k].elementoArticolato.nomeNir &&
						parsedArticles[z].numNir === this.elementsArray[k].elementoArticolato.numNir &&
						parsedArticles[z].idNir === this.elementsArray[k].elementoArticolato.idNir) {
						//TO-DO:Check field names es. testo
						this.elementsArray[k].elementoArticolato.testo = parsedArticles[k].testo;
					}
				}
			}
		}
	}

	getAllArticles() {
		return this.articlesArray;
	}

	getTranslateFunction(article, type) {
		return () => this.articleBuilder.createContent(article, type);
	}
}

module.exports = GenericBuilder;
