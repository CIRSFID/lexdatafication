/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import TagCreator from '../utils/TagCreator';

class ParagraphBuilder {
	constructor(jsonElement, paragraphObj) {
		this.jsonElement = jsonElement;
		this.paragraphObj = paragraphObj;
	}

	createContent() {
		if (!this.paragraphObj) {
			//this.jsonElement.testo = `<content>${TagCreator.createSimpleTag('p', this.jsonElement.testo)}</content>`;
			return this.jsonElement;
		}

		this.paragraphObj.forEach((paragraphItem) => {
			function escapeRegExp(str) {
				return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
			}
			//replace '/' char
			if (paragraphItem.value.endsWith('/')) {
				paragraphItem.value = paragraphItem.value.substring(0, paragraphItem.value.length - 1);
			}
			if (paragraphItem.bodyitem.endsWith('/')) {
				paragraphItem.bodyitem = paragraphItem.bodyitem.substring(0, paragraphItem.bodyitem.length - 1);
			}
			// Usefull when finished with a num e.g. 6.
			paragraphItem.value = paragraphItem.value.trim();

			let tmpString = paragraphItem.value;
			if (paragraphItem.numparagraph) {
				tmpString = tmpString.replace(paragraphItem.numparagraph, `<paragraph><num>${paragraphItem.numparagraph}</num>`);
			}
			if (paragraphItem.contains.hasOwnProperty('item1')) {
				tmpString = tmpString.replace(paragraphItem.bodyitem,
					`<list><intro>${TagCreator.createSimpleTag('p', paragraphItem.bodyitem)}</intro>`);
				this.jsonElement.testo = this.jsonElement.testo.replace(paragraphItem.value, tmpString);
				for (let k = 0; k < paragraphItem.contains.item1.length; k++) {
					if (k === paragraphItem.contains.item1.length - 1) {
						this.hanldeItem1Entry(paragraphItem.contains.item1[k], true);
					} else { this.hanldeItem1Entry(paragraphItem.contains.item1[k]); }
				}
			} else {
				if (paragraphItem.bodyitem) {
					tmpString = tmpString.replace(paragraphItem.bodyitem, `<content>${TagCreator.createSimpleTag('p', paragraphItem.bodyitem)}</content></paragraph>`);
				} else { // close paragraph opened at line 72
					tmpString = tmpString.replace('</num>', '</num></paragraph>');
				}
				let paraReg = escapeRegExp(paragraphItem.value);
				paraReg = new RegExp(`${paraReg}(?!\s*<\/num>)`);
				this.jsonElement.testo = this.jsonElement.testo.replace(paraReg, tmpString);
			}
		});

		//refining
		this.jsonElement.testo = this.refineXmlString(this.jsonElement.testo);
		return this.jsonElement;
	}

	hanldeItem1Entry(item1, isLast) {
		if (item1.value[item1.value.length - 1] === '/') {
			item1.value = item1.value.substring(0, item1.value.length - 1);
		}
		if (item1.bodyitem[item1.bodyitem.length - 1] === '/') {
			item1.bodyitem = item1.bodyitem.substring(0, item1.bodyitem.length - 1);
		}

		item1.value = item1.value.trim();

		let tmpString = item1.value;
		if (item1.numitem) {
			tmpString = tmpString.replace(item1.numitem, `<point><num>${item1.numitem}</num>`);
		}
		if (item1.contains.hasOwnProperty('item2')) {
			tmpString = tmpString.replace(item1.bodyitem, `<list><intro>${TagCreator.createSimpleTag('p', item1.bodyitem)}</intro>`);
			this.jsonElement.testo = this.jsonElement.testo.replace(item1.value, tmpString);
			for (let k = 0; k < item1.contains.item2.length; k++) {
				if (k === item1.contains.item2.length - 1) {
					this.hanldeItem2Entry(item1.contains.item2[k], true, isLast);
				} else { this.hanldeItem2Entry(item1.contains.item2[k]); }
			}
		} else {
			tmpString = tmpString.replace(item1.bodyitem, `<content>${TagCreator.createSimpleTag('p', item1.bodyitem)}</content></point>`);
			if (isLast) {
				this.jsonElement.testo = this.jsonElement.testo.replace(item1.value, `${tmpString}</list></paragraph>`);
			} else {
				this.jsonElement.testo = this.jsonElement.testo.replace(item1.value, tmpString);
			}
		}
	}

	hanldeItem2Entry(item2, isLast, isLastInList) {
		if (item2.value[item2.value.length - 1] === '/') {
			item2.value = item2.value.substring(0, item2.value.length - 1);
		}
		if (item2.bodyitem[item2.bodyitem.length - 1] === '/') {
			item2.bodyitem = item2.bodyitem.substring(0, item2.bodyitem.length - 1);
		}

		item2.value = item2.value.trim();

		let tmpString = item2.value;
		if (item2.numitem) {
			tmpString = tmpString.replace(item2.numitem, `<point><num>${item2.numitem}</num>`);
		}
		tmpString = tmpString.replace(item2.bodyitem, `<content>${TagCreator.createSimpleTag('p', item2.bodyitem)}</content></point>`);
		if (isLast && isLastInList) {
			this.jsonElement.testo = this.jsonElement.testo.replace(item2.value, `${tmpString}</list></point></list></paragraph>`);
		} else if (isLast) {
			this.jsonElement.testo = this.jsonElement.testo.replace(item2.value, `${tmpString}</list></point>`);
		} else {
			this.jsonElement.testo = this.jsonElement.testo.replace(item2.value, tmpString);
		}
	}

	refineXmlString(xmlString) {
		let refinedString = this.refineModsBeforeParagraph(xmlString);
		refinedString = this.refineModsBeforePoint(refinedString);
		return refinedString;
	}

	refineModsBeforePoint(xmlString) {
		return xmlString.replace(new RegExp('[(][(]<point><num>', 'g'), '<point><num>((');
	}

	refineModsBeforeParagraph(xmlString) {
		return xmlString.replace(new RegExp('[(][(]<paragraph><num>', 'g'), '<paragraph><num>((');
	}
}

module.exports = ParagraphBuilder;
