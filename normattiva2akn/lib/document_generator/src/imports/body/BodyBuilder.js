/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import TagCreator from '../utils/TagCreator';
import GenericBuilder from './GenericBuilder';
import BodyRefiner from './BodyRefiner';
import ArticolatoUtils from './ArticolatoUtils';
import ReferenceBuilder from '../poldo_utils/ReferenceBuilder';
import xmldom from 'xmldom';
import _ from 'underscore';

class BodyBuilder {
	constructor(elemAtricolatoArray, transitionalContent) {
		this.elemAtricolatoArray = elemAtricolatoArray || [];
		this.transitionalContent = transitionalContent || '';
		this.DOMParser = new xmldom.DOMParser();
		this.XMLSerializer = new xmldom.XMLSerializer();
	}

	buildHierarchy(jsonHierarchy, document, container) {
		_.each(jsonHierarchy, (el) => {
			const newContainerName = this.getAknNodeName(el.elementoArticolato.nomeNir);
			let newContainerNode = document.createElement(newContainerName);
			let newContainerNumNode = document.createElement('num');
			let newContainerHeadingNode = document.createElement('heading');
			let newContainerContentNode;

			if (el.elementoArticolato.numNir && el.elementoArticolato.nomeNir !== 'articolo') {
					newContainerNumNode.appendChild(document.createTextNode(el.elementoArticolato.numNir));
					newContainerNode.appendChild(newContainerNumNode);
				}

			if (el.elementoArticolato.rubricaNir && el.elementoArticolato.nomeNir !== 'articolo') {
					newContainerHeadingNode.appendChild(document.createTextNode(el.elementoArticolato.rubricaNir));
					newContainerNode.appendChild(newContainerHeadingNode);
				}

				if (el.elementoArticolato.nomeNir === 'articolo') {
					//wrap all content to a div node to ensure root container
					newContainerContentNode = this.DOMParser.parseFromString('<rootWrapper>' + el.elementoArticolato.testo + '</rootWrapper>');
					let rootWrapper = newContainerContentNode.getElementsByTagName('rootWrapper')[0];
					while (rootWrapper.hasChildNodes()) {
						newContainerNode.appendChild(rootWrapper.firstChild);
					}
				}
				container.appendChild(newContainerNode);
				if (el.elementoArticolato.elementi.length > 0) {
					this.buildHierarchy(el.elementoArticolato.elementi, document, newContainerNode);
				}
		});
	}

	getAknNodeName(nodeName) {
		const mappingObj = {
			libro: 'book',
			parte: 'part',
			titolo: 'title',
			capo: 'chapter',
			sezione: 'section',
			articolo: 'article'
		};
		return mappingObj[nodeName];
	}


	createFullTag(bodyName) {
		return new Promise((resolve, reject) => {
			let bodyStringXMLContent = '';
			if (this.elemAtricolatoArray.length === 0 || !this.elemAtricolatoArray[0]) return resolve('');
			const gb = new GenericBuilder(this.elemAtricolatoArray);
			//ArticolatoUtils.getParsedContent(this.elemAtricolatoArray);
			let bodyCnt = new xmldom.DOMImplementation().createDocument();
			gb.createContent().then((values) => {
				this.buildHierarchy(values, bodyCnt, bodyCnt);
				bodyStringXMLContent = this.XMLSerializer.serializeToString(bodyCnt);
				//append transitional content
				bodyStringXMLContent += this.transitionalContent;
				//resolve refined content inside <body> tag
				if (bodyStringXMLContent.length > 0) {
					let xmlCnt = new BodyRefiner(bodyStringXMLContent).refineAll(bodyName);
					let refBuilder = new ReferenceBuilder(xmlCnt);
					refBuilder.createRef()
						.then((res) => {
							resolve(res);
						})
						.catch((refErr) => {
							reject(refErr);
						});
				} else {
					resolve({
						xmlContent: '',
						references: []
					});
				}
			}).catch((err) => {
				console.error(err);
				reject(err);
			});
		});
	}
}

module.exports = BodyBuilder;
