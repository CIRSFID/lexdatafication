/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import FormData from 'form-data';
import fakeResponce from '../../../test/testData/jsonParserResponce.json';
import ParagraphBuilder from './ParagraphBuilder';
import ParserUtils from '../utils/ParserUtils';

class ArticleBuilder {
	constructor(parserUrl, lang, context, docType) {
		this.parserUrl = parserUrl;
		this.lang = lang;
		this.context = context || null;
		this.docType = docType || null;
		this.parserUtils = new ParserUtils();
		this.quotePlaceHolders = [];
		this.notesPlaceHolders = [];
	}

	makeParserRequest(stringToParse) {
		return new Promise((resolve, reject) => {
			const formData = new FormData();
			formData.append('s', stringToParse);
			formData.append('l', this.lang);
			if (this.context) formData.append('context', this.context);
			if (this.docType) formData.append('doctype', this.docType);
			formData.submit(this.parserUrl, (err, res) => {
				if (err) return reject(err);
				let responseData = '';
				res.on('data', (chunk) => {
					responseData += chunk;
				});
				res.on('end', () => {
					resolve(JSON.parse(responseData));
				});
				res.on('error', () => {
					console.log(err);
					reject(JSON.parse(responseData));
				});
			});
		});
	}

	getParsedObject(textContent, resField) {
		return new Promise((resolve, reject) => {
			//return resolve(fakeResponce.response[resField]);
			this.makeParserRequest(textContent)
				.then((res) => {
					resolve(res.response[resField]);
				}).catch((err) => {
					console.log(err);
					reject(err);
				});
		});
	}

	createContent(jsonElement, nodeName) {
		this.quotePlaceHolders = [];
		this.notesPlaceHolders = [];
		//try to find tag inside text ex: <a href="..">
		const allTagRefsOffsets = [];
		const tagRe = /\<(.*?)\>/g;
		let match = tagRe.exec(jsonElement.testo);
		while (match != null) {
			allTagRefsOffsets.push({
				start: match.index,
				end: match.index + match[0].length
			});
			match = tagRe.exec(jsonElement.testo);
		}
		//here remove and replace quoted text content
		this.parserUtils.getQuotedTextMatches(jsonElement.testo || '').forEach((el, id) => {
			let toExclude = false;
			//check if quotes are not inside an html tag
			allTagRefsOffsets.forEach((offset) => {
				if (el.offset.start >= offset.start && el.offset.end <= offset.end) {
					toExclude = true;
				}
			});
			if (!toExclude) {
				this.quotePlaceHolders.push({
					substitution: `|q-x||${id}||q-x|`,
					originalText: el.fullString,
					match: el
				});
				//replace text in article json element
				jsonElement.testo = jsonElement.testo.replace(el.fullString, `|q-x||${id}||q-x|`);
			}
		});
		//here remove and replace notes content
		this.parserUtils.getNotesMatches(jsonElement.testo || '').forEach((el, id) => {
			this.notesPlaceHolders.push({
				substitution: `|n-x||${id}||n-x|`,
				originalText: el.fullString,
				match: el
			});
			//replace text in article json element
			jsonElement.testo = jsonElement.testo.replace(el.fullString, `|n-x||${id}||n-x|`);
		});

		return new Promise((resolve, reject) => {
			this.getParsedObject(jsonElement.testo || '', nodeName)
				.then((res) => {
					resolve(this.createNode(jsonElement, res));
				}).catch((err) => {
					console.log(err);
					reject(err);
				});
		});
	}

	createNode(jsonElement, resObject) {
		const articleElement = new ParagraphBuilder(jsonElement, resObject).createContent();
		//apply notes substitutions
		this.notesPlaceHolders.forEach((el) => {
			articleElement.testo = articleElement.testo.replace(
				el.substitution,
				`<authorialNote marker="${el.match.noteNumber}" placement="bottom"><p>${el.originalText}</p></authorialNote>`
			);
		});
		//apply quoted text subsitutions
		this.quotePlaceHolders.forEach((el) => {
			articleElement.testo = articleElement.testo.replace(
				el.substitution,
				`<mod><quotedText>${el.originalText}</quotedText></mod>`
			);
		});

		return articleElement;
	}
}

module.exports = ArticleBuilder;
