/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import ParagraphUtils from './ParagraphUtils';
import traverse from 'traverse';


const getParsedContent = (articlesArray) => {
	let textTest = "Art. 2 \n \n                       Ammortizzatori sociali \n \n  (43. Ai contributi di cui  ai  commi  da  25  a  39  si  applica  la disposizione di cui all'articolo 26, comma 1, lettera e), della legge 9 marzo 1989, n. 88. \n  45. La durata massima legale,  in  relazione  ai  nuovi  eventi  di disoccupazione verificatisi a decorrere dal 1° gennaio 2013 e fino al 31 dicembre 2015, e' disciplinata nei seguenti termini: \n    a) per le prestazioni relative agli eventi  intercorsi  nell'anno 2013: otto mesi per  i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni e dodici mesi per i soggetti con eta' anagrafica  pari o superiore a cinquanta anni; \n 1.1) test123 \n  ((b) per le prestazioni relative agli eventi  intercorsi  nell'anno 2014: otto mesi per  i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni, dodici mesi per i soggetti con eta' anagrafica pari o superiore a  cinquanta  anni  e  inferiore  a  cinquantacinque  anni, quattordici mesi per i soggetti con eta' anagrafica pari o  superiore a cinquantacinque anni, nei limiti delle settimane  di  contribuzione negli ultimi due anni; \n    c) per le prestazioni relative agli eventi  intercorsi  nell'anno 2015: dieci mesi per i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni, dodici mesi per i soggetti con eta' anagrafica pari o superiore a cinquanta anni e inferiore a cinquantacinque anni, sedici mesi  per  i  soggetti  con  eta'  anagrafica  pari  o  superiore   a cinquantacinque anni, nei limiti  delle  settimane  di  contribuzione negli ultimi due anni. \n  46. Per i lavoratori collocati in  mobilita'  a  decorrere  dal  1° gennaio 2013 e fino al 31 dicembre  2016  ai  sensi  dell'articolo  7 della legge 23 luglio 1991, n. 223, e  successive  modificazioni,  il periodo  massimo  di  diritto  della  relativa  indennita'   di   cui all'articolo 7, commi 1 e 2, della legge 23 luglio 1991, n.  223,  e' ridefinito nei seguenti termini: \n    a) lavoratori collocati in mobilita' nel periodo dal  1°  gennaio 2013 al 31 dicembre 2014: \n      1) lavoratori di cui all'articolo  7,  comma  1:  dodici  mesi, elevato a ventiquattro per i lavoratori che hanno compiuto i quaranta anni e a trentasei per i lavoratori che hanno  compiuto  i  cinquanta anni; \n      2) lavoratori di cui  all'articolo  7,  comma  2:  ventiquattro mesi, elevato a trentasei per  i  lavoratori  che  hanno  compiuto  i quaranta anni e a quarantotto per i lavoratori che hanno  compiuto  i cinquanta anni; \n    b) LETTERA ABROGATA DAL D.L. 22 GIUGNO 2012,  N.  83,  CONVERTITO CON MODIFICAZIONI DALLA L. 7 AGOSTO 2012, N. 134;";
	ParagraphUtils.createParsedContent(textTest);
	traverse(articlesArray).forEach((el) => {
		if (el && el.nomeNir && el.nomeNir === 'articolo') {
			//updating article text
			//ParagraphUtils.createParsedContent(el.testo);
		}
	});
};


module.exports.getParsedContent = getParsedContent;
