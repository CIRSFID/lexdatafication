/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import parser from 'lib/parsers';
import ParserUtils from '../utils/ParserUtils';

let createContent = (startText, match, prevMatch) => {
	if (!match) return '';
	let newText = '';
	let outerTxt = '';
	//handling text between matches if present
	if (prevMatch) {
		outerTxt = startText.substring(prevMatch.content.offset.end, match.content.offset.start);
	}
	newText += outerTxt;
	//paragraph handling
	if (match.name === 'paragraph') {
		newText += '<paragraph>';
		if (match.num) {
			newText += '<num>';
			newText += startText.substring(match.num.offset.start, match.num.offset.end);
			newText += '</num>';
		}
		if (match.bodyItem.value && match.bodyItem.value.trim() && match.contains.length > 0) {
			newText += '<list>';
			newText += '<intro>';
			newText += '<p>';
			newText += startText.substring(match.bodyItem.offset.start, match.bodyItem.offset.end);
			newText += '</p>';
			newText += '</intro>';
			for (let z = 0; z < match.contains.length; z++) {
				if (z > 0) {
					newText += createContent(startText, match.contains[z], match.contains[z - 1]);
				} else {
					newText += createContent(startText, match.contains[z]);
				}
			}
			newText += '</list>';
			newText += '</paragraph>';

		} else if (match.bodyItem.value && match.bodyItem.value.trim() && match.contains.length <= 0) {
			newText += '<content>';
			newText += '<p>';
			newText += startText.substring(match.bodyItem.offset.start, match.bodyItem.offset.end);
			newText += '</p>';
			newText += '</content>';
			newText += '</paragraph>';
		}
	//items handling
	} else if (match.name === 'item1' || match.name === 'item2') {
		newText += '<point>';
		if (match.num) {
			newText += '<num>';
			newText += startText.substring(match.num.offset.start, match.num.offset.end);
			newText += '</num>';
		}
		if (match.bodyItem.value && match.bodyItem.value.trim() && match.contains.length > 0) {
			newText += '<list>';
			newText += '<intro>';
			newText += '<p>';
			newText += startText.substring(match.bodyItem.offset.start, match.bodyItem.offset.end);
			newText += '</p>';
			newText += '</intro>';
			for (let z = 0; z < match.contains.length; z++) {
				if (z > 0) {
					newText += createContent(startText, match.contains[z], match.contains[z - 1]);
				} else {
					newText += createContent(startText, match.contains[z]);
				}
			}
			newText += '</list>';
			newText += '</point>';
		} else if (match.bodyItem.value && match.bodyItem.value.trim() && match.contains.length <= 0) {
			newText += '<content>';
			newText += '<p>';
			newText += startText.substring(match.bodyItem.offset.start, match.bodyItem.offset.end);
			newText += '</p>';
			newText += '</content>';
			newText += '</point>';
		}
	}
	return newText;
};

//try to find tag inside text ex: <a href=".."> to avoid conflicts with quote parser
const getTagQuotesMatches = (text) => {
	let offsetsMathces = [];
	const tagRe = /\<(.*?)\>/g;
	let match = tagRe.exec(text);
	while (match != null) {
		offsetsMathces.push({
			start: match.index,
			end: match.index + match[0].length
		});
		match = tagRe.exec(text);
	}
	return offsetsMathces;
};

const setQuotesPlaceholders = (text) => {
	let placeholders = [];
	let parserUtils = new ParserUtils();
	let newText = text;
	//try to find tag inside text ex: <a href=".."> to avoid conflicts with quote parser
	let allTagRefsOffsets = getTagQuotesMatches(text);
	parserUtils.getQuotedTextMatches(text || '').forEach((el, id) => {
		let toExclude = false;
		//check if quotes are not inside an html tag
		allTagRefsOffsets.forEach((offset) => {
			if (el.offset.start >= offset.start && el.offset.end <= offset.end) {
				toExclude = true;
			}
		});
		if (!toExclude) {
			placeholders.push({
				substitution: `|q-x||${id}||q-x|`,
				originalText: el.fullString,
				match: el
			});
			//replace text with substitutions
			newText = newText.replace(el.fullString, `|q-x||${id}||q-x|`);
		}
	});
	return {
		placeholders,
		newText
	};
};

const setNotesPlaceholders = (text) => {
	let placeholders = [];
	let parserUtils = new ParserUtils();
	let newText = text;
	parserUtils.getNotesMatches(text || '').forEach((el, id) => {
		placeholders.push({
			substitution: `|n-x||${id}||n-x|`,
			originalText: el.fullString,
			match: el
		});
		//replace text in article json element
		newText = newText.replace(el.fullString, `|n-x||${id}||n-x|`);
	});
	return {
		placeholders,
		newText
	};
};

const createParsedContent = (text) => {
	// Parser from paragraph context
	const pp = new parser.PartitionsParser('it_IT');
	let quotesData, notesData, bpText, apText;
	let aknText = '';
	/*
		Apply quotes and notes substitution to avoid conflicts due to
		the quoted structure and notes parsing inside text
	*/
	// Text substitutions before parsing
	bpText = text;
	quotesData = setQuotesPlaceholders(bpText);
	bpText = quotesData.newText;
	notesData = setNotesPlaceholders(bpText);
	bpText = notesData.newText;
	/*
		Parse text with paragraph context
	*/
	const ms = pp.getMatches(bpText, 'paragraph');
	for (let k = 0; k < ms.length; k++) {
		if (k > 0) {
			aknText += createContent(bpText, ms[k], ms[k - 1]);
		} else {
			// Attach all text before the first parser match
			aknText += bpText.substring(0, ms[k].content.offset.start);
			aknText += createContent(bpText, ms[k]);
		}
	}
	/*
		Apply quotes and notes substitution to restore
		the original text, mark quoted structures and notes
	*/
	// Text substitutions after parsing
	apText = aknText;
	// Quoted structure substitutions
	quotesData.placeholders.forEach((el) => {
		apText = apText.replace(
			el.substitution,
			`<mod><quotedText>${el.originalText}</quotedText></mod>`
		);
	});
	// Notes substitutions
	notesData.placeholders.forEach((el) => {
		apText = apText.replace(
			el.substitution,
			`<authorialNote marker="${el.match.noteNumber}" placement="bottom"><p>${el.originalText}</p></authorialNote>`
		);
	});

	console.log(">>", apText);

};


module.exports.createParsedContent = createParsedContent;
