import xmldom from 'xmldom';
import xpath from 'xpath';

import config from '../../configs/configs.json';

const select = xpath.useNamespaces({ akn: config.aknTags.aknNamespace });

class BodyRefiner {
	constructor(xmlString) {
		this.xmlString = xmlString || '';
		this.xmlObj = null;
		try {
			this.xmlObj = new xmldom.DOMParser().parseFromString(this.xmlString);
		} catch (error) {
			console.log(error);
			throw error;
		}
	}

	refineAll(bodyName = 'body') {
		if (this.xmlObj) {
			//this.refineParagraphs(this.xmlObj);
			//this.refineListPoints(this.xmlObj);
			this.wrapTextWithContent(this.xmlObj);
			let bodyContent = new xmldom.XMLSerializer().serializeToString(this.xmlObj);
			return `<${bodyName}>${bodyContent}</${bodyName}>`;
		}
	}

	wrapTextWithContent(doc) {
		var getDirectTextOfNode = function(node) {
			var text = "";
			for(var i = 0; i < node.childNodes.length; i++) {
				if(node.childNodes[i].nodeType == node.TEXT_NODE) {
					text+= node.childNodes[i].nodeValue;
				}
			}
			return text;
		}
		const articlesWithText = select('//article[not(./content) and not(./paragraph)]', doc);
		articlesWithText.forEach((node) => {
			var text = getDirectTextOfNode(node).trim();
			if (!text) return;
			const lastHeading = select('./*[name() = "num" or name() = "heading" or name() = "subheading"]',
								node).pop();
			const contentNode = node.ownerDocument.createElement('content');
			const pNode = node.ownerDocument.createElement('p');
			contentNode.appendChild(pNode);
			if (lastHeading) {
				while(lastHeading.nextSibling) {
					pNode.appendChild(lastHeading.nextSibling);
				}
				node.appendChild(contentNode);
			} else {
				while(node.firstChild) {
					pNode.appendChild(node.firstChild);
				}
				node.appendChild(contentNode);
			}
			// console.log('->>', node.textContent);
		});
	}


	refineParagraphs(xmlElement) {
		const paragraphNodes = xmlElement.getElementsByTagName('article');
		for (let j = paragraphNodes.length - 1; j >= 0; j--) {
			const node = paragraphNodes[j];
			if (node) {
				if (node.childNodes) {
					for (let z = node.childNodes.length - 1; z >= 0; z--) {
						if (node.childNodes[z] && node.childNodes[z].nodeName === '#text') {
							//remove nodes whit whitespace text only
							console.log(node.childNodes[z].data)
							/*
							if (!node.childNodes[z].data.replace(/\s/g, '').length) {
								node.childNodes[z].parentNode.removeChild(node.childNodes[z]);
							} else {
								//wrap in a p tag all the outsider text
								const paraTag = xmlElement.createElement('paragraph');
								const cntTag = xmlElement.createElement('content');
								const pTag = xmlElement.createElement('p');
								const textNode = xmlElement.createTextNode(node.childNodes[z].data);
								pTag.appendChild(textNode);
								cntTag.appendChild(pTag);
								paraTag.appendChild(cntTag);
								xmlElement.replaceChild(paraTag, node.childNodes[z]);
							}
							*/
						}
					}
				}
			}
		}
	}

	refineListPoints(xmlElement) {
		const listNodes = xmlElement.getElementsByTagName('list');
		for (let j = listNodes.length - 1; j >= 0; j--) {
			const node = listNodes[j];
			if (node) {
				if (node.childNodes) {
					for (let z = node.childNodes.length - 1; z >= 0; z--) {
						if (node.childNodes[z].nodeName && node.childNodes[z].nodeName === '#text') {
							//remove nodes whit whitespace text only
							if (!node.childNodes[z].data.replace(/\s/g, '').length) {
								node.childNodes[z].parentNode.removeChild(node.childNodes[z]);
							} else {
								//wrap in a p tag all the outsider text
								const pntTag = xmlElement.createElement('point');
								const cntTag = xmlElement.createElement('content');
								const pTag = xmlElement.createElement('p');
								const textNode = xmlElement.createTextNode(node.childNodes[z].data);
								pTag.appendChild(textNode);
								cntTag.appendChild(pTag);
								pntTag.appendChild(cntTag);
								xmlElement.replaceChild(pntTag, node.childNodes[z]);
							}
						}
					}
				}
			}
		}
	}
}

module.exports = BodyRefiner;
