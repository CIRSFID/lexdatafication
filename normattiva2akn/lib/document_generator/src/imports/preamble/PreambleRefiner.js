import xmldom from 'xmldom';

class PreambleRefiner {
	constructor(xmlString) {
		this.xmlString = xmlString || '';
		this.xmlObj = null;
		try {
			this.xmlObj = new xmldom.DOMParser().parseFromString(this.xmlString);
		} catch (error) {
			console.error(error);
			throw error;
		}
	}

	refineCitationsRecitals() {
		if (this.xmlObj) {
			let refinedXMLString;
			this.wrapTextNodesInPtags(this.xmlObj);
			refinedXMLString = new xmldom.XMLSerializer().serializeToString(this.xmlObj);

			if (refinedXMLString.length > 0){
				if ((refinedXMLString.indexOf('<preamble>') >= 0)
					&& (refinedXMLString.indexOf('</preamble>') > 0)) {
						return refinedXMLString;
				}
				return `<preamble>${refinedXMLString}</preamble>`;
			} else {
				return '';
			}

		}
	}

	wrapTextNodesInPtags(xmlElement) {
		const preNode = xmlElement.getElementsByTagName('preamble');
		for (let k = 0; k < preNode[0].childNodes.length; k++) {
			if (preNode[0].childNodes[k].nodeName === '#text') {
				//remove nodes whit whitespace text only
				if (!preNode[0].childNodes[k].data.replace(/\s/g, '').length) {
					preNode[0].childNodes[k].parentNode.removeChild(preNode[0].childNodes[k]);
				} else {
				//wrap in a p tag all the outsider text
					const pTag = xmlElement.createElement('p');
					const textNode = xmlElement.createTextNode(preNode[0].childNodes[k].data);
					pTag.appendChild(textNode);
					xmlElement.replaceChild(pTag, preNode[0].childNodes[k]);
				}
			}
		}
	}
}

module.exports = PreambleRefiner;
