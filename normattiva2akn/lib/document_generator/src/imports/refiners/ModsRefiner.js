/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import xpath from 'xpath';
import xmldom from 'xmldom';
import arrayDedupe from 'array-dedupe';
import ParserUtils from '../utils/ParserUtils.js';
import config from '../../configs/configs.json';
import sorty from 'sorty';

const modIdPrefix = 'pmod_';

class ModsRefiner {
	constructor(xmlStringDoc, jsonData, locale) {
		if (!xmlStringDoc) {
			throw new Error('Not defined xml document on refiner constructor');
		}
		try {
			this.xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
		} catch (err) {
			console.error(err);
			throw err;
		}
		this.selectObj = xpath.useNamespaces({ akn: config.aknTags.aknNamespace });
		const selectedLocale = locale || 'it_IT';
		this.jsonData = jsonData;
		this.lavoriPreparatori = jsonData.lavoriPreparatori || [];
		this.inlineMods = [];
		this.totModsCounter = 0;
		this.modificationsArray = [];
		this.parserUtils = new ParserUtils(selectedLocale);
	}

	evalModificationsAndMetadata() {
		const entries = this.selectObj('//akn:num|//akn:heading', this.xmlDoc);
		this.evalModifiactionOutsideP(entries);
		this.evalIDs();
		this.generateMetadata();
		return new xmldom.XMLSerializer().serializeToString(this.xmlDoc);
	}

	evalIDs() {
		//TO-DO: one query only, one function
		const inlineMods = ['ins', 'del'];
		const elems = this.selectObj('//akn:ins|//akn:del|//akn:*[@status=\'editorial\']', this.xmlDoc);
		elems.forEach((el) => {
			this.totModsCounter++;
			let refId = '';
			let eventRefId = `eventRef_${this.totModsCounter}`;
			//update mod nodes
			//el.setAttribute('period', `#eventRef_${this.totModsCounter}`);
			//create modification array
			if (inlineMods.indexOf(el.nodeName) !== -1) {
				refId = `#${eventRefId}`;
			} else {
				refId = `#${el.getAttribute('eId')}`;
			}
			//reference are parsed only if inline
			this.modificationsArray.push({
				parentEId: `#${this.getParentNodeEId(el)}`,
				refId,
				eventRefId,
				textContent: el.textContent,
				type: el.nodeName,
				refParserObj: this.parserUtils.getReferencesByIndex(el.textContent, 0),
				nodeObj: el
			});
		});
	}

	evalModifiactionOutsideP(elems) {
		const breakAnchestorNames = ['article', 'section', 'chapter', 'title', 'part', 'book'];
		elems.forEach((el) => {
			if (el.textContent.indexOf('((') !== -1 && el.textContent.indexOf('))') < 0) {
				let parentNode = el.parentNode;
				if (parentNode && parentNode.nodeName) {
					if (parentNode.nodeName === 'point') {
						this.handleEditorial(parentNode);
					} else if (parentNode.nodeName === 'paragraph') {
						this.handleEditorial(parentNode);
					} else {
						//fallback to article
						//if all anchestors has nodeName is a normal chain of anchestors nodes
						let normalChain = true;
						while (breakAnchestorNames.indexOf(parentNode.nodeName) === -1) {
							if (parentNode && parentNode.nodeName) {
								parentNode = parentNode.parentNode;
							} else {
								normalChain = false;
								break;
							}
						}
						if (normalChain) {
							parentNode.setAttribute('status', 'editorial');
						}
					}
				} else {
					el.setAttribute('status', 'editorial');
				}
			}
		});
	}

	getParentNodeEId(node) {
		let currentNode = node;
		while (!currentNode.getAttribute('eId')) {
			currentNode = currentNode.parentNode;
		}
		return currentNode.getAttribute('eId');
	}

	handleEditorial(node) {
		const baseNode = node;
		let entrtyId = null;
		if (node.textContent.endsWith('))') || node.textContent.endsWith('));')) {
			node.setAttribute('status', 'editorial');
		} else {
			node = node.parentNode;
			if (node.childNodes.length > 0) {
				entrtyId = this.getEntryId(baseNode, node.childNodes);
				for (let z = 0; z < node.childNodes.length; z++) {
					if (z >= entrtyId && node.childNodes[z].nodeName === baseNode.nodeName) {
						node.childNodes[z].setAttribute('status', 'editorial');
						if (node.childNodes[z].textContent.endsWith('))') || node.childNodes[z].textContent.endsWith('));')) {
							node.childNodes[z].setAttribute('status', 'editorial');
							break;
						}
					}
				}
			}
		}
	}

	getEntryId (node, nodesList) {
		for (let z = 0; z < nodesList.length; z++) {
			if (node === nodesList[z]) {
				return z;
			}
		}
		return -1;
	}

	generateMetadata() {
		const metaNode = this.selectObj('//akn:meta', this.xmlDoc)[0];
		const anNode = this.selectObj('//akn:meta/akn:analysis', this.xmlDoc)[0];
		//get dom nodes
		const refMetaNode = metaNode.getElementsByTagName('references')[0];
		const fRBRWorkNode = metaNode.getElementsByTagName('identification')[0].getElementsByTagName('FRBRWork')[0];
		const fRBRWorkNode_FRBRthis_val = fRBRWorkNode.getElementsByTagName('FRBRthis')[0].getAttribute("value");
		//append <original> tag to passiveRef node
		const originalRefNode = this.xmlDoc.createElement('original');
		originalRefNode.setAttribute('eId', 'ro1');
		originalRefNode.setAttribute('href', fRBRWorkNode_FRBRthis_val);
		originalRefNode.setAttribute('showAs', '');
		refMetaNode.appendChild(originalRefNode);
		//generate nodes
		// const lifeCycleNode = this.xmlDoc.createElement('lifecycle');
		// lifeCycleNode.setAttribute('source', '#palmirani');
		const pmNode = this.selectObj('//akn:meta/akn:analysis/akn:passiveModifications', this.xmlDoc)[0];
		//modifications whit refs inside
		let modRefsArray = [];
		if (this.modificationsArray.length <= 0) return;
		this.modificationsArray.forEach((el, modCounter) => {
			const newNode = this.xmlDoc.createElement('new');
			const pNode = this.xmlDoc.createElement('cirsfid:text');
			const textualModNode = this.xmlDoc.createElement('textualMod');
			const sourceNode = this.xmlDoc.createElement('source');
			const destinationNode = this.xmlDoc.createElement('destination');
			//passiveModifications
			let txtModType = 'insertion';
			//TO-DO: hanlde type of insertion/substitution...
			if (el.type === 'del') { txtModType = 'repeal'; }
			textualModNode.setAttribute('type', txtModType);
			textualModNode.setAttribute('eId', modIdPrefix + modCounter);
			//not here
			//textualModNode.setAttribute('period', el.eventRefId);
			destinationNode.setAttribute('href', el.parentEId);
			sourceNode.setAttribute('href', '#');
			textualModNode.appendChild(sourceNode);
			textualModNode.appendChild(destinationNode);
			if (el.textContent) {
				pNode.appendChild(this.xmlDoc.createTextNode(el.textContent));
				newNode.appendChild(pNode);
				textualModNode.appendChild(newNode);
			}
			pmNode.appendChild(textualModNode);
			//create modifications whit refs inside:
			if (el.refParserObj && el.refParserObj.length > 0) {
				modRefsArray.push({
					href: el.refParserObj[0].href,
					element: el
				});
			}
		});

		//append original reference to refernces tag
		// const originalEventRefNode = this.xmlDoc.createElement('eventRef');
		// originalEventRefNode.setAttribute('eId', 'eventRef_0');
		// originalEventRefNode.setAttribute('date', '9999-09-09');
		// originalEventRefNode.setAttribute('source', '#ro1');
		// lifeCycleNode.appendChild(originalEventRefNode);
		//reference and lifecycle handling
		modRefsArray = arrayDedupe(modRefsArray, ['href']);
        modRefsArray.forEach((el) => {
            el.isoTimestamp = el.element.refParserObj[0].isoTimestamp;
        })
        sorty( [{name: 'isoTimestamp', dir: 'desc', type: 'number'}], modRefsArray);
        //creates all entries for lifecycle and references
		modRefsArray.forEach((el, counter) => {
			const passiveRefNode = this.xmlDoc.createElement('passiveRef');
			// const eventRefNode = this.xmlDoc.createElement('eventRef');
			// //eventRefs
			// eventRefNode.setAttribute('eId', `eventRef_${counter + 1}`);
			// eventRefNode.setAttribute('date', '9999-09-09');
			// eventRefNode.setAttribute('source', `#rp${counter + 1}`);
			//passiveRefs
			passiveRefNode.setAttribute('eId', `rp${counter + 1}`);
			passiveRefNode.setAttribute('href', el.element.refParserObj[0].href);
			passiveRefNode.setAttribute('showAs', el.element.refParserObj[0].allText);
			//create nodes
			// lifeCycleNode.appendChild(eventRefNode);
			refMetaNode.appendChild(passiveRefNode);
		});

		metaNode.insertBefore(anNode, refMetaNode);
	}
}

module.exports = ModsRefiner;
