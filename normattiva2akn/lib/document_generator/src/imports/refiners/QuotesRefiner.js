/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import xpath from 'xpath';
import xmldom from 'xmldom';
import parser from 'lib/parsers';
import config from '../../configs/configs.json';

class QuotesRefiner {
	constructor(xmlStringDoc, locale) {
		if (!xmlStringDoc) {
			throw new Error('Not defined xml document on refiner constructor');
		}
		try {
			this.xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
		} catch (err) {
			console.error(err);
			throw err;
		}
		const selectedLocale = locale || 'it_IT';
		this.selectObj = xpath.useNamespaces({ akn: config.aknTags.aknNamespace });
		this.quoteParser = new parser.QuoteParser(selectedLocale);
	}

	evalQuotedText() {
		const elems = this.selectObj('//akn:article', this.xmlDoc);
		elems.forEach((el) => {
			let articleString = new xmldom.XMLSerializer().serializeToString(el);
			const quotesMatches = this.quoteParser.getMatches(articleString);
			quotesMatches.forEach((quoteMatch) => {
				console.log(quoteMatch);
			});
			try {
				const newNode = new xmldom.DOMParser().parseFromString(articleString, 'text/xml');
				el.parentNode.replaceChild(newNode, el);
			} catch (err) {
				console.log(err);
			}
		});

	}

	evalIDs() {
		//TO-DO: one query only, one function
		const elems = this.selectObj('//akn:authorialNote', this.xmlDoc);
		elems.forEach((el, id) => {
			el.setAttribute('eId', `authorialNote_${id}`);
		});
	}

	evalQuotesAndMetadata() {
		this.evalQuotedText();
		return new xmldom.XMLSerializer().serializeToString(this.xmlDoc);
	}
}

module.exports = QuotesRefiner;
