/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import parser from 'lib/parsers';

class ParserUtils {
	constructor(locale) {
		const selectedLocale = locale || 'it_IT';
		this.dateParser = new parser.DateParser(selectedLocale);
		this.referenceParser = new parser.ReferenceParser(selectedLocale);
		this.quoteParser = new parser.QuoteParser(selectedLocale);
		this.notesParser = new parser.NotesParser(selectedLocale);
	}

	getReferencesByIndex(stringToParse, matchIndex) {
		//docType docNum docDate
		const refindedMatches = [];
		this.referenceParser.getWiderMatches(stringToParse).forEach((match) => {
			const normalizeRef = (refStr) => {
				refStr = refStr.toLowerCase();
				refStr = refStr.replace(/[^\w]/g, '-');
				return refStr;
			};
			const txtMatch = stringToParse.substring(match.offset.start, match.offset.end);
			let hrefStr = '/akn/it/act';
            let isoTimestamp = null;
			const refFinalStr = '';
			//create reference tag
			if (match.docType) {
				hrefStr += `/${normalizeRef(match.docType)}`;
			}
			if (match.isoDate) {
				hrefStr += `/${match.isoDate}`;
                isoTimestamp = parseInt(match.isoDate.replace(/\-/g,''));
			} else {
				hrefStr += '/xxxx-xx-xx';
                isoTimestamp = -1;
			}
			if (match.docNum) {
				//TO-DO: Fix reference parser
				let refindedDocNum = normalizeRef(match.docNum);
				refindedDocNum = refindedDocNum.replace('n..', '');
				hrefStr += `/${refindedDocNum}`;
			}
			if (match.authority) {
				hrefStr += `/${normalizeRef(match.authority)}`;
			}
			hrefStr += '/!main';
			/* TO-DO: parse
			try {
				hrefStr = AKNUri.parse(hrefStr);
			} catch (err) {
				console.log(err);
				hrefStr = '';
			}
			*/
			refindedMatches.push({
				docNum: match.docNum,
				docDate: match.date,
				isoDate: match.isoDate,
                isoTimestamp,
				authority: match.authority,
				number: match.number,
				href: hrefStr,
				allText: stringToParse.substring(match.offset.start, match.offset.end)
			});
		});
		if (matchIndex) {
			return refindedMatches[matchIndex];
		}
		return refindedMatches;
	}

	getQuotedTextMatches(stringToParse) {
		return this.quoteParser.getMatches(stringToParse);
	}

	getNotesMatches(stringToParse) {
		return this.notesParser.getMatches(stringToParse);
	}
}

module.exports = ParserUtils;
