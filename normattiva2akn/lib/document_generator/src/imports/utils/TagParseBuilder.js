/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import parser from 'lib/parsers';
import AKNUri from './AknUri';

class TagParseBuilder {
	constructor(locale) {
		const selectedLocale = locale || 'it_IT';
		this.dateParser = new parser.DateParser(selectedLocale);
		this.referenceParser = new parser.ReferenceParser(selectedLocale);
		this.notesParser = new parser.NotesParser(selectedLocale);
		this.locationParser = new parser.LocationParser(selectedLocale);
	}

	createDatesTagInString(stringToParse) {
		let stringWhitTags = stringToParse;
		this.dateParser.getMatches(stringToParse).forEach((match) => {
			const txtMatch = stringToParse.substring(match.offset.start, match.offset.end);
			const tagString = `<date date="${match.isoDate}">${txtMatch}</date>`;
			stringWhitTags = stringToParse.replace(txtMatch, tagString);
		});
		return stringWhitTags;
	}

	createLocationTagInString(stringToParse) {
		let stringWhitTags = stringToParse;
		this.locationParser.getMatches(stringToParse).forEach((match) => {
			const txtMatch = stringToParse.substring(match.offset.start, match.offset.end);
			const tagString = `<location refersTo="#${match.fullString.toLowerCase()}">${txtMatch}</location>`;
			stringWhitTags = stringToParse.replace(txtMatch, tagString);
		});
		return stringWhitTags;
	}

	createModsTagInString(stringToParse) {
		stringToParse = stringToParse || '';
		return stringToParse.replace(/\(\((.*?)\)\)/g, (match) => {
			const content = match.replace('((', '').replace('))', '');
			//excludes notes ex ((28))
			if (!Number.isInteger(+content)) {
				if (match.toLowerCase().indexOf('abrogat') !== -1 ||
					match.toLowerCase().indexOf('soppress') !== -1) {
					return `<del>${match}</del>`;
				}
				return `<ins>${match}</ins>`;
			}
			return match;
		});
	}

	createAutorialNoteTagInString(stringToParse) {
		let strOut = stringToParse || '';
		const notesMatches = this.notesParser.getMatches(strOut);
		notesMatches.forEach((el) => {
			console.log(el)
			if (stringToParse.indexOf(`(${el.noteNumber})`) !== -1) {
				//remove note content from string (article string)
				strOut = stringToParse.replace(el.fullString, '');
				strOut = stringToParse.replace(
					`(${el.noteNumber})`,
					`<authorialNote marker="((${el.noteNumber}))" palcement="bottom">
					<p>${el.fullString}</p></authorialNote>`
				);
			}
		});
		return strOut;
	}

	createReferenceTagInString(stringToParse) {
		let stringWhitTags = stringToParse;
		this.referenceParser.getWiderMatches(stringToParse).forEach((match) => {
			const normalizeRef = (refStr) => {
				refStr = refStr.toLowerCase();
				refStr = refStr.replace(/[^\w]/g, '.');
				return refStr;
			};
			const txtMatch = stringToParse.substring(match.offset.start, match.offset.end);
			let hrefStr = '/akn/it/act';
			let refFinalStr = '';
			//create reference tag
			if (match.docType) {
				hrefStr += `/${normalizeRef(match.docType)}`;
			}
			if (match.isoDate) {
				hrefStr += `/${match.isoDate}`;
			} else {
				hrefStr += '/xxxx-xx-xx';
			}
			if (match.docNum) {
				//TO-DO: Fix reference parser
				let refindedDocNum = normalizeRef(match.docNum);
				refindedDocNum = refindedDocNum.replace('n..', '');
				hrefStr += `/${refindedDocNum}`;
			}
			if (match.authority) {
				hrefStr += `/${normalizeRef(match.authority)}`;
			}
			hrefStr += '/!main';
			/* TO-DO: parse
			try {
				hrefStr = AKNUri.parse(hrefStr);
			} catch (err) {
				console.log(err);
				hrefStr = '';
			}
			*/
			refFinalStr = `<ref href="${hrefStr}">${txtMatch}</ref>`;
			stringWhitTags = stringToParse.replace(txtMatch, refFinalStr);
		});
		return stringWhitTags;
	}
}

module.exports = TagParseBuilder;
