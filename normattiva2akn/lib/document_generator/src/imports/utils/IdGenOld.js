/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
// Schema informations for id generation
const abbreviations = {
	article: 'art',
	paragraph: 'para',
	section: 'sec',
	quotedStructure: 'qstr',
	quotedText: 'qtxt',
	chapter: 'chp',
	recitals: 'rcs',
	citations: 'cts'
};
const contexts = ['book', 'part', 'title', 'chapter', 'section', 'article', 'paragraph', 'num', 'list', 'point', 'quotedStructure', 'transitional', 'citations'];
const excludeNodes = ['meta', 'component'];
const hidden_eId = ['akomaNtoso', 'bill', 'act', 'doc', 'p', 'ins', 'del'];
const numPointIds = ['part', 'title', 'chapter', 'section', 'article', 'paragraph', 'point'];
// Given the xmlbuilder object, add eId attributes to all
// appropriate elements
const getNumberFromString = (str) => {
	const numReg = new RegExp(/(?:[\u00C0-\u1FFF\u2C00-\uD7FF\w]+\W*\s+)?((?:\w+[\.)]?)+(?:[- ]+\w{3,10}){0,1})/);
	const romanReg = new RegExp(/M{0,4}(?:CM|CD|D?C{0,3})(?:XC|XL|L?X{0,3})(?:IX|IV|V?I{0,3})/);

	const normalize = (str) => {
		let normalized = '',
			numMatch = str.match(numReg);
		if (numMatch && numMatch.length) {
			normalized = numMatch[1];
		}

		const romanNumer = normalized.match(romanReg);
		if (!romanNumer || !romanNumer[0].length) {
			normalized = normalized.toLowerCase();
		}

		normalized = normalized.replace(/[\.)\s]/g, '');
		return normalized;
	};

	return normalize(str);
};
const generateIds = (xml, path) => {
	//console.log('generateIds', path);
	path = path || '';
	const stack = [];
	const counter = {};
	for (let i = xml.childNodes.length - 1; i >= 0; i--) { stack.push(xml.childNodes[i]); }

	let node, processed_nodes = [];
	while (node = stack.pop()) {
		if (!node.nodeName || excludeNodes.indexOf(node.nodeName) !== -1) continue;
		let name = abbreviations[node.nodeName] || node.nodeName,
			num = ++counter[node.nodeName] || (counter[node.nodeName] = 1),
			eId = `${path + name}_${num}`;
		if (hidden_eId.indexOf(node.nodeName) === -1 && node.nodeName !== '#text') {
			//getting eId from <num> tag
			if (numPointIds.indexOf(node.nodeName) !== -1) {
				//is always the first child
				if (node.firstChild && node.firstChild.nodeName === 'num') {
					if (node.firstChild &&
						node.firstChild.firstChild &&
						node.firstChild.firstChild.data) {
						const pointNum = getNumberFromString(node.firstChild.firstChild.data);
						eId = eId.substring(0, eId.lastIndexOf('_'));
						node.setAttribute('eId', `${eId}_${pointNum}`);
					}
				//if not, normal assignment
				} else {
					node.setAttribute('eId', eId);
				}
			//normal assignment
			} else {
				node.setAttribute('eId', eId);
			}
		}
		if (contexts.indexOf(node.nodeName) === -1 && node.childNodes) {
			for (let i = node.childNodes.length - 1; i >= 0; i--) { stack.push(node.childNodes[i]); }
		}

		processed_nodes.push(node);
	}

	 // Remove num _1 on unique elements
	// processed_nodes.forEach((node) => {
	// 	if (node.attributes) {
	// 		const eId = node.getAttribute('eId');
	// 		if (counter[node.nodeName] === 1 && eId) {
	// 			node.setAttribute('eId', eId.slice(0, -2));
	// 		}
	// 	}
	// });


	// Call generateIds recursively on sub contexts;
	processed_nodes.forEach((node) => {
		if (contexts.indexOf(node.nodeName) !== -1) {
			//here generate new context
			if (node.nodeName === 'article') {
				let newIdStr = node.getAttribute('eId');
				let startIndx = node.getAttribute('eId').indexOf('_art');
				newIdStr = newIdStr.substring(startIndx + 1, newIdStr.length);
				node.setAttribute('eId', newIdStr);
				generateIds(node, `${node.getAttribute('eId')}__`);
			} else {
				generateIds(node, `${node.getAttribute('eId')}__`);
			}
		}
	});
};

module.exports = generateIds;
