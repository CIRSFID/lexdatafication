// Code ported from LIME
// Class for AkomaNtoso URI management
// Note: documentation of AkomaNtoso URI is a little vague in some places.
// This implementation is not (and can't be) perfect.
//
// var uri = AknMain.Uri.parse(string uri work/expression/manifestation)
//
// uri.country     "it"                       | Work
// uri.type        "act"                      |
// uri.subtype     "legge" (optional)         |
// uri.author      "stato" (optional)         |
// uri.date        "2014-09-12"               |
// uri.name        "2" "nomelegge" (optional) |
// uri.component   "main" "table1" (optional) |
// uri.language    "ita"                      | Expression
// uri.version     "2015-03-12"    (optional) |
// uri.official    "official"      (optional) |
// uri.generation  "2015-04-11"    (optional) |
// uri.media       ".xml"                     | Manifestation
// uri.path        "http://sinatra ... xml"   | Item
//
// uri.work()          -> "/akn/it/act/legge/stato/2014-09-12/2/main"
// uri.expression()    -> "/akn/it/act/legge/stato/2014-09-12/2/ita@2015-03-12!official/2015-04-11/main"
// uri.manifestation() -> "/akn/it/act/legge/stato/2014-09-12/2/ita@2015-03-12!official/2015-04-11/main.xml"
// uri.item()          -> "http://sinatra.cirsfid.unibo.it/node/documentsdb/mnardi@unibo.it/myFiles/esempio.xml"
// work and expression allows you to hide the component part (Eg. FRBRuri generation)
// uri.work(true)      -> "/akn/it/act/legge/stato/2014-09-12/2"

// Todo: extract code
// Fix documentation
// handle components
var Uri = {
	empty: function () {
		var uri = {
			country: undefined,
			type: undefined,
			subtype: undefined,
			author: undefined,
			date: undefined,
			name: undefined,
			language: undefined,
			version: undefined,
			official: undefined,
			generation: undefined,
			component: undefined,
			media: 'xml',
			path: undefined
		};
		uri.work = this.uriFunctions.work.bind(uri),
		uri.expression = this.uriFunctions.expression.bind(uri),
		uri.manifestation = this.uriFunctions.manifestation.bind(uri),
		uri.item = this.uriFunctions.item.bind(uri);
		return uri;
	},

	parse: function (uriStr) {
		var uri = this.empty(),
			components = uriStr.split('/');
			is = this.is;

		if (is.component(components[components.length-1]))
			uri.component = components.pop();
		required(is.empty, 'Unexpected uri start');
		optional(is.equalTo('akn'));
		uri.country = required(is.country, 'Missing country');
		uri.type = required(is.type, 'Invalid doc type');

		uri.date = optional(is.date, function () {
			uri.subtype = required(is.subtype, 'Invalid date and subtype');
			return optional(is.date, function () {
				uri.author = required(is.anything);
				return required(is.date, 'Invalid date')
			});
		});

		var version = optional(is.version, function () {
			uri.name = optional(is.anything);
			var versionCandidate = optional(is.version);
			while(!versionCandidate && components.length > 0) {
				optional(is.anything); // Ignore not version part, just consume it
				versionCandidate = optional(is.version);
			}
			return versionCandidate;
		});

		if (is.version(version)) {
			uri.language = version.substring(0, 3);
			uri.version = version.substring(4);
			if (uri.version === '') uri.version = uri.date;

			var lastElement = optional(is.anything);
			if (lastElement && lastElement.endsWith('.xml')) {
				uri.media = 'xml';
				uri.component = lastElement.substring(0, lastElement.length - 4);
			} else if (lastElement) {
				uri.component = lastElement;
			}
		}

		uri.component = (uri.component !== undefined && uri.component.charAt(0) == '!')
							? uri.component.substring(1) : uri.component;

		function optional(test, failure) {
			if (components.length == 0)
				return failure ? failure() : undefined;
			var item = components.shift();
			if (test(item)) {
				return item;
			} else {
				components.unshift(item);
				return failure ? failure(item) : undefined;
			}
		}
		function required(test, errorMsg) {
			return optional(test, function (item) {
				throw new Error(errorMsg + ': "' + item + '"');
			});
		}

		return uri;
	},

	// Functions to check if a string can be a certain URI component
	is: {
		// Accept every non empty string
		anything: function (item) {
			return !!item;
		},
		// Accept only empty strings
		empty: function (item) {
			return item === '';
		},
		// Equality check
		equalTo: function (str) {
			return function (item) {
				return item === str;
			}
		},
		// Accept country codes
		country: function (item) {
			return item && (item.length === 2 || item.length === 3 || item.length === 4);
		},
		// Accept dates
		date: function (item) {
			return !isNaN(Date.parse(item));
		},
		// Accept versions eg "ita@2009-10-10"
		version: function (item) {
			return item && !!item.match(/^\w\w\w@/);
		},
		// Accept document types
		type: function (item) {
			return item && [
				'doc',
				'act',
				'bill',
				'debate',
				'debateReport',
				'statement',
				'judgment',
				'portion',
				'officialGazette',
				'documentCollection'
			].indexOf(item) !== -1;
		},
		// Accept subtypes
		subtype: function (item) {
			return item && !!item.match(/^[a-zA-Z\.]*$/);
		},
		author: function (item) {
			return item && !!item.match(/^[a-zA-Z\.]*$/);
		},
		component: function (item) {
			item = item && item.charAt(0) == '!' ? item.substring(1) : item;
			return item && [
				'main',
				'table',
				'schedule'
			].indexOf(item) !== -1;
		}
	},

	// URI output functions
	// Eg. uri.work()
	uriFunctions: {
		work: function (hideComponent) {
			var uri = '/akn' +
					'/' + this.country +
					'/' + this.type +
					(this.subtype ? '/' + this.subtype : '') +
					(this.author ? '/' + this.author : '') +
					'/' + this.date +
					(this.name ? '/' + this.name : '') +
					((this.component && !hideComponent) ? '/!' + this.component : '');
			return uri;
		},

		expression: function (hideComponent) {
			var version = ((this.version && this.version != this.date) ? this.version : '');
			var uri = this.work(true) +
					'/' + this.language +
					'@' + version +
					(this.official ? '!' + this.official : '') +
					((this.component && !hideComponent) ? '/!' + this.component : '');
			return uri;
		},

		/**
		 * If forFrbrUri is set to true returns with as should be set for FRBRthis in FRBRmanifestation
		 *  else if set to false returns with as should be set for FRBRuri in FRBRmanifestation (i.e.
		 *  with .akn instead of .xml )
		 * @param forFrbrUri
		 * @returns {string}
		 */
		manifestation: function (forFrbrUri) {
			var uri = this.expression(true) +
					(forFrbrUri ? '.' + this.media :
					'/!' + this.component + '.' +this.media);
			return uri;
		},

		item: function () {
			return this.path;
		}
	}
};

module.exports = Uri;
