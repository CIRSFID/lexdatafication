/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import xpath from 'xpath';
import appConfig from '../../configs/configs.json';

const select = xpath.useNamespaces({ akn: appConfig.aknTags.aknNamespace });

// Given the xmlbuilder object, add eId attributes to all
// appropriate elements
const getNumberFromString = (str) => {
	const numReg = new RegExp(/(?:[\u00C0-\u1FFF\u2C00-\uD7FF\w]+\W*\s+)?((?:\w+[\.)]?)+(?:[- ]+\w{3,10}){0,1})/);
	const romanReg = new RegExp(/M{0,4}(?:CM|CD|D?C{0,3})(?:XC|XL|L?X{0,3})(?:IX|IV|V?I{0,3})/);

	const normalize = (str) => {
		let normalized = '',
			numMatch = str.match(numReg);
		if (numMatch && numMatch.length) {
			normalized = numMatch[1];
		}

		const romanNumer = normalized.match(romanReg);
		if (!romanNumer || !romanNumer[0].length) {
			normalized = normalized.toLowerCase();
		}

		normalized = normalized.replace(/[\.)\s]/g, '');
		return normalized;
	};

	return normalize(str);
};

const AknIdGenerator = {
	noPrefixElements : ["act", "collectionBody", "body", "mainBody", "documentRef",
						 "componentRef", "authorialNote", "ref", "person",
						 "role", "location", "article", "mod"],
	noIdElements: ["act", "doc",  "preface", "preamble", "body", "mainBody",
					"collectionBody", "attachments", "content", "list", "p",
					"num", "heading", "table", "tr", "td", "conclusions",
					"date", "docDate", "docType", "docTitle", "docNumber",
					"formula"],
	documentContextElements: ["mod", "article"],
	noIdNumElements: ["components", "conclusions", "content", "heading"],
	noGenerateIdInside: ["meta"],
	prefixSeparator: "__",
	numSeparator: "_",
	abbreviations : {
		alinea : "al",
		article : "art",
		attachment : "att",
		blockList : "list",
		chapter : "chp",
		citation : "cit",
		citations : "cits",
		clause : "cl",
		component : "cmp",
		components : "cmpnts",
		componentRef : "cref",
		debateSection : "dbsect",
		division : "dvs",
		documentRef : "dref",
		eventRef : "eref",
		intro : "intro",
		list : "list",
		listIntroduction : "intro",
		listWrapUp : "wrap",
		paragraph : "para",
		quotedStructure : "qstr",
		quotedText : "qtext",
		recital : "rec",
		recitals : "recs",
		section : "sec",
		subchapter : "subchp",
		subclause : "subcl",
		subdivision : "subdvs",
		subparagraph : "subpara",
		subsection : "subsec",
		temporalGroup : "tmpg",
		wrapUp : "wrapup"
	},

	generateId : function(node, root, enforce, parents, globalCounter) {
		var me = this, markingId = "",
			elName = node.nodeName;
		me.parents = parents;
		try {
			if ( enforce || me.needsElementId(elName) ) {
				var markedParent = node.parentNode,
					documentContext = root;

				if (markedParent && me.getParentByNameFromParents(markedParent, 'mod')) {
					markingId = me.getMarkingIdParentPart(elName, markedParent, documentContext, true);
				} else {
					markingId = me.getMarkingIdParentPart(elName, markedParent, documentContext);
				}

				var context = markedParent || root;
				if (this.noPrefixElements.indexOf(elName) != -1) {
					context = documentContext;
				}
				var alternativeContext = me.getParentByNameFromParents(node, 'collectionBody');
				context = (alternativeContext) ? alternativeContext : context;

				if ( me.documentContextElements.indexOf(elName) != -1 ) {
					context = me.getParentByNameFromParents(node, 'mod') || documentContext;
				} else if ( elName != 'content' && me.getParentByNameFromParents(node, 'content', 2) ) {
					markingId+='content'+me.prefixSeparator;
				}
				var elNum = me.getElNum(node, elName, context, context == root, globalCounter);
				var elementRef = (me.abbreviations[elName]) ? (me.abbreviations[elName]) : elName;
				markingId += elementRef;
				if ( me.noIdNumElements.indexOf(elName) == -1 ) {
					markingId += me.numSeparator+elNum;
				}
			}
		} catch(e) {
			console.log(e, elName);
		}
		return markingId;
	},

	needsElementId: function(elName) {
		return (elName && (this.noIdElements.indexOf(elName) == -1));
	},

	getMarkingIdParentPart: function(elName, markedParent, documentContext, enforce) {
		var markingId = '',
			attributeName = 'eId',
			parentName = (markedParent) ? markedParent.nodeName : false,
			cmpParent = (documentContext) ? this.getParentByNameFromParents(documentContext, 'component') : false,
			cmpPrefix = (cmpParent) ? cmpParent.getAttribute(attributeName)+this.prefixSeparator : "";

		if( markedParent && (this.noPrefixElements.indexOf(elName) == -1 || enforce) ) {

			markingId += markedParent.getAttribute && markedParent.getAttribute(attributeName) || '';
			markingId += (!markingId) ?
						this.getMarkingIdParentPart(parentName, markedParent.parentNode)
						: this.prefixSeparator;
		}

		if ( cmpPrefix && !markingId.startsWith(cmpPrefix) ) {
			markingId = cmpPrefix+markingId;
		}
		return markingId;
	},


	/* Find elNum by counting the preceding elements with
	   in given the same name in the context.*/

	getElNum: function(element, elName, context, isGlobal, globalCounter) {

		var elNum = false,
			num = this.getChildByName(element, 'num');

		if ( num && (num.parentNode == element || num.parentNode.nodeName == 'content') ) {
			var text = (num) ? num.textContent : "";
			elNum = getNumberFromString(text);
		}


		if ( !elNum ) {
			elNum = 1;
			if (isGlobal) {
				elNum = globalCounter[elName];
			} else {
				//TODO: don't count the elements inside quotedStructure when the context is not mod
				var siblings = select('.//akn:'+elName, context);
				if(siblings.length) {
					var elIndexInParent = siblings.indexOf(element);
					elNum = (elIndexInParent!=-1) ? elIndexInParent+1 : elNum;
				}
			}
		}
		return elNum;
	},

	getParentByNameFromParents: function(node, name) {
		return this.parents[name];
	},

	getParentByName: function(node, name, maxSteps) {
		maxSteps = maxSteps === undefined ? Number.MAX_SAFE_INTEGER : maxSteps;
		return select('./*[ancestor::akn:'+name+']', node)[0];
		// console.log(maxSteps);
		// if (!node.parentNode || maxSteps === 0) return;
		// if (node.parentNode.nodeName == name) {
		// 	return node.parentNode;
		// }
		// return this.getParentByName(node.parentNode, name, --maxSteps);
	},

	getChildByName: function(node, name) {
		for (let i = 0; i < node.childNodes.length; i++) {
			let child = node.childNodes[i];
			if (child.nodeType !== node.ELEMENT_NODE)
				continue;
			if (child.nodeName == name)
				return child;
		}
	}
}

const generateIds = (xmlDoc) => {
	var globalCounter = {};
	const genCall = (node, parents) => {
		parents[node.nodeName] = node;
		for (let i = 0; i < node.childNodes.length; i++) {
			let child = node.childNodes[i];
			if (child.nodeType == node.PROCESSING_INSTRUCTION_NODE ||
				child.nodeType == node.COMMENT_NODE ||
				child.nodeName == '#text' ||
				AknIdGenerator.noGenerateIdInside.indexOf(child.nodeName) >= 0) {
					continue;
			}
			globalCounter[child.nodeName] = globalCounter[child.nodeName] || 0;
			globalCounter[child.nodeName]++;
			if (child.nodeType !== node.DOCUMENT_NODE &&
				child.nodeName !== 'akomaNtoso') {
				var eId = AknIdGenerator.generateId(child, xmlDoc, false, parents, globalCounter);
				// console.log(child.nodeName, ' -> eId -> ', eId);
				if (eId) {
					child.setAttribute('eId', eId);
				}
			}
			genCall(child, Object.assign({}, parents));
		}
	}
	genCall(xmlDoc, {});
};

module.exports = generateIds;
