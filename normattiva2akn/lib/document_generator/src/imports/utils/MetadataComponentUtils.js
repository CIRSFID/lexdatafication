/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import AknUri from './AknUri';

class MetadataComponentUtils {
	constructor(data, componentId) {
		this.data = data;
		this.componentId = componentId;
		this.invalidDatesTypes = ['0-00-00'];
		this.tipoDocRefinings = [
			{
				name: "DECRETO DEL PRESIDENTE DELLA REPUBBLICA",
				ref: "dpr"
			}
		];
	}

	checkDates(){
		if (!this.data.dataDoc || this.invalidDatesTypes.indexOf(this.data.dataDoc) !== -1) {
			this.data.dataDoc = '9999-09-09';
		}
		if (!this.data.dataVigoreOriginale || this.invalidDatesTypes.indexOf(this.data.dataVigoreOriginale) !== -1) {
			this.data.dataVigoreOriginale = '';
		}
		if (!this.data.dataPubblicazione || this.invalidDatesTypes.indexOf(this.data.dataPubblicazione) !== -1) {
			this.data.dataPubblicazione = '9999-09-09';
		}
		if (!this.data.tipoPubblicazione || this.invalidDatesTypes.indexOf(this.data.tipoPubblicazione) !== -1) {
			this.data.tipoPubblicazione = '9999-09-09';
		}
	}

	refineTipoDoc(stringIn){
		let refinedTipoDoc = stringIn;
		this.tipoDocRefinings.forEach((el) => {
			if(el.name === stringIn){
				refinedTipoDoc = el.ref;
			}
		});
		refinedTipoDoc = refinedTipoDoc.replace(' ','-');
		return refinedTipoDoc;
	}

	createContent() {
		this.checkDates();
		const workDate = this.data.dataDoc;
		const versionData = this.data.dataVigoreOriginale || workDate;
		const manifestationDate = new Date().toISOString().substr(0, 10);
		const pubDate = this.data.dataPubblicazione;
		const pubName = this.data.tipoPubblicazione;
		const pubNumber = this.data.numeroPubblicazione;
		const urnNir = this.data.urn;
		const eliNir = this.data.eli;
		const uri = this.buildUri();

		let metaStr = `<meta>
				<identification source="#source">
					<FRBRWork>
						<FRBRthis value="${uri.work()}"/>
						<FRBRuri value="${uri.work(true)}"/>
						<FRBRalias value="${urnNir}" name="urn:nir"/>
						<FRBRalias value="${eliNir}" name="eli"/>
						<FRBRdate name="" date="${workDate}"/>
						<FRBRauthor as="#author" href=""/>
						<FRBRcountry value="it"/>
					</FRBRWork>
					<FRBRExpression>
						<FRBRthis value="${uri.expression()}"/>
						<FRBRuri value="${uri.expression(true)}"/>
						<FRBRdate name="" date="${versionData}"/>
						<FRBRauthor as="#author" href=""/>
						<FRBRlanguage language="ita"/>
					</FRBRExpression>
					<FRBRManifestation>
						<FRBRthis value="${uri.manifestation()}"/>
						<FRBRuri value="${uri.manifestation(true)}"/>
						<FRBRdate name="" date="${manifestationDate}"/>
						<FRBRauthor as="#author" href=""/>
					</FRBRManifestation>
				</identification>
		</meta>`;
		metaStr = metaStr.replace(/[\t\n]+/g, '');
		return metaStr;
	}

	buildUri() {
		const uri = AknUri.empty();
		uri.country = 'it';
		uri.type = 'act';
		uri.subtype = this.refineTipoDoc(this.data.tipoDoc);
		uri.author = 'stato';
		uri.date = this.data.dataDoc;
		uri.language = 'ita';
		uri.version = this.data.dataVigoreOriginale;
		uri.component = this.componentId;
		if (this.data.numDoc === "0") {
			uri.name = this.data.redazione;
		} else {
			uri.name = this.data.numDoc;
		}
		return uri;
	}
}

module.exports = MetadataComponentUtils;
