/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import xmldom from 'xmldom';
import MetadataComponentUtils from '../utils/MetadataComponentUtils';

class LavoriPreparatoriBuilder {
	constructor(tagObj, metadata, actType, componentId) {
		this.lavoriPreparatori = tagObj || [];
		this.metadata = metadata || {};
		this.componentId = componentId || 'component_1';
		this.actType = actType || 'monovigente';
		this.metadataAknString = new MetadataComponentUtils(this.metadata, this.componentId).createContent();
		this.DOMParser = new xmldom.DOMParser();
		this.XMLSerializer = new xmldom.XMLSerializer();
	}

	createContent() {
		return new Promise((resolve, reject) => {
			resolve({
				xmlContent: this.createContentString(),
				references: []
			});
		});
	}

	createContentString() {
		if (this.lavoriPreparatori.length === 0) return '';
		const lpCnt = new xmldom.DOMImplementation().createDocument();
		const componentCnt = lpCnt.createElement('component');
		componentCnt.setAttribute('eId', this.componentId);
		//metadata section
		const metaData = this.DOMParser.parseFromString(this.metadataAknString);

		//doc section
		const actCnt = lpCnt.createElement('doc');
		actCnt.setAttribute('name', 'lavoriPreparatori');
		actCnt.setAttribute('contains', 'singleVersion');
		componentCnt.appendChild(actCnt);
		//table section
		const tableElement = lpCnt.createElement('table');

		this.lavoriPreparatori.forEach((el, idx) => {
			const tr = lpCnt.createElement('tr');
			const td = lpCnt.createElement('td');
			const p = lpCnt.createElement('p');
			const ref = lpCnt.createElement('ref');
			//const ar = lpCnt.createElement('alternativeReference');
			//table section
			ref.setAttribute('href', el.url);
			ref.setAttribute('eId', `ref_${idx}`);
			ref.appendChild(lpCnt.createTextNode(el.sequenza));
			p.appendChild(ref);
			td.appendChild(p);
			tr.appendChild(td);
			tableElement.appendChild(tr);
			//metadata section
			//ar.setAttribute('for', `#ref_${idx}`);
			//ar.setAttribute('href', el.url);
			//ar.setAttribute('refersTo', '#lavoriPreparatori');
			//orCnt.appendChild(ar);
		});
		const bodyCnt = lpCnt.createElement('mainBody');
		//metadata section
		actCnt.appendChild(metaData);
		//table section
		bodyCnt.appendChild(tableElement);
		actCnt.appendChild(bodyCnt);
		return this.XMLSerializer.serializeToString(componentCnt);
	}
}

module.exports = LavoriPreparatoriBuilder;
