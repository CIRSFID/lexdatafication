/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import TagCreator from '../utils/TagCreator';
import TagParseBuilder from '../utils/TagParseBuilder';
import FirmeConclusionBuilder from '../poldo_utils/FirmeConclusionBuilder';
import ReferenceBuilder from '../poldo_utils/ReferenceBuilder';

class ConclusionBuilder {
	constructor(tagObj) {
		this.testoFormulaFinaleConclusioni = tagObj.testoFormulaFinaleConclusioni || '';
		this.testoData = tagObj.testoData || '';
		this.testoFirmatari = tagObj.testoFirmatari || '';
		this.testoCompleto = tagObj.testoCompleto || '';
		this.tagParseBuilder = new TagParseBuilder();
	}

	createContent() {
		return new Promise((resolve, reject) => {
			const coclusionStr = this.createContentString();
			if (!coclusionStr) {
				return resolve({
					xmlContent: '',
					references: []
				});
			}
			const cnclBuilder = new FirmeConclusionBuilder(coclusionStr);
			cnclBuilder.createFirme()
				.then((firmeRes) => {
					const refBuilder = new ReferenceBuilder(firmeRes.xmlContent);
					refBuilder.createRef()
						.then((refRes) => {
							resolve({
								xmlContent: refRes.xmlContent,
								references: refRes.references.concat(firmeRes.references)
							});
						})
						.catch((refErr) => {
							console.log(refErr);
							resolve({
								xmlContent: firmeRes.xmlContent,
								references: []
							});
						});
				}).catch((firmeResErr) => {
					console.log(firmeResErr);
					resolve({
						xmlContent: coclusionStr,
						references: []
					});
				});
		});
	}

	createContentString() {
		let testoData, fullTag;
		if (!this.testoFormulaFinaleConclusioni
			&& !this.testoData
			&& !this.testoFirmatari
			&& this.testoCompleto) {
			return `<conclusions><p>${this.testoCompleto}</p></conclusions>`;
		}
		const testoFormulaFinaleConclusioni = TagCreator.createTagWithAttr('formula', this.testoFormulaFinaleConclusioni, [{ attrName: 'name', attrValue: 'formulaFinaleConclusioni' }], 'p');
		//parse and create dates
		testoData = this.tagParseBuilder.createDatesTagInString(this.testoData);
		//parse and create locations
		testoData = this.tagParseBuilder.createLocationTagInString(testoData);
		testoData = TagCreator.createSimpleTag('p', testoData);
		const testoFirmatari = TagCreator.createSimpleTag('p', this.testoFirmatari);
		if (this.testoCompleto !== '') {
			this.testoCompleto = this.testoCompleto.replace(this.testoFormulaFinaleConclusioni, testoFormulaFinaleConclusioni);
			this.testoCompleto = this.testoCompleto.replace(this.testoData, testoData);
			this.testoCompleto = this.testoCompleto.replace(this.testoFirmatari, testoFirmatari);
			return `<conclusions>${this.testoCompleto}</conclusions>`;
		}
		fullTag = `${testoFormulaFinaleConclusioni}${testoData}${testoFirmatari}`;
		if (fullTag.length > 0) {
			return `<conclusion>${fullTag}</conclusion>`;
		}
		return '';
	}
}

module.exports = ConclusionBuilder;
