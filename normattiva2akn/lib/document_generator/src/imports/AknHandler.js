/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import * as R from 'ramda';

import MetadataBuilder from './meta/MetadataBuilder';
import PrefaceBuilder from './preface/PrefaceBuilder';
import PreambleBuilder from './preamble/PreambleBuilder';
import BodyBuilder from './body/BodyBuilder';
import LavoriPreparatoriBuilder from './component/LavoriPreparatoriBuilder';
import ConclusionBuilder from './conclusion/ConclusionBuilder';
import TransitionalBuilder from './transitional/TransitionalBuilder';
import IdBuilder from './utils/IdBuilder';
import ModsRefiner from './refiners/ModsRefiner';
import config from '../configs/configs.json';
import IdRefiner from './refiners/IdRefiner';

class AknHandler {
	constructor(jsonData) {
		this.jsonData = jsonData;
	}

	buildContent() {
		return new Promise((resolve, reject) => {
			try {
				const promises = [];
				//metadata are builded after all the other promises are resolved
				//const metadata = new MetadataBuilder(this.jsonData);
				const conclusion = new ConclusionBuilder(this.jsonData.conclusioni).createContent();
				const preface = new PrefaceBuilder(this.jsonData.intestazione).createContent();
				const preamble = new PreambleBuilder(this.jsonData.preambolo).createContent();
				const transitional = new TransitionalBuilder(this.jsonData.transitorie).createContent();
				//doc component_1 -> Lavori Preparatori
				const lavoriPreparatori = new LavoriPreparatoriBuilder(
					this.jsonData.lavoriPreparatori,
					this.jsonData.metadati,
					'monovigente', //act type
					'component_1' // fixed eId
				).createContent();

				//fills promise array
				promises.push(conclusion); //0 conclusions
				promises.push(preface); //1 preface
				promises.push(preamble); //2 preamble
				promises.push(transitional); //3 transitional
				promises.push(lavoriPreparatori); //4 lavoriPreparatori

				Promise.all(promises)
					.then((resValues) => {
						//get xml content by index
						let conclusionXmlString = resValues[0].xmlContent;
						let prefaceXmlString = resValues[1].xmlContent;
						let preambleXmlString = resValues[2].xmlContent;
						let transitionalXmlString = resValues[3].xmlContent;
						let lavoriPreparatoriXmlString = resValues[4].xmlContent;
						let articolatoXmlString;
						//here builds the body
						new BodyBuilder(this.jsonData.articolato.elementi, transitionalXmlString)
							.createFullTag()
							.then((res) => {
								//add the body responce
								articolatoXmlString = res.xmlContent;
								resValues.push(res);
								/* put all the (poldo's) references together to create only one references array,
								then create a flat array */
								let allRefs = [].concat.apply([], resValues.reduce((a, b) => a.concat(b.references), []));
								//appends lavori preparatori to allRefs
								let lavoriPrepRefs = this.jsonData.lavoriPreparatori.map((el) => {
									el.kind = 'lavori_preparatori';
									return el;
								});
								allRefs = allRefs.concat(lavoriPrepRefs);
								let metadata = new MetadataBuilder(this.jsonData, allRefs).createContent();
								resolve({
									preface: prefaceXmlString,
									preamble: preambleXmlString,
									metadata,
									articolato: articolatoXmlString,
									conclusion: conclusionXmlString,
									lavoriPreparatori: lavoriPreparatoriXmlString
								});
							}).catch(err => reject(err));
					}).catch(err => reject(err));
			} catch (error) {
				console.error(error);
				reject(error);
			}
		});
	}

	buildAttachments() {
		console.log(this.jsonData.annessi.elementi.length);
		let attPromises = this.jsonData.annessi.elementi
							.map((att) => this.buildAttachment(att));
		return Promise.all(attPromises);
	}

	buildAttachment(attachment) {
		attachment.metadati = R.clone(this.jsonData.metadati);
		const metadata = new MetadataBuilder(attachment).createContent();
		const bodyPromise = new BodyBuilder(attachment.articolato.elementi)
							.createFullTag('mainBody');
		const preamble = new PreambleBuilder(attachment.preambolo).createContent();
		return bodyPromise.then((bodyStr) => {
			return this.createAttachmentTag(metadata, preamble, bodyStr);
		});
	}

	createAttachmentTag(meta = '', preamble = '', body = '') {
		return `<attachment>
					<doc name="allegato">${meta}${preamble}${body}</doc>
				</attachment>`;
	}

	createContent(config) {
		return new Promise((resolve, reject) => {
			this.buildContent().then((tagsObj) => {
				try {
					const aknContent = this.createAKNTag(
						`${tagsObj.metadata}${tagsObj.preface}${tagsObj.preamble}${tagsObj.articolato}${tagsObj.conclusion}`,
						[tagsObj.lavoriPreparatori],
						tagsObj.attachments || [],
					);
					//generates id
					const aknContentWhitIds = new IdBuilder(aknContent).createIdOnContent();
					//hanlde modifications (()) and relative metadata
					//NB: Create also analysis node in meta section
					let cntRefined = aknContentWhitIds;
					if (!config.disableMods) {
						cntRefined = new ModsRefiner(aknContentWhitIds, this.jsonData).evalModificationsAndMetadata();
					}

					cntRefined = new IdRefiner(cntRefined, this.jsonData).refineMeta();

					// TODO: move to a refiner
					cntRefined = cntRefined.replace(/<passiveModifications\/>/g, '');


					//resolve content
					resolve(cntRefined);
				} catch (error) {
					console.error(error);
					reject(error);
				}
			}).catch((err) => {
				console.error(err);
				reject(err);
			});
		});
	}
	createAKNTag(innerContent, components, attachments) {
		components = components.filter((str) => !!str);
		attachments = attachments.filter((str) => !!str);
		const componentsTag = components.length ?
					`<components>${components.join('')}</components>` : '';
		const attachmentsTag = attachments.length ?
					`<attachments>${attachments.join('')}</attachments>` : '';
		const defaultNS = config.aknTags.aknNamespace;
		const htmlNS = config.aknTags.w3Tag;
		const cirsfidNS= config.aknTags.xmlns_cirsfid;
		const foNS = config.aknTags.xmlns_fo;
		return `<akomaNtoso xmlns="${defaultNS}" xmlns:html="${htmlNS}" xmlns:fo="${foNS}" xmlns:cirsfid="${cirsfidNS}">
					<act name="monovigente">${innerContent}${attachmentsTag}</act>
					${componentsTag}
				</akomaNtoso>`;
	}
}
module.exports = AknHandler;
