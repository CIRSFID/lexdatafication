/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import xpath from 'xpath';
import xmldom from 'xmldom';
import http from 'http';
import config from '../../configs/configs.json';

const refMarkerApi = {
	host: 'bach.cirsfid.unibo.it',
	path: '/poldo/markup/',
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	}
};

class FirmeConclusionBuilder {
	constructor(xmlString) {
		this.xmlString = xmlString || '';
		this.xmlObj = null;
		try {
			this.xmlObj = new xmldom.DOMParser().parseFromString(this.xmlString);
		} catch (error) {
			console.log(error);
			throw error;
		}
	}

	createFirme() {
		return new Promise((resolve, reject) => {
			if (this.xmlObj) {
				this.getRemoteFirmeByRequest()
					.then((res) => {
						resolve({
							xmlContent: res.markup,
							references: res.references
						});
					})
					.catch((refErr) => {
						console.log(refErr);
						reject(refErr);
					});
			}
		});
	}

	evalRefsMeta(xmlStringDoc, references) {
		if (!(typeof references !== 'undefined' && references.length > 0)) {
			return xmlStringDoc;
		}
		const xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
		const corteCostRef = [];
		/*
		refMetadata.forEach((refArray) => {
			if (typeof refArray !== 'undefined' && refArray.length > 0) {
				refArray.forEach((el) => {
					if (el.akn_uri.indexOf('/cost/') > 0 && el.kind === 'atto_giudiziario') {
						corteCostRef.push(el);
					}
				});
			}
		});
		if (corteCostRef && corteCostRef.length > 0) {
			//set the analysis node for the otherReference
			const analysisNode = selectObj('//akn:meta/akn:analysis', xmlDoc);
			const referenceNode = selectObj('//akn:meta/akn:references', xmlDoc);
			const otherReferenceNode = xmlDoc.createElement('otherReferences');
			otherReferenceNode.setAttribute('source', '#cirsfid');
			corteCostRef.forEach((el) => {
				const alternativeReferenceNode = xmlDoc.createElement('alternativeReference');
				alternativeReferenceNode.setAttribute('for', `#${el.eId}`);
				alternativeReferenceNode.setAttribute('href', el.url);
				alternativeReferenceNode.setAttribute('refersTo', '#URL');
				otherReferenceNode.appendChild(alternativeReferenceNode);
			});
			if (analysisNode[0]) {
				analysisNode[0].appendChild(otherReferenceNode);
				//append TLCReference
				if (referenceNode[0]) {
					const tlcReferenceNode = xmlDoc.createElement('TLCReference');
					tlcReferenceNode.setAttribute('eId', 'URL');
					tlcReferenceNode.setAttribute('href', '/akn/ontology/url');
					tlcReferenceNode.setAttribute('showAs', 'URL');
					tlcReferenceNode.setAttribute('name', 'URL');
					referenceNode[0].appendChild(tlcReferenceNode);
				}
			}
		}
		*/
		return new xmldom.XMLSerializer().serializeToString(xmlDoc);
	}

	getRemoteFirmeByRequest() {
		return new Promise((resolve, reject) => {
			try {
				let refMarkupRes = '';
				const postOpt = refMarkerApi;
				const postData = JSON.stringify({
					text_input: this.xmlString,
					xml_safe: true,
					dev: true,
					filters: ['firme', 'all_persons']
				});
				postOpt.headers['Content-Length'] = Buffer.byteLength(postData);
				const postReq = http.request(postOpt, function (res) {
					res.setEncoding('utf8');
					res.on('data', (chunk) => {
						refMarkupRes += chunk.toString();
					});
					res.on('end', () => {
						try {
							resolve(JSON.parse(refMarkupRes));
						} catch (err) {
							console.log(err);
							console.log('----------------- POLDO ERROR --------------------');
							console.log('Sended data: ');
							console.log(this.xmlString);
							console.log('Server responce: ');
							console.log(refMarkupRes);
							console.log('--------------------------------------------------');
							reject(err);
						}
					});
					res.on('error', (err) => {
						console.log(err);
						reject(err);
					});
				});
				postReq.write(postData);
			} catch (err) {
				console.log(err);
				reject(err);
			}
		});
	}
}

module.exports = FirmeConclusionBuilder;
