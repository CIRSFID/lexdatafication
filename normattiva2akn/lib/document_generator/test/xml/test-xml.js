import expect from 'chai';
// import util from 'util';
import {
	parseString
} from 'xml2js';
import xml from '../../src/api/xml.api';

// xml.XMLparse('./test/resources/xslt-example.xsl', 'https://www.w3schools.com/xml/simple.xml').then((data) => {
// 	parseString(data, (err, result) => {
// 		console.log(util.inspect(result, false, null));	// pretty print JSON
// 	});
// }).catch(err => console.log(`Promise Rejected with error: ${err}`));

describe('#xmlChecker', () => {
	it('should contain only div, $, _, class as keys', () => {
		xml.XMLparse('./test/resources/xslt-example.xsl', './test/resources/xml-example.xml').then((data) => {
			parseString(data, (err, result) => {
				if (err) return console.log(err);
				expect(result).to.have.all.keys('div', '$', '_', 'class');
			});
		}).catch(err => console.log(`Promise Rejected with error: ${err}`));
	});
});
