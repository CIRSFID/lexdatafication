import glob from'glob';
import fs from 'fs';
import * as R from 'ramda';
import mongoose from 'mongoose';
import async from 'async';

const dbUrl = 'mongodb://137.204.140.124:27017/ipzs';

let allNums = [];

mongoose.connect(dbUrl);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function() {
	let docs = db.collection('docs');
	docs.find({"annessi.elementi.0": { "$exists": true }}, (err, cursor) => {
		if (err) {
			return console.error('error: ', err);
		}
		let count = 0;
		cursor.on('data', function(doc) {
			allNums = allNums.concat(getAttachmentsData(doc.annessi));
			count++;
			console.log('iter', allNums.length, count);
		});
		cursor.on('close', function() {
			console.log(count);
			mongoose.connection.close()
			var groups = R.toPairs(R.groupBy(R.prop('rubrica'))(allNums));
			// var nums = R.map(R.prop('0'), groups)
			console.log(JSON.stringify(groups, null, 4));
		});
	});
});

function getAttachmentsData(item) {
	let res = [];

	if (item.nomeNir && item.numNir && !isArt(item.numNir)) {
		res.push({rubrica: item.rubricaNir, numNir: item.numNir});
	}
	if (item.elementi) {
		item.elementi.map((el) => {
			let els = getAttachmentsData(el);
			res = res.concat(els);
		})
	}
	return res;
}

function isArt(num) {
	return num && (num.toLowerCase().indexOf('art.') >= 0 ||
				num.toLowerCase().indexOf('articolo') >= 0);
}
