import glob from'glob';
import fs from 'fs';
import * as R from 'ramda';
import mongoose from 'mongoose';
import async from 'async';

const dbUrl = 'mongodb://137.204.140.124:27017/ipzs';

let allNums = [];

mongoose.connect(dbUrl);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function() {
	let docs = db.collection('docs');
	docs.find({}, (err, cursor) => {
		if (err) {
			return console.error('error: ', err);
		}
		let count = 0;
		cursor.on('data', function(doc) {
			allNums = allNums.concat(getNums(doc.articolato));
			allNums = allNums.concat(getNums(doc.annessi));
			count++;
			console.error('iter', allNums.length, count);
		});
		cursor.on('close', function() {
			console.error(count);
			mongoose.connection.close();
			console.log(JSON.stringify(R.uniq(allNums), null, 4));
		});
	});
});

function getNums(item) {
	let res = [];

	if (isLibro(item.numNir)) {
		res.push(item.numNir);
	}
	if (item.elementi) {
		item.elementi.map((el) => {
			let els = getNums(el);
			res = res.concat(els);
		})
	}
	return res;
}

function isLibro(num) {
	return num && (num.toLowerCase().indexOf('lib.') >= 0 ||
				num.toLowerCase().indexOf('libro') >= 0);
}
