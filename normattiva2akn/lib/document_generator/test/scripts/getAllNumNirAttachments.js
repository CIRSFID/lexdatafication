import * as R from 'ramda';

import listOfAllAttNums from '../testData/listOfAllAttNums.json';

let items = listOfAllAttNums.map(JSON.parse);


let name = R.uniq(R.map(R.prop('nomeNir'), items));

let nums = R.uniq(
				R.map(R.prop('numNir'), items)
			);

let doubleNums = R.filter(
					R.compose(R.partial(R.equals, [3]), R.length, R.split('-')),
					nums
				);

// doubleNums.forEach((a) => console.log(a))
// console.log(doubleNums);

let notArt = R.filter(
	(num) => {
		return num.toLowerCase().indexOf('art.') < 0 &&
				num.toLowerCase().indexOf('articolo') < 0;
	},
	nums
);

notArt.forEach((a) => console.log(a))
console.log(notArt.length);
