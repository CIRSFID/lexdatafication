import { expect } from 'chai';
import crypto from 'crypto';
import nmt from '../../src/api/nmt.api.js';

// nmt.getGoogleGoodAndBad().then((data) => {
//     console.log(data);
// });

// nmt.getGoogleGoodAndBadSync((err, data) => {
//     if (err) console.log(`Errore: ${err}`);
//     else console.log(data);
// });

describe('#hiChecker', () => {
	it('should contain "Hi Law World!"', (done) => {
		nmt.getGoogleGoodAndBad().then((data) => {
			expect(data.resultsGood.hi).equal('Hi Law World!');
			done();
		}).catch(err => console.log(err));
	});

	it('should contain a correct hash field', (done) => {
		nmt.getGoogleGoodAndBad().then((data) => {
			expect(data.resultsGood.hash).equal(crypto.createHash('md5').update(data.resultsGood.data).digest('hex'));
			done();
		}).catch(err => console.log(err));
	}).timeout(5000);;
});

describe('#hiCheckerSync', () => {
	it('should contain "Hi Law World!"', (done) => {
		nmt.getGoogleGoodAndBadSync((err, data) => {
			expect(data.resultsGoodSync.hi).equal('Hi Law World!');
			done();
		});
	});

	it('should contain a correct hash field', (done) => {
		nmt.getGoogleGoodAndBadSync((err, data) => {
			//console.log(data.resultsGoodSync.hash, crypto.createHash('md5').update(data).digest('hex'))
			expect(data.resultsGoodSync.hash).equal(crypto.createHash('md5').update(data.resultsGoodSync.data).digest('hex'));
			done();
		})
	}).timeout(5000);;
});

