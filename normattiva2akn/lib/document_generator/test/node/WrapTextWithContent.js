
import { expect } from 'chai';
import nmt from '../../src/api/nmt.api.js';
import appConfig from '../../src/configs/configs.json';
import jsonTestData1 from '../testData/no_content_wrapper.json';
import jsonTestData2 from '../testData/no_content_wrapper2.json';
import xpath from 'xpath';
import xmldom from 'xmldom';

const select = xpath.useNamespaces({ akn: appConfig.aknTags.aknNamespace });

function parseDoc(xmlStringDoc) {
	if (!xmlStringDoc) {
		throw new Error('Not defined xml document on refiner constructor');
	}
	try {
		return new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
	} catch (err) {
		console.error(err);
		throw err;
	}
}

describe('#Translate akn from json', () => {
	it('should wrap text in articles', (done) => {
		nmt.translateDoc(jsonTestData1)
		.then((res) => {
			// console.log(res);
			const doc = parseDoc(res);
			let artContent = select('//akn:article[@eId="art_2"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_3"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_4"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_5"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_6"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_7"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_11"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_14"]/akn:content', doc);
			expect(artContent.length).equal(1);
			done();
		})
		.catch((err) => {
			console.error("Error:", err);
		});
	});
	it('should wrap text in articles second document', (done) => {
		nmt.translateDoc(jsonTestData2)
		.then((res) => {
			// console.log(res);
			const doc = parseDoc(res);
			let artContent = select('//akn:article[@eId="art_14"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_15-bis"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_16"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_18"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_20"]/akn:content', doc);
			expect(artContent.length).equal(1);
			artContent = select('//akn:article[@eId="art_34"]/akn:content', doc);
			expect(artContent.length).equal(1);
			done();
		})
		.catch((err) => {
			console.error("Error:", err);
		});
	});
});

