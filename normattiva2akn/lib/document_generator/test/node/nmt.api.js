
import { expect } from 'chai';
import nmt from '../../src/api/nmt.api.js';
import appConfig from '../../src/configs/configs.json';
import jsonTestData1 from '../testData/004G0062.json';
import fs from 'fs';
import http from 'http';

const validateAknCb = (xmlFileContent, cb) => {
		try {
			let validationRes = '';
			const postOpt = appConfig.validationApi;
			const postData = JSON.stringify({ source: xmlFileContent });
			postOpt.headers['Content-Length'] = Buffer.byteLength(postData);
			const postReq = http.request(postOpt, (res) => {
				res.setEncoding('utf8');
				res.on('data', (chunk) => {
					validationRes += chunk.toString();
				});
				res.on('end', () => {
					cb(null, validationRes);
				});
				res.on('error', (err) => {
					console.log(err);
					cb(err);
				});
			});
			postReq.write(postData);
		} catch (err) {
			console.log(err);
			cb(err);
		}
};

describe('#Translate akn from json', (done) => {
	it('should convert json to akn', () => {
		nmt.translateDoc(jsonTestData1)
			.then((res) => {
				fs.writeFile('./transalteTest.xml', res, (err) => {
					if (err) return console.error(err);
					console.log("xml test file created");
					console.log("...validating");
					validateAknCb(res, (err, res) => {
						if (err) return console.log("validation service error");
						console.log(res)
						done();
					});
				});
			})
			.catch((err) => {
				console.error("Error:", err);
			});
	});
});

