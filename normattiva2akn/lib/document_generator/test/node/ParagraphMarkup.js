
import { expect } from 'chai';
import nmt from '../../src/api/nmt.api.js';
import appConfig from '../../src/configs/configs.json';
import jsonTestData1 from '../testData/paragraph_markup.json';
import xpath from 'xpath';
import xmldom from 'xmldom';

const select = xpath.useNamespaces({ akn: appConfig.aknTags.aknNamespace });

function parseDoc(xmlStringDoc) {
	if (!xmlStringDoc) {
		throw new Error('Not defined xml document on refiner constructor');
	}
	try {
		return new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
	} catch (err) {
		console.error(err);
		throw err;
	}
}

describe('#Translate akn paragraphs from json', () => {
	it('should markup paragraphs correctly', (done) => {
		nmt.translateDoc(jsonTestData1)
		.then((res) => {
			// console.log(res);
			const doc = parseDoc(res);
			var articles = select('//akn:article', doc);
			expect(articles.length).equal(9);

			// console.log(new xmldom.XMLSerializer().serializeToString(articles[8]));

			var para = select('./akn:paragraph', articles[0]);
			expect(para.length).equal(1);
			para = para[0];
			var num = select('./akn:num', para);
			var content = select('./akn:content', para);
			expect(num.length).equal(1);
			expect(num[0].textContent.trim()).equal('1.');
			expect(content.length).equal(1);
			expect(content[0].textContent.trim()).equal("Le norme del presente testo unico disciplinano l'emissione, gestione, ammissione a quotazione e negoziazione dei titoli di Stato.");

			para = select('./akn:paragraph', articles[1]);
			expect(para.length).equal(1);
			para = para[0];
			num = select('./akn:num', para);
			content = select('./akn:content', para);
			expect(num.length).equal(1);
			expect(num[0].textContent.trim()).equal('1.');
			expect(content.length).equal(0);
			var points = select('.//akn:point', para);
			expect(points.length).equal(23);
			var contentsP = select('.//akn:point//akn:content/akn:p[text()]', para);
			expect(contentsP.length).equal(23);

			para = select('./akn:paragraph', articles[2]);
			expect(para.length).equal(3);
			num = select('./akn:paragraph/akn:num', articles[2]);
			expect(num.length).equal(3);
			contentsP = select('./akn:paragraph//akn:content/akn:p[text()]', articles[2]);
			expect(num.length).equal(3);


			var numWithTags = select('./akn:num[./akn:*]', articles[3]);
			expect(numWithTags.length).equal(0);
			para = select('./akn:paragraph', articles[3]);
			expect(para.length).equal(6);
			num = select('./akn:paragraph/akn:num', articles[3]);
			expect(num.length).equal(6);
			contentsP = select('./akn:paragraph//akn:content/akn:p[text()]', articles[3]);
			expect(contentsP.length).equal(0);

			para = select('./akn:paragraph', articles[4]);
			expect(para.length).equal(1);
			num = select('./akn:paragraph/akn:num', articles[4]);
			expect(num.length).equal(1);
			contentsP = select('./akn:paragraph//akn:content', articles[4]);
			expect(contentsP.length).equal(0);

			para = select('./akn:paragraph', articles[5]);
			expect(para.length).equal(3);
			num = select('./akn:paragraph/akn:num', articles[5]);
			expect(num.length).equal(3);
			contentsP = select('./akn:paragraph//akn:content/akn:p[text()]', articles[5]);
			expect(contentsP.length).equal(0);

			para = select('./akn:paragraph', articles[6]);
			expect(para.length).equal(1);
			num = select('./akn:paragraph/akn:num', articles[6]);
			expect(num.length).equal(1);
			contentsP = select('./akn:paragraph//akn:content/akn:p', articles[6]);
			expect(contentsP.length).equal(1);
			expect(contentsP[0].textContent.trim().endsWith('))')).equal(true);

			para = select('./akn:paragraph', articles[7]);
			expect(para.length).equal(5);
			num = select('./akn:paragraph/akn:num', articles[7]);
			expect(num.length).equal(5);
			contentsP = select('./akn:paragraph/akn:content/akn:p', articles[7]);
			expect(contentsP.length).equal(3);
			var list = select('./akn:list', para[0]);
			expect(list.length).equal(1);
			points = select('./akn:point', list[0]);
			expect(points.length).equal(3);
			expect(select('./akn:point/akn:num', list[0]).length).equal(3);
			expect(select('./akn:point/akn:content/akn:p', list[0]).length).equal(3);

			list = select('./akn:list', para[1]);
			expect(list.length).equal(1);
			points = select('./akn:point', list[0]);
			expect(points.length).equal(3);
			expect(select('./akn:point/akn:num', list[0]).length).equal(3);
			expect(select('./akn:point/akn:content/akn:p', list[0]).length).equal(3);

			done();
		})
		.catch((err) => {
			console.error("Error:", err);
		});
	});
});

