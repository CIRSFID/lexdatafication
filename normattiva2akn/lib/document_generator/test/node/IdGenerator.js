
import { expect } from 'chai';
import generateIds from '../../src/imports/utils/IdGen.js';
import appConfig from '../../src/configs/configs.json';
import xpath from 'xpath';
import xmldom from 'xmldom';
import fs from 'fs';
import path from 'path';

const select = xpath.useNamespaces({ akn: appConfig.aknTags.aknNamespace });

function parseDoc(xmlStringDoc) {
	if (!xmlStringDoc) {
		throw new Error('Not defined xml document on refiner constructor');
	}
	try {
		return new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
	} catch (err) {
		console.error(err);
		throw err;
	}
}

function getFile(file) {
	return fs.readFileSync(
			path.resolve(__dirname, '../testData/', file),
			{ encoding: 'utf-8' });
}

const desideredIds = [
	'chp_I',
	'art_1',
	'art_1__para_1',
	'art_1__para_2',
	'art_1__para_2__point_a',
	'art_1__para_2__point_b',
	'art_1__para_2__point_c',
	'art_1__para_2-bis',
	'art_1__para_2-ter',
	'art_1__para_2-quater',
	'art_1__para_2-quinquies',
	'art_2',
	'art_2__para_1',
	'art_3',
	'art_3__para_1',
	'art_3-bis',
	'art_3-bis__para_1',
	'art_3-bis__para_2',
	'art_3-bis__para_3',
	'art_3-bis__para_3-bis',
	'art_3-bis__para_3-ter',
	'art_3-ter',
	'art_3-ter__para_1',
	'art_4',
	'art_4__para_1',
	'art_4__para_2',
	'art_5',
	'art_5__para_1',
	'art_5-bis',
	'art_5-bis__para_1',
	'art_5-bis__para_2',
	'chp_II',
	'art_6',
	'art_6__para_1',
	'art_6__para_7',
	'art_6__para_8',
	'art_6__para_9',
	'art_6__para_10',
	'art_6__para_11',
	'art_6__para_12',
	'chp_III',
	'art_7',
	'art_7__para_1',
	'art_7__para_2',
	'art_7__para_3',
	'art_7__para_4',
	'art_7__para_5',
	'art_7__para_6',
	'art_7__para_7',
	'art_7__para_8',
	'art_7__para_9',
	'chp_IV',
	'art_8',
	'art_8__para_1',
	'art_8__para_1__point_a',
	'art_8__para_1__point_a-bis',
	'art_8__para_1__point_b',
	'art_8__para_1__point_c',
	'art_8__para_1__point_c-bis',
	'art_8__para_2',
	'art_8__para_2-bis',
	'art_8__para_3',
	'art_8__para_4',
	'art_8__para_4-bis',
	'art_8__para_5',
	'art_8__para_6',
	'art_8__para_7',
	'art_8__para_8',
	'art_8__para_8-bis',
	'art_8__para_9',
	'art_8__para_9__point_a',
	'art_8__para_9__point_b',
	'art_8__para_9__point_c',
	'art_8__para_10',
	'art_9',
	'art_9__para_1',
	'art_9__para_2',
	'art_9__para_2__point_a',
	'art_9__para_2__point_b',
	'art_9__para_5',
	'art_9__para_5__point_e',
	'art_9__para_5__point_f',
	'art_9__para_3',
	'art_9__para_4',
	'art_10',
	'art_10__para_1',
	'art_10__para_2',
	'art_10__para_3',
	'art_10__para_3__point_a',
	'art_10__para_3__point_b',
	'art_10__para_3__point_c',
	'art_10__para_3__point_d',
	'art_10__para_4',
	'art_10__para_5',
	'art_10__para_5-bis',
	'art_10__para_5-ter',
	'art_10__para_5-quater',
	'art_11',
	'art_11__para_1',
	'art_11__para_2',
	'art_11__para_3',
	'art_11__para_4',
	'art_11-bis',
	'art_11-ter',
	'art_12',
	'art_12__para_1',
	'art_12-bis',
	'art_12-bis__para_1',
	'art_13',
	'art_13__para_1',
	'art_13__para_2',
	'art_13-bis',
	'art_13-bis__para_1',
	'art_13-ter',
	'art_13-ter__para_1',
	'art_13-ter__para_2',
	'art_14'
];

describe('#Generate akn eId', () => {
	it('should generate correct eId', (done) => {
		var doc = parseDoc(getFile('akoma_id_test.xml'));
		generateIds(doc);
		// console.log(new xmldom.XMLSerializer().serializeToString(doc))
		var hcontainers = select('//akn:*[./akn:num]', doc);
		hcontainers.forEach((node, i) => {
			var eId = node.getAttribute('eId');
			expect(eId).equal(desideredIds[i]);
		})
		done();
	});

	it('shouldn\'t generate eId for formula', (done) => {
		var doc = parseDoc(getFile('akoma_id_conclusionspreface.xml'));
		generateIds(doc);
		// console.log(new xmldom.XMLSerializer().serializeToString(doc))
		var formula = select('//akn:formula[@eId]', doc);
		expect(formula.length).equal(0);
		done();
	});
});

