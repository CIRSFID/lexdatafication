
import { expect } from 'chai';
import nmt from '../../src/api/nmt.api.js';
import appConfig from '../../src/configs/configs.json';
import jsonTestData1 from '../testData/paragraph-disappear.json';
import xpath from 'xpath';
import xmldom from 'xmldom';

const select = xpath.useNamespaces({ akn: appConfig.aknTags.aknNamespace });

function parseDoc(xmlStringDoc) {
	if (!xmlStringDoc) {
		throw new Error('Not defined xml document on refiner constructor');
	}
	try {
		return new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
	} catch (err) {
		console.error(err);
		throw err;
	}
}

describe('#Translate akn from json', () => {
	it('should convert json to akn', (done) => {
		nmt.translateDoc(jsonTestData1)
		.then((res) => {
			// console.log(res);
			const doc = parseDoc(res);
			var para = select('//akn:*[@eId="art_3__para_1"]', doc);
			expect(para).not.to.be.empty;
			para = select('//akn:*[@eId="art_5__para_1"]', doc);
			expect(para).not.to.be.empty;
			console.log(para);
			done();
		})
		.catch((err) => {
			console.error("Error:", err);
		});
	});
});

