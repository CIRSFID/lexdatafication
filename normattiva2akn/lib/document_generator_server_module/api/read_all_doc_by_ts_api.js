import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';
import path from 'path';
import async from 'async';
import readFsUtils from '../utils/read_fs_utils';

const router = express.Router();
const uploadDir = process.env.UPLOAD_DIR || path.join(__dirname, '../uploads');

router.get('/:timestamp', (request, response) => {
    console.log("-> ", request.params.timestamp);
    //read fileList.json first
    let tsDir = path.join(uploadDir, '/' + request.params.timestamp);
    let fileListPath = path.join(tsDir, '/fileList.json'); 
    let fileList = [];  
    let resContent = [];
    let toConvert = [];     
    if (fs.existsSync(tsDir) && fs.existsSync(fileListPath)) {
        //read fileList.json
        fs.readFile(fileListPath, (err, data) => {
            if (err){
                console.log(err);
                response.status(500);
                return response.end('Errore lettura lista file!');
            }
            try {
                fileList = JSON.parse(data).content;
                //read files on directory to see which file are stored on filesystem
                fs.readdir(tsDir, function (err, filenames) {
                    if (err) {
                        console.log(err);
                        response.status(500);
                        return response.end('Errore lettura lista file!');                        
                    }                
                    fileList.forEach((el) => {
                        //if file are on fs are already translated and validated
                        if(filenames.indexOf(el + '.xml') !== -1 
                            && filenames.indexOf(el + '.validation.json') !== -1 ){
                            resContent.push({
                                name: el,
                                validPath: path.join(tsDir, '/' + el + '.validation.json'),
                                status: 'TRANSLATED',
                                validationPath: path.join(request.params.timestamp, '/' + el + '.validation.json'),
                                xmlPath: path.join(request.params.timestamp, '/' + el + '.xml')
                            });
                        } else {
                            toConvert.push({
                                name: el,
                                status: 'CONVERTING'                                
                            });
                        }
                    });
                    async.mapSeries(resContent, readFsUtils.readFilesVLD_FromDisk, (err, content) => {
                        if(err){
                            console.log(err);
                            response.status(500);
                            return response.end('Errore lettura lista file!');                        
                        }                                                
                        response.status(200);
                        return response.end(JSON.stringify({ content: content.concat(toConvert) }));                                        
                    });
                    
                });
            } catch(err){
                //send error responce
                response.status(500);
                return response.end('Errore lettura lista file!');
            }            
        });
    } else {
        //send error
        response.status(500);
        return response.end('Errore lettura lista file!');
    }
});	

exports.router = router;
