import fs from 'fs';
import path from 'path';
import jsonfile from 'jsonfile';
import express from 'express';
import bodyParser from 'body-parser';
import ipzsBulk from 'lib/ipzs-api';
import conversionUtils from '../utils/convertion_utils';

const router = express.Router();
const uploadDir = process.env.UPLOAD_DIR || path.join(__dirname, '../uploads');

router.get('/:numRed/:docDate/:vigDate?', (request, response) => {
    console.log("-> ", request.params.vigDate);
    let originalDoc = false;
    if (!request.params.vigDate){
        originalDoc = true;
    }
    ipzsBulk.downloadDoc(request.params.docDate, 
        request.params.numRed, 
        request.params.vigDate, 
        originalDoc, 
        (err, res) => {
            if (err) {
                console.log(err);
                response.status(500);
                return response.end('Errore ricezione documento');
            }
            try {
                if (res.errore) {
                    response.status(500);
                    response.end(res.errore);
                    return;
                }                
                const resFiles = [];
                const fileData = res;
                const fileNameNoExt = request.params.numRed;
                let currentTs = new Date().getTime().toString();
                let currentUploadDir = path.join(uploadDir, currentTs);
                let fileList = [];
                fs.mkdir(currentUploadDir, (err) => {
                    if (err) {
                        response.status(500);
                        return response.end('Errore creazione directory!');
                    }
                    //prepare fileList.json content and add the current upload dir to the zip file
                    fileList.push(fileNameNoExt);
                    //write fileList.json
                    jsonfile.writeFile(currentUploadDir + '/fileList.json', { content: fileList }, (err) => {
                        if (err) {
                            response.status(500);
                            return response.end('Errore creazione fileList.json!');
                        }
                        response.status(200);
                        response.end(JSON.stringify({ ts: currentTs.toString() }));
                        //here starts the async operation, and the responce is retured to the server
                        //start to convert and validate every single file    
                        conversionUtils.convertValidateLocalAndStore({
                            fileName: fileNameNoExt,
                            jsonContent: fileData,
                            uploadDir: currentUploadDir
                        }, (err, res) => {
                            if (err) return console.log(err);
                            console.log("Single file convertion end");
                            return;
                        });
                    });
                });               
            } catch (err) {
                console.log(err);
                response.status(500);
                return response.end('Errore traduzione file');
            }
    });
});	

exports.router = router;
