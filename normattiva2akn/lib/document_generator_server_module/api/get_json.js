import fs from 'fs';
import path from 'path';
import jsonfile from 'jsonfile';
import express from 'express';
import bodyParser from 'body-parser';
import ipzsBulk from 'lib/ipzs-api';
import conversionUtils from '../utils/convertion_utils';

const router = express.Router();
const uploadDir = process.env.UPLOAD_DIR || path.join(__dirname, '../uploads');

router.get('/:numRed/:docDate/:vigDate?', (request, response) => {    
    let originalDoc = false;
    if (!request.params.vigDate){
        originalDoc = true;
    }
    ipzsBulk.downloadDoc(request.params.docDate, 
        request.params.numRed, 
        request.params.vigDate, 
        originalDoc, 
        (err, res) => {
            if (err) {
                console.log(err);
                response.status(500);
                return response.end('Errore ricezione documento');
            }                            
        response.status(200);
        response.end(JSON.stringify(res));                
    });
});	

exports.router = router;
