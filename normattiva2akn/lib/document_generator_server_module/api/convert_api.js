
import express from 'express';
import bodyParser from 'body-parser';
import nmt from 'lib/document_generator';

const router = express.Router();
const jsonParser = bodyParser.json({ limit: '50mb' });

//Single json convertion directly from json with field "content"
router.post('/', jsonParser, (request, response) => {
    if (!request.body.content) {
        throw new Error('Invalid request, empty fields');
    }
    nmt.translateDoc(request.body.content)
        .then((res) => {
            response.end(res);
        })
        .catch((err) => {
            response.end(err);
        });
});

exports.router = router;
