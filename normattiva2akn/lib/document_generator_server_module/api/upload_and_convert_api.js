import express from 'express';
import fs from 'fs';
import path from 'path';
import async from 'async';
import multer from 'multer';
import promiseSequential from 'promise-sequential';
import http from 'http';
import jsonfile from 'jsonfile';
import zipUtils from '../utils/zip_utils';
import conversionUtils from '../utils/convertion_utils';

const router = express.Router();
const uploadDir = process.env.UPLOAD_DIR || path.join(__dirname, '../uploads');

//multipart upload middleware
const fileFilterHandler = (req, file, cb) => {
    if (file && file.originalname.split('.').length) {
        const fileExtension = file.originalname.split('.')[file.originalname.split('.').length - 1];
        const allowedExt = ['json', 'zip'];
        const allowedMymeTypes = [
            'application/json',
            'application/javascript',
            'application/zip',
            'application/x-zip-compressed',
            'application/octet-stream'];
        if (allowedExt.indexOf(fileExtension.toLowerCase()) !== -1 && allowedMymeTypes.indexOf(file.mimetype) !== -1) {
            cb(null, true);
        } else {
            cb('Invalid file type');
        }
    } else {
        cb(null, true);
    }
};
const multerUpload = multer({ fileFilter: fileFilterHandler });

//File json or zip request with conversion
router.post('/', multerUpload.single('file'), (request, response) => {
    if (request.file.mimetype === 'application/zip' ||
        request.file.mimetype === 'application/x-zip-compressed') {
        zipUtils.createFilesFromZip(request.file.buffer, (err, unzippedEntries) => {
            if (err) {
                throw err;
            }
            const promiseConversionArray = [], validateContentsPromiseArray = [], fileList = [];
            //create directory timestamp
            let currentTs = new Date().getTime().toString();
            let currentUploadDir = path.join(uploadDir, currentTs);             
            fs.mkdir(currentUploadDir, (err) => {
                if (err) {                    
                    response.status(500);
                    return response.end('Errore creazione directory!');
                }
                //prepare fileList.json content and add the current upload dir to the zip file
                unzippedEntries = unzippedEntries.map((entry) => {
                    //takes only filename without path              
                    let filename = entry.fileName.split('/')[entry.fileName.split('/').length - 1];
                    //remove extention
                    let fileNameNoExtArray = filename.split('.');
                    let fileNameNoExt;
                    fileNameNoExtArray.pop();
                    fileNameNoExt = fileNameNoExtArray.join('');
                    fileList.push(fileNameNoExt);
                    return {
                        fileName: fileNameNoExt,
                        jsonContent: entry.jsonContent,
                        uploadDir: currentUploadDir
                    }
                });
                //write fileList.json
                jsonfile.writeFile(currentUploadDir + '/fileList.json', { content: fileList }, (err) => {
                    if (err) {
                        response.status(500);
                        return response.end('Errore creazione fileList.json!');
                    }    
                    response.status(200);                                
                    response.end(JSON.stringify({ts: currentTs.toString()}));
                    //here starts the async operation, and the responce is retured to the server
                    //start to convert and validate every single file    
                    async.mapSeries(unzippedEntries, conversionUtils.convertValidateLocalAndStore, (err, content) => {
                        if (err) {
                            console.log(err);                         
                        }
                        console.log("Zip conversion ended!");
                        return;
                    });                    
                });
                
            });            
        });
    } else {
        try {
            const resFiles = [];
            const fileData = JSON.parse(request.file.buffer.toString('utf8'));
            const fileNameNoExt = request.file.originalname.substr(0, request.file.originalname.lastIndexOf('.'))
                || request.file.originalname;              
            let currentTs = new Date().getTime().toString();
            let currentUploadDir = path.join(uploadDir, currentTs);           
            let fileList = [];            
            fs.mkdir(currentUploadDir, (err) => {
                if (err) {
                    response.status(500);
                    return response.end('Errore creazione directory!');
                }
                //prepare fileList.json content and add the current upload dir to the zip file
                fileList.push(fileNameNoExt);
                //write fileList.json
                jsonfile.writeFile(currentUploadDir + '/fileList.json', { content: fileList }, (err) => {
                    if (err) {
                        response.status(500);
                        return response.end('Errore creazione fileList.json!');
                    }
                    response.status(200);
                    response.end(JSON.stringify({ ts: currentTs.toString() }));
                    //here starts the async operation, and the responce is retured to the server
                    //start to convert and validate every single file    
                    conversionUtils.convertValidateLocalAndStore({
                        fileName: fileNameNoExt,
                        jsonContent: fileData,
                        uploadDir: currentUploadDir
                    }, (err, res) => {
                        if(err) return console.log(err);
                        console.log("Single file convertion end");
                        return;
                    });
                });

            });
        } catch (err) {
            response.status(500);
            return response.end('Errore traduzione file');
        }
    }
});

exports.router = router;
