import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';
import path from 'path';

const router = express.Router();
const uploadDir = process.env.UPLOAD_DIR || path.join(__dirname, '../uploads');
const jsonParser = bodyParser.json({ limit: '50mb' });

router.post('/', jsonParser, (request, response) => {
    if (!request.body.content || !request.body.content.path) {
        response.status(500);
        return response.end('Invalid request!');
    }
    try {
        let pathToFile = path.join(uploadDir, request.body.content.path);
        let stat = fs.statSync(pathToFile);
        response.writeHead(200, {
            'Content-Type': 'text/html',
            'Content-Length': stat.size
        });
        let readStream = fs.createReadStream(pathToFile);        
        readStream.pipe(response);
    } catch(err) {
        console.log(err);
        response.status(500);
        return response.end('Internal error: stat file error');
    }    
});	

exports.router = router;
