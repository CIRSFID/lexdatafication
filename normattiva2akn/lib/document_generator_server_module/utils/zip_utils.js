import streamifier from 'streamifier';
import unzip from 'unzipper';

const createFilesFromZip = (zipFileBuffer, cb) => {
    const fileArray = [];
    streamifier.createReadStream(zipFileBuffer).pipe(unzip.Parse())
        .on('entry', (entry) => {
            const fileName = entry.path;            
            const type = entry.type;
            const size = entry.size;
            const fileExtension = fileName.split('.')[fileName.split('.').length - 1];
            if (type === 'Directory' || fileExtension !== 'json') {
                entry.autodrain();
            } else {
                let responseData = '';
                entry.on('data', (chunk) => {
                    responseData += chunk;
                });
                entry.on('end', () => {
                    try {
                        fileArray.push({ fileName, jsonContent: JSON.parse(responseData) });
                    } catch (error) {
                        //TO-DO: gestione file non valido (json parse)
                        console.log(error);
                        console.log(fileName);                        
                    }
                });
                entry.on('error', (error) => {
                    console.log("->",error);                    
                    cb(error);
                });
            }
        })
        .on('close', () => {
            cb(null, fileArray);
        })
        .on('error', (error) => {
            cb(error);
        });
};

exports.createFilesFromZip = createFilesFromZip;
