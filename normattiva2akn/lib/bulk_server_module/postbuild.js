var fs = require('fs');
var path = require('path');

var distFileName = 'server.api.min.js';
var distPath = '../dist/server_module/' + distFileName;
var srcPath = '/dist/' + distFileName;

function copyFile(source, target, cb) {
	var cbCalled = false;

	var rd = fs.createReadStream(source);
	rd.on("error", function (err) {
		done(err);
	});
	var wr = fs.createWriteStream(target);
	wr.on("error", function (err) {
		done(err);
	});
	wr.on("close", function (ex) {
		done();
	});
	rd.pipe(wr);

	function done(err) {
		if (!cbCalled) {
			cb(err);
			cbCalled = true;
		}
	}
}
console.log("Coping to dist folder...");
copyFile(path.join(__dirname, srcPath), path.join(__dirname, distPath), function () {
	console.log("Created shared dist build!");
});
