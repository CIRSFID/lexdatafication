import fs from 'fs';
import async from 'async';
import ProgressBar from 'progress'
import archiver from 'archiver';

import serverUtils from '../imports/utils/ServerUtils';
import nmt from 'lib/document_generator';
import ipzs from 'lib/ipzs-api';

const doc = {
	docType: "LEGGE",
	docDate: "2004-02-19",
	docNum: "40"
}

var output = fs.createWriteStream(__dirname + `/${ doc.docType }_${ doc.docDate }_${ doc.docNum }.zip`);
var aknPackage = archiver('zip', {
	zlib: { level: 9 }
});
aknPackage.on('warning', function(err) {
	if (err.code === 'ENOENT') {
		console.warn(err);
	} else {
		throw err;
	}
});
aknPackage.on('error', function(err) {
	throw err;
});

aknPackage.pipe(output);

let progressBar = null;
ipzs.downloadCatena(doc.docType, doc.docDate, doc.docNum, null, function(err, docs) {
	if (err) {
		console.log(err);
		return;
	}
	const currentTs = new Date().getTime();
	progressBar = new ProgressBar('Converting [:bar] :current/:total :percent :elapsed s\n',
						{ total: docs.length });
	console.log('Tot docs: ', docs.length, currentTs);
	async.mapSeries(docs, (docData, doneCb) => {
		async.waterfall([
			convertJSONtoAKN.bind(null, docData.doc),
			validateAKN,
			saveToZip.bind(null, docData.versionDate),
			(doc, cb) => {
				progressBar.tick();
				cb(null, doc);
			}
		], doneCb);
	}, (err, docs) => {
		if (err) {
			console.log(err);
			return;
		}
		aknPackage.finalize();
		console.log("Processed: ", docs.length, "docs!");
		console.log(docs);
		console.log("Catena conversion end");
	});
});

function convertJSONtoAKN(doc, cb) {
	try {
		nmt.translateDoc(JSON.parse(JSON.stringify(doc)))
		.then((aknString) => {
				cb(null, doc, aknString);
			})
			.catch((err) => {
				console.error(err);
				cb(null, doc, null);
			});
	} catch (err) {
		console.error(err);
		cb(null, doc, null);
	}
}

function validateAKN(doc, akn, cb) {
	if (!akn) {
		return cb(null, doc, null, null);
	} else {
		serverUtils.validateAknCb(akn, (err, validationResult) => {
			if (err) {
				return cb(null, doc, akn, null);
			}
			cb(null, doc, akn, validationResult);
		});
	}
}

function saveToZip(versionDate, doc, akn, validationResult, cb) {
	const basePath = `${ doc.metadati.tipoDoc }_${ doc.metadati.dataDoc }_${ doc.metadati.numDoc }_${ versionDate }`
	aknPackage.append(JSON.stringify(doc, null, 4), { name: basePath+'.json' });
	aknPackage.append(validationResult, { name: basePath+'_validation.json' });
	aknPackage.append(akn, { name: basePath+'.xml' });
	cb(null, basePath);
}
