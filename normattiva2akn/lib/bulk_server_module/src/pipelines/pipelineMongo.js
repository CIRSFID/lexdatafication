import async from 'async';
import ProgressBar from 'progress'
import mongoose from 'mongoose';

import serverUtils from '../imports/utils/ServerUtils';
import nmt from 'lib/document_generator';
import ipzs from 'lib/ipzs-api';
import {getUri, existPut} from './utils'

const dbUrl = 'mongodb://137.204.140.124:27017/ipzs';
const startDate = '2018-01-01';

mongoose.connect(dbUrl);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
let docs = db.collection('docs');

const query = {"metadati.dataDoc": { $gte: startDate }};

let progressBar = null;

mongoCount(query, (err, count) => {
	if (err) {
		return console.error(err);
	}
	progressBar = new ProgressBar('Processing [:bar] :current/:total :percent :elapsed s\n',
									{ total: count });
	console.log('Tot docs: ', count);
	mongoFind(query, (err, doc, cb) => {
		if (err) {
			console.error('mongo find, ', err);
		}
		pipelineOneDoc(doc, () => {
			progressBar.tick();
			cb();
		});
	}, () => {
		console.log('Finish!!');
	});
});

function mongoCount(query, cb) {
	db.once('open', function() {
		docs.countDocuments(query, (err, nr) => {
			if (err) {
				return cb(err);
			}
			cb(null, nr);
		});
	});
}

function mongoFind(query, everyDocCb, cb) {
	// db.once('open', function() {
	docs.find(query, (err, cursor) => {
		if (err) {
			return cb(err);
		}
		function iterFunction(err, doc) {
			if (err) {
				return everyDocCb(err, null, cursor.next.bind(cursor, iterFunction));
			}
			if (doc) {
				everyDocCb(err, doc, cursor.next.bind(cursor, iterFunction));
			} else {
				mongoose.connection.close();
				cb();
			}
		}
		cursor.next(iterFunction);
	});
	// });
}

function pipelineOneDoc(doc, doneCb) {
	async.waterfall([
		convertJSONtoAKN.bind(null, doc),
		validateAKNLocal,
		uploadToExist,
	], doneCb);
}

function convertJSONtoAKN(doc, cb) {
	try {
		nmt.translateDoc(JSON.parse(JSON.stringify(doc)))
		.then((aknString) => {
				cb(null, doc, aknString);
			})
			.catch((err) => {
				console.error(err);
				cb(null, doc, null);
			});
	} catch (err) {
		console.error(err);
		cb(null, doc, null);
	}
}

function validateAKNLocal(doc, akn, cb) {
	if (!akn) {
		return cb(null, doc, null, null);
	} else {
		serverUtils.validateAknLocalCb(akn, (err, validationResult) => {
			if (err) {
				return cb(null, doc, akn, null);
			}
			cb(null, doc, akn, validationResult);
		});
	}
}

function uploadToExist(docJSON, akn, validationResult, cb) {
	// console.log('uploadto->', docJSON, akn, validationResult);
	if (!akn) {
		console.log("Errors in doc translation: ", docJSON);
		return cb(null, docJSON);
	}
	const uri = getUri(akn);
	if (!uri) {
		console.log("Cannot get uri for ", akn.substring(0, 200));
		return cb(null, docJSON);
	}
	var baseUri = uri.substring(0, uri.lastIndexOf('/'));
	var component = uri.substring(baseUri.length+1).replace('!', '');
	async.series([
		existPut.bind(null, akn, 'application/xml', baseUri, component),
		// existPut.bind(null, JSON.stringify(docJSON), 'application/json', baseUri, 'source.json'),
		existPut.bind(null, validationResult, 'application/json', baseUri, 'validation.json')
	],
	function(err, results) {
		cb(null, {
			pathAkn: results[0],
			pathSource: results[1],
			pathValidation: results[2]
		});
	});
}
