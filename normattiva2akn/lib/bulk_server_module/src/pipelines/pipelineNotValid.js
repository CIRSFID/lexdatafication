import async from 'async';
import { DOMParser } from 'xmldom'
import xpath from 'xpath'
import ProgressBar from 'progress'

import {existGet, existPut} from './utils'
import serverUtils from '../imports/utils/ServerUtils';
import nmt from 'lib/document_generator';

let progressBar = null;
getNotValidDocs((err, res) => {
	if (err) {
		console.log(err);
		return;
	}
	const currentTs = new Date().getTime();
	progressBar = new ProgressBar('Uploading [:bar] :current/:total :percent :elapsed s\n', { total: res.length });
	console.log('Tot docs: ', res.length, currentTs);
	async.mapSeries(res, (uri, doneCb) => {
		async.waterfall([
			downloadSourceJSON.bind(null, uri),
			convertJSONtoAKN,
			validateAKN,
			uploadToExist,
			(doc, cb) => {
				progressBar.tick();
				cb(null, doc);
			}
		], doneCb);
	}, (err, docs) => {
		if (err) {
			console.log(err);
			return;
		}
		console.log("Processed: ", docs.length, "docs!");
		console.log(docs);
		console.log("Bulk conversion end");
	});
});

function getNotValidDocs(cb) {
	existGet('/db/normattiva/getNotValidDocs.xq', function(err, data) {
		if (err) {
			return cb(null, {});
		}
		var uris = JSON.parse(data).uri;
		filterNotExistent(uris, function(err, uris) {
			cb(null, uris);
		});
	});
}

function filterNotExistent(uris, cb) {
	async.filter(uris, function(uri, cb) {
		uri = uri.replace('/main', '/source.json')
				.replace('/db/normattiva_testpoldo/normattiva/',
							'/db/normattiva_testpoldo_nomods/akn/');
		existGet(uri, function(err, data) {
			if (err || !data) {
				return cb(null, true);
			}
			return cb(null, false);
		});
	}, function(err, res) {
		cb(null, res.filter((uri) => !!uri));
	});
}

function downloadSourceJSON(docPath, cb) {
	docPath = docPath.replace('/main', '/source.json');
	existGet(docPath, function(err, data) {
		if (err) {
			return cb(null, {});
		}
		cb(null, JSON.parse(data));
	});
}

function convertJSONtoAKN(doc, cb) {
	try {
		nmt.translateDoc(JSON.parse(JSON.stringify(doc)), null, {disableMods: true})
		.then((aknString) => {
				cb(null, doc, aknString);
			})
			.catch((err) => {
				console.error(err);
				cb(null, doc, null);
			});
	} catch (err) {
		console.error(err);
		cb(null, doc, null);
	}
}

function validateAKN(doc, akn, cb) {
	if (!akn) {
		return cb(null, doc, null, null);
	} else {
		serverUtils.validateAknCb(akn, (err, validationResult) => {
			if (err) {
				return cb(null, doc, akn, null);
			}
			cb(null, doc, akn, validationResult);
		});
	}
}

function uploadToExist(docJSON, akn, validationResult, cb) {
	// console.log('uploadto->', docJSON, akn, validationResult);
	if (!akn) {
		console.log("Errors in doc translation: ", docJSON);
		return cb(null, docJSON);
	}
	const uri = getUri(akn);
	if (!uri) {
		console.log("Cannot get uri for ", akn.substring(0, 200));
		return cb(null, docJSON);
	}
	var baseUri = uri.substring(0, uri.lastIndexOf('/'));
	var component = uri.substring(baseUri.length+1).replace('!', '');
	async.series([
		existPut.bind(null, akn, 'application/xml', baseUri, component),
		existPut.bind(null, JSON.stringify(docJSON), 'application/json', baseUri, 'source.json'),
		existPut.bind(null, validationResult, 'application/json', baseUri, 'validation.json')
	],
	function(err, results) {
		cb(null, {
			pathAkn: results[0],
			pathSource: results[1],
			pathValidation: results[2]
		});
	});
}

function getUri(aknString) {
	try {
		var doc = new DOMParser().parseFromString(aknString);
		var select = xpath.useNamespaces({
			"akn": "http://docs.oasis-open.org/legaldocml/ns/akn/3.0"
		})
		var node = select('//akn:FRBRExpression/akn:FRBRthis/@value', doc)[0];
		return node && node.textContent;
	}
	catch (e) {
		console.error(e);
		return false;
	}
}
