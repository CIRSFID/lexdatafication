import async from 'async';
import ProgressBar from 'progress';
import serverUtils from '../imports/utils/ServerUtils';
import nmt from 'lib/document_generator';
import ipzs from 'lib/ipzs-api';
import { getUri, existPut } from './utils';
import glob from 'glob';
import path from 'path';
import fs from 'fs';

const startDate = '2018-01-01';
const docDir = 'fs_docs';
let progressBar = null;
const currentDir = path.join(__dirname, docDir);

getFileLisrtFromFs(currentDir, (err, fileList) => {
	if (err) {
		return console.log(err);
	}
	let totFile = fileList.length;
	progressBar = new ProgressBar('Processing [:bar] :current/:total :percent :elapsed s\n',
		{ total: totFile });
	console.log('Tot docs: ', totFile);
	async.mapSeries(fileList, pipelineOneDoc, (err, content) => {
		if (err) {
			console.log(err);
		}
		console.log('Finished');
	});

});

function getFileLisrtFromFs(dirname, cb) {
	glob('**/*.json', {
		cwd: dirname
	}, (err, files) => {
		if(err) {
			return cb(err);
		}
		cb(null, files.map((file) => {
			return path.join(dirname, file);
		}));
	});
}

function pipelineOneDoc(docPath, doneCb) {
	async.waterfall([
		readFileFromFs.bind(null, docPath),
		convertJSONtoAKN,
		validateAKNLocal,
		uploadToExist,
		(doc, cb) => {
			progressBar.tick();
			cb(null, doc);
		}
	], doneCb);
}

function readFileFromFs(path, cb) {
	fs.readFile(path, 'utf8', (err, data) => {
		if (err) {
			cb(null, path, null);
		}
		try {
			let jsonDocObj = JSON.parse(data);
			cb(null, path, jsonDocObj);
		} catch (err) {
			cb(null, path);
		}
	});
}

function convertJSONtoAKN(path, doc, cb) {
	if (!doc) {
		console.error('Missing file, ', path);
		return cb(null, null, null);
	}
	console.log('Converting, ', path);
	try {
		nmt.translateDoc(JSON.parse(JSON.stringify(doc)))
			.then((aknString) => {
				cb(null, doc, aknString);
			})
			.catch((err) => {
				console.error(err);
				cb(null, doc, null);
			});
	} catch (err) {
		console.error(err);
		cb(null, doc, null);
	}
}

function validateAKNLocal(doc, akn, cb) {
	if (!akn) {
		return cb(null, doc, null, null);
	}
		serverUtils.validateAknLocalCb(akn, (err, validationResult) => {
			if (err) {
				return cb(null, doc, akn, null);
			}
			cb(null, doc, akn, validationResult);
		});

}

function uploadToExist(docJSON, akn, validationResult, cb) {
	// console.log('uploadto->', docJSON, akn, validationResult);
	if (!akn) {
		console.log('Errors in doc translation: ', docJSON);
		return cb(null, docJSON);
	}
	const uri = getUri(akn);
	if (!uri) {
		console.log('Cannot get uri for ', akn.substring(0, 200));
		return cb(null, docJSON);
	}
	let baseUri = uri.substring(0, uri.lastIndexOf('/'));
	let component = uri.substring(baseUri.length + 1).replace('!', '');
	async.series(
[
		existPut.bind(null, akn, 'application/xml', baseUri, component),
		// existPut.bind(null, JSON.stringify(docJSON), 'application/json', baseUri, 'source.json'),
		existPut.bind(null, validationResult, 'application/json', baseUri, 'validation.json')
	],
	(err, results) => {
		cb(null, {
			pathAkn: results[0],
			pathSource: results[1],
			pathValidation: results[2]
		});
	}
);
}
