import fs from 'fs';
import async from 'async';
import ProgressBar from 'progress'
import archiver from 'archiver';

import {existGet} from './utils'


let progressBar = null;

var output = fs.createWriteStream(__dirname + '/aknSample.zip');
var aknPackage = archiver('zip', {
	zlib: { level: 9 }
});
aknPackage.on('warning', function(err) {
	if (err.code === 'ENOENT') {
		console.warn(err);
	} else {
		throw err;
	}
});
aknPackage.on('error', function(err) {
	throw err;
});

aknPackage.pipe(output);

downloadSample((err, uris) => {
	const currentTs = new Date().getTime();
	progressBar = new ProgressBar('Downloading [:bar] :current/:total :percent :elapsed s\n',
							{ total: uris.length });
	console.log('Tot docs: ', uris.length, currentTs);
	async.mapSeries(uris, (data, doneCb) => {
		async.waterfall([
			downloadFromExist.bind(null, data),
			saveToZip,
			(doc, cb) => {
				progressBar.tick();
				cb(null, doc);
			}
		], doneCb);
	}, (err, docs) => {
		if (err) {
			console.log(err);
			return;
		}
		aknPackage.finalize();
		console.log("Processed: ", docs.length, "docs!");
		console.log(docs);
		console.log("Bulk conversion end");
	});
});

function downloadFromExist(config, cb) {
	existGet(config.uri, function(err, data) {
		if (err) {
			return cb(null, {});
		}
		cb(null, config.zipPath, data);
	});
}

function downloadSample(cb) {
	existGet('/db/normattiva/getDocumentsSampleLuca.xql', function(err, data) {
		if (err) {
			return cb(null, {});
		}
		cb(null, createDocList(JSON.parse(data).docs));
	});
}

function createDocList(docs) {
	let docList = [];
	docs.forEach((docXYear) => {
		forceArray(docXYear.docs).forEach((docXmonth) => {
			forceArray(docXmonth.docs).forEach((docXtype) => {
				forceArray(docXtype.docs).forEach((docXvalid) => {
					const validStr = (docXvalid.valid == 'true') ? 'valid' : 'notvalid';
					forceArray(docXvalid.uri).forEach((uri) => {
						let filePath = uri.split('/').slice(8).join('_');
						filePath = ['', docXYear.year, docXmonth.month,
											docXtype.docType, validStr, filePath]
										.join('/');
						docList.push({zipPath: filePath, uri:uri});
						if (docXvalid.valid == 'false') {
							docList.push({
								zipPath: filePath.replace('_main', '_validation.json'),
								uri:uri.replace('/main', '/validation.json')
							});
						}
					});
				});
			});
		});
	});
	// console.log(JSON.stringify(docList));
	return docList;
}

function forceArray(el) {
    if (!el || el == 'null') return [];
    if (Array.isArray(el)) return el;
    return [el];
}

function saveToZip(path, content, cb) {
	aknPackage.append(content, { name: path });
	cb(null, path);
}
