import fs from 'fs';
import async from 'async';
import ProgressBar from 'progress'
import archiver from 'archiver';

import {existGet} from './utils'


let progressBar = null;

var output = fs.createWriteStream(__dirname + '/aknSample.zip');
var aknPackage = archiver('zip', {
	zlib: { level: 9 }
});
aknPackage.on('warning', function(err) {
	if (err.code === 'ENOENT') {
		console.warn(err);
	} else {
		throw err;
	}
});
aknPackage.on('error', function(err) {
	throw err;
});

aknPackage.pipe(output);

downloadSample((err, uris) => {
	const currentTs = new Date().getTime();
	progressBar = new ProgressBar('Downloading [:bar] :current/:total :percent :elapsed s\n',
							{ total: uris.length });
	console.log('Tot docs: ', uris.length, currentTs);
	async.mapSeries(uris, (uri, doneCb) => {
		async.waterfall([
			downloadAkn.bind(null, uri),
			saveToZip,
			(doc, cb) => {
				progressBar.tick();
				cb(null, doc);
			}
		], doneCb);
	}, (err, docs) => {
		if (err) {
			console.log(err);
			return;
		}
		aknPackage.finalize();
		console.log("Processed: ", docs.length, "docs!");
		console.log(docs);
		console.log("Bulk conversion end");
	});
});

function downloadAkn(docPath, cb) {
	existGet(docPath, function(err, data) {
		if (err) {
			return cb(null, {});
		}
		cb(null, docPath, data);
	});
}

function downloadSample(cb) {
	existGet('/db/normattiva/getDocumentsSample.xql', function(err, data) {
		if (err) {
			return cb(null, {});
		}
		cb(null, JSON.parse(data).uri);
	});
}

function saveToZip(path, content, cb) {
	aknPackage.append(content, { name: path });
	cb(null, path);
}
