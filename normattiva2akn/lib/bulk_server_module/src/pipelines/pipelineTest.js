import async from 'async';
import ProgressBar from 'progress'

import serverUtils from '../imports/utils/ServerUtils';
import nmt from 'lib/document_generator';
import {getUri, existPut, existGet} from './utils'

import { docList } from './docList';


let progressBar = null;

const docListTest = docList;

const currentTs = new Date().getTime();
progressBar = new ProgressBar('Uploading [:bar] :current/:total :percent :elapsed s\n',
						{ total: docListTest.length });
console.log('Tot docs: ', docListTest.length, currentTs);
async.mapSeries(docListTest, (uri, doneCb) => {
	async.waterfall([
		downloadExistJSON.bind(null, uri),
		convertJSONtoAKN,
		validateAKN,
		uploadToExist,
		(doc, cb) => {
			progressBar.tick();
			cb(null, doc);
		}
	], doneCb);
}, (err, docs) => {
	if (err) {
		console.log(err);
		return;
	}
	console.log("Processed: ", docs.length, "docs!");
	console.log(docs);
	console.log("Bulk conversion end");
});

function downloadExistJSON(docPath, cb) {
	existGet(docPath, function(err, data) {
		if (err) {
			return cb(null, {});
		}
		cb(null, JSON.parse(data));
	});
}

function convertJSONtoAKN(doc, cb) {
	try {
		nmt.translateDoc(JSON.parse(JSON.stringify(doc)))
		.then((aknString) => {
				cb(null, doc, aknString);
			})
			.catch((err) => {
				console.error(err);
				cb(null, doc, null);
			});
	} catch (err) {
		console.error(err);
		cb(null, doc, null);
	}
}

function validateAKN(doc, akn, cb) {
	if (!akn) {
		return cb(null, doc, null, null);
	} else {
		serverUtils.validateAknCb(akn, (err, validationResult) => {
			if (err) {
				return cb(null, doc, akn, null);
			}
			cb(null, doc, akn, validationResult);
		});
	}
}

function uploadToExist(docJSON, akn, validationResult, cb) {
	// console.log('uploadto->', docJSON, akn, validationResult);
	if (!akn) {
		console.log("Errors in doc translation: ", docJSON);
		return cb(null, docJSON);
	}
	const uri = getUri(akn);
	if (!uri) {
		console.log("Cannot get uri for ", akn.substring(0, 200));
		return cb(null, docJSON);
	}
	var baseUri = uri.substring(0, uri.lastIndexOf('/'));
	var component = uri.substring(baseUri.length+1).replace('!', '');
	async.series([
		existPut.bind(null, akn, 'application/xml', baseUri, component),
		existPut.bind(null, JSON.stringify(docJSON), 'application/json', baseUri, 'source.json'),
		existPut.bind(null, validationResult, 'application/json', baseUri, 'validation.json')
	],
	function(err, results) {
		cb(null, {
			pathAkn: results[0],
			pathSource: results[1],
			pathValidation: results[2]
		});
	});
}
