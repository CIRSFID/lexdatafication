import streamifier from 'streamifier';
import http from 'http';
import {join as joinPath} from 'path';
import { DOMParser } from 'xmldom'
import xpath from 'xpath'

import appConfig from '../configs/configs.json';

function existGet(path, callback) {
    var resource = joinPath(appConfig.eXistOptions.rest, path);
    console.log('GET FILE', resource);
    http.get({
        host: appConfig.eXistOptions.host,
        port: appConfig.eXistOptions.port,
        auth: appConfig.eXistOptions.auth,
        path: resource
    }, function (res) {
		var data = '';
        if(res.statusCode == 404)
            return callback(404);
        else if (res.statusCode != 200)
			return callback(new Error('Exist GET request has status code ' + res.statusCode));
		res.on('data', function (chunk) {
			data += chunk;
		});
        res.on('error', function (err) {
            callback(new Error('Error getting file'+ err));
        });
        res.on('end', function () {
            callback(null, data);
        });
    }).on('error', (e) => {
		console.log(e);
		console.log('Trying again in 2 sec...');
		setTimeout(existGet.bind(null, path, callback), 2000);
	})
};


function existPut(content, mime, path, file, callback) {
	if (!content) return callback(null);
	var pathToPull = joinPath(appConfig.eXistOptions.rest,
								appConfig.eXistOptions.baseCollection,
								path, file);
	var resource = encode_write(pathToPull);
	var header = { 'Content-Type': mime };
	const input = streamifier.createReadStream(content);

	var output = http.request({
		method: "PUT",
		host: appConfig.eXistOptions.host,
		port: appConfig.eXistOptions.port,
		auth: appConfig.eXistOptions.auth,
		path: resource,
		headers: header
	}, function (res) {
		res.on('error', function (err) {
			input.unpipe(output);
			console.error('Error putting file ', err);
			callback(null);
		});
		res.resume();
		res.on('end', function () {
			//console.log(res)
			if (res.statusCode < 200 || res.statusCode >= 300) {
				console.error('Exist PUT request has status code ' + res.statusCode);
				callback(null);
			}
			else
				callback(null, pathToPull);
		});
	});
	input.pipe(output);
}

function encode_write(str) {
	return str.split('/').map(encodeURIComponent).join('/');
}

function getUri(aknString) {
	try {
		var doc = new DOMParser().parseFromString(aknString);
		var select = xpath.useNamespaces({
			"akn": "http://docs.oasis-open.org/legaldocml/ns/akn/3.0"
		})
		var node = select('//akn:FRBRExpression/akn:FRBRthis/@value', doc)[0];
		return node && node.textContent;
	}
	catch (e) {
		console.error(e);
		return false;
	}
}

export {existGet, existPut, getUri}
