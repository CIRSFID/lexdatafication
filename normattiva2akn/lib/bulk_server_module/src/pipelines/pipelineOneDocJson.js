import fs from 'fs';
import path from 'path';
import async from 'async';

import serverUtils from '../imports/utils/ServerUtils';
import nmt from 'lib/document_generator';

var jsonFile = '../../test/testData/003G0218.json';
var name = path.basename(jsonFile).split('.')[0];

var doc = require(jsonFile);

async.waterfall([
	convertJSONtoAKN.bind(null, doc),
	validateAKN,
], function(err, jsonDoc, aknSource, validationResult) {
	console.log(jsonDoc, aknSource, validationResult);
	writeToFile('./output/'+name+'.json', JSON.stringify(jsonDoc, null, 4));
	writeToFile('./output/'+name+'.xml', aknSource);
	writeToFile('./output/'+name+'-validation.json', JSON.stringify(JSON.parse(validationResult), null, 4));
});

function writeToFile(fileName, content) {
	fs.writeFileSync(fileName, content);
}

function convertJSONtoAKN(doc, cb) {
	try {
		nmt.translateDoc(JSON.parse(JSON.stringify(doc)))
		.then((aknString) => {
				cb(null, doc, aknString);
			})
			.catch((err) => {
				console.error(err);
				cb(null, doc, null);
			});
	} catch (err) {
		console.error(err);
		cb(null, doc, null);
	}
}

function validateAKN(doc, akn, cb) {
	if (!akn) {
		return cb(null, doc, null, null);
	} else {
		serverUtils.validateAknCb(akn, (err, validationResult) => {
			if (err) {
				return cb(null, doc, akn, null);
			}
			cb(null, doc, akn, validationResult);
		});
	}
}
