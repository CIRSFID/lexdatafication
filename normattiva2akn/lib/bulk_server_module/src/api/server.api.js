import express from 'express';
import fs from 'fs';
import path from 'path';
import promiseSequential from 'promise-sequential';
import async from 'async';
import appConfig from '../configs/configs.json';
import serverUtils from '../imports/utils/ServerUtils';
import nmt from 'lib/document_generator';

//ipzs bulk api
import ipzs from 'lib/ipzs-api';

// ----- Express app ------ //
let app = null;
const multerUpload = null;
const appPort = process.env.PORT || 80;
const feDir = process.env.ROOT_DIR || path.join(__dirname, '../frontend');
//helpers for promise-sequential
const getTranslateDocFunction = (jsonContent, fileName) => () => nmt.translateDoc(jsonContent, fileName);
const getGetJsonContentFunction = actElement => () => serverUtils.getJsonContent(actElement);
const getValidateAknFunction = xmlFileContent => () => serverUtils.validateAkn(xmlFileContent);

const errorHandler = (err, req, res, next) => {
	if (res.headersSent) {
		return next(err);
	}
	res.status(500);
	res.render('error', { error: err });
};

app = express();
app.use(errorHandler);
//for development static serve only
app.use(express.static(feDir));

//Bulk convertion api
app.get(`${appConfig.bulkOptions.apiBulk  }/:startDate/:endDate`, (request, response) => {
	console.log('Bulk - Date range: ', request.params.startDate, request.params.endDate);
	serverUtils.callBulkApiList(request.params.startDate, request.params.endDate)
		.then((res) => {
			try {
				const jsonRes = res;
				const gazzette = jsonRes.attiDaConvertire.gazzette;
				const actList = [];
				const getContentsPromiseArray = [];
				const translateContentsPromiseArray = [];
				const validateContentsPromiseArray = [];
				gazzette.forEach((el) => {
					if (el.attiGazzetta) {
						el.attiGazzetta.forEach((el) => {
							actList.push(el);
							getContentsPromiseArray.push(getGetJsonContentFunction(el));
						});
					}
				});
				//get content from json
				promiseSequential(getContentsPromiseArray)
					.then((jsonFilesContent) => {
						jsonFilesContent.forEach((el) => {
							try {
								const parsedContent = JSON.parse(el.data);
								translateContentsPromiseArray.push(getTranslateDocFunction(
parsedContent,
									el.actElement.filePath
));
							}catch (err) {
								console.log('err: ', err);
								throw err;
							}
						});
						//translate content from json to akn
						promiseSequential(translateContentsPromiseArray)
							.then((xmlFilesContent) => {
								xmlFilesContent = xmlFilesContent.map(entry => ({
									fileName: `${entry.fileName.substr(0, entry.fileName.lastIndexOf('.')) || entry.fileName}.xml`,
									xmlContent: entry.xmlContent
								}));
								xmlFilesContent.forEach((el) => {
									validateContentsPromiseArray.push(getValidateAknFunction(el));
								});
								//validate akn content
								promiseSequential(validateContentsPromiseArray)
									.then((validateXmlFilesContent) => {
										response.end(JSON.stringify({ content: validateXmlFilesContent }, null, 3));
									})
									.catch((rjReason) => {
										console.log(rjReason);
										throw rjReason;
									});
							})
							.catch((rjReason) => {
								console.log(rjReason);
								throw rjReason;
							});
					})
					.catch((rjReason) => {
						console.log(rjReason);
						throw rjReason;
					});
			} catch (err) {
				console.log('err: ', err);
				throw err;
			}
		})
		.catch((err) => {
			console.log('err: ', err);
			throw err;
		});
});

//for each file in bulk write source json, translated akn and .validation json
//TO-DO: post api with filter criteria
app.get(`${appConfig.bulkOptions.indirizzoBulkFileSystem}/:startDate/:endDate`, (request, response) => {
	console.log('Bulk - Date range: ', request.params.startDate, request.params.endDate);
	ipzs.downloadDocList(request.params.startDate, request.params.endDate, ["LEGGE"], (err, res) => {
		if (err) {
			console.log(err);
			response.status(500);
			response.end(err);
			return;
		}
		const currentTs = new Date().getTime();
		console.log('Tot docs: ', res.length, currentTs);
		//write status file that contains the list of docs
		response.status(200);
		response.end(res.length.toString());
		//convert and save each file on disk
		async.mapSeries(res, serverUtils.convertSaveValidateDoc_FS, (err, content) => {
			if (err) {
				console.log(err);
				response.status(500);
				response.end(err);
				return;
			}
			console.log("Processed: ", content.length, "docs!");
			console.log("Bulk conversion end");
		});
	});
});

//for each file in bulk write source json, translated akn and .validation json
//TO-DO: post api with filter criteria
app.get(`${appConfig.bulkOptions.indirizzoBulkExist}/:startDate/:endDate`, (request, response) => {
	console.log('Bulk - Date range: ', request.params.startDate, request.params.endDate);
	ipzs.downloadDocList(request.params.startDate, request.params.endDate, ["LEGGE"], (err, res) => {
		if (err) {
			console.log(err);
			response.status(500);
			response.end(err);
			return;
		}
		const currentTs = new Date().getTime();
		//here can filter for tipoDocumento es. DECRETO LEGGE, COMUNICATO ecc...
		const filteredDocs = res.filter((el) => el.tipoDocumento === 'LEGGE');
		const fileNamesList = filteredDocs.map((el) => el.codiceRedazionale + '_' + el.dataDocumento);

		console.log('Tot docs: ', filteredDocs.length, currentTs);
		//write status file that contains the list of docs
		response.status(200);
		response.end(filteredDocs.length.toString());
		//convert and save each file on disk
		async.mapSeries(filteredDocs, serverUtils.convertSaveValidateDoc_eXist, (err, content) => {
			if (err) {
				console.log(err);
				response.status(500);
				response.end(err);
				return;
			}
			console.log("Processed: ", content.length, "docs!");
			console.log("Bulk conversion end");
		});
	});
});

app.listen(appPort, () => {
	console.log('App started on: localhost:', appPort);
});

