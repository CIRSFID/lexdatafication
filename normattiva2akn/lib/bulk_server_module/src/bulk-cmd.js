import commander from 'commander';
import pkg from '../package.json';
import ipzs from 'lib/ipzs-api';
import serverUtils from './imports/utils/ServerUtils';

commander
	.version(pkg.version)
	.command('convert-bulk <startDate_YYYY-MM-DD> <endDate_YYYY-MM-DD>')
	.action((startDate, endDate) => {
		console.log(`Converting doc in range: ${startDate} - ${endDate}`);
		ipzs.downloadDocList(startDate, endDate, ['LEGGE'], (err, res) => {
			if (err) {
				return console.log(err);
			}
			const currentTs = new Date().getTime();
			//here can filter for tipoDocumento es. DECRETO LEGGE, COMUNICATO ecc...
			const filteredDocs = res.filter(el => el.tipoDocumento === 'LEGGE');
			const fileNamesList = filteredDocs.map(el => `${el.codiceRedazionale}_${el.dataDocumento}`);
			console.log('Tot docs: ', filteredDocs.length, currentTs);
			//convert and save each file on exist instance
			async.mapSeries(filteredDocs, serverUtils.convertSaveValidateDoc_eXist, (err, content) => {
				if (err) {
					return console.log(err);
				}
				console.log('Processed: ', content.length, 'docs!');
				console.log('Bulk conversion end');
			});
		});
	});

commander.parse(process.argv);

