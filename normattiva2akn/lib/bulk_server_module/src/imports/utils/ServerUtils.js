import fs from 'fs';
import path from 'path';
import http from 'http';
import async from 'async';
import streamifier from 'streamifier';
import appConfig from '../../configs/configs.json';
import nmt from 'lib/document_generator';
//ipzs bulk api
import ipzs from 'lib/ipzs-api';
import xmlUtils from 'lib/xml_utils';

const appDir = process.env.APP_DIR || path.join(__dirname, appConfig.bulkOptions.directoryTestDWBulk);

const serverUtils = {
	callBulkApiList: (startDate, endDate) => new Promise((resolve, reject) => {
		//TO-DO: call real
		ipzs.download(startDate, endDate, appDir, (res) => {
			console.log(res);
			resolve(res);
		});
	}),
	//Get json content from api
	getJsonContent: actElement => new Promise((resolve, reject) => {
		//TO-DO: call real
		const stream = fs.createReadStream(
			path.join(appDir, actElement.filePath),
			{ flags: 'r', encoding: 'utf-8' }
		);
		let bufferedData = '';
		stream.on('data', (data) => {
			bufferedData += data.toString();
		});
		stream.on('end', () => {
			resolve({
				data: bufferedData,
				actElement
			});
		});
		stream.on('error', (err) => {
			console.log(err);
			reject(err);
		});
	}),
	validateAkn: xmlFileContent => new Promise((resolve, reject) => {
		try {
			let validationRes = '';
			const postOpt = appConfig.validationApi;
			const postData = JSON.stringify({ source: xmlFileContent.xmlContent });
			postOpt.headers['Content-Length'] = Buffer.byteLength(postData);
			const postReq = http.request(postOpt, (res) => {
				res.setEncoding('utf8');
				res.on('data', (chunk) => {
					validationRes += chunk.toString();
				});
				res.on('end', () => {
					try {
						xmlFileContent.validationObj = JSON.parse(validationRes);
						resolve(xmlFileContent);
					} catch (err) {
						console.log(err);
						reject(err);
					}
				});
				res.on('error', (err) => {
					console.log(err);
					reject(err);
				});
			});
			postReq.write(postData);
		} catch (err) {
			console.log(err);
			reject(err);
		}
	}),
	validateAknCb: (xmlFileContent, cb) => {
		try {
			let validationRes = '';
			const postOpt = appConfig.validationApi;
			const postData = JSON.stringify({ source: xmlFileContent });
			postOpt.headers['Content-Length'] = Buffer.byteLength(postData);
			const postReq = http.request(postOpt, (res) => {
				res.setEncoding('utf8');
				res.on('data', (chunk) => {
					validationRes += chunk.toString();
				});
				res.on('end', () => {
					console.log(validationRes);
					cb(null, validationRes);
				});
				res.on('error', (err) => {
					console.log(err);
					cb(err);
				});
			});
			postReq.write(postData);
		} catch (err) {
			console.log(err);
			cb(err);
		}
	},
	validateAknLocalCb: (xmlFileContent, cb) => {
		xmlUtils.validate(xmlFileContent, (err, res) => {
			if (err) {
				return cb(err);
			}
			cb(null, JSON.stringify(res));
		});
	},
	convertSaveValidateDoc_FS: (docObj, doneCb) => {
		async.waterfall([
			(cb) => {
				cb(null, docObj);
			},
			serverUtils.downloadAndWriteIpzsDoc,
			serverUtils.convertAndWrite,
			serverUtils.validateAndWrite
		], doneCb);
	},
	convertSaveValidateDoc_eXist: (docObj, doneCb) => {
		async.waterfall([
			(cb) => {
				cb(null, docObj);
			},
			serverUtils.downloadAndWriteIpzsDoc,
			serverUtils.convert,
			serverUtils.writeToeXist
		], doneCb);
	},
	downloadIpzsDoc: (docObj, cb) => {
		//download atto and save content to filesystem
		ipzs.downloadAtto(docObj, (err, data) => {
			if (err) {
				return cb(err);
			}
			const fileName = `${docObj.codiceRedazionale}_${docObj.dataDocumento}.json`;
			console.log('---- ', fileName, ' ----');
			cb(null, data, `${docObj.codiceRedazionale}_${docObj.dataDocumento}`);
		});
	},
	convert: (fileContent, fileNameNoExt, cb) => {
		//translate and write akn to
		try {
			nmt.translateDoc(fileContent)
				.then((data) => {
					//write content to
					cb(null, data, fileNameNoExt);
				})
				.catch((err) => {
					//write error file if convertion is not good
					const fileName = `${fileNameNoExt}.errors.txt`;
					fs.writeFile(path.join(appDir, fileName), err, (err) => {
						if (err) return cb(err);
						cb(null, null, fileNameNoExt);
					});
				});
		} catch (err) {
			//write error file if convertion is not good
			const fileName = `${fileNameNoExt}.errors.txt`;
			fs.writeFile(path.join(appDir, fileName), err, (err) => {
				if (err) return cb(err);
				cb(null, null, fileNameNoExt);
			});
		}
	},
	writeToeXist: (fileContent, fileNameNoExt, cb) => {
		if (!fileContent) {
			console.log("Errors in doc translation: ", fileNameNoExt);
			return cb(null, fileNameNoExt);
		}
		const encodeWrite = str => str.split('/').map(encodeURIComponent).join('/');
		const resource = encodeWrite(`${appConfig.eXistOptions.rest + appConfig.eXistOptions.baseCollection}/${fileNameNoExt}`);
		const input = streamifier.createReadStream(fileContent);
		console.log('PUT FILE', resource);
		const output = http.request({
			method: 'PUT',
			host: appConfig.eXistOptions.host,
			port: appConfig.eXistOptions.port,
			auth: appConfig.eXistOptions.auth,
			path: resource,
			headers: {
				'Content-Type': 'application/xml'
			}
		}, (res) => {
			res.on('error', (err) => {
				cb(err);
				input.unpipe(output);
			});
			res.resume();
			res.on('end', () => {
				if (res.statusCode < 200 || res.statusCode >= 300) {
					cb(new Error(`Exist PUT request has status code ${res.statusCode}`));
				} else {
					cb(null, fileNameNoExt);
				}
			});
		});
		input.pipe(output);
	},
	downloadAndWriteIpzsDoc: (docObj, cb) => {
		//download atto and save content to filesystem
		ipzs.downloadAtto(docObj, (err, data) => {
			if (err) {
				return cb(err);
			}
			const fileName = `${docObj.codiceRedazionale}_${docObj.dataDocumento}.json`;
			console.log('---- ', fileName, ' ----');
			fs.writeFile(path.join(appDir, fileName), JSON.stringify(data), (err) => {
				if (err) {
					cb(err);
				}
				cb(null, data, `${docObj.codiceRedazionale}_${docObj.dataDocumento}`);
			});
		});
	},
	convertAndWrite: (fileContent, fileNameNoExt, cb) => {
		//translate and write akn to
		try {
			nmt.translateDoc(fileContent)
				.then((data) => {
					//write content to
					const fileName = `${fileNameNoExt}.xml`;
					fs.writeFile(path.join(appDir, fileName), data, (err) => {
						if (err) {
							cb(err);
						}
						cb(null, data, fileNameNoExt);
					});
				})
				.catch((err) => {
					//write error file if convertion is not good
					const fileName = `${fileNameNoExt}.errors.txt`;
					fs.writeFile(path.join(appDir, fileName), err, (err) => {
						if (err) return cb(err);
						cb(null, null, fileNameNoExt);
					});
				});
		} catch (err) {
			//write error file if convertion is not good
			const fileName = `${fileNameNoExt}.errors.txt`;
			fs.writeFile(path.join(appDir, fileName), err, (err) => {
				if (err) return cb(err);
				cb(null, null, fileNameNoExt);
			});
		}
	},
	validateAndWrite: (fileContent, fileNameNoExt, cb) => {
		//if content is null an error occured during translation
		if (!fileContent) {
			console.log('Errors on: ', fileNameNoExt);
			const fileName = `${fileNameNoExt}.validation.json`;
			fs.writeFile(path.join(appDir, fileName), JSON.stringify({ success: 'false' }), (err) => {
				if (err) {
					cb(err);
				}
				//resolve the name of converted file without extension
				cb(null, fileNameNoExt);
			});
		} else {
			serverUtils.validateAknCb(fileContent, (err, data) => {
				if (err) {
					cb(err);
				}
				//write validation file to filesystem
				const fileName = `${fileNameNoExt}.validation.json`;
				fs.writeFile(path.join(appDir, fileName), data, (err) => {
					if (err) {
						cb(err);
					}
					//resolve the name of converted file without extension
					cb(null, fileNameNoExt);
				});
			});
		}
	}
};

export default serverUtils;

