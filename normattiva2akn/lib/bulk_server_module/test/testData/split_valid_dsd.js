var fs = require('fs');
var path = require('path');
var gracefulFs = require('graceful-fs')
gracefulFs.gracefulify(fs);

var download_dir = path.join(__dirname,'./download_stub_data/');

function copyFile(source, target, cb) {
	var cbCalled = false;

	var rd = fs.createReadStream(source);
	rd.on("error", function (err) {
		done(err);
	});
	var wr = fs.createWriteStream(target);
	wr.on("error", function (err) {
		done(err);
	});
	wr.on("close", function (ex) {
		done();
	});
	rd.pipe(wr);

	function done(err) {
		if (!cbCalled) {
			cb(err);
			cbCalled = true;
		}
	}
}

if (!fs.existsSync(path.join(download_dir, '/not_valid'))) {
	fs.mkdirSync(path.join(download_dir, '/not_valid'));
}

if (!fs.existsSync(path.join(download_dir, '/valid'))) {
	fs.mkdirSync(path.join(download_dir, '/valid'));
}

fs.readdir(download_dir, function (err, filenames) {
	if (err) {
		console.log(err);
		return;
	}
	console.log("Writing files...")
	filenames.forEach(function (filename) {
		if (filename.indexOf('.validation') !== -1){
			fs.readFile(download_dir + filename, 'utf-8', function (err, content) {
				if (err) {
					console.log(err);
					return;
				}
				try{
					var filenameNoExt = filename.split('.')[0];
					var pCnt = JSON.parse(content);
					var cntPath = download_dir + '/';

					if (pCnt.success) {
						console.log("Valid: ", filename);
						if (fs.existsSync(path.join(cntPath, filenameNoExt + '.xml'))) {
							copyFile(path.join(cntPath, filenameNoExt + '.xml'), path.join(cntPath, '/valid/', filenameNoExt + '.xml'), function () { });
						}
						if (fs.existsSync(path.join(cntPath, filenameNoExt + '.json'))) {
							copyFile(path.join(cntPath, filenameNoExt + '.json'), path.join(cntPath, '/valid/', filenameNoExt + '.json'), function () { });
						}
						//re writes the validation content
						fs.writeFile(path.join(cntPath, '/valid/', filenameNoExt + '.validation.json'), content, function (err) {
							if(err) console.log(err);
						});
					} else {
						console.log("Not Valid: ", filename);
						if (fs.existsSync(path.join(cntPath, filenameNoExt + '.xml'))) {
							copyFile(path.join(cntPath, filenameNoExt + '.xml'), path.join(cntPath, '/not_valid/', filenameNoExt + '.xml'), function () { });
						}
						if (fs.existsSync(path.join(cntPath, filenameNoExt + '.json'))) {
							copyFile(path.join(cntPath, filenameNoExt + '.json'), path.join(cntPath, '/not_valid/', filenameNoExt + '.json'), function () { });
						}
						//re writes the validation content
						fs.writeFile(path.join(cntPath, '/not_valid/', filenameNoExt + '.validation.json'), content, function (err) {
							if (err) console.log(err);
						});
					}
				} catch(err){
					console.log("Not Valid: ", filename);
				}

			});
		}
	});
});

