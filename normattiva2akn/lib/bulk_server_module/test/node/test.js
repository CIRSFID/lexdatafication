import { expect } from 'chai';
import fs from 'fs';
import path from 'path';
import nmt from '../../src/api/nmt.api.min.js';

const downloadPath = '../testData/download_stub_data';
const getTranslateDocFunction = (jsonContent, fileName) => () => nmt.translateDoc(jsonContent, fileName);

describe('#convert all downloaded directory files', () => {
	it('Should convert all files in directory', (done) => {
		fs.readdir(path.join(__dirname, downloadPath), function (err, items) {

			for (var i = 0; i < items.length; i++) {
				console.log(items[i]);
			}
		});
		done();

	});

	it('should return false if the language is wrong', (done) => {
		alt.languageDetect('test/resources/akn.1.xml',(data) => {
			expect(data).equal(false);
			done();
		})
	});
});
