<img src="https://gitlab.com/lucacervone/node-module-template/raw/master/assets/logo.jpg"
     width="313"
     style="box-shadow:none!important">

A template for bootstrapping the development of node modules with ES2017.

## Create a new module ##

To create a new module, first clone the repository.

`git clone https://gitlab.com/lucacervone/node-module-template`.

The core of the template is the file `nmt.api.js` located in `src/api/`. You can define your own API by adding it to the `mnt` object inside the file.

You can also load external javascript modules. There are some import examples placed in `src/imports/`.

## Tests ##

You can define your own tests inside the appropriate `test/` folder.

- `test/node/test.js` contains some example tests which are meant to be run with node, while
- `test/browser/test-browser.js` contains some example tests which can be run inside the browser.
- `test/xml/test-xml.js` contains an example of a test for checking the result of a routine used to apply XSLTs to XMLs.

In order to run your node tests, you can simply run `npm test`, which will compile the template and search for .js files inside `test/node/`.

In order to run your browser tests, run `npm run test-browser`, which will compile and open up a browser window with the content provided by `test/browser/test-browser.html`.

In order to run your xml tests, run `npm run test-xml`, which will load two xslt and xml examples from `test/resources/` and apply the xslt to the xml.

<!-- ### Node test ### -->

<!-- ### Browser test ### -->

<!-- ### CLI test ### -->

## Contributing ##

In lieu of a formal style guide, take care to maintain the existing coding style. Add at least 3 unit tests for any new functionality. Be sure that any changed functionality continue to pass its dedicated tests. Lint and test your code.
