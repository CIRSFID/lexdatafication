import { expect } from 'chai';
import jsonData from '../resources/090G0041.json';
import JsonHandler from '../../src/imports/JsonHandler';
import jsonFile from 'jsonfile';
import fs from 'fs';

describe('#JsonHandlerTest', () => {
	it('should write on file ref-json-test.json refined output', () => {
		let jsonContent = new JsonHandler(jsonData);
		let refinedContent = jsonContent.generateJSON();
		jsonFile.writeFileSync('./ref-json-test.json', refinedContent);
	});
});
