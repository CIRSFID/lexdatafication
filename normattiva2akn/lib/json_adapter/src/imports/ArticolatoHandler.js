/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
	--- object model ---
	"elementoArticolato": {
		"idInterno": "",
		"rubricaNir": "Definizioni",
		"livello": 1,
		"numNir": "Art. 1 .",
		"novelle": {}, //only if article
		"gruppoInterno": "",
		"idNir": "1",
		"testo": "1. Ai fini del pres...", //only if article
		"nomeNir": "articolo",
		"elementi": [] //only if child elements are present
	}
*/

import ArticleUtils from './article_generator/ArticleUtils';
import HierarchyHandler from './articolato_hierachy/HierarchyHandler';
import cloneDeep from 'clone-deep';
import fs from 'fs';

class ArticolatoHandler{
	constructor(jsonIn){
		this.articolato = jsonIn.articolato || {
			"articolato": {
				"elementi": []
			}
		};
		this.artUtils = new ArticleUtils();
		this.hierarchyHandler = new HierarchyHandler();
		this.refinedFlatArticoltato = [];
		this.refinedArticoltato = [];
		this.firstArticle = this.artUtils.getFirstArticle(jsonIn);
	}

	getArticolato(){
		return {
			"articolato": {
				"elementi": this.refinedArticoltato
			}
		};
	}

	refine(){
		//refine single article entry
		this.refineAllArticles(this.articolato.elementi);
		//find capo and sezioni creating new elems (create elementoArticolato)
		this.createAllSections(this.articolato.elementi);
	}

	refineAllArticles(elementsArray){
		for (let k = 0; k < elementsArray.length; k++) {
			if (elementsArray[k].elementi && elementsArray[k].nomeNir !== 'articolo') {
				this.refineAllArticles(elementsArray[k].elementi);
			} else if (elementsArray[k].elementi && elementsArray[k].nomeNir === 'articolo') {
				//skips first article to handle preamble apart
				if (elementsArray[k]
					&& elementsArray[k].testo.length > 0
					&& this.firstArticle.testo === elementsArray[k].testo){
						continue;
					}
				//here refine single article testo entry
				if(elementsArray[k] && elementsArray[k].testo.length > 0){
					//replace \n with spaces
					elementsArray[k].testo = this.artUtils.refineText(elementsArray[k].testo);
				}
				//replace numNir with the article declaration (es Art. 1.)
				this.artUtils.refineNumNir(elementsArray[k]);
				//remove the rubricaNir from the article
				this.artUtils.refineRubricaNir(elementsArray[k]);
				//trims testo after all substitutions
				if (elementsArray[k] && elementsArray[k].testo.length > 0) {
					elementsArray[k].testo = elementsArray[k].testo.trim();
				}
			}
		}
	}

	createAllSections(elementsArray) {
		let lastParsedParent = null;
		let lastParsedChild = null;
		for (let k = 0; k < elementsArray.length; k++) {
			if (elementsArray[k].numNir &&
				elementsArray[k].elementi &&
				elementsArray[k].nomeNir !== 'articolo' &&
				elementsArray[k].nomeNir !== 'allegato') {
				let parsedObj = this.hierarchyHandler.generateArticolatoHierarchyObject(elementsArray[k]);
				this.refinedFlatArticoltato.push(parsedObj);
			} else if (elementsArray[k].elementi && !elementsArray[k].nomeNir) {
				elementsArray[k].elementi.forEach(el => {
					this.refinedFlatArticoltato.push({
						'elementoArticolato': el
					});
				});
			}
		}
		this.refinedArticoltato.push(this.refinedFlatArticoltato[0]);
		for (let k = 1; k < this.refinedFlatArticoltato.length; k++) {
			if (this.refinedArticoltato[this.refinedArticoltato.length - 1].elementoArticolato.level < this.refinedFlatArticoltato[k].elementoArticolato.level){
				this.hierarchyHandler.setCorrectLevel(this.refinedArticoltato[this.refinedArticoltato.length - 1].elementoArticolato, this.refinedFlatArticoltato[k].elementoArticolato);
			} else {
				this.refinedArticoltato.push(this.refinedFlatArticoltato[k]);
			}
		}

	}




}

module.exports = ArticolatoHandler;
