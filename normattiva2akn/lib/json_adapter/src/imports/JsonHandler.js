import ArticolatoHandler from './ArticolatoHandler';
import PreambleHandler from './PreambleHandler';
import ConclusionHandler from './ConclusionHandler';
import TransitionHandler from './TransitionHandler';
import ArticleUtils from './article_generator/ArticleUtils';
import AttachmentsHandler from './AttachmentsHandler';
import traverse from 'traverse';
import * as R from 'ramda';

class JsonHandler {
	constructor(jsonIn) {
		this.jsonObj = jsonIn || {};
		this.articolatoHandler = new ArticolatoHandler(jsonIn);
		this.preambleHandler = new PreambleHandler(jsonIn);
		this.conclusionHandler = new ConclusionHandler(jsonIn);
		this.transitionHandler = new TransitionHandler(jsonIn);
		this.articleUtils = new ArticleUtils(jsonIn);
		this.attachmentsHandler = new AttachmentsHandler(jsonIn);
		this.jsonContent = {};
	}

	generateJSON() {
		let firstArticle, lastArticle, endOffset, rpStrPre;
		//preamble handling
		this.preambleHandler.refine();
		//metadata handling

		this.jsonContent.metadati = this.jsonObj.metadati;
		this.jsonContent.metadati.dataVigoreOriginale = this.inferVersionDate();
		this.jsonContent.preambolo = this.preambleHandler.getPreamble().preambolo;
		//articolato handling
		this.articolatoHandler.refine();
		this.jsonContent.articolato = this.articolatoHandler.getArticolato().articolato;
		//transitorie handling
		this.transitionHandler.refine();
		this.jsonContent.transitorie = this.transitionHandler.getTransitorie().transitorie;
		//intestazione handling
		this.jsonContent.intestazione = this.jsonObj.intestazione;
		//note handling
		this.jsonContent.note = this.jsonObj.note;
		//annessi handling
		this.jsonContent.annessi = this.jsonObj.annessi;
		// this.jsonContent.annessi = this.attachmentsHandler.getAnnessi();
		//lavoriPreparatori
		this.jsonContent.lavoriPreparatori = this.jsonObj.lavoriPreparatori;
		//errori handling
		this.jsonContent.errori = this.jsonObj.errori;
		//conclusion handling
		this.conclusionHandler.refine();
		this.jsonContent.conclusioni = this.conclusionHandler.getConclusioni().conclusioni;
		//remove preamble from first article only if the preamble was finded
		firstArticle = this.articleUtils.getFirstArticle(this.jsonObj);
		if (this.jsonContent.preambolo.testoCompleto !== '') {
			if (firstArticle.testo && firstArticle.testo.length > 0) {
				firstArticle.testo = this.articleUtils.refineText(firstArticle.testo);
				endOffset = firstArticle.testo.indexOf(this.jsonContent.preambolo.testoFormulaFinalePreambolo);
				rpStrPre = firstArticle.testo.substring(0,
					endOffset + this.jsonContent.preambolo.testoFormulaFinalePreambolo.length);
				firstArticle.testo = firstArticle.testo.replace(rpStrPre, '');
				this.articleUtils.refineNumNir(firstArticle);
				this.articleUtils.refineRubricaNir(firstArticle);
				firstArticle.testo = firstArticle.testo.trim();
			}
		} else {
			//if no preamble was finded create a null numNir to remove
			//the <num> that can't be replaced from text
			//check if first article is an object (no articles in list check)
			if(!R.isEmpty(R.keys(firstArticle))){
				firstArticle.numNir = null;
				//if does not find try to find the correct num
				this.articleUtils.refineNumNirNoPreamble(firstArticle);
				this.articleUtils.refineRubricaNir(firstArticle);
			}

		}

		//remove conclusions and transitional from last article
		lastArticle = this.articleUtils.getLastArticle(this.jsonObj);

		if (lastArticle.testo
			&& lastArticle.testo.length > 0
			&& this.jsonContent.conclusioni
			&& this.jsonContent.conclusioni.testoCompleto
			&& this.jsonContent.conclusioni.testoCompleto.length > 0) {
			//if  the first and the last article are same (one article only), need to refine for conclusions
			lastArticle.testo = this.articleUtils.refineText(lastArticle.testo);
			lastArticle.testo = lastArticle.testo.replace(this.jsonContent.transitorie.testoCompleto.trim(), '');
			lastArticle.testo = lastArticle.testo.replace(this.jsonContent.conclusioni.testoCompleto.trim(), '');
		}
		return this.jsonContent;
	}

	inferVersionDate() {
		return traverse(this.jsonObj)
					.reduce((acc, el) => {
						if (el && el.inizioVigore)
							acc = R.max(acc, el.inizioVigore);
						return acc;
					}, '');
	}


}

module.exports = JsonHandler;

