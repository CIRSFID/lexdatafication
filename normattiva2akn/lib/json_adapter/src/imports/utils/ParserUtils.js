/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import parser from 'lib/parsers';
import sorty from 'sorty';
import _ from 'underscore';

class ParserUtils {
	constructor(locale) {
		const selectedLocale = locale || 'it_IT';
		this.dateParser = new parser.DateParser(selectedLocale);
		this.referenceParser = new parser.ReferenceParser(selectedLocale);
		this.chapterSectionParser = new parser.ChapterSectionParser(selectedLocale);
		this.preambleParser = new parser.PreambleParser(selectedLocale);
		this.conclusionParser = new parser.ConclusionParser(selectedLocale);
		this.conclusionDateParser = new parser.ConclusionDateParser(selectedLocale);
		this.articleNumParser = new parser.ArticleNumParser(selectedLocale);
		this.citationRecitalsParser = new parser.CitationRecitalsParser(selectedLocale);
		this.transitionalParser = new parser.TransitionalParser(selectedLocale);
		this.preambleInitParser = new parser.PreambleInitParser(selectedLocale);
		this.preambleEndParser = new parser.PreambleEndParser(selectedLocale);
	}

	getChapterSectionStatus(stringToParse){
		let parseStatus = {
			foundChapter: false,
			foundSection: false,
			matches: []
		};
		this.chapterSectionParser.getWiderMatches(stringToParse).forEach((match) => {
			parseStatus.matches.push(match);
			if (match.section){
				parseStatus.foundSection = true;
			}
			if (match.chapter){
				parseStatus.foundChapter = true;
			}
		});
		return parseStatus;
	}

	getNumNirParsedStatus(stringToParse) {
		let rightMatches = [];
		this.chapterSectionParser.getWiderMatches(stringToParse).forEach((match) => {
			//exclude patterns and modification parenthesis from matches
			if (match.fullString.indexOf('(') < 0
				&& match.fullString.indexOf(')') < 0
				&& match.fullString.toLowerCase().indexOf('parte del') < 0
				&& match.fullString.toLowerCase().indexOf('parte di') < 0) {
					rightMatches.push(match);
			}
		});
		return rightMatches;
	}

	getPreambleParserStatus(stringToParse) {
		let parsedSections = {
			testoCompleto: null,
			testoFormulaInzialePreambolo: null,
			testoFormulaFinalePreambolo: null,
			testoVisti: []
		}
		let initMatches = [];
		let endMathces = [];
		let initMatch, endMatch;

		initMatches = this.preambleInitParser.getMatches(stringToParse); //offset start minor
		endMathces = this.preambleEndParser.getMatches(stringToParse); //offset end major
		if (initMatches.length > 0){
			initMatch = _.min(initMatches, _.property(['offset', 'start']));
			parsedSections.testoFormulaInzialePreambolo = initMatch.refString;
		}
		if (endMathces.length > 0) {
			endMatch = _.max(endMathces, _.property(['offset', 'end']));
			parsedSections.testoFormulaFinalePreambolo = endMatch.refString;
		}
		//create testo completo
		if (endMatch) {
			parsedSections.testoCompleto = stringToParse.substring(0,
				endMatch.offset.end);
		} else {
			parsedSections.testoCompleto = '';
		}
		//ad hoc parsing for testo visti
		this.citationRecitalsParser.getMatches(parsedSections.testoCompleto).forEach((match) => {
			parsedSections.testoVisti.push(match.fullString);
		});
		return parsedSections;
	}

	getConclusionParserStatus(stringToParse) {
		let conclusionSections = {
			testoFormulaFinaleConclusioni: null,
			testoData: null,
			testoFirmatari: null,
			testoCompleto: null
		}
		//get wider matches
		let conclusionMatches = this.conclusionParser.getMatches(stringToParse).map(el => {
			el.wide = el.offset.end - el.offset.start
			return el;
		});
		if (!conclusionMatches.length || conclusionMatches.length <= 0){
			return conclusionSections;
		}
		let testoDataMatch, tcDateInside, conclusionInitMatch;
		//to take the offset start of all the conclusion block take the widest result
		//given from the parser
		conclusionInitMatch = _.max(conclusionMatches, _.property(['wide']));
		if (!conclusionInitMatch) {
			return conclusionSections;
		}
		//to take the testo completo
		conclusionSections.testoCompleto = stringToParse.substring(conclusionInitMatch.offset.start,
			stringToParse.length);
		//to take testoData (the first conclusion_data pattern that find in conclusion block)
		testoDataMatch = this.conclusionDateParser.getMatches(conclusionSections.testoCompleto)[0];
		if (!testoDataMatch){
			return conclusionSections;
		}
		conclusionSections.testoData = conclusionSections.testoCompleto.substring(testoDataMatch.offset.start,
			testoDataMatch.offset.end);
		//testoFormulaFinaleConclusioni starts at the beginning of testoCompleto end when start testoDataMatch
		conclusionSections.testoFormulaFinaleConclusioni = conclusionSections.testoCompleto.substring(0,
			testoDataMatch.offset.start);
		//testoFirmatari start at the end of testoData and ends at the end of testoCompleto
		conclusionSections.testoFirmatari = conclusionSections.testoCompleto.substring(testoDataMatch.offset.end,
			conclusionSections.testoCompleto.length);
		return conclusionSections;
	}

	getFirstArticleNumOccurrence(stringToParse) {
		let allArticleMatch = [];
		//sort by start offset to take the first one
		this.articleNumParser.getMatches(stringToParse).forEach((match) => {
			match.startIdx = match.offset.start;
			allArticleMatch.push(match);
		});
		sorty([{ name: 'startIdx', dir: 'asc', type: 'number'}], allArticleMatch);
		return allArticleMatch[0];
	}

	getArticleNumByNumNir(stringToParse, numNir) {
		let numMatch;
		//sort by start offset to take the first one
		this.articleNumParser.getMatches(stringToParse).forEach((match) => {
			if (+match.number === +numNir) {
				numMatch = match;
			}
		});
		return numMatch;
	}

	getTransitionalContent(stringToParse) {
		return this.transitionalParser.getMatches(stringToParse);
	}
}

module.exports = ParserUtils;
