/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import ParserUtils from '../utils/ParserUtils';

class ArticleUtils {
	constructor() {
		this.parserUtils = new ParserUtils();
	}

	refineText(stringIn) {
		stringIn = stringIn || '';
		let strOut = '';
		//remove newlines
		strOut = stringIn.replace(/\n/g, ' ');
		//trim spaces all around text
		strOut = strOut.replace(/\s+/g, ' ');
		//space after parethesis fix for parser
		strOut = strOut.replace(/ \)/g, ')');
		// Useful in the last article
		strOut = strOut.replace(/<\/num>\n?/g, '</num>\n');
		strOut = strOut.replace(/<\/heading>\n?/g, '</heading>\n');
		return strOut;
	}
	//refineTextKeepNewLines -> refineText
	refineTextKeepNewLines(stringIn) {
		stringIn = stringIn || '';
		let strOut = stringIn
			.split('\n')
			.reduce(function(prev, curr, currId, arr) {
				if (curr.trim().length == 0) return prev;
				if (!currId) {
					return prev+curr;
				}
				var prevRow = arr[currId-1];
				if (prevRow.length >= 69) {
					return prev+' '+curr;
				} else {
					return prev+'\n'+curr;
				}
			}, '')
			.replace(/( *\n *)+/g, '\n')
			.replace(/ +/g, ' ');
		return strOut;
	}

	refineNumNir(article) {
		const artNumStr = this.parserUtils.getFirstArticleNumOccurrence(article.testo) || {};
		this.parserUtils.getArticleNumByNumNir(article.testo, article.numNir) || {};
		article.testo = article.testo.replace(artNumStr.articleFullString, '<num>' + artNumStr.articleFullString +'</num>\n');
		article.numNir = artNumStr.articleFullString;
		return article;
	}


	refineNumNirNoPreamble(article) {
		const artNumStr = this.parserUtils.getFirstArticleNumOccurrence(article.testo) || {};
		let textBefore = article.testo.substring(0, artNumStr.offset.start);
		let isNotEmpty = /\S/.test(textBefore);
		if (!isNotEmpty) {
			article.testo = article.testo.replace(artNumStr.articleFullString, '<num>' + artNumStr.articleFullString + '</num>\n');
			article.numNir = artNumStr.articleFullString;
		}
		return article;
	}

	refineRubricaNir(article) {
		if (article.rubricaNir) {
			let refinedRNir = article.rubricaNir;
			//replace all accents (not utf in testo field of json object?)
			refinedRNir = refinedRNir.replace(/è/g, "e'");
			refinedRNir = refinedRNir.replace(/é/g, "e'");
			refinedRNir = refinedRNir.replace(/à/g, "a'");
			refinedRNir = refinedRNir.replace(/ò/g, "o'");
			refinedRNir = refinedRNir.replace(/ù/g, "u'");
			refinedRNir = refinedRNir.replace(/ì/g, "i'");
			refinedRNir = refinedRNir.replace(/È/g, "E'");
			refinedRNir = refinedRNir.replace(/À/g, "A'");
			refinedRNir = refinedRNir.replace(/Ó/g, "O'");
			refinedRNir = refinedRNir.replace(/Ú/g, "U'");
			refinedRNir = refinedRNir.replace(/Í/g, "I'");
			refinedRNir = refinedRNir.replace(/_/g, " ");
			/*
			if (article.testo.indexOf(`${refinedRNir}`) >= 0) {
				article.testo = article.testo.replace(`${refinedRNir}`, '');
				return article;
			}
			*/
			refinedRNir = refinedRNir.trim();
			let rgx = "(?:[\\s'\\(\\)\\.,]*)";
			let rgxRNir = refinedRNir.replace(/(?:[^\s\w])/g, "(?:\\W)");
			rgxRNir = rgxRNir.replace(/(?:\s+)/g, "(?:\\s+)");
			rgxRNir = "([( ]+)?" +  rgxRNir + "([) \\.]+)?";
			let match = article.testo.match(new RegExp(rgxRNir));
			let headingFound = '';
			if(match){
				headingFound = match[0];
			} else { // second attempt
				// rgxRNir = refinedRNir.replace(/(?:(\w))/g, "$1(?:\\s*)");
				rgxRNir = refinedRNir.replace(/./g, (character, offset, str) => {
					var nextCharacter = (offset < str.length-1) ? str[offset+1] : undefined;
					var nextIsNotWordChar = nextCharacter && nextCharacter.match(/\W/);
					// Avoid catastrofic backtracking by avoiding \s* be followed by \W*.
					if (character.match(/\w/)) {
						return nextIsNotWordChar ? character : character+'(?:\\s*)';
					}
					return nextIsNotWordChar ? '' : '(?:\\W*)';
					// This is causing catastrofic backtracking
					// return character.match(/\w/) ? character+'(?:\\s*)' : '(?:\\W*)';
				});
				match = article.testo.match(new RegExp(rgxRNir));
				if (match) {
					headingFound = match[0];
				}
			}
			if (headingFound) {
				article.rubricaNir = headingFound;
				article.testo = article.testo.replace(headingFound, '<heading>' + match[0] + '</heading>\n').trim();
			}
		}
		return article;
	}

	refineRubricaNirOld(article) {
		if (article.rubricaNir) {
			let refinedRNir = article.rubricaNir
				.trim()
				.replace(/\n/g, ' ')
				.replace(/\s+/g, ' ');
			//remove last character if is a dot
			if (refinedRNir.slice(-1) === '.') {
				refinedRNir = refinedRNir.substring(0, refinedRNir.length - 1);
			}
			//replace all accents (not utf in testo field of json object?)
			refinedRNir = refinedRNir.replace(/è/g, "e'");
			refinedRNir = refinedRNir.replace(/à/g, "a'");
			refinedRNir = refinedRNir.replace(/ò/g, "o'");
			refinedRNir = refinedRNir.replace(/ù/g, "u'");
			refinedRNir = refinedRNir.replace(/ì/g, "i'");
			refinedRNir = refinedRNir.replace(/È/g, "E'");
			refinedRNir = refinedRNir.replace(/À/g, "A'");
			refinedRNir = refinedRNir.replace(/Ó/g, "O'");
			refinedRNir = refinedRNir.replace(/Ú/g, "U'");
			refinedRNir = refinedRNir.replace(/Í/g, "I'");
			//handle parethesis with dots
			if (article.testo.indexOf(`((${refinedRNir}))`) >= 0) {
				article.testo = article.testo.replace(`((${refinedRNir}))`, '');
			} else if (article.testo.indexOf(`((${refinedRNir})).`) >= 0) {
				article.testo = article.testo.replace(`((${refinedRNir})).`, '');
			} else if (article.testo.indexOf(`((${refinedRNir}.))`) >= 0) {
				article.testo = article.testo.replace(`((${refinedRNir}.))`, '');
			} else if (article.testo.indexOf(`(${refinedRNir}).`) >= 0) {
				article.testo = article.testo.replace(`(${refinedRNir}).`, '');
			} else if (article.testo.indexOf(`(${refinedRNir})`) >= 0) {
				article.testo = article.testo.replace(`(${refinedRNir})`, '');
			} else if (article.testo.indexOf(`(${refinedRNir}.)`) >= 0) {
				article.testo = article.testo.replace(`(${refinedRNir}.)`, '');
			} else if (article.testo.indexOf(refinedRNir) >= 0) {
				article.testo = article.testo.replace(refinedRNir, '');
			}
		}
		return article;
	}

	getChapterOrSectionMathces(stringIn) {
		return this.parserUtils.getChapterSectionStatus(stringIn);
	}

	//TO-DO: create article Art. num. and remove from testo
	getArticleText() {
		return this.articletext;
	}

	getFirstArticle(jsonIn) {
		//Are always 2 level object??
		if (jsonIn.articolato
			&& jsonIn.articolato.elementi[0]
			&& jsonIn.articolato.elementi[0].elementi[0]) {
			return jsonIn.articolato.elementi[0].elementi[0];
		}
		return {};
	}

	getLastArticle(jsonIn) {
		//Are always 2 level object??
		if (jsonIn.articolato
			&& jsonIn.articolato.elementi
			&& jsonIn.articolato.elementi.length > 0) {
			return jsonIn.articolato.elementi[jsonIn.articolato.elementi.length - 1].elementi[jsonIn.articolato.elementi[jsonIn.articolato.elementi.length - 1].elementi.length - 1];
		}
		return {};
	}
}

module.exports = ArticleUtils;
