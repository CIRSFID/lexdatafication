/*
 * Copyright (c) 2014 - Copyright holders CIRSFID and Department of
 * Computer Science and Engineering of the University of Bologna
 *
 * Authors:
 * Monica Palmirani – CIRSFID of the University of Bologna
 * Fabio Vitali – Department of Computer Science and Engineering of the University of Bologna
 * Luca Cervone – CIRSFID of the University of Bologna
 *
 * Permission is hereby granted to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The Software can be used by anyone for purposes without commercial gain,
 * including scientific, individual, and charity purposes. If it is used
 * for purposes having commercial gains, an agreement with the copyright
 * holders is required. The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders and authors shall not be used in advertising or otherwise to
 * promote the sale, use or other dealings in this Software without prior
 * written authorization.
 *
 * The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes
 * software developed by University of Bologna (CIRSFID and Department of
 * Computer Science and Engineering) and its authors (Monica Palmirani,
 * Fabio Vitali, Luca Cervone)", in the same place and form as other
 * third-party acknowledgments. Alternatively, this acknowledgment may
 * appear in the software itself, in the same form and location as other
 * such third-party acknowledgments.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import sorty from 'sorty';
import ParserUtils from '../utils/ParserUtils';

class HierarchyHandler {
	constructor(elementsArray) {
		this.elements = elementsArray || [];
		this.flatHierarchy = [];
		this.refinedHierarchy = [];
		this.parserUtils = new ParserUtils();
	}

	generateArticolatoHierarchyObject(articolatoObj) {
		const parsingNumNir = articolatoObj.numNir;
		const parsedNir = this.parserUtils.getNumNirParsedStatus(parsingNumNir);
		const elementiArticolato = []; let parseNirContent = []; let elems = []; let curNumNir;
		//recreates elementoarticolato nesting for all childs
		for (let z = 0; z < articolatoObj.elementi.length; z++) {
			elementiArticolato.push({
				'elementoArticolato': articolatoObj.elementi[z]
			});
		}

		for (let i = 0; i < parsedNir.length; i++) {
			if (i === 0) {
				curNumNir = parsingNumNir.substring(parsedNir[i].offset.end, parsingNumNir.length);
				curNumNir = curNumNir.replace(/[*-]/g, ' ').trim();
				curNumNir = curNumNir.replace(/[* ]/g, ' ').trim();
				curNumNir = curNumNir.replace(/\s+/g, ' ').trim();
				elems = elementiArticolato;
			} else {
				curNumNir = parsingNumNir.substring(parsedNir[i].offset.end, parsedNir[i - 1].offset.start);
				curNumNir = curNumNir.replace(/[*-]/g, ' ').trim();
				curNumNir = curNumNir.replace(/[* ]/g, ' ').trim();
				curNumNir = curNumNir.replace(/\s+/g, ' ').trim();
				elems = [];
			}
			parseNirContent.push({
				elementoArticolato: {
					idInterno: articolatoObj.idInterno,
					rubricaNir: curNumNir,
					livello: articolatoObj.livello,
					numNir: parsedNir[i].fullString,
					gruppoInterno: articolatoObj.gruppoInterno,
					idNir: articolatoObj.idNir,
					nomeNir: this.getNumNirType(parsedNir[i].fullString).name,
					level: this.getNumNirType(parsedNir[i].fullString).level,
					elementi: elems
				}
			});
		}
		return parseNirContent.reduceRight((prev, next) => {
			prev.elementoArticolato.elementi.push(next);
			return prev;
		});
	}

	setCorrectLevel(elemToInsTo, elem) {
		if (elemToInsTo.elementi.length === 0 ||
			elemToInsTo.elementi[elemToInsTo.elementi.length - 1].elementoArticolato.level === elem.level ||
			elemToInsTo.elementi[elemToInsTo.elementi.length - 1].elementoArticolato.nomeNir === 'articolo') {
			elemToInsTo.elementi.push({
				elementoArticolato: elem
			});
		} else {
			this.setCorrectLevel(elemToInsTo.elementi[elemToInsTo.elementi.length - 1].elementoArticolato, elem);
		}
	}

	getNumNirType(stringToParse) {
		const str = stringToParse.toLowerCase();
		if (str.indexOf('libro') >= 0) {
			return {
				name: 'libro',
				level: 1
			};
		}
		if (str.indexOf('parte') >= 0) {
			return {
				name: 'parte',
				level: 2
			};
		}
		if (str.indexOf('titolo') >= 0 || str.indexOf('tit.') >= 0) {
			return {
				name: 'titolo',
				level: 3
			};
		}
		if (str.indexOf('capo') >= 0) {
			return {
				name: 'capo',
				level: 4
			};
		}
		if (str.indexOf('capitolo') >= 0) {
			return {
				name: 'capo',
				level: 4
			};
		}
		if (str.indexOf('sezione') >= 0 || str.indexOf('sez.') >= 0) {
			return {
				name: 'sezione',
				level: 5
			};
		}
	}
}

module.exports = HierarchyHandler;
