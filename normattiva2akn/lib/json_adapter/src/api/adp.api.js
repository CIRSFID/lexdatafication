import JsonHandler from '../imports/JsonHandler';
//json test data file:
import jsonData from '../../test/resources/act_test_data/000A0013_0-00-00.json';

const jsonAdapterNir = {
	translateJson: (jsonDataIn, fileName) => {
		let jsonHandler = new JsonHandler(jsonDataIn);
		return jsonHandler.generateJSON();
	}
}

export default jsonAdapterNir;
