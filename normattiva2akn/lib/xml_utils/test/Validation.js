var fs = require('fs');
var path = require('path');
var chai = require('chai');

var validate = require('../').validate;
var expect = chai.expect;

var getContent = function(file) {
	return fs.readFileSync(path.join(__dirname,  file), { encoding: 'utf-8' });
}

describe('Should validate simple akn file', () => {
	it('should validate with success', (done) => {
		var akn = getContent('validAknSample.xml');
		validate(akn, (err, result) => {
			expect(err).to.be.null;
			expect(result.success).to.be.true;
			done();
		});
	});
	it('should return failure', (done) => {
		var akn = getContent('notValidAknSample.xml');
		validate(akn, (err, result) => {
			expect(err).to.be.null;
			expect(result.success).to.be.false;
			expect(result.errors).not.to.be.undefined;
			expect(result.errors.length).to.equal(12);
			done();
		});
	});
});