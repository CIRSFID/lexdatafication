<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output omit-xml-declaration="yes"/>
 <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <xsl:apply-templates />
  </xsl:template>
 
  <xsl:template match="*[local-name() != 'role']">
      <xsl:element name="{local-name(.)}">
            <xsl:apply-templates />
      </xsl:element>
  </xsl:template>

  <xsl:template match="*[local-name() = 'role']">
      <xsl:element name="ruolo">
            <xsl:apply-templates />
      </xsl:element>
  </xsl:template>

</xsl:stylesheet>