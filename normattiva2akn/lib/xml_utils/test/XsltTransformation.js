var fs = require('fs');
var path = require('path');
var chai = require('chai');

var transform = require('../').transform;
var expect = chai.expect;

var getContent = function(file) {
	return fs.readFileSync(path.join(__dirname,  file), { encoding: 'utf-8' });
}

describe('Should transform', () => {
	it('should change role to ruolo', (done) => {
		var akn = getContent('validAknSample.xml');
		var xslt = getContent('TransformSample.xsl');
		transform(akn, xslt, (err, result) => {
			expect(err).to.be.undefined;
			expect(result).not.to.be.undefined;
			var roleCount = result.match(/<role>/g);
			roleCount = roleCount && roleCount.length || 0;
			expect(roleCount).to.equal(0);
			var ruoloCount = result.match(/<ruolo>/g);
			ruoloCount = ruoloCount && ruoloCount.length || 0;
			expect(ruoloCount).to.equal(4);
			done();
		});
	});
});