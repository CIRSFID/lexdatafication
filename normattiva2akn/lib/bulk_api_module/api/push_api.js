import express from 'express';
import bodyParser from 'body-parser';
import streamifier from 'streamifier';
import backend_exist from '../backends/exist/backend_exist';
import ipzsBulk from 'lib/ipzs-api';
import dg from 'lib/document_generator';

const router = express.Router();
const jsonParser = bodyParser.json({ limit: '50mb' });

//Retrive data from the resource field in request
router.post('/', jsonParser, (request, response) => {

    //TO-DO: elaborete/encode request params ---
    //supposed to be:
    //request.body.docDate
    //request.body.numRed
    //request.body.vigDate
    //request.body.originalDoc
    //------------------------------------------

    if (!request.body.docDate || !request.body.numRed) {
        throw new Error('Invalid request, empty fields');
    }
    
    //download resource from ipzs
    ipzsBulk.downloadDoc(request.body.docDate,
        request.body.numRed,
        request.body.vigDate,
        request.body.originalDoc,
        (err, res) => {            
            if (err) {
                console.log(err);
                response.status(500);
                return response.end('Errore ricezione documento');
            }
            try {
                if (res.errore) {
                    response.status(500);
                    response.end(res.errore);
                    return;
                }
                //translate document
                dg.translateDoc(res)
                    .then((res) => {
                        //save document into exist
                        let input = streamifier.createReadStream(res);                        
                        //TO-DO: elaborete/encode request params to 
                        //path to use for storing docs in exist ? 
                        //-------------------------------------------                                                
                        //supposed to be:
                        //request.body.docDate
                        //request.body.numRed
                        //request.body.vigDate
                        //request.body.originalDoc
                        //-> request.body.numRed_request.body.docDate
                        //-------------------------------------------
                        let resourceSaveName = request.body.numRed + '_' + request.body.docDate;
                        backend_exist.putFile(input, true, '', resourceSaveName, (err) => {
                            if(err) {                                
                                return response.end(err);
                            }    
                            response.end("SAVED: " + resourceSaveName);    
                        });                            
                    })
                    .catch((err) => {                    
                        response.end(err);
                    });                    
            } catch (err){

            }
        });    
});

exports.router = router;
