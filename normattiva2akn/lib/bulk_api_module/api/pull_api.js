import express from 'express';
import bodyParser from 'body-parser';
import backend_exist from '../backends/exist/backend_exist';

const router = express.Router();
const jsonParser = bodyParser.json({ limit: '50mb' });

//Retrive data from the resource field in request
router.post('/', jsonParser, (request, response) => {
    console.log(request.body)
    if (!request.body.resource) {
        throw new Error('Invalid request, empty fields');
    }
    //TO-DO: elaborete/encode resource uri TO-DO ---
    let path = request.body.resource;
    let filename = path.replace(/^.*[\\\/]/, '');
    console.log("path ", path);
    console.log("filename ", filename);
    //---------------------------------------
    backend_exist.checkIfExist('/', filename, (exist) => {
        if (!exist){ 
            throw new Error('Resource does not exist');
        } else {
            backend_exist.getFile(response, '/', filename, (err) =>{
                if (err == 404)
                    throw new Error(`404 the file ${name} does not exist`);
                else if (err)
                    throw err;
            });
        }
    });
});

exports.router = router;
