import async from 'async';
import fs from 'fs';
import validateUtils from './validate_utils';
import nmt from 'lib/document_generator';

const convertValidateAndStore = (fileObj, doneCb) => {
    async.waterfall([
        (cb) => {
            cb(null, fileObj)
        },
        convertSingleFile,
        validateUtils.validateAknCb,
        (fileObj, cb) => {
            //writes xml content file
            fs.writeFile(fileObj.uploadDir + '/' + fileObj.fileName + '.xml', fileObj.xmlContent, (err) => {
                if (err) {
                    cb(err);
                }
                //writes .validation.json file 
                fs.writeFile(fileObj.uploadDir + '/' + fileObj.fileName + '.validation.json', fileObj.validationRes, (err) => {
                    if (err) {
                        cb(err);
                    }
                    cb(null, fileObj);
                }); 
            });
        }
    ], doneCb)
}

const convertSingleFile = (fileObj, cb) => {
    nmt.translateDoc(fileObj.jsonContent)
        .then((res) => {
            fileObj.xmlContent = res;
            cb(null, fileObj);
        })
        .catch((err) => {
            cb(err);
        });
}


exports.convertValidateAndStore = convertValidateAndStore;