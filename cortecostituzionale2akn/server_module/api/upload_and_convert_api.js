import express from 'express';
import fs from 'fs';
import path from 'path';
import async from 'async';
import multer from 'multer';
import promiseSequential from 'promise-sequential';
import http from 'http';
import jsonfile from 'jsonfile';
import xmlFromatter from 'xml-formatter';
import zipUtils from '../utils/zip_utils';
import conversionUtils from '../utils/convertion_utils';
import splitUtils from '../utils/split_utils';
import bufferReplace from 'buffer-replace';
import xmlInputRefiners from '../imports/XmlInputRefiners';



const router = express.Router();
const uploadDir = process.env.UPLOAD_DIR || path.join(__dirname, '../uploads');

//multipart upload middleware
const fileFilterHandler = (req, file, cb) => {
    if (file && file.originalname.split('.').length) {
        const fileExtension = file.originalname.split('.')[file.originalname.split('.').length - 1];
        const allowedExt = ['xml', 'zip'];
        const allowedMymeTypes = [
            'text/xml',
            'application/xml',
            'application/json',
            'application/javascript',
            'application/zip',
            'application/x-zip-compressed',
            'application/octet-stream'];
        if (allowedExt.indexOf(fileExtension.toLowerCase()) !== -1 && allowedMymeTypes.indexOf(file.mimetype) !== -1) {
            cb(null, true);
        } else {
            cb('Invalid file type');
        }
    } else {
        cb(null, true);
    }
};
const multerUpload = multer({ fileFilter: fileFilterHandler });

//File json or zip request with conversion
router.post('/', multerUpload.single('file'), (request, response) => {
    if (request.file.mimetype === 'application/zip' ||
        request.file.mimetype === 'application/x-zip-compressed') {
        zipUtils.createFilesFromZip(request.file.buffer, (err, unzippedEntries) => {
            if (err) {
                throw err;
            }
            const promiseConversionArray = [], validateContentsPromiseArray = [], fileList = [], allEntries = [];
            //create directory timestamp
            let currentTs = new Date().getTime().toString();
            let currentUploadDir = path.join(uploadDir, currentTs);             
            fs.mkdir(currentUploadDir, (err) => {
                if (err) {                    
                    response.status(500);
                    return response.end('Errore creazione directory!');
                }
                //prepare fileList.json content and add the current upload dir to the zip file
                unzippedEntries = unzippedEntries.map((entry) => {
                    //takes only filename without path              
                    let filename = entry.fileName.split('/')[entry.fileName.split('/').length - 1];
                    //remove extension
                    let fileNameNoExtArray = filename.split('.');
                    let fileNameNoExt;
                    fileNameNoExtArray.pop();
                    fileNameNoExt = fileNameNoExtArray.join('');                                        
                    return {
                        fileName: fileNameNoExt,
                        content: xmlInputRefiners.refineInput(entry.content), //refining input
                        uploadDir: currentUploadDir
                    }
                });
                splitUtils.splitFileArray(unzippedEntries, 'pronuncia')
                    .then(res => {
                        res.forEach((fileObj) => {                            
                            fileObj.documents.forEach((splitCnt, id) => {
                                fileList.push(fileObj.fileName + '_s' + ++id);
                                allEntries.push({
                                    fileName: fileObj.fileName + '_s' + id,
                                    content: splitCnt,
                                    uploadDir: fileObj.uploadDir
                                });
                            })
                        });
                        //write fileList.json
                        jsonfile.writeFile(currentUploadDir + '/fileList.json', { content: fileList }, (err) => {
                            if (err) {
                                response.status(500);
                                return response.end('Errore creazione fileList.json!');
                            }
                            response.status(200);
                            response.end(JSON.stringify({ ts: currentTs.toString() }));
                            //here starts the async operation, and the responce is retured to the server
                            //start to convert and validate every single file    
                            async.mapSeries(allEntries, conversionUtils.convertXSLTValidateAndStore, (err, content) => {
                                if (err) {
                                    console.log(err);
                                }
                                console.log("Zip conversion, split and translation ended!");
                                return;
                            });
                        });
                    });                
            });            
        });
    } else {
        try {
            const resFiles = [];            
            const fileNameNoExt = request.file.originalname.substr(0, request.file.originalname.lastIndexOf('.'))
                || request.file.originalname;
            let currentTs = new Date().getTime().toString();
            let currentUploadDir = path.join(uploadDir, currentTs);
            let fileList = []; let allEntries = [];     
            //buffer content replacement, not handled by carachter encoding, direct to buffer
            let fileData = xmlInputRefiners.replaceFromBuffer(request.file.buffer).toString('utf8');            
            //refining input                       
            fileData = xmlInputRefiners.refineInput(fileData);   
            //here generate the splitted content
            splitUtils.createSplittedArray(fileData, 'pronuncia')
                .then((res) => {  
                    if (res.documents.length > 0){
                        fs.mkdir(currentUploadDir, (err) => {
                            if (err) {
                                response.status(500);
                                return response.end('Errore creazione directory!');
                            }
                            //prepare fileList.json content and add the current upload dir to the zip file
                            allEntries = res.documents.map((fileContent, id) => {                                                            
                                fileList.push(fileNameNoExt + '_s' + ++id);
                                return {
                                    fileName: fileNameNoExt + '_s' + id,
                                    content: fileContent,
                                    uploadDir: currentUploadDir
                                }       
                            });
                            //write fileList.json
                            jsonfile.writeFile(currentUploadDir + '/fileList.json', { content: fileList }, (err) => {
                                if (err) {
                                    response.status(500);
                                    return response.end('Errore creazione fileList.json!');
                                }
                                    response.status(200);
                                    response.end(JSON.stringify({ ts: currentTs.toString() }));
                                    //here starts the async operation, and the responce is retured to the server
                                    //start to convert and validate every single file    
                                    async.mapSeries(allEntries, conversionUtils.convertXSLTValidateAndStore, (err, content) => {
                                        if (err) {
                                            console.log(err);
                                        }
                                        console.log("Split and conversion end!");
                                        return;
                                });     
                            });
            
                        }); 
                    }           
            }).catch((err) => {
                console.log(err)
                response.status(500);
                return response.end('Errore split file');
            });            
        } catch (err) {
            console.log(err)
            response.status(500);
            return response.end('Errore traduzione file');
        }
    }
});

exports.router = router;
