import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';
import path from 'path';
import async from 'async';
import readFsUtils from '../utils/read_fs_utils';
import zipUtils from '../utils/zip_utils';

const router = express.Router();
const uploadDir = process.env.UPLOAD_DIR || path.join(__dirname, '../uploads');

router.get('/:timestamp', (request, response) => {
    //console.log("-> ", request.params.timestamp);
    //read fileList.json first
    let tsDir = path.join(uploadDir, '/' + request.params.timestamp);        
    if (fs.existsSync(tsDir)) {        
        zipUtils.createZipFromDir(tsDir, (err, res) => {
            if(err) {
                response.status(500);
                return response.end('Errore creazione zip!');        
            }
            return response.end(new Buffer(res, 'binary'))
        });
    } else {
        //send error
        response.status(500);
        return response.end('Errore lettura lista file!');
    }
});	

exports.router = router;
