import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class MotivationFormulaParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				motivationFormula: super.createInlineMatchProp(matchProps.mp, 'motivationFormula'),				
				fullString: matchProps.mp[0]
			};
			matchObj.fullString = matchObj.fullString.substring(0, matchObj.fullString.indexOf(matchObj.conclusionInitList))
			matches.push(matchObj);
		});
		return matches;
	}
}

module.exports = MotivationFormulaParser;
