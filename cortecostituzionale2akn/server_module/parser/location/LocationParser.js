import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class LocationParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				location: super.createInlineMatchProp(matchProps.mp, 'locationNames'),
				fullString: matchProps.mp[0]
			};
			matches.push(matchObj);
		});
		return matches;
	}
}

module.exports = LocationParser;
