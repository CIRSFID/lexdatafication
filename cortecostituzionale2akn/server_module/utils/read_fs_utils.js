import async from 'async';
import fs from 'fs';

const readFilesXML_VLD_FromDisk = (fileObj, doneCb) => {
    async.waterfall([
        (cb) => {
            cb(null, fileObj)
        },
        readXMLContent,
        readValidationContent,
        (err, fileObj, cb) => {            
            if(err){
                fileObj.error = err;
            } else {
                fileObj.error = "NONE";
            }
            cb(null, fileObj);            
        }
    ], doneCb)
}

const readFilesVLD_FromDisk = (fileObj, doneCb) => {
    async.waterfall([
        (cb) => {
            cb(null, null, fileObj)
        },
        readValidationData,        
        (err, fileObj, cb) => {
            if (err) {
                fileObj.error = err;
            } else {
                fileObj.error = "NONE";
            }                                    
            cb(null, fileObj);
        }
    ], doneCb)
}

const readXMLContent = (fileObj, cb) => {    
    fs.readFile(fileObj.xmlPath, (err, data) => {
        if (err) {            
            cb(null, "READ ERROR", fileObj);  
        }else {
            fileObj.xmlContent = data.toString();            
            cb(null, null, fileObj);
        }        
    });
}

const readValidationContent = (err, fileObj, cb) => {
    if(err){        
        cb(null, "READ ERROR", fileObj);  
    } else {
        fs.readFile(fileObj.validPath, (err, data) => {
            if (err) {
                cb(null, "READ ERROR", fileObj);  
            } else {
                fileObj.validationObj = data.toString();   
                cb(null, null, fileObj);
            }            
        });
    }    
}

const readValidationData = (err, fileObj, cb) => {    
    fs.readFile(fileObj.validPath, (err, data) => {
        if (err) {            
            cb(null, "READ ERROR", fileObj);
        } else {
            //remove unused properties
            delete fileObj.validPath;
            try {
                fileObj.isValid = JSON.parse(data.toString()).success;
                cb(null, null, fileObj);    
            } catch (err) {
                fileObj.isValid = false;
                cb(null, null, fileObj);
            }                        
        }
    });    
}

exports.readFilesXML_VLD_FromDisk = readFilesXML_VLD_FromDisk;
exports.readFilesVLD_FromDisk = readFilesVLD_FromDisk;