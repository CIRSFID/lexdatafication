import streamifier from 'streamifier';
import unzip from 'unzipper';
import fs from 'fs';
import path from 'path';
import AdmZip from 'adm-zip';
import xmlInputRefiners from '../imports/XmlInputRefiners';

const createFilesFromZip = (zipFileBuffer, cb) => {
    const fileArray = []; 
    streamifier.createReadStream(zipFileBuffer).pipe(unzip.Parse())
        .on('entry', (entry) => {            
            const fileName = entry.path;            
            const type = entry.type;
            const size = entry.size;
            const fileExtension = fileName.split('.')[fileName.split('.').length - 1];
            if (type === 'Directory' || fileExtension !== 'xml') {
                entry.autodrain();
            } else {
                const bufs = [];
                entry.on('data', (chunk) => {                                        
                    bufs.push(chunk);
                });
                entry.on('end', () => {
                    try {
                        const responseDataBuf = Buffer.concat(bufs);
                        fileArray.push({ fileName, content: xmlInputRefiners.replaceFromBuffer(responseDataBuf).toString('utf8') });
                    } catch (error) {
                        //TO-DO: gestione file non valido (json parse)
                        console.log(error);
                        console.log(fileName);                        
                    }
                });
                entry.on('error', (error) => {
                    console.log("->",error);                    
                    cb(error);
                });
            }
        })
        .on('close', () => {
            cb(null, fileArray);
        })
        .on('error', (error) => {
            cb(error);
        });
};

const createZipFromDir = (dir, cb) => {
    var zip = new AdmZip();
    if (fs.existsSync(dir)) {
        fs.readdir(dir, function (err, filenames) {
            if (err) {
                console.log(err);
                response.status(500);
                return response.end('Errore lettura lista file!');
            }
            let fileList = filenames.filter((el) => el.indexOf('.xml'));
            let zip = new AdmZip();
            fileList.forEach((el) => {                
                zip.addLocalFile(path.join(dir, el));                
            });            
            cb(null, zip.toBuffer());
        });
    } else {
        //send error
        cb(Error("Zip creation directory does not exist"));
    }
}

exports.createFilesFromZip = createFilesFromZip;
exports.createZipFromDir = createZipFromDir;