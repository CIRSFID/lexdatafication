import os
import re

from datetime import datetime
from lxml import etree as ET
from tqdm import tqdm


SRC_DIR = os.path.join(os.pardir, os.pardir, "input_samples", "src")
XSLT_FILE = os.path.join(os.pardir, os.pardir, os.pardir, "dist", "xslt", "CorteCostituzionale2Akn.xsl")
XSLT = ET.parse(XSLT_FILE)
transform = ET.XSLT(XSLT)
INPUT_DIR = os.path.join(os.pardir, "test_input")
OUTPUT_DIR = os.path.join(os.pardir, "test_output")

os.makedirs(INPUT_DIR, exist_ok=True)
os.makedirs(OUTPUT_DIR, exist_ok=True)



def cleanup_file(fname):
    with open(fname, "r", encoding="ISO-8859-1") as f:
        broken_xml = f.read()
    return str(re.sub(r"<xw:alr .+>", "", broken_xml))


def multiple_conversion(fname, splitby="pronuncia"):
    root = ET.parse(fname)
    for i, doc in enumerate(tqdm(root.findall(splitby), desc=fname, leave=False)):
        ecli = doc.find(".//ecli").text
        print(ecli)
        akn = transform(doc)
        inputfile = os.path.join(INPUT_DIR, ecli.replace(":", "-") + ".xml")
        with open(inputfile, "w") as f:
            f.write(str(ET.tostring(akn, pretty_print=True, encoding="unicode")))


def sort_output_files(from_year=1990, to_year=datetime.now().year):
    for file in os.listdir(OUTPUT_DIR):
        if file.endswith(".xml"):
            anno = int(file.split('-')[-2])
            fname = os.path.join(OUTPUT_DIR, file)
            new_dir = os.path.join(OUTPUT_DIR, f"{from_year}_{to_year}")
            if from_year <= anno <= to_year:
                new_fname = os.path.join(new_dir, file)
                os.system(f"cp {fname} {new_fname}")


def convert_all():
    for filename in tqdm(os.listdir(SRC_DIR)):
        if filename.endswith(".xml"):
            # print(filename)
            input_file = os.path.join(SRC_DIR, filename)
            multiple_conversion(input_file)


if __name__ == "__main__":
    convert_all()
    sort_output_files()
