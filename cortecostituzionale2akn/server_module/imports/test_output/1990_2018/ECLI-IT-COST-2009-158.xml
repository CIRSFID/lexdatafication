<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
    
  
    <judgment name="ordinanza">
        
    
        <meta>
            
      
            <identification source="#cirsfid">
                
        
                <FRBRWork>
                    
          
                    <FRBRthis value="/akn/it/judgment/ordinanza/2009/158/!main"/>
                    
          
                    <FRBRuri value="/akn/it/judgment/ordinanza/2009/158/"/>
                    
          
                    <FRBRalias value="ECLI:IT:COST:2009:158" name="ECLI"/>
                    
          
                    <FRBRdate date="06/05/2009" name=""/>
                    
          
                    <FRBRauthor href="#corteCostituzionale"/>
                    
          
                    <FRBRcountry value="it"/>
                    
          
                    <FRBRnumber value="158"/>
                    
        
                </FRBRWork>
                
        
                <FRBRExpression>
                    
          
                    <FRBRthis value="/akn/it/judgment/ordinanza/2009/158/ita@/!main"/>
                    
          
                    <FRBRuri value="/akn/it/judgment/ordinanza/2009/158/ita@/!main"/>
                    
          
                    <FRBRdate date="06/05/2009" name=""/>
                    
          
                    <FRBRauthor href="#corteCostituzionale"/>
                    
          
                    <FRBRlanguage language="ita"/>
                    
        
                </FRBRExpression>
                
        
                <FRBRManifestation>
                    
          
                    <FRBRthis value="/akn/it/judgment/ordinanza/2009/158/ita@/!main.xml"/>
                    
          
                    <FRBRuri value="/akn/it/judgment/ordinanza/2009/158/ita@.xml"/>
                    
          
                    <FRBRdate date="06/05/2009" name=""/>
                    
          
                    <FRBRauthor href="#corteCostituzionale"/>
                    
        
                </FRBRManifestation>
                
      
            </identification>
            
      
            <workflow source="#corteCostituzionale">
                
        
                <step date="19/05/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
                
      
            </workflow>
            
      
            <references source="#cirsfid">
                
        
                <original href="" showAs=""/>
                
        
                <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
                
        
                <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
                
        
                <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
                
        
                <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
                
        
                <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
                
      
                <TLCRole eId="presidente" href="/akn/ontology/role/it/presidente" showAs="Presidente"/>
                <TLCRole eId="giudici" href="/akn/ontology/role/it/giudici" showAs="Giudici"/>
                <TLCRole eId="redattore" href="/akn/ontology/role/it/redattore" showAs="Redattore"/>
                <TLCRole eId="cancelliere" href="/akn/ontology/role/it/cancelliere" showAs="Cancelliere"/>
                <TLCRole eId="direttoreDellaCancelleria" href="/akn/ontology/role/it/direttoreDellaCancelleria" showAs="Direttore della Cancelleria"/>
                <TLCLocation eId="roma" href="/akn/ontology/location/it/roma" showAs="Roma"/>
                <TLCPerson eId="francescoAmirante" href="/akn/ontology/person/it/francescoAmirante" showAs="Francesco AMIRANTE"/>
                <TLCPerson eId="ugoDeSiervo" href="/akn/ontology/person/it/ugoDeSiervo" showAs="Ugo DE SIERVO"/>
                <TLCPerson eId="alfonsoQuaranta" href="/akn/ontology/person/it/alfonsoQuaranta" showAs="Alfonso QUARANTA"/>
                <TLCPerson eId="francoGallo" href="/akn/ontology/person/it/francoGallo" showAs="Franco GALLO"/>
                <TLCPerson eId="gaetanoSilvestri" href="/akn/ontology/person/it/gaetanoSilvestri" showAs="Gaetano SILVESTRI"/>
                <TLCPerson eId="giuseppeTesauro" href="/akn/ontology/person/it/giuseppeTesauro" showAs="Giuseppe TESAURO"/>
                <TLCPerson eId="alessandroCriscuolo" href="/akn/ontology/person/it/alessandroCriscuolo" showAs="Alessandro CRISCUOLO"/>
                <TLCPerson eId="paoloGrossi" href="/akn/ontology/person/it/paoloGrossi" showAs="Paolo GROSSI"/>
            </references>
            
      
            <proprietary source="#cirsfid">
                
        
                <cc:anno_pronuncia>
                    2009
                </cc:anno_pronuncia>
                
        
                <cc:tipologia_pronuncia>
                    O
                </cc:tipologia_pronuncia>
                
        
                <cc:presidente>
                    AMIRANTE
                </cc:presidente>
                
        
                <cc:relatore_pronuncia>
                    Sabino Cassese
                </cc:relatore_pronuncia>
                
        
                <cc:data_decisione>
                    06/05/2009
                </cc:data_decisione>
                
      
            </proprietary>
            
    
        </meta>
        
    
        <header>
            
      
            <block name="">
                <courtType>
                    LA CORTE COSTITUZIONALE
                </courtType>
                composta dai signori: 
                <role refersTo="#presidente" eId="role_0">
                    Presidente
                </role>
                : 
                <person refersTo="#francescoAmirante" eId="person_0">
                    Francesco AMIRANTE
                </person>
                ; 
                <role refersTo="#giudici" eId="role_1">
                    Giudici
                </role>
                : 
                <person refersTo="#ugoDeSiervo" eId="person_1">
                    Ugo DE SIERVO
                </person>
                , Paolo MADDALENA, Alfio FINOCCHIARO, 
                <person refersTo="#alfonsoQuaranta" eId="person_2">
                    Alfonso QUARANTA
                </person>
                , 
                <person refersTo="#francoGallo" eId="person_3">
                    Franco GALLO
                </person>
                , Luigi MAZZELLA, 
                <person refersTo="#gaetanoSilvestri" eId="person_4">
                    Gaetano SILVESTRI
                </person>
                , Sabino CASSESE, Maria Rita SAULLE, 
                <person refersTo="#giuseppeTesauro" eId="person_5">
                    Giuseppe TESAURO
                </person>
                , Paolo Maria NAPOLITANO, Giuseppe FRIGO, 
                <person refersTo="#alessandroCriscuolo" eId="person_6">
                    Alessandro CRISCUOLO
                </person>
                , 
                <person refersTo="#paoloGrossi" eId="person_7">
                    Paolo GROSSI
                </person>
                ,
            </block>
            
    
        </header>
        
    
        <judgmentBody>
            
      
            <introduction>
                
        
                <division>
                    
          
                    <content>
                        
            
                        <p>
                            ha pronunciato la seguente
                            <docType>
                                ORDINANZA
                            </docType>
                            nel giudizio di legittimità costituzionale dell'art. 4, comma 2-bis, del 
                            <ref href="/akn/it/act/decreto.legge/2005-06-30/115/!main">
                                decreto-legge 30 giugno 2005, n. 115
                            </ref>
                             (Disposizioni urgenti per assicurare la funzionalità di settori della pubblica amministrazione) convertito, con modificazioni, dalla 
                            <ref href="/akn/it/act/legge/2005-08-17/168/!main">
                                legge 17 agosto 2005, n. 168
                            </ref>
                            , promosso dal Consiglio di giustizia amministrativa per la Regione siciliana nel procedimento vertente tra il Ministero della Giustizia ed altri e G. C., con 
                            <ref href="/akn/it/act/ordinanza/2008-10-07/!main">
                                ordinanza del 7 ottobre 2008
                            </ref>
                            , iscritta al n. 432 del registro ordinanze del 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 2, prima serie speciale, dell'anno 2009. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 6 maggio 2009 il Giudice relatore Sabino Cassese.  
    Ritenuto che il Consiglio di giustizia amministrativa per la Regione siciliana ha sollevato, con riferimento agli articoli 3, 24, 25, 103, 111, secondo comma, 113 e 125 della Costituzione, questione di legittimità costituzionale dell'art. 4, comma 2-bis, del 
                            <ref href="/akn/it/act/decreto.legge/2005-06-30/115/!main">
                                decreto-legge 30 giugno 2005, n. 115
                            </ref>
                             (Disposizioni urgenti per assicurare la funzionalità di settori della pubblica amministrazione), convertito, con modificazioni, dalla 
                            <ref href="/akn/it/act/legge/2005-08-17/168/!main">
                                legge 17 agosto 2005, n. 168
                            </ref>
                            , a norma del quale «conseguono ad ogni effetto l'abilitazione professionale o il titolo per il quale concorrono i candidati, in possesso dei titoli per partecipare al concorso, che abbiano superato le prove d'esame scritte ed orali previste dal bando, anche se l'ammissione alle medesime o la ripetizione della valutazione da parte della commissione sia stata operata a seguito di provvedimenti giurisdizionali o di autotutela»; 
    che il Collegio rimettente espone che dinanzi a esso pende il ricorso in appello contro la sentenza con la quale il Tribunale amministrativo regionale della Sicilia, sede di Catania, sez. IV, ha accolto il ricorso presentato da un candidato che non era stato ammesso alle prove orali dell'esame di abilitazione all'esercizio della professione forense, per l'insufficiente punteggio riportato nelle prove scritte; 
    che l'appellato, il quale – a seguito della sentenza impugnata – ha superato le prove orali e si è iscritto all'Albo degli avvocati, eccepisce l'improcedibilità dell'appello, facendo riferimento tra l'altro alla disposizione impugnata; 
    che, in ordine alla rilevanza della questione di legittimità costituzionale, il Collegio rimettente osserva che, se si dovesse applicare la disposizione impugnata, l'atto di appello sarebbe improcedibile in quanto essa, comportando il definitivo conseguimento dell'abilitazione professionale da parte del candidato, farebbe cessare ex lege ogni interesse dell'amministrazione appellante alla decisione; 
    che, secondo il rimettente, la disposizione impugnata – rendendo avulsa la misura cautelare dal giudizio di merito – viola, in primo luogo, l'art. 3 Cost., poiché lede l'interesse dell'amministrazione che ha indetto il concorso o la sessione d'esame a far sì che la misura cautelare eventualmente accordata conservi il suo carattere strumentale rispetto alla decisione di merito e, consolidando gli effetti prodotti dall'ordinanza cautelare favorevole all'interessato, si pone in contrasto con il dovere dell'amministrazione di tutelare la par condicio degli esaminandi; 
    che la violazione dell'art. 3 Cost. viene prospettata anche sotto il profilo dell'eguaglianza e della ragionevolezza, in quanto la disposizione impugnata farebbe sì che la possibilità, per la parte soccombente, di ottenere la decisione di merito sull'appello dipenda da un elemento di fatto come i tempi entro i quali l'amministrazione dà esecuzione alla decisione del giudice di primo grado; 
    che la disposizione violerebbe, in secondo luogo, gli artt. 24 e 111 Cost., che garantiscono il diritto al contraddittorio e la sua effettività, introducendo un modello di processo nel quale viene attribuita efficacia di giudicato all'esito di un giudizio che non è neppure a cognizione piena; 
    che la violazione degli artt. 24 e 111 Cost. viene prospettata anche sotto il profilo della parità delle parti nel processo, non essendo accettabile che, secundum eventum litis, una sola parte venga privata del diritto di appello; 
    che la disposizione impugnata violerebbe, in terzo luogo, l'art. 25 Cost., in quanto la rivalutazione delle prove scritte potrebbe avvenire per effetto di una decisione cautelare emessa da un giudice incompetente, e in quanto la possibilità che sia precluso il giudizio di appello distoglierebbe la parte pubblica dal suo giudice naturale precostituito per legge, cioè – in grado di appello – il Consiglio di Stato; 
    che la disposizione violerebbe, in quarto luogo, gli artt. 24, 111 e 113 Cost., in quanto la decisione cautelare favorevole al candidato diverrebbe sostanzialmente inimpugnabile una volta che egli abbia superato le prove concorsuali scritte e orali, con ciò verificandosi, da un lato, che un'ordinanza di sospensiva produca effetti definitivi e irreversibili e, dall'altro lato, che la parte interessata perda la possibilità di ottenere il riesame della decisione cautelare, ogni qualvolta la rivalutazione con esito positivo delle prove scritte si concluda – com'è nella normalità dei casi – prima della decisione sull'appello avverso l'ordinanza cautelare; 
    che la disposizione violerebbe, in quinto luogo, gli artt. 111 e 113 Cost. sulla garanzia del doppio grado di giurisdizione, oltre che i principi comunitari relativi alla qualità e all'efficacia della tutela giurisdizionale nell'ordinamento comunitario; 
    che la violazione dell'art. 113 Cost. deriverebbe altresì dalla lesione del principio di non limitabilità della tutela giurisdizionale a particolari mezzi di impugnazione o per determinate categorie di atti, che conseguirebbe all'inappellabilità delle statuizioni rese dal giudice di prime cure; 
    che la disposizione impugnata violerebbe, infine, l'art. 125 Cost., impedendo di fatto, almeno potenzialmente, lo svolgimento del giudizio di appello in tutti i casi di accoglimento in prime cure di ricorsi avverso l'esito negativo degli esami e facendo sì che i tribunali amministrativi regionali operino come giudici di unico grado; 
    che nel giudizio dinanzi alla Corte si è costituito il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, per affermare l'infondatezza della questione, in quanto relativa a una norma di sanatoria, ragionevolmente introdotta per esigenze organizzative e di celerità, per evitare gravi disfunzioni e consolidare posizioni acquisite; 
    che la difesa statale afferma che non vi è violazione né del principio di eguaglianza, in quanto il presupposto per l'applicazione della disposizione è comunque il superamento dell'esame, né del diritto di difesa, in quanto la disposizione si applica solo agli esami di abilitazione, nei quali non vi sono controinteressati, né del principio del giudice naturale, essendo irrilevante che in singoli casi concreti possa essere adito un giudice incompetente, né del principio del doppio grado di giurisdizione, in quanto la disposizione non preclude la proposizione dell'appello, ma può solo renderlo improcedibile. 
    Considerato che il Consiglio di giustizia amministrativa per la Regione siciliana ha sollevato, con riferimento agli articoli 3, 24, 25, 103, 111, secondo comma, 113 e 125 della Costituzione, questione di legittimità costituzionale dell'art. 4, comma 2-bis, del 
                            <ref href="/akn/it/act/decreto.legge/2005-06-30/115/!main">
                                decreto-legge 30 giugno 2005, n. 115
                            </ref>
                             (Disposizioni urgenti per assicurare la funzionalità di settori della pubblica amministrazione), convertito, con modificazioni, dalla 
                            <ref href="/akn/it/act/legge/2005-08-17/168/!main">
                                legge 17 agosto 2005, n. 168
                            </ref>
                            , a norma del quale «conseguono ad ogni effetto l'abilitazione professionale o il titolo per il quale concorrono i candidati, in possesso dei titoli per partecipare al concorso, che abbiano superato le prove d'esame scritte ed orali previste dal bando, anche se l'ammissione alle medesime o la ripetizione della valutazione da parte della commissione sia stata operata a seguito di provvedimenti giurisdizionali o di autotutela»; 
    che la questione è manifestamente infondata; 
    che, infatti, analoga questione, sollevata dal Consiglio di giustizia amministrativa per la Regione siciliana con una precedente ordinanza, è già stata dichiarata infondata da questa Corte con la 
                            <ref href="/akn/it/act/sentenza/xxxx-xx-xx/108/!main">
                                sentenza n. 108 del 2009
                            </ref>
                            ; 
    che, in tale occasione, la Corte ha osservato che la disposizione impugnata «ha lo scopo di evitare che il superamento delle prove di un esame di abilitazione venga reso inutile dalle vicende processuali successive al provvedimento, con il quale un giudice o la stessa amministrazione, in via di autotutela, abbiano disposto l'ammissione alle prove di esame o la ripetizione della valutazione» e che, per raggiungere questo scopo, essa «rende irreversibili – secondo la giurisprudenza amministrativa – gli effetti del superamento delle prove scritte e orali previste dal bando»; 
    che la Corte ha ritenuto che la disposizione sia il risultato di una scelta operata dal legislatore in sede di bilanciamento di interessi contrapposti; 
    che, infatti, da un lato, vi è «l'interesse alla piena e definitiva verifica della legittimità degli atti compiuti dall'amministrazione nel corso del procedimento di esame e, quindi, della correttezza della precedente valutazione, che abbia in ipotesi condotto all'esclusione del candidato», il quale indurrebbe a consentire la prosecuzione del processo fino alla sua naturale conclusione, e allo stesso esito condurrebbe la piena esplicazione del diritto di difesa di entrambe le parti; 
    che, dall'altro lato, vi sono «l'interesse a evitare che gli esami si svolgano inutilmente, quello a evitare che la lentezza dei processi ne renda incerto l'esito e, soprattutto, l'affidamento del privato, il quale abbia superato le prove di esame e – in ipotesi – avviato in buona fede la relativa attività professionale», oltre a un'esigenza generale di certezza in ordine ai tempi di conclusione dell'accertamento dell'idoneità dei candidati e in ordine ai rapporti instaurati dal candidato nello svolgimento dell'attività professionale; 
    che, come affermato nella menzionata sentenza, il legislatore «ha ritenuto di contemperare i diversi interessi rilevanti, accordando una particolare tutela all'affidamento del cittadino», che «comporta indubbiamente una certa compressione del diritto di difesa, in quanto si introduce una dissimmetria tra le due parti del processo amministrativo eventualmente avviato», ma – alla luce dei principi costituzionali, che non escludono una ragionevole limitazione del diritto di difesa dell'amministrazione – il bilanciamento di interessi non è irragionevole; 
    che, infatti, il diritto di difesa dell'amministrazione «è sì compresso, ma non eliminato, in quanto esso può comunque esplicarsi fino all'eventuale superamento delle prove» e «la sua compressione è giustificata dal fatto che dell'interesse pubblico all'accertamento dell'idoneità del candidato, di cui l'amministrazione stessa è portatrice, la disposizione si fa comunque carico, richiedendo il superamento della prova»; 
    che, in base alle considerazioni che precedono, la Corte ha ritenuto infondate le censure prospettate dal rimettente; 
    che, pertanto, non essendo state proposte censure nuove e diverse da quelle già esaminate da questa Corte, la questione di legittimità costituzionale deve essere dichiarata manifestamente infondata.  
    Visti gli artt. 26, secondo comma, della 
                            <ref href="/akn/it/act/legge/1953-03-11/87/!main">
                                legge 11 marzo 1953, n. 87
                            </ref>
                            , e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.
                        </p>
                        
          
                    </content>
                    
        
                </division>
                
      
            </introduction>
            
      
            <decision>
                
        
                <block name="formulaRito">
                    per questi motivi 
 LA CORTE
                COSTITUZIONALE 
                    <eol/>
                </block>
                
        
                <division>
                    
          
                    <content>
                        
            
                        <p>
                             
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 4, comma 2-bis, del 
                            <ref href="/akn/it/act/decreto.legge/2005-06-30/115/!main">
                                decreto-legge 30 giugno 2005, n. 115
                            </ref>
                             (Disposizioni urgenti per assicurare la funzionalità di settori della pubblica amministrazione), convertito, con modificazioni, dalla 
                            <ref href="/akn/it/act/legge/2005-08-17/168/!main">
                                legge 17 agosto 2005, n. 168
                            </ref>
                            , sollevata, in riferimento agli artt. 3, 24, 25, 103, 111, secondo comma, 113 e 125 della Costituzione, dal Consiglio di giustizia amministrativa per la Regione siciliana con l'ordinanza in epigrafe. 
    
                        </p>
                        
          
                    </content>
                    
        
                </division>
                
      
            </decision>
            
    
        </judgmentBody>
        
    
        <conclusions>
            
      
            <block name="">
                 Così deciso  in 
                <location refersTo="#roma" eId="location_0">
                    Roma
                </location>
                , nella sede della Corte costituzionale, Palazzo della Consulta, il 
                <date date="2009-05-06">
                    6 maggio 2009
                </date>
                .  
F.to:  

                <person refersTo="#francescoAmirante" eId="person_8">
                    Francesco AMIRANTE
                </person>
                , 
                <role refersTo="#presidente" eId="role_2">
                    Presidente
                </role>
                  
Sabino CASSESE, 
                <role refersTo="#redattore" eId="role_3">
                    Redattore
                </role>
                  
Giuseppe DI PAOLA, 
                <role refersTo="#cancelliere" eId="role_4">
                    Cancelliere
                </role>
                  
Depositata in Cancelleria il 
                <date date="2009-05-19">
                    19 maggio 2009
                </date>
                .  
Il 
                <role refersTo="#direttoreDellaCancelleria" eId="role_5">
                    Direttore della Cancelleria
                </role>
                  
F.to: DI PAOLA
            </block>
            
    
        </conclusions>
        
  
    </judgment>
    

</akomaNtoso>