<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/9/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/9/"/>
          <FRBRalias value="ECLI:IT:COST:2003:9" name="ECLI"/>
          <FRBRdate date="13/01/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="9"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/9/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/9/ita@/!main"/>
          <FRBRdate date="13/01/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/9/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/9/ita@.xml"/>
          <FRBRdate date="13/01/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="15/01/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>13/01/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 34, comma 2, del codice di procedura penale, promosso, nell'ambito di un procedimento penale, dal Tribunale di Verona, sezione distaccata di Soave, con ordinanza del 19 ottobre 2001, iscritta al n. 253 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 22, prima serie speciale, dell'anno 2002. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri;  
    udito nella camera di consiglio del 4 dicembre 2002 il Giudice relatore Guido Neppi Modona. 
    Ritenuto che il Tribunale di Verona, sezione distaccata di Soave, ha sollevato, in riferimento agli artt. 3, 25 e 97 della Costituzione, questione di legittimità costituzionale dell'art. 34, comma 2, del codice di procedura penale, come risultante a seguito della sentenza n. 186 del 1992, nella parte in cui prevede l'incompatibilità alla funzione di giudizio del giudice che nella fase degli atti introduttivi del dibattimento abbia respinto la richiesta di applicazione della pena concordata ex art. 444 cod. proc. pen.; 
    che il rimettente premette di aver rigettato la richiesta di applicazione della pena formulata dall'imputato con il consenso del pubblico ministero prima delle formalità di apertura del dibattimento e che - rinviato il processo ad altra udienza - la difesa ha formulato istanza di giudizio abbreviato; 
    che il giudice a quo rileva che, per effetto della sentenza n. 186 del 1992, che ha dichiarato l'illegittimità costituzionale dell'art. 34, comma 2, cod. proc. pen. nella parte in cui non prevede l'incompatibilità a partecipare al giudizio del giudice che prima dell'apertura del dibattimento ha respinto la richiesta di applicazione della pena concordata tra le parti, egli sarebbe incompatibile a giudicare l'imputato nel merito;  
    che ad avviso del rimettente tale ipotesi di incompatibilità si pone in contrasto con la più recente giurisprudenza costituzionale e, in particolare, con l'ordinanza n. 232 del 1999 che - nel dichiarare manifestamente infondata una questione di legittimità costituzionale dell'art. 34 cod. proc. pen. nella parte in cui non prevede l'incompatibilità ad emettere sentenza del giudice del dibattimento che abbia rigettato istanza di oblazione - ha ribadito il principio generale secondo cui l'imparzialità del giudice non può ritenersi &amp;laquo;pregiudicata da una valutazione, anche di merito, compiuta nella medesima fase del procedimento&amp;raquo;, affermando che devono ritenersi &amp;laquo;superate le conclusioni cui è pervenuta questa Corte nella sentenza n. 186 del 1992&amp;raquo;;  
    che il giudice a quo dà atto di aver già sollevato identica questione di legittimità costituzionale, dichiarata manifestamente inammissibile da questa Corte con ordinanza n. 108 del 2001, in quanto diretta a censurare una precedente decisione di accoglimento, ma ritiene di dover nuovamente sollevare la questione di costituzionalità;  
    che, a sostegno della ammissibilità della questione, il giudice a quo rileva che, secondo la giurisprudenza assolutamente maggioritaria della Corte, la reiterazione di eccezioni già sollevate e risolte è sempre stata ritenuta possibile, salvo a dichiararne l'inammissibilità per ragioni di merito, legate alla loro manifesta infondatezza, e che in alcuni casi la Corte ha accolto l'eccezione di incostituzionalità di una norma già dichiarata incostituzionale, &amp;laquo;modificando con la seconda pronunzia l'ambito della prima&amp;raquo;;  
    che, come ripetutamente affermato dalla stessa Corte costituzionale, le sentenze interpretative di rigetto &amp;laquo;cre(a)no - con il loro effetto parzialmente abrogativo della precedente - una nuova norma, suscettibile come ogni altra di censure di costituzionalità&amp;raquo;; 
    che ai sensi dell'art. 23, comma secondo, della legge n. 87 del 1953, il giudice ha il dovere di sollevare questione di costituzionalità ogniqualvolta la stessa non risulti manifestamente infondata;  
    che, quanto al merito della questione, il giudice a quo ribadisce che la incompatibilità al giudizio del giudice che abbia rigettato nella fase degli atti introduttivi del dibattimento la richiesta di pena patteggiata determina una irragionevole disparità di trattamento rispetto a situazioni analoghe (come quella - oggetto dell'ordinanza n. 232 del 1999 - del giudice del dibattimento che abbia rigettato istanza di oblazione), e nello stesso tempo assoggetta irragionevolmente alla medesima disciplina situazioni processuali non comparabili, &amp;laquo;prevedendo l'incompatibilità al giudizio sia del giudice che abbia legittimamente espresso valutazioni di merito nell'ambito della medesima fase processuale, sia del giudice che le abbia espresse nell'ambito di fase processuale diversa&amp;raquo;;  
    che la disciplina censurata violerebbe inoltre i principi del buon andamento della pubblica amministrazione e del giudice naturale precostituito per legge, realizzando per un verso &amp;laquo;un'assurda frammentazione del procedimento&amp;raquo; e per l'altro consentendo alle parti, mediante &amp;laquo;studiata proposizione&amp;raquo; di istanze inaccoglibili di applicazione della pena, di &amp;laquo;"sbarazzarsi" del loro giudice naturale, costringendolo all'astensione&amp;raquo;; 
    che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile e comunque infondata. 
    Considerato che la medesima questione, sollevata in altri procedimenti dallo stesso rimettente, è già stata dichiarata manifestamente inammissibile con le ordinanze n. 58 del 2002, successiva all'ordinanza di rimessione, e n. 108 del 2001, in quanto diretta a censurare una precedente decisione di accoglimento di questa Corte; 
    che le considerazioni svolte dal giudice a quo per superare i profili di inammissibilità non sono pertinenti, posto che ad essere impugnato è proprio l'enunciato normativo frutto della sentenza n. 186 del 1992, e che con l'accoglimento della questione verrebbe ripristinata la norma censurata dalla predetta sentenza; 
    che la questione deve pertanto essere dichiarata manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 34, comma 2, del codice di procedura penale, sollevata, in riferimento agli artt. 3, 25 e 97 della Costituzione, dal Tribunale di Verona, sezione distaccata di Soave, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 gennaio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 15 gennaio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
