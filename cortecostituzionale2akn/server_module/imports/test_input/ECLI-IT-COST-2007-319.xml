<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/319/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/319/"/>
          <FRBRalias value="ECLI:IT:COST:2007:319" name="ECLI"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="319"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/319/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/319/ita@/!main"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/319/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/319/ita@.xml"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="20/07/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>10/07/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 276, primo comma, del codice civile, promosso con ordinanza del 10 luglio 2006 dal Tribunale di Milano sul ricorso proposto da B.I., iscritta al n. 142 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 13, prima serie speciale, dell'anno 2007. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 4 luglio 2007 il Giudice relatore Alfio Finocchiaro. 
    Ritenuto che, con ordinanza del 10 luglio 2006, il Tribunale di Milano ha sollevato questione di legittimità costituzionale dell'art. 276, primo comma, del codice civile, nella parte in cui non prevede la possibilità della nomina di un curatore speciale nei cui confronti promuovere l'azione per la dichiarazione giudiziale della paternità o della maternità naturale in caso di premorienza sia dei presunti padre o madre sia degli eredi, per contrasto con gli artt. 3 e 24 della Costituzione; 
    che il rimettente premette che, con ricorso del 1° giugno 2006, I. B., figlia naturale riconosciuta di M. R. B., ha esposto di essere altresì figlia naturale di L. F. G., nato a Torino il 20 novembre 1903 e deceduto il 2 febbraio 1967; che in data 7 ottobre 1999 era deceduto anche il figlio di L. F. G., A. M. G.; che, in seguito alla pronuncia di ammissibilità ai sensi dell'art. 274 cod. civ. del 12 luglio 2001, la stessa I. B. aveva instaurato il giudizio di accertamento della paternità naturale di L. F. G., convenendo tutti gli eredi legittimi di A. M. G. nonché l'erede testamentaria M. L. E. R.; che nel corso del giudizio era stata effettuata perizia emato-genetica che si era conclusa con l'affermazione del rapporto di filiazione dedotto in giudizio; che il Tribunale di Milano, con sentenza 1° febbraio 2006, aveva tuttavia dichiarato l'inammissibilità della domanda per carenza di legittimazione passiva dei soggetti convenuti, in ossequio al principio da ultimo affermato dalle sezioni unite della Corte di cassazione (sentenza 3 novembre 2005, n. 21287), secondo cui legittimati passivi dell'azione di accertamento di cui all'art. 269 e seguenti cod. civ. sono unicamente, oltre al presunto genitore, i suoi eredi diretti, come espressamente sancito dall'art. 276 cod.civ., e non anche gli eredi degli eredi;  
    che, poste tali premesse, la ricorrente ha chiesto, in via principale, che, in applicazione analogica dell'art. 247 cod. civ., il Tribunale provvedesse alla nomina di un curatore speciale nei confronti del quale potere esercitare l'azione di accertamento della paternità naturale; e, in via subordinata, che il Tribunale sollevasse la questione di legittimità costituzionale dell'art. 276 cod. civ., per violazione degli artt. 3 e 24 della Costituzione; 
    che, secondo il rimettente, non può trovare applicazione, nel caso di specie, il procedimento analogico di cui all'art. 12 delle disposizioni sulla legge in generale richiesto in via principale dalla ricorrente, dal momento che la fattispecie del disconoscimento di paternità, all'interno della quale la norma di cui all'art. 247, quarto comma, cod. civ., ammette la nomina di un curatore speciale in caso di premorienza del presunto padre o della madre o del figlio, non è sovrapponibile, quanto a princípi ispiratori ed a strumenti applicativi, alla dichiarazione giudiziale di paternità o maternità naturale di cui all'art. 269 cod. civ., e, dunque, non sussiste il requisito della eadem ratio; 
    che, nel primo caso, vertendosi in materia di disconoscimento, assume rilievo in via esclusiva la tutela del soggetto interessato a ristabilire la verità in merito alla filiazione, senza conseguenze pregiudizievoli di ordine ereditario nei confronti di soggetti terzi; laddove, nel secondo caso, la dichiarazione giudiziale di paternità può incidere in maniera rilevante su posizioni soggettive di terzi, comportando in particolare la possibilità di riduzione delle quote ereditarie; 
    che l'applicazione analogica – utile per integrare eventuali lacune normative e, dunque, per l'ipotesi in cui una fattispecie non sia espressamente disciplinata – non può essere utilizzata per modificare o alterare il significato della specifica disciplina giuridica di riferimento, dal momento che, nel caso di specie, non è ravvisabile una lacuna, sussistendo invece diverse scelte del legislatore, che ammette la nomina di un curatore speciale per l'azione di disconoscimento di paternità in caso di premorienza del legittimato passivo (art. 274, quarto comma, cod. civ.) e non la ammette invece in alcuna ipotesi per l'azione per la dichiarazione giudiziale di paternità (art. 276 cod. civ.); 
    che, esclusa la possibilità di applicazione analogica dell'art. 247, quarto comma, cod. civ., il rimettente ritiene che la questione di legittimità costituzionale non è manifestamente infondata, in quanto di fatto prospettata dalla stessa sentenza delle sezioni unite della Corte di cassazione n. 21287 del 2005, che ha testualmente affermato che «il fatto, peraltro, che l'azione in esame si consumi, in concreto, nel caso di intervenuta morte del preteso genitore e di tutti i suoi eredi evidenzia, comunque, un punto di debolezza e di perfettibilità dell'attuale disciplina rispetto alle sempre più avvertite esigenze di tutela dell'interesse del figlio naturale all'accertamento della genitorialità, anche per il profilo del suo diritto all'identità personale. In tale prospettiva potrebbe auspicarsi che quella disciplina sia integrata stabilendosi che, nel caso appunto di morte del presunto genitore e in mancanza dei suoi eredi, l'azione possa proporsi, come anche suggerito in dottrina, nei confronti di un curatore nominato dal giudice, analogamente a quanto già previsto dall'ultimo comma dell'art. 247 c.c. ai fini della proponibilità dell'azione di disconoscimento della paternità, nella parallela ipotesi di già intervenuta morte del presunto padre e di mancanza dei litisconsorti necessari indicati nel primo comma della norma stessa. Un'integrazione siffatta, oltre che di un intervento legislativo, potrebbe formare eventualmente oggetto di una pronunzia additiva (in questi termini “a rima obbligata”) della Corte costituzionale»; 
    che, in piena aderenza con tale insegnamento, ritiene il rimettente che la formulazione dell'art. 276 cod. civ., nella parte in cui limita la determinazione dei soggetti passivamente legittimati nell'azione per dichiarazione giudiziale di paternità, in caso di morte del genitore, esclusivamente ai suoi eredi e non anche agli eredi degli eredi, senza ammettere la possibilità della nomina di un curatore speciale nominato dal giudice, si pone in contrasto con il principio di cui all'art. 3 Cost., poiché determina una disparità di trattamento, generale ed in linea di principio e senza alcuna possibilità di diversa valutazione, rispetto a fattispecie simili (quale appunto l'azione di disconoscimento di paternità); e reca altresì vulnus al principio di cui all'art. 24 Cost., in quanto pone limiti alla possibilità di far valere in giudizio il riconoscimento della paternità o maternità naturale, che è, per di più, azione imprescrittibile (art. 270, primo comma, cod.civ.); 
    che la scelta del legislatore di non prevedere in linea di principio la possibilità della nomina di un curatore speciale per l'instaurazione del giudizio di riconoscimento di paternità naturale, con la possibilità dunque per il giudice competente di valutare le specificità della fattispecie, e di prevedere invece la medesima nomina per l'azione di disconoscimento della paternità, può porsi in contrasto con i richiamati princípi costituzionali; 
    che, inoltre, recenti e radicali pronunzie della Corte costituzionale in materia di tutela del riconoscimento della paternità naturale (sentenza 10 febbraio 2006, n. 50, che ha caducato la norma di cui all'art. 274 cod.civ.), si pongono esattamente in questa direzione;  
    che non vi è alcun dubbio sulla sussistenza del requisito della rilevanza poiché il giudizio a quo ha proprio per oggetto la domanda di nomina di un curatore speciale ex art. 276 cod. civ., che non potrebbe essere accolta alla stregua dell'attuale formulazione della normativa di riferimento; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che, in un primo momento, ha concluso per l'infondatezza della questione e, successivamente, ha dichiarato di rinunciare all'intervento. 
    Considerato che il Tribunale di Milano dubita della legittimità costituzionale dell'art. 276, primo comma, del codice civile, nella parte in cui non prevede la possibilità della nomina di un curatore speciale nei cui confronti promuovere l'azione per la dichiarazione giudiziale della paternità o della maternità naturale in caso di premorienza sia dei presunti padre o madre sia degli eredi, per violazione dell'art. 3 della Costituzione, poiché determinerebbe una ingiustificata disparità di trattamento rispetto alla disciplina dell'azione per il disconoscimento della paternità o della maternità, che, all'art. 247 cod. civ., prevede invece la possibilità per il giudice di nominare un curatore in caso di morte del presunto padre o della presunta madre; nonché dell'art. 24 della Costituzione, in quanto limiterebbe al figlio naturale la possibilità di agire per il riconoscimento della paternità o della maternità naturale, in contrasto con l'imprescrittibilità dell'azione prevista dall'art. 270 cod. civ.; 
    che il giudice a quo, dopo avere premesso che lo stesso Tribunale di Milano ha dichiarato, con sentenza, l'inammissibilità dell'azione di dichiarazione giudiziale di paternità per carenza di legittimazione passiva dei soggetti convenuti, si è limitato ad affermare, apoditticamente, la rilevanza della questione, senza motivare in ordine alla persistenza del proprio potere decisorio a séguito della pronuncia di inammissibilità dell'azione, emessa fra le stesse parti, costituente giudicato rilevabile d'ufficio (Corte di cassazione, sentenza 16 gennaio 2004, n. 630); 
    che, peraltro, anche ad ammettere la sussistenza di tale potere in capo al giudice a quo, neppure è motivata l'applicabilità alla fattispecie dell'eventuale auspicata pronuncia di incostituzionalità, in presenza di una sentenza che ha individuato i legittimati passivi dell'azione di cui all'art. 269 cod. civ. nel presunto genitore o, in mancanza di lui, nei suoi eredi diretti; 
    che tali carenze di motivazione sulla rilevanza rendono manifestamente inammissibile la questione sollevata. 
    Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 276, primo comma, del codice civile, sollevata, in riferimento agli artt. 3 e 24 della Costituzione, dal Tribunale di Milano, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 luglio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfio FINOCCHIARO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 20 luglio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
