<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/137/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/137/"/>
          <FRBRalias value="ECLI:IT:COST:2016:137" name="ECLI"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="137"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/137/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/137/ita@/!main"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/137/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/137/ita@.xml"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/06/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Silvana Sciarra</cc:relatore_pronuncia>
        <cc:data_decisione>18/05/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 29 della legge della Provincia autonoma di Bolzano 19 maggio 2015, n. 6 (Ordinamento del personale della Provincia), promosso dal Presidente del Consiglio dei ministri, con ricorso notificato il 24-29 luglio 2015, depositato in cancelleria il 30 luglio 2015 ed iscritto al n. 78 del registro ricorsi 2015.
 	Udito nella camera di consiglio del 18 maggio 2016 il Giudice relatore Silvana Sciarra.
 Ritenuto che, con ricorso spedito il 24 luglio 2015, pervenuto al destinatario il 29 luglio 2015 e depositato il giorno successivo (reg. ric. n. 78 del 2015), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questione principale di legittimità costituzionale dell'art. 29 della legge della Provincia autonoma di Bolzano 19 maggio 2015, n. 6 (Ordinamento del personale della Provincia), in riferimento agli artt. 3, 81, terzo comma, 117, secondo comma, lettera o), e terzo comma, e 119 della Costituzione;
 che l'impugnato art. 29 - il quale, come le altre disposizioni della legge provinciale n. 6 del 2015, è applicabile al «personale della Provincia e degli enti pubblici da essa dipendenti o il cui ordinamento rientra nella competenza legislativa propria o delegata della Provincia» (art. 1 della legge provinciale n. 6 del 2015) - sotto la rubrica «Collocamento a riposo d'ufficio», stabilisce che «Al fine di favorire il ricambio generazionale e il contenimento delle spese di personale degli enti di cui all'articolo 1, la Giunta provinciale stabilisce il collocamento a riposo d'ufficio e la contestuale risoluzione del rapporto di lavoro del personale con diritto a pensione tenendo conto dei seguenti criteri: a) un'età anagrafica non inferiore a 63 anni e non superiore all'età prevista per la pensione di vecchiaia; b) graduale riduzione dell'età anagrafica per il collocamento a riposo d'ufficio, garantendo comunque la permanenza in servizio fino all'età anagrafica necessaria per escludere la riduzione della pensione; c) previsione del trattamento in servizio oltre il termine di cui alla lettera a) al fine di maturare il diritto alla pensione, e comunque non oltre l'età prevista dalla normativa vigente per i dipendenti pubblici [comma 1]. In deroga al comma 1 trovano applicazione le specifiche disposizioni statali vigenti per la dirigenza medica e sanitaria [comma 2]»;
 che, ad avviso del Presidente del Consiglio dei ministri, tale disposizione eccede la competenza legislativa attribuita alla Provincia autonoma di Bolzano dagli artt. 8, 9 e 10 dello statuto speciale per il Trentino-Alto Adige, approvato con il d.P.R. 31 agosto 1972, n. 670  (Approvazione del testo unico delle leggi costituzionali concernenti lo statuto speciale per il Trentino-Alto Adige), che non contemplano la materia «previdenza sociale», nella quale ha invece legislazione esclusiva, ai sensi dell'art. 117, secondo comma, lettera o), Cost., lo Stato;
 che, secondo lo stesso Presidente del Consiglio dei ministri, la disposizione censurata víola gli artt. 3, 81, terzo comma, 117, secondo comma, lettera o), e terzo comma, e 119 Cost., perché disciplina «l'anticipo del trattamento di fine rapporto o di fine servizio» in modo difforme dalla normativa dettata in materia dallo Stato con le disposizioni - costituenti parametri interposti - dell'art. 1, comma 5, del decreto-legge 24 giugno 2014, n. 90 (Misure urgenti per la semplificazione e la trasparenza amministrativa e per l'efficienza degli uffici giudiziari), convertito, con modificazioni, dell'art. 1, comma 1, della legge 11 agosto 2014, n. 114, dell'art. 24 del decreto-legge 6 dicembre 2011, n. 201 (Disposizioni urgenti per la crescita, l'equità e il consolidamento dei conti pubblici), convertito, con modificazioni, dall'art. 1, comma 1, della legge 22 dicembre 2011, n. 214, e del decreto-legge 31 maggio 2010, n. 78 (Misure urgenti in materia di stabilizzazione finanziaria e di competitività economica), convertito, con modificazioni, dall'art. 1, comma 1, della legge 30 luglio 2010, n. 122;
 che l'art. 29 della legge provinciale n. 6 del 2015 si porrebbe in contrasto anche con l'art. 3 Cost., perché víola il «diritto di uguaglianza dei cittadini su tutto il territorio nazionale»;
 che, sempre secondo il ricorrente, la disposizione impugnata, essendo «suscettibile di determinare maggiori oneri per la finanza pubblica», violerebbe inoltre gli artt. 81, terzo comma, e 117, terzo comma, Cost., là dove questo riserva allo Stato la determinazione dei principi fondamentali nella materia «coordinamento della finanza pubblica», nonché l'art. 119 Cost., là dove prevede che le Regioni e le Province concorrono ad assicurare l'osservanza dei vincoli economici e finanziari derivanti dall'ordinamento dell'Unione europea;
 che la Provincia autonoma di Bolzano non si è costituita in giudizio;
 che, nelle more di questo, l'art. 23, comma 1, lettera f), della legge della Provincia autonoma di Bolzano 25 settembre 2015, n. 11 (Disposizioni in connessione con l'assestamento del bilancio di previsione della Provincia autonoma di Bolzano per l'anno finanziario 2015 e per il triennio 2015-2017), ha abrogato la disposizione impugnata; 
 che il 22 dicembre 2015 l'Avvocatura generale dello Stato ha depositato atto di rinuncia al ricorso, approvata dal Consiglio dei ministri nella riunione del 4 dicembre 2015, con la motivazione che, a seguito della menzionata sopravvenienza normativa, «appaiono venute meno le ragioni che hanno condotto all'impugnativa»;
 che il 9 febbraio 2016 l'Avvocatura generale dello Stato ha depositato la nota dell'Avvocatura della Provincia autonoma di Bolzano protocollo n. 17.01.02/14189/57952 del 1° febbraio 2016, indirizzata alla stessa Avvocatura generale, con la quale veniva comunicato che la Giunta della Provincia autonoma, con la deliberazione n. 30 del 19 gennaio 2016 (allegata alla nota e anch'essa depositata), ha accettato la rinuncia al ricorso e veniva richiesto all'Avvocatura statale di volere provvedere, stante la mancata costituzione in giudizio della Provincia, al deposito, oltre che della stessa nota, anche della menzionata deliberazione della Giunta provinciale.
 Considerato che il Presidente del Consiglio dei ministri ha promosso questione principale di legittimità costituzionale dell'art. 29 della legge della Provincia autonoma di Bolzano 19 maggio 2015, n. 6 (Ordinamento del personale della Provincia), in riferimento agli artt. 3, 81, terzo comma, 117, secondo comma, lettera o), e terzo comma, e 119 della Costituzione;
 che la Provincia autonoma di Bolzano non si è costituita in giudizio;
 che, conformemente alla delibera adottata dal Consiglio dei ministri nella riunione del 4 dicembre 2015, il ricorrente ha rinunciato al ricorso;
 che, in mancanza di costituzione in giudizio della Provincia convenuta, l'intervenuta rinuncia al ricorso determina, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (da ultimo, ordinanze n. 27 del 2016, n. 199, n. 134 e n. 9 del 2015).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p/>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso </block>
    </conclusions>
  </judgment>
</akomaNtoso>
