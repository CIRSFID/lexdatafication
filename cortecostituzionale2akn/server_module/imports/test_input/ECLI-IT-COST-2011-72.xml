<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/72/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/72/"/>
          <FRBRalias value="ECLI:IT:COST:2011:72" name="ECLI"/>
          <FRBRdate date="23/02/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="72"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/72/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/72/ita@/!main"/>
          <FRBRdate date="23/02/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/72/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/72/ita@.xml"/>
          <FRBRdate date="23/02/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="03/03/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>DE SIERVO</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maria Napolitano</cc:relatore_pronuncia>
        <cc:data_decisione>23/02/2011</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 10-bis del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), aggiunto dall'art. 1, comma 16, lettera a), della legge 15 luglio 2009, n. 94 (Disposizioni in materia di sicurezza pubblica), promossi dal Giudice di pace di Genova con ordinanza del 9 dicembre 2009 e dal Giudice di pace di Isernia con ordinanza del 12 febbraio 2010, iscritte ai nn. 112 e 222 del registro ordinanze 2010 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 16 e 35, prima serie speciale, dell'anno 2010.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 26 gennaio 2011 il Giudice relatore Paolo Maria Napolitano.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che, con ordinanza del 9 dicembre 2009, il Giudice di pace di Genova ha sollevato, in riferimento agli artt. 2, 3, 24 e 25 della Costituzione, questione di legittimità costituzionale dell'art. 10-bis del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), aggiunto dall'art. 1, comma 16, lettera a), della legge 15 luglio 2009, n. 94 (Disposizioni in materia di sicurezza pubblica); 
 che il giudice a quo premette, in fatto, di dover giudicare un cittadino straniero extracomunitario accusato del nuovo reato di ingresso o soggiorno illegale nel territorio dello Stato di cui all'art.10-bis del d.lgs. n. 286 del 1998 e afferma che la questione è rilevante perché dalle risultanze dibattimentali non emergerebbero ostacoli all'applicazione della norma incriminatrice;
 che il rimettente, in punto di non manifesta infondatezza, ritiene che la norma censurata violi i principi di ragionevolezza, uguaglianza e solidarietà (artt. 2 e 3 Cost.), come già ritenuto con ampia ed esaustiva motivazione dal Tribunale ordinario di Pesaro con ordinanza in data 31 agosto 2009, cui egli fa integrale ed esplicito riferimento;
 che, inoltre, il Giudice di pace di Genova dubita della conformità a Costituzione della fattispecie incriminatrice in esame anche con riferimento agli artt. 24 e 25 Cost.;
 che, a suo parere, sarebbe violato il principio di determinatezza della fattispecie penale così come elaborato dalla Corte costituzionale nella sentenza n. 34 del 1995;
 che nella suindicata decisione concernente, anche in quel caso, un reato di mera omissione, la Corte aveva affermato che la generica indicazione del comportamento penalmente punibile, l'assenza di indicazioni circa il grado di inerzia sanzionabile e la mancanza di determinazione del tempo entro il quale porre in essere la condotta doverosa comportavano una violazione del «principio di tassatività» della fattispecie e, di conseguenza, del principio di riserva di legge assoluta in materia penale consacrata dall'art. 25 Cost.;
 che l'art. 10-bis del d.lgs. n. 286 del 1998 presenterebbe gli stessi difetti di insufficiente tipizzazione della fattispecie in primo luogo perché non incriminerebbe una condotta ma un mero status quale quello di irregolare nel territorio dello Stato italiano, in secondo luogo, perché non sarebbe indicato il grado dell'inerzia punibile mancando la specificazione del momento dal quale lo straniero deve effettivamente allontanarsi dal territorio dello Stato e, infine,  perché non sarebbe indicato il tempo entro il quale la condotta doverosa ipotizzata dal legislatore deve essere compiuta;
 che, pertanto, la condotta di indebito trattenimento sarebbe del tutto indeterminata e potenzialmente applicabile ad una serie illimitata di comportamenti, determinando in tal modo una violazione del diritto di difesa, poiché un soggetto sarebbe esposto alla contestazione del reato per il solo fatto di essersi trattenuto nel territorio dello Stato, senza avere la possibilità in alcun modo di contrastare l'addebito e senza poter dare prova di avere soddisfatto il precetto legislativo;
 che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o infondata;
 che, a parere dell'Avvocatura dello Stato, la questione relativa alla violazione degli artt. 2 e 3 Cost. è inammissibile in quanto motivata per relationem mentre è noto che secondo la giurisprudenza costituzionale il giudice remittente deve manifestare il proprio dubbio di legittimità costituzionale con argomentazioni proprie, espressamente indicate nell'ordinanza di rimessione;
 che l'ulteriore questione sarebbe invece infondata perché la scelta di dare rilievo penale a comportamenti illeciti precedentemente sanzionati solo in via amministrativa rientra nella discrezionalità del legislatore in ordine all'individuazione delle condotte penalmente punibili e delle relative sanzioni;
 che, inoltre, non sarebbe esatta l'affermazione del rimettente circa l'indeterminatezza della fattispecie in quanto il legislatore punisce una condotta specifica e inequivoca, quale quella di chi sia presente sul territorio dello Stato senza titolo legittimante, «il cui grado di inerzia non punibile [sarebbe] dato dalla inesigibilità della condotta»;
 che, con ordinanza del 12 febbraio 2010, il Giudice di pace di Isernia ha sollevato, in riferimento agli artt. 3, 24, secondo comma, e 111, primo e secondo comma, Cost., questione di legittimità costituzionale dell'art. 10-bis del d.lgs. n. 286 del 1998, aggiunto dall'art. 1, comma 16, lettera a), della legge n. 94 del 2009; 
 che il giudice a quo premette, in fatto, di dover giudicare un cittadino straniero extracomunitario imputato del nuovo reato di ingresso o soggiorno illegale nel territorio dello Stato per «essersi trattenuto nel territorio dello Stato in violazione delle disposizioni del T.U. sull'immigrazione (in quanto privo del permesso di soggiorno)»;
 che l'imputato, a seguito di un controllo dei carabinieri, risultava sprovvisto dei documenti di identificazione e di soggiorno nel territorio nazionale e dai successivi rilievi dattiloscopici e fotosegnaletici, emergeva che, pur non essendo gravato da pregiudizi penali, lo stesso aveva fatto ingresso nel territorio dello Stato sottraendosi ai controlli di frontiera;
 che, a parere del remittente, la norma sarebbe in contrasto con l'art. 3 Cost. perché non prevede, quale elemento costitutivo del reato (o quantomeno come esimente codificata), l'assenza di un giustificato motivo, che è invece espressamente previsto nella fattispecie ben più grave di inottemperanza all'ordine del questore di lasciare il territorio nazionale (delittuosa, e non già contravvenzionale) di cui all'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998, operando così un'evidente discriminazione tra chi rifiuta di obbedire ad un ordine legalmente reso dell'autorità amministrativa e chi semplicemente permane sul territorio nazionale;
 che risulterebbero violati anche gli artt. 24, secondo comma, e 111, primo e secondo comma, Cost., in quanto l'impossibilità dello straniero di provare l'esistenza di un giustificato motivo che gli abbia impedito di regolarizzare la sua posizione costituirebbe una chiara lesione del diritto di difesa nonché del principio del contraddittorio nella formazione della prova;
 che, infatti, a causa della natura del reato in esame, non sarebbe possibile per l'imputato alcuna pratica difesa risultando la prova della sussistenza dell'elemento costitutivo del reato, rappresentato dalla presenza sul territorio nazionale, ex actis dalla semplice lettura dell'atto di presentazione immediata, ciò in palese contrasto con il principio per il quale la prova si forma solo nel dibattimento;
 che, pertanto, il processo si risolverebbe in un vuoto simulacro privo delle guarentigie costituzionali di cui all'art. 111 Cost.;
 che la norma incriminatrice sarebbe in contrasto con l'art. 3 Cost. anche sotto il profilo dell'irragionevolezza dell'azione legislativa, assolutamente arbitraria e priva di ogni giustificazione sul piano costituzionale;
 che il rimettente ritiene la questione rilevante non essendo possibile l'applicazione delle scriminanti previste dagli artt. 51 e seguenti cod. pen., di natura tipica, né tantomeno l'estensione, in chiave analogica, della scriminante (anch'essa tipizzata) prevista dall'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998, di stretta applicazione, poiché posta in relazione ad un comportamento del tutto diverso (cioè quello di non aver ottemperato all'ordine di allontanamento disposto dal Questore), non assimilabile, stricto sensu, alla fattispecie prevista dalla norma qui censurata;
 che, infine, la questione di legittimità costituzionale si porrebbe come questione pregiudiziale, in quanto, se accolta, comporterebbe l'assoluzione dell'imputato dal reato per cui si procede o quantomeno imporrebbe di valutarne l'effettiva colpevolezza.
 Considerato che le ordinanze di rimessione, indicate in epigrafe, sollevano questioni identiche o analoghe, onde i relativi giudizi vanno riuniti per essere definiti con unica decisione;
 che i giudici a quibus dubitano, in riferimento a plurimi parametri, della legittimità costituzionale dell'art. 10-bis del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), aggiunto dall'art. 1, comma 16, lettera a), della legge 15 luglio 2009, n. 94 (Disposizioni in materia di sicurezza pubblica), che punisce con l'ammenda da 5.000 a 10.000 euro, salvo che il fatto costituisca più grave reato, lo straniero che fa ingresso o si trattiene illegalmente nel territorio dello Stato; 
 che le ordinanze di rimessione presentano carenze in punto di descrizione della fattispecie concreta e di motivazione sulla rilevanza tali da precludere lo scrutinio nel merito delle questioni;
 che, quanto all'ordinanza di rimessione del Giudice di pace di Genova, l'indicato difetto di descrizione della fattispecie è pressoché totale, in quanto il rimettente non riporta il capo d'imputazione né tantomeno descrive l'effettiva condotta contestata all'imputato, limitandosi a riferire che questi, rintracciato nel territorio dello Stato, non è stato in grado di dimostrare la regolarità della propria posizione di soggiorno;
 che, in mancanza di qualsiasi riferimento alla fattispecie concreta che ha dato origine all'imputazione, resta inibita per questa Corte la necessaria verifica circa l'influenza della questione di legittimità sulla decisione richiesta al rimettente;
 che anche l'ordinanza proveniente dal Giudice di pace di Isernia presenta carenze insuperabili quanto alla descrizione della fattispecie;
 che, infatti, il rimettente descrive la fattispecie concreta in modo contraddittorio, non chiarendo se la condotta contestata all'imputato è quella di ingresso illegale nel territorio dello Stato o di indebito trattenimento nel territorio dello Stato, circostanza che assume particolare rilievo, dato che le questioni sollevate attengono alla mancanza dell'esimente del giustificato motivo;
 che, inoltre, manca ogni motivazione in ordine alla rilevanza della questione, non essendo prospettata, neppure con riguardo a mere allegazioni difensive, alcuna circostanza che, nel caso di specie, potrebbe assumere rilievo quale «giustificato motivo»; 
 che la carenza di elementi potenzialmente giustificativi della condotta, nella descrizione della fattispecie sottoposta a giudizio, priva la questione sollevata di attuale rilevanza, o comunque non consente il necessario controllo al riguardo; 
 che le questioni vanno dichiarate, pertanto, manifestamente inammissibili.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 riuniti i giudizi,&#13;
 dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 10-bis del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), aggiunto dall'art. 1, comma 16, lettera a), della legge 15 luglio 2009, n. 94 (Disposizioni in materia di sicurezza pubblica), sollevate, in riferimento agli artt. 3, 10, 24, 25 e 111 della Costituzione, dal Giudice di pace di Genova e dal Giudice di pace di Isernia con le ordinanze indicate in epigrafe. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, palazzo della Consulta, il 23 febbraio 2011.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Paolo Maria NAPOLITANO, Redattore&#13;
 Maria Rosaria FRUSCELLA, Cancelliere&#13;
 Depositata in Cancelleria il 3 marzo 2011.&#13;
 Il Cancelliere&#13;
 F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
