<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/102/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/102/"/>
          <FRBRalias value="ECLI:IT:COST:2009:102" name="ECLI"/>
          <FRBRdate date="01/04/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="102"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/102/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/102/ita@/!main"/>
          <FRBRdate date="01/04/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/102/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/102/ita@.xml"/>
          <FRBRdate date="01/04/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="02/04/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>01/04/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Francesco AMIRANTE; Giudici: Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 438 del codice di procedura penale, promossi dalla Corte d'assise d'appello di Napoli nei procedimenti penali a carico di R.V. e di G.P., con ordinanze del 19 febbraio e del 28 marzo 2007, iscritte ai nn. 828 e 849 del registro ordinanze 2007 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 3 e 5, prima serie speciale, dell'anno 2008. 
    Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio dell'11 marzo 2009 il Giudice relatore Giuseppe Frigo. 
    Ritenuto che, con due ordinanze emesse il 19 febbraio e il 28 marzo 2007 in diversi processi, la Corte d'assise d'appello di Napoli ha proposto identica questione di legittimità costituzionale dell'art. 438 del codice di procedura penale «nella parte in cui demanda al giudice dell'udienza preliminare lo svolgimento del giudizio abbreviato anche nei procedimenti di competenza della corte di assise», prospettandone il contrasto con gli artt. 1, 24, 25, 101, 102 e 111 della Costituzione; 
    che il giudice a quo espone nella prima ordinanza di essere stato investito della cognizione dell'appello proposto dall'imputato R.V. avverso sentenza di condanna alla pena di 14 anni di reclusione, inflittagli all'esito di un giudizio abbreviato dal giudice dell'udienza preliminare del Tribunale di Napoli per il delitto di omicidio volontario aggravato, tenuto conto della riduzione della pena conseguente alla scelta di tale rito; e, nella seconda ordinanza, dell'appello proposto dall'imputato G.P. avverso sentenza di condanna alla pena di 12 anni di reclusione, pure inflittagli all'esito di un giudizio abbreviato dal giudice dell'udienza preliminare del Tribunale di Napoli per omicidio volontario e porto abusivo di coltello, tenuto conto della riduzione della pena per la scelta del rito; 
    che il giudice a quo aggiunge di doversi preliminarmente occupare, in entrambi i processi, della suddetta questione di legittimità costituzionale, espressamente eccepita dalla difesa degli imputati e ritenuta rilevante – in quanto l'eventuale accoglimento determinerebbe la esigenza di annullare le sentenze appellate, con regressione al primo grado ed il rinvio degli imputati a giudizio avanti la corte d'assise – e altresì non manifestamente infondata; 
    che la non manifesta infondatezza si apprezzerebbe, in primo luogo, con riferimento alla disposizione combinata degli artt. 1 e 25, primo comma, Cost. e comunque al principio di ragionevolezza, in quanto per i processi di sua competenza la corte d'assise, istituita dalla legge 10 aprile 1951, n. 287 (Riordinamento dei giudizi di assise) ed espressione della diretta partecipazione del popolo, detentore della sovranità, all'amministrazione della giustizia, sarebbe da intendere quale giudice naturale, a cui nessuno può essere sottratto, come, invece, avviene con l'attribuzione della competenza per quegli stessi reati al giudice monocratico dell'udienza preliminare; 
    che altrettanto non manifestamente infondato sarebbe il dubbio di incostituzionalità con riguardo agli artt. 101 e 102, terzo comma, Cost., i quali avrebbero “blindato di costituzionalità” la corte d'assise quale giudice naturale in relazione alla citata legge istitutiva della medesima; 
    che ulteriormente sarebbe violato l'art. 25, primo comma, Cost., in quanto la sottrazione al giudice naturale avverrebbe per effetto dell'esercizio del diritto potestativo del solo imputato di adire il giudizio abbreviato, configurandosi, con riguardo ai processi suddetti, una competenza alternativa, consegnata alla sua libera e immotivata scelta; 
    che ciò costituirebbe altresì violazione del principio costituzionale di parità delle parti, espresso dall'art. 111, secondo comma, Cost.; 
    che si dovrebbe, infine, rilevare il contrasto con il precetto di inviolabilità del diritto di difesa enunciato dall'art. 24, secondo comma, Cost., a causa delle conseguenze della asserita sottrazione al giudice naturale, in tema di prova, la cui valutazione da parte del collegio misto, comprendente i giudici popolari, seguirebbe criteri più ampi di cosiddetto libero convincimento, mentre il giudice professionale sarebbe rigorosamente vincolato a quelli recati dal codice di procedura penale; 
    che comunque non varrebbe a riportare in equilibrio e a ragionevolezza il sistema la cognizione del collegio misto per gli appelli anche delle sentenze rese in primo grado secondo il rito abbreviato; ciò, anzi, aggraverebbe gli squilibri; 
    che da tempo la sensibilità generale avverte l'esigenza di mantenere la competenza della corte d'assise anche per i giudizi abbreviati e di ciò si sono fatte espressione varie iniziative legislative; 
    che nei giudizi di costituzionalità promossi con le due ordinanze di rimessione è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata inammissibile o infondata. 
    Considerato che identica è la questione sollevata nei due giudizi e che, dunque, essi vanno riuniti per essere definiti con unica decisione; 
    che la Corte d'assise d'appello di Napoli solleva questione di legittimità costituzionale dell'art. 438 del codice di procedura penale per contrasto con gli artt. 1, 24, 25, 101, 102 e 111 della Costituzione, nella parte in cui demanda al giudice dell'udienza preliminare lo svolgimento del giudizio abbreviato anche nei procedimenti di competenza della corte d'assise; 
    che, pur essendo rilevante nei giudizi a quibus, la questione è manifestamente infondata con riguardo a tutti i parametri costituzionali evocati; 
    che, in particolare, l'attribuzione della competenza al giudice dell'udienza preliminare (giudice professionale monocratico) per il giudizio abbreviato nei procedimenti che, nel rito ordinario, sono attribuiti alla corte d'assise (giudice collegiale a composizione mista, con prevalente componente di giudici popolari) non sottrae l'imputato al giudice naturale e non viola la disposizione combinata degli artt. 1, secondo comma, e 25, primo comma, Cost.; 
    che, infatti, secondo quanto ha più volte e costantemente deciso questa Corte, l'ordinamento costituzionale non propone una nozione autonoma di giudice naturale, distinta e diversa da quella di giudice precostituito per legge, dovendosi con ciò intendere, secondo una equivalenza e reciproca integrazione delle due locuzioni, che spetta alla legge previamente determinare, rispetto alle possibili controversie giudiziarie, il giudice competente a conoscerle, così ripartendo la giurisdizione tra i vari giudici previsti dall'ordinamento giudiziario, sicché giudice naturale è quello prefigurato dalla legge, secondo criteri generali che, nei limiti della non manifesta irragionevolezza e arbitrarietà, appartengono alla discrezionalità legislativa (tra le molte, sentenze n. 460 del 1994, n. 305 del 1993, n. 375 del 1991, n. 135 del 1980, n. 88 del 1962 e n. 29 del 1958; ordinanze n. 138 del 2008, n. 193 del 2003 e n. 481 del 2002); 
    che entro tali limiti non assumono rilievo e non possono essere motivo di censura la presunta maggiore o minore idoneità o qualificazione, che possa essere rivendicata o riconosciuta all'uno o all'altro organo della giurisdizione, e così la scelta per l'attribuzione della competenza al giudice monocratico piuttosto che a quello collegiale e, nell'ambito del secondo, al collegio tutto professionale piuttosto che a quello composto anche da «soggetti idonei estranei all'amministrazione della giustizia» o anche da «giudici popolari» (art. 102, secondo e terzo comma, Cost.); 
    che, più specialmente, va esclusa una “costituzionalizzazione” della corte d'assise, quale giudice naturale per i reati di sua competenza, con qualunque rito siano svolti i relativi processi, in forza della citata legge n. 287 del 1951 sul riordinamento dei giudizi di assise, che è legge ordinaria non assumibile nella cornice delineata dall'art. 25, primo comma, Cost. né in quella degli artt. 101 e 102, terzo e quarto comma, ma piuttosto emanata, in collegamento con la disciplina della competenza dettata dal codice di rito, nell'adempimento della riserva di legge, in via generale contenuta nell'art. 25 e, con riguardo alla partecipazione diretta del popolo all'amministrazione della giustizia, nel terzo comma dell'art. 102: dovendosi, a proposito di questa norma, rammentare che il dibattito politico-culturale, svoltosi a suo tempo all'Assemblea costituente proprio sul tema della costituzionalizzazione di quella forma più avanzata di tale partecipazione quale è la giuria, si concluse con un rinvio al legislatore ordinario, cui venne attribuita la piena libertà di scegliere «i casi» e «le forme» di tale partecipazione (al che appunto esso provvide con la legge suddetta); 
    che ovvia conseguenza è che nessuna censura a livello costituzionale può essere mossa alla scelta di attribuire gli stessi processi, che in via ordinaria sono di competenza della corte d'assise, al giudice dell'udienza preliminare, quando si svolgano secondo le norme del giudizio abbreviato, dovendo entrambi essere considerati giudici naturali precostituiti per legge; 
    che l'attribuzione di competenza, nel secondo caso, al giudice dell'udienza preliminare non è né arbitraria né irragionevole, restando nell'area delle scelte discrezionali, semmai opinabili ma non censurabili sul piano della legittimità costituzionale, con cui il legislatore all'evidenza ha valorizzato i connotati specifici del giudizio abbreviato quale giudizio a prova contratta, articolato su attività e cadenze comunque più semplici e spedite, al cui governo poteva bastare un giudice monocratico; 
    che, analogamente, questa Corte (sentenza n. 460 del 1994) ha già escluso qualsiasi lesione dei parametri costituzionali qui evocati, in relazione al giudizio abbreviato nel processo penale militare, per il fatto che ne sia attribuita la cognizione al giudice monocratico dell'udienza preliminare, anziché al tribunale militare in composizione mista, che è il giudice competente in via ordinaria; 
    che neppure può dirsi violato l'art. 25, primo comma, Cost. sotto il profilo di una incompiuta predeterminazione per legge della competenza: infatti, quella così delineata non integra una competenza alternativa né si configura come una situazione in cui il solo imputato sarebbe arbitro di scegliersi il giudice; sono, piuttosto, riconoscibili due diversi processi: il giudizio ordinario in dibattimento e il giudizio abbreviato, con caratteristiche marcatamente distinte, per ciascuno dei quali è precostituito per legge un solo giudice; la scelta dell'imputato non è scelta del giudice, ma del rito con tutte le sue caratteristiche, che, rese accettabili per lui dalla prospettiva premiale sul piano sanzionatorio, si risolvono essenzialmente in rinunce a cospicue garanzie, quali sono indubbiamente il contraddittorio per la prova e la stessa composizione, collegiale e mista, del giudice (come questa Corte ha già a suo tempo sottolineato: sentenza n. 691 del 1991); 
    che, in considerazione di ciò, non si configura una lesione delle condizioni di parità delle parti (in assunto contrastante con l'art. 111, secondo comma, Cost.), una volta che, quanto al pubblico ministero, la scelta del rito fatta dall'imputato non reca alcun pregiudizio agli interessi di cui la parte pubblica è portatrice, mentre alla eventuale parte civile la legge riserva la facoltà di non accettare il rito, senza conseguenze negative per il conseguimento dei propri obbiettivi in un separato processo civile; 
    che, da un punto di vista generale, questa Corte ha già comunque statuito che non contrastano con il principio del giudice precostituito le norme con le quali sia scelta, come criterio di radicamento della competenza, un'attività dell'imputato che non si riduca a mero arbitrio (sentenza n. 217 del 1993); 
    che, infine, neppure sussiste lesione del diritto di difesa tutelato dall'art. 24, secondo comma, Cost., sotto il dedotto profilo dei criteri di valutazione della prova, sia perché, quando pure la disciplina censurata incidesse realmente su quel diritto, ciò sarebbe legittimato dal consenso prestato dall'imputato che ne è titolare, sia soprattutto perché tali criteri sono identici sia per il giudice collegiale misto nel giudizio ordinario avanti la corte d'assise, sia per il giudice monocratico nel giudizio abbreviato, contrariamente a quanto opina la Corte rimettente.  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'art. 438 del codice di procedura penale, sollevate, in riferimento agli artt. 1, 24, 25, 101, 102 e 111 della Costituzione, dalla Corte d'assise d'appello di Napoli con le ordinanze indicate in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'1 aprile 2009.  &#13;
F.to:  &#13;
Francesco AMIRANTE, Presidente  &#13;
Giuseppe FRIGO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 2 aprile 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
