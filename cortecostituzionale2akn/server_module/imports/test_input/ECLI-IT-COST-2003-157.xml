<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/157/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/157/"/>
          <FRBRalias value="ECLI:IT:COST:2003:157" name="ECLI"/>
          <FRBRdate date="05/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="157"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/157/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/157/ita@/!main"/>
          <FRBRdate date="05/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/157/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/157/ita@.xml"/>
          <FRBRdate date="05/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="09/05/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Annibale Marini</cc:relatore_pronuncia>
        <cc:data_decisione>05/05/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di ammissibilità del conflitto di attribuzione tra poteri dello Stato sorto a seguito della delibera della Camera dei deputati del 17 novembre 1999 relativa alla insindacabilità delle opinioni espresse dal deputato Vittorio Sgarbi nei confronti del magistrato Antonio Tricoli, promosso dalla Corte d'appello di Roma, sezione prima civile, con ricorso depositato il 6 febbraio 2002 ed iscritto al n. 209 del registro ammissibilità conflitti. 
    Udito nella camera di consiglio del 29 gennaio 2003 il Giudice relatore Annibale Marini.      
    Ritenuto che con ordinanza-ricorso del 18 gennaio 2002, depositata presso la cancelleria di questa Corte il 6 febbraio 2002, la Corte d'appello di Roma, nel corso di un giudizio civile per il risarcimento dei danni promosso dal magistrato Antonio Tricoli nei confronti del deputato Vittorio Sgarbi ed altri, ha sollevato conflitto di attribuzione tra poteri dello Stato nei confronti della Camera dei deputati in relazione alla deliberazione con la quale l'Assemblea, nella seduta del 17 novembre 1999, ha dichiarato insindacabili, ai sensi dell'art. 68, primo comma, della Costituzione, le opinioni espresse dal deputato Sgarbi, cui la pretesa risarcitoria si riferisce; 
    che il giudice ricorrente premette di avere già sollevato conflitto, in relazione alla medesima deliberazione e nel corso dello stesso processo, con ordinanza del 27 novembre 2000: conflitto dichiarato ammissibile da questa Corte, con ordinanza depositata il 31 maggio 2001; 
    che, per un disguido della propria cancelleria, la ricorrente Corte d'appello di Roma non ha provveduto nei termini alle prescritte notificazioni ed al conseguente deposito nella cancelleria di questa Corte degli atti notificati; 
    che tuttavia permarrebbero, secondo lo stesso giudice, le ragioni di conflitto già esposte nella ricordata ordinanza del 27 novembre 2000, in quanto le frasi e le opinioni asseritamente lesive dell'onore e della reputazione dell'attore, espresse dallo Sgarbi nel corso di una trasmissione televisiva, non sarebbero in alcun modo collegate all'esercizio della funzione parlamentare; 
    che pertanto la Corte d'appello di Roma chiede che questa Corte accerti che non spetta alla Camera dei deputati dichiarare la insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal deputato Vittorio Sgarbi e conseguentemente annulli la deliberazione parlamentare adottata dalla stessa Camera dei deputati nella seduta del 17 novembre 1999. 
    Considerato che in questa fase la Corte è chiamata, ai sensi dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), a deliberare se il ricorso sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivo ed oggettivo di un conflitto di attribuzione tra poteri dello Stato; 
    che a tali fini - ed a prescindere dall'esame di ogni altro profilo - è sufficiente rilevare che la deliberazione della Camera dei deputati in data 17 novembre 1999, in relazione alla quale la Corte d'appello di Roma ha sollevato conflitto, è già stata annullata da questa Corte con la sentenza n. 448 del 2002, a seguito del giudizio su un distinto conflitto di attribuzione, sollevato dal Tribunale di Caltanissetta nel corso di un processo penale a carico del deputato Sgarbi; 
    che, infatti, la deliberazione in questione si riferiva sia al procedimento penale nel cui ambito è stato sollevato il conflitto giudicato con la sentenza n. 448 del 2002, sia al procedimento civile di cui si tratta, all'epoca pendente, in primo grado, dinanzi al Tribunale di Roma, l'uno e l'altro originati dalle medesime dichiarazioni dello Sgarbi, riguardanti due diversi magistrati; 
    che pertanto, una volta caducata la predetta deliberazione della Camera, è venuto meno l'ostacolo che, secondo la giurisprudenza di questa Corte, preclude al giudice di pronunciarsi sui comportamenti oggetto della deliberazione stessa; 
    che, conseguentemente, non esiste più la materia di un conflitto di attribuzione tra poteri dello Stato (cfr. ordinanza n. 3 del 2003).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
  &#13;
    dichiara inammissibile il ricorso per conflitto di attribuzione di cui in epigrafe, proposto dalla Corte d'appello di Roma nei confronti della Camera dei deputati. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 maggio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Annibale MARINI, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 9 maggio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
