<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2017/249/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2017/249/"/>
          <FRBRalias value="ECLI:IT:COST:2017:249" name="ECLI"/>
          <FRBRdate date="24/10/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="249"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2017/249/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2017/249/ita@/!main"/>
          <FRBRdate date="24/10/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2017/249/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2017/249/ita@.xml"/>
          <FRBRdate date="24/10/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="01/12/2017" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2017</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Giancarlo Coraggio</cc:relatore_pronuncia>
        <cc:data_decisione>24/10/2017</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 1, comma 335, della legge 30 dicembre 2004, n. 311, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2005)», promosso dalla Commissione tributaria regionale del Lazio nel procedimento vertente tra M.L. F. e l'Agenzia delle entrate - Ufficio provinciale di Roma, con ordinanza del 16 dicembre 2016, iscritta al n. 82 del registro ordinanze 2017 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 24, prima serie speciale, dell'anno 2017. 
 Visti l'atto di costituzione di M.L. F., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella udienza pubblica del 24 ottobre 2017 il Giudice relatore Giancarlo Coraggio;
 uditi l'avvocato Mariano Buratti per M.L. F. e l'avvocato dello Stato Anna Lidia Caputi Iambrenghi per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Con ordinanza del 16 dicembre 2016, la Commissione tributaria regionale del Lazio ha sollevato questione di legittimità costituzionale, per violazione degli artt. 3, 53 e 97 della Costituzione, dell'art. 1, comma 335, della legge 30 dicembre 2004, n. 311, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2005)», il quale prevede che «La revisione parziale del classamento delle unità immobiliari di proprietà privata site in microzone comunali, per le quali il rapporto tra il valore medio di mercato individuato ai sensi del regolamento di cui al decreto del Presidente della Repubblica 23 marzo 1998, n. 138, e il corrispondente valore medio catastale ai fini dell'applicazione dell'imposta comunale sugli immobili si discosta significativamente dall'analogo rapporto relativo all'insieme delle microzone comunali, è richiesta dai comuni agli Uffici provinciali dell'Agenzia del territorio. Per i calcoli di cui al precedente periodo, il valore medio di mercato è aggiornato secondo le modalità stabilite con il provvedimento di cui al comma 339. L'Agenzia del territorio, esaminata la richiesta del comune e verificata la sussistenza dei presupposti, attiva il procedimento revisionale con provvedimento del direttore dell'Agenzia medesima».
 1.1.- A parere del collegio tributario regionale, dal complessivo quadro normativo emergerebbe che il classamento è un'operazione che interessa necessariamente una singola unità immobiliare, come tale sarebbe quindi difficilmente conciliabile con la previsione di una revisione del classamento per intere microzone motivata da scostamenti tra valori di mercato e valori catastali. Infatti, il classamento andrebbe ad inquadrare caratteristiche specifiche, vale a dire identità, natura e qualità, di singole unità immobiliari, le quali restano le stesse, salvo trasformazioni specifiche avvenute tramite opere di risanamento o di ristrutturazione, per le quali è previsto il meccanismo di cui al comma 336 del medesimo art. 1 della legge n. 311 del 2004.  
 Il rimettente sostiene che il riclassamento disciplinato dal censurato comma 335, in quanto «collegato ai soli valori di mercato di zona e senza modificazioni nella realtà si porrebbe inevitabilmente in contrasto con la capacità contributiva dei singoli» e quindi con l'art. 53 Cost. Infatti, pur non essendo un vero e proprio atto di imposizione fiscale, inciderebbe tuttavia sulla rendita del bene ed avrebbe effetti sull'imposizione diretta e su quella locale. 
 Sarebbe altresì ravvisabile la violazione dell'art. 3 Cost., perché il singolo contribuente si troverebbe «irrazionalmente esposto a rivalutazione del proprio bene in relazione alla significativa rivalutazione di beni altrui sol perché situato in una "microzona" oggetto di attenzione da parte del Comune, con disparità di trattamento rispetto ad altre "microzone", pur significati[va]mente da rivalutare, ma non oggetto di richiesta da parte del comune medesimo all'Agenzia del territorio».
 La norma impugnata contrasterebbe, infine, con l'art. 97 Cost., in quanto la rivalutazione «massiva» operata dalla norma censurata «non assicur[erebbe] né il buon andamento né l'imparzialità dell'amministrazione, colpendo indiscriminatamente tutte le unità immobiliari (di una determinata microzona) senza alcuna verifica concreta del singolo bene (non essendo necessario il sopralluogo, Cass. n. 21176 del 2016; Cass. n. 21923 del 2012), esponendo l'Amministrazione medesima ad una altrettanto "massiva" opposizione da parte dei contribuenti interessati». 
 2.- Con atto depositato il 4 luglio 2017, è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata inammissibile o infondata. 
 La prospettata questione di costituzionalità sarebbe inammissibile in quanto involgente scelte discrezionali spettanti esclusivamente al legislatore: essa, infatti, non sarebbe suscettibile di una soluzione costituzionalmente imposta, ma di una pluralità di soluzioni.
 Nel merito, poi, ricorda l'interveniente che la revisione parziale del classamento rappresenta, in sostanza, uno strumento previsto dal legislatore al fine di rimuovere significative sperequazioni - rinvenibili nell'ambito di alcune microzone comunali - che emergono dal confronto tra i valori medi di mercato ed i valori medi catastali rilevati nelle diverse microzone in cui risulta suddiviso il territorio comunale e che sono riconducibili a variazioni del contesto urbano verificatesi nel tempo. Al fine di determinare le modalità di avvio della procedura in argomento, sono state definite, in modo uniforme per l'intero territorio nazionale, le regole o le modalità di calcolo dei valori medi di mercato e dei valori medi catastali posti alla base dei rapporti che portano all'individuazione delle microzone «anomale», secondo criteri precisi e puntuali che non lascerebbero margini di discrezionalità all'amministrazione. 
 Il Presidente del Consiglio dei ministri segnala altresì il differente ambito applicativo della norma impugnata e del successivo comma 336 dello stesso art. 1 della legge n. 311 del 2004 (richiamato dal giudice rimettente), il quale disciplina una procedura di revisione legata a situazioni di fatto non più coerenti con i classamenti passati per intervenute variazioni edilizie della singola unità immobiliare. 
 Tanto premesso sulla ricostruzione della disciplina di riferimento, si nega il contrasto di essa con il principio di capacità contributiva di cui all'art. 53 Cost., atteso che proprio l'aggiornamento delle rendite catastali permetterebbe di individuare con maggior precisione il reddito potenzialmente ritraibile dai beni immobili, e quindi la correlata capacità contributiva. Il procedimento disciplinato dalla disposizione censurata comporterebbe non l'attribuzione di un classamento collegato ai valori di mercato, bensì il riesame dei classamenti relativi agli immobili presenti nelle microzone «anomale» al fine di un loro adeguamento rispetto alle altre microzone comunali. Nel concreto, l'attività di riclassamento dei singoli immobili permetterebbe di individuare, in relazione alle loro caratteristiche e tramite raffronto con le unità di riferimento, la categoria e la classe più appropriate.
 Non sarebbe fondata neanche la questione sollevata con riferimento alla violazione della disparità di trattamento di cui all'art. 3 Cost., posto che le considerazioni svolte dal rimettente si risolverebbero nella prospettazione di eventuali ricadute pratiche, legate ad un patologico funzionamento del sistema, che tuttavia non possono involvere profili di costituzionalità della norma censurata. Quest'ultima, anzi, consentirebbe proprio di operare in senso opposto, perequando, ove non coerenti, i classamenti degli immobili situati nelle microzone «anomale».
 Infine, contrariamente a quanto ritenuto dalla Commissione tributaria regionale in relazione al denunciato contrasto con l'art. 97 Cost., il legislatore non avrebbe disposto una «rivalutazione massiva», nel senso di prevedere un riclassamento «disancorato dalla situazione del singolo bene», e le procedure attuative della disposizione censurata non avrebbero colpito indiscriminatamente gli immobili oggetto di verifica. Il procedimento in parola, invero, consentirebbe un diverso apprezzamento connesso a fattori esterni comuni a tutte le unità immobiliari delle microzone «anomale», con l'inevitabile conseguenza che la revisione dei classamenti operata ai sensi del comma 335 in esame interesserebbe generalmente un gran numero di immobili, proprio perché, essendo situati nei medesimi, mutati, contesti urbani, risulterebbero parimenti coinvolti dallo stesso fenomeno di «riqualificazione esterna». 
 3.- Nel giudizio si è costituita l'appellante del giudizio a quo, con atto depositato il 30 giugno 2017, ripercorrendo le argomentazioni della Commissione tributaria regionale e sostenendo, sulla base delle medesime, l'illegittimità costituzionale della norma censurata in relazione agli stessi parametri evocati dal giudice rimettente. 
 4.- In data 26 settembre 2017 il Presidente del Consiglio dei ministri ha depositato memoria, ribadendo le considerazioni già svolte nell'atto di costituzione ed in particolare sottolineando come la normativa in esame preveda un «procedimento "asettico"», ovverosia ancorato a criteri rigorosi e oggettivi, «teso ad evitare situazioni di palese ingiustizia all'interno di ogni singolo comune», tramite l'individuazione delle microzone «anomale» e l'applicazione agli immobili in esse ubicati della revisione parziale del classamento. 
 5.- Con memoria depositata il 2 ottobre 2017, la parte privata ha sostenuto l'ammissibilità della questione di costituzionalità - sulla base della considerazione che se alla Corte costituzionale è precluso ogni giudizio di natura politica e ogni sindacato sull'uso del potere discrezionale del Parlamento, quest'ultimo resta comunque subordinato al rispetto dei limiti posti dalla Carta costituzionale -, riproponendo, nel merito, le considerazioni precedentemente svolte.Considerato in diritto1.- La Commissione tributaria regionale del Lazio dubita della legittimità costituzionale dell'art. 1, comma 335, della legge 30 dicembre 2004, n. 311, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2005)», il quale prevede il riclassamento di unità immobiliari ubicate in microzone nelle quali il rapporto tra il valore medio di mercato individuato ai sensi del d.P.R. 23 marzo 1998, n. 138 (Regolamento recante norme per la revisione generale delle zone censuarie, delle tariffe d'estimo delle unità immobiliari urbane e dei relativi criteri nonché delle commissioni censuarie in esecuzione dell'articolo 3, commi 154 e 155, della L. 23 dicembre 1996, n. 662) e il corrispondente valore medio catastale, ai fini dell'applicazione dell'imposta comunale sugli immobili, si discosta significativamente dall'analogo rapporto relativo all'insieme delle microzone comunali. 
 1.1.- Secondo il rimettente, dal quadro generale della legislazione vigente emergerebbe che il classamento è un'operazione legata necessariamente ad una singola unità immobiliare. Pertanto sarebbe incompatibile con il sistema la previsione di una revisione parziale del classamento di intere microzone. 
 In particolare, l'impugnato comma 335 sarebbe in contrasto con l'art. 3 della Costituzione, atteso che il singolo contribuente si troverebbe esposto a rivalutazione del proprio bene solo in base alla circostanza che il suo immobile è situato in una microzona oggetto di richiesta di revisione parziale del classamento da parte del Comune interessato. 
 La norma violerebbe, poi, l'art. 53 Cost., in quanto un riaccatastamento di una serie di edifici «collegato ai soli valori di mercato di zona e senza modificazioni nella realtà» si porrebbe in conflitto con il principio di capacità contributiva dei singoli, posto che il classamento, pur non essendo un vero e proprio atto di imposizione fiscale, incide sulla rendita del bene ed ha quindi effetti sull'imposizione diretta e su quella locale.
 Essa contrasterebbe, infine, con l'art. 97 Cost., dato che «non assicur[erebbe] né il buon andamento né l'imparzialità dell'amministrazione, colpendo indiscriminatamente tutte le unità immobiliari (di una determinata microzona) senza alcuna verifica concreta del singolo bene [...] esponendo l'amministrazione medesima ad una altrettanto "massiva" opposizione da parte dei contribuenti interessati». 
 2.- In via preliminare, va esaminata l'eccezione di inammissibilità sollevata dall'Avvocatura generale dello Stato, in considerazione del fatto che la questione di costituzionalità non sarebbe suscettibile di una soluzione costituzionalmente imposta e quindi involgerebbe scelte discrezionali spettanti esclusivamente al legislatore.
 2.1.- L'eccezione è inammissibile in quanto formulata in maniera generica e gravemente carente nella motivazione.
 3.- Nel merito, le censure sono infondate.
 4.- In via generale, si procede al riclassamento di un immobile quando, pur disponendo, quest'ultimo, già di una categoria, di una classe e di una rendita, si prospetta che tale classamento sia divenuto inadeguato. 
 Le cause che possono rendere necessaria l'operazione sono riconducibili alle ipotesi individuate dai commi 335 e 336 dell'art. 1 della legge n. 311 del 2004. 
 Si tratta di due procedure con presupposti diversi. La prima concerne situazioni legate a mutamenti di carattere generale o collettivo interessanti una determinata area; la seconda riguarda le situazioni di fatto non più coerenti con i classamenti passati per intervenute variazioni edilizie della singola unità immobiliare e presuppone quindi che si sia di fronte ad innovazioni specifiche relative ad un determinato immobile. 
 5.- Ai sensi del comma 335 - oggetto del giudizio - il procedimento prende avvio su iniziativa del Comune, il quale formula richiesta di revisione del classamento al competente Ufficio provinciale dell'Agenzia del territorio (ora assorbita dall'Agenzia delle entrate ai sensi del decreto-legge 6 luglio 2012, n. 95, recante «Disposizioni urgenti per la revisione della spesa pubblica con invarianza dei servizi ai cittadini nonché misure di rafforzamento patrimoniale delle imprese del settore bancario», convertito, con modificazioni, dalla legge 7 agosto 2012, n. 135). Quest'ultima, accertata la sussistenza dei presupposti di legge, con provvedimento del direttore attiva il «processo revisionale», che si snoda secondo le modalità tecniche e operative previste nel successivo comma 339 e stabilite nelle linee guida di cui alla Determinazione del direttore dell'Agenzia del territorio del 16 febbraio 2005. A conclusione del procedimento, gli intestatari delle unità immobiliari interessate ricevono un avviso di accertamento con la rideterminazione del classamento e l'attribuzione di una nuova rendita catastale.
 6.- Venendo all'esame delle censure, il contrasto con l'art. 3 Cost. è impostato nella limitata prospettiva secondo cui il singolo contribuente si troverebbe esposto a rivalutazione del proprio bene solo in base alla circostanza che il suo immobile sia situato in una microzona oggetto di richiesta di revisione parziale del classamento da parte del Comune interessato, con la conseguente sperequazione rispetto al contribuente che, pur trovandosi nella stessa situazione, non subisca analoga iniziativa comunale. 
 La censura, come è evidente, non è riferibile alla previsione della norma, ma è ricollegabile, invece, a circostanze contingenti - e cioè la maggiore o minore sollecitudine del Comune di riferimento nell'avanzare l'istanza - che attengono alla concreta applicazione della disciplina (ordinanza n. 270 del 2012). 
 Si tratta, dunque, di un inconveniente di fatto, che, secondo pacifica giurisprudenza, è irrilevante nel giudizio costituzionale (ex multis, sentenze n. 35 del 2017, n. 219 e n. 192 del 2016; ordinanza n. 122 del 2016).  
 7.- Il Collegio tributario prospetta poi la violazione dell'art. 53 Cost., in quanto un riaccatastamento conseguente alla sola variazione dei valori di mercato di una determinata aerea contrasterebbe con il principio di capacità contributiva. 
 7.1.- Va in proposito ricordato che questa Corte ha ritenuto che la rendita catastale «non costituisce [...] un presupposto d'imposta» (sentenza n. 162 del 2008): non è pertanto prospettabile, in riferimento ad essa, una questione collegata al parametro di cui all'art. 53 Cost. (sentenze n. 263 del 1994 e n. 162 del 2008). Si è però anche affermato che, benché le tariffe di estimo e le rendite catastali non siano atti di imposizione tributaria, i criteri per la loro determinazione, ove non ispirati a princìpi di ragionevolezza, potrebbero porre le premesse per l'incostituzionalità delle singole imposte che su di essi si fondino (sentenza n. 263 del 1994). 
 7.2.- Si è poi ritenuto che «la capacità contributiva, desumibile dal presupposto economico al quale l'imposta è collegata, può essere ricavata, in linea di principio, da qualsiasi indice rivelatore di ricchezza, secondo valutazioni riservate al legislatore, salvo il controllo di costituzionalità, sotto il profilo della palese arbitrarietà e manifesta irragionevolezza» (sentenza n. 162 del 2008).
 Esaminata la questione sotto questa specifica prospettiva, la scelta fatta dal legislatore con il censurato comma 335 non presenta profili di irragionevolezza. 
 La decisione di operare una revisione del classamento per microzone si basa sul dato che la qualità del contesto di appartenenza dell'unità immobiliare rappresenta una componente fisiologicamente idonea ad incidere sul valore del bene, tanto che il fattore posizionale già costituisce una delle voci prese in considerazione dal sistema catastale in generale.
 Può quindi ritenersi non irragionevole che l'accertamento di una modifica del valore degli immobili presenti in una determinata microzona abbia una ricaduta sulla rendita catastale. Il conseguente adeguamento, proprio in quanto espressione di una accresciuta capacità contributiva, è volto in sostanza ad eliminare una sperequazione esistente a livello impositivo. 
 7.3.- È bene ricordare, peraltro, che la natura e le modalità dell'operazione enfatizzano l'obbligo di motivazione in merito agli elementi che hanno, in concreto, interessato una determinata microzona, così incidendo sul diverso classamento della singola unità immobiliare; obbligo che, proprio in considerazione del carattere "diffuso" dell'operazione, deve essere assolto in maniera rigorosa in modo tale da porre il contribuente in condizione di conoscere le concrete ragioni che giustificano il provvedimento.
 8.- Infondata, infine, è la censura (peraltro formulata in maniera generica e apodittica) relativa all'art. 97 Cost., secondo la quale la rivalutazione «massiva» operata dalla norma censurata, colpendo indiscriminatamente tutte le unità immobiliari di una determinata microzona senza alcuna verifica concreta del singolo bene, «espo[rrebbe] l'amministrazione medesima ad una altrettanto "massiva" opposizione da parte dei contribuenti interessati». 
 Non è dato comprendere, infatti, come il rischio che l'Amministrazione sia esposta ad azioni giudiziarie possa rilevare nel senso dell'incostituzionalità di una disciplina. Ogni riforma normativa, per la sua portata innovativa, è potenzialmente idonea a suscitare reazioni da parte dei destinatari, ma ciò evidentemente rientra nella fisiologia dell'ordinamento.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara non fondata la questione di legittimità costituzionale dell'art. 1, comma 335, della legge 30 dicembre 2004, n. 311, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2005)», sollevata, in riferimento agli artt. 3, 53 e 97 della Costituzione, dalla Commissione tributaria regionale del Lazio, con l'ordinanza indicata in epigrafe. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 ottobre 2017. &#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Giancarlo CORAGGIO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria l'1 dicembre 2017.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
