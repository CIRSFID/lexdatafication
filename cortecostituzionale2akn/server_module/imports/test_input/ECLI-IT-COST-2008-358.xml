<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/358/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/358/"/>
          <FRBRalias value="ECLI:IT:COST:2008:358" name="ECLI"/>
          <FRBRdate date="22/10/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="358"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/358/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/358/ita@/!main"/>
          <FRBRdate date="22/10/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/358/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/358/ita@.xml"/>
          <FRBRdate date="22/10/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="31/10/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>FLICK</cc:presidente>
        <cc:relatore_pronuncia>Gaetano Silvestri</cc:relatore_pronuncia>
        <cc:data_decisione>22/10/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 5-bis del decreto-legge 11 luglio 1992, n. 333 (Misure urgenti per il risanamento della finanza pubblica), convertito, con modificazioni, dalla legge 8 agosto 1992, n. 359, promossi con ordinanze della Corte d'appello di Venezia del 20 aprile 2007, della Corte d'appello di Firenze del 24 luglio 2007 e della Corte d'appello di Torino del 22 ottobre 2007, iscritte, rispettivamente, ai nn. 616 e 780 del registro ordinanze 2007 ed al n. 134 del registro ordinanze 2008 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 36 e 47, prima serie speciale, dell'anno 2007 e n. 20, prima serie speciale, dell'anno 2008. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio dell'8 ottobre 2008 il Giudice relatore Gaetano Silvestri. 
Ritenuto che la Corte d'appello di Venezia, con ordinanza del 20 aprile 2007 (r.o. n. 616 del 2007), ha sollevato questione di legittimità costituzionale dell'art. 5-bis del decreto-legge 11 luglio 1992, n. 333 (Misure urgenti per il risanamento della finanza pubblica), convertito, con modificazioni, dalla legge 8 agosto 1992, n. 359, per violazione dell'art. 117, primo comma, della Costituzione, in relazione all'art. 1 del primo Protocollo addizionale alla Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU); 
che la rimettente è investita, in sede di giudizio di rinvio dalla Corte di cassazione, della decisione in ordine all'applicazione della decurtazione del 40% dell'indennità di espropriazione, prevista dall'art. 5-bis citato, essendo stata sul punto annullata, per difetto di motivazione, la sentenza pronunciata da altra sezione della medesima Corte d'appello; 
che, riferisce inoltre il giudice a quo, nell'atto di riassunzione i soggetti espropriati hanno avanzato domanda diretta ad ottenere «la determinazione della giusta indennità di espropriazione e la conseguente giusta indennità di occupazione nella somma già liquidata da questa Corte con la sentenza cassata ma senza la decurtazione del 40% e con l'aggiunta di un'ulteriore somma a titolo di indennità agricola ai sensi dell'art. 37 comma nono del D.P.R. 327/2001», mentre la controparte ha chiesto il rigetto delle domande e la conferma dell'indennità nella misura già determinata; 
che, prosegue la rimettente, disposta la riunione al predetto giudizio di altra causa pendente tra le stesse parti e precisate le conclusioni, in comparsa conclusionale gli appellanti in riassunzione hanno eccepito l'illegittimità costituzionale dell'art. 5-bis del decreto-legge n. 333 del 1992, richiamando la decisione assunta in data 29 marzo 2006 dalla Grande Chambre della Corte EDU, nella causa Scordino contro Governo italiano; 
che il giudice a quo condivide il dubbio di costituzionalità della norma citata e richiama, a sua volta, sia la motivazione della citata sentenza della Corte EDU, sia l'orientamento della giurisprudenza di legittimità (Cassazione civile, ordinanze n. 11887 del 2005 e n. 22357 del 2006, con le quali è stata sollevata identica questione), secondo cui, diversamente da quanto avviene per i regolamenti comunitari, deve ritenersi escluso che il giudice nazionale possa disapplicare le norme interne contrastanti con la Convenzione, così imponendosi il ricorso al giudice delle leggi; 
che la Corte rimettente conclude osservando che «l'eventuale esercizio, da parte dello Stato, del proprio potere normativo in materia espropriativa in contrasto con l'art. 1 del primo Protocollo addizionale della Convenzione Europea risulterebbe in contrasto con la disposizione dell'art. 117 della Costituzione»; 
che, con atto depositato il 9 ottobre 2007, è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale, dopo aver rilevato l'analogia tra la questione in esame ed altre quattro questioni già trattate all'udienza del 3 luglio 2007 (r.o. nn. 401, 402, 681 del 2006, e n. 2 del 2007), ha concluso per la declaratoria di non fondatezza;  
che anche le Corti d'appello di Firenze (r.o. n. 780 del 2007, del 24 luglio 2007) e di Torino (r.o. n. 134 del 2008, del 22 ottobre 2007) hanno sollevato questione di legittimità costituzionale dell'art. 5-bis del decreto-legge n. 333 del 1992, rispettivamente in riferimento all'art. 117 Cost. (r.o. n. 780 del 2007) ed agli artt. 111 e 117 Cost., anche in rapporto all'art. 6 della CEDU e all'art. 1 del primo Protocollo addizionale alla Convenzione EDU (r.o. n. 134 del 2008); 
che la Corte d'appello di Firenze, nell'ambito di due giudizi riuniti aventi ad oggetto la domanda di pagamento dell'indennità di occupazione d'urgenza di un'area edificabile e l'opposizione alla stima, è chiamata a determinare, ai sensi dell'art. 5-bis citato, l'indennità di espropriazione ed occupazione; 
che, secondo quanto riferito dalla rimettente, i soggetti espropriati hanno chiesto la disapplicazione dei criteri di quantificazione dell'indennità previsti dalla predetta norma e la rideterminazione dell'indennità medesima sulla base del valore venale dei beni ablati, prospettando, in subordine, «la necessità di sollevare la questione interpretativa ex art. 234 del Trattato UE, ovvero, in via ulteriormente gradata, la necessità di sollevare la questione di legittimità costituzionale dell'art. 5-bis»; 
che la Corte d'appello di Firenze argomenta sia in punto di inaccoglibilità della richiesta di disapplicazione dell'art. 5-bis per contrasto con le norme convenzionali, sia in punto di non manifesta infondatezza della questione di legittimità costituzionale della citata norma per contrasto con l'art. 117 Cost., richiamando diffusamente le motivazioni con le quali la Corte di cassazione ha sollevato identica questione (ordinanze n. 22357 e n. 12810 del 2006); 
che, in particolare, la rimettente si sofferma sulla pronuncia resa dalla Grande Chambre della Corte EDU, in causa Scordino contro Governo italiano, del 29 marzo 2006, nella quale si troverebbe affermato che il citato art. 5-bis viola il “sistema” della Convenzione sulla privazione della proprietà individuale per pubblica utilità, come interpretato sulla base della relazione tra i due commi dell'art. 1 del I Protocollo addizionale; 
che, infatti, la previsione di una indennità largamente inferiore al valore venale dei beni ablati, riducibile fino ad un terzo del prezzo di mercato, oltre al carico tributario, e senza considerazione dell'entità della causa di pubblica utilità, «rompe il giusto equilibrio tra interesse generale e diritto di proprietà individuale tutelato dall'art. 1 del I Protocollo addizionale», là dove tale ultima norma impone di regola un ristoro corrispondente al valore di mercato; 
che la rimettente richiama la motivazione della già citata ordinanza n. 22357 del 2006 della Corte di cassazione, nella quale si assume che le modifiche apportate all'art. 117 Cost. dalla legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte seconda della Costituzione), pur avendo effetto dalla data di entrata in vigore, incidono non solo sulla normativa futura ma anche su quella previgente, che deve essere dichiarata illegittima se con esse contrastanti; 
che, pertanto, l'art. 5-bis violerebbe l'art. 117, primo comma, Cost., in rapporto all'art. 1 del primo Protocollo addizionale alla Convenzione EDU; 
che la Corte d'appello di Torino solleva analoga questione, nell'ambito di giudizio di rinvio avente ad oggetto la rideterminazione dell'indennità di occupazione di aree edificabili; 
che, in punto di rilevanza, la rimettente precisa di dover applicare l'art. 5-bis del decreto-legge n. 333 del 1992, in quanto il principio di diritto enunciato dalla Corte di cassazione impone di liquidare l'indennità di occupazione in misura corrispondente ad una percentuale di quella che sarebbe spettata per l'espropriazione dell'area occupata; 
che nel ricorso in riassunzione i soggetti espropriati hanno eccepito l'illegittimità costituzionale della predetta norma, richiamando le ordinanze della Corte di cassazione che hanno sollevato identica questione (ordinanze n. 22357 e n. 12810 del 2006); 
che, in punto di non manifesta infondatezza, la rimettente dichiara di voler esporre argomentazioni «del tutto aderenti a quelle già svolte dalle citate ordinanze della Corte di cassazione», e richiama i relativi passaggi motivazionali, in termini sostanzialmente analoghi a quanto sintetizzato con riferimento alla questione sollevata dalla Corte d'appello di Firenze; 
che, in aggiunta alla prospettata violazione dell'art. 117, primo comma, Cost., comune anche alle ordinanze di rimessione provenienti dalle Corti d'appello di Venezia e di Firenze, la Corte d'appello di Torino censura l'art. 5-bis con riguardo all'applicazione retroattiva dei criteri in esso previsti, e ciò sul rilievo che nel giudizio a quo i soggetti espropriati avevano proposto opposizione alla stima prima dell'entrata in vigore della predetta norma; 
che, pertanto, sarebbe violato l'art. 111 Cost., in rapporto all'art. 6 della Convenzione EDU; 
che, in linea con le più volte citate ordinanze della Corte di cassazione, la rimettente osserva come il principio del «giusto processo», posto a garanzia della parità delle parti davanti al giudice, implichi tra l'altro il divieto di ingerenza del potere legislativo nella risoluzione della singola causa o di una determinata categoria di controversie; 
che, in prossimità della decisione, il Presidente del Consiglio dei ministri, già intervenuto nel giudizio introdotto con l'ordinanza r.o. n. 616 del 2007 della Corte d'appello di Venezia, ha depositato breve memoria nella quale rileva che la norma censurata è stata dichiarata costituzionalmente illegittima con la sentenza n. 348 del 2007 della Corte costituzionale, successiva all'ordinanza di rimessione, e conclude chiedendo che sia disposta la restituzione degli atti al giudice a quo per una nuova valutazione della rilevanza. 
Considerato che con tre distinte ordinanze le Corti d'appello di Venezia, Firenze e Torino sottopongono a scrutinio di costituzionalità l'art. 5-bis del decreto-legge 11 luglio 1992, n. 333 (Misure urgenti per il risanamento della finanza pubblica), convertito, con modificazioni, dalla legge 8 agosto 1992, n. 359, per contrasto con l'art. 117, primo comma, della Costituzione, in relazione all'art. 1 del primo Protocollo addizionale della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU); 
che la sola Corte d'appello di Torino solleva questione di legittimità costituzionale del citato art. 5-bis per contrasto con l'art. 111 Cost., anche in rapporto all'art. 6 della medesima Convenzione europea; 
che, stante l'identità delle questioni, i giudizi possono essere riuniti per essere decisi con unico provvedimento; 
che questa Corte, con la sentenza n. 348 del 2007, successiva alle ordinanze di rimessione, ha dichiarato l'illegittimità costituzionale dell'art. 5-bis, commi 1 e 2, del decreto-legge n. 333 del 1992, per contrasto con l'art. 117, primo comma, Cost.; 
che nella citata pronuncia è precisato che «la dichiarazione di illegittimità costituzionale della norma censurata in riferimento all'art. 117, primo comma, Cost., rende superflua ogni valutazione sul dedotto contrasto con l'art. 111 Cost., in rapporto all'applicabilità della stessa norma ai giudizi in corso al momento della sua entrata in vigore, poiché, ai sensi dell'art. 30, terzo comma, della legge 11 marzo 1953, n. 87, essa non potrà avere più applicazione dal giorno successivo alla pubblicazione delle presente sentenza»; 
che, inoltre, con la medesima sentenza è stata dichiarata l'illegittimità costituzionale, in via consequenziale, dei commi 1 e 2 dell'art. 37 del d.P.R. 8 giugno 2001, n. 327 (Testo unico delle disposizioni legislative e regolamentari in materia di espropriazione per pubblica utilità), stante l'identità delle norme ivi contenute con quelle già dichiarate in contrasto con la Costituzione; 
che, pertanto, deve essere disposta la restituzione degli atti ai giudici rimettenti, per un rinnovato esame della rilevanza delle questioni (ex plurimis, ordinanze nn. 340 e 317 del 2008). 
Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
riuniti i giudizi, &#13;
ordina la restituzione degli atti alle Corti d'appello di Venezia, Firenze e Torino. &#13;
</p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 ottobre 2008.  &#13;
F.to:  &#13;
Giovanni Maria FLICK, Presidente  &#13;
Gaetano SILVESTRI, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 31 ottobre 2008.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
