<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/184/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/184/"/>
          <FRBRalias value="ECLI:IT:COST:2017:184" name="ECLI"/>
          <FRBRdate date="06/06/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="184"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/184/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/184/ita@/!main"/>
          <FRBRdate date="06/06/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/184/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/184/ita@.xml"/>
          <FRBRdate date="06/06/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/07/2017" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2017</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Marta Cartabia</cc:relatore_pronuncia>
        <cc:data_decisione>06/06/2017</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 73, comma 1, del decreto del Presidente della Repubblica 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza), promosso dalla Corte di cassazione, sezione sesta penale, nel procedimento penale a carico di W. C., con ordinanza del 12 gennaio 2017, iscritta al n. 23 del registro ordinanze 2017 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 9, prima serie speciale, dell'anno 2017.
 Visti l'atto di costituzione di W. C., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 6 giugno 2017 il Giudice relatore Marta Cartabia;
 uditi l'avvocato Federico Vianelli per W. C. e l'avvocato dello Stato Massimo Giannuzzi per il Presidente del Consiglio dei ministri.
 Ritenuto che, con ordinanza del 12 gennaio 2017 (r.o. n. 23 del 2017), la Corte di cassazione, sezione sesta penale, ha sollevato «questione di legittimità costituzionale, per contrasto con gli artt. 25, 3 e 27 Cost., in relazione all'art. 73, comma l, d.P.R. 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza), nella parte in cui detta norma prevede - a seguito della sentenza n. 32 dell'11 febbraio 2014 della Corte costituzionale - la pena minima edittale di otto anni in luogo di quella di sei anni introdotta con l'art. 4-bis del decreto-legge 30 dicembre 2005, n. 272, convertito con modificazioni con la legge 21 febbraio 2006, n. 49»;
 che, in punto di rilevanza, il rimettente ha premesso di essere chiamato a pronunciarsi sul ricorso proposto dal pubblico ministero presso il Tribunale di Imperia contro la sentenza emessa all'esito del giudizio abbreviato il 22 aprile 2015 dal Giudice per l'udienza preliminare del medesimo Tribunale, con la quale l'imputato W. C. è stato condannato alle pene di legge, previa concessione delle attenuanti generiche e previa riqualificazione come fatto di «lieve entità», ai sensi dell'art. 73, comma 5, del d.P.R. n. 309 del 1990, delle originarie imputazioni ex art. 73, comma 1, dello stesso decreto;
 che, come precisato dalla rimettente Corte di cassazione, il pubblico ministero ha chiesto l'annullamento della sentenza per inosservanza o erronea applicazione di legge e contraddittorietà o manifesta illogicità della motivazione in relazione alla concessione delle circostanze attenuanti generiche e alla riqualificazione del fatto come di «lieve entità», ai sensi del citato art. 73, comma 5;
 che il giudice a quo considera erronea la riqualificazione del fatto operata dal giudice di prime cure, attesi il dato ponderale della sostanza detenuta, il contesto di spaccio continuato, abituale, a cadenze ravvicinate, condotto con modalità organizzate e rivolto ad un'ampia platea di clienti, tutti elementi che portano a ritenere la sussistenza dei presupposti dell'ipotesi non lieve delineata nell'art. 73, comma 1, del d.P.R. n. 309 del 1990;
 che da ciò il rimettente desume la rilevanza nel caso di specie delle questioni di legittimità costituzionale aventi ad oggetto il trattamento sanzionatorio previsto dall'art. 73, comma 1, del d.P.R. n. 309 del 1990, come risultante a seguito della pronuncia di illegittimità costituzionale dell'art. 4-bis del d.l. n. 272 del 2005, contenuta nella sentenza della Corte costituzionale n. 32 del 2014, ciò in quanto «[g]iudica il Collegio che, in considerazione del quantitativo di stupefacente oggetto delle condotte e delle modalità e circostanze dei fatti - dunque della concreta gravità dei reati sotto il profilo oggettivo e soggettivo -, pur non versandosi in casi riportabili al disposto del comma 5 dell'art. 73, si tratti nondimeno di vicende di non particolare gravità, rispetto alle quali il Giudice di merito investito del giudizio di rinvio attesterebbe l'entità della sanzione intorno al minimo edittale» della pena detentiva che la Corte di cassazione ritiene non conforme al dettato costituzionale;
 che, secondo la Corte di cassazione, il minimo edittale della pena detentiva di otto anni di reclusione, costituendo non il frutto di una scelta legislativa, ma il risultato in malam partem di una sentenza della Corte costituzionale, violerebbe il principio della riserva di legge di cui all'art. 25 Cost.;
 che, in particolare, secondo il giudice rimettente - ferma l'illegittimità costituzionale dell'art. 4-bis del d.l. n. 272 del 2005, dichiarata dalla Corte costituzionale con sentenza n. 32 del 2014 per contrasto con l'art. 77, secondo comma, Cost. e ferme le modifiche alla normativa in tema di stupefacenti conseguenti a detta pronuncia - il ripristino della disciplina anteriormente vigente risulterebbe, nondimeno, precluso con limitato riguardo alla previsione - nel corpo dell'art. 73, comma l, del d.P.R. n. 309 del 1990 - della pena detentiva minima edittale di otto anni di reclusione (in luogo di quella di sei anni di reclusione introdotta con la novella del 2005-2006);
 che, secondo il giudice a quo, la "re-introduzione" nell'ordinamento giuridico di una disposizione in malam partem, per effetto di una sentenza della Corte costituzionale, violerebbe il principio di riserva di legge in materia penale sancito dall'art. 25, secondo comma, Cost., norma imperativa direttamente connessa alla tutela dei diritti fondamentali della persona, da ritenere prevalente su quella confliggente di cui al citato art. 77, secondo comma, Cost.;
 che, secondo la Corte di cassazione, «le sentenze costituzionali di accoglimento - di natura sia ablativa, sia additiva -, avendo carattere di generalità (erga omnes), incidono direttamente sulla disciplina normativa vigente in una determinata materia e devono, pertanto, essere considerate quali vere e proprie fonti del diritto penale»;
 che, tuttavia, sempre secondo il rimettente, esse nella gerarchia delle fonti dovrebbero essere equiparate alla legge ordinaria e da ciò si dovrebbe ricavare che «l'esercizio della funzione legislativa ad opera della giustizia costituzionale non può non raffrontarsi, ed incontrare in essa un limite, con la riserva di legge in materia penale, sancita dall'art. 25, comma secondo, Cost. [...] secondo il quale gli interventi in materia penale tesi ad ampliare l'area di un'incriminazione ovvero ad inasprirne le sanzioni possono essere legittimamente compiuti soltanto ad opera del legislatore parlamentare»;
 che, secondo il giudice a quo, in base ai principi esposti nella nota sentenza della Corte costituzionale n. 394 del 2006 «lo scrutinio di costituzionalità - sebbene suscettibile di riverberare in una decisione in malam partem - sia consentito solo e soltanto con riguardo alle norme penali di favore in senso stretto, id est a quelle che introducono una disciplina speciale rispetto ad una disciplina generale», ma non nel caso di norme generali favorevoli quale dovrebbe essere considerato l'art. 73, comma 1, del d.P.R. n. 309 del 1990, prima della più volte citata pronuncia di illegittimità costituzionale;
 che, ad avviso del giudice a quo, non potrebbero invocarsi altre pronunce costituzionali, in particolare quelle riguardanti casi di norme emanate dal Governo «extra-delega» legislativa, scrutinati «nelle sentenze n. 28 del 2010 e n. 5 del 2014»: ciò in quanto «negli arresti testé rammentati, la Consulta ha dichiarato l'incostituzionalità in malam partem sul presupposto che, in detti casi, la norma favorevole espunta dall'ordinamento giuridico fosse stata emanata - nella sostanza - in assenza di potere legislativo e che, di conseguenza, non vi fosse materia per ritenere operante e validamente invocabile il principio della riserva di legge in materia penale, sancito dall'art. 25, secondo comma, Cost.»;
 che, secondo la Corte di cassazione, sarebbe diverso il caso in esame, nel quale la norma di cui al citato art. 4-bis è stata introdotta dal Parlamento quale disposizione aggiuntiva rispetto a quelle presenti, seppure in difetto di omogeneità, e quindi di nesso funzionale, tra le disposizioni del decreto-legge e quelle censurate introdotte nella legge di conversione, di tal che la norma generale favorevole sarebbe stata «adottata - non da parte del Governo extra delega, cioè sostanzialmente in mancanza di potere legislativo  -, bensì dal Parlamento, organo costituzionale cui spetta in via esclusiva di legiferare in materia penale, pur commettendo un vizio procedurale di rilievo costituzionale»;
 che il giudice a quo, in via subordinata, ritiene violato il principio di ragionevolezza e proporzione delle pene, ai sensi degli artt. 3 e 27 Cost., sia in considerazione dello iato sanzionatorio sussistente tra il massimo della pena prevista per il fatto lieve e il minimo della pena per il fatto non lieve, sia in considerazione della previsione di un trattamento sanzionatorio unitario o diversificato per categorie di stupefacente, a seconda del carattere lieve o non lieve delle condotte illecite;
 che la Corte di cassazione si manifesta consapevole del fatto che, con la sentenza n. 148 del 2016, la Corte costituzionale ha dichiarato inammissibile, per assenza di soluzioni costituzionalmente obbligate in materia riservata alla discrezionalità legislativa, una questione di legittimità costituzionale del medesimo art. 73, comma l, del d.P.R. n. 309 del 1990, come risultante a seguito della sentenza n. 32 del 2014, sollevata per la disparità di trattamento sussistente tra le pene ivi previste e quelle di cui al comma 5 del medesimo art. 73;
 che, tuttavia, a differenza del precedente, nella specie verrebbe sollecitato un intervento della Corte costituzionale volto ad ottenere, secondo i dettami di cui alla recente sentenza della Corte costituzionale n. 236 del 2016, la reductio ad legitimitatem della pena in base a una «grandezza già rinvenibile nell'ordinamento», nella specie rappresentata da quella già individuata dal legislatore del 2006, il quale aveva comminato, per le condotte previste nell'art. 73, comma l, del d.P.R. n. 309 del 1990, la pena minima di sei anni di reclusione per tutte le tipologie di droga e, dunque, secondo il rimettente, anche per quelle cosiddette pesanti;
 che, nell'opinione del rimettente, l'invocato petitum - mirante al ripristino del trattamento sanzionatorio da sei a venti anni di reclusione introdotto dal legislatore in sede di conversione del d.l. n. 272 del 2005 - costituirebbe l'unica soluzione conforme ai parametri costituzionali degli artt. 25, secondo comma, 3 e 27 Cost., così da rappresentare un intervento "a rime obbligate", idoneo a superare il vizio di inammissibilità rilevato dalla Corte riguardo alla precedente ordinanza di rimessione;
 che, con atto depositato il 21 marzo 2017, è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni vengano dichiarate inammissibili o infondate;
 che, in particolare, secondo l'interveniente le questioni sollevate sarebbero irrilevanti, in quanto, nel giudizio a quo, la Corte di cassazione non è stata chiamata a decidere sul trattamento sanzionatorio stabilito dall'art. 73, comma 1, del d.P.R. n. 309 del 1990, ma sulla correttezza della qualificazione giuridica dei fatti sussunti dal giudice di prime cure nell'ipotesi lieve di cui all'art. 73, comma 5, del citato d.P.R., di tal che sarebbe ininfluente sul giudizio del rimettente la decisione della Corte costituzionale, all'esito della quale (e qualunque essa sia) la Corte di cassazione null'altro potrebbe fare se non annullare con rinvio, senza poter applicare la pena per i fatti riqualificati;
 che, in ogni caso, le questioni sarebbero comunque infondate nel merito, in quanto il minimo edittale previsto nella norma denunciata dovrebbe considerarsi pienamente rispettoso del principio della riserva di legge ai sensi dell'art. 25, secondo comma, Cost., costituendo espressione di una scelta del legislatore parlamentare che ha ripreso vigore a seguito della declaratoria di illegittimità costituzionale della norma che l'aveva modificata;
 che incompatibile con il principio della riserva di legge sarebbe invece proprio l'intervento additivo richiesto dalla Corte di cassazione, in quanto fondato sull'utilizzo di un tertium comparationis ormai definitivamente espunto dall'ordinamento in quanto costituzionalmente illegittimo;
 che la norma denunciata costituirebbe il frutto di un esercizio non irragionevole della discrezionalità politica del Parlamento, dettato dall'allarme sociale rispetto al considerevole numero di condotte non lievi concernenti le cosiddette droghe "pesanti";
 che, con atto depositato il 21 marzo 2017, si è costituita la parte privata e ha chiesto che le sollevate questioni vengano accolte;
 che, con memoria depositata il 16 maggio 2017, il Presidente del Consiglio dei ministri ha insistito per l'inammissibilità e l'infondatezza delle questioni sollevate.
 Considerato che la Corte di cassazione, sezione sesta penale, dubita della legittimità costituzionale dell'art. 73, comma 1, del d.P.R. 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza), nella parte in cui detta norma prevede - a seguito della sentenza della Corte costituzionale n. 32 del 2014 - la pena minima edittale della reclusione nella misura di otto anni, in luogo di quella di sei anni introdotta con l'art. 4-bis del decreto-legge 30 dicembre 2005, n. 272 (Misure urgenti per garantire la sicurezza ed i finanziamenti per le prossime Olimpiadi invernali, nonché la funzionalità dell'Amministrazione dell'interno. Disposizioni per favorire il recupero di tossicodipendenti recidivi e modifiche al testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza, di cui al decreto del Presidente della Repubblica 9 ottobre 1990, n. 309), come convertito, con modificazioni, dalla legge 21 febbraio 2006, n. 49;
 che, in via principale, il giudice a quo ravvisa una violazione dell'art. 25, secondo comma, della Costituzione in quanto la misura della pena di otto anni, nel minimo edittale, sarebbe frutto di una sentenza della Corte costituzionale - la n. 32 del 2014 - che avrebbe determinato la "re-introduzione" di una disposizione penale in malam partem in contrasto con il principio della riserva di legge;
 che, in via subordinata, il rimettente ritiene violati gli artt. 3 e 27 Cost., in quanto lo iato sussistente tra il minimo edittale per i fatti non lievi concernenti le cosiddette droghe "pesanti" - otto anni - e il massimo della pena per i fatti lievi - quattro anni - per i quali, tra l'altro, non si distingue tra droghe "pesanti" e droghe "leggere", contrasterebbe con i principi di ragionevolezza e proporzione cui deve sottostare ogni previsione sanzionatoria in materia penale;
 che le questioni sollevate sono manifestamente inammissibili per molteplici ragioni;
 che l'art. 23, secondo comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale) richiede, ai fini dell'ammissibilità della questione di legittimità costituzionale in via incidentale, che «il giudizio non possa essere definito indipendentemente dalla risoluzione della questione» e che tale disposizione va interpretata nel senso che la questione sollevata deve avere ad oggetto una disciplina legislativa applicabile nel giudizio a quo da parte del giudice rimettente, sicché la questione, per essere rilevante, deve concernere la norma applicabile in quella certa fase del giudizio;
 che, infatti, secondo consolidata giurisprudenza costituzionale (ex plurimis, ordinanze n. 259 del 2016, n. 161 del 2015 e n. 117 del 1984), non è rilevante la questione di legittimità costituzionale avente ad oggetto un contenuto normativo che attiene al compimento di un atto processuale inserito in una fase successiva a quella in cui versa il giudizio a quo;
 che, per tali motivi, le questioni sollevate non sono rilevanti nel giudizio davanti alla Corte di cassazione, in quanto la suprema Corte non deve fare applicazione della norma sanzionatoria censurata, la quale verrà in rilievo nella successiva fase del giudizio di rinvio davanti al giudice di merito, alla cui cognizione dovrà essere eventualmente rimessa la determinazione della pena a seguito dell'annullamento della sentenza di primo grado, impugnata per inosservanza o erronea applicazione di legge e contraddittorietà o manifesta illogicità della motivazione in relazione alla concessione delle circostanze attenuanti generiche e alla riqualificazione del fatto;
 che, laddove la prospettiva decisoria del giudice a quo fosse quella del rigetto o della inammissibilità del ricorso, a maggior ragione si profilerebbe l'irrilevanza delle questioni, non potendo in alcun modo venire in discorso il punto relativo al trattamento sanzionatorio, che neppure costituisce oggetto di devoluzione da parte del ricorrente;
 che, inoltre, la questione sollevata in riferimento all'art. 25 Cost. è inammissibile anche per intima contraddittorietà della motivazione, in quanto da un lato il rimettente afferma la tesi secondo cui le sentenze della Corte costituzionale costituirebbero fonti del diritto equiparate alla legge - e, dall'altro, assume che, in quanto non equiparabile alla legge, la sentenza n. 32 del 2014 della Corte costituzionale violi la riserva di legge di cui all'art. 25 Cost.;
 che, sempre in riferimento alla lesione dell'art. 25 Cost., la questione è inammissibile perché consiste in una censura degli effetti della sentenza della Corte costituzionale n. 32 del 2014, di cui costituisce un improprio tentativo di impugnazione, in violazione dell'art. 137, terzo comma, Cost., secondo cui «[c]ontro le decisioni della Corte costituzionale non è ammessa alcuna impugnazione»;
 che tale improprio tentativo di impugnazione si risolve nel contestare l'affermazione della citata sentenza n. 32 del 2014 - analoga a quella già espressa dalla giurisprudenza costituzionale in relazione ai vizi della delega legislativa e del suo esercizio da parte del Governo, ex art. 76 Cost. (sentenze n. 5 del 2014 e n. 162 del 2012) - sulla ripresa dell'applicazione della normativa precedente a quella dichiarata costituzionalmente illegittima ex art. 77 Cost. data l'inidoneità dell'atto, per il radicale vizio procedurale che lo inficia, a produrre effetti abrogativi;
 che, tuttavia, contraddittoriamente, lo stesso giudice a quo vorrebbe far salvi gli effetti in bonam partem della medesima sentenza n. 32 del 2014, connessi alla ripresa di vigore della precedente disciplina sanzionatoria sui fatti non lievi riguardanti le cosiddette droghe "leggere";
 che, quindi, l'inammissibile tentativo di impugnazione di una sentenza della Corte costituzionale è addirittura fondato su una motivazione contraddittoria e illogica; ciò che costituisce ulteriore ragione di inammissibilità della sollevata questione di legittimità costituzionale;
 che, in relazione alle censure sollevate ai sensi degli artt. 3 e 27 Cost., l'ordinanza di rimessione risulta inammissibile anche per incompleta ed erronea ricostruzione del quadro normativo, in quanto lo iato edittale tra le pene previste rispettivamente al comma 1 e al comma 5 dell'art. 73 del d.P.R. n. 309 del 1990, quale risulta nella misura attuale e oggetto di censura, non è soltanto frutto degli effetti della sentenza n. 32 del 2014 della Corte costituzionale, ma anche di interventi del legislatore, precedenti e successivi alla citata decisione;
 che, segnatamente, il decreto-legge 20 marzo 2014, n. 36 (Disposizioni urgenti in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza, di cui al decreto del Presidente della Repubblica 9 ottobre 1990, n. 309, nonché di impiego di medicinali), convertito, con modificazioni, dalla legge 16 maggio 2014, n. 79, non solo ha ridotto il massimo edittale di pena prevista per i fatti lievi, allargando così, nei termini attuali, la forbice rispetto al minimo edittale previsto per i fatti non lievi, ma ha anche completamente ridisegnato il quadro normativo di riferimento, operando diversi adattamenti conseguenti alla decisione della Corte;
 che, in relazione a tutte le questioni sollevate, si richiede un inammissibile ripristino di una disciplina sanzionatoria contenuta in una disposizione dichiarata costituzionalmente illegittima per vizi procedurali di tale gravità da determinare l'inidoneità dello stesso a innovare l'ordinamento.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 73, comma 1, del d.P.R. 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza), sollevate, in riferimento agli artt. 3, 25, secondo comma, e 27 della Costituzione, dalla Corte di cassazione, sezione sesta penale, con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 giugno 2017.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Marta CARTABIA, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 13 luglio 2017.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
