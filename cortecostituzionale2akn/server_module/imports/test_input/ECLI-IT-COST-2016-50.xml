<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/50/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/50/"/>
          <FRBRalias value="ECLI:IT:COST:2016:50" name="ECLI"/>
          <FRBRdate date="24/02/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="50"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/50/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/50/ita@/!main"/>
          <FRBRdate date="24/02/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/50/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/50/ita@.xml"/>
          <FRBRdate date="24/02/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="09/03/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>FRIGO</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>24/02/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giuseppe FRIGO; Giudici : Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 2 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), promosso dal Giudice di pace di Termini Imerese nel procedimento penale a carico di G.P. con ordinanza del 12 dicembre 2014, iscritta al n. 82 del registro ordinanze 2015 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 20, prima serie speciale, dell'anno 2015.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 24 febbraio 2016 il Giudice relatore Giuseppe Frigo.
 Ritenuto che, con ordinanza del 12 dicembre 2014, il Giudice di pace di Termini Imerese, nel corso del processo penale nei confronti di una persona imputata del delitto di lesioni personali colpose (art. 590 del codice penale), ha sollevato, in riferimento agli artt. 3 e 24 della Costituzione, questione di legittimità costituzionale dell'art. 2 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui esclude l'applicazione della pena su richiesta delle parti nel procedimento penale davanti al giudice di pace;
 che il giudice a quo riferisce che alla prima udienza di trattazione il difensore dell'imputato aveva chiesto l'applicazione della pena ai sensi dell'art. 444 del codice di procedura penale, eccependo l'illegittimità costituzionale dell'art. 2 del d.lgs. n. 274 del 2000 nella parte in cui non consente di accedere a detto rito alternativo nel procedimento dinanzi al giudice di pace;
 che, ad avviso del rimettente, la questione sarebbe rilevante, giacché solo la preclusione stabilita dalla norma censurata impedirebbe di accogliere la richiesta della difesa;
 che quanto, poi, alla non manifesta infondatezza, il giudice a quo rileva che, dai lavori preparatori del codice di procedura penale, emergerebbe con chiarezza che i riti alternativi sono stati introdotti per «ragioni di praticità ed economia processuale», nonché in risposta all'esigenza di «deflazionare i procedimenti penali»;
 che siffatte rationes sarebbero riscontrabili anche in rapporto al procedimento penale davanti al giudice di pace, la cui disciplina è ispirata a criteri di massima semplificazione, con i quali risulterebbe del tutto coerente l'istituto del "patteggiamento", che evita l'instaurazione della fase dibattimentale;
 che la violazione dei principi di ragionevolezza e di eguaglianza (art. 3 Cost.) risulterebbe, d'altronde, evidente ove si consideri che nei casi in cui i reati di competenza del giudice di pace sono giudicati dal tribunale per ragioni di connessione, l'imputato può accedere ai riti alternativi, compreso il "patteggiamento", e fruire quindi dei relativi benefici sul piano sanzionatorio: con l'illogica conseguenza che per fatti più gravi l'interessato potrebbe ottenere una pena più mite; 
 che, d'altra parte, per le pene irrogate dal giudice di pace non è ammessa la sospensione condizionale, beneficio al quale possono invece accedere gli imputati giudicati dal tribunale: donde un ulteriore profilo di contrasto con l'art. 3 Cost.;
 che sarebbe violato, inoltre, l'art. 24 Cost., in quanto, «pur rimanendo identici gli elementi sia soggettivi che oggettivi del reato», il diritto di difesa dell'imputato, del quale la scelta dei riti alternativi è espressione, risulterebbe menomato dall'essere il reato devoluto alla competenza del giudice di pace, anziché del tribunale;
 che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile per insufficiente descrizione della fattispecie concreta e difetto di motivazione sulla rilevanza, ovvero, in subordine, manifestamente infondata.
 Considerato che il Giudice di pace di Termini Imerese dubita, in riferimento agli artt. 3 e 24 della Costituzione, della legittimità costituzionale dell'art. 2 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui esclude l'applicazione della pena su richiesta delle parti nel procedimento penale davanti al giudice di pace;
 che l'eccezione di inammissibilità della questione formulata dall'Avvocatura generale dello Stato non è fondata;
 che il rimettente ha riferito, infatti, di essere investito, nell'ambito del processo penale nei confronti di una persona imputata del delitto di lesioni personali colpose, della richiesta della difesa di applicazione della pena ai sensi dell'art. 444 del codice di procedura penale, richiesta che solo la norma censurata impedirebbe di accogliere: il che è sufficiente ai fini dell'assolvimento dell'onere di descrizione della fattispecie concreta e di motivazione sulla rilevanza della questione;
 che, quanto al merito, questa Corte ha già reiteratamente escluso che l'inapplicabilità dei riti alternativi - e, in particolare, del "patteggiamento" - nel procedimento davanti al giudice di pace, stabilita dall'art. 2, comma 1, del d.lgs. n. 274 del 2000, possa reputarsi in contrasto con gli artt. 3 e 24 Cost., dichiarando manifestamente infondate le questioni di legittimità costituzionale al riguardo sollevate (ordinanze n. 28 del 2007, n. 312 e n. 228 del 2005);
 che, in proposito, questa Corte ha rimarcato come il procedimento davanti al giudice di pace presenti caratteri assolutamente peculiari, che lo rendono non comparabile con il procedimento davanti al tribunale, e comunque tali da giustificare sensibili deviazioni rispetto al modello ordinario; 
 che il d.lgs. n. 274 del 2000 contempla, infatti, forme alternative di definizione, non previste dal codice di procedura penale, le quali si innestano in un procedimento connotato, già di per sé, da un'accentuata semplificazione e concernente reati di minore gravità, con un apparato sanzionatorio del tutto autonomo: procedimento nel quale il giudice deve inoltre favorire la conciliazione tra le parti (artt. 2, comma 2, e 29, commi 4 e 5), e in cui la citazione a giudizio può avvenire anche su ricorso della persona offesa (art. 21); 
 che, in particolare, l'istituto del patteggiamento mal si concilierebbe con il costante coinvolgimento della persona offesa nel procedimento, anche in rapporto alle forme alternative di definizione (artt. 34, comma 2, e 35, commi 1 e 5, del d.lgs. n. 274 del 2000); 
 che, di conseguenza, «le caratteristiche del procedimento davanti al giudice di pace consentono di ritenere che l'esclusione dell'applicabilità dei riti alternativi sia frutto di una scelta non irragionevole del legislatore [...], comunque tale da non determinare una ingiustificata disparità di trattamento», impedendo altresì di ravvisare in essa una violazione del diritto di difesa (ordinanze n. 28 del 2007 e n. 228 del 2005);
 che tali conclusioni non sono inficiate dal rilievo che, nel caso di connessione tra procedimenti di competenza del giudice di pace e procedimenti di competenza di altro giudice - connessione circoscritta, peraltro, dall'art. 6 del d.lgs. n. 274 del 2000 alla sola ipotesi del concorso formale di reati - è consentito il ricorso al "patteggiamento" anche per i reati attratti nella competenza del giudice superiore;
 che, infatti, le situazioni poste a raffronto «sono tra loro affatto diverse e non possono essere oggetto di comparazione al fine del giudizio di costituzionalità» (ordinanza n. 228 del 2005);
 che inconferente risulta, infine, il riferimento dell'odierno rimettente al fatto che, nel caso in cui il reato di competenza del giudice di pace sia giudicato dal tribunale per ragioni di connessione, l'imputato possa beneficiare, oltre che del "patteggiamento", anche della sospensione condizionale della pena, diversamente che nell'ipotesi in cui il reato fosse giudicato dal giudice onorario;
 che, a prescindere da ogni altro possibile rilievo, è sufficiente rilevare che la disparità di trattamento ora indicata non deriverebbe comunque dalla norma sottoposta a scrutinio, ma dalla distinta disposizione - l'art. 60 del d.lgs. n. 274 del 2000 - che rende inapplicabile l'istituto della sospensione condizionale alle pene inflitte dal giudice di pace (disposizione ritenuta, peraltro, anch'essa non in contrasto con l'art. 3 Cost. da questa Corte: sentenza n. 47 del 2014);
 che la questione va dichiarata, pertanto, manifestamente infondata.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 2 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), sollevata, in riferimento agli artt. 3 e 24 della Costituzione, dal Giudice di pace di Termini Imerese con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 febbraio 2016.&#13;
 F.to:&#13;
 Giuseppe FRIGO, Presidente e Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 9 marzo 2016.&#13;
 Il Cancelliere&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
