<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/169/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/169/"/>
          <FRBRalias value="ECLI:IT:COST:2011:169" name="ECLI"/>
          <FRBRdate date="11/05/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="169"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/169/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/169/ita@/!main"/>
          <FRBRdate date="11/05/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/169/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/169/ita@.xml"/>
          <FRBRdate date="11/05/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/05/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MADDALENA</cc:presidente>
        <cc:relatore_pronuncia>Sabino Cassese</cc:relatore_pronuncia>
        <cc:data_decisione>11/05/2011</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo MADDALENA; Giudici : Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito dei decreti del Presidente della Repubblica del 23 marzo 2011, con i quali, viste le sentenze di ammissibilità della Corte costituzionale nn. 24 e 26 del 2011, e vista la deliberazione del Consiglio dei ministri del 23 marzo 2011, sono stati indetti i due referendum, e sono stati convocati i relativi comizi per il giorno di domenica 12 giugno 2011, con prosecuzione delle operazioni di votazione nel giorno successivo, giudizio promosso da Carsetti Paolo, nella qualità di presidente e legale rappresentante del Comitato promotore per il Sì ai referendum per l'Acqua Pubblica, con ricorso depositato in cancelleria l'8 aprile 2011 ed iscritto al n. 1 del registro conflitti tra poteri dello Stato 2011, fase di ammissibilità.
 Udito nella camera di consiglio dell'11 maggio 2011 il Giudice relatore Sabino Cassese.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che, con ricorso depositato l'8 aprile 2011, Paolo Carsetti, nella qualità di presidente e legale rappresentante del Comitato promotore per il Sì ai referendum per l'Acqua Pubblica - referendum ammessi da questa Corte con sentenze n. 24 e n. 26 del 2011 e riguardanti, il primo, l'art. 23-bis del decreto-legge 25 giugno 2008, n. 112 (Disposizioni urgenti per lo sviluppo economico, la semplificazione, la competitività, la stabilizzazione della finanza pubblica e la perequazione tributaria), convertito, con modificazioni, dalla legge 6 agosto 2008, n. 133, come modificato dall'art. 30, comma 26, della legge 23 luglio 2009, n. 99 (Disposizioni per lo sviluppo e l'internazionalizzazione delle imprese, nonché in materia di energia), e dall'art. 15 del decreto-legge 25 settembre 2009, n. 135 (Disposizioni urgenti per l'attuazione di obblighi comunitari e per l'esecuzione di sentenze della Corte di giustizia della Comunità europea), convertito, con modificazioni, dalla legge 20 novembre 2009, n. 166; il secondo, l'art. 154, comma 1, del decreto legislativo del 3 aprile 2006, n. 152 (Norme in materia ambientale) - ha sollevato conflitto di attribuzione tra poteri dello Stato nei confronti del Consiglio dei ministri, in riferimento ai decreti del Presidente della Repubblica del 23 marzo 2011 che hanno indetto i referendum, in una data (12-13 giugno 2011) diversa da quella stabilita per le elezioni amministrative (15-16 maggio);
 che, con riguardo alla ammissibilità del ricorso, sotto il profilo soggettivo, i ricorrenti ritengono pacifica la qualificazione del comitato promotore come potere dello Stato, richiamando l'orientamento costante di questa Corte, che risale all'ordinanza n. 17 del 1978, in base al quale il comitato promotore di un referendum, pur essendo figura soggettiva esterna rispetto allo Stato-apparato, è titolare di funzioni pubbliche tutelate dall'art. 75 della Costituzione;
 che, sotto il profilo oggettivo, i ricorrenti sostengono che il Governo abbia fatto cattivo uso del potere attribuitogli dall'art. 34 della legge 25 maggio 1970, n. 325 (Norme sui referendum previsti dalla Costituzione e sulla iniziativa legislativa del popolo), non avendo accorpato lo svolgimento dei referendum con le elezioni amministrative indette il 15-16 maggio 2011;
 che in tal modo, secondo i ricorrenti, il Governo - «lungi dall'implementare il mandato dell'art. 3 Cost.», nella parte in cui richiede la rimozione degli ostacoli che impediscono l'effettiva partecipazione all'organizzazione politica del paese - avrebbe compiuto una scelta irragionevole, «invasiv[a] e lesiv[a] di attribuzioni di rilievo costituzionale dei ricorrenti in quanto rappresentanti del popolo sovrano», perché il mancato accorpamento rivelerebbe un tentativo di elusione della richiesta referendaria, che contrasta con il principio d'imparzialità nell'esercizio dei pubblici poteri e con il favor che assiste l'istituto referendario (art. 75 Cost.);
 che la decisione del Governo sarebbe altresì contraria al principio di buon andamento sancito dall'art. 97 Cost., in quanto il mancato accorpamento del referendum con le elezioni amministrative arrecherebbe un notevole danno alle finanze pubbliche, oltre che all'economia nazionale, e perciò violerebbe i criteri di efficienza, efficacia ed economicità che connotano la buona azione amministrativa;
 che i ricorrenti richiamano le pronunce con le quali questa Corte ha chiarito che la discrezionalità di cui gode il Governo nello scegliere la data delle consultazioni incontra il limite delle ipotesi in cui «sussistano oggettive situazioni di carattere eccezionale [...] idonee a determinare un'effettiva menomazione del diritto di voto referendario» (ordinanze n. 38 del 2008, n. 198 del 2005 e n. 131 del 1997) e affermano che, nel caso specifico, siffatte situazioni oggettive di carattere eccezionale sarebbero rappresentate dalla duplice circostanza che «i comizi elettorali per le elezioni amministrative sono già stati convocati in date interne alla finestra referendaria» e che il Paese sta attraversando una crisi economica di gravità eccezionale, tale da rendere la scelta compiuta dal Governo irragionevole e lesiva dell'esercizio del diritto di voto referendario;
 che la determinazione da parte del Governo della data dei referendum sarebbe lesiva della sfera di attribuzioni dei ricorrenti perché avvenuta in violazione del principio - immanente nell'ordinamento costituzionale - di leale collaborazione tra poteri, in base al quale tale data dovrebbe essere stabilita in concertazione con il comitato promotore e previa audizione dello stesso;
 che, poi, sorgerebbero dubbi di legittimità costituzionale in relazione all'art. 34 della legge n. 352 del 1970, nella parte in cui non prevede il coinvolgimento del comitato promotore nella determinazione della data d'indizione dei referendum, «unica fase di tutto il procedimento in cui esso non è chiamato in causa»;
 che, in ogni caso, anche se la legge non prevede la consultazione del comitato, «ciò non significa che il Governo non debba attenersi al principio di leale collaborazione tra poteri dello Stato, che trova applicazione ogni qual volta diversi poteri abbiano in cura il medesimo interesse (che in questo caso non può che essere l'esercizio della sovranità popolare)»;  
 che i ricorrenti chiedono, pertanto, che questa Corte pronunci un'ordinanza di sospensione ex art. 26 delle Norme integrative per i giudizi davanti alla Corte costituzionale del 7 ottobre 2008, in considerazione «della durata minima necessaria di tale procedimento e dell'urgenza di una decisione che risolva il conflitto sollevato»; che «sollev[i] davanti a sé la questione di legittimità costituzionale dell'art. 34 della legge n. 352 del 1970 nella parte in cui non prevede che il comitato promotore del referendum partecipi con il Governo alla determinazione della data del referendum»; che «annull[i] i decreti del Presidente della Repubblica del 23 marzo 2011 pubblicati nella Gazzetta Ufficiale n. 77 del 4 aprile 2011 e indic[hi] la fissazione di una nuova data, coincidente con la data del primo turno delle elezioni amministrative (15-16 maggio) o con quella del secondo turno (29 maggio)»; che, «in subordine, qualora i tempi non consentano l'anticipazione delle consultazioni referendarie, posticip[i] la data delle elezioni amministrative al 12-13 giugno, massimizzando in tal modo il risparmio di denaro pubblico secondo quanto disposto dall'art. 97 Costituzione»;
 che, con memoria integrativa depositata il 19 aprile 2011, il comitato promotore lamenta che il mancato accorpamento della consultazione referendaria con le elezioni amministrative produrrebbe un ulteriore effetto negativo, consistente nella disinformazione degli elettori circa il suo svolgimento, ulteriormente acuito sia dalla mancata regolamentazione delle tribune referendarie da parte della Commissione per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi, sia dall'introduzione - a norma dell'art. 13 della legge 30 aprile 1999, n. 120 (Disposizioni in materia di elezione degli organi degli enti locali, nonché disposizioni sugli adempimenti in materia elettorale) - della tessera elettorale, che, dato il suo carattere permanente, non ha, a differenza del preesistente certificato elettorale, funzione di notifica rispetto alle consultazioni referendarie;
 che, infine, i ricorrenti formulano due richieste aggiuntive, invitando questa Corte a sollevare davanti a sé la questione di legittimità costituzionale dell'art. 13 della legge n. 120 del 1999, «nella parte in cui istituisce la tessera elettorale per tutte le consultazioni referendarie, senza considerare la particolarità delle consultazioni referendari[e] data dall'elemento costitutivo di validità del quorum», e a «disporre che, anche nelle more della fissazione dell'udienza di merito, sia data agli elettori debita comunicazione, notificata personalmente, delle consultazioni referendarie del 12 e 13 giugno».
 Considerato che, ai sensi dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), in questa fase questa Corte è chiamata a delibare esclusivamente se il ricorso sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivi e oggettivi di un conflitto di attribuzione tra poteri dello Stato;
 che, sotto il profilo soggettivo, la giurisprudenza di questa Corte è costante nel ritenere che va riconosciuto agli elettori, in numero non inferiore a 500.000, sottoscrittori della richiesta di referendum - dei quali i promotori sono competenti a dichiarare la volontà in sede di conflitto - la titolarità, nell'ambito della procedura referendaria, di una funzione costituzionalmente rilevante e garantita, in quanto essi attivano la sovranità popolare nell'esercizio dei poteri referendari (ex multis, ordinanze n. 172 del 2009, n. 38 del 2008 e n. 17 del 1978);
 che, ancora sotto il profilo soggettivo, il conflitto è proponibile nei confronti del Governo;
 che, in relazione al requisito oggettivo, occorre verificare se gli atti impugnati possano dar luogo a una lesione della sfera di attribuzioni che le norme costituzionali assegnano al comitato promotore;
 che, a questo proposito, i ricorrenti sostengono che il Governo abbia fatto cattivo uso del potere di fissazione della data del referendum, non avendone accorpato lo svolgimento con le elezioni amministrative e compiendo così una scelta irragionevole che rivelerebbe un tentativo di elusione della richiesta referendaria e che contrasterebbe con il principio di buon andamento sancito dall'art. 97 Cost., in quanto arrecherebbe un notevole danno alle finanze pubbliche;
 che questa Corte ha già chiarito che «rientra nella sfera delle attribuzioni del comitato la pretesa allo svolgimento delle operazioni di voto referendario, una volta compiuta la procedura di verifica della legittimità e della costituzionalità delle relative domande, ma non anche la pretesa alla scelta, tra le molteplici, legittime opzioni, della data entro l'arco temporale prestabilito» (ordinanza n. 131 del 1997; ordinanze n. 38 del 2008 e n. 198 del 2005);
 che, inoltre, questa Corte ha affermato che «l'individuazione di un rigido e ristretto arco temporale, entro il quale deve essere tenuta la votazione, rivela che la valutazione dei possibili interessi coinvolti é stata effettuata dal legislatore, secondo la disciplina, di per sé non irragionevole, dettata dalla legge n. 352 del 1970 in un contesto procedimentale con puntuali scansioni temporali, che rende, nella fisiologia del sistema, non altrimenti vincolata la scelta della data all'interno del predetto arco temporale, salvo che sussistano oggettive situazioni di carattere eccezionale - [...] idonee a determinare un'effettiva menomazione dell'esercizio del diritto di voto referendario» (ordinanza n. 131 del 1997);
 che le situazioni considerate «eccezionali» dal comitato promotore sono in realtà circostanze ordinarie e, in ogni caso, riferibili a situazioni «esterne» o di contesto: esse non incidono direttamente sul diritto di voto referendario e non ne precludono l'esercizio;
 che, pertanto, in assenza di tali oggettive situazioni di carattere eccezionale, il mancato accorpamento dei referendum con le elezioni amministrative di per sé non agevola, ma neppure ostacola, lo svolgimento delle operazioni di voto referendario e non è suscettibile di incidere sulle attribuzioni costituzionalmente garantite del comitato promotore;
 che, non essendo configurabile, in ordine alla scelta della data, una specifica potestà costituzionalmente garantita del comitato promotore, risulta inconferente il richiamo al principio di leale collaborazione: esso in tanto può trovare applicazione in quanto vi sia l'esigenza di coordinare l'esercizio di prerogative analoghe spettanti a poteri diversi che concorrono alla cura di un medesimo interesse costituzionalmente rilevante, né sussistono i presupposti affinché questa Corte sollevi dinanzi a sé la questione di legittimità costituzionale dell'art. 34 della legge n. 352 del 1970, secondo quanto richiesto dai ricorrenti;
 che, quanto al presunto contrasto della scelta governativa con il principio di buon andamento, occorre osservare che, in assenza di situazioni oggettive di carattere eccezionale, nella fissazione della data del referendum spetta al Governo, nell'ambito della cornice temporale definita dalla legge, «la valutazione dei possibili interessi coinvolti» (ordinanza n. 131 del 1997), tra i quali rientra anche quello al contenimento della spesa;
 che anche le circostanze menzionate nella censura secondo cui il mancato accorpamento avrebbe l'effetto di disinformare gli elettori circa lo svolgimento della consultazione referendaria e che tale effetto di disinformazione sarebbe ulteriormente acuito dall'introduzione, a norma dell'art. 13 della legge 30 aprile 1999, n. 120, della tessera elettorale - a prescindere dalla ammissibilità di tale censura, perché avanzata solo nella memoria integrativa - non introducono ostacoli che impediscono lo svolgimento delle operazioni di voto referendario e, quindi, non ledono le attribuzioni del comitato promotore costituzionalmente garantite dall'art. 75 Cost.;
 che, in conclusione, assorbita ogni altra questione, il ricorso per conflitto di attribuzione è inammissibile per mancanza del requisito oggettivo.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara inammissibile il ricorso per conflitto di attribuzione tra poteri dello Stato proposto dal Comitato promotore per il Sì ai referendum per l'Acqua Pubblica nei confronti del Consiglio dei ministri con ricorso indicato in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 maggio 2011.&#13;
 F.to:&#13;
 Paolo MADDALENA, Presidente&#13;
 Sabino CASSESE, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 13 maggio 2011.&#13;
 Il Cancelliere&#13;
 F.to: MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
