<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/463/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/463/"/>
          <FRBRalias value="ECLI:IT:COST:2006:463" name="ECLI"/>
          <FRBRdate date="13/12/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="463"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/463/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/463/ita@/!main"/>
          <FRBRdate date="13/12/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/463/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/463/ita@.xml"/>
          <FRBRdate date="13/12/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="28/12/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>FLICK</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Tesauro</cc:relatore_pronuncia>
        <cc:data_decisione>13/12/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 23 della legge 17 febbraio 1992, n. 179 (Norme per l'edilizia residenziale pubblica), promosso con ordinanza del 26 gennaio 2006 dal Tribunale di Modena, nel procedimento civile vertente tra Odorici Roberta e il Comune di Vignola, iscritta al n. 194 del registro ordinanze e pubblicata nella Gazzetta Ufficiale della Repubblica n. 26, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 6 dicembre 2006 il Giudice relatore Giuseppe Tesauro. 
    Ritenuto che il Tribunale di Modena, con ordinanza del 26 gennaio 2006, ha sollevato questione di legittimità costituzionale dell'art. 23 (recte: art. 23, comma 2) della legge 17 febbraio 1992, n. 179 (Norme per l'edilizia residenziale pubblica), in riferimento agli artt. 3 e 42 della Costituzione; 
    che, nel giudizio a quo, la proprietaria di un appartamento edificato su di un suolo concesso in proprietà dal Comune di Vignola, destinato ad edilizia abitativa residenziale di tipo economico e popolare, compreso nel piano di zona formato ai sensi delle leggi 13 aprile 1962, n. 167 (Disposizioni per favorire l'acquisizione di aree fabbricabili per l'edilizia economica e popolare) e 22 ottobre 1971, n. 865 (Programmi e coordinamento dell'edilizia residenziale pubblica; norme sulla espropriazione per pubblica utilità; modifiche ed integrazioni alla L. 17 agosto 1942, n. 1150; L. 18 aprile 1962, n. 167; L. 29 settembre 1964, n. 847; ed autorizzazione di spesa per interventi straordinari nel settore dell'edilizia residenziale, agevolata e convenzionata), ha chiesto la condanna di detto Comune alla restituzione della somma allo stesso versata, alla scopo di ottenere l'autorizzazione alla vendita dell'alloggio, pari alla differenza tra il valore di mercato dell'area al momento dell'alienazione ed il prezzo di acquisto stabilito nella convenzione urbanistica stipulata ai sensi dell'art. 35 della legge n. 865 del 1971; 
    che, secondo il Tribunale di Modena, la norma impugnata, abrogando i commi dal quindicesimo al diciannovesimo dell'art. 35 della legge n. 865 del 1971, ha eliminato i vincoli temporali stabiliti per la cessione degli alloggi oggetto della norma, con conseguente insussistenza dell'obbligo del proprietario di corrispondere al Comune la citata somma, nel caso di alienazione dell'immobile anteriormente al termine dalla stessa fissato; 
    che, ad avviso del rimettente, l'abrogazione di detti divieti, disposta «con effetto ex nunc», comporterebbe comunque «la cessazione dell'efficacia delle clausole contrattuali che li riportano, indipendentemente dal fatto che la Convenzione sia stata stipulata prima dell'entrata in vigore della legge abrogativa»; 
    che il giudice a quo dà atto che il convenuto ha, tuttavia, eccepito l'illegittimità costituzionale dell'art. 23, comma 2, della legge n. 179 del 1992 «rispetto al principio di parità stabilito dall'art. 3 della Costituzione nonché rispetto all'art. 42 della medesima, con riferimento alla funzione sociale della proprietà» e, quindi, solleva questione di costituzionalità di detta norma in riferimento ad entrambi i parametri; 
    che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente inammissibile, per difetto di motivazione in ordine alla non manifesta infondatezza. 
    Considerato che il Tribunale di Modena dubita della legittimità costituzionale dell'art. 23 (recte: art. 23, comma 2) della legge 17 febbraio 1992, n. 179 (Norme per l'edilizia residenziale pubblica), in riferimento agli artt. 3 e 42 della Costituzione; 
    che l'ordinanza di rimessione manca del tutto della motivazione in ordine alla non manifesta infondatezza della questione ed il mero rinvio alla richiesta della difesa di una delle parti non può colmare detta lacuna, in quanto «il giudice deve rendere esplicite le ragioni che lo portano a dubitare della costituzionalità della norma con una motivazione autosufficiente» (così, per tutte, ordinanze n. 423 e n. 312 del 2005); 
    che, pertanto, indipendentemente da ogni ulteriore valutazione, deve essere dichiarata la manifesta inammissibilità della presente questione di legittimità costituzionale. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 23, comma 2, della legge 17 febbraio 1992, n. 179 (Norme per l'edilizia residenziale pubblica), sollevata, in riferimento agli artt. 3 e 42 della Costituzione, dal Tribunale di Modena con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 dicembre 2006. &#13;
F.to: &#13;
Giovanni Maria FLICK, Presidente &#13;
Giuseppe TESAURO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 28 dicembre 2006. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
