<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/326/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/326/"/>
          <FRBRalias value="ECLI:IT:COST:2006:326" name="ECLI"/>
          <FRBRdate date="02/10/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="326"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/326/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/326/ita@/!main"/>
          <FRBRdate date="02/10/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/326/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/326/ita@.xml"/>
          <FRBRdate date="02/10/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/10/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>02/10/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 1 della legge 1° agosto 2003, n. 207 (Sospensione condizionata dell'esecuzione della pena detentiva nel limite massimo di due anni), promosso con ordinanze del 22 novembre 2005 dal Magistrato di sorveglianza di Palermo e del 7 dicembre 2005 dal Tribunale di sorveglianza di Bari sui ricorsi proposti da Di Leonardo Mario e da Verardi Giovanni, iscritte ai nn. 70 e 85 del registro ordinanze 2006 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 12 e 14, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 5 luglio 2006 il Giudice relatore Alfio Finocchiaro. 
    Ritenuto che, con ordinanza del 22 novembre 2005, il Magistrato di sorveglianza di Palermo, sulla istanza di un condannato, cui era già stata revocata la misura alternativa alla detenzione, di fruire del beneficio della sospensione condizionata dell'esecuzione della pena detentiva, ai sensi della legge 1° agosto 2003, n. 207 (Sospensione condizionata dell'esecuzione della pena detentiva nel limite massimo di due anni), ha sollevato questione di legittimità costituzionale dell'art. 1 della predetta legge, nella parte in cui non prevede l'esclusione di detto beneficio nei confronti dei soggetti cui è stata revocata una misura alternativa, per contrasto con gli artt. 2, 3 e 27 della Costituzione; 
    che il rimettente richiama la sentenza n. 278 del 2005 con la quale la Corte costituzionale ha dichiarato l'illegittimità costituzionale dell'art. 1, comma 3, lettera d), della legge n. 207 del 2003, norma, questa, che costituiva la base letterale e normativa di quell'indirizzo interpretativo che precludeva l'accesso al c.d. indultino anche a quei soggetti che «erano stati ammessi alla misura alternativa», poi però revocata per fatto colpevole; 
    che, secondo il giudice a quo, a seguito della citata sentenza, tale indirizzo interpretativo sarebbe rimasto privo di base testuale, il che comporterebbe la conseguenza, grave e paradossale, dell'ammissibilità del beneficio anche per soggetti cui la misura alternativa è stata revocata, come nel caso di specie; 
    che la concessione di tale beneficio ad un soggetto che si è mostrato "immeritevole" di una misura alternativa (magari di contenuto analogo al c.d. indultino, come nel caso dell'affidamento in prova al servizio sociale), rispetto al quale l'intervenuta revoca può costituire un indice di accresciuta pericolosità sociale, sarebbe fonte di irragionevole disparità di trattamento di casi simili (si pensi alla disciplina di cui all'art. 58-quater della legge n. 354 del 1975) e causa di possibili gravi pregiudizi ai beni della collettività, esposti al ritorno in libertà di un soggetto in ipotesi più pericoloso, e, soprattutto, manifestamente contraria al principio rieducativo cui la pena e la esecuzione della stessa nelle sue varie forme devono necessariamente tendere; 
    che, con ordinanza del 7 dicembre 2005, il Tribunale di sorveglianza di Bari, in sede di reclamo proposto, ai sensi dell'art. 2, comma 2, della legge n. 207 del 2003, avverso l'ordinanza con la quale il Magistrato di sorveglianza di Bari aveva rigettato la istanza di un condannato, al quale era stata già revocata la misura alternativa dell'affidamento in prova al servizio sociale, di fruire del beneficio della sospensione condizionata dell'esecuzione della parte finale della pena detentiva, ha sollevato la medesima questione di legittimità costituzionale, con riferimento agli stessi parametri e sulla base di analoghe argomentazioni; 
    che nel giudizio promosso dal Tribunale di sorveglianza di Bari è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione venga dichiarata inammissibile o infondata. 
    Considerato che il Magistrato di sorveglianza di Palermo e il Tribunale di sorveglianza di Bari dubitano della legittimità costituzionale dell'art. 1 della legge 1° agosto 2003, n. 207 (Sospensione condizionata dell'esecuzione della pena detentiva nel limite massimo di due anni), nella parte in cui non prevede come causa ostativa del beneficio della sospensione condizionata della pena la revoca di una misura alternativa, per violazione dell'art. 2 e dell'art. 3 della Costituzione, per irragionevole disparità di trattamento tra i soggetti che hanno subito la revoca di una misura alternativa alla detenzione e usufruiscono di questo beneficio e quelli che parimenti hanno subito la revoca di una misura alternativa, i quali non possono, ex art. 58-quater della legge 26 luglio 1975, n. 354, essere assegnati al lavoro esterno, usufruire di permessi premio, della detenzione domiciliare o della semilibertà o essere affidati in prova al servizio sociale, e, inoltre, per violazione dell'art. 27 della Costituzione, perché la pena non avrebbe alcuna funzione rieducativa; 
    che le ordinanze di rimessione sollevano questioni di legittimità costituzionale della stessa disposizione di legge con motivazioni analoghe, sicché i relativi giudizi devono essere riuniti per essere decisi con unico provvedimento; 
    che, successivamente alla proposizione delle questioni, questa Corte, con sentenza n. 255 del 2006, ha dichiarato l'illegittimità costituzionale dell'art. 1, comma 1, della legge 1° agosto 2003, n. 207, nella parte in cui non prevede che il giudice di sorveglianza possa negare la sospensione condizionata dell'esecuzione della pena detentiva al condannato sulla base di un giudizio di non meritevolezza del beneficio, per il contrasto dell'automatismo che si rinviene nella norma denunciata con i principi di proporzionalità e individualizzazione della pena; 
    che, pertanto, va ordinata la restituzione degli atti ai giudici rimettenti, al fine di un nuova valutazione della rilevanza delle questioni proposte, alla luce della predetta sopravvenuta sentenza di questa Corte n. 255 del 2006 (negli stessi termini, ex plurimis, ordinanze n. 346, n. 229 e n. 206 del 2005).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti ai giudici rimettenti. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 2 ottobre 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfio FINOCCHIARO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 6 ottobre 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
