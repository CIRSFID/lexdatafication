<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/25/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/25/"/>
          <FRBRalias value="ECLI:IT:COST:2018:25" name="ECLI"/>
          <FRBRdate date="10/01/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="25"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/25/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/25/ita@/!main"/>
          <FRBRdate date="10/01/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/25/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/25/ita@.xml"/>
          <FRBRdate date="10/01/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="09/02/2018" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2018</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Franco Modugno</cc:relatore_pronuncia>
        <cc:data_decisione>10/01/2018</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 10-bis del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), promosso dal Tribunale ordinario di Verbania nel procedimento penale a carico di C. C., con ordinanza del 17 ottobre 2014, iscritta al n. 89 del registro ordinanze 2017 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 25, prima serie speciale, dell'anno 2017. 
 Udito nella camera di consiglio del 10 gennaio 2018 il Giudice relatore Franco Modugno.
 Ritenuto che, con ordinanza del 17 ottobre 2014, pervenuta a questa Corte, con la prova delle notifiche prescritte, il 23 maggio 2017, il Tribunale ordinario di Verbania ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 10-bis del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), nella parte in cui, con riferimento ai fatti commessi sino al 17 settembre 2011, punisce l'omesso versamento delle ritenute risultanti dalla certificazione rilasciata ai sostituiti per un ammontare superiore ad euro 50.000 per ciascun periodo d'imposta, anziché ad euro 103.291,38;
 che il giudice a quo premette di essere investito del processo penale nei confronti di una persona imputata del reato previsto dalla norma censurata, per aver omesso, in qualità di legale rappresentante di una società a responsabilità limitata, di versare, entro il termine per la presentazione della dichiarazione annuale di sostituto d'imposta (ossia entro il 20 agosto 2010), ritenute certificate effettuate nell'anno d'imposta 2009 per un importo complessivo di euro 80.461;
 che il rimettente riferisce, altresì, che l'istruzione dibattimentale svolta avrebbe confermato l'avvenuto superamento della soglia di rilevanza penale di euro 50.000, prevista dall'art. 10-bis del d.lgs. n. 74 del 2000, essendo incontestato anche dalla difesa l'omesso versamento di ritenute certificate effettuate nel 2009 per un ammontare di euro 70.462,01: donde la rilevanza della questione;
 che quanto, poi, alla non manifesta infondatezza, il rimettente osserva come, con la sentenza n. 80 del 2014, la Corte costituzionale abbia dichiarato costituzionalmente illegittimo, in riferimento all'art. 3 Cost., l'art. 10-ter del d.lgs. n. 74 del 2000, nella parte in cui, con riferimento ai fatti commessi sino al 17 settembre 2011, puniva l'omesso versamento dell'imposta sul valore aggiunto (IVA), dovuta in base alla relativa dichiarazione annuale, per importi non superiori, per ciascun periodo di imposta, ad euro 103.291,38;
 che la Corte ha ritenuto, in specie, lesiva del principio di eguaglianza la previsione, per il delitto di omesso versamento dell'IVA, di una soglia di punibilità (euro 50.000) inferiore a quelle stabilite per la dichiarazione infedele e l'omessa dichiarazione dagli artt. 4 e 5 del medesimo decreto legislativo (rispettivamente, euro 103.291,38 ed euro 77.468,53), prima della loro modifica in diminuzione ad opera del decreto-legge 13 agosto 2011, n. 138 (Ulteriori misure urgenti per la stabilizzazione finanziaria e per lo sviluppo), convertito, con modificazioni, in legge 14 settembre 2011, n. 148: modifica operante, per espressa previsione normativa, in rapporto ai soli fatti commessi dopo il 17 settembre 2011;
 che in questo modo, infatti, veniva riservato un trattamento deteriore a comportamenti di evasione tributaria meno insidiosi e lesivi degli interessi del fisco, attenendo l'omesso versamento a somme di cui lo stesso contribuente si era riconosciuto debitore nella dichiarazione annuale dell'IVA;
 che, ad avviso del giudice a quo, la medesima conclusione si imporrebbe anche in rapporto al delitto di omesso versamento di ritenute certificate, trattandosi di figura criminosa del tutto omogenea, per «struttura e funzione», a quella delineata dall'art. 10-ter del d.lgs. n. 74 del 2000, modellata su di essa con la previsione della medesima pena, della stessa soglia di punibilità e di un momento consumativo «collegato ad un termine di adempimento ben determinato»;
 che, al pari della disposizione dichiarata costituzionalmente illegittima, anche quella sottoposta a scrutinio sanzionerebbe il mero inadempimento di un'obbligazione tributaria di pagamento di una somma di denaro, «sia pure "in nome e per conto" del contribuente sostituito», a fronte di una corretta autoliquidazione e di una veritiera certificazione dell'imposta dovuta;
 che, di conseguenza, l'omesso allineamento della soglia di punibilità del delitto in questione a quella prevista, all'epoca, dall'art. 4 del d.lgs. n. 74 del 2000, ossia ad euro 103.291,38, determinerebbe un trattamento sanzionatorio sperequato, tale da rendere censurabile, per la sua manifesta irragionevolezza, l'esercizio della discrezionalità spettante al legislatore in materia di configurazione delle fattispecie astratte di reato.
 Considerato che il Tribunale ordinario di Verbania dubita, in riferimento all'art. 3 della Costituzione, della legittimità costituzionale dell'art. 10-bis del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), nella parte in cui, con riferimento ai fatti commessi sino al 17 settembre 2011, punisce l'omesso versamento delle ritenute risultanti dalla certificazione rilasciata ai sostituiti per un ammontare non superiore ad euro 103.291,38; 
 che, successivamente all'ordinanza di rimessione, è intervenuto il decreto legislativo 24 settembre 2015, n. 158 (Revisione del sistema sanzionatorio, in attuazione dell'articolo 8, comma 1, della legge 11 marzo 2014, n. 23), il cui art. 7 ha modificato la norma censurata; 
 che la novella del 2015 ha previsto che le ritenute, il cui omesso versamento assume rilievo penale, possano risultare, oltre che dalla certificazione rilasciata ai sostituiti, anche dalla dichiarazione di sostituto d'imposta (donde il nuovo nomen iuris del reato, risultante dalla rubrica, di «Omesso versamento di ritenute dovute o certificate»), innalzando, al tempo stesso - per quanto qui più interessa - la soglia di punibilità dell'illecito dai precedenti 50.000 euro a 150.000 euro per ciascun periodo d'imposta: dunque, ad un importo più elevato di quello che il giudice a quo ha chiesto a questa Corte di introdurre, con riguardo ai fatti commessi sino al 17 settembre 2011; 
 che, conformemente a quanto è già avvenuto in rapporto ad analoghe questioni (ordinanze n. 168 e n. 141 del 2017, n. 230, n. 229, n. 89 e n. 14 del 2016, n. 256 del 2015), va quindi disposta la restituzione degli atti al giudice a quo per un nuovo esame della rilevanza e della non manifesta infondatezza della questione sollevata alla luce dello ius superveniens.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 ordina la restituzione degli atti al Tribunale ordinario di Verbania.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 gennaio 2018. &#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Franco MODUGNO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 9 febbraio 2018.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
