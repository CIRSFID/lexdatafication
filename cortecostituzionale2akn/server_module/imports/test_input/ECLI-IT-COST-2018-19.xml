<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/19/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/19/"/>
          <FRBRalias value="ECLI:IT:COST:2018:19" name="ECLI"/>
          <FRBRdate date="10/01/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="19"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/19/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/19/ita@/!main"/>
          <FRBRdate date="10/01/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/19/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/19/ita@.xml"/>
          <FRBRdate date="10/01/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="02/02/2018" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2018</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Giancarlo Coraggio</cc:relatore_pronuncia>
        <cc:data_decisione>10/01/2018</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 106 del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo), e degli artt. 395 e 396 del codice di procedura civile, promosso dal Consiglio di Stato, sezione quarta, nel procedimento vertente tra D. P. e altri e la Presidenza del Consiglio dei ministri e altro, con ordinanza del 17 novembre 2016, iscritta al n. 276 del registro ordinanze 2016 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 4, prima serie speciale, dell'anno 2017. 
 Visto l'atto di costituzione di D. P. e altri; 
 udito nella camera di consiglio del 10 gennaio 2018 il Giudice relatore Giancarlo Coraggio.
 Ritenuto che, con ordinanza del 17 novembre 2016, il Consiglio di Stato, sezione quarta, ha sollevato, in riferimento agli artt. 24, 111 e 117, primo comma, della Costituzione, questione di legittimità costituzionale dell'art. 106 del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo), e degli artt. 395 e 396 del codice di procedura civile, «nella parte in cui non prevedono un diverso caso di revocazione della sentenza quando ciò sia necessario, ai sensi dell'art. 46, par. 1, della Convenzione europea dei diritti dell'uomo e delle libertà fondamentali, per conformarsi ad una sentenza definitiva della Corte europea dei diritti dell'uomo»;
 che il rimettente, in punto di fatto, ha dedotto che:
 - con sentenza del 12 marzo 1996, n. 175, il Tribunale amministrativo regionale per la Campania, sezione staccata di Salerno, aveva accolto il ricorso proposto dai consiglieri di Stato S. G., F. M. e F. P., volto all'accertamento del loro diritto all'allineamento stipendiale, con conseguente condanna della Presidenza del Consiglio dei ministri al pagamento delle somme spettanti a tale titolo, oltre accessori di legge;
 - con sentenza 22 maggio 2006, n. 3017, il Consiglio di Stato, sezione quarta, aveva dichiarato inammissibile l'appello avverso la pronuncia di primo grado per inesistenza della notificazione;
 - i ricorrenti avevano quindi proposto ricorso in ottemperanza per l'esecuzione della sentenza passata in cosa giudicata e l'amministrazione, costituitasi in giudizio, aveva eccepito l'impossibilità di darvi esecuzione a causa del sopravvenuto art. 50, comma 4, della legge 23 dicembre 2000, n. 388, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2001)», che aveva disposto la perdita di efficacia delle decisioni delle autorità giurisdizionali comportanti provvedimenti di allineamento stipendiale;
 - il TAR Campania, con sentenza 28 gennaio 2008, n. 93, aveva accolto il ricorso in ottemperanza, ma la pronuncia era stata riformata dal Consiglio di Stato, sezione quarta, con sentenza 16 giugno 2008, n. 2986, sul presupposto che l'art. 50, comma 4, della legge n. 388 del 2000 era intervenuto prima che si fosse formato il giudicato sulla pretesa dei ricorrenti, il che avrebbe impedito la valida instaurazione del giudizio esecutivo;
 - con il ricorso per revocazione la parte ricorrente (eredi di F. P.) ha dedotto di essersi rivolta alla Corte europea dei diritti dell'uomo, lamentando che, in forza di non chiare e illegittime disposizioni nazionali aventi efficacia retroattiva e incidenti su sentenze rese da autorità giurisdizionali, la pronuncia n. 2986 del 2008 del Consiglio di Stato le aveva precluso il conseguimento di un diritto quesito, in violazione dell'art. 6, paragrafo I, della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con legge 4 agosto 1955, n. 848; e che la Corte EDU, con la sentenza 1° luglio 2014, Guadagno e altri c. Italia, aveva riconosciuto la fondatezza delle sue doglianze e liquidato un indennizzo;
 - i ricorrenti hanno sostenuto la revocabilità della sentenza gravata sulla base dell'art. 106 del d.lgs. n. 104 del 2010 (d'ora in avanti: cod. proc. amm.) e, in via subordinata, hanno chiesto sollevarsi questione di legittimità costituzionale della disposizione citata, nella parte in cui non contempla, tra le ipotesi di revocazione, quella del contrasto della sentenza nazionale con una successiva pronuncia della Corte EDU;
 - l'amministrazione ha eccepito l'impossibilità di una interpretazione costituzionalmente orientata delle norme sulla revocazione, perché il loro contrasto con la Convenzione potrebbe essere rimosso solo con la declaratoria di incostituzionalità; e che, tuttavia, non potrebbe trovare miglior sorte la richiesta di sollevare questione di costituzionalità delle stesse norme, poiché dalla sentenza della Corte EDU non potrebbe trarsi il convincimento che la parte ricorrente abbia conseguito il diritto ad ottenere l'adeguamento stipendiale richiesto; 
 - sempre secondo l'amministrazione, la richiesta di sollevare la questione di legittimità costituzionale sarebbe da respingere anche perché la Corte costituzionale non potrebbe pronunciare una sentenza additiva, invadendo la sfera del legislatore; e, infine, perché il riconoscimento delle pretese della parte ricorrente dovrebbe passare in ogni caso dalla declaratoria di incostituzionalità dell'art. 50, comma 4, della legge n. 388 del 2000, disposizione, questa, già ritenuta, dalla Corte costituzionale, immune da vizi;
 che il rimettente, in punto di rilevanza, ha osservato che:
 - con l'ordinanza 4 marzo 2015, n. 2, «da intendersi integralmente richiamata e trascritta in questa sede», l'adunanza plenaria del Consiglio di Stato ha già sollevato questione di legittimità costituzionale dell'art. 106 cod. proc. amm. e degli artt. 395 e 396 cod. proc. civ., in relazione agli artt. 24, 111 e 117, primo comma, Cost., nella parte in cui non prevedono un diverso caso di revocazione della sentenza, quando ciò sia necessario per conformarsi ad una sentenza definitiva della Corte EDU;
 - in tale ordinanza, «pienamente condivisa dal Collegio», si sarebbe precisato che la questione era rilevante in quel giudizio, in quanto dalla sua soluzione dipendeva l'ammissibilità del ricorso per revocazione proposto, e che la situazione sarebbe «perfettamente traslabile» alla fattispecie esaminata dal rimettente, poiché l'unico motivo di revocazione spiegato dalla parte ricorrente riposa sull'asserito contrasto della sentenza passata in giudicato con la sopravvenuta pronuncia della Corte EDU;
 - «per le ragioni già chiarite nella ordinanza n. 2/2015 dell'Adunanza Plenaria del Consiglio di Stato» non potrebbe essere condivisa la prospettazione della parte ricorrente, secondo cui sarebbe possibile una interpretazione estensiva dei casi di revocazione previsti dal diritto positivo;
 che il Consiglio di Stato, infine, ha affermato di condividere «il giudizio di non manifesta infondatezza delle questioni prospettate nella citata ordinanza n. 2/2015 in quanto, non contemplando tra i casi di revocazione quella che si renda necessaria per conformarsi ad una sentenza definitiva della Corte europea dei diritti dell'uomo, le norme processuali surrichiamate appaiono in contrasto con l'art. 46 CEDU che, invece, sancisce tale obbligo per gli Stati aderenti»; 
 che, con memoria depositata nella cancelleria di questa Corte il 14 febbraio 2017, si è costituita la parte ricorrente nel giudizio a quo, chiedendo l'accoglimento della questione sollevata e riservandosi di meglio illustrare in prosieguo la sua posizione;
 che, con memoria depositata il 20 dicembre 2017, la parte costituita ha fatto istanza perché la causa venga trattata in pubblica udienza, poiché il caso in esame sarebbe differente da quello già esaminato dalla Corte costituzionale con la sentenza n. 123 del 2017, non essendovi controinteressati, né essendo configurabile una violazione dei diritti dei terzi in caso di revocazione della sentenza nazionale contraria alla sentenza della Corte EDU.
 Considerato che la questione può essere decisa in camera di consiglio, sussistendo le condizioni per una pronuncia di manifesta inammissibilità;
 che, infatti, in ordine alla non manifesta infondatezza, il rimettente si limita ad affermare che dall'art. 46, paragrafo 1, della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con legge 4 agosto 1955, n. 848, discende un obbligo di riapertura del processo interno, e a richiamare «le considerazioni in diritto illustrate» nella ordinanza di rimessione 4 marzo 2015, n. 2, dell'adunanza plenaria del Consiglio di Stato, considerazioni «da ritenersi integralmente ritrascritte nella presente ordinanza collegiale»;
 che la motivazione sul punto è carente, perché si risolve unicamente nel richiamo per relationem ad altra ordinanza del Consiglio di Stato;
 che la consolidata giurisprudenza di questa Corte «esclude che, nei giudizi incidentali di costituzionalità delle leggi, sia ammessa la cosiddetta motivazione per relationem. Infatti, il principio di autonomia di ciascun giudizio di costituzionalità in via incidentale, quanto ai requisiti necessari per la sua valida instaurazione, e il conseguente carattere autosufficiente della relativa ordinanza di rimessione, impongono al giudice a quo di rendere espliciti, facendoli propri, i motivi della non manifesta infondatezza, non potendo limitarsi ad un mero richiamo di quelli evidenziati dalle parti nel corso del processo principale (ex plurimis, sentenze n. 49, n. 22 e n. 10 del 2015; ordinanza n. 33 del 2014), ovvero anche in altre ordinanze di rimessione emanate nello stesso o in altri giudizi (sentenza n. 103 del 2007; ordinanze n. 156 del 2012 e n. 33 del 2006)» (sentenza n. 170 del 2015).
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, comma 2, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 106 del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo), e degli artt. 395 e 396 del codice di procedura civile, sollevata, in riferimento agli artt. 24, 111 e 117, primo comma, della Costituzione, dal Consiglio di Stato, sezione quarta, con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 gennaio 2018.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Giancarlo CORAGGIO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 2 febbraio 2018.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
