<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/1987/420/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/1987/420/"/>
          <FRBRalias value="ECLI:IT:COST:1987:420" name="ECLI"/>
          <FRBRdate date="11/11/1987" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="420"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/1987/420/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/1987/420/ita@/!main"/>
          <FRBRdate date="11/11/1987" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/1987/420/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/1987/420/ita@.xml"/>
          <FRBRdate date="11/11/1987" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/11/1987" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>1987</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>SAJA</cc:presidente>
        <cc:relatore_pronuncia>Gabriele Pescatore</cc:relatore_pronuncia>
        <cc:data_decisione>11/11/1987</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: 
 Presidente: dott. Francesco SAJA; 
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo 
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. 
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. 
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo 
 CAIANIELLO;</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei  giudizi  di legittimità costituzionale dell'art. 28 della legge    
 24 novembre 1981, n. 689 ("Modifiche al  sistema  penale"),  promossi    
 con le seguenti ordinanze:                                               
      1)  ordinanza  emessa  il  24 marzo 1983 dal Pretore di Todi nel    
 procedimento civile vertente tra Zoppetti Maria e  la  Prefettura  di    
 Perugia,  iscritta al n. 416 del registro ordinanze 1983 e pubblicata    
 nella Gazzetta Ufficiale della Repubblica n. 274 dell'anno 1983;         
      2)  ordinanza  emessa il 18 febbraio 1986 dal Pretore di Saluzzo    
 nei procedimenti civili riuniti vertenti tra Palmero Oreste ed  altri    
 e  il  Presidente  della  Regione  Piemonte,  iscritta  al n. 752 del    
 registro ordinanze 1986 e pubblicata nella Gazzetta  Ufficiale  della    
 Repubblica n. 60, prima serie speciale dell'anno 1986;                   
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    
 ministri;                                                                
    Udito  nella  camera  di  consiglio del 28 ottobre 1987 il Giudice    
 relatore Gabriele Pescatore;                                             
    Ritenuto  che  con le ordinanze in epigrafe è stata sollevata, in    
 riferimento all'art. 3 della Costituzione, questione di  legittimità    
 costituzionale  dell'art.  28  della  legge 24 novembre 1981, n. 689,    
 nella parte in cui contempla per l'illecito amministrativo un termine    
 di prescrizione superiore a quello stabilito dall'art. 157, n. 6 cod.    
 pen. per le contravvenzioni  punite  con  la  sola  ammenda,  ovvero,    
 diversamente da quanto stabilito dagli artt. 157, 161, 172 e 173 cod.    
 pen., la prescrizione della sola sanzione e non anche dell'illecito;     
    Considerato  che  il  raffronto  fra la disciplina penale e quella    
 amministrativa  è  di  per  sé  di  scarso  significato,   ove   si    
 considerino   le  importanti  diversità  che  esistono  tra  le  due    
 categorie di illecito, già sul piano costituzionale (l'intero  testo    
 dell'art.   27   della   Costituzione   si   applica   soltanto  alla    
 responsabilità penale; il  principio  della  irretroattività  della    
 legge  è  costituzionalizzato solo con riguardo alla materia penale:    
 v. Corte cost. n. 68/1984);                                              
      che  importanti diversità si ravvisano altresì sul piano della    
 normativa ordinaria, ove si consideri che il  legislatore  del  1981,    
 tenuto  conto  della  peculiarità  della  materia,  ha  disciplinato    
 l'illecito amministrativo anche con ricorso ad  istituti  di  diritto    
 civile  (non  solo  nei  casi  cui  ci  si riferisce nella specie, ma    
 ugualmente per la responsabilità solidale: v. art. 6);                  
      che non potendosi ravvisare quella omogeneità di situazioni che    
 il giudice a quo pone a fondamento  della  questione,  la  stessa  va    
 ritenuta manifestamente infondata;                                       
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    
 9, secondo comma, delle Norme integrative per i  giudizi  innanzi  la    
 Corte costituzionale;</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi                              &#13;
                         LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p/>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso </block>
    </conclusions>
  </judgment>
</akomaNtoso>
