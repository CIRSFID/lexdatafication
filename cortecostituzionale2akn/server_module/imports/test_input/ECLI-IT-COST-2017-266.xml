<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/266/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/266/"/>
          <FRBRalias value="ECLI:IT:COST:2017:266" name="ECLI"/>
          <FRBRdate date="22/11/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="266"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/266/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/266/ita@/!main"/>
          <FRBRdate date="22/11/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/266/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/266/ita@.xml"/>
          <FRBRdate date="22/11/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/12/2017" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2017</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Nicolò Zanon</cc:relatore_pronuncia>
        <cc:data_decisione>22/11/2017</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 102, comma 2, del codice di procedura penale, promosso dal Tribunale ordinario di Lecce, nel procedimento a carico di R. S., con ordinanza del 22 luglio 2016, iscritta al n. 56 del registro ordinanze 2017 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2017.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
 udito nella camera di consiglio del 22 novembre 2017 il Giudice relatore Nicolò Zanon.
 Ritenuto che, con ordinanza del 22 luglio 2016 (r.o. n. 56 del 2017), il Tribunale ordinario di Lecce ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 102, comma 2, del codice di procedura penale;
 che, secondo il rimettente, tale disposizione, nel riconoscere al difensore sostituto i diritti e i doveri del difensore sostituito, determinerebbe, «nella sua comune e dominante interpretazione giurisprudenziale», un irragionevole trattamento differenziato tra difensori sostituti;
 che, infatti, essa attribuirebbe al difensore sostituto nominato ai sensi dell'art. 97, comma 4, cod. proc. pen. il diritto alla liquidazione erariale - previsto espressamente per il difensore d'ufficio dagli artt. 116 e 117 del decreto del Presidente della Repubblica 30 maggio 2002, n. 115, recante «Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia (Testo A)» - mentre non riconoscerebbe questo stesso diritto al difensore sostituto nominato per delega (art. 102, comma 1, cod. proc. pen.);
 che il giudice rimettente riferisce di essere chiamato a decidere su un'istanza di liquidazione dei compensi ai sensi dell'art. 117 del d.P.R. n. 115 del 2002 presentata dall'avvocato nominato sostituto d'ufficio del difensore di fiducia dell'imputato;
 che egli ritiene pregiudiziale la questione sollevata nell'ambito del pendente procedimento di liquidazione, «in quanto solo in virtù della summenzionata interpretazione [...] il difensore istante [...] andrebbe ritenuto legittimato ad avanzare la propria richiesta di liquidazione [...], dovendo in caso contrario la predetta richiesta dichiararsi inammissibile»; 
 che il rimettente afferma che gli artt. 116 e 117 del d.P.R. n. 115 del 2002 attribuirebbero al difensore d'ufficio la facoltà di accedere alla liquidazione da parte dello Stato in determinati casi, mentre la giurisprudenza di legittimità consolidata (viene richiamata la sentenza della Corte di cassazione, sezione quarta penale, 10 aprile 2008, n. 17721) estenderebbe tale diritto al difensore sostituto d'ufficio, anche alla luce delle ordinanze n. 201 del 2015, n. 176 del 2006 e n. 8 del 2005 della Corte costituzionale; 
 che simile conclusione si fonderebbe sul richiamo operato dall'art. 97, comma 4, cod. proc. pen. all'art. 102, comma 2, cod. proc. pen., che, come ricordato, stabilisce che il sostituto esercita i diritti e assume i doveri del difensore;
 che il giudice rimettente ritiene che la disposizione censurata si applichi a due categorie di difensori sostituti, sia quello nominato d'ufficio ai sensi dell'art. 97, comma 4, cod. proc. pen., sia quello delegato dal difensore ai sensi dell'art. 102, comma 1, cod. proc. pen., mentre, secondo l'orientamento giurisprudenziale richiamato, la stessa previsione riconoscerebbe la liquidazione erariale solo al primo e non anche al secondo;
 che tale interpretazione sarebbe «alquanto contraddittoria, incoerente ed irragionevole», poiché in tal modo la disposizione censurata - pur riferendosi a due categorie di difensori sostituti - «opererebbe in concreto» solo per una di esse, escludendo quella dei difensori sostituti per delega, nei cui confronti l'art. 102 cod. proc. pen. «nella sua interezza» sarebbe invece «destinato ad operare ab origine ed in via primaria»;
 che proprio in ciò risiederebbe la manifesta irragionevolezza dell'interpretazione dominante, «contraria ai più comuni principi di logica e ragionevolezza, in quanto pretende di selezionare - violando la stessa lettera della legge [...] - i soggetti nei cui confronti applicare una determinata previsione [...], escludendone altri ivi espressamente indicati», con violazione dell'art. 3 Cost., ossia «del principio di non contraddizione di matrice aristotelica»;
 che il giudice a quo, successivamente, individua quella che definisce l'«unica possibile interpretazione dell'art. 102 co. 2 c.p.p. conforme a Costituzione, ed ancor prima a logica e ragionevolezza»;
 che, a suo avviso, la disposizione censurata andrebbe interpretata come applicabile a tutti i difensori sostituti (artt. 97, comma 4, e 102, comma 1, cod. proc. pen.), con specifico riferimento «a diritti e doveri di natura esclusivamente processuale e deontologica (e non patrimoniale o economica)»;
 che, per questi motivi, chiede alla Corte costituzionale di «prendere atto della manifesta irragionevolezza» dell'interpretazione estensiva dell'art. 102, comma 2, cod. proc. pen. e di dichiararne l'illegittimità costituzionale, «nella sua comune e dominante interpretazione giurisprudenziale (diritto vivente) secondo cui tra i diritti e doveri ivi menzionati sarebbero ricompresi anche diritti e doveri di natura patrimoniale ed economica, fra cui il diritto del difensore sostituto alla liquidazione erariale del proprio compenso»;
 che, con atto depositato il 16 maggio 2017, è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione di legittimità costituzionale venga dichiarata inammissibile e in subordine infondata;
 che l'Avvocatura generale dello Stato ricorda le precedenti decisioni con le quali la Corte costituzionale si è pronunciata su analoghe questioni sollevate dal medesimo giudice rimettente, dichiarandole manifestamente infondate o manifestamente inammissibili (ordinanze n. 201 del 2015, n. 191 del 2013 e n. 185 del 2012);
 che, quanto al merito, afferma che, con l'ordinanza n. 176 del 2006, la Corte costituzionale avrebbe già chiarito la portata applicativa della norma censurata, nel senso che al difensore sostituto d'ufficio si applicherebbero le previsioni di cui all'art. 102 cod. proc. pen., compresa la disciplina sul patrocinio a spese dello Stato;
 che la mancata previsione della facoltà di accedere alla liquidazione erariale per il sostituto nominato dal difensore e non per il sostituto nominato d'ufficio si giustificherebbe considerando il diverso rapporto sottostante alla stessa prestazione, per delega nel primo caso, ex officio nel secondo, e che la distinta fonte della prestazione professionale legittimerebbe tale trattamento differenziato.
 Considerato che il Tribunale ordinario di Lecce ha sollevato, in relazione all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 102, comma 2, del codice di procedura penale;
 che tale disposizione prevede che il difensore «sostituto esercita i diritti e assume i doveri del difensore» titolare e riguarda entrambe le categorie di difensori sostituti, quello nominato per delega e quello nominato d'ufficio;
 che - secondo la prospettiva che il giudice rimettente persegue, in contrasto con il diritto vivente - essa dovrebbe interpretarsi come riferita ai «diritti e doveri di natura esclusivamente processuale e deontologica (e non patrimoniale o economica)», con il risultato di impedire al difensore sostituto d'ufficio l'accesso alla liquidazione erariale dell'onorario e delle spese, alle condizioni definite dagli artt. 116 e 117 del decreto del Presidente della Repubblica 30 maggio 2002, n. 115, recante «Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia (Testo A)»;
 che, attraverso tale lettura, il difensore sostituto d'ufficio sarebbe equiparato al difensore sostituto per delega, nei cui confronti la liquidazione erariale è preclusa in virtù del mandato ricevuto dal difensore titolare;
 che, secondo il rimettente, solo tale interpretazione - che questa Corte dovrebbe accogliere, dichiarando costituzionalmente illegittima la disposizione censurata - consentirebbe di non trattare in modo irragionevolmente differenziato le due categorie di sostituti, entrambe disciplinate dall'art. 102, comma 2, cod. proc. pen.;
 che la questione è manifestamente inammissibile;
 che, secondo la costante giurisprudenza di questa Corte, in presenza di un orientamento giurisprudenziale consolidato, ma non condiviso dal giudice rimettente perché ritenuto costituzionalmente illegittimo, questi ha facoltà di scegliere tra l'adozione di una diversa interpretazione e la proposizione della questione di legittimità costituzionale sulla disposizione, interpretandola alla luce di quel medesimo orientamento, assunto in termini di diritto vivente (da ultimo, sentenza n. 122 del 2017);
 che invece, nel caso in esame, il giudice rimettente da un lato censura l'art. 102, comma 2, cod. proc. pen. e sottopone a diffusa e insistita critica la dominante interpretazione di quest'ultimo, ma, dall'altro, individua contestualmente l'«unica possibile interpretazione [...] conforme a Costituzione» del medesimo articolo, rendendone estesamente conto nella propria ordinanza di rimessione e chiedendo a questa Corte di prenderne atto;
 che, in tal modo, il rimettente utilizza il giudizio incidentale di legittimità costituzionale all'improprio scopo di ottenere un avallo dell'interpretazione della disposizione censurata, che egli prospetta come asseritamente conforme a Costituzione (e che, invero, contrasta con quella già indicata da questa Corte, secondo cui al difensore sostituto d'ufficio spetta la liquidazione erariale per l'attività svolta in udienza, in conseguenza della sua equiparazione al difensore d'ufficio: ordinanze n. 201 del 2015, n. 191 del 2013, n. 176 del 2006 e n. 8 del 2005);
 che da tale impropria richiesta deriva la manifesta inammissibilità della questione sollevata (da ultimo, ordinanza n. 87 del 2016).
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 102, comma 2, del codice di procedura penale, sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale ordinario di Lecce, con l'ordinanza in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 novembre 2017.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Nicolò ZANON, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 13 dicembre 2017.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
