<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/180/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/180/"/>
          <FRBRalias value="ECLI:IT:COST:2007:180" name="ECLI"/>
          <FRBRdate date="23/05/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="180"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/180/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/180/ita@/!main"/>
          <FRBRdate date="23/05/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/180/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/180/ita@.xml"/>
          <FRBRdate date="23/05/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/06/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Franco Gallo</cc:relatore_pronuncia>
        <cc:data_decisione>23/05/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 62 della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2003), promossi con un'ordinanza depositata il 18 gennaio e due ordinanze depositate il 1° febbraio 2006 dalla Commissione tributaria provinciale di Benevento, rispettivamente iscritte ai nn. 593, 594 e 595 del registro ordinanze 2006 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 1, prima serie speciale, dell'anno 2007. 
    Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 9 maggio 2007 il Giudice relatore Franco Gallo. 
    Ritenuto che, nel corso di due giudizi riuniti aventi ad oggetto l'impugnazione di atti di recupero di crediti di imposta emessi dall'Agenzia delle entrate, ufficio di Benevento, la Commissione tributaria provinciale di Benevento, con ordinanza depositata il 18 gennaio 2006 (R.O. n. 593 del 2006), ha sollevato, in riferimento agli artt. 97 e 24 della Costituzione ed «in relazione anche all'art. 3 della legge 27 luglio 2000, n. 212» (Disposizioni in materia di statuto dei diritti del contribuente), questione di legittimità costituzionale dell'art. 62 (rectius: 62, comma 1, lettera a), della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2003); 
    che la disposizione denunciata impone ai soggetti che hanno conseguito in via automatica, anteriormente alla data dell'8 luglio 2002, il diritto al contributo di cui all'art. 8 della legge 23 dicembre 2000, n. 388 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2001), l'obbligo di inviare entro il 28 febbraio 2003, a pena di «decadenza» dal contributo medesimo, utilizzando un modello da predisporsi con provvedimento del Direttore dell'Agenzia delle entrate, i dati occorrenti per la ricognizione degli investimenti realizzati; 
    che, quanto alla non manifesta infondatezza, il giudice a quo osserva che la norma censurata, in violazione del divieto di retroattività delle leggi tributarie sancito dall'art. 3, comma 1, della citata legge n. 212 del 2000, ha efficacia retroattiva e, pertanto, si pone in contrasto con i princípi di correttezza e buona fede, nonché, conseguentemente, con quelli di buon andamento e di imparzialità della pubblica amministrazione garantiti dall'art. 97 Cost.; 
    che il rimettente osserva, altresì, che la disposizione censurata pone a carico dell'interessato un adempimento (cioè l'invio dei dati suddetti) da effettuarsi in un termine inferiore a sessanta giorni dalla sua entrata in vigore e, quindi, è in contrasto con l'art. 3, comma 2, della legge n. 212 del 2000, il quale stabilisce che le disposizioni tributarie «non possono prevedere adempimenti a carico dei contribuenti la cui scadenza sia fissata anteriormente al sessantesimo giorno dalla data della loro entrata in vigore o dell'adozione dei provvedimenti di attuazione in esse espressamente previsti»; 
    che, per il giudice a quo, tale contrasto comporta la violazione dell'art. 24 Cost., perché il termine di sessanta giorni previsto dall'art. 3, comma 2, della legge n. 212 del 2000 costituisce un «termine minimo indispensabile perché il contribuente possa esplicare gli adempimenti a suo carico a tutela dei suoi diritti e, quindi, del diritto di difesa»; 
    che, quanto alla rilevanza, la Commissione tributaria afferma sia che deve fare applicazione della disposizione censurata, non essendo fondate le altre ragioni di impugnazione prospettate in via logicamente preliminare dalla parte ricorrente, sia che dall'eventuale accoglimento della sollevata questione discenderebbe il venir meno della pretesa tributaria dell'Ufficio; 
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata inammissibile o, in subordine, infondata; 
    che, ad avviso della difesa erariale, la questione è inammissibile, perché il giudice a quo si è limitato a richiamare gli argomenti del ricorrente, senza esporre in modo chiaro i motivi che hanno giustificato l'adozione degli atti impugnati e senza illustrare il collegamento eventualmente esistente tra le determinazioni assunte dall'Agenzia delle entrate e la disposizione censurata; 
    che, nel merito, l'Avvocatura generale dello Stato rileva che la disposizione censurata è già stata sottoposta all'esame della Corte costituzionale, la quale ha affermato che «la previsione della “decadenza” dal contributo appare adeguata e coerente con la ratio della norma censurata e non eccede i limiti dell'ampia discrezionalità riservata al legislatore in materia di agevolazioni» e ha, comunque, escluso l'asserita violazione del principio di irretroattività della legge tributaria, perché «la norma censurata non dispone per il passato, ma fissa per il futuro un obbligo di comunicazione di dati a pena di decadenza dal contributo, a nulla rilevando che tale decadenza abbia ad oggetto un contributo già conseguito» (ordinanza n. 124 del 2006); 
    che, per la stessa Avvocatura, deve conseguentemente escludersi la violazione dell'art. 97 Cost., perché tale censura è stata formulata sull'erroneo presupposto della natura retroattiva della disposizione denunciata; 
    che, quanto alla dedotta violazione dell'art. 24 Cost., la difesa erariale rileva che la norma censurata non incide in alcun modo sulla tutela giurisdizionale del contribuente; 
    che, nel corso di altri due distinti giudizi – aventi ad oggetto l'impugnazione di atti con cui l'Agenzia delle entrate, ufficio di Benevento, in ragione del mancato tempestivo invio da parte degli interessati del “modello CVS” contenente i dati occorrenti per la ricognizione degli investimenti realizzati, aveva disposto il recupero dei crediti di imposta fruiti dai contribuenti ai sensi dell'art. 8 della legge n. 388 del 2000 –, la Commissione tributaria provinciale di Benevento, con due ordinanze depositate entrambe il 1° febbraio 2006 (R.O. n. 594 e n. 595 del 2006), ha sollevato, sulla base delle medesime argomentazioni di cui all'ordinanza di rimessione sopra menzionata, identiche questioni di legittimità costituzionale;  
    che è intervenuto in entrambi i giudizi il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale, richiamando le ragioni già esposte nel precedente giudizio, ha chiesto che la questione sia dichiarata «inammissibile, perché manifestamente infondata», o sia comunque rigettata per infondatezza.  
    Considerato che la Commissione tributaria provinciale di Benevento, con tre ordinanze di analogo contenuto, dubita, in riferimento agli artt. 24 e 97 della Costituzione, «in relazione anche all'art. 3 della legge 27 luglio 2000, n. 212» (Disposizioni in materia di statuto dei diritti del contribuente), della legittimità costituzionale dell'art. 62, comma 1, lettera a), della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2003), nella parte in cui determina, in una data non successiva al 28 febbraio 2003, il termine – da fissarsi dall'Agenzia delle entrate nei 30 giorni dall'entrata in vigore della legge (cioè non oltre il 30 gennaio 2003) – entro il quale le imprese, che hanno conseguito automaticamente, prima dell'8 luglio 2002, contributi nella forma di crediti di imposta per gli investimenti di cui all'art. 8 della legge 23 dicembre 2000, n. 388 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2001), devono inviare i dati occorrenti per la ricognizione degli investimenti realizzati, a pena di «decadenza» dai contributi stessi; 
    che l'Agenzia delle entrate, con provvedimento emesso in data 24 gennaio 2003, pubblicato nella Gazzetta Ufficiale della Repubblica del 4 febbraio 2003, ha fissato tale termine al 28 febbraio 2003; 
    che, ad avviso del giudice rimettente, la disposizione censurata, in quanto avente efficacia retroattiva, si pone in contrasto con l'art. 3, comma 1, della citata legge n. 212 del 2000, il quale fa divieto al legislatore di adottare in materia tributaria leggi retroattive, e víola quindi l'art. 97 Cost., perché, avendo il legislatore del citato statuto dei diritti del contribuente ritenuto la retroattività della legge tributaria contraria ai princípi di correttezza e buona fede, risulta evidente che, quando tali princípi non sono rispettati, «non possono certo essere assicurati “il buon andamento e l'imparzialità dell'amministrazione”»; 
    che per il rimettente, inoltre, la medesima disposizione, avendo posto a carico dei contribuenti adempimenti da compiersi entro un termine inferiore a sessanta giorni dalla sua entrata in vigore (e comunque dall'entrata in vigore delle disposizioni di attuazione della stessa), si pone in contrasto con l'art. 3, comma 2, della legge n. 212 del 2000, il quale stabilisce invece che le disposizioni tributarie «non possono prevedere adempimenti a carico dei contribuenti la cui scadenza sia fissata anteriormente al sessantesimo giorno dalla data della loro entrata in vigore o dell'adozione dei provvedimenti di attuazione in esse espressamente previsti» e, quindi, víola l'art. 24 Cost., in quanto tale termine costituisce il «termine minimo indispensabile perché il contribuente possa esplicare gli adempimenti a suo carico a tutela dei suoi diritti e, quindi, del diritto di difesa»; 
    che, stante la identità delle questioni proposte con le tre ordinanze, i relativi giudizi possono essere riuniti e decisi con unica pronuncia; 
    che deve preliminarmente essere disattesa l'eccezione di inammissibilità della questione sollevata con l'ordinanza iscritta nel registro delle ordinanze al n. 593 del 2006, espressamente formulata dalla difesa erariale sulla base del rilievo che la carente descrizione della fattispecie non consentirebbe la valutazione della rilevanza della questione medesima; 
    che, al contrario di quanto sostenuto dalla difesa erariale, il giudice a quo ha indicato in modo sufficiente le ragioni per le quali ritiene di dover fare applicazione della norma censurata, affermando in particolare di non poter pervenire all'accoglimento dei ricorsi sulla base delle altre pregiudiziali ragioni fatte valere dalla parte ricorrente; 
    che, nel merito, le questioni sono manifestamente infondate; 
    che le questioni concernenti la violazione dell'art. 97 Cost. risultano proposte sulla base dell'erroneo presupposto interpretativo della natura retroattiva della norma censurata; 
    che tale natura è stata negata da questa Corte, con l'ordinanza n. 124 del 2006, perché la norma censurata «non dispone per il passato, ma fissa per il futuro un obbligo di comunicazione di dati a pena di “decadenza dal contributo”, a nulla rilevando che tale decadenza abbia ad oggetto un contributo già conseguito»; 
    che le questioni concernenti la violazione dell'art. 24 Cost. risultano proposte sul duplice presupposto che: a) il termine minimo di sessanta giorni, stabilito in via generale dall'art. 3, comma 2, della legge n. 212 del 2000 per l'effettuazione degli adempimenti del contribuente, abbia uno specifico fondamento costituzionale; b) detto termine attenga all'esercizio del diritto di difesa; 
    che entrambi tali presupposti sono erronei: il primo, perché l'art. 3 della legge n. 212 del 2000 non costituisce parametro idoneo a fondare il giudizio di legittimità costituzionale (ordinanza n. 216 del 2004); il secondo, perché la norma censurata fissa un termine di natura non processuale – e quindi non finalizzato all'esercizio del diritto di difesa – che, per sua natura, è estraneo all'àmbito di tutela dell'art. 24 Cost. (ex plurimis, ordinanze n. 940 e n. 21 del 1988, n. 324 del 1987); 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'art. 62, comma 1, lettera a), della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2003), sollevate dalla Commissione tributaria provinciale di Benevento, in riferimento agli artt. 24 e 97 della Costituzione, con le ordinanze indicate in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 maggio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Franco GALLO, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 7 giugno 2007.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
