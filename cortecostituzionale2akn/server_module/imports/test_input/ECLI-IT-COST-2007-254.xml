<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/254/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/254/"/>
          <FRBRalias value="ECLI:IT:COST:2007:254" name="ECLI"/>
          <FRBRdate date="20/06/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="254"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/254/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/254/ita@/!main"/>
          <FRBRdate date="20/06/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/254/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/254/ita@.xml"/>
          <FRBRdate date="20/06/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/07/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Maria Rita Saulle</cc:relatore_pronuncia>
        <cc:data_decisione>20/06/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 102 del decreto del Presidente della Repubblica 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia), promosso con ordinanza del 4 maggio 2006 dal Giudice per le indagini preliminari del Tribunale di Venezia sul ricorso proposto da B. L., iscritta al n. 605 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 2, prima serie speciale, dell'anno 2007. 
    Visti l'atto di costituzione di B. L., nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nell'udienza pubblica dell'8 maggio 2007 il Giudice relatore Maria Rita Saulle; 
    udito l'avvocato Luciano Faraon per B. L. e l'avvocato dello Stato Giovanni Lancia per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Il Giudice per le indagini preliminari del Tribunale di Venezia, con ordinanza del 4 maggio 2006, ha sollevato, in riferimento all'art. 24 della Costituzione, questione di legittimità costituzionale dell'art. 102 del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia), nella parte in cui non prevede la possibilità, per lo straniero ammesso al patrocinio a spese dello Stato, di nominare un interprete. 
    In punto di fatto, il rimettente rileva che B. L., dopo aver chiesto la liquidazione degli onorari per l'opera prestata quale traduttrice tra A. Y., imputata del reato di omicidio, e il suo difensore, vedeva rigettata la propria istanza in quanto, sebbene l'imputata fosse stata ammessa al patrocino a spese dello Stato, il difensore non aveva provveduto a nominarla quale sua consulente. 
    Il rimettente, investito del giudizio di impugnazione avverso il provvedimento di rigetto sopra indicato, quanto alla rilevanza e alla non manifesta infondatezza, osserva che il d.P.R. n. 115 del 2002 non contempla «la nomina di un interprete da parte dell'imputato o, comunque, un intervento privato di tale ausiliario, né tanto meno il pagamento del compenso allo stesso da parte dello Stato», limitandosi a prevedere la possibilità di nomina di un sostituto del difensore, di un investigatore e di un consulente tecnico di parte (artt. 101 e 102), precisando, ulteriormente, che il GIP liquida il compenso all'ausiliario del magistrato e non ad altri (art. 105). 
    A parere del giudice a quo, tale normativa, in quanto pone delle spese a carico dello Stato, ha carattere di eccezionalità e non è suscettibile di applicazione analogica, di talché, seppure l'istituto del patrocinio a spese dello Stato risulta ispirato ai principi di cui al primo e terzo comma dell'art. 24 della Costituzione, non sarebbe possibile liquidare alcun compenso all'interprete nominato dall'imputato, con conseguente violazione del diritto di difesa di quest'ultimo. 
    In particolare, il rimettente osserva che, una volta ammessa anche per gli stranieri la possibilità di accedere al patrocinio a spese dello Stato, deve essere loro conseguentemente consentita la possibilità di nominare un interprete al fine di soddisfare le loro necessità difensive consistenti sia nella traduzione di atti e documenti, sia nella possibilità di poter conferire con il proprio difensore. 
    2.- Si è costituita in giudizio B. L., parte ricorrente nel giudizio principale, la quale, dopo aver rilevato che il pieno esercizio del diritto di difesa deve essere assicurato anche agli stranieri mediante la conoscenza degli atti processuali che li riguardano, ha chiesto che la Corte dichiari fondata la sollevata questione di legittimità costituzionale. 
    In particolare, la difesa di B. L. osserva che, sulla base delle norme internazionali – art. 6 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con legge 4 agosto 1955, n. 848; e art. 14, par. 3, lettera f), del Patto delle Nazioni Unite sui diritti civili e politici, adottato a New York il 19 dicembre 1966, ratificato e reso esecutivo con legge 25 ottobre 1977, n. 881 – deve essere garantita allo straniero la presenza gratuita di un interprete di parte. Ciò risulta tanto più necessario nei casi, come quello di specie, in cui l'interprete nominato dall'autorità giudiziaria non sia idoneo allo svolgimento dell'incarico, in quanto l'errata traduzione degli atti processuali ha fatto sì che l'assistita della B. L., da testimone è divenuta imputata del reato di cui all'art. 575 cod. pen. 
    3.- È intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha chiesto di dichiarare la questione inammissibile o, in subordine, infondata. 
    La difesa erariale, in via preliminare, rileva che l'ordinanza di rimessione difetterebbe del requisito della rilevanza, non avendo il rimettente indicato se l'attività svolta dall'interprete nominata dal difensore dell'imputata abbia assunto i caratteri della necessità. 
    Nel merito, l'Avvocatura osserva che gli artt. 101 e 102 del d.P.R. n. 115 del 2002, nel prevedere la facoltà di nomina, per l'imputato del difensore, di un suo sostituto, di un investigatore privato e di un consulente tecnico, risultano conformi all'art. 24, terzo comma, della Costituzione.  
    La mancata indicazione dell'interprete nelle norme sopra indicate, infatti, sarebbe giustificata dalla circostanza che l'attività da questo svolta è diretta a rendere comprensibili al difensore e all'imputato le rispettive lingue e, pertanto, non è tipica dell'ufficio di difesa. In ragione di ciò tutte le volte in cui risulti necessaria la nomina di un interprete, il difensore dovrebbe liquidarne il compenso e farsi successivamente rimborsare il relativo importo ex art. 82 del d.P.R. n. 115 del 2002.Considerato in diritto1.- Il Giudice per le indagini preliminari del Tribunale di Venezia dubita, in riferimento all'art. 24 della Costituzione, della legittimità costituzionale dell'art. 102 del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia), nella parte in cui non prevede la possibilità, per lo straniero ammesso al patrocinio a spese dello Stato, di nominare un proprio interprete. 
    Il rimettente osserva che tale omessa previsione, non garantendo allo straniero, che non comprende la lingua italiana, il pieno esercizio del diritto di difesa, sarebbe causa del denunciato vulnus costituzionale. 
    Lamenta, infatti, il giudice a quo che se, da un lato, le norme sul patrocinio a spese dello Stato risultano applicabili anche agli stranieri, in attuazione dei principi di cui all'art. 24 della Costituzione, dall'altro, proprio nel rispetto dei suddetti principi, non può essere negata loro la possibilità di nominare un interprete di parte. 
    2.- La questione è fondata. 
    L'art. 102 del d.P.R. n. 155 del 2002 prevede al primo comma che «Chi è ammesso al patrocinio può nominare un consulente tecnico di parte residente nel distretto di corte di appello nel quale pende il processo», e non prevede, altresì, la possibilità per lo straniero, ammesso al gratuito patrocinio, che non conosca la lingua italiana, di ricorrere all'ausilio di un proprio interprete, la cui figura differisce sia da quella del consulente di parte sia da quella dell'interprete nominato dal giudice. 
    In proposito va rilevato che il codice di procedura penale prevede, agli artt. 143 e seguenti, la figura dell'interprete, attribuendo all'autorità procedente il relativo potere di nomina qualora le parti coinvolte nel processo non ne conoscano la lingua ufficiale (che, in base all'art. 109 cod. proc. pen., è l'italiano), o non la conoscano a sufficienza per affrontare adeguatamente la dinamica processuale. Tale nomina è motivata dalla necessità di garantire all'imputato che non capisce e/o non parla l'italiano il diritto di comprendere le accuse formulate contro di lui e intendere il procedimento al quale partecipa, in modo tale da renderne effettiva la partecipazione. Questa deve avvenire consapevolmente e porre l'imputato in condizione di comprendere il significato linguistico delle espressioni degli altri soggetti processuali, tra le quali quelle del proprio difensore, nonché di esprimersi, a sua volta, essendo da questi compreso. 
    La partecipazione personale e consapevole dell'imputato al procedimento, mediante il riconoscimento del diritto in capo all'accusato straniero, che non conosce la lingua italiana, di nominare un proprio interprete, rientra nella garanzia costituzionale del diritto di difesa nonché nel diritto al giusto processo, in quanto l'imputato deve poter comprendere, nella lingua da lui conosciuta, il significato degli atti e delle attività processuali, ai fini di un concreto ed effettivo esercizio del proprio diritto alla difesa (art. 24, comma secondo, della Costituzione). Inoltre, l'art. 111 della Costituzione stabilisce che la legge assicura che «la persona accusata di un reato sia assistita da un interprete se non comprende o non parla la lingua impiegata nel processo». 
    I principi costituzionali sopra riportati trovano riconoscimento in alcune norme internazionali che prevedono fra i diritti dell'accusato quello di «farsi assistere gratuitamente da un interprete se non comprende o non parla la lingua usata in udienza» (art. 6, n. 3, lettera e della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con legge 4 agosto 1955, n. 848; disposizione riproposta in modo analogo nell'art. 14, comma 3, lettera f, del Patto internazionale delle Nazioni Unite, sui diritti civili e politici del 19 dicembre 1966, adottato a New York il 19 dicembre 1966, ratificato e reso esecutivo con legge 25 ottobre 1977, n. 881). 
    In ragione di tali principi, questa Corte (sentenze n. 10 del 1993 e n. 341 del 1999), seppure con riferimento alla diversa posizione dell'interprete nominato dal giudice, ha ritenuto che l'art. 143 cod. proc. pen., laddove statuisce che l'imputato che non conosce la lingua italiana «ha diritto di farsi assistere gratuitamente da un interprete al fine di potere comprendere l'accusa contro di lui formulata e di seguire il compimento degli atti cui partecipa», configura «il ricorso all'interprete non già come un mero strumento tecnico a disposizione del giudice per consentire o facilitare lo svolgimento del processo in presenza di persone che non parlino o non comprendano l'italiano, ma come oggetto di un diritto individuale dell'imputato, diretto a consentirgli quella partecipazione cosciente al procedimento che, come si è detto, è parte ineliminabile del diritto di difesa». 
    Il riconoscimento in capo all'imputato straniero che non conosce la lingua italiana del diritto di nomina di un proprio interprete non può, in virtù dei principi sopra esposti, soffrire alcuna limitazione. Invero, l'istituto del patrocinio a spese dello Stato, essendo diretto a garantire anche ai non abbienti l'attuazione del precetto costituzionale di cui al terzo comma dell'art. 24 della Costituzione, prescrive che a questi siano assicurati i mezzi per agire e difendersi davanti ad ogni giurisdizione e ciò in esecuzione del principio posto dal primo comma della stessa disposizione, secondo cui tutti possono agire in giudizio per la tutela dei propri diritti e interessi legittimi. 
    Pertanto deve essere dichiarata l'illegittimità costituzionale dell'art. 102 del d.P.R. n. 115 del 2002, nella parte in cui non prevede, per lo straniero ammesso al patrocinio a spese dello Stato che non conosce la lingua italiana, la possibilità di nominare un proprio interprete. Resta fermo che il legislatore dovrà compiutamente disciplinare la materia inerente a questa figura di interprete.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara l'illegittimità costituzionale dell'art. 102 del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia), nella parte in cui non prevede la possibilità, per lo straniero ammesso al patrocinio a spese dello Stato che non conosce la lingua italiana, di nominare un proprio interprete. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 giugno 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 6 luglio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
