<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/264/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/264/"/>
          <FRBRalias value="ECLI:IT:COST:2004:264" name="ECLI"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="264"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/264/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/264/ita@/!main"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/264/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/264/ita@.xml"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="22/07/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Franco Bile</cc:relatore_pronuncia>
        <cc:data_decisione>08/07/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfonso QUARANTA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 37, comma 8, della legge 23 dicembre 1998, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2002); dell'art. 1, comma 260, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), e dell'art. 52, comma 2, della legge 9 marzo 1989, n. 88 (Ristrutturazione dell'INPS e dell'INAIL), promosso con ordinanza del 23 settembre 2003 dal Tribunale di Terni sul ricorso proposto da Nadia Tazza contro il Ministero dell'interno, iscritta al n. 1054 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 49, prima serie speciale, dell'anno 2003. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 9 giugno 2004 il Giudice relatore Franco Bile. 
     Ritenuto che, con ordinanza del 23 settembre 2003, il Tribunale di Terni - nel corso di un giudizio pendente tra un'invalida civile percettrice di indennità di accompagnamento ed il Ministero dell'interno - ha sollevato questione di legittimità costituzionale, in relazione agli artt. 3 e 38, primo comma, della Costituzione, dell'art. 37, comma 8, della legge 23 dicembre 1998, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2002), nella parte in cui non prevede l'irripetibilità delle somme indebitamente percepite prima della sospensione dell'erogazione dell'indennità, e degli artt. 1, comma 260, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), e 52, comma 2, della legge 9 marzo 1989, n. 88 (Ristrutturazione dell'INPS e dell'INAIL), nella parte in cui non prevedono l'irripetibilità delle somme indebitamente percepite a titolo d'indennità di accompagnamento negli stessi limiti degli indebiti previdenziali;  
    che, nella specie, alla ricorrente la prestazione assistenziale è stata revocata con decorrenza dalla visita medico-legale di verifica (8 febbraio 1994) a seguito della quale era emersa l'insussistenza dei presupposti sanitari del beneficio; 
    che l'amministrazione aveva sospeso l'erogazione dell'indennità dopo circa due anni dalla visita di verifica e poi, per recuperare le somme indebitamente erogate, aveva chiesto ed ottenuto decreto ingiuntivo, al quale l'interessata si era opposta; 
    che il Tribunale rimettente rileva come l'art. 37, comma 8, della legge 23 dicembre 1998, n. 448, abbia da ultimo disciplinato il regime dell'indebito per tali prestazioni assistenziali prevedendo che, in caso di accertata insussistenza dei requisiti sanitari, il Ministero del tesoro, del bilancio e della programmazione economica dispone l'immediata sospensione dell'erogazione del beneficio e provvede, entro i novanta giorni successivi, alla revoca delle provvidenze economiche a decorrere dalla data della visita di verifica;  
    che - osserva ancora il Tribunale - secondo il diritto vivente “risultano [...]  ripetibili tutte le somme corrisposte dopo la visita di revisione, a prescindere dalla immediata sospensione”, mentre la non immediata sospensione delle prestazioni, con conseguente formazione dell'indebito, dovrebbe comportare che la revoca operi da data successiva alla visita ed in particolare dalla data della sospensione dell'erogazione; 
    che invece la decorrenza dell'indebito dalla visita di verifica violerebbe i parametri evocati sia sotto il profilo della disparità di trattamento (art. 3 Cost.), secondo che la sospensione dell'erogazione del beneficio avvenga subito dopo la visita, ovvero dopo un più ampio periodo di tempo, sia sotto il profilo dell'inadeguata tutela dell'assistito (art. 38, primo comma, Cost.); 
    che comunque - rileva ancora il rimettente - vi sarebbe disparità di trattamento tra i beneficiari di prestazioni d'invalidità civile e gli altri soggetti che, in situazione analoga di indebita percezione della prestazione, fruiscono della più favorevole disciplina dell'indebito previdenziale, onde (anche sotto questo aspetto) un'insufficiente protezione del sistema di sicurezza sociale (art. 38, primo comma, Cost.);  
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ed ha concluso per l'infondatezza della questione di costituzionalità. 
    Considerato che il giudice rimettente solleva, in riferimento ai medesimi parametri (artt. 3 e 38, primo comma, della Costituzione), due distinte questioni incidentali di legittimità costituzionale nei confronti - rispettivamente - dell'art. 37, comma 8, della legge 23 dicembre 1998, n. 448, e degli artt. 1, comma 260, della legge 23 dicembre 1996, n. 662, e 52, comma 2, della legge 9 marzo 1989, n. 88, e che tali questioni concernono, sotto diversi profili, la disciplina della ripetizione dell'indennità di accompagnamento spettante agli invalidi civili, percepita indebitamente a causa dell'insussistenza dello stato di invalidità del beneficiario, successivamente verificata a seguito di visita medica di controllo; 
    che - quanto alla prima questione - la normativa  nella specie applicabile ratione temporis perché in vigore al momento della visita di verifica (art. 11, comma 4, della legge 24 dicembre 1993, n. 537, recante “Interventi correttivi di finanza pubblica”) è stata abrogata dall'art. 4, comma 3-nonies, del decreto-legge 20 giugno 1996, n. 323 (Disposizioni urgenti per la finanza pubblica), convertito in legge 8 agosto 1996, n. 425, e che, secondo la giurisprudenza di legittimità, per effetto di questa abrogazione espressa tale disciplina non potrebbe più trovare applicazione neppure nei casi in cui la visita di verifica sia intervenuta precedentemente;   
    che lo stesso art. 4 ha posto poi, al comma 3-ter, una nuova regolamentazione della ripetizione dell'indebito, prevedendo - in caso di accertata insussistenza del prescritto requisito sanitario - la revoca dell'indennità di accompagnamento entro novanta giorni dalla visita di verifica o degli ulteriori accertamenti che si fossero resi necessari; 
    che tale disciplina è stata a sua volta modificata dall'art. 37, comma 8, della legge 23 dicembre 1998, n. 448, che ha previsto un duplice provvedimento dopo la visita medica di controllo da cui sia risultata l'insussistenza del requisito sanitario e quindi la non spettanza del beneficio assistenziale: prima la sospensione immediata dell'erogazione dell'indennità e poi, entro i successivi novanta giorni, la revoca della stessa dalla data della visita; 
    che, a fronte di questo articolato quadro normativo, il giudice rimettente ha censurato solo l'art. 37, comma 8, della legge n. 448 del 1998, chiedendo una pronuncia additiva che ne modifichi il contenuto, senza motivare in alcun modo in ordine al suo ritenuto carattere retroattivo e all'ipotizzata sua applicabilità ad una fattispecie di indebito interamente maturata, come quella oggetto del giudizio a quo, ben prima della sua entrata in vigore; 
    che l'insufficiente motivazione sull'applicabilità della disposizione censurata nel giudizio a quo comporta la manifesta inammissibilità della questione stessa; 
    che infine - quanto all'ulteriore questione posta nei confronti dell'art. 1, comma 260, della legge n. 662 del 1996, e dell'art. 52, comma 2, della legge n. 88 del 1989 - il rimettente pone a confronto la disciplina dell'indebito assistenziale e quella dell'indebito previdenziale; 
    che a tal proposito questa Corte (ordinanza n. 448 del 2000) ha già ritenuto che non sussiste un'esigenza costituzionale che imponga per l'indebito previdenziale e per quello assistenziale un'identica disciplina, atteso che - pur operando in questa materia un principio di settore, onde la regolamentazione della ripetizione dell'indebito è tendenzialmente sottratta a quella generale del codice civile - rientra però nella discrezionalità del legislatore porre distinte discipline speciali adattandole alle caratteristiche dell'una o dell'altra prestazione; 
    che nessun argomento nuovo o diverso, rispetto a quelli già vagliati da questa Corte, è stato prospettato nell'ordinanza di rimessione, sicché la questione proposta è manifestamente infondata. 
    Visti  gli  artt. 26, secondo comma, della legge 11  marzo  1953, n. 87,  e 9, secondo comma, delle norme integrative per i  giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 37, comma 8, della legge 23 dicembre 1998, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2002), sollevata, in riferimento agli articoli 3 e 38, primo comma, della Costituzione, dal Tribunale di Terni con l'ordinanza indicata in epigrafe; &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 1, comma 260, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), e dell'art. 52, comma 2, della legge 9 marzo 1989, n. 88 (Ristrutturazione dell'INPS e dell'INAIL), sollevata, in riferimento agli articoli 3 e 38, primo comma, della Costituzione, dal Tribunale di Terni con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8  luglio 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Franco BILE, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 22 luglio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
