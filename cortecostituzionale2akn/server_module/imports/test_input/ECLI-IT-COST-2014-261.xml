<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/261/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/261/"/>
          <FRBRalias value="ECLI:IT:COST:2014:261" name="ECLI"/>
          <FRBRdate date="05/11/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="261"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/261/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/261/ita@/!main"/>
          <FRBRdate date="05/11/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/261/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/261/ita@.xml"/>
          <FRBRdate date="05/11/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="20/11/2014" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2014</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>NAPOLITANO</cc:presidente>
        <cc:relatore_pronuncia>Giuliano Amato</cc:relatore_pronuncia>
        <cc:data_decisione>05/11/2014</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo Maria NAPOLITANO; Giudici : Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale degli artt. 13, 14, 15, 16 e 135, comma 1, lettera q-quater), del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo), promossi dal Tribunale amministrativo regionale per la Calabria, sezione staccata di Reggio Calabria, con due ordinanze del 7 marzo 2014, rispettivamente iscritte ai nn. 106 e 107 del registro ordinanze 2014 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 27, prima serie speciale, dell'anno 2014.
 Udito nella camera di consiglio del 5 novembre 2014 il Giudice relatore Giuliano Amato.
 Ritenuto che il Tribunale amministrativo regionale per la Calabria, sezione staccata di Reggio Calabria, ha sollevato - in riferimento agli artt. 3, 24, 25, 77, 111 e 125 della Costituzione - questione di legittimità costituzionale dell'art. 135, comma 1, lettera q-quater), del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo), nella parte in cui prevede la devoluzione alla competenza inderogabile del Tribunale amministrativo regionale del Lazio, sede di Roma, delle controversie aventi ad oggetto i provvedimenti emessi dall'Autorità di polizia relativi al rilascio di autorizzazioni in materia di giochi pubblici con vincita in denaro;
 che il medesimo giudice ha inoltre sollevato - in riferimento all'art. 76 Cost. - questione di legittimità costituzionale degli artt. 13, 14, 15 e 16 dello stesso d.lgs. n. 104 del 2010, recanti la disciplina della competenza, territoriale e funzionale, dei Tribunali amministrativi regionali;
 che, in primo luogo, il giudice rimettente denuncia l'illegittimità dell'art. 135, comma 1, lettera q-quater), del d.lgs. n. 104 del 2010, il quale prevede la devoluzione alla competenza funzionale inderogabile del TAR Lazio, sede di Roma, delle controversie aventi ad oggetto i provvedimenti, emessi dall'autorità di polizia, relativi al rilascio di autorizzazioni in materia di giochi pubblici con vincita in denaro;
 che in particolare - con due ordinanze di analogo tenore - il rimettente, richiamando le motivazioni contenute in precedenti ordinanze di rimessione del medesimo giudice, ha ritenuto che l'art. 135, comma 1, lettera q-quater), violi in primo luogo l'art. 3 Cost. sotto il profilo della ragionevolezza, poiché - in mancanza di una valida e sufficiente ragione giustificatrice - introdurrebbe una deroga ai criteri ordinari di individuazione della competenza, legati agli indici di collegamento territoriale;
 che il giudice a quo ritiene inoltre che la disposizione in esame violi gli artt. 25 e 125 Cost., in quanto - vanificando l'articolazione su base regionale del sistema di giustizia amministrativa - determinerebbe l'alterazione dell'equilibrio del controllo giurisdizionale sugli atti amministrativi; sarebbero inoltre violati gli artt. 24 e 111 Cost., in quanto la devoluzione della competenza al TAR Lazio renderebbe più difficoltoso l'esercizio del diritto di difesa e confliggerebbe con il canone della ragionevole durata del processo;
 che, infine, la norma censurata costituirebbe violazione dell'art. 77 Cost., in quanto non sarebbe ravvisabile, nella materia dei giuochi pubblici, un caso straordinario di necessità e d'urgenza che giustifichi il ricorso allo strumento del decreto-legge;
 		che il medesimo giudice a quo ha, altresì, sollevato, in riferimento all'art. 76 Cost., un'ulteriore questione di legittimità costituzionale relativa alla complessiva disciplina della competenza prevista dagli artt. 13, 14, 15 e 16 del d.lgs. n. 104 del  2010, in quanto eccedente l'ambito della delega legislativa di cui all'art. 44 della legge 18 giugno 2009, n. 69 (Disposizioni per lo sviluppo economico, la semplificazione, la competitività nonché in materia di processo civile), limitata al riordino delle norme vigenti sulla giurisdizione del giudice amministrativo.
 Considerato che il Tribunale amministrativo regionale per la Calabria, sezione staccata di Reggio Calabria, ha sollevato - in riferimento agli artt. 3, 24, 25, 77, 111 e 125 della Costituzione - questione di legittimità costituzionale dell'art. 135, comma 1, lettera q-quater), del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo), nella parte in cui prevede la devoluzione alla competenza inderogabile del Tribunale amministrativo regionale del Lazio, sede di Roma, delle controversie aventi ad oggetto i provvedimenti emessi dall'Autorità di polizia relativi al rilascio di autorizzazioni in materia di giochi pubblici con vincita in denaro;
 che il medesimo giudice ha inoltre sollevato questione di legittimità costituzionale degli artt. 13, 14, 15 e 16 dello stesso d.lgs. n. 104 del 2010, per violazione dell'art. 76 Cost;
 che successivamente all'ordinanza di rimessione, questa Corte, con la sentenza n. 174 del 2014, ha dichiarato l'illegittimità costituzionale dell'art. 135, comma 1, lettera q-quater), del d.lgs.  n. 104 del 2010;
 che, dunque, la questione di legittimità costituzionale avente ad oggetto la citata disposizione deve essere dichiarata manifestamente inammissibile per sopravvenuta carenza di oggetto poiché, a seguito della sentenza citata, l'art. 135, comma 1, lettera q-quater), censurato dal giudice a quo è già stato rimosso dall'ordinamento con efficacia ex tunc (ex plurimis, ordinanze n. 206 del 2014, n. 321 e n. 177 del 2013, n. 315 e n. 182 del 2012);
 	che, d'altra parte, la questione di legittimità costituzionale degli artt. 13, 14, 15 e 16 del d.lgs. n. 104 del 2010, è identica - anche nella sua formulazione letterale - a quella sollevata dal medesimo giudice nelle tre ordinanze di rimessione iscritte ai nn. 208, 209 e 210 del registro ordinanze 2013, decise da questa Corte con la stessa sentenza n. 174 del 2014, che ne ha dichiarato l'inammissibilità per difetto di rilevanza;
 		che l'identità delle questioni e delle relative argomentazioni, già valutate da questa Corte nella richiamata pronuncia, impone la medesima decisione di inammissibilità.
 		Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 	1) dichiara manifestamente inammissibile la questione di legittimità costituzionale dell'art. 135, comma 1, lettera q-quater), del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recente delega al governo per il riordino del processo amministrativo), sollevata, in riferimento agli artt. 3, 24, 25, 77, 111 e 125 della Costituzione, dal Tribunale amministrativo regionale per Calabria, sezione staccata di Reggio Calabria, con le ordinanze indicate in epigrafe;&#13;
 2) dichiara manifestamente inammissibile la questione di legittimità costituzionale degli artt. 13, 14, 15 e 16 del decreto legislativo 2 luglio 2010, n. 104, sollevata, in riferimento all'art. 76 Cost., dal Tribunale amministrativo regionale per la Calabria, sezione staccata di Reggio Calabria, con le ordinanze indicate in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 novembre 2014.&#13;
 F.to:&#13;
 Paolo Maria NAPOLITANO, Presidente&#13;
 Giuliano AMATO, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 20 novembre 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
