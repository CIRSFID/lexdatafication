<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2013/78/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2013/78/"/>
          <FRBRalias value="ECLI:IT:COST:2013:78" name="ECLI"/>
          <FRBRdate date="22/04/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="78"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2013/78/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2013/78/ita@/!main"/>
          <FRBRdate date="22/04/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2013/78/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2013/78/ita@.xml"/>
          <FRBRdate date="22/04/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/04/2013" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2013</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>GALLO</cc:presidente>
        <cc:relatore_pronuncia>Sergio Mattarella</cc:relatore_pronuncia>
        <cc:data_decisione>22/04/2013</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Franco GALLO; Giudici : Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 1, comma 10, della legge 4 novembre 2005, n. 230 (Nuove disposizioni concernenti i professori e i ricercatori universitari e delega al Governo per il riordino del reclutamento dei professori universitari), promosso dal Tribunale amministrativo regionale per il Veneto, nel procedimento vertente tra M.R. e l'Università degli Studi di Padova ed altri, con ordinanza dell'8 aprile 2011, iscritta al n. 205 del registro ordinanze 2011 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 42, prima serie speciale, dell'anno 2011.
 Visti l'atto di costituzione di M.R., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 26 marzo 2013 il Giudice relatore Sergio Mattarella;
 uditi l'avvocato Paolo Francesco Brunello per M.R. e l'avvocato dello Stato Ettore Figliolia per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Nel corso di un giudizio, promosso per l'annullamento di due provvedimenti con i quali il Preside della Facoltà di medicina e chirurgia dell'Università di Padova aveva respinto altrettante istanze finalizzate al conseguimento di due incarichi annuali di insegnamento a titolo gratuito, il Tribunale amministrativo regionale per il Veneto ha sollevato, in riferimento agli articoli 3, 33, 35 e 97 della Costituzione, questione di legittimità costituzionale dell'art. 1, comma 10, della legge 4 novembre 2005, n. 230 (Nuove disposizioni concernenti i professori e i ricercatori universitari e delega al Governo per il riordino del reclutamento dei professori universitari).
 2.- Osserva il giudice rimettente che il ricorrente - dipendente dell'Università di Padova con la qualifica di tecnico informatico - si è visto respingere le proprie domande di affidamento dell'incarico, a titolo gratuito, di docente di informatica, quanto al corso di laurea in infermieristica pediatrica, e del corso integrato, quanto al corso di laurea in terapia della neuro e psicomotricità dell'età evolutiva; ciò pur essendo laureato in informatica ed abilitato all'insegnamento di tale materia presso le scuole medie superiori. A tale decisione l'amministrazione è pervenuta sulla base della norma censurata, secondo cui il personale tecnico amministrativo delle università è escluso dalla possibilità di ottenere incarichi di insegnamento (anche gratuito) da parte delle università stesse; in relazione al primo incarico, poi, non era stato possibile applicare la specifica deroga di cui all'art. 54, comma 8, del contratto collettivo del personale del comparto universitario, non trattandosi di materia considerata come "caratterizzante" i corsi di studio per le professioni sanitarie infermieristiche ed ostetriche.
 Nell'impugnare i due provvedimenti di diniego, il ricorrente ha, tra l'altro, prospettato dubbi di legittimità costituzionale relativamente all'art. 1, comma 10, della legge n. 230 del 2005.
 Ciò premesso, il giudice a quo evidenzia, in relazione al profilo della rilevanza, che la decisione dei due ricorsi impone la preliminare valutazione della legittimità costituzionale della censurata disposizione. Al riguardo, il TAR per il Veneto dà conto del fatto che detta norma è stata abrogata, nelle more del giudizio, ad opera dell'art. 29 della legge 30 dicembre 2010, n. 240 (Norme in materia di organizzazione delle università, di personale accademico e reclutamento, nonché delega al Governo per incentivare la qualità e l'efficienza del sistema universitario); ed afferma che, ciò nonostante, permane la necessità di scrutinare la legittimità costituzionale della disposizione, in quanto ogni volta che una norma, benché formalmente abrogata, abbia trovato applicazione nel caso concreto, occorre accertare se la stessa «abbia prodotto effetti pregiudizievoli». Nella specie - osserva il TAR - la norma in questione è stata applicata dall'amministrazione, la quale «ha respinto le istanze del ricorrente in ragione della sua appartenenza al personale tecnico amministrativo dell'Università»; la disposizione, quindi, benché abrogata, ha ricevuto applicazione, sicché «persiste l'interesse e la rilevanza alla definizione del giudizio instaurato avverso i provvedimenti che sono stati assunti in sua applicazione».
 Tanto premesso in punto di rilevanza, il giudice a quo osserva che la questione di legittimità costituzionale dell'art. 1, comma 10, della legge n. 230 del 2005 appare anche non manifestamente infondata. Evidenti risultano al TAR rimettente i dubbi di legittimità costituzionale in riferimento agli artt. 3 e 97 Cost.; la discriminazione operata dalla censurata disposizione, infatti, sarebbe in contrasto col principio di uguaglianza, perché la limitazione della possibilità di conferimento di insegnamenti è «esclusivamente diretta nei confronti di una particolare categoria di dipendenti pubblici, in particolare delle università, quale è il personale tecnico amministrativo»; né, d'altra parte, vi sono degli impedimenti legittimi a che detto personale, ove dotato dei necessari requisiti professionali, svolga anche un'attività di docenza in incarichi che - come nel caso dei ricorsi in esame - sono di limitato impegno orario e privi di compenso.
 In tal modo, inoltre, l'amministrazione è costretta a privarsi, in violazione del principio costituzionale del buon andamento, «di potenziali validi elementi che, in possesso delle necessarie competenze, potrebbero metterle al servizio della stessa Università presso la quale già svolgono la loro attività lavorativa».
 Secondo il TAR per il Veneto, infine, la disposizione censurata sarebbe in contrasto anche con gli artt. 33 e 35 Cost., norme che sono espressione dei principi della libertà di insegnamento e dell'arricchimento professionale dei lavoratori.
 3.- Si è costituito nel giudizio davanti a questa Corte R.M., ricorrente nel giudizio a quo, chiedendo l'accoglimento della prospettata questione di legittimità costituzionale.
 Osserva la parte privata, nel richiamare integralmente il contenuto degli atti difensivi già proposti davanti al TAR per il Veneto, che l'abrogazione della norma sottoposta al giudizio della Corte non toglie rilevanza alla questione di legittimità costituzionale né fa venire meno l'interesse ad ottenere una pronuncia di merito davanti al giudice amministrativo. D'altra parte, la norma impugnata ha esplicato i suoi effetti in danno del ricorrente; né la sua abrogazione è stata totale, perché l'art. 23 della legge n. 240 del 2010 ha riproposto, sia pure in altra forma, la medesima esclusione del personale tecnico amministrativo delle università dalla possibilità di ottenere incarichi di insegnamento universitario.
 4.- È intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione venga dichiarata inammissibile o infondata.
 Secondo la difesa erariale, la giurisprudenza costituzionale ha ribadito in più occasioni che è possibile lo scrutinio di legittimità costituzionale di una norma abrogata in epoca antecedente alla rimessione della questione, ma a condizione che il giudice a quo dia conto delle ragioni per le quali continua a sussistere il requisito della rilevanza (vengono richiamate, al riguardo, le sentenze n. 391 del 2008 e n. 104 del 2003; nonché l'ordinanza n. 83 del 2009). Nella specie, al contrario, l'ordinanza di rimessione non fornirebbe adeguata motivazione in tal senso, né la circostanza che la disposizione sia stata applicata nel giudizio è sufficiente, di per sé, a determinare la rilevanza della questione. Oltre a ciò, l'eventuale declaratoria di illegittimità costituzionale sarebbe priva di conseguenze concrete in quanto, in considerazione della natura gratuita degli incarichi di insegnamento oggetto del giudizio principale, l'accoglimento della questione condurrebbe all'illegittimità derivata dei provvedimenti impugnati, «senza che a tale declaratoria possano collegarsi effetti risarcitori».
 Da tanto dovrebbe dedursi l'inammissibilità della questione prospettata.
 Nel merito, peraltro, la stessa sarebbe comunque priva di fondamento.
 Rileva l'Avvocatura dello Stato, al riguardo, che la distinzione del personale che svolge attività didattica da quello di supporto tecnico amministrativo sarebbe del tutto ragionevole sotto il profilo organizzativo; d'altra parte, nulla vieterebbe al personale tecnico amministrativo di concorrere per l'accesso ai ruoli del personale docente. La normativa speciale di cui all'art. 6, comma 3, del decreto legislativo 30 dicembre 1992, n. 502 (Riordino della disciplina in materia sanitaria, a norma dell'articolo 1 della legge 23 ottobre 1992, n. 421) - che consente incarichi di insegnamento da parte del personale del ruolo sanitario nei corsi per il personale sanitario infermieristico - sarebbe, secondo la difesa erariale, la conferma della ragionevolezza della disposizione in esame nel presente giudizio, in quanto il servizio espletato è strettamente collegato con le materie oggetto di insegnamento; attinenza che, invece, non sussisterebbe nel caso oggetto della presente questione.Considerato in diritto1.- Il Tribunale amministrativo regionale per il Veneto dubita della legittimità costituzionale dell'art. 1, comma 10, della legge 4 novembre 2005, n. 230 (Nuove disposizioni concernenti i professori e i ricercatori universitari e delega al Governo per il riordino del reclutamento dei professori universitari), per contrasto con gli articoli 3, 33, 35 e 97 della Costituzione.
 Tale disposizione sarebbe infatti discriminatrice della particolare categoria di dipendenti pubblici, nell'ambito delle università, costituita dal personale tecnico amministrativo, e sarebbe altresì lesiva degli artt. 33 e 35 Cost., relativi alla libertà di insegnamento e all'arricchimento professionale dei lavoratori.
 2.- Occorre preliminarmente esaminare l'eccezione di inammissibilità per difetto di motivazione in ordine alla rilevanza, sollevata dalla difesa erariale, specie in ragione dell'abrogazione della norma impugnata, avvenuta in epoca antecedente alla rimessione della questione di legittimità costituzionale.
 Tale eccezione risulta non fondata.
 L'ordinanza di rimessione contiene, invero, una dettagliata ricostruzione della vicenda processuale, che consente il controllo sulla rilevanza, sia riguardo al contenuto del ricorso principale, sia riguardo alla legittimità degli atti impugnati.
 Il rimettente esamina i motivi di impugnazione per evidenziare che l'accoglimento del ricorso è subordinato alla declaratoria di illegittimità costituzionale della norma impugnata, che vieta il conferimento di incarichi di insegnamento al personale tecnico amministrativo delle università.
 Quanto all'asserito difetto di motivazione circa la rilevanza della questione di legittimità costituzionale di norma abrogata in epoca antecedente alla rimessione della questione stessa, questa Corte ha costantemente affermato la persistenza della rilevanza, anche nel caso in cui la norma sottoposta a scrutinio sia stata dichiarata incostituzionale o sostituita da una successiva, perché, ove un determinato atto amministrativo sia stato adottato sulla base di una norma poi abrogata o dichiarata costituzionalmente illegittima, «la legittimità dell'atto deve essere esaminata, in virtù del principio tempus regit actum, con riguardo alla situazione di fatto e di diritto esistente al momento della sua adozione» (sentenza n. 177 del 2012; nonché, tra le altre, sentenze n. 321 del 2011, n. 209 del 2010, n. 391 del 2008, n. 509 del 2000).
 Nel caso in esame, il TAR per il Veneto fornisce adeguata motivazione della sussistenza del requisito della rilevanza. L'ordinanza di rimessione non si limita ad indicare le ragioni per le quali l'accertamento della legittimità degli atti impugnati, che hanno fatto diretta applicazione della norma censurata allora vigente, dipenda dal giudizio di conformità al dettato costituzionale della norma stessa, ma anche i motivi della persistenza dell'interesse ad ottenere una pronuncia in ordine ai provvedimenti impugnati dal ricorrente nel giudizio principale.
 In effetti, contrariamente a quanto eccepito dall'Avvocatura dello Stato, il giudice rimettente non richiama soltanto la circostanza che la disposizione successivamente abrogata ha trovato concreta applicazione nel giudizio a quo - circostanza che, di per sé, in base alla giurisprudenza di questa Corte potrebbe anche non determinare la rilevanza della questione di costituzionalità per carenza di descrizione della fattispecie (tra le altre, ordinanze n. 43 del 2012, n. 38 del 2012, n. 203 del 2011) o per difetto di motivazione sulla rilevanza stessa (ex plurimis, ordinanza n. 25 del 2012, sentenza n. 41 del 2011) - bensì fornisce adeguata motivazione in ordine agli effetti esplicati dalla norma impugnata in danno del ricorrente.
 Né può farsi discendere, come eccepito dall'Avvocatura dello Stato, il difetto di rilevanza della questione dall'asserita mancanza di effetti risarcitori in caso di eventuale declaratoria di incostituzionalità della norma impugnata, in ragione della gratuità degli incarichi di insegnamento. Infatti, secondo consolidata giurisprudenza di questa Corte, stante l'autonomia del giudizio costituzionale rispetto al giudizio a quo, che preclude a questa Corte la valutazione delle concrete conseguenze sulle situazioni giuridiche soggettive azionate nel giudizio principale, è sufficiente che l'ordinanza di rimessione argomenti non implausibilmente la rilevanza della questione di legittimità costituzionale (ex plurimis, ordinanza n. 25 del 2012, sentenza n. 270 del 2010).
 Nel caso in esame, il TAR per il Veneto fornisce adeguata motivazione circa la perdurante rilevanza della questione e la persistenza dell'interesse ad ottenere una pronuncia di merito, anche in ragione dell'astratta prevedibilità del danno, che non spetta comunque a questa Corte valutare.
 3.- Nel merito, la questione sollevata in riferimento all'art. 3 Cost. è fondata.
 Il divieto introdotto dalla norma impugnata, successivamente abrogata dall'art. 29, comma 11, lettera c), della legge 30 dicembre 2010, n. 240 (Norme in materia di organizzazione delle università, di personale accademico e reclutamento, nonché delega al Governo per incentivare la qualità e l'efficienza del sistema universitario), è diretto esclusivamente nei confronti di una particolare categoria di dipendenti pubblici, nell'ambito delle diverse categorie dei dipendenti delle università, quale si configura il personale tecnico amministrativo, e non già nei confronti di una categoria generale.
 Siffatta evidente diversità della disciplina di medesime categorie di dipendenti pubblici, sottoposti, tra l'altro, ai fini dell'eventuale svolgimento dell'incarico di insegnamento, all'ordinario regime autorizzatorio previsto dall'art. 53 del decreto legislativo 30 marzo 2001, n. 165 (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche), non appare riconducibile ad alcuna ragionevole ratio giustificatrice, ed anzi risulta manifestamente irragionevole. 
 Al riguardo, questa Corte ha costantemente censurato norme discriminatrici di determinate categorie di dipendenti pubblici o privati per effetto di trattamento irragionevolmente differenziato (ex plurimis, sentenze n. 321 del 2011, n. 296 del 2010).
 Anche in specifico riferimento alle diverse categorie di dipendenti pubblici delle università, pur ribadendo, sotto molteplici profili, l'«essenziale differenziazione» tra personale docente e personale non docente (tra le altre, la sentenza n. 191 del 2008; nonché le ordinanze n. 160 del 2003 e n. 262 del 2002), questa Corte ha dichiarato, siccome in contrasto con il principio di ragionevolezza, l'illegittimità costituzionale di norme ritenute discriminatrici di determinate categorie del personale pubblico universitario, in presenza di trattamenti differenziati privi di razionale giustificazione (oltre alla già richiamata sentenza n. 191 del 2008, tra le altre, le sentenze n. 305 del 1995 e n. 39 del 1989).
 4.- Va, dunque, dichiarata l'illegittimità costituzionale dell'art. 1, comma 10, della legge n. 230 del 2005, per violazione dell'art. 3 Cost. 
 Gli ulteriori profili rimangono assorbiti.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara l'illegittimità costituzionale dell'art. 1, comma 10, della legge 4 novembre 2005 n. 230 (Nuove disposizioni concernenti i professori e i ricercatori universitari e delega al Governo per il riordino del reclutamento dei professori universitari).&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 aprile 2013.&#13;
 F.to:&#13;
 Franco GALLO, Presidente&#13;
 Sergio MATTARELLA, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 24 aprile 2013.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
