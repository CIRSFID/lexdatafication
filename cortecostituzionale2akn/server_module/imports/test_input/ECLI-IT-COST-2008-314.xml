<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/314/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/314/"/>
          <FRBRalias value="ECLI:IT:COST:2008:314" name="ECLI"/>
          <FRBRdate date="29/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="314"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/314/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/314/ita@/!main"/>
          <FRBRdate date="29/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/314/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/314/ita@.xml"/>
          <FRBRdate date="29/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="30/07/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>29/07/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 91, primo comma, del codice di procedura civile e dell'art. 75 delle disposizioni di attuazione del codice di procedura civile, promosso con ordinanza del 26 aprile 2007 dal Giudice di pace di Milano nel procedimento civile vertente tra V. S. e B J. J. ed altra, iscritta al n. 60 del registro ordinanze 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 12, prima serie speciale, dell'anno 2008. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 9 luglio 2008 il Giudice relatore Alfio Finocchiaro. 
    Ritenuto che – nel corso di giudizio promosso da V. S. nei confronti di B J. J. e della compagnia assicuratrice Zurich Insurance Company per il risarcimento dei danni patrimoniali subiti a causa del sinistro stradale asseritamente cagionato dalla condotta del convenuto – il Giudice di pace di Milano, con ordinanza depositata il 26 aprile 2007, ha sollevato questione di legittimità costituzionale degli artt. 91 del codice di procedura civile e 75 delle disposizioni di attuazione al codice di procedura civile, nella parte in cui non prevedono il contraddittorio sul quantum delle spese processuali, per violazione dell'art. 111, secondo comma, della Costituzione, sotto il profilo della lesione al principio del contraddittorio; dell'art. 24, secondo comma, della Costituzione, per vulnus al diritto di difesa; dell'art. 3 della Costituzione, per irragionevolezza delle norme denunciate; 
    che il rimettente, premesso che, nel corso dell'udienza di precisazione delle conclusioni e di discussione della causa, il difensore dell'attore aveva depositato nota spese, mentre il difensore dei convenuti, pur chiedendo il rimborso delle spese processuali, si era astenuto dal presentare la nota relativa, rimettendosi al giudice per la liquidazione, ha ritenuto che, allo stato, non potesse essere emessa sentenza, non avendo le parti convenute, in base alla vigente legislazione ordinaria, potuto esercitare il loro diritto di difesa, nel rispetto del principio del contraddittorio, sulla domanda dell'attore di una loro condanna per spese processuali; 
    che – rileva il rimettente – il giudice, se e quando condanna al rimborso delle spese processuali, emette una pronuncia di condanna inaudita altera parte, e non di rado per importi che superano, e non di poco, il valore della stessa causa; 
    che gli artt. 91 cod. proc. civ. e 75 disp. att. cod. proc. civ. non prevedono che ciascuna delle parti possa esaminare la nota spese dell'altra e possa, sia pure in un breve lasso di tempo, formulare al giudice osservazioni o riserve; 
    che – prosegue il giudice a quo – la mancata previsione del contraddittorio sulle spese processuali, in particolare per quanto riguarda il quantum, è di dubbia legittimità costituzionale, non potendo l'espressione «nel contraddittorio tra le parti» non riferirsi a tutto lo svolgimento del processo, anche perché il giudice, in presenza di nota specifica prodotta dalla parte vittoriosa, ha l'onere di dare adeguata motivazione dell'eliminazione o della riduzione di voci da lui operata, allo scopo di consentire, attraverso il sindacato di legittimità, l'accertamento della conformità della liquidazione a quanto risulta dagli atti e alle tariffe; 
    che in base alla normativa vigente, inoltre, le sentenze di condanna al rimborso delle spese processuali vengono emesse senza alcuna possibilità di difesa – da cui la ritenuta violazione dell'art. 24 Cost. – almeno per quanto riguarda il quantum; 
    che l'esclusione del contraddittorio sulle spese processuali potrebbe quindi essere illegittima anche in relazione al principio di ragionevolezza di cui all'art. 3 Cost., quando entrambe le parti in causa sono costituite; 
    che, sotto il profilo della rilevanza, il rimettente assume che, ai fini della definizione del giudizio a quo, la eventuale declaratoria di illegittimità costituzionale delle norme denunciate comporterebbe che egli debba, prima di emettere la sentenza, sentire ciascuna delle parti sulla nota spese della parte avversaria e, quindi, sul quantum dei diritti e degli onorari; 
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto dichiararsi l'infondatezza della questione proposta; 
    che, secondo la difesa erariale, la determinazione del compenso professionale: a) rientra nel potere discrezionale del giudice, in relazione a limiti tabellari; b) è attività che non attiene al merito della controversia, e può essere compiuta d'ufficio; c) mira a tenere la parte vittoriosa indenne dagli oneri processuali; d) è attività accessoria, consequenziale alla decisione sul merito della causa e, per questo, non è assimilabile alla decisione attinente il merito delle questioni discusse dalle parti in contraddittorio. 
    Considerato che il Giudice di pace di Milano dubita della legittimità costituzionale degli artt. 91 del codice di procedura civile e 75 delle disposizioni di attuazione al codice di procedura civile, nella parte in cui prevedono che il giudice condanna la parte soccombente nel processo innanzi a sé al rimborso delle spese a favore dell'altra parte e ne liquida l'ammontare inaudita altera parte, sulla base della nota spese depositata dal difensore della parte vittoriosa al momento del passaggio in decisione della causa, per violazione dell'art. 111, secondo comma, della Costituzione, sotto il profilo della lesione al principio del contraddittorio; dell'art. 24, secondo comma, della Costituzione, per vulnus al diritto di difesa; dell'art. 3 della Costituzione, per irragionevolezza delle norme denunciate; 
    che il giudice a quo considera la statuizione sulle spese come un capo autonomo della sentenza, corrispondente ad una domanda anch'essa autonoma, su cui dovrebbe, a suo parere, svilupparsi il contraddittorio tra le parti, come su ogni questione di merito sottoposta alla sua decisione; 
    che il presupposto da cui muove il rimettente, nell'esporre le ragioni della ritenuta non manifesta infondatezza della questione, non è esatto; 
    che la regolamentazione delle spese, cui è tenuto il giudice ogni volta che chiuda il giudizio davanti a sé, in base alla nota spese (non indefettibilmente) presentata dalle parti, non è assimilabile alla decisione di merito su un capo della domanda, dal momento che la natura accessoria della pronuncia sulle spese, non scindibile dalla decisione sul merito, esclude che sulla questione, ed in particolare sulla quantificazione delle spese stesse, sia necessario instaurare uno specifico contraddittorio; 
    che, dovendosi allegare la nota spese al fascicolo di parte «al momento del passaggio in decisione della causa» (art. 75 disp. att. cod. proc. civ.), e dunque al momento del deposito della comparsa conclusionale (art. 169, secondo comma, cod. proc. civ.), la controparte può controdedurre, anche sulla eventuale nota spese, nei successivi venti giorni, attraverso la memoria di replica, o alla stessa udienza unificata, ex art. 281-quinquies, secondo comma, cod. proc. civ., ed ex art. 321 cod. proc. civ., nel giudizio davanti al giudice di pace; 
    che la dipendenza della decisione sulle spese rispetto al capo della sentenza che dispone circa il diritto fatto valere in giudizio si basa su una serie di riscontri positivi; 
    che la condanna alla rifusione delle spese è impugnabile o opponibile solo se è impugnabile o opponibile il provvedimento che dispone sul diritto principale, mentre non lo è se questo è passato in giudicato o comunque non è sottoponibile ad ulteriore controllo da parte di altri giudici; 
    che il sistema processuale non ammette la proposizione di una domanda per conseguire il rimborso delle spese processuali, che sia formulata autonomamente e fuori della sede nella quale quelle spese furono prodotte, poiché il danno che una parte abbia subìto per far valere in giudizio un diritto o per resistere a una pretesa di altri, non può essere oggetto di autonomo processo e deve essere invocato all'interno del processo in cui si discuta del merito; 
    che la condanna del soccombente alla rifusione delle spese può intervenire senza che il vincitore abbia spiegato domanda in tal senso, provvedendovi il giudice come conseguenza dovuta dell'accoglimento della domanda di merito; 
    che la domanda di condanna nel merito contiene (salvo rinuncia) anche la richiesta di condanna alle spese; 
    che da quanto precede consegue la marginalità del tema delle spese rispetto ai principi del giusto processo, e dunque l'estraneità di ogni questione ad esse relativa all'ambito di estensione del contraddittorio; 
    che questa Corte ha ritenuto (ord. n. 117 del 1999) che il regolamento delle spese processuali comunque non incide sulla tutela giurisdizionale del diritto di chi agisce o si difende in giudizio, non potendosi sostenere che la possibilità di conseguire la ripetizione delle spese processuali (ovvero, dei diritti e degli onorari di avvocato) consenta alla parte di meglio difendere la sua posizione e di apprestare meglio le sue difese; 
    che gli elementi, su cui si basa la condanna alle spese, che sono isolabili su un piano diverso dall'ambito delle prove e degli elementi dai quali muove la decisione sul merito della causa, evidenzia la natura “di risulta” (o accessoria) della statuizione sulle spese, che non è assimilabile, nel suo processo formativo, alla decisione sul merito della causa; 
    che, con riguardo allo specifico profilo della quantificazione delle spese, la richiesta di liquidazione, connessa alla presentazione della nota delle spese (art. 75 disp. att. cod. proc. civ.), non muta i termini della questione, non inserendo alcun elemento contenzioso su cui si configuri la necessità di un contraddittorio, e in relazione al quale sia ipotizzabile la lesione del diritto di difesa o la violazione dei principi del giusto processo, ove il destinatario della futura condanna alle spese non sia stato in condizione di replicare alla nota spese della parte risultata vittoriosa; 
    che la funzione della nota spese si limita ad un ausilio al magistrato, alla stregua di rendiconto, nel compimento di una mera operazione contabile, dal momento che la mancata presentazione della stessa non può procrastinare la decisione della controversia, e la condanna al pagamento delle spese di lite legittimamente può essere emessa, a carico della parte soccombente, anche d'ufficio, in mancanza di un'esplicita richiesta della parte vittoriosa, a meno che risulti l'esplicita volontà di quest'ultima di rinunziarvi; 
    che la questione è, pertanto, manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 91 del codice di procedura civile e 75 delle disposizioni di attuazione al codice di procedura civile, sollevata, in riferimento agli artt. 3, 24, secondo comma, e 111, secondo comma, della Costituzione, dal Giudice di pace di Milano, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 29 luglio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfio FINOCCHIARO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 30 luglio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
