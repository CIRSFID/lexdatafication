<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2016/106/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2016/106/"/>
          <FRBRalias value="ECLI:IT:COST:2016:106" name="ECLI"/>
          <FRBRdate date="06/04/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="106"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2016/106/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2016/106/ita@/!main"/>
          <FRBRdate date="06/04/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2016/106/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2016/106/ita@.xml"/>
          <FRBRdate date="06/04/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/05/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Mario Rosario Morelli</cc:relatore_pronuncia>
        <cc:data_decisione>06/04/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giuseppe FRIGO, Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nei giudizi di legittimità costituzionale degli artt. 15, comma 2, e 34, comma 17, del decreto legislativo 1° settembre 2011, n. 150 (Disposizioni complementari al codice di procedura civile in materia di riduzione e semplificazione dei procedimenti civili di cognizione, ai sensi dell'articolo 54 della legge 18 giugno 2009, n. 69), promossi dalla Corte di cassazione, con ordinanza del 1° aprile 2015, e dal Tribunale ordinario di Bergamo, con ordinanza del 4 maggio 2015, iscritte ai nn. 216 e 217 del registro ordinanze 2015 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 43, prima serie speciale, dell'anno 2015.
 Udito nella camera di consiglio del 6 aprile 2016 il Giudice relatore Mario Rosario Morelli.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Nel corso di una procedura di espropriazione immobiliare, in sede di ricorso per cassazione, proposto dalla creditrice espropriante avverso il provvedimento (del Tribunale di Como), con il quale la sua opposizione al decreto di liquidazione del compenso all'ausiliario del giudice, era stata dichiarata inammissibile per inosservanza del termine perentorio (di «venti giorni dalla avvenuta comunicazione») di cui all'art. 170 del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia - Testo A), l'adita Corte di legittimità - premessane la rilevanza, in quanto la ricorrente eccepiva l'estraneità di quel termine al testo novellato (applicabile ratione temporis) del citato art. 170 - ha sollevato, con l'ordinanza in epigrafe, questione di legittimità costituzionale dell'art. 34, comma 17 (modificativo del predetto art. 170 del d.P.R. n. 115 del 2002), e dell'art. 15, comma 2 (recante nuova disciplina dell'opposizione a decreto di pagamento delle spese di giustizia), del decreto legislativo 1° settembre 2011, n. 150 (Disposizioni complementari al codice di procedura civile in materia di riduzione e semplificazione dei procedimenti civili di cognizione, ai sensi dell'articolo 54 della legge 18 giugno 2009, n. 69), nella parte, appunto, in cui risulta, da dette disposizioni, «abrogato l'inciso contenuto nell'originario primo comma dell'art. 170 d.P.R. 30 maggio 2002, n. 115, "entro venti giorni dall'avvenuta comunicazione"».
 Secondo la Corte rimettente, la normativa denunciata violerebbe, in primo luogo, l'art. 76 della Costituzione, in relazione alla norma di delega di cui all'art. 54, commi 1 e 4, della legge 18 giugno 2009, n. 69 (Disposizioni per lo sviluppo economico, la semplificazione, la competitività nonché in materia di processo civile), in quanto la soppressione, da parte del legislatore delegato, del termine, originariamente previsto per la proposizione della opposizione al decreto di liquidazione delle spese di giustizia, «eccede certamente dall'ambito della delega, circoscritta com'è stata questa - nella specie - alla mera "riconduzione" di un rito preesistente ad altro». In via subordinata, contrasterebbe, comunque, con l'art. 3 Cost., con il dar luogo ad una ingiustificata diversa disciplina della fattispecie della liquidazione dell'ausiliario del giudice rispetto ad ogni altra ipotesi di provvedimento emesso inaudita altera parte, per le quali è previsto il termine decadenziale di proposizione del ricorso; con l'art. 24 Cost., poiché il provvedimento avente ad oggetto la liquidazione dell'ausiliario del giudice, risulterebbe sine die ricorribile, in mancanza di un termine in proposito; e con l'art. 111, settimo comma, Cost., in quanto la mancata previsione di un termine per la proposizione del ricorso sottrarrebbe la fattispecie in esame ad un equo vaglio giurisdizionale sulla base di primarie esigenze di certezza sui tempi del processo.
 2.- In altro procedimento di opposizione a decreto di liquidazione del compenso spettante ad un ausiliario del giudice, il Tribunale ordinario di Bergamo ha, a sua volta, sollevato, con l'ordinanza in epigrafe, questione - sostanzialmente identica a quella prospettata in via principale dalla Corte di cassazione - di legittimità costituzionale «dell'art. 34, comma 17, del d.lgs. 150 del 2011, per contrasto con l'art. 76 Cost. in relazione all'art. 54, comma 4, della l. 69 del 2009, nella parte in cui, sostituendo il comma 1 dell'art. 170 del d.P.R. 30 maggio 2002, n. 115, ha soppresso il termine di venti giorni dall'avvenuta comunicazione previsto dall'originaria versione della norma sostituita».
 3.- Nei due giudizi innanzi a questa Corte - che possono riunirsi per l'identità della norma denunciata sulla base della medesima censura di eccesso di delega - non vi è stata costituzione di parti, né ha spiegato intervento il Presidente del Consiglio dei ministri.Considerato in diritto1.- La Corte di cassazione e il Tribunale ordinario di Bergamo, nei due giudizi  come sopra riuniti, sollevano la medesima questione di legittimità costituzionale dell'art. 34, comma 17 - cui la Corte di cassazione affianca l'art. 15, comma 2 - del decreto legislativo 1° settembre 2011, n. 150 (Disposizioni complementari al codice di procedura civile in materia di riduzione e semplificazione dei procedimenti civili di cognizione, ai sensi dell'articolo 54 della legge 18 giugno 2009, n. 69), per contrasto con l'art. 76 della Costituzione, in relazione alla norma di delega di cui all'art. 54, comma 4 - cui la Corte di cassazione premette il comma 1- della legge 18 giugno 2009, n. 69 (Disposizioni per lo sviluppo economico, la semplificazione, la competitività nonché in materia di processo civile), nella parte in cui la denunciata normativa, sostituendo l'art. 170, comma 1, del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia - Testo A), ha soppresso il termine di «venti giorni dall'avvenuta comunicazione», previsto per la proposizione dell'opposizione al decreto di liquidazione delle spese di giustizia, nel testo della disposizione sostituita.
 1.1.- Il testo originario dell'art. 170 prevedeva, infatti, al comma 1, che «Avverso il decreto di pagamento emesso a favore dell'ausiliario del magistrato [...] il beneficiario e le parti processuali, compreso il pubblico ministero, possono proporre opposizione, entro venti giorni dall'avvenuta comunicazione, al presidente dell'ufficio giudiziario competente».
 In attuazione della delega di cui ai primi quattro commi dell'art. 54 della legge n. 69 del 2009, il legislatore delegato ha, per quanto qui rileva, con il denunciato art. 34, comma 17, del d.lgs. n. 150 del 2011, sostituito, come detto, il primo comma dell'art. 170 ed abrogato i due suoi commi successivi, sicché effettivamente esso, ora, solamente prevede che «Avverso il decreto di pagamento emesso a favore dell'ausiliario del magistrato, [...] il beneficiario e le parti processuali, compreso il pubblico ministero, possono porre opposizione» e che «L'opposizione è disciplinata dall'articolo 15 del decreto legislativo 1° settembre 2011, n. 150». 
 Quest'ultima disposizione definisce l'iter processuale delle opposizioni in esame, stabilendo che esse «sono regolate dal rito sommario di cognizione [...] Il  ricorso è proposto al capo dell'ufficio giudiziario cui appartiene il magistrato che ha emesso il provvedimento impugnato [...] Nel giudizio di merito le parti possono stare in giudizio personalmente [...] L'efficacia esecutiva del provvedimento impugnato può essere sospesa [...] Il presidente può chiedere a chi ha provveduto alla liquidazione o a chi li detiene, gli atti, i documenti e le informazioni necessari ai fini della decisione [...] L'ordinanza che definisce il giudizio non è appellabile.»
 A sua volta, anche il predetto art. 15 non fa, però, menzione alcuna del termine perentorio originariamente previsto per la proposizione della opposizione di che trattasi.
 Da ciò il sospetto di violazione dell'art. 76 Cost., riferito alla denunciata normativa delegata, la quale - con il "sopprimere" il termine di cui sopra, «coessenziale alla certezza del diritto e quindi alla funzione stessa del processo» - avrebbe ecceduto dagli obiettivi, di "coordinamento", fissati dal legislatore delegante del 2009.
 1.2.- Nella prospettazione comune ad entrambi i rimettenti, la questione, così sollevata, muove, dunque, dalla premessa che - in conseguenza dell'intervenuta sostituzione dell'art. 170 del d.P.R. n. 115 del 2002 ad opera dell'art. 34, comma 17, del d.lgs. n. 150 del 2011 - l'opposizione avverso il decreto di liquidazione del compenso dovuto all'ausiliario del giudice sia ora proponibile "sine die" e resti, perciò, soggetta solo al termine ordinario di prescrizione, «irragionevolmente eccessivo».
 1.3.- Una tale premessa evidenzia, però, una non completa ricognizione del quadro normativo di riferimento.
 1.3.1.- In attuazione della delega di cui al comma 1 dell'art. 54 della legge n. 69 del 2009 - la quale demandava al Governo di adottare uno o più decreti legislativi «in materia di riduzione e semplificazione dei procedimenti civili di cognizione che rientrano nell'ambito della giurisdizione ordinaria e che sono regolati dalla legislazione speciale» - il legislatore delegato, con il d.lgs. n. 150 del 2011, ha, come è noto, "ricondotto" varie tipologie di procedimenti a tre soli principali schemi di rito: rispettivamente, il cosiddetto rito del lavoro, il rito ordinario ed il rito sommario. E, quanto a quest'ultimo, ha fatto riferimento alla disciplina introdotta ex novo dall'art. 51 della medesima legge di delega, con l'inserimento - nel corpus del codice di procedura civile, all'interno del Titolo I del suo Libro IV - di un Capo III-bis (rubricato «Del procedimento sommario di cognizione»), composto dagli artt. 702-bis (Forma della domanda - Costituzione delle parti), 702-ter (Procedimento) e 702-quater (Appello).
 In particolare, l'art. 702-quater prevede che il provvedimento adottato in prima istanza dal giudice monocratico si consolidi in giudicato se non è appellato «entro trenta giorni dalla sua comunicazione o notificazione».
 1.3.2.- Orbene, l'art. 15, comma 1, del d.lgs. n. 150 del 2011 dispone, appunto, che le opposizioni ai decreti in tema di spese di giustizia «sono regolate dal rito sommario».
 Ciò presuppone che, nello schema base di tale modulo processuale, il decreto di liquidazione del compenso all'ausiliario - emesso dal giudice che lo ha nominato ed opponibile (ex art. 15, comma 2, del predetto decreto legislativo) innanzi al capo dell'ufficio cui appartiene quel magistrato - debba, di conseguenza, considerarsi equiparato all'ordinanza del giudice monocratico, appellabile ex art. 702-quater cod. proc. civ.
 Pertanto, il termine, di trenta giorni dalla comunicazione o notificazione del provvedimento, di cui al citato art. 702-quater cod. proc. civ., deve ritenersi parimenti riferito, sia all'opposizione avverso il decreto sulle spese di giustizia, sia all'appello avverso l'ordinanza di cui all'art. 702-ter dello stesso codice, per esigenze di omogeneità del rito, al quale i due (sia pur diversi) comparati procedimenti sono ricondotti.
 1.3.3.- L'attrazione dell'opposizione in esame nel modello del rito sommario di cognizione spiega, dunque, perché il termine per la correlativa proposizione non sia più quello speciale, di venti giorni, previsto nel testo originario dell'art. 170 del d.P.R. n. 115 del 2002, bensì quello di trenta giorni stabilito ora in via generale per il riesame dei provvedimenti adottati in prima istanza nell'ambito di procedure riconducibili allo schema del rito sommario.
 1.3.4.- Cade, così, la premessa che l'opposizione al decreto di liquidazione delle spese di giustizia - nel testo del predetto art. 170, come novellato dall'impugnato art. 34, comma 17, del d.lgs. n. 150 del 2011 - sia stata sottratta a qualsiasi termine impugnatorio e resa proponibile sine die. E ciò conduce ad escludere che abbia alcun fondamento il dubbio di violazione dell'art. 76 Cost., riferito, in ragione di quella errata premessa, ai denunciati artt. 34, comma 17, e 15, comma 2, del d.lgs. n. 150 del 2011.
 2.- In quanto argomentata sulla base della medesima errata premessa di cui sopra, risulta priva di consistenza anche la questione di legittimità costituzionale della suddetta normativa delegata, come sollevata, in via subordinata, dalla Corte di cassazione, in riferimento agli artt. 3, 24 e 111, settimo comma, Cost.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 riuniti i giudizi, &#13;
 1) dichiara non fondata, nei sensi di cui in motivazione, la questione di legittimità costituzionale degli artt. 34, comma 17, e 15, comma 2, del decreto legislativo 1° settembre 2011, n. 150 (Disposizioni complementari al codice di procedura civile in materia di riduzione e semplificazione dei procedimenti civili di cognizione, ai sensi dell'articolo 54 della legge 18 giugno 2009, n. 69), sollevata, in riferimento all'art. 76 della Costituzione - in relazione all'art. 54, commi 1 e 4, della legge n. 69 del 2009 - ed agli artt. 3, 24 e 111, settimo comma, Cost., dalla Corte di cassazione, con l'ordinanza in epigrafe indicata;&#13;
 2) dichiara non fondata, nei sensi di cui in motivazione, la questione di legittimità costituzionale dell'art. 34, comma 17, del d.lgs. n. 150 del 2011, sollevata in riferimento all'art. 76 Cost., in relazione all'art. 54, comma 4, della legge n. 69 del 2009, dal Tribunale ordinario di Bergamo, con l'ordinanza in epigrafe indicata.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 aprile 2016.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Mario Rosario MORELLI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 12 maggio 2016.&#13;
 Il Cancelliere&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
