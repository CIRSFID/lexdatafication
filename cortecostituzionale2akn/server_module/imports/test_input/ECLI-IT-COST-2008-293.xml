<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/293/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/293/"/>
          <FRBRalias value="ECLI:IT:COST:2008:293" name="ECLI"/>
          <FRBRdate date="09/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="293"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/293/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/293/ita@/!main"/>
          <FRBRdate date="09/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/293/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/293/ita@.xml"/>
          <FRBRdate date="09/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="18/07/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Ugo De Siervo</cc:relatore_pronuncia>
        <cc:data_decisione>09/07/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 139 del codice penale, promosso con ordinanza del 13 luglio 2006 dalla Corte d'assise d'appello di Milano nel procedimento penale a carico di Bissoni Franco, iscritta al n. 563 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 50, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 25 giugno 2008 il Giudice relatore Ugo De Siervo. 
    Ritenuto che la Corte d'assise d'appello di Milano, con ordinanza in data 13 luglio 2006, ha sollevato – in riferimento agli artt. 3 e 27, terzo comma della Costituzione – questione di legittimità costituzionale dell'art. 139 del codice penale nella parte in cui «non consente, una volta espiata la pena principale, il differimento della pena accessoria della sospensione della patente, già differita sino al termine dell'espiazione della pena principale in quanto di ostacolo alla espiazione della pena detentiva nelle misure alternative della semi libertà e dell'affidamento in prova»; 
    che il rimettente riferisce di essere chiamato a decidere in ordine all'istanza con cui un soggetto, il quale ha riportato due condanne definitive per il reato di cui all'art. 73 del d.P.R. 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza), ha chiesto la modifica dell'esecuzione della pena accessoria del ritiro della patente di guida per tre anni irrogata ai sensi dell'art. 85 del citato decreto; 
    che, prosegue il rimettente, il condannato, detenuto dal 15 febbraio 1993, con fine pena al 16 agosto 2006, è stato ammesso al regime della semilibertà e quindi all'affidamento in prova e, a partire dal 21 maggio 1998, svolge attività lavorativa presso lo studio di un commercialista, attività per la quale ha necessità di disporre della patente di guida; 
    che, al fine di «non ostacolare lo svolgimento del programma lavorativo nel corso delle misure alternative», il procuratore generale competente per l'esecuzione, in data 5 giugno 2000, ha disposto il differimento della pena accessoria al termine della espiazione della pena principale; 
    che, in prossimità della cessazione della pena principale, il condannato ha chiesto di poter usufruire della patente di guida al fine di continuare a svolgere la propria attività lavorativa, invocando l'applicazione analogica dell'art. 62 della legge 24 novembre 1981, n. 689 (Modifiche al sistema penale), il quale dispone che quando il condannato svolge un lavoro per il quale la patente di guida costituisce indispensabile requisito, il magistrato di sorveglianza può disciplinare la sospensione in modo tale da non ostacolare il lavoro del condannato; 
    che il competente procuratore generale ha reso parere negativo sulla richiesta, ritenendo che l'esecuzione della pena accessoria non possa essere sospesa né che tale pena possa essere eseguita con modalità diverse da quelle tipicamente previste dalla legge; 
    che, tuttavia, ritiene il rimettente di dover sollevare questione di legittimità costituzionale dell'art. 139 cod. pen., dal momento che esso non consente, una volta espiata la pena principale, il differimento della pena accessoria della sospensione della patente, già differita sino al termine dell'espiazione della pena principale in quanto di ostacolo alla espiazione della pena detentiva nelle misure alternative della semi libertà e dell'affidamento in prova.; 
    che tale previsione, infatti, contrasterebbe con gli artt. 3 e 27, terzo comma, della Costituzione; 
    che, ad avviso della Corte d'assise d'appello, l'art. 139 cod. pen. deve essere inteso nel senso che la pena accessoria temporanea non può avere esecuzione contemporaneamente alla pena principale detentiva allorché vi sia assoluta incompatibilità tra le due pene; 
    che, rileva ancora il giudice a quo, mentre nel corso dell'espiazione della pena principale «una lettura costituzionalmente orientata dell'art. 139 c.p.» ha permesso di differire l'espiazione della pena accessoria al fine di non impedire al condannato di partecipare all'attività lavorativa utile al suo reinserimento sociale, «analoga interpretazione non sembra consentita allorché la pena principale sia stata espiata e resti da eseguire la pena accessoria»; 
    che, di conseguenza, si creerebbe una disparità di trattamento tra situazioni omogenee, dal momento che mentre sarebbe possibile sospendere temporaneamente l'esecuzione della pena accessoria durante la pena alternativa della semilibertà, ciò non sarebbe possibile una volta che la pena principale sia stata espiata; 
    che la rigida esecuzione della pena accessoria si porrebbe altresì in contrasto con le finalità rieducative della pena stessa; 
    che, con riguardo alla rilevanza della questione, il rimettente sostiene che l'istanza proposta dal condannato non possa essere decisa indipendentemente dalla risoluzione della questione di legittimità costituzionale dal momento che solo l'eventuale dichiarazione di incostituzionalità dell'art. 139 cod. pen. consentirebbe di valutare la sussistenza delle condizioni per poter accoglierla; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione venga dichiarata infondata; 
    che non pertinente sarebbe, infatti, il richiamo alla disciplina di cui all'art. 62 della legge n. 689 del 1981, evocato dal rimettente quale tertium comparationis, dal momento che tale disposizione disciplinerebbe solo l'esecuzione della semidetenzione e della libertà controllata, cioè di misure alternative alla pena principale; 
    che neppure sussisterebbe un contrasto con l'art. 27, terzo comma, della Costituzione, dal momento che l'ordinamento non potrebbe consentire un differimento sine die dell'esecuzione della pena accessoria, pena la vanificazione del contenuto dissuasivo e di difesa sociale proprio anche di tale pena. 
    Considerato che la Corte d'assise di appello di Milano ha sollevato questione di legittimità costituzionale dell'art. 139 cod. pen., nella parte in cui tale disposizione «non consente, una volta espiata la pena principale, il differimento della pena accessoria della sospensione della patente» di guida, in riferimento agli artt. 3 e 27, terzo comma, della Costituzione; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione venga dichiarata infondata; 
    che il giudizio a quo si origina da un'istanza di “modificazione” del regime di esecuzione di tale pena accessoria, proposta da parte di un soggetto che ad essa è stato condannato in via definitiva ai sensi dell'art. 85 del d.P.R. 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza); 
    che l'istanza è stata presentata al termine della espiazione della pena detentiva principale, eseguita, in parte, nella forma dell'affidamento in prova ai servizi sociali; 
    che, nel corso di tale ultima misura, l'istante ha avviato un'attività lavorativa, per l'esecuzione della quale si è valso della patente di guida, avendo il PM competente disposto in quest'occasione il differimento dell'esecuzione della pena accessoria, al fine di non compromettere il percorso di risocializzazione del reo; 
    che, a parere del giudice a quo, tale differimento non può più essere concesso, una volta interamente espiata la pena principale, posto che la norma censurata imporrebbe di eseguire la pena accessoria incompatibile con il regime carcerario, non appena scontata quella principale; 
    che tale rigida previsione si porrebbe in contrasto con le finalità rieducative della sanzione penale, vanificando il cammino di risocializzazione già fruttuosamente intrapreso dal reo, e violando in tal modo l'art. 27, terzo comma, della Costituzione; 
    che sarebbe parimenti leso l'art. 3 della Costituzione, posto che la norma oggetto determinerebbe disparità di trattamento rispetto all'ipotesi normata dall'art. 62 della legge 24 novembre 1981, n. 689 (Modifiche al sistema penale), ove invece è consentito al magistrato di sorveglianza di disciplinare il regime di esecuzione della semidetenzione e della libertà controllata, quanto all'impiego della patente di guida, «in modo da non ostacolare il lavoro del condannato»; 
    che, per tali ragioni, il rimettente chiede a questa Corte di configurare in capo al giudice dell'esecuzione, mediante la declaratoria di illegittimità dell'art. 139 cod. pen., un potere di differimento della pena accessoria in questione, alle condizioni sopra ricordate; 
    che la norma censurata stabilisce che «nel computo delle pene accessorie temporanee non si tiene conto del tempo in cui il condannato sconta la pena detentiva, o è sottoposto a misura di sicurezza detentiva, né del tempo in cui egli si è sottratto volontariamente alla esecuzione della pena o della misura di sicurezza»; 
    che da tale previsione la giurisprudenza ha tratto la non implausibile conseguenza per cui la pena accessoria temporanea, incompatibile con la detenzione in casa di reclusione, possa e debba essere eseguita, solo una volta scontata la pena principale detentiva; 
    che, per effetto di ciò, la pena accessoria in tali casi si connota per una tendenziale impermeabilità rispetto all'esito del percorso di rieducazione che il reo ha compiuto, proprio nel corso dell'esecuzione della pena principale; 
    che questa Corte ha già avuto occasione di valutare la compatibilità di una pena accessoria di tale natura con l'art. 27, terzo comma, della Costituzione, concludendo per l'infondatezza della questione di legittimità allora proposta, «specie per la breve durata della proibizione» […] «e tenuto conto altresì dell'abbastanza ampio potere del giudice di adeguare (tale) pena […] alla particolarità del caso concreto» (sentenza n. 30 del 1972); 
    che, entro tale contesto, la giurisprudenza costituzionale ha già rimarcato che «tutto il tema relativo alle pene accessorie avrebbe forse bisogno di precisazioni e chiarimenti legislativi e dottrinali»; 
    che, in questa sede, la Corte intende ribadire la necessità che il legislatore ponga mano ad una riforma del sistema delle pene accessorie, che lo renda pienamente compatibile con i principi appena espressi, ed in particolare con l'art. 27, terzo comma, della Costituzione; 
    che, tuttavia, la questione di legittimità costituzionale oggi all'esame della Corte è manifestamente inammissibile; 
    che, infatti, l'addizione normativa richiesta dal giudice a quo non costituisce una soluzione costituzionalmente obbligata, ed anzi eccede i poteri di intervento di questa Corte, impingendo in scelte affidate alla discrezionalità del legislatore; 
    che, in particolare, il potere di differire l'esecuzione della pena accessoria non solo non si profila quale univoca risposta al dubbio di costituzionalità proposto, ma persino denuncia profili eccentrici rispetto ai principi ordinamentali che governano l'esecuzione della sanzione penale, giacché si traduce in un anomala prerogativa del giudice dell'esecuzione di paralizzare sine die l'applicazione di una pena definitivamente inflitta;  
    che, invece, solo il legislatore può determinare forme e condizioni, in presenza delle quali incidere sull'esecuzione della pena accessoria, per adeguarla al principio di progressività del trattamento sanzionatorio penale; 
    che lo stesso rimettente manca di specificare le modalità e i termini entro cui dovrebbe essere esercitato il potere di differimento di cui chiede il riconoscimento; 
    che, pertanto, il petitum formulato dal giudice a quo è inadeguato rispetto al dubbio di costituzionalità proposto, pretendendo egli di operare nel testo dell'art. 139 cod. pen. un'inserzione normativa di cui non vengono definiti i contorni e che non corrisponde al portato di tale disposizione, posto che essa si limita a regolare, nella lettura offertane dalla giurisprudenza, il coordinamento cronologico tra pena principale e pena accessoria, e non i poteri del giudice dell'esecuzione quanto a quest'ultima. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 139 del codice penale, sollevata, in riferimento agli artt. 3 e 27, terzo comma, della Costituzione, dalla Corte di assise di appello di Milano con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 luglio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Ugo DE SIERVO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 18 luglio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
