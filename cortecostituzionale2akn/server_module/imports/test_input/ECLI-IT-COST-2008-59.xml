<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/59/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/59/"/>
          <FRBRalias value="ECLI:IT:COST:2008:59" name="ECLI"/>
          <FRBRdate date="10/03/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="59"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/59/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/59/ita@/!main"/>
          <FRBRdate date="10/03/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/59/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/59/ita@.xml"/>
          <FRBRdate date="10/03/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/03/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Tesauro</cc:relatore_pronuncia>
        <cc:data_decisione>10/03/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 1, comma 65, della legge 28 dicembre 1995, n. 549 (Misure di razionalizzazione della finanza pubblica) e dell'articolo 5-bis del decreto-legge 11 luglio 1992, n. 333 (Misure urgenti per il risanamento della finanza pubblica), convertito, con modificazioni, dalla legge 8 agosto 1992, n. 359, promosso con ordinanza del 27 marzo 1996 dal Tribunale di Avezzano nel procedimento civile vertente tra Carattoli Gabriella ed altre ed il Comune di Massa d'Albe, iscritta al n. 609 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 36, prima serie speciale, dell'anno 2007. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 13 febbraio 2008 il Giudice relatore Giuseppe Tesauro. 
    Ritenuto che il Tribunale ordinario di Avezzano, con ordinanza del 27 marzo 1996, (pervenuta a questa Corte in data 1° agosto 2007) ha sollevato, in riferimento agli artt. 3, 24, 42, terzo comma, e 97, primo e secondo comma, della Costituzione questione di legittimità costituzionale degli artt. 1, comma 65, della legge 28 dicembre 1995, n. 549 (Misure di razionalizzazione della finanza pubblica) e 5-bis del decreto-legge 11 luglio 1992, n. 333 (Misure urgenti per il risanamento della finanza pubblica), convertito, con modificazioni, dalla legge 8 agosto 1992, n. 359; 
    che, nel giudizio principale, alcuni privati hanno dedotto che il Comune di Massa d'Albe ha realizzato un inceneritore su di un appezzamento di terreno di loro proprietà e ne hanno chiesto la condanna al risarcimento del danno subito a causa della occupazione ed irreversibile trasformazione del fondo; 
    che, secondo il rimettente, sussistendo i presupposti dell'accessione invertita, il risarcimento del danno dovrebbe essere quantificato in base al criterio stabilito dal «combinato disposto» dei citati artt. 1, comma 65, e 5-bis (recte: dall'art. 5-bis, comma 6, del decreto-legge n. 333 del 1992, convertito dalla legge n. 359 del 1992, nel testo sostituito dall'art. 1, comma 65, della legge n. 549 del 1995); 
    che, a suo avviso, la norma denunciata, disponendo la retroattività della nuova regola di liquidazione del danno violerebbe l'art. 3 Cost. sotto due profili: in primo luogo, realizzerebbe una ingiustificata discriminazione in danno dei proprietari dei suoli che non hanno percepito il risarcimento del danno in base ad una sentenza passata in giudicato, ovvero di transazione stipulata anteriormente all'entrata in vigore del d.l. n. 333 del 1992; in secondo luogo, non ragionevolmente equiparerebbe l'entità del risarcimento del danno conseguente da una condotta illegittima della Pubblica amministrazione all'indennità di espropriazione; 
    che il citato art. 5-bis, comma 6, si porrebbe altresì in contrasto: con l'art. 24, Cost., in quanto l'irragionevole discriminazione sopra indicata comporterebbe una violazione del diritto di difesa; con l'art. 42, terzo comma, Cost., poiché avrebbe introdotto nell'ordinamento «una sorta di espropriazione di fatto legalizzata», equiparando, non ragionevolmente, accessione invertita ed espropriazione legittima; con l'art. 97, primo e secondo comma, Cost., in quanto l'accessione invertita, in violazione del principio di legalità, permetterebbe di eludere le disposizioni in materia di controlli e di competenze dei Comuni, delle Province, delle Regioni e dello Stato, favorendo il comportamento illecito dei pubblici funzionari; 
    che, nel giudizio innanzi a questa Corte, è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente inammissibile; 
    che, secondo la difesa erariale, la questione non ha ad oggetto il combinato disposto degli artt. 1, comma 65, della legge n. 549 del 1995 e 5-bis del decreto-legge n. 333 del 1992, convertito, con modificazioni, dalla legge n. 359 del 1992, bensì il comma 6 dell'art. 5-bis di detto decreto-legge, introdotto dal citato art. 1, comma 65, dichiarato costituzionalmente illegittimo dalla sentenza n. 369 del 1996, con conseguente manifesta inammissibilità della questione. 
    Considerato che il dubbio di legittimità costituzionale sottoposto a questa Corte ha ad oggetto l'art. 5-bis, comma 6, del decreto legge n. 333 del 1992, convertito dalla legge n. 359 del 1992, nel testo sostituito dall'art. 1, comma 65, della legge n. 549 del 1995, nella parte in cui equiparava la disciplina del risarcimento del danno da accessione invertita alla disciplina concernente la determinazione della indennità dovuta nel caso di espropriazione per pubblica utilità; 
    che, successivamente all'ordinanza di rimessione, questa Corte, con la sentenza n. 369 del 1996, ha dichiarato l'illegittimità costituzionale della norma censurata, nella parte in cui applica al «risarcimento del danno» i criteri di determinazione stabiliti per l'indennità di espropriazione per pubblica utilità; 
    che, dopo questa sentenza, l'art. 3, comma 65, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica) ha introdotto nel citato art. 5-bis il comma 7-bis, il quale ha stabilito una disciplina differenziata del risarcimento del danno rispetto a quella concernente l'indennità di espropriazione per pubblica utilità; 
    che, infine, questa Corte, con sentenza n. 349 del 2007, ha dichiarato l'illegittimità costituzionale del comma 7-bis dell'art. 5-bis del d.l. n. 333 del 1992, aggiunto dall'art. 3, comma 65, della legge n. 662 del 1996, il quale stabiliva il risarcimento del danno subito per effetto dell'occupazione acquisitiva da parte della pubblica amministrazione in misura non corrispondente al valore di mercato del bene occupato; 
    che pertanto, alla stregua di dette pronunce di questa Corte, gli atti devono essere restituiti al Tribunale di Avezzano per un nuovo esame della rilevanza della questione.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    ordina la restituzione degli atti al Tribunale di Avezzano. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 marzo 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giuseppe TESAURO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 13 marzo 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
