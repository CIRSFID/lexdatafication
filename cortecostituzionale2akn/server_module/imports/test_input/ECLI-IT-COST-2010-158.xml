<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/158/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/158/"/>
          <FRBRalias value="ECLI:IT:COST:2010:158" name="ECLI"/>
          <FRBRdate date="28/04/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="158"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/158/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/158/ita@/!main"/>
          <FRBRdate date="28/04/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/158/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/158/ita@.xml"/>
          <FRBRdate date="28/04/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/05/2010" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2010</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maria Napolitano</cc:relatore_pronuncia>
        <cc:data_decisione>28/04/2010</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Francesco AMIRANTE; Giudici : Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale del combinato disposto dell'art. 2 e dell'allegato 1 del decreto-legge 22 dicembre 2008, n. 200 (Misure urgenti in materia di semplificazione normativa), convertito, con modificazioni, dalla legge 18 febbraio 2009, n. 9, promosso dalla Regione siciliana con ricorso notificato l'8 aprile 2009, depositato in cancelleria il 15 aprile 2009 ed iscritto al n. 27 del registro ricorsi 2009.
 Udito nell'udienza pubblica del 23 marzo 2010 il Giudice relatore Paolo Maria Napolitano;
 udito l'avvocato Michele Arcadipane per la Regione siciliana.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che con ricorso notificato il 7 aprile 2009 e depositato il successivo 15 aprile, la Regione siciliana ha promosso questione di legittimità costituzionale - in riferimento agli artt. 14, lettera o), e 15, comma terzo, del regio decreto legislativo 15 maggio 1946, n. 455 (Approvazione dello statuto della Regione siciliana), convertito dalla legge costituzionale 26 febbraio 1948, n. 2, nonché agli artt. 3 e 97 della Costituzione - del combinato disposto dell'art. 2 e dell'allegato 1 del decreto-legge 22 dicembre 2008, n. 200 (Misure urgenti in materia di semplificazione normativa), convertito, con modificazioni, dalla legge 18 febbraio 2009, n. 9;
 che la ricorrente premette che le norme impugnate prevedono l'abrogazione, tra le altre, di numerose leggi concernenti l'istituzione o la variazione dei territori di diversi Comuni siciliani;
 che l'abrogazione di tali disposizioni, secondo la ricorrente, è costituzionalmente illegittima in quanto lesiva delle attribuzioni proprie della Regione siciliana quali risultano garantite dalla Costituzione, dallo statuto speciale e dalle correlate norme di attuazione;
 che l'art. 14, lettera o), dello statuto regionale, infatti, assegna all'Assemblea regionale la competenza legislativa esclusiva nella materia «regime degli enti locali e delle circoscrizioni relative»;
 che l'art. 15 del medesimo statuto ribadisce, al terzo comma, l'attribuzione alla Regione siciliana di tale competenza legislativa esclusiva e dell'«esecuzione diretta in materia di circoscrizione, ordinamento e controllo degli enti locali»;
 che, pertanto, le abrogazioni disposte dalla predetta normativa statale violerebbero le indicate norme statutarie che attribuiscono alla Regione siciliana la competenza esclusiva in materia di circoscrizione e ordinamento degli enti locali, competenza che la Regione ha esercitato sin dal 1948 sia con specifiche disposizioni legislative che con leggi organiche;
 che, sotto altro profilo, risulterebbe leso il principio di ragionevolezza di cui all'art. 3 Cost., vista l'evidente arbitrarietà e contrarietà al pubblico interesse dell'abrogazione di disposizioni che hanno determinato l'ambito territoriale di Comuni o, addirittura, la loro istituzione o ricostituzione;
 che la Regione siciliana ritiene leso anche il principio di buon andamento dell'attività amministrativa di cui all'art. 97 Cost., dal momento che l'abrogazione delle norme sopraelencate comporta sicuri elementi d'incertezza per l'operatività degli enti locali stessi e, quindi, del loro corretto andamento gestionale;
 che il Presidente del Consiglio dei ministri non si è costituito in giudizio;
 che, all'udienza del 23 marzo 2010, a seguito dell'emanazione del decreto legislativo 1° dicembre 2009, n. 179 (Disposizioni legislative statali anteriori al 1° gennaio 1970, di cui si ritiene indispensabile la permanenza in vigore, a norma dell'articolo 14 della legge 28 novembre 2005, n. 246), la difesa della Regione siciliana, ha depositato atto di rinuncia al ricorso.
 Considerato che la Regione siciliana ha promosso questione di legittimità costituzionale del combinato disposto dell'art. 2 e dell'allegato 1 del decreto-legge 22 dicembre 2008, n. 200 (Misure urgenti in materia di semplificazione normativa), convertito, con modificazioni, dalla legge 18 febbraio 2009, n. 9, per violazione degli artt. 14, lettera o), e 15, comma terzo, del regio decreto legislativo 15 maggio 1946, n. 455 (Approvazione dello statuto della Regione siciliana), convertito dalla legge costituzionale 26 febbraio 1948, n. 2, nonché degli artt. 3 e 97 Cost.;
 che, successivamente alla proposizione del ricorso, è intervenuto il decreto legislativo 1° dicembre 2009, n. 179 (Disposizioni legislative statali anteriori al 1° gennaio 1970, di cui si ritiene indispensabile la permanenza in vigore, a norma dell'articolo 14 della legge 28 novembre 2005, n. 246), che ha sottratto all'effetto abrogativo di cui all'art. 2 del d.l. n. 200 del 2008 tutte le disposizioni riguardanti l'istituzione, ricostituzione o variazione dei territori di Comuni siciliani, di cui la Regione lamentava l'abrogazione;
 che, proprio in considerazione dell'entrata in vigore del d.lgs. n. 179 del 2009, il Presidente della Regione siciliana, previa delibera della Giunta regionale in data 18 marzo 2010, ha rinunciato al ricorso, affermando che sono venute meno le ragioni dell'impugnazione;
 che, in mancanza di costituzione in giudizio della parte resistente, ai sensi dell'art. 23 delle norme integrative per i giudizi dinanzi alla Corte costituzionale, la mera rinuncia al ricorso è di per sé idonea a determinare l'estinzione del processo.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
 dichiara estinto il processo.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 28 aprile 2010.&#13;
 F.to:&#13;
 Francesco AMIRANTE, Presidente&#13;
 Paolo Maria NAPOLITANO, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 6 maggio 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
