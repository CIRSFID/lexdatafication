<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/165/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/165/"/>
          <FRBRalias value="ECLI:IT:COST:2006:165" name="ECLI"/>
          <FRBRdate date="05/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="165"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/165/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/165/ita@/!main"/>
          <FRBRdate date="05/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/165/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/165/ita@.xml"/>
          <FRBRdate date="05/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="14/04/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Romano Vaccarella</cc:relatore_pronuncia>
        <cc:data_decisione>05/04/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 38, comma secondo, ultima parte (rectius: ultimo periodo), del codice di procedura civile promosso con ordinanza del 28 gennaio 2005 dal Tribunale di Lamezia Terme, nel procedimento civile vertente tra il Gruppo Parlamentare Lega Padana del Senato della XIV legislatura e Paola Francesco ed altri iscritta al n. 303 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica Italiana n. 24, prima serie speciale, dell'anno 2005. 
      Visti l'atto di costituzione di Paola Francesco, nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 22 marzo 2006 il Giudice relatore Romano Vaccarella.  
      Ritenuto che il Tribunale di Lamezia Terme, con ordinanza del 28 gennaio 2005, ha sollevato questione di legittimità costituzionale, in riferimento agli artt. 3, 24 e 111 della Costituzione, dell'art. 38, comma secondo, ultima parte (rectius: ultimo periodo), del codice di procedura civile, nella parte in cui consente all'attore di aderire in ogni tempo all'eccezione di incompetenza territoriale derogabile, tempestivamente sollevata dal convenuto, con contestuale indicazione del giudice reputato competente; 
      che, in particolare, il rimettente osserva che la norma censurata consente alla controparte, qualora l'eccezione di incompetenza territoriale sia stata proposta tempestivamente e correttamente, di aderire a tale indicazione senza limiti preclusivi, costringendo il giudice, omesso ogni vaglio sull'effettiva competenza del giudice indicato, a disporre la cancellazione della causa dal ruolo, a seguito della quale il giudizio andrà riassunto nei successivi tre mesi; 
      che, in punto di rilevanza della questione – affermata la compatibilità  del meccanismo delineato nell'art. 38, comma secondo, ultima parte (rectius: ultimo periodo), cod. proc. civ. con il giudizio di opposizione a decreto ingiuntivo (come nel caso di specie) –, il giudice a quo osserva che la disposizione impugnata, la quale pure pone precisi limiti temporali per la valida proposizione dell'eccezione di incompetenza territoriale, nulla dice sui tempi entro i quali va manifestata l'adesione della controparte, limitandosi a disciplinare gli effetti che da essa scaturiscono; 
      che, a fronte della lettera della norma, il rimettente rileva che gli è impedita l'interpretazione «sistematica o adeguatrice o costituzionalmente orientata» secondo cui l'adesione – in applicazione del «principio di eventualità», desumibile dagli artt. 180, 183 e 184 cod. proc. civ., nonché dei principi di economia processuale e di tutela del diritto di difesa enunciati negli artt. 111 e 24 della Costituzione – dovrebbe avvenire prontamente ed essere preclusa dall'espressa contestazione iniziale e comunque dall'espletamento dell'udienza di trattazione ex art. 183 cod. proc. civ.; 
      che, conseguentemente, pur avendo, nel caso di specie, la parte opposta inizialmente contestato l'eccezione e pur avendo formalizzato la propria adesione solo durante la pendenza dei termini concessi ex art. 184 cod. proc. civ., dovrebbe essere disposta la cancellazione della causa dal ruolo;  
      che, tuttavia, la disposizione, così interpretata, permetterebbe di subordinare la decisione di adesione all'andamento del giudizio, e di esplicitarla, dopo l'iniziale contestazione, malgrado lo svolgimento della fase istruttoria, con chiara elusione del principio del giusto processo, inteso sia come sua ragionevole durata, sia come parità delle armi dei contendenti;  
      che la circostanza che la parte che ha sollevato l'eccezione di incompetenza territoriale si trovi vincolata a subire l'adesione della controparte fino all'udienza di precisazione delle conclusioni concreterebbe una violazione degli artt. 24 e 111 della Costituzione, così come la previsione di un limite preclusivo assai ristretto per formularla, a fronte della possibilità lasciata alla controparte di aderirvi in ogni tempo, si risolverebbe in una violazione dell'art. 3 della Costituzione, e segnatamente del principio di ragionevolezza, in considerazione della «posizione passiva di soggezione», in cui la prima verrebbe a trovarsi rispetto alla seconda;  
      che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, col patrocinio dell'Avvocatura generale dello Stato, che ha chiesto alla Corte di dichiarare infondata la sollevata questione;  
      che, secondo la difesa erariale, non sarebbe condivisibile l'interpretazione dalla quale è partito il rimettente, dovendosi ritenere che l'adesione all'eccezione di incompetenza territoriale sollevata dalla controparte, dopo l'iniziale contestazione della sua fondatezza, sia soggetta alle preclusioni di cui all'art. 183 cod. proc. civ.,  mentre, «stante la natura di dichiarazione negoziale» dell'eccezione stessa, «la possibilità di revoca, di cui all'art. 183 c.p.c.», sarebbe «di per sé il massimo che può concedersi in via interpretativa»;   
      che, peraltro, in ogni caso, quand'anche il sistema fosse ricostruibile nei sensi indicati dal giudice a quo, i dubbi di costituzionalità sarebbero ugualmente infondati, posto che la parte che eccepisce l'incompetenza territoriale derogabile accetterebbe per ciò solo il rischio di un'istruttoria «resa inutile dalla dichiarazione di adesione», con un risultato tuttavia compensato dallo spostamento del processo nel foro indicato;  
      che, in definitiva, la tutela del diritto di difesa, le esigenze del giusto processo e il criterio della ragionevolezza, di cui agli artt. 24, 111 e 3 della Costituzione, non risulterebbero limitati, ove fosse corretta l'interpretazione del  rimettente, più di quanto non lo siano in una lettura del sistema che neghi la possibilità di rinunciare all'originaria contestazione dell'eccezione di incompetenza;  
      che si è costituito in giudizio l'avvocato Francesco Paola, opposto nel giudizio a quo, chiedendo alla Corte di dichiarare inammissibile la proposta questione di costituzionalità, per manifesta irrilevanza – essendo stato emesso, il provvedimento di sospensione, «in carenza di potere del giudice a quo» – o, in subordine, di ritenerla infondata;  
      che il deducente segnala in particolare come, dopo un'iniziale contestazione, egli avesse manifestato la sua adesione all'eccezione di incompetenza con atto depositato il 24 dicembre 2004, allo scadere dei termini fissati ex art. 184 cod. proc. civ., in un momento cioè in cui l'eccezione non solo non era stata ancora rinunciata, ma era stata anzi ulteriormente ribadita;  
      che il giudice a quo, obliterando completamente tale circostanza essenziale, era incorso in un chiaro error in procedendo, mentre era fuori luogo parlare di qualsivoglia «posizione passiva di soggezione» o di violazione del principio di «parità delle armi» delle parti;  
      che, in ogni caso, alcun pregiudizio potrebbe mai derivare allo svolgimento del processo dalla translatio iudicii, restando comunque immodificato anche innanzi al nuovo giudice il materiale assertivo e probatorio acquisito davanti all'organo giudiziario inizialmente adito;  
      che, in definitiva, la sollevata questione sarebbe irrilevante anche sotto il profilo che l'eventuale declaratoria di incostituzionalità della norma non potrebbe spiegare i suoi effetti nel giudizio a quo, in deroga al disposto dell'art. 5 cod. proc. civ., considerato che la competenza territoriale, in caso di adesione dell'attore all'eccezione di incompetenza sollevata dal convenuto, viene fissata in itinere, in virtù di una fattispecie complessa, «a natura processuale e dispositiva», e che la declaratoria di incostituzionalità «non riguarderebbe direttamente la norma determinativa della competenza, bensì la norma che configura barriere preclusive processuali»;  
      che tali argomentazioni sono state dalla parte ulteriormente ribadite nella memoria depositata ai sensi dell'art. 10 delle norme integrative per i giudizi davanti alla Corte costituzionale;  
      che l'esponente, in particolare, ha segnalato come, posto che lo stesso giudice a quo non dubita del fatto che l'eccezione di incompetenza territoriale possa essere rinunciata dalla parte che l'ha sollevata, non si comprende per quale motivo la manifestazione di adesione dovrebbe intervenire entro termini preclusivi che, ove previsti, porrebbero, essi sì, la controparte in posizione di sostanziale subordinazione rispetto al proponente;  
      che in tale contesto sarebbe evidente l'infondatezza della questione, con riferimento ai parametri di cui agli artt. 24 e 111 della Costituzione;  
      che, quanto alla prospettata lesione dell'art. 3 della Costituzione, la scelta del legislatore di dettare termini stringati al convenuto per sollevare l'eccezione e al giudice per rilevare d'ufficio l'incompetenza, ma di non imporre analoghi vincoli temporali alla controparte per esercitare il suo potere negoziale di adesione, lungi dall'essere irragionevole, costituirebbe piuttosto il corollario della rilevanza accordata all'autonomia dispositiva delle parti in materia, mentre la possibilità di aderire all'eccezione dopo l'iniziale contestazione si legherebbe al fatto che, fino a quando l'eccezione non venga rinunciata, i suoi effetti  «processuali» – ovvero, da un lato, la facoltà della parte di aderirvi, dall'altro, l'obbligo del giudice di pronunciarsi sul punto – devono considerarsi immanenti al processo.  
      Considerato che il Tribunale di Lamezia Terme dubita, in riferimento agli articoli 3, 24 e 111 della Costituzione, della legittimità costituzionale dell'art. 38, comma secondo, ultima parte (rectius: ultimo periodo), cod. proc. civ. nella parte in cui consente all'attore di aderire in ogni tempo all'eccezione d'incompetenza territoriale derogabile ritualmente proposta dal convenuto; 
      che la questione è manifestamente infondata sotto tutti i profili dedotti dal rimettente, in quanto, da un lato, la possibilità per il convenuto di rinunciare all'eccezione esclude in radice – a prescindere da altri rilievi – che egli si trovi in una “posizione passiva di soggezione”, con violazione del principio della “parità delle armi” e, dall'altro lato, la circostanza che il processo “continua” (art. 50 cod. proc. civ.) davanti al giudice la cui competenza scaturisca dall'adesione dell'attore all'indicazione del convenuto esclude ogni lesione del c.d. “principio dell'economia processuale”, la cui più efficace tutela, peraltro, la legge affida al giudice, attribuendogli il potere di delibare la fondatezza della questione di competenza sollevata dal convenuto e di stabilire se essa meriti una pronta decisione ovvero di essere risolta insieme con il merito della causa (art. 187 cod. proc. civ.). 
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 38, comma secondo, ultima parte (rectius: ultimo periodo), del codice di procedura civile, sollevata, in riferimento agli articoli 3, 24 e 111 della Costituzione, dal Tribunale di Lamezia Terme con l'ordinanza in epigrafe.  &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 aprile 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Romano VACCARELLA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 14 aprile 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
