<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/373/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/373/"/>
          <FRBRalias value="ECLI:IT:COST:2003:373" name="ECLI"/>
          <FRBRdate date="17/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="373"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/373/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/373/ita@/!main"/>
          <FRBRdate date="17/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/373/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/373/ita@.xml"/>
          <FRBRdate date="17/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/12/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Fernanda Contri</cc:relatore_pronuncia>
        <cc:data_decisione>17/12/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 5, comma 1, della legge 9 dicembre 1998, n. 431 (Disciplina delle locazioni e del rilascio degli immobili adibiti ad uso abitativo), promosso con ordinanza del 12 febbraio 2003 dal Tribunale di Brescia nel procedimento civile vertente tra Fogliarini Fiorenzo ed altra e Somenzi Elvina, iscritta al n. 264 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 20, prima serie speciale, dell'anno 2003. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 12 novembre 2003 il Giudice relatore Fernanda Contri. 
    Ritenuto che il Tribunale di Brescia, con ordinanza in data 12 febbraio 2003, ha sollevato, in riferimento agli artt. 41, terzo comma, e 42, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 5, comma 1, della legge 9 dicembre 1998, n. 431 (Disciplina delle locazioni e del rilascio degli immobili adibiti ad uso abitativo), nella parte in cui subordina la legittimità di contratti di locazione di natura transitoria alle condizioni e alle modalità definite dal decreto di cui al comma 2 dell'art. 4 della stessa legge; 
    che il rimettente, adito per la convalida di licenza per finita locazione relativamente ad un contratto abitativo di natura transitoria, premette che l'intimata ha eccepito la riconduzione del rapporto alla disciplina ordinaria, ai sensi dell'art. 2, commi 2 e 6, lettere c) e d), del decreto del Ministro dei lavori pubblici 5 marzo 1999, in quanto nel contratto stipulato dalle parti non era stato previsto l'onere per il locatore di confermare, prima della scadenza, i motivi di transitorietà posti a base del contratto stesso e comunque il locatore non aveva adempiuto tale onere, come invece è prescritto dall'art. 2 del citato decreto; 
    che, ad avviso del giudice a quo, la norma impugnata sarebbe in contrasto con gli artt. 41, terzo comma, e 42, secondo comma, della Costituzione, poiché rimette alla normazione secondaria una disciplina che incide sull'autonomia negoziale delle parti, sulla libertà di iniziativa economica e sulla proprietà privata, senza tuttavia indicare le direttrici e i principi entro i quali deve svolgersi l'azione amministrativa, come costantemente affermato dalla giurisprudenza costituzionale in tema di riserva di legge relativa; 
      che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la infondatezza della questione; 
    che, ad avviso della difesa erariale, la riserva di legge relativa sarebbe stata rispettata, poiché l'attività esecutiva della pubblica amministrazione non risulta caratterizzata da discrezionalità assoluta e illimitata, come si desume dalla circostanza che i criteri fissati nel decreto non sono imposti autoritativamente ma costituiscono il frutto di una concertazione attuata con i destinatari stessi della disciplina, secondo il procedimento delineato nell'art. 4 della medesima legge.  
    Considerato che l'ordinanza di rimessione è priva di un'adeguata motivazione sia in relazione ai profili di rilevanza della questione, sia in ordine alla non manifesta infondatezza della prospettata censura; 
    che, quanto al primo aspetto, il rimettente non specifica se il contratto fosse stato stipulato per soddisfare esigenze transitorie dell'uno o dell'altro contraente, né individua quale fosse la parte gravata dall'onere di confermare il permanere delle proprie esigenze transitorie, diverse essendo le conseguenze ai fini della decisione, posto che la riconduzione del contratto alla durata legale si verifica solo nell'ipotesi in cui le esigenze transitorie siano relative al locatore che abbia omesso di dare conferma del loro permanere;  
    che del resto non sono nemmeno indicati gli effetti nel giudizio a quo dell'eventuale caducazione della norma censurata; 
    che, secondo la prospettazione del giudice rimettente, la illegittimità costituzionale della norma impugnata deriverebbe dalla circostanza che la disciplina del contratto transitorio è rimessa a un decreto ministeriale, senza che siano indicati dalla legge principi o direttive; 
    che il rimettente ha tuttavia omesso ogni considerazione in ordine al complesso procedimento, articolato anche attraverso fasi di concertazione collettiva, nel quale si inserisce il decreto ministeriale; onde le prospettate censure risultano prive del necessario sostegno argomentativo; 
    che, in definitiva, le diffuse carenze di motivazione impediscono a questa Corte di esaminare nel merito la questione, la quale, pertanto, va dichiarata manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 5, comma 1, della legge 9 dicembre 1998, n. 431 (Disciplina delle locazioni e del rilascio degli immobili adibiti ad uso abitativo), sollevata, in riferimento agli artt. 41, terzo comma, e 42, secondo comma, della Costituzione, dal Tribunale di Brescia con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 17 dicembre 2003. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Fernanda CONTRI, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 23 dicembre 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
