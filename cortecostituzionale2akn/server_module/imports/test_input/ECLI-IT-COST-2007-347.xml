<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/347/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/347/"/>
          <FRBRalias value="ECLI:IT:COST:2007:347" name="ECLI"/>
          <FRBRdate date="10/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="347"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/347/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/347/ita@/!main"/>
          <FRBRdate date="10/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/347/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/347/ita@.xml"/>
          <FRBRdate date="10/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/10/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Luigi Mazzella</cc:relatore_pronuncia>
        <cc:data_decisione>10/10/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Luigi MAZZELLA, Gaetano SILVESTRI, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 3 della legge 8 marzo 1968, n. 152 (Nuove norme in materia previdenziale per il personale degli Enti locali), promosso con ordinanza del 14 marzo 2006 dal Tribunale di Lecce nel procedimento civile vertente tra Petruzzo Anna Rosa ed altri nella qualità di eredi di Petruzzo Carmine e INPDAP ed altra, iscritta al n. 243 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica, prima serie speciale, edizione straordinaria del 26 aprile 2007. 
      Udito nella camera di consiglio del 26 settembre 2007 il Giudice relatore Luigi Mazzella. 
    Ritenuto che nel corso di un giudizio civile, promosso dai figli di Carmine Petruzzo – dipendente di un ente locale deceduto in corso di servizio –  contro l'Istituto nazionale di previdenza per i dipendenti dell'amministrazione pubblica (INPDAP) e diretto ad ottenere l'accertamento del diritto dei ricorrenti a percepire una quota pari al 60 per cento, da dividersi in parti uguali, dell'indennità premio di servizio dovuta al loro genitore (indennità che l'INPDAP aveva corrisposto in misura integrale a Grazia Merico, coniuge in seconde nozze del Petruzzo), il Tribunale di Lecce ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 3 della legge 8 marzo 1968, n. 152 (Nuove norme in materia previdenziale per il personale degli Enti locali), nella parte in cui non prevede che, laddove con il coniuge superstite concorrano orfani maggiorenni, l'indennità sia ripartita tra di essi secondo le previsioni dell'art. 5, terzo comma, del d. P. R. 29 dicembre 1973, n. 1032 (Approvazione del testo unico delle norme sulle prestazioni previdenziali a favore dei dipendenti civili e militari dello Stato); 
      che il rimettente premette che la norma censurata – la quale reca la disciplina dell'erogazione in forma indiretta dell'indennità premio di servizio – è stata oggetto di numerosi interventi della Corte costituzionale, che ne ha dichiarato l'illegittimità sotto vari aspetti, e che tali pronunce sono state, in parte, conseguenza dell'affermato venir meno di una razionale giustificazione della diversità di disciplina tra l'indennità premio di  servizio prevista per i dipendenti degli enti locali e l'indennità di buonuscita spettante ai dipendenti statali, e, in parte, effetto del superamento del carattere meramente previdenziale delle indennità di fine servizio dei dipendenti pubblici e del riconoscimento della loro concorrente natura di retribuzione differita, la cui devoluzione ereditaria deve essere soggetta alle normali regole successorie; 
      che il giudice a quo afferma che, nel caso sottoposto al suo esame, l'operato dell'INPDAP – il quale ha corrisposto l'intera indennità premio di servizio al coniuge superstite del dipendente – è stato conforme alle previsioni dell'art. 3 della legge n. 152 del 1968 (in base alle quali la prole maggiorenne del dipendente deceduto in attività di servizio ha diritto a percepire l'indennità premio di servizio solamente in mancanza del coniuge superstite) e che però la predetta norma contrasta con l'art. 3 Cost., in considerazione della ingiustificata disparità di trattamento che si verifica rispetto alle stesse categorie di  superstiti di  dipendenti dello Stato, per i quali l'art. 5, terzo comma, del d. P. R. n. 1032 del 1973 stabilisce che, se con il coniuge superstite concorrano più orfani maggiorenni, l'indennità è attribuita nella misura del  40 per cento al coniuge superstite e del 60 per cento, in parti uguali tra loro, agli orfani; 
      che il rimettente aggiunge che, rispetto al coniuge superstite ed ai figli maggiorenni, la norma censurata non subordina il diritto alla corresponsione dell'indennità premio di servizio alla sussistenza di una situazione di effettiva convivenza a carico del lavoratore defunto, sicché deve anche escludersi che, nei rapporti interni tra le suddette categorie di superstiti, la funzione previdenziale dell'indennità possa giustificare l'ordine di precedenza dettato dall'art. 3 della legge n. 152 del 1968. 
    Considerato che il Tribunale di Lecce dubita, in riferimento all'art. 3 della Costituzione, della legittimità dell'art. 3 della legge 8 marzo 1968, n. 152 (Nuove norme in materia previdenziale per il personale degli Enti locali), nella parte in cui non prevede che, laddove con il coniuge superstite concorrano orfani maggiorenni, l'indennità sia ripartita tra di essi secondo le previsioni dell'art. 5, terzo comma, del d. P. R. 29 dicembre 1973, n. 1032 (Approvazione del testo unico delle norme sulle prestazioni previdenziali a favore dei dipendenti civili e militari dello Stato); 
      che il rimettente denuncia la ingiustificata disparità di trattamento che si verificherebbe ai danni degli orfani maggiorenni del dipendente di ente locale deceduto in corso di servizio (i quali, nel caso in cui vi sia anche il coniuge superstite, non concorrono nella distribuzione dell'indennità premio di servizio che viene attribuita integralmente al coniuge), rispetto agli orfani maggiorenni del dipendente statale deceduto anch'esso in corso di servizio (i quali, anche nel caso di coesistenza con il coniuge superstite, percepiscono comunque una quota dell'indennità di buonuscita); 
    che la questione è manifestamente inammissibile, perché, se è consentito che il trattamento di fine rapporto del dipendente può essere sottratto all'asse ereditario per essere devoluto in via preferenziale a soggetti legati al lavoratore da un determinato vincolo familiare, deve essere riconosciuto al legislatore un certo grado di discrezionalità nell'individuare i soggetti beneficiari ed i criteri per distribuire l'emolumento tra di essi; 
    che, al riguardo, non vi è una sola soluzione logicamente necessitata ed implicita nello stesso contesto normativo, essendo invece necessario operare una scelta tra varie opzioni possibili e tutte lecite, come è confermato dal fatto che nell'ordinamento sono rinvenibili plurimi modelli legislativi di erogazione in forma indiretta dei trattamenti di fine rapporto spettanti ai lavoratori dei vari settori pubblici e privati (art. 5 del d. P. R. n. 1032 del 1973, per i dipendenti dello Stato; art. 3 della legge n. 152 del 1968, per i dipendenti degli enti locali; art. 2122 del codice civile, per i dipendenti privati e quelli degli enti parastatali), i quali, con riferimento ai soggetti beneficiari e ai criteri di distribuzione dell'emolumento, presentano differenze tali da non consentire un raffronto circoscritto ad un singolo aspetto; 
      che, con riferimento alla questione sollevata dal rimettente, non possono essere utilmente invocate le sentenze con le quali la Corte ha dichiarato l'illegittimità di norme relative all'indennità di premio servizio a seguito della riscontrata disparità di trattamento rispetto alla disciplina dell'indennità di buonuscita, perché in quei casi le disposizioni dichiarate illegittime comportavano la mancata insorgenza del diritto all'indennità premio di servizio in ipotesi nelle quali invece la disciplina per l'indennità di buonuscita prevedeva la corresponsione dell'emolumento (v. sentenze n. 763 e n. 821 del 1988). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. dell'art. 3 della legge 8 marzo 1968, n. 152 (Nuove norme in materia previdenziale per il personale degli Enti locali), sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale di Lecce con l'ordinanza in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 ottobre 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Luigi MAZZELLA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 19 ottobre 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
