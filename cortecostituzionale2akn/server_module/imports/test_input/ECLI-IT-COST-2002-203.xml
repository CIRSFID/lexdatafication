<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/203/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/203/"/>
          <FRBRalias value="ECLI:IT:COST:2002:203" name="ECLI"/>
          <FRBRdate date="09/05/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="203"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/203/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/203/ita@/!main"/>
          <FRBRdate date="09/05/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/203/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/203/ita@.xml"/>
          <FRBRdate date="09/05/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="16/05/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>09/05/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 453 del codice di procedura penale, promosso, nell'ambito di un procedimento penale, con ordinanza del Tribunale di La Spezia in data 8 giugno 2001, iscritta al n. 694 del registro ordinanze 2001 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 38, prima serie speciale, dell'anno 2001. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 10 aprile 2002 il Giudice relatore Guido Neppi Modona. 
    Ritenuto che il Tribunale di La Spezia, su eccezione della difesa, ha sollevato, in riferimento agli artt. 3 e 24, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 453 del codice di procedura penale, nella parte in cui non prevede che la richiesta di giudizio immediato debba essere preceduta dall'avviso di conclusione delle indagini preliminari ex art. 415-bis cod. proc. pen.;  
    che il Tribunale, premesso che procede per i reati di usura e di estorsione a seguito di decreto di giudizio immediato, rileva che l'avviso di conclusione delle indagini preliminari è previsto soltanto quando il pubblico ministero esercita l'azione penale con richiesta di rinvio a giudizio o con citazione diretta, come emerge dai richiami rispettivamente operati dall'art. 415-bis all'art. 405 e dall'art. 550 all'art. 415-bis cod. proc. pen., mentre analogo richiamo non è contenuto nella disposizione censurata;  
    che, ad avviso del Tribunale, la mancata previsione dell'avviso di conclusione delle indagini prima della richiesta di giudizio immediato determina un'indebita compressione del diritto di difesa e un'ingiustificata disparità di trattamento dell'imputato nei cui confronti si procede con giudizio immediato rispetto all'imputato tratto a giudizio con citazione diretta - prevista per reati di minore gravità - o a seguito di udienza preliminare; 
    che all'imputato nei cui confronti è disposto il giudizio immediato risulta infatti inibito «di presentare memorie, produrre documenti, depositare documentazione relativa alle investigazioni difensive, chiedere al p.m. il compimento di atti d'indagine nonché compiere ogni altra attività difensiva che l'art. 415-bis prevede al fine di consentirgli, laddove è possibile, di non essere tratto a giudizio rappresentando la propria posizione e fornendo ogni elemento a discarico»; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente infondata, in base al rilievo che la disciplina censurata non pregiudica l'esercizio del diritto di difesa in vista della pronuncia di merito; 
    che, inoltre, l'istituto di cui all'art. 415-bis cod. proc. pen. sarebbe incompatibile con la struttura del giudizio immediato, «caratterizzato dalla possibilità connessa a specifiche contingenze processuali di natura probatoria verificate dal giudice, di accelerare oltre modo i tempi del processo», in quanto ne determinerebbe una inevitabile dilatazione temporale;  
    che, infine, ad avviso dell'Avvocatura il procedimento disciplinato dagli artt. 453 e seguenti cod. proc. pen. è già sufficientemente garantito dalla necessità del previo interrogatorio dell'imputato, e quindi gli adempimenti processuali previsti dall'art. 415-bis si risolverebbero in una inutile duplicazione di tutela. 
    Considerato che il rimettente dubita, in riferimento agli artt. 3 e 24, secondo comma, della Costituzione, della legittimità costituzionale dell'art. 453 del codice di procedura penale, in quanto l'omessa previsione dell'avviso della conclusione delle indagini preliminari prima della presentazione della richiesta del pubblico ministero di giudizio immediato precluderebbe all'imputato di compiere le attività difensive menzionate dall'art. 415-bis cod. proc. pen. e determinerebbe così una ingiustificata e deteriore disparità di trattamento rispetto all'imputato rinviato a giudizio a seguito di udienza preliminare o con citazione diretta; 
    che il confronto tra le opportunità di esercizio del diritto di difesa conseguenti all'avviso di conclusione delle indagini preliminari e quelle offerte dalla disciplina del giudizio immediato deve essere condotto tenendo presenti gli specifici presupposti e la peculiare struttura del rito alternativo regolato dagli artt. 453 e seguenti cod. proc. pen.;  
    che, in primo luogo, presupposto del giudizio immediato è l'evidenza della prova, la cui sussistenza, sottoposta al controllo del giudice per le indagini preliminari a norma dell'art. 455 cod. proc. pen., rende superflua la celebrazione dell'udienza preliminare (v. ordinanze n. 276 del 1995 e n. 482 del 1992); 
    che, inoltre, il pubblico ministero può presentare richiesta di giudizio immediato solo se la persona sottoposta alle indagini sia stata interrogata sui fatti da cui emerge l'evidenza della prova, ovvero se - a seguito di invito a presentarsi emesso a norma dell'art. 375, comma 3, secondo periodo, cod. proc. pen. e contenente, oltre la sommaria enunciazione del fatto risultante dalle indagini compiute, l'indicazione degli elementi e delle fonti da cui risulta l'evidenza della prova e l'avvertimento che potrà essere presentata richiesta di giudizio immediato - la persona indagata non sia comparsa, sempre che non abbia addotto un legittimo impedimento o non sia irreperibile; 
    che, ai fini della contestazione del fatto, tali garanzie sono sostanzialmente analoghe a quelle contenute nell'avviso della conclusione delle indagini preliminari, l'unica differenza essendo riscontrabile nel deposito della documentazione delle indagini espletate, previsto dall'art. 415-bis, comma 2, cod. proc. pen., al quale peraltro fa riscontro, ove si ponga mente alla specificità del giudizio immediato, la contestazione verbale degli elementi e delle fonti su cui si basa l'evidenza della prova, richiamata dagli artt. 453 e 375, comma 3, cod. proc. pen.;  
    che, a seguito dell'interrogatorio svolto con l'osservanza di tali forme, la persona sottoposta alle indagini, al fine di contestare l'evidenza della prova e, quindi, di evitare di essere tratta a giudizio, è posta in condizione di esercitare le più opportune iniziative defensionali previste in via generale nel corso delle indagini preliminari, dalla presentazione di memorie e richieste scritte al pubblico ministero alle attività di sollecitazione probatoria e alle investigazioni difensive; 
    che l'art. 415-bis, comma 3, cod. proc. pen., elencando le attività difensive di cui deve essere fatta menzione nell'avviso della conclusione delle indagini preliminari, non riconosce alla persona sottoposta alle indagini poteri di iniziativa diversi e ulteriori rispetto a quelli esercitabili nel corso delle indagini preliminari prima che il pubblico ministero abbia presentato richiesta di giudizio immediato; 
    che sotto il profilo dell'esercizio del diritto di difesa, con particolare riferimento alle attività volte ad evitare il rinvio a giudizio, nella disciplina censurata non è pertanto riscontrabile alcuna violazione dell'art. 24 Cost.;  
    che, per quanto concerne le censure che attengono alla disparità di trattamento, tenuto conto della struttura del processo penale, caratterizzato dalla presenza di una pluralità di riti alternativi che mirano, attraverso la semplificazione dei meccanismi e l'abbreviazione dei tempi del procedimento, a pervenire ad una più rapida conclusione della vicenda processuale, è ragionevole che le forme di esercizio del diritto di difesa siano modulate in funzione delle caratteristiche dei singoli procedimenti speciali (v., ex plurimis, sentenze n. 344 del 1991 e n. 16 del 1970, nonché ordinanze n. 326 del 1999 e n. 432 del 1998); 
    che, in particolare, in tema di giudizio immediato la brevità del termine, giustificata dall'evidenza della prova, entro cui il pubblico ministero deve presentare, ex art. 454, comma 1, cod. proc. pen., la relativa richiesta, comporta la necessità di anticipare le attività difensive volte ad evitare il rinvio a giudizio prima della conclusione delle indagini, e cioè a partire dal momento in cui, grazie all'interrogatorio, alla persona sottoposta alle indagini sono stati contestati i fatti dai quali emerge l'evidenza della prova; 
    che l'estensione al giudizio immediato delle modalità di esercizio del diritto di difesa previste dall'art. 415-bis cod. proc. pen. si porrebbe in antinomia con i presupposti che giustificano la costruzione di questo rito secondo criteri di massima celerità e semplificazione, senza il filtro dell'udienza preliminare, analogamente agli altri procedimenti speciali - giudizio direttissimo e decreto penale di condanna - nei quali, per ragioni diverse, non è previsto l'avviso di conclusione delle indagini;  
    che la questione deve pertanto essere dichiarata manifestamente infondata anche in riferimento all'art. 3 Cost. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 453 del codice di procedura penale, sollevata, in riferimento agli artt. 3 e 24, secondo comma, della Costituzione, dal Tribunale di La Spezia, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 maggio 2002. &#13;
    F.to: &#13;
    Cesare RUPERTO, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 16 maggio 2002. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
