<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/85/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/85/"/>
          <FRBRalias value="ECLI:IT:COST:2010:85" name="ECLI"/>
          <FRBRdate date="24/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="85"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/85/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/85/ita@/!main"/>
          <FRBRdate date="24/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/85/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/85/ita@.xml"/>
          <FRBRdate date="24/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="05/03/2010" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2010</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>24/02/2010</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Francesco AMIRANTE; Giudici : Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 149 del decreto legislativo 7 settembre 2005, n. 209 (Codice delle assicurazioni private), promosso dal Giudice di pace di Arezzo, nel procedimento civile vertente tra M. Y. e la Toro Assicurazioni s.p.a. ed altro, con ordinanza del 23 settembre 2008, iscritta al n. 69 del registro ordinanze 2009 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 10, prima serie speciale, dell'anno 2009.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 16 dicembre 2009 il Giudice relatore Alfio Finocchiaro.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che nel corso di un giudizio promosso per il risarcimento del danno subito da M. Y. in un incidente stradale avvenuto il 26 giugno 2007, il Giudice di pace di Arezzo, con ordinanza depositata il 23 settembre 2008, ha sollevato questione di legittimità costituzionale dell'art. 149 del decreto legislativo 7 settembre 2005, n. 209 (Codice delle assicurazioni private), per violazione degli artt. 3, 24 e 76 della Costituzione;
 che il rimettente riferisce che l'attore ha intrapreso l'azione diretta contro la propria compagnia assicuratrice e che si è costituito in causa il preteso danneggiante, il quale ha chiesto sollevarsi la questione di legittimità costituzionale;
 che, secondo il giudice a quo, in assenza dell'art. 149, comma 6, del d.lgs. n. 209 del 2005, l'azione risarcitoria avrebbe dovuto invece essere esercitata nei confronti del responsabile del danno, soggetto diverso dall'odierna convenuta, e che la stessa conseguenza si avrebbe qualora la norma citata fosse ritenuta in contrasto con la Costituzione;
 che, riguardo alla non manifesta infondatezza della questione, il giudice denuncia: a) il vizio di formazione legislativa, per avere il Consiglio di Stato espresso il parere su uno schema di codice parzialmente diverso da quello poi emanato e privo delle norme relative al risarcimento diretto; b) l'eccesso di delega di cui all'art. 76 Cost., per avere il Governo, introducendo l'azione diretta nei confronti della compagnia di assicurazione del danneggiato, modificato i diritti dei danneggiati, senza che tale facoltà fosse concessa dalla legge delega (legge 29 luglio 2003, n. 229, recante «Interventi in materia di qualità della regolazione, riassetto normativo e codificazione. - Legge di semplificazione 2001»), che in nessun punto autorizzava ad abrogare la normativa in tema di responsabilità per danni dalla circolazione stradale; c) la violazione dell'art. 3 Cost., per irragionevole disparità di trattamento fra danneggiati; d) la violazione dell'art. 24 Cost., perché ai fini della disciplina del risarcimento diretto, il regolamento adottato in base all'art. 150 prevede che le spese accessorie dovute al danneggiato dall'impresa di assicurazione sono solo quelle relative alle consulenze medico-legali, e non anche quelle di assistenza legale stragiudiziale;
 che nel giudizio di legittimità costituzionale è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, deducendo l'inammissibilità e, nel merito, l'infondatezza della questione sollevata.
 Considerato che il Giudice di pace di Arezzo dubita della legittimità costituzionale dell'art. 149 del decreto legislativo 7 settembre 2005, n. 209 (Codice delle assicurazioni private), nella parte in cui disciplina il risarcimento diretto dei danni da circolazione stradale, per violazione dell'art. 76 Cost., per essere stato il decreto legislativo in esame emanato in assenza del parere del Consiglio di Stato, e, inoltre, per aver esorbitato dalla delega contenuta nell'art. 4, comma 1, della legge 29 luglio 2003, n. 229 (Interventi in materia di qualità della regolazione, riassetto normativo e codificazione. - Legge di semplificazione 2001), operando una revisione abrogativa delle norme preesistenti in tema di responsabilità per danni dalla circolazione; dell'art. 3 Cost., per aver creato irragionevole disparità di trattamento fra danneggiati, assoggettati a diversi trattamenti processuali; nonché dell'art. 24 Cost., per aver previsto l'art. 150 del Codice l'introduzione di un regolamento che esclude il rimborso delle spese di assistenza legale stragiudiziale;
 che l'ordinanza del Giudice di pace di Arezzo è priva di qualsiasi riferimento al fatto cui sarebbe applicabile la norma censurata, precisandosi soltanto che l'azione è stata promossa da un soggetto nei confronti della propria compagnia assicuratrice, per il risarcimento dei danni da circolazione di veicoli, e che nel giudizio si è costituito il danneggiante;
 che, sulla base dell'anzidetto rilievo, la questione proposta è manifestamente inammissibile sia per omessa specifica motivazione sulla rilevanza della stessa nel giudizio a quo, sia per omessa descrizione della fattispecie (ex plurimis: ordinanze nn. 201 e 191 del 2009; n. 441 del 2008, tutte in tema di risarcimento diretto);
 che, con riguardo al denunciato contrasto con l'art. 76 Cost., non vi è motivazione alcuna, in relazione al procedimento di formazione legislativa, della necessità di un nuovo parere del Consiglio di Stato su uno schema di decreto legislativo al quale, nell'esercizio della funzione legislativa delegata di «riassetto» della materia, siano state apportate modifiche migliorative che tuttavia non abbiano prodotto radicali mutamenti; 
 che riguardo alla norma regolamentare, cui fa rinvio l'art. 150 dello stesso Codice delle assicurazioni private, che esclude il rimborso al danneggiato delle spese stragiudiziali, la censura, oltre a non essere motivata circa la sua applicabilità nel giudizio a quo, si appunta su una norma sottratta al sindacato di costituzionalità (ordinanza n. 440 del 2008);
 che, infine, il giudice rimettente non ha adempiuto l'obbligo di ricercare una interpretazione costituzionalmente orientata della norma impugnata, nel senso, cioè, che essa si limita a rafforzare la posizione dell'assicurato rimasto danneggiato, considerato soggetto debole, legittimandolo ad agire direttamente nei confronti della propria compagnia assicuratrice, senza peraltro togliergli la possibilità di fare valere i suoi diritti secondo i principi della responsabilità civile dell'autore del fatto dannoso (in questo senso la sentenza n. 180 del 2009);
 che tale interpretazione avrebbe consentito di superare i prospettati dubbi di costituzionalità.
 Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 149 del decreto legislativo 7 settembre 2005, n. 209 (Codice delle assicurazioni private), sollevata, in riferimento agli articoli 3, 24 e 76 della Costituzione, dal Giudice di pace di Arezzo con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 febbraio 2010.&#13;
 F.to:&#13;
 Francesco AMIRANTE, Presidente&#13;
 Alfio FINOCCHIARO, Redattore&#13;
 Maria Rosaria FRUSCELLA, Cancelliere&#13;
 Depositata in Cancelleria il 5 marzo 2010.&#13;
 Il Cancelliere&#13;
 F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
