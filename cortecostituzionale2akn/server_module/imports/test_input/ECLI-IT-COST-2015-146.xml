<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2015/146/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2015/146/"/>
          <FRBRalias value="ECLI:IT:COST:2015:146" name="ECLI"/>
          <FRBRdate date="24/06/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="146"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2015/146/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2015/146/ita@/!main"/>
          <FRBRdate date="24/06/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2015/146/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2015/146/ita@.xml"/>
          <FRBRdate date="24/06/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="09/07/2015" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2015</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>CRISCUOLO</cc:presidente>
        <cc:relatore_pronuncia>Mario Rosario Morelli</cc:relatore_pronuncia>
        <cc:data_decisione>24/06/2015</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alessandro CRISCUOLO; Giudici : Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 104, commi 2 e 3, del decreto legislativo 28 dicembre 2013, n. 154 (Revisione delle disposizioni vigenti in materia di filiazione, a norma dell'articolo 2 della legge 10 dicembre 2012, n. 219), promosso dal Tribunale ordinario di Genova nel procedimento vertente tra B.C.M. e M.F. ed altri, con ordinanza del 15 maggio 2014, iscritta al n. 225 del registro ordinanze 2014 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 52, prima serie speciale, dell'anno 2014.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 24 giugno 2015 il Giudice relatore Mario Rosario Morelli.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Nel corso di un giudizio di petizione di eredità, promosso nel marzo 2011 e relativo ad una successione apertasi nel novembre 2004, l'adito Tribunale ordinario di Genova - rilevato, in premessa, che la pretesa avanzata dall'attore «presuppone il riconoscimento in capo allo stesso dello status di chiamato a pieno titolo alla successione legittima di una parente "naturale" collaterale in quarto grado, ai sensi delle disposizioni di cui agli artt. 74 e 258 C.C.», come innovate dalla legge 10 dicembre 2012, n. 219 (Disposizioni in materia di riconoscimento dei figli naturali), ed applicabili retroattivamente, ai giudizi pendenti, solo in virtù delle disposizioni, sopravvenute in corso di causa, di cui ai commi 2 e 3 dell'art. 104 del decreto legislativo 28 dicembre 2013, n. 154 (Revisione delle disposizioni vigenti in materia di filiazione, a norma dell'articolo 2 della legge 10 dicembre 2012, n. 219) - ha ritenuto, di conseguenza, rilevante, al fine del decidere, e non manifestamente infondata, in riferimento agli artt. 2, 3 e 77 della Costituzione, la questione, che ha perciò sollevato con l'ordinanza in epigrafe, di legittimità costituzionale delle suddette disposizioni del d.lgs. n. 154 del 2013.
 Secondo il rimettente, la normativa censurata contrasterebbe, infatti, con gli artt. 2 e 3 Cost. «sotto il profilo della ragionevolezza [...] e della sua conseguente idoneità a dirigersi verso la generalità dei consociati», a causa della sua portata retroattiva non giustificata dalla natura di norma di interpretazione autentica; e violerebbe, altresì, l'art. 77 Cost. «sotto il profilo del mancato rispetto dei limiti imposti dalla legge delega» [id est: della delega di cui all'art. 2, comma 1, lettera f), della citata legge n. 219 del 2012].
 2.- È intervenuta, in questo giudizio, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha contestato la fondatezza della questione, in ogni suo prospettato profilo.Considerato in diritto1.- L'art. 74 del codice civile - recante la definizione della "parentela" come «il vincolo tra le persone che discendono da uno stesso stipite» - è stato, come è noto, integrativamente modificato dall'art. 1, comma 1, della legge 10 dicembre 2012, n. 219 (Disposizioni in materia di riconoscimento dei figli naturali), a tenore del quale (e per quanto qui rileva) un tal vincolo sussiste «sia nel caso in cui la filiazione è avvenuta all'interno del matrimonio, sia nel caso in cui è avvenuta al di fuori di esso».
 2.- In ossequio al principio generale per cui la legge regolatrice della successione è quella vigente al momento della apertura della stessa - osserva il rimettente - la nuova disciplina della «parentela» avrebbe dovuto applicarsi  «soltanto alle successioni apertesi dopo il 1° gennaio 2013 [data di entrata in vigore della legge n. 219 del 2012], ritenendosi precluso ogni diritto dei parenti "naturali" con riferimento alle successioni [...] apertesi in precedenza».
 Viceversa, lo ius superveniens di cui all'art. 104, commi 2 e 3, del decreto legislativo 28 dicembre 2013, n. 154, recante «Revisione delle disposizioni vigenti in materia di filiazione a norma dell'articolo 2 [id est: della delega di cui all'art. 2] della legge 10 dicembre 2012 n. 219», ha previsto, con specifico riferimento all'azione di petizione di eredità, che gli effetti successori della parentela naturale, di cui al novellato art. 74 cod. civ., siano riferibili anche a successioni apertesi anteriormente alla indicata data del 1° gennaio 2013 ed oggetto di giudizi tutt'ora pendenti al momento dell'entrata in vigore del predetto decreto legislativo.
 Da qui la rilevanza, nel processo a quo (introdotto nel 2011 e relativo a successione risalente al 2004), della questione di legittimità costituzionale dell'art. 104, commi 2 e 3, del d.lgs. n. 154 del 2013, e la sua non manifesta infondatezza, secondo il Tribunale rimettente, in riferimento agli artt. 2 e 3 della Costituzione - per irragionevolezza della deroga apportata in materia al divieto di retroattività della legge - ed all'art. 77 Cost., per eccesso di delega.
 3.- La censura di violazione dell'art. 77 (rectius: 76) Cost. - che, in quanto logicamente preliminare, va esaminata con carattere di priorità - non è fondata.
 3.1.- In linea di principio, va ribadito che l'art. 76 Cost. non riduce la funzione del legislatore delegato ad una mera scansione linguistica delle previsioni stabilite dal legislatore delegante, poiché consente l'emanazione di norme che rappresentino un coerente sviluppo e completamento dei contenuti di indirizzo della delega, nel quadro della fisiologica attività di riempimento che lega i due livelli normativi (ex plurimis, sentenze n. 229 del 2014, n. 98 del 2008, n. 163 del 2000).
 3.2.- Nella specie, l'applicabilità retroattiva, ai giudizi pendenti, del novellato art. 74 cod. civ. - introdotta dall'art. 104, commi 2 e 3, del d.lgs. n. 154 del 2013 - riflette una scelta del legislatore delegato compatibile con la ratio della delega (sub art. 2, comma 1, lettera l, della legge n. 219 del 2012) e in linea con i criteri direttivi della stessa. Tra i quali vi è, infatti, l'espressa previsione che l'obiettivo dell'«adeguamento della disciplina delle successioni e delle donazioni al principio di unicità dello stato di figlio» si realizzi assicurando appunto, «anche in relazione ai giudizi pendenti», una disciplina che consenta «la produzione degli effetti successori riguardo ai parenti anche per gli aventi causa del figlio naturale premorto o deceduto nelle more del riconoscimento e conseguentemente l'estensione delle azioni di petizione di cui agli artt. 533 e seguenti del codice civile».
 Ciò che, del resto, ammette poi lo stesso rimettente, là dove, «dalla lettura dei lavori parlamentari», testualmente desume che «il profilo centrale della scelta del legislatore del 2012 sia la volontà di rimuovere definitivamente, sul piano sostanziale e terminologico, le differenze che residuano nel trattamento successorio dei figli, tenendo conto anche delle situazioni in fase di accertamento, nell'ambito dei giudizi pendenti».
 4.- Anche il sospetto di violazione degli artt. 2 e 3 Cost. è privo di fondamento.
 4.1.- Con riguardo al principio di irretroattività delle leggi in materia civile - sancito dall'art. 11 delle preleggi, e che non ha valenza costituzionale a differenza di quanto espressamente previsto in materia penale dall'art. 25, secondo comma, Cost. - questa Corte ha reiteratamente chiarito come al legislatore non sia, quindi, precluso di emanare norme retroattive (sia innovative che di interpretazione autentica), «purché la retroattività trovi adeguata giustificazione nella esigenza di tutelare principi, diritti e beni di rilievo costituzionale, che costituiscono altrettanti "motivi imperativi di interesse generale" ai sensi della giurisprudenza della Corte EDU» (sentenze n. 156 del 2014 e n. 264 del 2012).
 4.2.- Nella specie, la normativa denunciata dal rimettente è volta alla tutela di un valore di rilievo costituzionale - quello della completa parificazione dei figli naturali ai figli nati all'interno del matrimonio - specificamente riconducibile all'art. 30, primo comma, Cost.: un valore coerente anche al bene della «vita familiare», di cui all'art. 8 della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con legge 4 agosto 1955, n. 848, come interpretato dalla Corte di Strasburgo, nel senso della sua tutelabilità anche con riguardo alla famiglia costruita fuori dal matrimonio (sentenza 13 giugno 1979, Marckx contro Belgio, e successive conformi).
 E ciò, appunto, esclude la violazione dei parametri evocati dal rimettente.
 5.- La questione sollevata è sotto ogni profilo, pertanto, non fondata.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara non fondata la questione di legittimità costituzionale dell'art. 104, commi 2 e 3, del decreto legislativo 28 dicembre 2013, n. 154 (Revisione delle disposizioni vigenti in materia di filiazione, a norma dell'articolo 2 della legge 10 dicembre 2012, n. 219), sollevata, in riferimento agli artt. 2, 3 e 76 della Costituzione, dal Tribunale ordinario di Genova, con l'ordinanza in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 giugno 2015.&#13;
 F.to:&#13;
 Alessandro CRISCUOLO, Presidente&#13;
 Mario Rosario MORELLI, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 9 luglio 2015.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
