<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/341/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/341/"/>
          <FRBRalias value="ECLI:IT:COST:2004:341" name="ECLI"/>
          <FRBRdate date="28/10/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="341"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/341/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/341/ita@/!main"/>
          <FRBRdate date="28/10/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/341/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/341/ita@.xml"/>
          <FRBRdate date="28/10/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/11/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ONIDA</cc:presidente>
        <cc:relatore_pronuncia>Annibale Marini</cc:relatore_pronuncia>
        <cc:data_decisione>28/10/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 4 maggio 1998 relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal senatore Paolo Emilio Taviani nei confronti del senatore Massimo Riva, promosso dalla Corte d'appello di Genova, con ricorso depositato il 13 giugno 2003 ed iscritto al n. 249 del registro ammissibilità conflitti. 
    Udito nella camera di consiglio del 7 luglio 2004 il Giudice relatore Annibale Marini. 
    Ritenuto che con ordinanza del 9 aprile 2003 la Corte d'appello di Genova, sezione terza civile - nel corso di un giudizio di impugnazione avverso la sentenza, emessa dal Tribunale di Genova il 5 ottobre 1996, con la quale il senatore Paolo Emilio Taviani era stato condannato al risarcimento dei danni patiti dal dott. Massimo Riva per talune dichiarazioni asseritamente diffamatorie rese dal primo nel febbraio del 1992 durante un incontro, svoltosi alla presenza della stampa, con «i quadri della Democrazia cristiana di Busalla», nel corso del quale avrebbe fra l'altro affermato che «il caso Gladio è venuto fuori per il complotto di De Benedetti, Scalfari e il miliardario della Sinistra indipendente Riva contro il Presidente Cossiga» - ha sollevato conflitto di attribuzione tra poteri dello Stato nei confronti del Senato della Repubblica, in relazione alla deliberazione del 14 maggio 1998 con la quale l'Assemblea ha dichiarato che i fatti per i quali è in corso detto procedimento riguardano opinioni espresse nell'esercizio delle funzioni parlamentari, come tali insindacabili a norma dell'art. 68, primo comma, della Costituzione;  
    che la Corte ricorrente premette: 
    a) che l'art. 68 della Costituzione «non è affrancazione indiscriminata del parlamentare dalle responsabilità connesse alla violazione del diritto di terzi, ma solo tutela di uno dei mezzi attraverso i quali si estrinseca il mandato parlamentare»; 
    b) che «le frasi pronunciate dal senatore Taviani sembrano collocarsi fuori dal paradigma costituzionale non apparendo connesse al mandato parlamentare»; 
    c) che «la decisione del Senato (….) incide nell'esercizio del potere di azione riconosciuto dall'art. 24 Cost. e nella funzione giurisdizionale attribuita a questo giudice dall'art. 102 della Costituzione»; 
    che, sulla base di tali premesse, la Corte d'appello di Genova solleva conflitto di attribuzione tra poteri dello Stato chiedendo l'annullamento della citata delibera di insindacabilità.   
    Considerato che in questa fase la Corte è chiamata, ai sensi dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, a deliberare esclusivamente se il ricorso sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivo ed oggettivo di un conflitto di attribuzione tra poteri dello Stato, impregiudicata rimanendo ogni definitiva decisione anche in ordine all'ammissibilità; 
    che, quanto al requisito soggettivo, la Corte d'appello di Genova è legittimata a sollevare il conflitto, essendo competente a dichiarare definitivamente, in relazione al procedimento del quale è investita, la volontà del potere cui appartiene, in considerazione della posizione di indipendenza, costituzionalmente garantita, di cui godono i singoli organi giurisdizionali; 
    che analogamente il Senato della Repubblica, che ha deliberato l'insindacabilità delle opinioni espresse da un proprio membro, è legittimato ad essere parte del conflitto, in quanto organo competente a dichiarare definitivamente la volontà del potere che rappresenta;  
    che, per quanto riguarda il profilo oggettivo del conflitto, la Corte ricorrente denuncia la menomazione della propria sfera di attribuzione, garantita da norme costituzionali, in conseguenza dell'adozione, da parte del Senato, di una deliberazione ove si afferma, in modo asseritamente illegittimo, che le opinioni espresse da un proprio membro rientrano nell'esercizio delle funzioni parlamentari e sono, quindi, coperte dalla garanzia di insindacabilità stabilita dall'art. 68, primo comma, della Costituzione; 
    che, pertanto, esiste la materia di un conflitto la cui risoluzione spetta alla competenza della Corte; 
    che, infine, non è ostativa alla ammissibilità del conflitto la circostanza che, nel corso del medesimo giudizio, la Corte d'appello di Genova abbia già investito questa Corte con un precedente ricorso, posto che l'ordinanza n. 266 del 2002 con la quale tale ricorso è stato dichiarato inammissibile risulta motivata con l'assunto che trattavasi di atto con contenuto talmente atipico da non poter «essere propriamente qualificato in termini di ricorso per conflitto di attribuzione tra poteri dello Stato»; 
    che, pertanto, il ricorso del quale è attualmente delibata la ammissibilità deve essere considerato, non la riproposizione di un precedente ricorso per conflitto di attribuzione tra poteri dello Stato, ma l'atto con il quale per la prima volta viene effettivamente sollevato il conflitto.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzione proposto dalla Corte d'appello di Genova, sezione terza civile, nei confronti del Senato della Repubblica con l'atto introduttivo indicato in epigrafe; &#13;
    dispone:  &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza alla ricorrente Corte d'appello;  &#13;
    b) che l'atto introduttivo e la presente ordinanza siano, a cura della ricorrente, notificati al Senato della Repubblica entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere poi depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di venti giorni previsto dall'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 28 ottobre 2004. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Annibale MARINI, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 12 novembre 2004. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
