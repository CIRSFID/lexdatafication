<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/38/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/38/"/>
          <FRBRalias value="ECLI:IT:COST:2017:38" name="ECLI"/>
          <FRBRdate date="11/01/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="38"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/38/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/38/ita@/!main"/>
          <FRBRdate date="11/01/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/38/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/38/ita@.xml"/>
          <FRBRdate date="11/01/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="15/02/2017" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2017</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Aldo Carosi</cc:relatore_pronuncia>
        <cc:data_decisione>11/01/2017</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 17-bis del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), inserito dall'art. 39, comma 9, del decreto-legge 6 luglio 2011, n. 98 (Disposizioni urgenti per la stabilizzazione finanziaria), convertito, con modificazioni, dall'art. 1, comma 1, della legge 15 luglio 2011, n. 111, promosso dalla Commissione tributaria provinciale di Milano, nel procedimento vertente tra G.G. e l'Agenzia delle entrate - Direzione provinciale II di Milano, con ordinanza del 29 luglio 2014, iscritta al n. 114 del registro ordinanze 2016 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 24, prima serie speciale, dell'anno 2016.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio dell'11 gennaio 2017 il Giudice relatore Aldo Carosi.
 Ritenuto che con ordinanza pronunciata il 3 giugno 2013 e depositata il 29 luglio 2014 (reg. ord. n. 114 del 2016), la Commissione tributaria provinciale di Milano ha sollevato, in riferimento agli artt. 3, 24 e 111 della Costituzione, questioni di legittimità costituzionale dell'art. 17-bis del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), inserito dall'art. 39, comma 9, del decreto-legge 6 luglio 2011, n. 98 (Disposizioni urgenti per la stabilizzazione finanziaria), convertito, con modificazioni, dall'art. 1, comma 1, della legge 15 luglio 2011, n. 111;
 che il rimettente riferisce di essere investito del giudizio avverso due avvisi di accertamento, emessi dall'Agenzia delle entrate e notificati il 25 luglio 2012, con i quali si chiede al ricorrente di versare le maggiori imposte accertate in relazione alle annualità 2007-2008, oltre all'importo dovuto a titolo di sanzioni;
 che l'amministrazione finanziaria si è costituita in giudizio eccependo, pregiudizialmente, l'inammissibilità del ricorso per non avere il ricorrente preliminarmente presentato, in violazione dell'art. 17-bis del d.lgs. n. 546 del 1992, il reclamo che tale disposizione obbligatoriamente impone a chi intenda proporre ricorso avverso un atto emesso dall'Agenzia delle entrate con il quale sia richiesto il pagamento di un importo inferiore a ventimila euro, notificato successivamente al 1° aprile 2012; 
 che il ricorrente ha sollevato eccezione di illegittimità costituzionale del menzionato art. 17-bis per violazione degli artt. 3, 24 e 111 Cost.;
 che il rimettente ritiene, innanzitutto, le questioni rilevanti, in quanto alla disposizione censurata occorre riferirsi per verificare l'ammissibilità dei ricorsi;
 che, quanto alla non manifesta infondatezza, dopo aver premesso il contenuto della disposizione censurata, che disciplina il reclamo e la mediazione tributaria, il giudice rimettente afferma la natura amministrativa del primo, come si evincerebbe sia dalla collocazione della disposizione censurata all'interno del Titolo I, Capo II, del d.lgs. n. 546 del 1992, sia dalla formulazione letterale del comma 2 dello stesso art. 17-bis, che indica il reclamo quale condizione di ammissibilità dell'azione, e del successivo comma 9, il quale specifica che, in caso di mancata conclusione del procedimento entro novanta giorni, esso si trasforma in atto introduttivo del giudizio; 
 che la disposizione impugnata lederebbe, innanzitutto, l'art. 3 Cost., sotto il profilo della disparità di trattamento, dal momento che essa si riferisce ingiustificatamente ai soli tributi di competenza dell'Agenzia delle entrate e nel limite dei ventimila euro; 
 che, in particolare, la soglia dei ventimila euro discriminerebbe, in assenza di idonea giustificazione, la situazione dei contribuenti in ragione di un mero dato quantitativo che non troverebbe riscontro in altri istituti del diritto tributario; 
 che la violazione del menzionato parametro risulterebbe anche dal fatto che il contribuente potrebbe invocare la tutela cautelare solo a seguito della proposizione del ricorso giurisdizionale, dal momento che l'art. 47 del d.lgs. n. 546 del 1992 consente di ottenere la sospensione dell'esecuzione dell'atto impugnato solo a condizione che il contribuente si sia costituito nel giudizio sul merito dell'atto stesso, come si evince sia dal comma 1 dell'art. 47, che consente di chiedere detta sospensione con atto separato notificato alle altre parti e depositato in segreteria «sempre che siano osservate le disposizioni di cui all'art. 22», sia dal comma 6 dello stesso art. 47, il quale dispone che, nei casi di sospensione dell'atto impugnato, la trattazione della controversia sia fissata non oltre novanta giorni dalla pronuncia della sospensione;
 che l'esclusione della possibilità di adire con immediatezza la tutela cautelare in relazione alla sola tipologia di contenzioso descritta sarebbe «irrazionale, contraria al principio di uguaglianza e ingiustificata», trattandosi della tutela giurisdizionale di posizioni giuridiche soggettive che «devono essere garantite nei casi nei quali l'atto sia immediatamente esecutivo (avvisi di accertamento disciplinati a seguito dell'art. 29 del d.l. n. 78 del 2010; cartelle esattoriali (art. 36 bis DPR n. 600 del 1973 e art. 54 bis DPR n. 633 del 1972)»;
 che la disposizione censurata lederebbe, inoltre, l'art. 24 Cost., dal momento che limita la possibilità di agire per la tutela dei propri diritti, sino ad escluderla del tutto qualora non venga esperita una preventiva fase amministrativa;
 che, in particolare, questa Corte, sebbene abbia, generalmente, ritenuto legittimo il differimento della possibilità di agire in giudizio in presenza di specifiche necessità, ha tuttavia escluso che, anche in tali casi, il diritto di azione possa essere eccessivamente compresso e, comunque, che l'ammissibilità stessa dell'azione possa essere condizionata al previo esperimento di un rimedio amministrativo; 
 che, in riferimento all'art. 111 Cost., il giudice rimettente deduce in primo luogo che la disposizione censurata comporterebbe un'eccessiva dilatazione dei tempi di introduzione del giudizio tributario, in violazione del principio della ragionevole durata del processo;
 che, inoltre, l'art. 111 Cost. risulterebbe leso, sempre in relazione al principio della ragionevole durata del processo, anche in considerazione della circostanza per cui, decorsi sessanta giorni dalla notifica, gli avvisi di accertamento e le cartelle di pagamento diventano esecutivi ed il contribuente non potrebbe proporre istanza di sospensione in difetto di instaurazione del giudizio (art. 47 del d.lgs. n. 546 del 1992); 
 che la violazione del medesimo parametro è dedotta anche in quanto la disposizione censurata attribuisce il compito di decidere il reclamo e quello di mediatore a una delle parti della controversia (sia pure attraverso apposite strutture diverse e autonome da quelle che curano l'istruttoria degli atti reclamabili), in contrasto con la necessaria terzietà dell'organo al quale detti ruoli - quanto a quello di mediatore, «anche nel diritto comunitario» - devono essere conferiti; 
 che il rimettente sottolinea come la sentenza n. 272 del 2012 di questa Corte in materia di mediazione civile abbia evidenziato che l'istituto della mediazione trova il suo fondamento nel diritto dell'Unione europea come risulta dalle conclusioni adottate dal Consiglio europeo nel maggio del 2000, dal libro verde presentato dalla Commissione nell'aprile del 2002, dalla direttiva n. 2008/52/CE del Parlamento europeo e del Consiglio in data 21 maggio 2008 e dalla Risoluzione del Parlamento europeo del 25 ottobre 2011 (2011/2117-INI), con particolare riferimento al suo paragrafo 31, sesto capoverso;
 che, inoltre, questa Corte ha precisato come dal diritto comunitario si evinca, da un lato, che la finalità deflattiva dell'istituto della mediazione deve essere raggiunta in forza dell'autorevolezza e dell'utilità concreta della mediazione e non con strumenti di obbligatorietà del ricorso ad essa e, dall'altro, che la mediazione deve svolgersi in modo imparziale rispetto alle parti coinvolte (art. 4 della direttiva n. 2008/52/CE);
 che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per l'inammissibilità o la non fondatezza delle questioni sollevate;
 che, in via preliminare, ha evidenziato che, successivamente alla deliberazione ma anteriormente al deposito dell'ordinanza in esame, è intervenuta la sentenza n. 98 del 2014 che ha dichiarato l'incostituzionalità di alcune disposizioni dell'art. 17-bis del d.lgs. n. 546 del 1992 e, in particolare, della norma che dispone la inammissibilità del ricorso per omessa presentazione del reclamo e della mancata previsione della sospensione degli atti impugnati; 
 che, dunque, in relazione a dette disposizioni, la questione deve essere dichiarata inammissibile;
 che parimenti inammissibile sarebbe la questione, sollevata in riferimento agli artt. 3 e 24 Cost., relativa alla mancata previsione della possibilità di sospensione dell'atto impugnato nelle more della procedura di mediazione, per difetto di rilevanza, dal momento che nel giudizio a quo non è stato presentato il reclamo e, pertanto, non si deve fare applicazione della norma censurata;
 che, infine, le ulteriori questioni dovrebbero essere dichiarate non fondate, sulla base di quanto già dichiarato nella sentenza n. 98 del 2014 a riguardo e richiamato nell'atto di intervento.
 Considerato che la Commissione tributaria provinciale di Milano ha sollevato, in riferimento agli artt. 3, 24 e 111 della Costituzione, questioni di legittimità costituzionale dell'art. 17-bis del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), inserito dall'art. 39, comma 9, del decreto-legge 6 luglio 2011, n. 98 (Disposizioni urgenti per la stabilizzazione finanziaria), convertito, con modificazioni, dall'art. 1, comma 1, della legge 15 luglio 2011, n. 111; 
 che questa Corte, con la sentenza n. 98 del 2014, ha dichiarato, in riferimento all'art. 24 Cost., l'illegittimità costituzionale del denunciato art. 17-bis, comma 2, del d.lgs. n. 546 del 1992, nel testo originario, anteriore alla sostituzione dello stesso ad opera dell'art. 1, comma 611, lettera a), numero 1), della legge 27 dicembre 2013, n. 147, recante «(Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge di stabilità 2014)»; 
 che, dunque, la questione sollevata con riferimento a detta disposizione, nella formulazione originaria, in quanto applicabile, ratione temporis, al giudizio a quo, instaurato avverso avvisi di accertamento del 25 luglio 2012 e, quindi, successivamente al 1° aprile 2012, va dichiarata manifestamente inammissibile, risultando, secondo la consolidata giurisprudenza costituzionale, ormai priva di oggetto (ex plurimis, ordinanza n. 208 del 2016);
 che manifestamente inammissibile, per difetto di rilevanza, è anche la questione relativa alla preclusione della tutela cautelare giurisdizionale dal momento che dall'ordinanza di rimessione, così come nei casi già decisi dalla sentenza n. 98 del 2014, si evince che il ricorrente nel giudizio a quo non ha presentato il reclamo previsto dal censurato art. 17-bis, ma ha proposto direttamente ricorso alla Commissione tributaria provinciale, con la conseguenza che è mancata del tutto la fase amministrativa che solo la presentazione del reclamo avrebbe potuto introdurre e, pertanto, il giudice a quo non deve fare applicazione della norma censurata che (in assunto) precluderebbe l'accesso alla tutela cautelare giurisdizionale in una fase, quella amministrativa introdotta dal reclamo, che nella specie, come si è detto, non si è svolta;
 che parimenti manifestamente inammissibile per difetto di rilevanza è la censura inerente la ragionevole durata del processo, dal momento che, anche nel caso in esame, come quelli oggetto della sentenza n. 98 del 2012, non risulta che i ricorrenti abbiano giustificato l'omissione del reclamo con l'argomento che, se avessero formulato detta istanza, la presentazione del reclamo avrebbe comportato una durata del processo non ragionevole; 
 che le ulteriori questioni sono manifestamente non fondate, in quanto sostanzialmente coincidenti con quelle già dichiarate non fondate dalla sentenza n. 98 del 2014; 
 che, in particolare, quanto alla dedotta violazione dell'art. 3 Cost., sotto il profilo della disparità di trattamento e dell'irragionevolezza, in considerazione del fatto che la previsione in esame riguarda solo i contribuenti parti di controversie rientranti nell'àmbito di applicazione dell'impugnato art. 17-bis e non, quindi, tutti gli altri contribuenti (in particolare, quelli che sono parti di controversie relative ad atti emessi da enti impositori diversi dall'Agenzia delle entrate o di controversie relative ad atti emessi da tale Agenzia ma di valore superiore a ventimila euro), va in questa sede ribadito il convincimento per cui il legislatore ha perseguito l'interesse generale alla deflazione del contenzioso tributario in modo ragionevole, prevedendo il rinvio dell'accesso al giudice con riguardo alle liti (quelle nei confronti dell'Agenzia delle entrate) che notoriamente rappresentano il numero più consistente delle controversie tributarie ed, al contempo, a quelle di esse che comportano le minori conseguenze finanziarie sia per la parte privata che per quella pubblica; 
 che, pertanto, la scelta del legislatore, in quanto congrua rispetto alla ratio dell'intervento normativo, è frutto di un corretto esercizio della discrezionalità legislativa, non censurabile né sul piano del diritto alla tutela giurisdizionale, né su quello del rispetto dei princìpi di uguaglianza e di ragionevolezza; 
 che la lesione del menzionato parametro non sussisterebbe neanche sotto il diverso profilo relativo alla previsione dell'obbligatorietà della procedura di reclamo, in presenza di «altri preventivi istituti deflattivi»;
 che, come già evidenziato nella sentenza n. 98 del 2014, è consentito al legislatore di imporre l'adempimento di oneri - in particolare, il previo esperimento di un rimedio amministrativo - che, condizionando la proponibilità dell'azione, ne comportino il differimento, purché gli stessi siano giustificati da esigenze di ordine generale o da superiori finalità di giustizia;
 che il reclamo e la mediazione tributari, col favorire la definizione delle controversie (che rientrino nel menzionato àmbito di applicazione dei due istituti) nella fase pregiurisdizionale introdotta con il reclamo, tendono a soddisfare l'interesse generale sotto un duplice aspetto: da un lato, assicurando un più pronto e meno dispendioso (rispetto alla durata e ai costi della procedura giurisdizionale) soddisfacimento delle situazioni sostanziali oggetto di dette controversie, con vantaggio sia per il contribuente che per l'amministrazione finanziaria; dall'altro, riducendo il numero dei processi di cui sono investite le commissioni tributarie e, conseguentemente, assicurando il contenimento dei tempi ed un più attento esame di quelli residui (che, nell'àmbito di quelli promossi nei confronti dell'Agenzia delle entrate, comportano le più rilevanti conseguenze finanziarie per le parti);
 che, infine, è manifestamente non fondata la dedotta violazione dell'art. 111 Cost., in relazione al principio della terzietà del giudice, per aver la norma censurata attribuito il compito di mediatore a una delle parti della controversia; 
 che, difatti, va ribadita la non conferenza del richiamo alla direttiva 21 maggio 2008, n. 2008/52/CE del Parlamento europeo e del Consiglio, in quanto essa si applica «nelle controversie transfrontaliere, in materia civile e commerciale», con l'espressa esclusione della «materia fiscale, doganale e amministrativa» (art. 1, comma 2), cioè proprio della materia che viene qui in rilievo;
 che, più in generale, va osservato che la mediazione tributaria introdotta dall'impugnato art. 17-bis costituisce una forma di composizione pregiurisdizionale delle controversie basata sull'intesa raggiunta, fuori e prima del processo, dalle stesse parti (senza l'ausilio di terzi), che agiscono, quindi, su un piano di parità;  
 che dunque deve escludersi che un tale procedimento conciliativo, non riconducibile alla mediazione, preprocessuale, il cui esito positivo è rimesso anche al consenso dello stesso contribuente, possa violare il suo diritto di difesa, il principio di ragionevolezza o, tanto meno, il diritto a non essere distolto dal giudice naturale precostituito per legge.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 1, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 1) dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 17-bis, comma 2, del d.lgs. 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), nel testo originario, anteriore alla sostituzione dello stesso ad opera dell'art. 1, comma 611, lettera a), numero 1, della legge 27 dicembre 2013, n. 147, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge di stabilità 2014)», sollevata, in riferimento all'art. 24 della Costituzione, dalla Commissione tributaria provinciale di Milano con l'ordinanza indicata in epigrafe;&#13;
 2) dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 17-bis del d.lgs. n. 546 del 1992, nel testo originario, anteriore alle modificazioni ad esso apportate dall'art. 1, comma 611, lettera a), numero 1, della legge n. 147 del 2013, nella parte in cui precluderebbe ai contribuenti la tutela cautelare giurisdizionale durante la procedura amministrativa introdotta con il reclamo, sollevata, in riferimento all'art. 3 Cost., dalla Commissione tributaria provinciale di Milano con l'ordinanza indicata in epigrafe;&#13;
 3) dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 17-bis del d.lgs. n. 546 del 1992, nel testo originario, anteriore alle modificazioni ad esso apportate dall'art. 1, comma 611, lettera a), numero 1, della legge n. 147 del 2013, sollevata, in riferimento all'art. 111, secondo comma, Cost., dalla Commissione tributaria provinciale di Milano con l'ordinanza indicata in epigrafe; &#13;
 4) dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 17-bis del d.lgs. n. 546 del 1992, nel testo originario, anteriore alle modificazioni ad esso apportate dall'art. 1, comma 611, lettera a), numero 1), della legge n. 147 del 2013, nella parte in cui prevede l'obbligo, per chi intende proporre ricorso avverso atti emessi dall'Agenzia delle entrate e di valore non superiore a ventimila euro, di presentare preliminarmente reclamo, sollevata, in riferimento all'art. 3 Cost., dalla Commissione tributaria provinciale di Milano con l'ordinanza indicata in epigrafe;&#13;
 5) dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 17-bis del d.lgs. n. 546 del 1992, nel testo originario, anteriore alle modificazioni ad esso apportate dall'art. 1, comma 611, lettera a), numero 1, della legge n. 147 del 2013, nella parte in cui non prevede che la mediazione sia svolta da un terzo, sollevata, in riferimento all'art. 111 Cost., dalla Commissione tributaria provinciale di Milano con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 gennaio 2017.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Aldo CAROSI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 15 febbraio 2017.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
