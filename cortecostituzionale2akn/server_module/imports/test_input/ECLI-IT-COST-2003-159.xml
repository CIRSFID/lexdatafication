<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/159/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/159/"/>
          <FRBRalias value="ECLI:IT:COST:2003:159" name="ECLI"/>
          <FRBRdate date="05/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="159"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/159/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/159/ita@/!main"/>
          <FRBRdate date="05/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/159/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/159/ita@.xml"/>
          <FRBRdate date="05/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="09/05/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Gustavo Zagrebelsky</cc:relatore_pronuncia>
        <cc:data_decisione>05/05/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 1, commi 1 e 2, e 2, comma 1, del decreto legislativo 16 marzo 1992, n. 265 (Norme di attuazione dello statuto speciale per il Trentino-Alto Adige in ordine all'insegnamento in lingua tedesca nel conservatorio di musica di Bolzano), promosso con ordinanza del 27 febbraio 2002 dal Tribunale di La Spezia nel procedimento civile vertente tra Carlo Benzi e il Ministero della pubblica istruzione, iscritta al n. 205 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 19, prima serie speciale, dell'anno 2002. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 12 febbraio 2003 il Giudice relatore Gustavo Zagrebelsky. 
    Ritenuto che il Tribunale di La Spezia, in funzione di giudice del lavoro, con ordinanza del 27 febbraio 2002 ha sollevato questione di costituzionalità (a) degli artt. 1, comma 1, e 2, comma 1, del decreto legislativo 16 marzo 1992, n. 265 (Norme di attuazione dello statuto speciale per il Trentino-Alto Adige in ordine all'insegnamento in lingua tedesca nel conservatorio di musica di Bolzano), in riferimento all'art. 3 della Costituzione, e (b) degli artt. 1, commi 1 e 2, e 2, comma 1, dello stesso decreto legislativo n. 265 del 1992, in riferimento agli artt. 3 e 97 della Costituzione; 
    che il rimettente riferisce di essere chiamato a decidere sulla domanda, formulata da un docente della materia «armonia complementare» nei confronti del Ministero della pubblica istruzione [ora, Ministero dell'istruzione, dell'università e della ricerca], rivolta a ottenere l'esecuzione di un provvedimento di trasferimento del docente medesimo presso il conservatorio di musica di Bolzano, trasferimento già disposto dal Ministero convenuto ma poi annullato in via di autotutela; 
    che l'anzidetto annullamento del trasferimento da parte dell'amministrazione si fonda sul disposto degli artt. 1, comma 1, e 2, comma 1, del decreto legislativo n. 265 del 1992, che per talune materie - c.d. «tabellate», cioè elencate nella tabella allegata allo stesso decreto n. 265, tra cui la materia dell'«armonia complementare», equivalente, come precisa il rimettente, a quella della «cultura musicale generale»  -  stabiliscono che il relativo insegnamento «è impartito in lingua italiana ed in lingua tedesca, per classi formate annualmente» (art. 1, comma 1), e che alle cattedre per l'insegnamento delle materie in questione accedono docenti «rispettivamente di madre lingua italiana e di madre lingua tedesca» (art. 2, comma 1); 
    che pertanto, rileva il giudice a quo, secondo la richiamata disciplina, nel conservatorio di musica di Bolzano l'insegnamento di una materia compresa nella tabella allegata al decreto legislativo n. 265 del 1992 è precluso per i docenti che, pur essendo in possesso di una adeguata conoscenza della lingua di insegnamento - come nella specie il ricorrente nel giudizio principale, quanto alla lingua tedesca -, non siano di «madre lingua», potendo essi accedere soltanto all'insegnamento di materie diverse da quelle «tabellate», secondo quanto stabilisce l'art. 1, comma 2, del citato decreto legislativo n. 265; 
    che, tutto ciò premesso, il rimettente solleva una prima questione di legittimità costituzionale relativamente agli artt. 1, comma 1, e 2, comma 1, del decreto legislativo n. 265 del 1992, che nel loro combinato disposto riservano l'insegnamento delle materie indicate nell'allegata tabella ad insegnanti di madre lingua corrispondente a quella d'insegnamento, affermando che il ricorso ad un concetto come quello di «madre lingua», non adeguatamente specificato dallo stesso legislatore né desumibile da altre fonti normative, lascerebbe all'interprete (nel caso di specie, all'amministrazione) il «compito di riempire di significato l'espressione in parola», aprendo in tal modo la via a «differenti e contrastanti soluzioni, suscettibili di condurre a valutazioni diverse per casi sostanzialmente identici e, quindi, a disparità di trattamento [...] a seconda della definizione che dell'espressione in parola l'amministrazione discrezionalmente faccia propria», con conseguente violazione dell'art. 3 della Costituzione; 
    che, d'altra parte, ipotizzando la possibilità di attribuire un significato univoco all'espressione «madre lingua», nel senso di intenderla come «designante [...] colui che è nato in una certa area geografica nella quale si parla una determinata lingua», il rimettente solleva una seconda questione di legittimità costituzionale, relativamente agli artt. 1, commi 1 e 2, e 2, comma 1, del decreto legislativo n. 265 del 1992, poiché tali disposizioni, differenziando i requisiti soggettivi richiesti per l'insegnamento nelle classi di lingua tedesca presso il conservatorio di musica di Bolzano, tra le materie indicate nella tabella (riservate a docenti di «madre lingua» tedesca) e tutte le altre materie (il cui insegnamento può invece essere impartito da docenti che, «nei limiti di quanto occorrente all'insegnamento della disciplina, siano in grado di usare la lingua [...] tedesca»: art. 1, comma 2), si porrebbero in contrasto con i principi di uguaglianza e ragionevolezza e di buon andamento della pubblica amministrazione (artt. 3 e 97 della Costituzione), per l'assenza di un'apprezzabile ragione giustificativa di detta differenziazione, con ingiustificato deteriore trattamento dei docenti non di madre lingua ma che possiedono una adeguata conoscenza della lingua di insegnamento (nella specie, tedesca), e per il risultato di non fare accedere all'insegnamento delle materie «tabellate» docenti di comprovate qualità, con conseguente «possibile impoverimento dell'offerta formativa in danno del buon funzionamento dell'amministrazione oltre che delle esigenze e delle aspettative degli utenti del servizio»; 
    che nel giudizio così introdotto è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per l'infondatezza di entrambe le questioni sollevate. 
    Considerato che il Tribunale di La Spezia, in funzione di giudice del lavoro, solleva (a) in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale degli artt. 1, comma 1, e 2, comma 1, del decreto legislativo 16 marzo 1992, n. 265 (Norme di attuazione dello statuto speciale per il Trentino-Alto Adige in ordine all'insegnamento in lingua tedesca nel conservatorio di musica di Bolzano), «nella parte in cui non viene definito il concetto di “madre lingua”», e (b), in riferimento agli artt. 3 e 97 della Costituzione, ulteriore questione di legittimità costituzionale degli artt. 1, commi 1 e 2, e 2, comma 1, dello stesso decreto legislativo n. 265 del 1992, «nella parte in cui inseriscono nella [...] tabella [...] allegata al decreto alcune materie di insegnamento e non altre tra quelle riservate agli insegnanti di “madre lingua”»;  
    che attraverso la formulazione delle due diverse questioni di legittimità costituzionale il rimettente, da un lato, censura la mancata definizione normativa della nozione giuridica di «madre lingua», ravvisandovi un contrasto con l'art. 3 della Costituzione per la possibile arbitrarietà delle determinazioni dell'amministrazione circa il significato da attribuire a tale nozione, mentre, dall'altro lato, reputa comunque non risolutiva una eventuale definizione di tale concetto, poiché ritiene anche in tale ipotesi contraria ai principi di uguaglianza e di ragionevolezza, nonché al principio di buon andamento dell'amministrazione, una disciplina che, facendo leva sull'anzidetta nozione, riservi talune materie di insegnamento («tabellate») a docenti di madre lingua, escludendo tutti gli altri, anche se in possesso di una adeguata conoscenza della lingua di insegnamento; 
    che le due questioni sollevate, non legate da un rapporto di subordinazione (poiché la seconda non è necessariamente conseguente al rigetto della prima, bensì semplicemente ne prescinde), muovono da premesse tra loro antitetiche, l'una assumendo che la nozione di «madre lingua» sia priva di un contenuto definibile, l'altra invece postulando che lo abbia; 
    che, così impostando le questioni di costituzionalità, il giudice rimettente chiede due distinti interventi correttivi su ambiti normativi diversi della medesima disciplina, demandando a questa Corte l'esercizio di una preliminare scelta interpretativa - circa la possibilità o l'impossibilità di attribuire un significato univoco alla nozione di  «madre lingua» e con essa al criterio di distinzione censurato - che invece spetta allo stesso giudice effettuare; 
    che, per la contraddittorietà intrinseca di tale prospettazione alternativa, e per l'obiettiva incertezza che ne deriva quanto ai termini del dubbio di costituzionalità rimesso al controllo di questa Corte, deve essere dichiarata la manifesta inammissibilità delle questioni in tal modo sollevate (ex plurimis, ordinanze n. 34 del 2003, n. 67 del 2001 e n. 435 del 2000). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale degli artt. 1, commi 1 e 2, e 2, comma 1, del decreto legislativo 16 marzo 1992, n. 265 (Norme di attuazione dello statuto speciale per il Trentino-Alto Adige in ordine all'insegnamento in lingua tedesca nel conservatorio di musica di Bolzano), sollevate, in riferimento agli artt. 3 e 97 della Costituzione, dal Tribunale di La Spezia con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 maggio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Gustavo ZAGREBELSKY, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 9 maggio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
