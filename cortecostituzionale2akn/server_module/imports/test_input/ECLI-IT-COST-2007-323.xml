<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/323/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/323/"/>
          <FRBRalias value="ECLI:IT:COST:2007:323" name="ECLI"/>
          <FRBRdate date="11/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="323"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/323/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/323/ita@/!main"/>
          <FRBRdate date="11/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/323/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/323/ita@.xml"/>
          <FRBRdate date="11/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/07/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>11/07/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 461, comma 3, e 464, comma 1, del codice di procedura penale, promosso con ordinanza del 29 giugno 2004 dal Tribunale di Fermo nel procedimento penale a carico di R.M., iscritta al n. 842 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 44, prima serie speciale, dell'anno 2004. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 20 giugno 2007 il Giudice relatore Giovanni Maria Flick.  
    Ritenuto che, con l'ordinanza in epigrafe, il Tribunale di Fermo ha sollevato, in riferimento agli artt. 3, 24, secondo comma, e 111, secondo e terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 461, comma 3, del codice di procedura penale, «nella parte in cui non prevede la facoltà dell'imputato di chiedere al giudice che ha emesso il decreto penale, in alternativa ai riti speciali, il giudizio ordinario e, quindi, la fissazione dell'udienza preliminare, e correlativamente, dell'art. 464, comma 1, cod. proc. pen., per violazione degli stessi parametri costituzionali, nella parte in cui non prevede che, se l'opponente a decreto penale ha chiesto il giudizio ordinario, o comunque non ha formulato richiesta di riti speciali, il giudice fissi l'udienza preliminare»; 
      che il rimettente premette di dover procedere al giudizio nei confronti di persona imputata del reato di illecita detenzione di sostanze stupefacenti, la quale aveva proposto opposizione a decreto penale di condanna: opposizione cui aveva fatto seguito, a norma degli artt. 464 e 456 cod. proc. pen., l'emissione da parte del giudice delle indagini preliminari del decreto di giudizio immediato e la fissazione dell'udienza dibattimentale, nella quale la difesa dell'imputato sollevava eccezione di legittimità costituzionale;  
      che il giudice a quo, valutata positivamente la rilevanza della questione, evidenzia come la non manifesta infondatezza della medesima debba apprezzarsi, innanzitutto, in relazione a due importanti innovazioni legislative: per un verso, l'introduzione di rilevanti modifiche di natura, struttura e funzione dell'udienza preliminare ad opera della legge 16 dicembre 1999, n. 479 (Modifiche alle disposizioni sul procedimento davanti al tribunale in composizione monocratica e altre modifiche al codice di procedura penale, modifiche al codice penale e all'ordinamento giudiziario, disposizioni in materia di contenzioso civile pendente, di indennità spettanti al giudice di pace e di esercizio della professione forense), che ne hanno accentuato «le connotazioni in senso garantista e la sua coessenzialità al diritto di difesa», sì da configurare - a parere del rimettente - l'esistenza di un vero e proprio “diritto all'udienza preliminare”, quale articolazione del più generale diritto di difesa dell'imputato; per altro verso, l'ampliamento del novero dei reati per i quali può essere emesso decreto penale di condanna, con la conseguente estensione di tale procedimento speciale anche a reati di notevole rilevanza; 
    che, secondo il rimettente, tali innovazioni, innestandosi nella «controversa e mai sopita» questione della generale compatibilità costituzionale del procedimento speciale, renderebbero ancor più evidente il contrasto con l'art. 24 Cost., non essendo consentito all'imputato, a seguito dell'opposizione, l'esperimento di mezzi di difesa propri dei procedimenti ordinari attraverso la celebrazione dell'udienza preliminare, che, in relazione ai reati per i quali non è prevista la citazione diretta ai sensi dell'art. 550 cod. proc. pen., è configurabile quale garanzia processuale coessenziale al più generale diritto di difesa;  
    che infatti - argomenta ancora il rimettente - se l'opposizione ha l'effetto di «porre nel nulla» il decreto penale di condanna, esigenze di «razionalità sistemica e compatibilità costituzionale» imporrebbero di ritenere che, nei casi in cui il decreto venga emesso per reati per i quali è prevista l'udienza preliminare, l'imputato che non intenda accedere ai riti speciali «non possa essere “privato” di tale fase processuale»: effetto che, invece, attualmente  consegue al disposto degli artt. 461, comma 3, e 464, comma 1, cod. proc. pen., in forza del quale - alla stregua di un'interpretazione assolutamente costante -  in assenza di richieste dell'imputato, il procedimento successivo all'opposizione prosegue con le forme del giudizio immediato;  
    che sarebbe altresì violato l'art. 3 Cost., poiché la disciplina censurata determina una «evidente disparità» di trattamento tra l'imputato nei cui confronti è stato emesso decreto penale di condanna e che ad esso si sia opposto e l'imputato rinviato a giudizio nelle forme del giudizio ordinario; infatti, solo quest'ultimo si potrà avvalere di un modulo processuale comprensivo dell'intera serie di garanzie difensive, tra cui, in primo luogo, quella dell'udienza preliminare: con la conseguenza  che, in riferimento all'identica fattispecie di reato ed in presenza di un analogo quadro probatorio d'accusa, l'imputato destinatario del decreto penale rischierebbe la medesima condanna di quello giudicato nelle forme ordinarie, senza godere delle stesse garanzie difensive, peraltro sulla base di una scelta non sindacabile dall'imputato medesimo;  
    che, infine, a parere del giudice a quo, la disciplina oggetto di censura si porrebbe in contrasto con il principio, sancito nell'art. 111, secondo comma, della Carta,  secondo il quale il processo deve avvenire, oltre che nel contraddittorio delle parti, in condizioni di parità tra le medesime: garanzia per l'accusato, questa, che -  assicurata lungo tutto l'arco del procedimento, comprese le fasi iniziali - comporterebbe che egli non debba trovarsi, in alcun caso, in situazioni di svantaggio «tali da comportare il rischio di una decisione fondata su elementi non sottoposti al principio della parità delle armi»; 
    che, in tale ottica, una volta riconosciuto all'accusato il diritto di introdurre nel procedimento ogni elemento idoneo a scongiurare un avventato esercizio dell'azione penale nei suoi confronti, tale diritto non potrebbe non valere anche per il procedimento per decreto: donde la necessità di attribuire all'opponente la facoltà di optare per la progressione ordinaria del procedimento, con udienza preliminare e senza l'imposizione del giudizio immediato;  
    che d'altra parte  - conclude il rimettente - il giudizio immediato conseguente all'opposizione a decreto penale, proprio in quanto processualmente imposto, risulta privo delle garanzie minime ed indefettibili che ordinariamente caratterizzano tale procedimento speciale, costituite dall'interrogatorio dell'imputato e, soprattutto, dall'evidenza della prova: requisito, quest'ultimo, nella specie totalmente assente, potendo il giudice emettere decreto penale «sulla scorta di elementi che rappresentino poco più di un simulacro di prova» ed essendo sufficiente, in realtà, che non risulti dagli atti, incontrovertibilmente, l'innocenza del prevenuto; 
    che nel giudizio di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo perché la questione sia dichiarata manifestamente infondata: e ciò sul rilievo della peculiarità del rito monitorio e, segnatamente, sulla scorta della considerazione che il giudizio immediato conseguente all'opposizione - non fondato sui presupposti di cui all'art. 453, comma 1, cod. proc. pen. - rappresenta una soluzione individuata dal legislatore, nell'ambito di una ragionevole discrezionalità, per ripristinare il contraddittorio processuale nell'ipotesi di contestazione del fondamento del decreto da parte del suo destinatario. 
    Considerato che il Tribunale di Fermo dubita della compatibilità costituzionale, in riferimento agli artt. 3, 24, secondo comma, e 111, secondo e terzo comma, della Costituzione, del disposto dell'art. 461, comma 3, del codice di procedura penale, nella parte in cui non prevede la facoltà dell'imputato, opponente a decreto penale di condanna, di chiedere al giudice, in alternativa ai riti speciali, il giudizio ordinario e, dunque - ove si proceda per reato in relazione al quale non è prevista la citazione diretta - di ottenere la fissazione dell'udienza preliminare;  
    che il rimettente estende tale dubbio, in relazione ai medesimi parametri costituzionali, anche alla disposizione di cui all'art. 464, comma 1, cod. proc. pen., nella parte in cui non prevede che, se l'opponente a decreto penale ha chiesto il giudizio ordinario o comunque non ha formulato richiesta di riti speciali, il giudice fissi l'udienza preliminare; 
    che il nucleo delle doglianze formulate dal giudice rimettente si fonda, peraltro, sulla non condivisibile premessa che il giudizio introdotto a seguito dell'opposizione dell'imputato debba essere - necessariamente ed integralmente - “rispristinatorio” della situazione processuale in cui l'imputato versava prima dell'emissione del decreto penale di condanna: e ciò perchè tale effetto - unico idoneo a garantire, ad avviso del giudice a quo, la compatibilità costituzionale del procedimento speciale - discenderebbe, come affermato da questa Corte, dalla natura  del decreto quale «decisione “preliminare”, destinata ad essere posta nel nulla nel caso di opposizione» ed a svolgere, in tal caso, la mera funzione di informazione dei motivi dell'accusa (v. ordinanza n. 8 del 2003);  
    che, in realtà, la peculiarità del procedimento per decreto si manifesta, del tutto ragionevolmente, anche nelle modalità di introduzione della fase processuale e di investitura della regiudicanda in capo al giudice del dibattimento, proprio perché - come recita la stessa rubrica di una delle norme oggetto di censura, ossia l'art. 464 cod. proc. pen. - si tratta di giudizio pur sempre “conseguente all'opposizione”: sicchè l'introduzione del rito, improntato a presupposti e cadenze diversi ab origine da quelli del modulo processuale ordinario, non può che confermare - anche in ordine alle concrete modalità di instaurazione del giudizio - la specificità propria del procedimento monitorio, configurato quale rito a contraddittorio eventuale e differito, che, una volta instaurato con l'opposizione, assicura nel dibattimento l'integrale attuazione delle garanzie dell'imputato; 
    che, peraltro, la conferma di tale tratto caratteristico è nella costante qualificazione, da parte della giurisprudenza di legittimità, dell'atto di opposizione quale rimedio impugnatorio, destinato ad impedire che il decreto penale di condanna divenga irrevocabile (art. 648, comma 3, cod. proc. pen.); 
    che pertanto - benché il decreto penale di condanna si configuri come una sorta di decisione “preliminare” destinata, in caso di opposizione, ad essere sostituita da una pronuncia resa all'esito del dibattimento - la proposizione dell'opposizione non è idonea ad elidere in toto le peculiarità che contraddistinguono presupposti, finalità e modulazione del rito monitorio e che lo differenziano sensibilmente rispetto al  procedimento ordinario: risultando così erronea la piena assimilazione - posta a base dell'argomentazione del rimettente - tra la posizione dell'imputato che, destinatario di un decreto penale di condanna, abbia proposto opposizione e quella dell'imputato nei cui confronti si procede nelle forme ordinarie; 
    che solo nella prima di tali eventualità, infatti, viene in essere una delibazione giurisdizionale compiuta dal giudice richiesto del provvedimento, la quale si concreta in una penetrante valutazione, in rito e nel merito, di tutti i presupposti che condizionano la adottabilità del decreto (vale a dire: verifica del rispetto delle condizioni formali della richiesta e del relativo termine; affermazione di responsabilità; verifica dell'adeguatezza della pena); 
    che, dunque, se l'opposizione vale certamente a garantire il contraddittorio pretermesso nella fase monitoria - evitando che si determini la conseguenza processuale della irrevocabilità del decreto penale - essa non è idonea, tuttavia, ad eliminare in toto, per tale atto, il suo valore terminativo di una fase processuale ritualmente conclusasi con una statuizione giurisdizionale: così evidenziandosi la piena ragionevolezza del mancato innesto dell'udienza preliminare nella fase successiva all'opposizione, pena l'indebita perequazione tra il “già giudicato” con decreto di condanna e colui nei cui confronti sia stata soltanto esercitata l'azione penale; 
    che proprio la evidenziata non comparabilità tra le situazioni appena richiamate rende prive di fondamento le censure  prospettate dal rimettente: e ciò sia sotto il profilo della  denunciata disparità di trattamento tra imputati e della conseguente violazione del principio di eguaglianza; sia sotto quello della ipotizzata lesione del principio del contraddittorio tra le parti in condizione di parità, non potendosi riconoscere all'opponente, per le ragioni evidenziate, un “diritto alla progressione ordinaria del procedimento”, con udienza preliminare e senza l'imposizione del giudizio immediato; sia, infine, sotto il profilo dell'ipotizzata lesione del diritto di difesa, posto che la specialità di instaurazione del giudizio - lungi dall'incidere, in ragione della mancata previsione dell'udienza preliminare, sulla piena fruibilità delle garanzie in capo all'imputato, pienamente espanse nella successiva fase dibattimentale - risulta ragionevolmente coerente con la specialità del rito monitorio;  
    che pertanto la questione deve essere dichiarata manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 8, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 461, comma 3, e dell'art. 464, comma 1, del codice di procedura penale, sollevata, in riferimento agli artt. 3, 24, secondo comma, e 111, secondo e terzo comma, della Costituzione, dal Tribunale di Fermo con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 luglio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 24 luglio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
