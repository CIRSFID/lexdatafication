<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/280/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/280/"/>
          <FRBRalias value="ECLI:IT:COST:2009:280" name="ECLI"/>
          <FRBRdate date="19/10/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="280"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/280/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/280/ita@/!main"/>
          <FRBRdate date="19/10/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/280/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/280/ita@.xml"/>
          <FRBRdate date="19/10/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="29/10/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>19/10/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Francesco AMIRANTE; Giudici: Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 210 del codice di procedura penale, promosso dal Tribunale di Verbania nel procedimento penale a carico di P.V. ed altra con ordinanza del 6 giugno 2008, iscritta al n. 402 del registro ordinanze 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 52, prima serie speciale, dell'anno 2008. 
      Visto l' atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 7 ottobre 2009 il Giudice relatore Giuseppe Frigo. 
      Ritenuto che, con ordinanza emessa il 6 giugno 2008, il Tribunale di Verbania, in composizione monocratica, ha sollevato, in riferimento agli artt. 3 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 210 del codice di procedura penale, «nella parte in cui non consente al giudice del dibattimento di decidere le forme in cui assumere il dichiarante»; 
      che – secondo quanto viene riferito nell'ordinanza di rimessione – il giudice a quo procede, in sede dibattimentale, nei confronti di due agenti di pubblica sicurezza, imputati del reato di lesioni personali gravi ed aggravate ai sensi dell'art. 61, numero 9), del codice penale, conseguenti alle percosse inferte ad un arrestato; 
    che, dopo l'escussione dei testi a carico – le cui deposizioni avrebbero avvalorato l'ipotesi accusatoria – e l'esame degli imputati, dovrebbe procedersi all'audizione, quali testi della difesa, di alcuni agenti di pubblica sicurezza, colleghi degli imputati; 
      che il rimettente osserva, peraltro, come sia «pacifica», alla luce delle risultanze processuali, la presenza di detti agenti «nei sotterranei della Questura» al momento del fatto: circostanza che – secondo il giudice a quo – avrebbe imposto al pubblico ministero di iscriverli nel registro delle notizie di reato e di sottoporli ad indagini in procedimenti connessi o collegati, aventi ad oggetto le ipotesi alternative di concorso morale o materiale nel delitto per cui si procede, omessa denuncia di reato (art. 361 cod. pen.), omissione di atti d'ufficio o lesioni colpose; 
      che la mancata attivazione in tali sensi dell'organo dell'accusa avrebbe fatto sì che detti soggetti fossero addotti dalla difesa come testimoni: donde la rilevanza della questione; 
      che, quanto alla non manifesta infondatezza, il giudice a quo deduce che, per costante giurisprudenza, in assenza di una pregressa iscrizione della persona da esaminare nel registro di cui all'art. 335 cod. proc. pen., non è consentito al giudice del dibattimento stabilire che la stessa debba essere sentita nelle forme dell'art. 210 cod. proc. pen. (che regola l'esame di persona imputata in un procedimento connesso o di un reato collegato); 
      che il conseguente dubbio di legittimità costituzionale non potrebbe essere, d'altro canto, superato dall'applicazione dell'art. 63 cod. proc. pen.; 
    che tale disposizione prevede, al comma 1 – in ossequio al principio nemo tenetur se detegere – che, ove una persona, sentita come testimone (in dibattimento) o come persona informata sui fatti (nelle indagini preliminari) renda dichiarazioni da cui emergano indizi di reità a suo carico, l'autorità interrogante debba sospendere l'esame e proseguirlo in «forme assistite», sancendo, altresì, l'inutilizzabilità delle dichiarazioni rese dall'esaminato contra se; 
      che il successivo comma 2 regola, per contro, l'ipotesi «patologica» in cui l'autorità esamini senza le garanzie difensive una persona che, sin dall'inizio, avrebbe dovuto essere sentita in qualità di imputato o di persona sottoposta alle indagini: ipotesi nella quale viene sancita l'inutilizzabilità erga omnes delle dichiarazioni rese, quale «deterrente contro la prassi […] di ignorare indizi di reità a carico dell'escusso per avere dichiarazioni negoziate o compiacenti»; 
      che il rimettente ricorda, altresì, come sia controverso in giurisprudenza se – ai fini della sanzione di inutilizzabilità prevista dal citato comma 2 dell'art. 63 cod. proc. pen. – occorra, oltre al «dato sostanziale», anche quello «formale» dell'avvenuta iscrizione (anche successiva) del dichiarante nel registro delle notizie di reato; 
      che, in ogni caso, la norma si limiterebbe a sanzionare a posteriori una violazione, ma non permetterebbe al giudice di stabilire, «ex ante», le forme in cui assumere il dichiarante, in modo «da garantire comunque il diritto all'assunzione di una prova legalmente idonea»; 
      che da ciò deriverebbe la lesione dei principi del «giusto processo» (art. 111 Cost.), sotto un triplice profilo; 
      che risulterebbe compromessa, anzitutto, la terzietà del giudice, giacché quest'ultimo si troverebbe vincolato, nell'espletamento della propria funzione, «dal comportamento patologico di una parte»: il giudice sarebbe infatti costretto, «contrariamente a quel che ritiene», a violare sia il principio nemo tenetur se detegere che quello della incapacità a testimoniare, facendo «giurare un dichiarante che, per la sua valutazione, non dovrebbe giurare»; 
      che verrebbe lesa, altresì, la parità delle parti, in quanto la disciplina censurata attribuirebbe al pubblico ministero, nella fase dibattimentale, un potere non riconosciuto, invece, alla difesa ed il cui uso patologico quest'ultima non potrebbe comunque contrastare; 
      che sarebbe violato, ancora, il diritto dell'imputato a far interrogare le persone a sua difesa: tale diritto sarebbe garantito, difatti, solo formalmente dal «meccanismo» censurato, a fronte del quale il giudice sarebbe costretto al rispetto di «qualifiche formali» della persona da esaminare derivanti «dal comportamento patologico della parte pubblica», salvo poi a dover dichiarare l'inutilizzabilità delle dichiarazioni rese al momento della decisione; 
      che la norma denunciata si porrebbe, da ultimo, in contrasto con l'art. 3 Cost., per difetto di ragionevolezza; 
      che, secondo quanto affermato dalle sezioni unite della Corte di cassazione (viene citata la sentenza 29 novembre 2007-14 febbraio 2008, n. 7208), il coimputato che venga sentito come testimone, anziché nelle forme dell'art. 210 cod. proc. pen., non è punibile per la falsa testimonianza, ai sensi dell'art. 384, secondo comma, cod. pen., indipendentemente dalla ragione per cui ha dichiarato il falso; 
      che da ciò si desumerebbe non soltanto che, ove il giudice dichiari inutilizzabile la testimonianza, il riconoscimento ex post della qualità di coimputato comporta la non punibilità per la falsa testimonianza del dichiarante, ma anche «che la qualità di teste debba essere assunta esclusivamente da chi comunque ha l'obbligo di dire la verità»; 
      che sarebbe pertanto irragionevole che non spetti al giudice che sovraintende alla formazione della prova stabilire, nell'esercizio del suo potere di direzione del dibattimento, in quale veste assumere il dichiarante: e ciò conformemente ai canoni di un processo accusatorio, nel quale primo compito del giudice è dirimere il confronto probatorio delle parti ed evitare che gli esiti dibattimentali siano «deviati extra legem»; 
      che nel giudizio di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata manifestamente infondata. 
      Considerato che il Tribunale di Verbania dubita della legittimità costituzionale, in riferimento agli artt. 3 e 111 della Costituzione, dell'art. 210 del codice di procedura penale, «nella parte in cui non consente al giudice del dibattimento di decidere le forme in cui assumere il dichiarante»: se, cioè, nelle forme – regolate dalla norma censurata – dell'esame della persona imputata in un procedimento connesso o di un reato collegato, anziché come testimone; 
      che il giudice a quo pone a fondamento del dubbio di costituzionalità premesse interpretative intrinsecamente contraddittorie; 
      che il rimettente dà atto che in ordine ai presupposti di applicabilità dell'art. 63, comma 2, cod. proc. pen. – che sancisce l'inutilizzabilità erga omnes delle dichiarazioni rese da persona che avrebbe dovuto essere sentita, sin dall'inizio, in qualità di imputato o di persona sottoposta alle indagini – sussiste un contrasto giurisprudenziale; 
      che nella giurisprudenza di legittimità è, in effetti, ricorrente l'affermazione per cui, ai suddetti fini, non occorre che, al momento delle dichiarazioni, il soggetto abbia già assunto la posizione formale di imputato o di indagato (con l'iscrizione del suo nome nel registro delle notizie di reato): quello che conta è la situazione sostanziale, ossia che egli si trovi di fatto nella relativa condizione, a fronte degli indizi di reità da cui è colpito; 
    che secondo la tesi prevalente, nondimeno, il divieto di utilizzazione non può comunque colpire le dichiarazioni rese al giudice da un soggetto che non abbia mai acquisito, neppure successivamente, la qualità di imputato o di indagato, dal momento che il giudice, a differenza del pubblico ministero, non può attribuire ad alcuno, di propria iniziativa, l'anzidetta qualità; 
    che vi è, però, anche un indirizzo giurisprudenziale contrario, stando al quale la sanzione prevista dall'art. 63, comma 2, cod. proc. pen. prescinderebbe dall'adempimento postumo dell'iscrizione del dichiarante nel registro delle notizie di reato: si tratterebbe, infatti, di adempimento non previsto dalla norma, che finirebbe per far dipendere l'inutilizzabilità delle dichiarazioni irritualmente assunte dalla «resipiscenza» tardiva dell'organo inquirente, col risultato di avallare proprio la prassi illegittima che la disposizione vorrebbe contrastare (quella, cioè, di barattare l'impunità del dichiarante – attribuendogli la veste formale di testimone, anziché di indagato – con compiacenti accuse a carico di terzi); 
    che, nel ricordare tale contrasto di giurisprudenza, il rimettente mostra, in concreto, di aderire al secondo orientamento: le censure di costituzionalità si imperniano, difatti, sull'asserita assenza di strumenti che, nella situazione considerata, consentano al giudice di assumere «una prova legalmente idonea»; 
    che, in particolare, il giudice a quo basa la supposta violazione tanto del diritto dell'imputato di interrogare le persone a sua difesa (art. 111, secondo comma, Cost.), quanto dell'art. 3 Cost., sull'assunto che – sentendo come testi persone raggiunte da indizi di reità, ma non formalmente indagate – il giudice sarebbe poi costretto, in sede di decisione, a ritenere inutilizzabili le loro dichiarazioni (con conseguente vanificazione del mezzo istruttorio); 
    che, in pari tempo, però, il rimettente solleva la questione sul presupposto che, per “diritto vivente”, la disciplina dell'art. 210 cod. proc. pen. – a differenza di quella dell'art. 63, comma 2, dello stesso codice – possa applicarsi solo ove la persona da esaminare abbia formalmente assunto la qualità di imputato o di indagato; 
    che, in tal modo, il giudice a quo non tiene conto del collegamento sistematico esistente tra l'art. 63, comma 2, e gli artt. 197, comma 1, lettere a) e b), e 210 cod. proc. pen.; 
    che, come reiteratamente affermato dalla Corte di cassazione, difatti, l'art. 63, comma 2, cod. proc. pen. attua una tutela anticipata delle incompatibilità con l'ufficio di testimone previste dall'art. 197, comma 1, lettere a) e b), cod. proc. pen. nei confronti dell'imputato in un procedimento connesso o di un reato collegato: incompatibilità che, a loro volta, impongono che l'esame del soggetto avvenga nelle forme dell'art. 210; 
    che, in questa prospettiva, ove si reputi – come mostra di fare il rimettente – che la sanzione di inutilizzabilità prevista dall'art. 63, comma 2, cod. proc. pen. intervenga anche quando il dichiarante non è mai stato iscritto nel registro delle notizie di reato, se ne deve trarre un logico corollario: e, cioè, che il giudice, una volta delibata la sussistenza a carico del soggetto di indizi di reità, non solo può, ma deve astenersi dall'esaminarlo nella veste di testimone, giacché altrimenti darebbe adito proprio alla «patologia» che la norma mira ad evitare (una precisa indicazione in tal senso si rinviene nella sentenza della Corte di cassazione 24 aprile 2007-6 luglio 2007, n. 26258, citata dallo stesso giudice a quo); 
    che, se così è, peraltro – escluso che il contributo all'accertamento del fatto che il soggetto può dare resti completamente “neutralizzato” – detto soggetto non potrebbe essere sentito altrimenti che nelle forme dell'art. 210: dovrebbe valere, cioè, la medesima soluzione applicabile nel caso in cui il teste, non già attinto in precedenza da indizi di reità, renda, nel corso dell'esame dibattimentale, dichiarazioni “autoindizianti” (ipotesi regolata dal comma 1 dell'art. 63, verificandosi la quale il dichiarante andrebbe assunto, per l'appunto, nelle forme dell'art. 210); 
    che, in conclusione, delle due l'una: o si ritiene che la sanzione di inutilizzabilità di cui all'art. 63, comma 2, cod. proc. pen. colpisca anche le dichiarazioni rese al giudice del dibattimento da chi non è mai stato formalmente imputato o indagato, ma allora bisogna concludere che il giudice ha già il potere-dovere di sentire tale soggetto nelle forme dell'art. 210, piuttosto che come testimone; oppure si nega al giudice tale potere-dovere, ma allora bisogna ritenere – con la giurisprudenza dominante – che anche la sanzione di inutilizzabilità prevista dall'art. 63, comma 2, cod. proc. pen. non possa prescindere dalla formale assunzione delle qualità in discorso: conclusione che farebbe peraltro cadere uno dei presupposti fondanti delle censure di costituzionalità formulate dal rimettente; 
    che la combinazione, operata dal giudice a quo, dell'un assunto con l'altro rende, per converso, contraddittorie le premesse ermeneutiche della questione sollevata, la quale va pertanto dichiarata manifestamente inammissibile (sulla manifesta inammissibilità della questione sollevata in termini contraddittori, si vedano, ex plurimis, le ordinanze n. 127 del 2009, n. 427 e n. 218 del 2008). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 210 del codice di procedura penale, sollevata, in riferimento agli artt. 3 e 111 della Costituzione, dal Tribunale di Verbania con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 ottobre 2009.  &#13;
F.to:  &#13;
Francesco AMIRANTE, Presidente  &#13;
Giuseppe FRIGO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 29 ottobre 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
