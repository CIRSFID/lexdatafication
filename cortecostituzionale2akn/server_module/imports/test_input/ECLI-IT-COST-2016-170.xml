<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/170/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/170/"/>
          <FRBRalias value="ECLI:IT:COST:2016:170" name="ECLI"/>
          <FRBRdate date="14/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="170"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/170/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/170/ita@/!main"/>
          <FRBRdate date="14/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/170/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/170/ita@.xml"/>
          <FRBRdate date="14/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/07/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Giancarlo Coraggio</cc:relatore_pronuncia>
        <cc:data_decisione>14/06/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 1, commi 325 e 441, della legge 27 dicembre 2013, n. 147 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2014), promossi dalle Regioni Lombardia e Piemonte con ricorsi notificati il 25 febbraio 2014, depositati in cancelleria il 5 ed il 7 marzo 2014 ed iscritti ai nn. 16 e 20 del registro ricorsi 2014.
 Visti gli atti di costituzione del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 14 giugno 2016 il Giudice relatore Giancarlo Coraggio;
 uditi l'avvocato Giandomenico Falcon per le Regioni Lombardia e Piemonte e l'avvocato dello Stato Wally Ferrante per il Presidente del Consiglio dei ministri.
 Ritenuto che, con ricorsi depositati, rispettivamente, il 5 e il 7 marzo 2014, la Regione Lombardia e la Regione Piemonte hanno impugnato l'art. 1, commi 325 e 441, della legge 27 dicembre 2013, n. 147 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2014), in riferimento agli artt. 1, 3, 5, 81, 97, 114, 117, primo comma (in relazione all'art. 3 della Carta europea dell'autonomia locale, firmata a Strasburgo il 15 ottobre 1985, ratificata e resa esecutiva con legge 30 dicembre 1989, n. 439), 136 della Costituzione e VIII disposizione transitoria e finale Cost., nonché in relazione all'art. 15, comma 2, e 21 della legge 24 dicembre 2012, n. 243 (Disposizioni per l'attuazione del principio del pareggio di bilancio ai sensi dell'articolo 81, sesto comma, della Costituzione);
 che, con atti depositati il 4 aprile 2014, si è costituito nei rispettivi giudizi il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che i ricorsi siano dichiarati inammissibili o infondati; 
 che, in data 5 gennaio 2016, le Regioni ricorrenti hanno depositato memoria, segnalando che, dopo la presentazione del ricorso, è entrata in vigore la legge 7 aprile 2014, n. 56 (Disposizioni sulle città metropolitane, sulle province, sulle unioni e fusioni di comuni), e ribadendo l'incostituzionalità delle disposizioni censurate;
 che, nella medesima data, il Presidente del Consiglio dei ministri ha depositato, in entrambi i giudizi, memoria, asserendo che il mutato quadro normativo confermava la natura transitoria della normativa impugnata e la sua legittimità ed insistendo sulle proprie conclusioni rassegnate nell'atto di costituzione;
 che, successivamente, le Regioni Lombardia e Piemonte, con atti depositati nella cancelleria di questa Corte, rispettivamente, in data 1° e 7 giugno 2016, hanno rinunciato, ciascuna, al proprio ricorso;
 che in data 13 giugno 2016, per entrambi i ricorsi è stata depositata accettazione della rinuncia da parte del Presidente del Consiglio dei ministri, come da delibera del Consiglio dei ministri del 10 giugno 2016. 
 Considerato che, i ricorsi, aventi ad oggetto le medesime disposizioni, vanno riuniti;
 che con riguardo alle questioni proposte vi è stata rinuncia da parte delle Regioni ricorrenti e accettazione da parte del Presidente del Consiglio dei ministri; 
 che, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, la rinuncia al ricorso, seguita dall'accettazione della controparte costituita, comporta l'estinzione del processo (ex multis, ordinanze n. 119 e n. 35 del 2016, n. 93, n. 79 e n. 73 del 2015).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 riuniti i giudizi, &#13;
 dichiara estinti i processi. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 14 giugno 2016. &#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Giancarlo CORAGGIO, Redattore&#13;
 Carmelinda MORANO, Cancelliere&#13;
 Depositata in Cancelleria il 13 luglio 2016.&#13;
 Il Cancelliere&#13;
 F.to: Carmelinda MORANO</block>
    </conclusions>
  </judgment>
</akomaNtoso>
