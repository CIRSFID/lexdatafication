<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/68/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/68/"/>
          <FRBRalias value="ECLI:IT:COST:2004:68" name="ECLI"/>
          <FRBRdate date="09/02/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="68"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/68/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/68/ita@/!main"/>
          <FRBRdate date="09/02/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/68/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/68/ita@.xml"/>
          <FRBRdate date="09/02/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/02/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Franco Bile</cc:relatore_pronuncia>
        <cc:data_decisione>09/02/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 1, commi 260 e 261, della legge 23 dicembre 1996, n. 662 (Misure per la razionalizzazione della finanza pubblica), promosso con ordinanza del 30 marzo 2001 dal Tribunale di Viterbo nel procedimento civile vertente tra Angelica Pianeselli e l'Istituto nazionale per la previdenza sociale (INPS), iscritta al n. 503 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 47, prima serie speciale, dell'anno 2002. 
    Visti gli atti di costituzione di Angelica Pianeselli e dell'INPS; 
    udito nella camera di consiglio del 17 dicembre 2003 il Giudice relatore Franco Bile.   
    Ritenuto che con ordinanza del 30 marzo 2001 (pervenuta il 31 ottobre 2002) il Tribunale di Viterbo ha sollevato d'ufficio, nel procedimento civile vertente tra Angelica Pianeselli e l'INPS, questione di legittimità costituzionale dell'art. 1, commi 260 e 261, della legge 23 dicembre 1996, n. 662 (Misure per la razionalizzazione della finanza pubblica), per contrasto con gli artt. 3 e 38 della Costituzione;  
    che, secondo il giudice rimettente, il regime dell'indebito in materia previdenziale, introdotto dalle norme censurate, esclude - nei confronti dei soggetti che prima del 1° gennaio 1996 abbiano percepito indebitamente prestazioni pensionistiche - il recupero dell'indebito se i soggetti medesimi, ove non versino in dolo, siano percettori, per l'anno 1995, di un reddito personale imponibile ai fini dell'IRPEF di importo pari o inferiore a lire 16.000.000, mentre il recupero avviene nei limiti dell'indebito per i percettori di reddito superiore;  
    che, così interpretata, la normativa non si sottrae, secondo il giudice rimettente, a sospetti di incostituzionalità, sussistendo disparità di trattamento tra pensionati a favore dei quali, in applicazione della previgente disciplina dell'indebito previdenziale, è stata sancita l'irripetibilità delle somme percepite in buona fede, e pensionati soggetti invece alla nuova disposizione, nonostante la percezione dell'indebito si sia verificata prima della data di entrata in vigore della normativa stessa; 
    che la nuova disciplina, incidendo sulle situazioni sostanziali maturate nella vigenza di quella precedente, frustra l'affidamento di una vasta categoria di cittadini nella sicurezza giuridica che costituisce elemento fondamentale dello Stato di diritto;  
    che - secondo il giudice rimettente - sarebbero danneggiati pensionati a reddito non elevato, i quali, avendo già destinato le somme percepite ai bisogni alimentari propri e della famiglia, dovrebbero non di meno restituirle, sicché si determinerebbe una situazione di insufficiente protezione sociale con conseguente violazione dell'art. 38 Cost.; 
    che la finalità di contrazione della spesa pubblica sottesa alla disposizione in esame non costituisce ragione sufficiente a giustificare le violazioni dei suddetti precetti costituzionali;  
    che si è costituita la Pianeselli aderendo alle prospettazioni dell'ordinanza di rimessione e quindi chiedendo la declaratoria di incostituzionalità della disposizione censurata; 
    che si è costituito l'INPS chiedendo in via principale che gli atti siano restituiti al giudice rimettente per jus superveniens (art. 38, commi 7-10, della legge 28 dicembre 2001, n. 448) ed in subordine concludendo per la declaratoria di inammissibilità della questione. 
    Considerato che dopo la pronuncia dell'ordinanza di rimessione il legislatore ha introdotto, in materia di ripetizione di indebito previdenziale, una nuova disciplina, contenuta nell'art. 38, commi 7, 8, 9 e 10, della legge 28 dicembre 2001, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2002); 
    che - come già affermato da questa Corte (ordinanza n. 249 del 2002) - tale disciplina contiene norme, in ordine alla ripetizione di indebito previdenziale, in parte non coincidenti con quelle oggetto del presente giudizio e tali da poter portare anche ad una riconsiderazione della natura transitoria o meno degli effetti sulle ripetizioni di indebito pregresso; 
    che, pertanto, si rende necessaria la restituzione degli atti al giudice rimettente, cui spetta valutare se, alla luce tanto della legislazione sopravvenuta quanto del mutamento del quadro normativo, la questione sollevata sia tuttora rilevante per la definizione del giudizio a quo e se persistano, in tutto o in parte, i motivi posti a base dell'ordinanza di rimessione.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    ordina la restituzione degli atti al Tribunale di Viterbo. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 febbraio 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Franco BILE, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 12 febbraio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
