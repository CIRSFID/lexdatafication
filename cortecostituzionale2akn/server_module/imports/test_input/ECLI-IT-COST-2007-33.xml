<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/33/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/33/"/>
          <FRBRalias value="ECLI:IT:COST:2007:33" name="ECLI"/>
          <FRBRdate date="24/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="33"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/33/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/33/ita@/!main"/>
          <FRBRdate date="24/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/33/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/33/ita@.xml"/>
          <FRBRdate date="24/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="09/02/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Gaetano Silvestri</cc:relatore_pronuncia>
        <cc:data_decisione>24/01/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 420-quater, comma 1, del codice di procedura penale promosso con ordinanza del 16 luglio 2004 dal Tribunale di S. Maria Capua Vetere, iscritta al n. 870 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 45, prima serie speciale, dell'anno 2004. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 10 gennaio 2007 il Giudice relatore Gaetano Silvestri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. – Con ordinanza emessa il 16 luglio 2004 il Tribunale di S. Maria Capua Vetere ha sollevato, in riferimento agli artt. 3, 25 e 112 della Costituzione, questione di legittimità costituzionale dell'art. 420-quater, comma 1, del codice di procedura penale, nella parte in cui «non prevede espressamente l'obbligo del giudice di dichiarare, lì dove prevista dalle norme pattizie, la contumacia dell'imputato estradato per fatti diversi e anteriori alla sua consegna». 
    Il rimettente procede nei confronti di un imputato rinviato a giudizio dal giudice dell'udienza preliminare del Tribunale di Napoli con decreto in data 27 gennaio 2001, successivamente tratto in arresto, mentre si trovava in Polonia, per fatti diversi da quelli oggetto del giudizio principale, e infine estradato e consegnato alle autorità italiane per essere giudicato nell'ambito di procedimenti diversi. L'ipotesi accusatoria sottoposta al rimettente concerne fatti commessi in territorio italiano nel corso dell'anno 1996, e quindi anteriori al momento della consegna dell'imputato alle autorità italiane, avvenuta nell'anno 2004. 
    Il giudice a quo precisa di non poter promuovere la procedura di estradizione suppletiva, con riferimento ai fatti contestati nel giudizio principale, per mancanza di «misura cautelare eseguibile» nei confronti dell'imputato (l'ordinanza di custodia cautelare in carcere emessa dal Giudice per le indagini preliminari del Tribunale di Napoli, in data 13 marzo 2000, risulta annullata dal Tribunale del riesame in data 18 aprile 2000). 
    Tanto premesso in fatto, il rimettente osserva come nei confronti dell'imputato debba trovare applicazione la clausola di specialità prevista sia dall'art. 14, paragrafo 1, della Convenzione europea di estradizione, resa esecutiva in Italia con la legge 30 gennaio 1963, n. 300, sia dall'art. 7 della Convenzione di estradizione tra Italia e Polonia, resa esecutiva con la legge 7 giugno 1993, n. 193. 
    Il Tribunale si sofferma, quindi, sulla interpretazione della disposizione contenuta nell'art. 14 della citata Convenzione europea fornita dalla Corte di Cassazione a sezioni unite (sentenza n. 8 del 2001), secondo la quale, in assenza di estradizione per i fatti oggetto del giudizio in corso, l'imputato estradato per fatti diversi non può essere giudicato, in quanto la clausola di specialità introdurrebbe una condizione di procedibilità, con conseguente obbligo per il giudice di emettere sentenza di non luogo a procedere, ai sensi degli artt. 129 e 529 cod. proc. pen. 
    A parere del giudice a quo, il consolidato indirizzo interpretativo, formatosi a seguito della decisione citata, introdurrebbe un limite oggettivo all'esercizio della giurisdizione di cognizione, non giustificato dal tenore della previsione pattizia, limite dal quale deriverebbe la violazione dei principi di uguaglianza, legalità e obbligatorietà dell'esercizio dell'azione penale. 
    In particolare, il rimettente osserva come la disciplina pattizia non risulti in sé ostativa alla celebrazione di un giudizio di «mero accertamento», da attuarsi nelle forme del procedimento contumaciale, finalizzato quanto meno alla interruzione della prescrizione, ferma restando l'ineseguibilità dell'eventuale sentenza di condanna in assenza di estradizione suppletiva.  
    Tuttavia, osserva il giudice a quo, la giurisprudenza di legittimità citata ha escluso l'utilizzabilità del procedimento contumaciale sul rilievo che esso si risolverebbe in una ingiustificata menomazione del diritto di difesa dell'imputato estradato in relazione a fatti diversi, ritenendo che non possa neppure esserne disposta la citazione a giudizio al fine di ottenere il suo consenso all'estensione della estradizione, atteso che tale consenso deve essere manifestato con dichiarazione «espressa e non univoca». 
    A fronte di tali affermazioni, secondo il Tribunale rimettente non vi sarebbe spazio per percorsi interpretativi alternativi, pure delineati da pronunce successive (è richiamata Cass., sent. n. 15093 del 2002), e si renderebbe necessario l'intervento «chiarificatore» del giudice delle leggi, stante il «manifesto contrasto tra le disposizioni pattizie, la previsione contenuta nell'art. 420-quater, cod. proc. pen. – come interpretata dalla consolidata giurisprudenza di legittimità – ed il principio costituzionale di necessaria effettività della giurisdizione penale», affermato quest'ultimo in numerose pronunce (è citata la sentenza di questa Corte n. 353 del 1996). 
    A parere del giudice a quo, l'esistenza del bene di rilievo costituzionale della «efficienza del processo» renderebbe intollerabile la situazione di sostanziale «improcessabilità» del cittadino italiano rifugiatosi in territorio estero e rientrato in Italia a seguito di estradizione, con riguardo ai fatti diversi da quelli per i quali è concessa l'estradizione medesima, e ciò anche nei limiti del mero accertamento dell'ipotesi accusatoria formulata a suo carico. 
    Andrebbe quindi verificata, sempre secondo il rimettente, la compatibilità con il dettato costituzionale dell'omessa esplicazione, nella norma processuale che disciplina l'istituto della contumacia, dei riferimenti delle norme pattizie alla possibilità del ricorso al giudizio contumaciale, quanto meno a fini interruttivi del decorso della prescrizione.  
    Da tale lacuna normativa, infatti, discenderebbe la legittimazione dell'orientamento interpretativo restrittivo, e ciò sarebbe confermato dai richiami comparatistici contenuti nella più volte richiamata sentenza n. 8 del 2001 delle sezioni unite della Corte di Cassazione, nella quale sono evidenziate le rilevanti differenze strutturali tra il procedimento disciplinato dagli artt. 420 e ss. cod. proc. pen., e il modello processuale di giudizio contumaciale previsto in altri Paesi aderenti alla Convenzione, con particolare riguardo alla «definitività» della pronuncia emanata in esito a tale giudizio. 
    Inoltre, la mancanza, nell'ordinamento interno, di una disposizione che imponga al giudice di procedere in contumacia nei confronti dell'imputato estradato per fatti diversi, determinerebbe la violazione del parametro costituzionale dell'art. 3, per la situazione di irragionevole privilegio – consistente nella sostanziale «immunità ed improcessabilità» – di cui verrebbe a beneficiare il cittadino italiano sottrattosi volontariamente all'applicazione del principio di territorialità, e successivamente estradato, sia pure in relazione a fatti diversi, rispetto al cittadino il quale, essendo rimasto sul territorio dello Stato, abbia reso possibile lo svolgimento del processo nei suoi confronti. 
    Quanto alla rilevanza della questione, il rimettente rappresenta che soltanto l'eventuale declaratoria di illegittimità costituzionale della norma impugnata, nei termini invocati, consentirebbe la prosecuzione del processo a carico dell'imputato, previa dichiarazione di contumacia dello stesso.  
    2. – Con atto depositato il 7 dicembre 2004, si è costituito in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente infondata. 
    La difesa erariale condivide la ricostruzione operata dalla giurisprudenza di legittimità, secondo cui la clausola di specialità prevista dall'art. 14, paragrafo 1, della Convenzione europea di estradizione introduce una condizione di procedibilità, con la conseguenza che è inibito l'esercizio dell'azione penale per fatti diversi da quelli per cui è stata concessa l'estradizione, commessi prima della consegna. 
    Peraltro, tale ricostruzione non impedisce il compimento degli atti di indagine necessari ad assicurare le fonti di prova, eventualmente mediante il ricorso all'incidente probatorio, né l'emissione di provvedimenti restrittivi della libertà personale (è richiamata Cass., sent. n. 24627 del 2004). 
    Conclusivamente l'Avvocatura ritiene che la normativa attuale, come interpretata dalla giurisprudenza prevalente, in quanto consente sia atti di accertamento del fatto, sia atti interruttivi della prescrizione, garantirebbe «l'efficienza del processo» e pertanto risulterebbe esente dalle censure prospettate dal rimettente.Considerato in diritto1. – Con ordinanza emessa il 16 luglio 2004 il Tribunale di S. Maria Capua Vetere ha sollevato, in riferimento agli artt. 3, 25 e 112 della Costituzione, questione di legittimità costituzionale dell'art. 420-quater, comma 1, del codice di procedura penale, nella parte in cui «non prevede espressamente l'obbligo del giudice di dichiarare, lì dove prevista dalle norme pattizie, la contumacia dell'imputato estradato per fatti diversi e anteriori alla sua consegna». 
    2. – L'art. 14 della Convenzione europea di estradizione, stipulata a Parigi il 13 dicembre 1957, resa esecutiva in Italia con la legge 30 gennaio 1963, n. 300, fissa il principio di specialità, in base a cui la persona estradata non può essere perseguita, giudicata o arrestata, in vista dell'esecuzione di una pena o di una misura di sicurezza, né sottoposta a qualsiasi altra restrizione della sua libertà personale per un qualsiasi fatto anteriore alla consegna, diverso da quello che ha dato luogo all'estradizione. Il medesimo principio di specialità è contenuto nell'art. 721 cod. proc. pen. ed è ribadito dall'art. 7 della Convenzione di estradizione tra Italia e Polonia, resa esecutiva in Italia con la legge 7 giugno 1993, n. 193, che specificamente riguarda il giudizio a quo. 
    La Convenzione europea (art. 14, paragrafo 2) dà facoltà alla Parte richiedente di adottare le misure necessarie, in vista di una interruzione della prescrizione, in conformità con la propria legislazione, ivi compreso il ricorso ad un procedimento contumaciale. La Convenzione italo-polacca (art. 7, paragrafo 4) precisa che il principio di specialità, in essa ribadito, non preclude «la possibilità di un procedimento giudiziario in contumacia, se tale procedimento è previsto dalla legge della Parte richiedente». 
    3. – Sulla base della normativa pattizia prima richiamata, il giudice rimettente – preso atto del consolidato orientamento della giurisprudenza di legittimità, a partire dal 2001, nel senso che la clausola di specialità introduce una condizione di procedibilità – chiede a questa Corte una pronuncia additiva che dichiari l'illegittimità costituzionale dell'art. 420-quater, comma 1, cod. proc. pen., nella parte in cui non prevede espressamente l'obbligo per il giudice di dichiarare, lì dove prevista dalle norme pattizie, la contumacia dell'imputato estradato per fatti diversi ed anteriori alla sua consegna. 
    4. – La questione è inammissibile. 
    4.1. – Il giudice rimettente sollecita questa Corte ad introdurre, mediante una pronuncia additiva, un tipo di processo contumaciale nuovo e diverso rispetto a quello disciplinato dal vigente codice di rito penale.  
    Secondo l'art. 420-quater cod. proc. pen., la contumacia dell'imputato deve essere dichiarata dal giudice quando non ricorrono le condizioni indicate negli artt. 420, comma 2, 420-bis e 420-ter, commi 1 e 2, tra cui va segnalato, perché rilevante ai fini del presente giudizio, il «legittimo impedimento», distinto nel citato comma 1 dell'art. 420-ter sia dal caso fortuito che dalla forza maggiore. 
    Da quanto detto discende che nell'ordinamento processuale italiano non è ammessa la celebrazione in contumacia di un processo penale, se l'imputato non si presenta all'udienza per un legittimo impedimento, che ben può essere individuato, nel caso oggetto del giudizio a quo, nella clausola di specialità, la quale conferisce all'imputato il diritto a non essere processato per fatti anteriori e diversi da quelli che hanno dato luogo all'estradizione. L'inammissibilità di un giudizio in contumacia nei confronti dell'imputato che non si presenti in udienza al fine di esercitare un diritto è stata riconosciuta dalla giurisprudenza di legittimità, che ha così portato a compimento un lungo processo interpretativo. In assenza dunque di una volontaria rinuncia, da parte dell'imputato, alla prerogativa implicata dal principio di specialità, la contumacia non può essere dichiarata dal giudice. 
    4.2. – Il Tribunale rimettente mostra di essere consapevole della impossibilità di giungere per via interpretativa alla celebrazione di un processo contumaciale, che comporterebbe un'inammissibile estensione analogica delle norme processuali che regolano questo tipo di giudizio, stante la natura eccezionale delle stesse, riconosciuta come tale dalla giurisprudenza di legittimità. Per superare l'ostacolo e conseguire ugualmente lo scopo di superare la barriera della clausola di specialità – che, in alcuni casi, porta ad una sostanziale non processabilità dei presunti autori di reati anche gravi – il giudice a quo chiede alla Corte di rendere possibile, mediante una sentenza additiva, la celebrazione di un «giudizio contumaciale di mero accertamento», al doppio fine di interrompere la prescrizione e di costituire un titolo valido per inoltrare allo “Stato-rifugio” richiesta di estradizione suppletiva.  
    4.3. – Non rientra nei poteri di questa Corte creare un nuovo tipo di processo contumaciale – previsto e regolato solo in funzione della interruzione della prescrizione o della costituzione di un titolo valido per richiedere l'estradizione suppletiva – che obblighi il giudice, come richiesto dal rimettente, a dichiarare la contumacia al di là delle condizioni attualmente previste dalla legge. Ciò potrebbe fare, in ipotesi, il legislatore, avvalendosi della possibilità offerta dalle norme internazionali pattizie prima citate, che esplicitamente subordinano la deroga alla clausola di specialità all'esistenza di una apposita normativa nell'ordinamento interno dello Stato richiedente. Il giudice rimettente ritiene che, per effetto di una pronuncia del giudice delle leggi, sia possibile operare il salto dalla mera possibilità che le Convenzioni internazionali citate lasciano ai legislatori nazionali ad una “necessità”, tale da rendere ammissibile un intervento “creativo” in materia processuale, che è precluso a questa Corte.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara inammissibile la questione di legittimità costituzionale dell'art. 420-quater del codice di procedura penale, sollevata, in riferimento agli artt. 3, 25 e 112 della Costituzione, dal Tribunale di S. Maria Capua Vetere con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  il 24 gennaio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Gaetano SILVESTRI, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 9 febbraio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
