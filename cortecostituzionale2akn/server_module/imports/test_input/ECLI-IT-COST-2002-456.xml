<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/456/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/456/"/>
          <FRBRalias value="ECLI:IT:COST:2002:456" name="ECLI"/>
          <FRBRdate date="24/10/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="456"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/456/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/456/ita@/!main"/>
          <FRBRdate date="24/10/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/456/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/456/ita@.xml"/>
          <FRBRdate date="24/10/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/11/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Annibale Marini</cc:relatore_pronuncia>
        <cc:data_decisione>24/10/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 4 della legge 27 marzo 2001, n. 97 (Norme sul rapporto tra procedimento penale e procedimento disciplinare ed effetti del giudicato penale nei confronti dei dipendenti delle amministrazioni pubbliche), promossi con ordinanze emesse il 20 giugno e l'11 luglio 2001 dal Tribunale amministrativo regionale dell'Emilia Romagna e il 19 settembre 2001 dal Tribunale amministrativo regionale della Campania, rispettivamente iscritte al n. 935 del registro ordinanze 2001 ed ai nn. 99 e 141 del registro ordinanze 2002 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2001 e nn. 11 e 14, prima serie speciale, dell'anno 2002. 
    Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 23 ottobre 2002 il Giudice relatore Annibale Marini. 
    Ritenuto che il Tribunale amministrativo regionale dell'Emilia Romagna, con ordinanza del 20 giugno 2001,  depositata l'11 luglio 2001, ha sollevato, in riferimento agli artt. 3 e 27, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 4, comma 1, della legge 27 marzo 2001, n. 97 (Norme sul rapporto tra procedimento penale e procedimento disciplinare ed effetti del giudicato penale nei confronti dei dipendenti delle amministrazioni pubbliche), secondo cui i dipendenti pubblici, in caso di condanna, anche non definitiva, per alcuno dei delitti previsti dagli artt. 314, primo comma, 317, 318, 319, 319-ter e 320 del codice penale e dall'art. 3 della legge 9 dicembre 1941, n. 1383, sono sospesi dal servizio; 
    che, secondo il rimettente, il carattere automatico della misura cautelare prevista dalla norma non sarebbe compatibile - alla luce della stessa giurisprudenza costituzionale - né con i principi di ragionevolezza e proporzionalità di cui all'art. 3 della Costituzione né con la presunzione di non colpevolezza di cui all'art. 27, secondo comma, della Costituzione; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la declaratoria di non fondatezza della questione; 
    che, secondo la parte pubblica, il riferimento al parametro di cui all'art. 27, secondo comma, della Costituzione sarebbe inconferente in quanto la sospensione dal servizio di cui alla norma censurata sarebbe una tipica misura cautelare che opera, per definizione, prima dell'accertamento definitivo di colpevolezza; 
    che la questione sarebbe, d'altro canto, priva di fondamento anche con riferimento al parametro rappresentato dal principio di ragionevolezza, considerato che una pronuncia di colpevolezza per taluno dei reati considerati dalla norma, pur se non definitiva, è comunque tale da pregiudicare l'affidabilità del pubblico dipendente, cosicché la misura sospensiva, in attesa dell'accertamento definitivo, non potrebbe certamente ritenersi non proporzionata all'interesse pubblico tutelato; 
    che il medesimo rimettente, con ordinanza dell'11 luglio 2001, depositata il 18 ottobre 2001, ha sollevato, in riferimento ai parametri di cui agli artt. 3, primo comma, 27, secondo comma, e 97, primo comma, della Costituzione, questione di legittimità costituzionale dello stesso art. 4, commi 1 e 2, della legge n. 97 del 2001, «nella parte in cui non prevede la valutazione discrezionale da parte della P.A. circa la durata della sospensione automatica dal servizio dei dipendenti disciplinata dalla detta disposizione»; 
    che, secondo il giudice a quo, il provvedimento di sospensione previsto dalla norma impugnata, pur avendo natura cautelare, a causa della sua automaticità acquisterebbe il carattere di «anticipazione degli effetti repressivi conseguenti all'accertamento della  colpevolezza a seguito della condanna definitiva», senza che peraltro possa tenersi alcun conto della concessione del beneficio della sospensione condizionale della pena; 
    che, oltretutto, per la sua durata tendenzialmente indeterminata, la sospensione stessa potrebbe talvolta risultare di gravità maggiore della successiva sanzione disciplinare inflitta dopo la condanna definitiva; 
    che solamente la previsione di una valutazione discrezionale della pubblica amministrazione riguardo, quanto meno, alla durata della misura potrebbe rendere la norma compatibile - ad avviso del rimettente - con il principio di non colpevolezza di cui all'art. 27, secondo comma, della Costituzione; 
    che la medesima norma, sempre in relazione ai caratteri della automaticità e della indeterminatezza della durata della misura cautelare, si porrebbe poi in contrasto con l'art. 3, primo comma, della Costituzione sotto il profilo della irrazionalità e della mancanza di proporzionalità, tenuto conto della notoria lunga durata media dei giudizi penali; 
    che risulterebbe altresì leso il principio di eguaglianza, in quanto la norma comminerebbe lo stesso trattamento a situazioni differenziate, restando irrilevante sia la concessione o meno della sospensione condizionale della pena sia l'entità della pena stessa, e ciò nonostante che il successivo art. 5 della stessa legge preveda, al comma 2, l'automatica estinzione del rapporto di lavoro solo nel caso di condanna definitiva per determinati reati contro la pubblica amministrazione a pena detentiva non inferiore a tre anni; 
    che la norma impugnata, infine, privando l'amministrazione del potere di compiere qualsiasi valutazione di opportunità riguardo alla adozione della misura cautelare, contrasterebbe con il principio di buon andamento, «inteso come efficienza ed economicità dell'azione amministrativa»; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha concluso per la declaratoria di infondatezza della questione, riportandosi integralmente al contenuto della memoria - depositata in copia - relativa ad analoga questione già sollevata, con più ordinanze, da altro giudice; 
    che il Tribunale amministrativo regionale della Campania, con ordinanza del 19 settembre 2001, depositata il 23 gennaio 2002, ha sollevato questione di legittimità costituzionale del medesimo art. 4 della legge n. 97 del 2001, in riferimento agli artt. 3, 4, 24, 27, 35, 36 e 97 della Costituzione; 
    che l'incostituzionalità della norma si incentrerebbe essenzialmente, ad avviso del rimettente, sulla irragionevolezza «del bilanciamento operato dal legislatore fra le esigenze di buon andamento ed imparzialità della pubblica amministrazione e tutela dei diritti compressi dalla misura cautelare»; 
    che la giurisprudenza costituzionale sarebbe del resto consolidata - sempre secondo il giudice a quo - nel senso di «escludere ogni automatismo di trasposizione di effetti dal piano penale a quello disciplinare e cautelare nell'ambito del rapporto d'impiego», salva l'ipotesi estrema - cui farebbe riferimento la sentenza n. 206 del 1999 - della condanna per reato associativo di stampo mafioso, prevista quale causa di sospensione dall'ufficio dall'art. 15 della legge 19 marzo 1990, n. 55; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, anche in tal caso riportandosi integralmente alla memoria depositata nell'altro giudizio. 
    Considerato  preliminarmente che i tre giudizi, in considerazione della evidente affinità delle questioni sollevate, vanno riuniti per essere decisi con unico provvedimento; 
    che questa Corte, con sentenza n. 145 del 2002, successiva alle tre ordinanze di rimessione, ha dichiarato l'illegittimità costituzionale, nei sensi di cui in motivazione, dell'art. 4, comma 2, della legge 27 marzo 2001, n. 97 (Norme sul rapporto tra procedimento penale e procedimento disciplinare ed effetti del giudicato penale nei confronti dei dipendenti delle amministrazioni pubbliche), «nella parte in cui dispone che la sospensione perde efficacia decorso un periodo di tempo pari a quello di prescrizione del reato»; 
    che - stante l'evidente connessione tra le disposizioni contenute nel primo e nel secondo comma della norma impugnata - l'intervenuto mutamento del quadro normativo rende necessaria la restituzione degli atti ai giudici a quibus perché valutino se le questioni sollevate possano ritenersi tuttora rilevanti.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      riuniti i giudizi, &#13;
      ordina la restituzione degli atti al Tribunale amministrativo regionale dell'Emilia-Romagna ed al Tribunale amministrativo regionale della Campania.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 ottobre 2002. &#13;
    F.to: &#13;
    Cesare RUPERTO, Presidente &#13;
    Annibale MARINI, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 12 novembre 2002. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
