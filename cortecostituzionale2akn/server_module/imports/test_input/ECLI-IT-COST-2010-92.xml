<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/92/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/92/"/>
          <FRBRalias value="ECLI:IT:COST:2010:92" name="ECLI"/>
          <FRBRdate date="24/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="92"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/92/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/92/ita@/!main"/>
          <FRBRdate date="24/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/92/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/92/ita@.xml"/>
          <FRBRdate date="24/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="05/03/2010" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2010</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>DE SIERVO</cc:presidente>
        <cc:relatore_pronuncia>Sabino Cassese</cc:relatore_pronuncia>
        <cc:data_decisione>24/02/2010</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell' art. 9, commi 2 e 3 della legge della Regione Marche 24 dicembre 2008 n. 37 ( Disposizioni per la formazione del Bilancio annuale 2009 e pluriennale 2009/2011 della Regione - Legge finanziaria 2009), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 26 febbraio-3 marzo 2009, depositato in cancelleria il 3 marzo 2009 ed iscritto al n. 16 del registro ricorsi 2009.
 Udito nella camera di consiglio del 10 febbraio 2010 il Giudice relatore Sabino Cassese.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, con ricorso notificato il 26 febbraio e depositato il 3 marzo del 2009, ha impugnato l'art. 9, commi 2 e 3, della legge della Regione Marche 24 dicembre 2008, n. 37 (Disposizioni per la formazione del Bilancio annuale 2009 e pluriennale 2009/2011 della Regione - Legge finanziaria 2009);
 che il ricorrente sostiene che le disposizioni impugnate disciplinano le modalità di stabilizzazione del personale non dirigenziale in contrasto con la normativa statale di riferimento, costituita dall'art. 1, commi 557 e 558, della legge 27 dicembre 2006, n. 296 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2007), dall'art. 3, commi 90 e 94, della legge 24 dicembre 2007, n. 244 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2008) e dalla circolare 18 aprile 2008, n. 5, emanata dalla Presidenza del Consiglio dei ministri (Linee di indirizzo in merito all'interpretazione e all'applicazione dell'art. 3, commi da 90 a 95 e comma 106, della legge 24 dicembre 2007, n. 244 - legge finanziaria 2008);
 che, in particolare, ad avviso del Presidente del Consiglio dei ministri, l'art. 9, comma 2, della legge regionale censurata, nell'includere «nel periodo di anzianità positivamente valutabile» ai fini della stabilizzazione «i periodi relativi a rapporti di collaborazione coordinata e continuativa», detterebbe una disciplina difforme da quella statale, la quale non ammette «la cumulabilità delle esperienze lavorative maturate con tipologie contrattuali differenti»;
 che, in modo analogo, l'art. 9, comma 3, della legge impugnata, la quale concerne la stabilizzazione del personale non dirigenziale del servizio sanitario regionale e dell'Agenzia regionale per la protezione ambientale delle Marche, «nel prevedere la possibilità di cumulare il periodo di lavoro a tempo determinato o con contratto di collaborazione coordinata e continuativa con altre forme di rapporto di lavoro flessibile o attuato mediante convenzione», confliggerebbe a sua volta con la disciplina statale in materia, la quale «non consente la positiva attuazione della procedura di stabilizzazione del personale in servizio con forme contrattuali di lavoro differenti dal tempo determinato e dalla collaborazione coordinata e continuativa»;
 che, per tali ragioni, secondo il Presidente del Consiglio dei ministri, le disposizioni censurate, «introducendo una normativa diversa e più favorevole ai fini della procedura di stabilizzazione solo in un ambito regionale», violerebbero l'art. 3 Cost., in considerazione della «disparità di trattamento nei confronti di omologhe categorie lavorative radicate in altre regioni», nonché l'art. 97 Cost., «sotto il profilo della violazione del principio di imparzialità dell'azione amministrativa e uniformità della stessa nel territorio nazionale»; 
 che la Regione Marche non si è costituita in giudizio;
 che, con atto notificato a controparte in data 16 novembre 2009 e depositato presso la cancelleria della Corte costituzionale il 24 novembre successivo, il Presidente del Consiglio dei ministri ha dichiarato di rinunciare al presente ricorso, considerato che la Regione Marche, con la successiva legge regionale 3 aprile 2009, n. 8 (Ulteriori Modifiche all'articolo 9 della legge regionale 24 dicembre 2008, n. 37 - Legge finanziaria 2009), ha apportato modifiche alla disciplina impugnata, adeguandosi ai rilievi governativi e conformando la propria disciplina alla normativa statale di riferimento.
 Considerato che, in mancanza di costituzione in giudizio della parte resistente, la rinuncia al ricorso determina, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (fra le più recenti, ordinanze n. 14 e n. 8 del 2010).</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara estinto il processo.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 febbraio 2010.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Sabino CASSESE, Redattore&#13;
 Maria Rosaria FRUSCELLA, Cancelliere&#13;
 Depositata in Cancelleria il 5 marzo 2010.&#13;
 Il Cancelliere&#13;
 F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
