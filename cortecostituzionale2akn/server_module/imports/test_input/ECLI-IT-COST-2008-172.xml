<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/172/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/172/"/>
          <FRBRalias value="ECLI:IT:COST:2008:172" name="ECLI"/>
          <FRBRdate date="19/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="172"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/172/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/172/ita@/!main"/>
          <FRBRdate date="19/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/172/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/172/ita@.xml"/>
          <FRBRdate date="19/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/05/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Luigi Mazzella</cc:relatore_pronuncia>
        <cc:data_decisione>19/05/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo ADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 1, comma 777, della legge 27 dicembre 2006, n. 296 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2007), promosso con ordinanza del 5 marzo 2007 dalla Corte di cassazione nel procedimento civile vertente tra Valerio Morettini e l'INPS iscritta al n. 507 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 27, prima serie speciale, dell'anno 2007. 
    Visti l'atto di costituzione dell'INPS nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nell'udienza pubblica del 6 maggio 2008 il Giudice relatore Luigi Mazzella; 
    uditi l'avvocato Nicola Valente per l'INPS e l'avvocato dello Stato Gabriella Palmieri per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. – Nel corso di un giudizio civile promosso da Valerio Morettini contro l'Istituto nazionale della previdenza sociale (INPS), la Corte di cassazione ha sollevato, in riferimento agli artt. 3, primo comma, 35, quarto comma, e 38, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 1, comma 777, della legge 27 dicembre 2006, n. 296 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2007). 
    La rimettente premette che il Morettini, titolare di pensione di anzianità liquidata sulla base sia di contributi versati in Svizzera, sia di contributi versati in Italia, ha proposto ricorso per cassazione contro la sentenza d'appello che ha respinto la domanda di accertamento del suo diritto al ricalcolo della pensione tenendo conto della retribuzione effettivamente percepita in Svizzera negli ultimi cinque anni di lavoro ed ha statuito (accogliendo la tesi sostenuta dall'INPS) che tale retribuzione deve essere riparametrata secondo le aliquote contributive vigenti nell'assicurazione generale obbligatoria. 
    Il giudice a quo aggiunge che, nelle more del giudizio di cassazione, è sopravvenuta la legge n. 296 del 2006, la quale, all'art. 1, comma 777, prevede che «L'articolo 5, secondo comma, del decreto del Presidente della Repubblica 27 aprile 1968, n. 488, e successive modificazioni, si interpreta nel senso che, in caso di trasferimento presso l'assicurazione generale obbligatoria italiana dei contributi versati ad enti previdenziali di Paesi esteri in conseguenza di convenzioni ed accordi internazionali di sicurezza sociale, la retribuzione pensionabile relativa ai periodi di lavoro svolto nei Paesi esteri è determinata moltiplicando l'importo dei contributi trasferiti per cento e dividendo il risultato per l'aliquota contributiva per l'invalidità, vecchiaia e superstiti in vigore nel periodo cui i contributi si riferiscono. Sono fatti salvi i trattamenti pensionistici più favorevoli già liquidati alla data di entrata in vigore della presente legge». 
    Ad avviso della Corte di cassazione, tale disposizione, applicabile nel giudizio principale, ancorché si autoqualifichi come interpretativa, ha invece carattere innovativo. 
    Infatti, sul regime dei contributi trasferiti in forza della convenzione italo-svizzera del 14 dicembre l962, resa esecutiva con la legge 31 ottobre 1963, n. 1781 (Ratifica ed esecuzione della Convenzione tra l'Italia e la Svizzera relativa alla sicurezza sociale col Protocollo finale e Dichiarazioni comuni, conclusa a Roma il 14 dicembre 1962), non sussisteva alcun contrasto di giurisprudenza, ma si era invece affermato un orientamento unico ed ormai consolidato, secondo il quale, ai sensi dell'art. 1 dell'accordo aggiuntivo alla predetta convenzione del 14 dicembre 1962, concluso a Berna il 4 luglio 1969, reso esecutivo con la legge 18 maggio 1973, n. 283 (Ratifica ed esecuzione dell'Accordo aggiuntivo alla Convenzione tra l'Italia e la Svizzera relativa alla sicurezza sociale del 14 dicembre 1962, concluso a Berna il 4 luglio 1969), il lavoratore italiano può chiedere il trasferimento all'INPS dei contributi versati in Svizzera in suo favore, al fine di conseguire i vantaggi derivanti dalla legislazione italiana sull'assicurazione invalidità, vecchiaia e superstiti e, tra questi, quello della determinazione della pensione con il metodo retributivo. Sicché, non essendo state adottate dal legislatore italiano disposizioni particolari per regolare l'effetto del trasferimento dei contributi, nella determinazione del trattamento previdenziale in favore del lavoratore doveva farsi riferimento alla retribuzione da questi percepita, a nulla rilevando che i contributi accreditati in Svizzera e trasferiti in Italia fossero stati calcolati sulla base dell'aliquota prevista dalla legislazione elvetica. 
    Quindi, prosegue la rimettente, la norma censurata ha introdotto un nuovo criterio contabile non ricavabile dalla disposizione interpretata, il quale, nei casi cui si applica la menzionata convenzione italo-svizzera, è peggiorativo per il pensionato, perché, essendo l'aliquota contributiva vigente in Svizzera sensibilmente inferiore a quella vigente in Italia nel periodo rilevante per il calcolo della pensione, anche la retribuzione pensionabile è proporzionalmente minore di quella computabile secondo il diverso criterio affermato dalla citata giurisprudenza di legittimità.  
    Inoltre, la nuova disposizione trova applicazione anche nel caso in cui il lavoratore abbia maturato il diritto alla pensione di anzianità e ne abbia già chiesto la corresponsione, ancorché non abbia ottenuto la liquidazione della stessa per aver l'INPS opposto l'applicabilità di un criterio di calcolo diverso (e meno favorevole) rispetto a quello affermato dalla giurisprudenza di legittimità. Pertanto, un lavoratore (come quello ricorrente nel giudizio principale), pur avendo già maturato il diritto ad un trattamento pensionistico calcolato sulle retribuzioni effettivamente percepite in Svizzera senza alcuna riparametrazione in base alle aliquote contributive vigenti nell'assicurazione generale obbligatoria in Italia, si trova a subire ex tunc una decurtazione di tale trattamento già entrato nel suo patrimonio. 
    Ad avviso della rimettente, la riduzione ex post di un trattamento previdenziale già maturato ridonda in lesione dell'art. 38, secondo comma, Cost., giacché priva il pensionato di mezzi adeguati alle proprie esigenze di vita. Inoltre, è leso l'affidamento riposto nella certezza dei rapporti giuridici dal pensionato che, avendo operato scelte di vita collocandosi in quiescenza sulla base della normativa all'epoca vigente, si vede ridimensionato un diritto già maturato ed anche esercitato, con conseguente irragionevolezza intrinseca dell'efficacia retroattiva dell'art. 1, comma 777, della legge n. 296 del 2006, in violazione dell'art. 3, primo comma, della Costituzione. 
    La Corte di cassazione sostiene, poi, che la norma censurata collide anche con il principio costituzionale della tutela del lavoro all'estero (art. 35, quarto comma, Cost. in combinato disposto con l'art. 3, primo comma, Cost.), perché – a parità di retribuzione percepita in Italia e all'estero – svantaggia il lavoratore emigrato rispetto al lavoratore rimasto in Italia, in quanto riparametra retroattivamente la retribuzione pensionabile del primo in termini ingiustificatamente riduttivi e penalizzanti. 
    A parere del giudice a quo, è violato, infine, il principio di eguaglianza enunciato dall'art. 3, primo comma, Cost., poiché l'efficacia retroattiva dell'art. 1, comma 777, della legge n. 296 del 2006 si arresta di fronte all'avvenuta liquidazione della pensione che costituisce una circostanza contingente e casuale, inidonea di per sé a giustificare un regime differenziato. 
      2. – Nel giudizio si è costituito l'INPS, il quale chiede che la questione di illegittimità costituzionale sia dichiarata infondata. 
    Ad avviso dell'istituto previdenziale non sussiste violazione dell'art. 38 della Costituzione, non essendo vulnerato il principio dell'adeguatezza del trattamento pensionistico. 
      Né la retroattività della norma lede l'affidamento nutrito dai lavoratori circa i criteri di computo della pensione, perché sussisteva un'obiettiva incertezza in ordine alle modalità di determinazione della retribuzione pensionabile, incertezza causata dalla contrapposizione fra la costante prassi amministrativa e l'orientamento giurisprudenziale della Corte di cassazione. 
      L'INPS sostiene che la questione è infondata anche con riferimento all'art. 35, quarto comma, Cost., perché l'art. 1, comma 777, della legge n. 296 del 2006 non introduce alcuna discriminazione a danno del lavoratore impiegato all'estero, ma, al contrario, elimina la rilevante disparità di trattamento che si verifica a danno dei lavoratori che non possono avvalersi del trasferimento, i quali, applicando il criterio di calcolo affermato dalla giurisprudenza di legittimità, si troverebbero, a parità di retribuzione percepita, ad usufruire dello stesso trattamento pensionistico di chi ha trasferito all'INPS i contributi versati in Svizzera, nonostante che, a causa della rilevantissima differenza delle aliquote vigenti nei due Paesi, abbiano versato una contribuzione molto maggiore. 
      Infine, a parere dell'istituto previdenziale, la norma censurata non víola neppure il principio di eguaglianza laddove individua nell'avvenuta liquidazione del trattamento pensionistico il limite alla sua efficacia retroattiva, poiché una simile clausola di salvezza non è irragionevole, valendo solamente ad escludere il ricalcolo in peius delle pensioni costituite in forza di sentenze passate in giudicato ovvero dei pochissimi trattamenti pensionistici liquidati in conformità all'orientamento espresso dalla Corte di cassazione a seguito dell'accoglimento di ricorsi amministrativi da parte dei comitati centrali dell'INPS. 
      3. – Nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso nel senso dell'infondatezza della questione. 
      Ad avviso della difesa erariale, la norma censurata enuncia una delle possibili varianti di senso del testo della disposizione interpretata e non lede il principio della tutela dei lavoratori italiani all'estero, perché il sistema di calcolo della retribuzione pensionabile sulla base del trasferimento dei contributi versati in Svizzera, rimasto in vigore fino al 2002, è comunque maggiormente favorevole rispetto a quello in vigore per i lavoratori italiani occupati nei Paesi dell'Unione europea.Considerato in diritto1. – La Corte di cassazione dubita, in riferimento agli artt. 3, primo comma, 35, quarto comma, e 38, secondo comma, della Costituzione, della legittimità costituzionale dell'art. 1, comma 777, della legge 27 dicembre 2006 n. 296 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2007), a norma del quale «L'articolo 5, secondo comma, del decreto del Presidente della Repubblica 27 aprile 1968, n. 488, e successive modificazioni, si interpreta nel senso che, in caso di trasferimento presso l'assicurazione generale obbligatoria italiana dei contributi versati ad enti previdenziali di Paesi esteri in conseguenza di convenzioni ed accordi internazionali di sicurezza sociale, la retribuzione pensionabile relativa ai periodi di lavoro svolto nei Paesi esteri è determinata moltiplicando l'importo dei contributi trasferiti per cento e dividendo il risultato per l'aliquota contributiva per l'invalidità, vecchiaia e superstiti in vigore nel periodo cui i contributi si riferiscono. Sono fatti salvi i trattamenti pensionistici più favorevoli già liquidati alla data di entrata in vigore della presente legge». 
    Ad avviso del giudice a quo, tale disposizione ha carattere innovativo, perché, quanto al regime dei contributi trasferiti in Italia in forza di convenzioni ed accordi conclusi con la Svizzera, si era già affermato un orientamento giurisprudenziale incontrastato, secondo il quale il lavoratore italiano che chiede il trasferimento all'INPS dei contributi versati in Svizzera in suo favore ha diritto di conseguire i vantaggi derivanti dalla legislazione previdenziale italiana, in particolare quello della determinazione della pensione con il metodo retributivo sulla base della retribuzione effettivamente percepita in Svizzera, a nulla rilevando che i contributi accreditati in Svizzera e trasferiti in Italia siano stati versati secondo l'aliquota prevista dalla legislazione elvetica, di gran lunga inferiore a quella stabilita dalla legislazione italiana. 
    La norma censurata avrebbe quindi introdotto un nuovo criterio contabile non ricavabile dalla disposizione interpretata, con conseguente violazione dell'art. 3, primo comma, Cost., poiché sarebbe leso l'affidamento riposto nella certezza dei rapporti giuridici dai titolari di pensione, i quali, essendosi collocati in quiescenza sulla base della normativa all'epoca vigente, si vedrebbero ridimensionati diritti già maturati, e perché l'efficacia retroattiva della norma censurata, arrestandosi di fronte all'avvenuta liquidazione della pensione (che costituisce una circostanza contingente e casuale), lederebbe il principio di eguaglianza 
    Sarebbero poi violati il principio della tutela del lavoro all'estero (enunciato dall'art. 35, quarto comma, Cost.), perché – a parità di retribuzione percepita in Italia e all'estero – la norma censurata svantaggerebbe il lavoratore emigrato rispetto a quello rimasto in Italia, e l'art. 38, secondo comma, Cost., poiché la riduzione ex post di un trattamento previdenziale già maturato priverebbe il pensionato di mezzi adeguati alle proprie esigenze di vita. 
    2. – La questione non è fondata. 
    In base al sistema «retributivo» di computo delle pensioni erogate dall'assicurazione generale obbligatoria, introdotto dal d. P. R. 27 aprile 1968, n. 488 (Aumento e nuovo sistema di calcolo delle pensioni a carico dell'assicurazione generale obbligatoria), e valevole per l'assicurato attore del giudizio principale, la pensione si calcola applicando un coefficiente (proporzionato al numero complessivo di settimane di contribuzione vantate dall'interessato) alla retribuzione annua pensionabile, vale a dire alla retribuzione annua media percepita dal lavoratore durante un certo periodo di riferimento. 
    Interventi legislativi succedutisi nel tempo hanno definito in maniera diversa i criteri di determinazione della retribuzione pensionabile: si vedano, in particolare, l'art. 5, secondo comma, del d. P. R. n. 488 del 1968, l'art. 14 della legge 30 aprile 1969, n. 153 (Revisione degli ordinamenti pensionistici e norme in materia di sicurezza sociale), l'art. 3 della legge 29 maggio 1982, n. 297 (Disciplina del trattamento di fine rapporto e norme in materia pensionistica), l'art. 3 del decreto legislativo 30 dicembre 1992, n. 503 (Norme per il riordinamento del sistema previdenziale dei lavoratori privati e pubblici, a norma dell'articolo 3 della legge 23 ottobre 1992, n. 421), l'art. 1 del decreto legislativo 11 agosto 1993, n. 373 (Attuazione dell'art. 3, comma 1, lettera o), della legge 23 ottobre 1992, n. 421, recante calcolo delle pensioni per i nuovi assunti). 
    Caratteristica comune di tutte le menzionate norme di definizione della retribuzione pensionabile è che esse si collocano nell'àmbito di un sistema previdenziale tendente alla corrispondenza fra le risorse disponibili e le prestazioni erogate. E ciò anche in ossequio al vincolo imposto dall'art. 81, quarto comma, della Costituzione. Difatti, lo stesso passaggio dal criterio «contributivo» a quello «retributivo» nel calcolo delle pensioni, non è avvenuto a discapito dell'esigenza della sostenibilità finanziaria del sistema. Il principio secondo il quale la pensione deve essere calcolata applicando alla retribuzione mediamente percepita dal lavoratore in un determinato arco di tempo una certa percentuale direttamente proporzionale al numero di settimane coperte da contribuzione ha sempre avuto per presupposto la circostanza che le aliquote contributive vigenti fossero tali da garantire l'equilibrio finanziario di un tale sistema.  
    Se, dunque, le previsioni espresse dall'art. 5, secondo comma, del d. P. R. n. 488 del 1968 e dalle successive disposizioni in materia, implicano che il rapporto tra la retribuzione pensionabile e la massa dei contributi disponibili sia quello espresso dalle aliquote contributive previste in Italia, non è affatto incompatibile con tali norme una loro applicazione, secondo la quale, nei casi in cui occorra calcolare la retribuzione pensionabile di chi abbia versato i contributi secondo sistemi diversi da quello italiano, si proceda ad una riparametrazione della retribuzione percepita all'estero che consenta di rendere il rapporto tra retribuzione pensionabile e contributi versati omogeneo a quello vigente in Italia nello stesso periodo di tempo. Ed è ciò che ha stabilito l'art. 1, comma 777, della legge n. 296 del 2006, disponendo che la retribuzione percepita all'estero, da porre a base del calcolo della pensione, sia riproporzionata al fine di stabilire lo stesso rapporto percentuale previsto per i contributi versati nel nostro Paese nel medesimo periodo. In altri termini, la norma ha reso esplicito un precetto già contenuto nelle disposizioni oggetto dell'interpretazione autentica. Sotto tale profilo essa non è quindi irragionevole (sentenze n. 274 e n. 135 del 2006). 
      La norma censurata, inoltre, assegnando alla disposizione interpretata un significato rientrante nelle possibili letture del testo originario, non determina alcuna lesione dell'affidamento del cittadino nella certezza dell'ordinamento giuridico, anche perché nella fattispecie l'ente previdenziale ha continuato a contestare l'interpretazione sostenuta dalle controparti private – ed accolta dalla giurisprudenza – rendendo così reale il dubbio ermeneutico. 
    Né sussiste violazione del principio di eguaglianza, anch'esso sancito dall'art. 3, primo comma, Cost., perché la salvezza delle posizioni dei lavoratori, cui già sia stato liquidato il trattamento pensionistico secondo un criterio più favorevole, risponde, questo sì, all'esigenza di rispettare il principio dell'affidamento ed i diritti ormai acquisiti di detti lavoratori. 
    Non è leso neppure l'art. 35, quarto comma, Cost., perché l'art. 1, comma 777, della legge n. 296 del 2006 non attribuisce al lavoro prestato all'estero un trattamento deteriore rispetto a quello svolto in Italia, ma anzi assicura la razionalità complessiva del sistema previdenziale, evitando che, a fronte di una esigua contribuzione versata nel Paese estero, si possano ottenere le stesse utilità che chi ha prestato attività lavorativa esclusivamente in Italia può conseguire solo grazie ad una contribuzione molto più gravosa. 
    Infine, non è ravvisabile un contrasto con l'art. 38, secondo comma, Cost., perché la norma censurata non determina alcuna riduzione ex post del trattamento previdenziale spettante ai lavoratori. Essa, in definitiva, non fa altro che imporre per legge un'interpretazione già desumibile dalle disposizioni interpretate. Né la rimettente offre elementi per far ritenere che la norma determini un trattamento pensionistico addirittura insufficiente al soddisfacimento delle esigenze di vita del lavoratore.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara non fondata la questione di legittimità costituzionale dell'art. 1, comma 777, della legge 27 dicembre 2006 n. 296 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2007), sollevata, in riferimento agli artt. 3, primo comma, 35, quarto comma, e 38, secondo comma, della Costituzione, dalla Corte di cassazione con l'ordinanza indicata in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 maggio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Luigi MAZZELLA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 23 maggio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
