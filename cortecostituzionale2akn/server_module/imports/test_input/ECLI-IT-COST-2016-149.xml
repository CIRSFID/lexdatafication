<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/149/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/149/"/>
          <FRBRalias value="ECLI:IT:COST:2016:149" name="ECLI"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="149"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/149/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/149/ita@/!main"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/149/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/149/ita@.xml"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="16/06/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Marta Cartabia</cc:relatore_pronuncia>
        <cc:data_decisione>18/05/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della decisione del Vice Presidente della Commissione giustizia del Senato della Repubblica del 12 ottobre 2015 e della decisione della Conferenza dei Presidenti dei Gruppi parlamentari del Senato della Repubblica del 13 ottobre 2015, promosso dal senatore Giovanardi Carlo Amedeo e da altri senatori, con ricorso depositato in cancelleria l'11 febbraio 2016 ed iscritto al n. 4 del registro conflitti tra poteri dello Stato 2016, fase di ammissibilità.
 Udito nella camera di consiglio del 18 maggio 2016 il Giudice relatore Marta Cartabia.
 Ritenuto che, con ricorso depositato nella cancelleria della Corte costituzionale in data 11 febbraio 2016, alcuni senatori della Repubblica hanno sollevato conflitto di attribuzione tra poteri dello Stato, ai sensi dell'art. 134 della Costituzione, nei confronti del Presidente del Senato della Repubblica, della Conferenza dei Presidenti dei Gruppi parlamentari del Senato e del Vice Presidente della Commissione giustizia del Senato;
 che i ricorrenti impugnano il provvedimento con cui il Vice Presidente della Commissione giustizia del Senato ha disposto l'abbinamento del disegno di legge n. 2081, recante «Regolamentazione delle unioni civili tra persone dello stesso sesso e disciplina delle convivenze», ad altri disegni di legge (n. 2069 e n. 2084) in materia di unioni civili già all'esame dell'Assemblea (come risulta dal resoconto sommario della seduta della Commissione giustizia n. 243 del 12 ottobre 2015); nonché il provvedimento con cui la Conferenza dei Presidenti dei Gruppi parlamentari ha inserito l'esame del d.d.l. n. 2081 nel calendario dei lavori dell'Assemblea (come risulta dalla riunione n. 132 della Conferenza relativamente al calendario dei lavori dal 14 al 22 ottobre 2015), e quello con cui il Presidente del Senato ha sottoposto il medesimo disegno di legge all'esame e al voto dell'Assemblea (come risulta dai resoconti stenografici delle sedute pubbliche n. 522 e n. 523 dell'Assemblea del Senato rispettivamente del 13 e del 14 ottobre 2015);
 che, secondo quanto affermato nel ricorso, per effetto dei suddetti atti sarebbe stato menomato l'esercizio delle prerogative spettanti a ciascun parlamentare e sarebbero stati conseguentemente violati gli artt. 1, secondo comma, 67, 71, 72, primo e quarto comma, Cost.;
 che, quanto al profilo soggettivo, i ricorrenti affermano la loro piena legittimazione, muovendo dalla considerazione che la giurisprudenza costituzionale in materia non esclude esplicitamente che i singoli parlamentari possano essere qualificati poteri dello Stato ai fini del conflitto di attribuzione ex art. 37 della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), salvo il caso in cui il rimedio non abbia carattere di residualità;
 che la legittimazione del singolo parlamentare deriverebbe dall'essere titolare di specifici poteri riconosciutigli direttamente dalla Costituzione, quale rappresentante della Nazione (art. 67 Cost.), «prima forma organica attraverso la quale, a livello di Stato-apparato, si esprime la sovranità popolare» (art. 1, secondo comma, Cost.), e partecipe della funzione legislativa delle Camere (art. 71 Cost.), poteri che si estrinsecano sia tramite la presentazione di progetti di legge e di proposte emendative, sia tramite la partecipazione ai lavori delle commissioni, anche se di esse non si faccia parte;
 che corollario di tali prerogative sarebbe la sussistenza, in capo al singolo parlamentare, del «diritto di esigere che i Regolamenti (espressamente richiamati dall'art. 72, co. 1, Cost.) siano formulati e, comunque, interpretati ed applicati conformemente a Costituzione [...], a tutela proprio delle attribuzioni che a lui competono in virtù del potere di cui è uti singulus portatore»;
 che, quanto al profilo oggettivo, è lamentata la violazione di una serie di norme regolamentari relative all'iter di formazione della legge (particolarmente, degli artt. 31, 43, 44 e 51 del Regolamento del Senato della Repubblica) e, per il loro tramite, dell'art. 72, primo e quarto comma, Cost.;
 che i ricorrenti ricostruiscono analiticamente le vicende dei lavori parlamentari dalle quali discenderebbero le lamentate violazioni, esponendo che il disegno di legge n. 2081, presentato il 6 ottobre 2015 e assegnato il successivo 7 ottobre all'esame della Commissione giustizia del Senato, è stato illustrato da parte della relatrice della Commissione, unitamente ad altri due disegni di legge in materia (n. 2069 e n. 2084), nella seduta notturna del 12 ottobre 2015, mentre la sua trattazione rinviata alla seduta del 13 ottobre 2015;
 che tale trattazione è stata poi ulteriormente rinviata alla seduta pomeridiana del giorno successivo, ma non ha avuto luogo, essendo stato nel frattempo disposto, da parte del Vice Presidente della Commissione giustizia, l'abbinamento del d.d.l. n. 2081 ad altri disegni di legge in materia di unioni civili già all'esame dell'Assemblea, senza che, ad avviso dei ricorrenti, la Commissione si sia espressa sul punto, come invece previsto dall'art. 51 del Regolamento del Senato della Repubblica;
 che la Conferenza dei Presidenti dei Gruppi parlamentari ha inserito l'esame del d.d.l. n. 2081 nel calendario dell'Assemblea, senza il previo esame in Commissione e senza rispettare i termini indicati dalle norme regolamentari (art. 44); 
 che, ad avviso dei ricorrenti, il Presidente del Senato ha sottoposto il disegno di legge all'esame e al voto dell'Assemblea, in contrasto con le norme regolamentari sulla programmazione dei lavori (artt. 53 e 55);
 che il mancato esame in Commissione del testo normativo, ridotti i lavori alla sua mera illustrazione, avrebbe determinato «un caso di grave menomazione delle funzioni e delle prerogative dei Senatori facenti parte della Commissione, della minoranza parlamentare e, più ampiamente, di tutti i membri della Camera Alta».
 Considerato che, in questa fase del giudizio, la Corte è chiamata a deliberare, ai sensi dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), l'ammissibilità del ricorso, valutando, senza contraddittorio, se sussistano i requisiti soggettivo e oggettivo di un conflitto di attribuzione tra poteri dello Stato;
 che la Corte costituzionale è chiamata a verificare, in camera di consiglio, l'esistenza o meno della «materia di un conflitto la cui risoluzione spetti alla sua competenza»;
 che il conflitto di attribuzione tra poteri dello Stato è risolto dalla Corte costituzionale «per la delimitazione della sfera di attribuzioni determinata per i vari poteri da norme costituzionali» (art. 37, primo comma, della l. n. 87 del 1953);
 che occorre, quindi, affinché vi sia materia del conflitto, che si lamenti la violazione di norme costituzionali attributive di potere al soggetto ricorrente;
 che, a questo proposito, vero è che i senatori ricorrenti invocano gli artt. 1, secondo comma, 67, 71 e 72, primo e quarto comma, della Costituzione, ritenendo che da tali disposizioni costituzionali derivi la titolarità, in capo a ciascun parlamentare, del potere di iniziativa legislativa, che si estrinseca non solo con la presentazione di proposte di legge, ma altresì con la formalizzazione di emendamenti ai progetti di legge in discussione e con la partecipazione ai lavori delle Commissioni parlamentari, anche se di esse non si faccia parte;
 che, tuttavia, è altresì vero che i ricorrenti, dopo aver invocato le suddette disposizioni costituzionali, sviluppano le censure lamentando una serie di violazioni dei regolamenti e della prassi parlamentare dovute a uno scorretto andamento dei lavori parlamentari relativi al disegno di legge n. 2081;
 che, in particolare, i ricorrenti si dolgono del fatto che il Vice Presidente della Commissione giustizia abbia disposto l'abbinamento del d.d.l. n. 2081 agli altri disegni di legge in materia di unioni civili già all'esame dell'Assemblea, senza richiedere sul punto una deliberazione dell'intera Commissione, in violazione dell'art. 51 del Regolamento del Senato della Repubblica;
 che, inoltre, i ricorrenti lamentano il mancato rispetto delle varie fasi della programmazione dei lavori come descritte dagli artt. 53 e 55 del Regolamento del Senato della Repubblica da parte del Presidente del Senato e della Conferenza dei Presidenti dei Gruppi parlamentari, che avrebbero disposto l'inserimento dell'esame del d.d.l. n. 2081 direttamente nel calendario dell'Assemblea, senza preventivamente ricomprenderlo nel programma;
 che, infine, i ricorrenti ritengono che tali violazioni dei procedimenti parlamentari abbiano indebitamente ridotto l'esame del d.d.l. n. 2081 in Commissione giustizia, dove il suddetto disegno di legge è pervenuto poco più di una settimana prima della sua "calendarizzazione" all'esame in Assemblea, in virtù di una erronea applicazione dell'art. 44 del Regolamento del Senato della Repubblica;
 che, dunque, alla luce della ricostruzione della vicenda, la menomazione lamentata dai ricorrenti inerisce tutta alle modalità di svolgimento dei lavori parlamentari come disciplinati da norme e prassi regolamentari, che scandiscono e regolano i "momenti" del procedimento di formazione delle leggi, quali sono sia l'abbinamento dei disegni di legge attinenti a materie identiche o strettamente connesse, sia la "calendarizzazione" dei lavori in Assemblea, con conseguente discussione, esame, modifica e votazione dei disegni di legge in tale sede;
 che, come questa Corte ha già avuto modo di chiarire, a ciascuna Camera è riconosciuta e riservata la potestà di disciplinare, tramite il proprio Regolamento, il procedimento legislativo «in tutto ciò che non sia direttamente ed espressamente già disciplinato dalla Costituzione» (sentenza n. 78 del 1984);
 che, entro questi limiti, le vicende e i rapporti attinenti alla disciplina del procedimento legislativo «ineriscono alle funzioni primarie delle Camere» (sentenza n. 120 del 2014) e sono, per ciò stesso, coperte dall'autonomia che a queste compete e che si estrinseca non solo nella determinazione di cosa approvare, ma anche nella determinazione di quando approvare;
 che, dunque, il presente conflitto, nei termini in cui è stato articolato, «non attinge al livello del conflitto tra poteri dello Stato, la cui risoluzione spetta alla Corte costituzionale» (ordinanze n. 366 del 2008 e n. 90 del 1996), inerendo le argomentazioni addotte nel ricorso esclusivamente alla lesione di norme del Regolamento del Senato e della prassi parlamentare, senza che sia validamente dimostrata l'idoneità di queste ultime a integrare i parametri costituzionali invocati;
 che le eventuali violazioni di mere norme regolamentari e della prassi parlamentare lamentate dai ricorrenti debbono trovare all'interno delle stesse Camere gli strumenti intesi a garantire il corretto svolgimento dei lavori, nonché il rispetto del diritto parlamentare, dei diritti delle minoranze e dei singoli componenti (sentenza n. 379 del 1996);
 che, pertanto, il ricorso deve ritenersi inammissibile, restando assorbito l'esame di ogni altro profilo e requisito, anche soggettivo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara inammissibile il conflitto di attribuzione tra poteri dello Stato promosso dal senatore Giovanardi Carlo Amedeo e da altri senatori nei confronti del Presidente del Senato della Repubblica, della Conferenza dei Presidenti dei Gruppi parlamentari del Senato e del Vice Presidente della Commissione Giustizia del Senato, con il ricorso indicato in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 18 maggio 2016.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Marta CARTABIA, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 16 giugno 2016.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
