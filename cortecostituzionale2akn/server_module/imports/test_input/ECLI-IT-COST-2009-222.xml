<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/222/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/222/"/>
          <FRBRalias value="ECLI:IT:COST:2009:222" name="ECLI"/>
          <FRBRdate date="24/06/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="222"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/222/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/222/ita@/!main"/>
          <FRBRdate date="24/06/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/222/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/222/ita@.xml"/>
          <FRBRdate date="24/06/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="17/07/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Grossi</cc:relatore_pronuncia>
        <cc:data_decisione>24/06/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Francesco AMIRANTE; Giudici: Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato – sorto a seguito della lettera del Presidente del Senato, d'intesa con il Presidente della Camera dei deputati, inviata, in data 21 gennaio 2009, a tutti i membri della Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi, con la quale si procede alla revoca del Presidente della Commissione medesima e di tutti i suoi componenti al fine di consentire il rinnovo integrale dell'organo – promosso dalla Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi con ricorso depositato in cancelleria il 18 febbraio 2009 ed iscritto al n. 3 del registro conflitti tra poteri dello Stato 2009, fase di ammissibilità. 
    Udito nella camera di consiglio del 24 giugno 2009 il Giudice relatore Paolo Grossi. 
  
    Ritenuto che, con ricorso depositato il 18 febbraio 2009, la Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi, nella persona del suo Presidente pro-tempore, ha proposto – nei confronti del Presidente del Senato della Repubblica pro-tempore e «ove occorra» del Presidente della Camera dei deputati pro-tempore – conflitto di attribuzione tra poteri dello Stato, chiedendo alla Corte costituzionale di «dichiarare che non spettava al Presidente del Senato, d'intesa con il Presidente della Camera, far cessare dalle loro cariche il Presidente della Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi, nonché i componenti della stessa Commissione, in assenza di un formale atto di dimissioni o di altro legittimo impedimento anziché procedere alla rinnovazione dell'organo mediante la sostituzione dei suoi componenti dimissionari»; e, conseguentemente, di «annullare la lettera, in data 21 gennaio 2009, inviata dal Presidente del Senato, d'intesa con il Presidente della Camera, a tutti i componenti della predetta Commissione, nonché tutti gli atti conseguenti o connessi»; 
    che la ricorrente ricostruisce analiticamente le vicende che, in data 13 novembre 2008, a seguito della contestata elezione del senatore Riccardo Villari alla presidenza della Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi, avevano portato, dapprima – a causa del suo rifiuto di dimettersi dalla carica –, alla espulsione del medesimo dal gruppo parlamentare del Partito Democratico cui apparteneva (con conseguente iscrizione d'ufficio al gruppo misto del Senato) e, successivamente – in ragione della situazione di “paralisi” dell'organismo di garanzia determinata dalla mancata partecipazione ai lavori della Commissione di quasi tutti i componenti appartenenti ai gruppi di maggioranza e di opposizione –, alla decisione presa, nelle giornate del 19 e 20 gennaio 2009, da trentasette dei quaranta membri della Commissione medesima di rassegnare le dimissioni, con lettere scritte ai Presidenti dei due rami del Parlamento, rimanendo così in carica soltanto il Presidente, senatore Villari, e i due membri, deputati Beltrandi e Sardelli; 
    che la ricorrente, altresì, espone che, in data 21 gennaio 2009, in conformità ai pareri espressi all'unanimità dalle Giunte per il regolamento delle due Camere, il Presidente del Senato aveva inviato una lettera a ciascuno dei quaranta membri della Commissione, con la quale – richiamata, tra l'altro, la «evidente, permanente e irreversibile impossibilità» per la Commissione di svolgere le rilevanti funzioni che l'ordinamento le assegna, a tutela di valori costituzionali primari quali la libertà di manifestazione del pensiero, la libertà ed il pluralismo dell'informazione – aveva concluso nei seguenti termini: «Le comunico, pertanto, d'intesa con il Presidente della Camera, che, proprio al fine di consentire il rinnovo integrale dell'organo, deve considerarsi revocata, a far data da oggi, la Sua nomina a componente della Commissione […] come quella di tutti gli altri parlamentari già chiamati a farne parte»; 
    che, infine, la ricorrente rammenta che, in data 29 gennaio 2009, i Presidenti delle due Camere, d'intesa fra loro, avevano nominato quali «nuovi membri» i precedenti componenti della stessa Commissione ad eccezione del senatore Villari e di altri quattro componenti e che, il successivo 30 gennaio 2009, i medesimi Presidenti delle due Camere avevano convocato la Commissione, nuovamente nominata, per la riunione del 4 febbraio 2009, in cui era stato eletto nuovo Presidente della Commissione il senatore Sergio Zavoli; 
    che, con riferimento alla ammissibilità del ricorso, l'istante, da un lato, rammenta che questa Corte, nell'ordinanza n. 61 del 2008, ha già riconosciuto alla Commissione parlamentare di vigilanza la natura di «potere dello Stato» sotto entrambi i profili soggettivo ed oggettivo; dall'altro lato, rileva che anche gli organi convenuti – il Presidente del Senato e il Presidente della Camera dei deputati – sono abilitati a dichiarare in via definitiva la volontà del potere cui appartengono allorché, come nel caso di specie, tale volontà sia stata manifestata in un atto di rimozione dall'ufficio dei componenti della Commissione medesima; 
    che, peraltro, secondo l'istante non potrebbe essere oggetto di contestazione il fatto che il ricorso sia stato deliberato dalla Commissione di vigilanza e proposto dopo che il Presidente del Senato, d'intesa con quello della Camera, aveva spogliato delle loro cariche i componenti della Commissione medesima, in quanto «è proprio l'illegittimità degli atti (lesivi) e della spoliazione subita a costituire la ragione delle doglianze e quindi il fondamento della causa petendi» e, pertanto, almeno limitatamente a quella funzione di impugnazione dell'atto di rimozione dalla carica, il collegio doveva ritenersi ancora in vita, ultrattivamente rispetto al suo formale scioglimento; 
    che, d'altra parte, neppure potrebbe sostenersi che la deliberazione con la quale, il 23 gennaio 2009, la Commissione ha deciso di sollevare conflitto di attribuzione sia priva di validità in quanto mancante del quorum richiesto dall'art. 12 del regolamento della stessa Commissione («presenza della metà più uno dei suoi componenti»), giacché, per consuetudine parlamentare, il numero dei «componenti» va misurato su quello dei membri rimasti in carica perché non dimissionari (i quali tutti, nella specie, si sono regolarmente riuniti e hanno votato all'unanimità la proposizione del conflitto); 
    che, quanto al merito, la ricorrente deduce: a) l'illegittimità del potere di «revoca» dei Presidenti delle due Camere nei confronti dei membri della Commissione di vigilanza, non esistendo nei regolamenti parlamentari, né in quello della Commissione stessa, alcuna norma (neppure ricavabile per analogia) che lo attribuisca ad essi; b) la contrarietà agli usi parlamentari del potere di «revoca» dei membri delle commissioni parlamentari di garanzia, in quanto la consuetudine costituzionale si è formata nel senso che i Presidenti delle due Camere lascino in carica i parlamentari non dimissionari e sostituiscano immediatamente quelli dimissionari senza sciogliere o azzerare le commissioni; c) in subordine, l'esercizio irragionevole, sproporzionato e contro il principio di leale cooperazione del potere di revoca, che ha riguardato solo i tre membri rimasti in carica; d) l'illegittimità dell'atto impugnato ove (al di là della sua autoqualificazione) fosse configurato come atto di scioglimento della Commissione di vigilanza, in quanto, allo stato della normativa, non può riconoscersi ai Presidenti delle due Camere un siffatto potere di scioglimento e, ove tale potere fosse da riconoscere, nella specie sarebbe stato esercitato in modo illegittimo; e) l'illegittimità dell'atto impugnato, ove fosse configurato come una sorta di sfiducia individuale al presidente della Commissione di vigilanza, giacché proprio la ratio sottesa alla istituzione della Commissione medesima è finalizzata a sottrarla al dominio della maggioranza, in modo da salvaguardare tutti gli apporti pluralistici che in essa si manifestino. 
    Considerato che in questa fase del giudizio –  a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 – la Corte è chiamata a deliberare, con ordinanza in camera di consiglio e senza contraddittorio, circa l'esistenza o meno della «materia di un conflitto la cui risoluzione spetti alla sua competenza», con riferimento alla sussistenza dei requisiti soggettivi ed oggettivi richiamati dal primo comma dello stesso articolo; 
    che, avuto riguardo al requisito soggettivo, questa Corte ha riconosciuto la legittimazione attiva a promuovere conflitto di attribuzione tra poteri dello Stato alla Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi, in quanto organo competente a dichiarare in via definitiva la volontà della Camera dei deputati e del Senato della Repubblica (sentenza n. 69 del 2009 ed ordinanza n. 61 del 2008); 
    che, tuttavia (come affermato nello stesso contesto del ricorso introduttivo e come si evince dai documenti allegati), risulta che – a seguito della revoca, adottata con l'atto impugnato, di tutti i componenti della Commissione (trentasette dei quali, peraltro, già dimissionari) –  alla seduta del 23 gennaio 2009 abbiano partecipato soltanto tre dei quaranta membri convocati, e che solo essi (seppure all'unanimità) abbiano deciso di proporre il presente conflitto; 
    che, pertanto – poiché il comma 1 dell'art. 12 del regolamento interno della Commissione di vigilanza (emanato dai Presidenti delle Camere il 13 novembre 1975) sancisce che «per la validità delle deliberazioni e delle decisioni della Commissione occorre la presenza della metà più uno dei suoi componenti» – il mancato raggiungimento del necessario quorum strutturale osta a ritenere che la deliberazione di sollevare il conflitto sia ascrivibile ad una volontà ritualmente e validamente espressa dalla Commissione nella sua configurazione collegiale; 
    che, d'altra parte, l'assunto circa la sussistenza di una consuetudine parlamentare secondo cui non verrebbero computati nel suddetto quorum i componenti dimissionari – oltre a non trovare dimostrazione in relazione ad una sua forza derogatoria rispetto a quanto espressamente previsto dalla citata norma regolamentare – appare, di fatto, smentito sia dalla rilevata impossibilità di un regolare funzionamento dell'organo per la ripetuta assenza dei membri dimissionari, sia dalla circostanza che la convocazione per la seduta del 23 gennaio 2009 è stata disposta nei confronti di tutti i membri della Commissione, anche quindi di quelli dimissionari; 
    che inoltre, sotto diverso profilo, questa Corte – nel riconoscere, come detto, alla Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi la qualifica di «organo competente a dichiarare in via definitiva la volontà della Camera dei deputati e del Senato della Repubblica», di cui costituisce articolazione interna – ha precisato che la Commissione medesima «è investita di attribuzioni che discendono dall'esigenza di garantire il pluralismo dell'informazione, fondato sull'art. 21 Cost., in base al quale la presenza di un organo parlamentare di indirizzo e vigilanza serve ad evitare che il servizio pubblico radiotelevisivo venga gestito dal Governo in modo “esclusivo e preponderante”» (sentenza n. 69 del 2009); 
    che, viceversa – dalla stessa prospettazione dei fatti e dei motivi posti a sostegno della asserita illegittimità dell'atto impugnato, nonché dal petitum proposto –, risulta evidente che il ricorso è essenzialmente diretto a contestare la legittimità dell'atto inteso a far «cessare» dalla carica il presidente e/o il membro della Commissione chiamato a farne parte; 
    che, dunque, il presente conflitto (nei termini in cui è stato articolato) non riguarda propriamente la lesione di quelle attribuzioni istituzionali (derivanti da norme e principi costituzionali) a tutela delle quali la Commissione di vigilanza si configura, appunto, quale organo competente a dichiarare all'esterno ed in via definitiva la volontà della Camera dei deputati e del Senato della Repubblica, ma attiene esclusivamente alla posizione “interna” del singolo parlamentare, presidente o componente della Commissione, che – in quanto tale e con riferimento alla peculiarità della fattispecie – non può essere ritenuto potere dello Stato; 
    che, di conseguenza, non sussistendo i requisiti soggettivi ed oggettivi per l'instaurazione del sollevato conflitto, il ricorso è inammissibile.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara inammissibile il ricorso per conflitto di attribuzione tra poteri dello Stato di cui in epigrafe, proposto dalla Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi, nella persona del suo Presidente, nei confronti del Presidente del Senato della Repubblica e del Presidente della Camera dei deputati. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 giugno 2009.  &#13;
F.to:  &#13;
Francesco AMIRANTE, Presidente  &#13;
Paolo GROSSI, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 17 luglio 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
