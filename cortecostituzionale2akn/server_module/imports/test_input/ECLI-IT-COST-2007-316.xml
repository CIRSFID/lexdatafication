<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/316/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/316/"/>
          <FRBRalias value="ECLI:IT:COST:2007:316" name="ECLI"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="316"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/316/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/316/ita@/!main"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/316/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/316/ita@.xml"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="20/07/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>10/07/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 146, comma 3, lettera c), del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia – Testo A), promosso con ordinanza del 17 febbraio 2006 dal Tribunale di Roma, sezione fallimentare, sul reclamo proposto da Antoniucci Samuele n.q. di curatore del fallimento Orsa Minore s.r.l., iscritta al n. 600 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 1, prima serie speciale, dell'anno 2007. 
    Udito nella camera di consiglio del 23 maggio 2007 il Giudice relatore Alfio Finocchiaro. 
    Ritenuto che, con ordinanza del 17 febbraio 2006 (pervenuta alla Corte costituzionale il 20 novembre 2006), il Tribunale di Roma, sezione fallimentare, ha sollevato questione di legittimità costituzionale dell'art. 146, comma 3, lettera c), del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia – Testo A), nella parte in cui non include, tra le spese anticipate dall'Erario, qualora tra i beni compresi nel fallimento non vi sia denaro sufficiente per gli atti richiesti dalla legge, le spese e gli onorari liquidati al curatore, «ovvero» dell'art. 146, comma 3, lettera d), dello stesso d.P.R. n. 115 del 2002, nella parte in cui non include le spese anticipate dal curatore per ogni pubblicità, ivi comprese quelle relative a tutti gli avvisi inviati ai creditori, tra le «spese per gli strumenti di pubblicità di provvedimento dell'autorità giudiziaria», per violazione degli artt. 3 e 36 della Costituzione, nonché «sotto il profilo della violazione dell'art. 39 l. f. in relazione all'art. 36 Cost.»; 
    che, rileva il rimettente, l'art. 146, comma 3, lettera c), del d.P.R. n. 115 del 2002 stabilisce che sono a carico dell'Erario, e, quindi, da questo anticipati, le spese ed i compensi agli ausiliari del giudice nei casi in cui la procedura fallimentare sia priva di fondi necessari, senza nulla dire sui compensi ai curatori che abbiano prestato la propria attività nell'ambito di procedure cosiddette incapienti, nonché sulle spese per atti che essi obbligatoriamente devono compiere, quali le convocazioni delle parti, gli avvisi ai creditori, da ripetersi nelle varie ipotesi previste dalla legge, le spese di pubblicità relative a vendite anche di oggetti che poi si rivelino privi di economicità; 
    che, ad avviso del giudice a quo, soltanto un'interpretazione estensiva della norma censurata, che riconduca la figura del curatore nell'alveo della categoria di «ausiliario del giudice», garantendo ai curatori, indipendentemente dall'esistenza o meno di un attivo fallimentare sufficiente, l'effettiva remunerazione della carica, consentirebbe di superare il problema; 
    che tale interpretazione, peraltro, non sarebbe plausibile, atteso che quella del curatore è figura del tutto peculiare, essendo quest'ultimo titolare di specifici poteri e doveri – in ragione dell'eccezionalità della procedura fallimentare – di cui tutti gli ausiliari del giudice sono privi, trattandosi di un organo necessario, che esclude qualsiasi connotazione caratteristica di «ausiliarietà»; 
    che lo stesso legislatore elenca, con efficacia sicuramente tassativa, i soggetti che rientrano nel concetto di «ausiliario del magistrato» ai sensi dell'art. 3, lettera n), dello stesso d.P.R. n. 115/2002: il perito, il consulente tecnico, l'interprete, il traduttore e qualunque altro soggetto competente, in una determinata arte o professione o comunque idoneo al compimento di atti, che il magistrato o il funzionario addetto all'ufficio può nominare a norma di legge, e tra questi non è rinvenibile un richiamo applicabile al curatore; 
    che, secondo il giudice a quo, è evidente la rilevanza della questione sollevata, ai fini della decisione del reclamo a lui sottoposto, dal momento che, nel caso di specie, il curatore, non trovando nell'attivo fallimentare denaro sufficiente, non solo non ha ottenuto la richiesta liquidazione degli onorari, ma neppure il semplice rimborso delle rilevanti spese vive sostenute per atti doverosi, non essendovi alcuna norma che disponga in tal senso; 
    che neppure può ritenersi che il citato art. 146, comma 3, lettera d), sia riferibile anche alle spese per strumenti di «pubblicità degli avvisi» imposti dalla normativa in tema di espropriazione.  
    Considerato che il Tribunale di Roma, sezione fallimentare, dubita della legittimità costituzionale dell'art. 146, comma 3, lettera c), del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia – Testo A), in tema di patrocinio a spese dello Stato, nella parte in cui non include, tra le spese anticipate dall'Erario, qualora tra i beni compresi nel fallimento non vi sia denaro sufficiente per gli atti richiesti dalla legge, le spese e gli onorari liquidati al curatore; «ovvero» dell'art. 146, comma 3, lettera d), dello stesso d.P.R., nella parte in cui non include, tra dette spese, quelle anticipate dal curatore per ogni pubblicità, ivi incluse quelle relative a tutti gli avvisi inviati ai creditori: per violazione dell'art. 3 della Costituzione, perché il curatore fallimentare rimarrebbe l'unico soggetto, in caso di fallimento privo di attivo, a non essere retribuito per l'attività svolta, determinandosi così una disparità di trattamento rispetto a tutti gli altri soggetti che prestano la propria opera a favore della massa – stimatori, consulenti contabili e fiscali, notai, avvocati, etc. – e che vengono retribuiti con compensi posti a carico dell'Erario; nonché per contrasto con l'art. 39 del Regio decreto 16 marzo 1942, n. 267 (Disciplina del fallimento, del concordato preventivo, dell'amministrazione controllata e della liquidazione coatta amministrativa), che stabilisce il principio della remuneratività dell'incarico di curatore fallimentare, in relazione all'art. 36 della Costituzione, perché quest'ultimo – svolgendo un'attività avente carattere professionale e rientrando quindi nell'ambito dei soggetti che svolgono una professione intellettuale di cui agli artt. 2229 e segg. del codice civile, – rientrerebbe nel concetto di «lavoratore» di cui all'art. 36 della Costituzione, cui deve essere riconosciuto il diritto alla retribuzione, proporzionata alla qualità ed alla quantità del lavoro svolto; 
    che – a prescindere dal fatto che, successivamente all'ordinanza di rimessione questa Corte, con sentenza n. 174 del 2006, ha dichiarato l'illegittimità costituzionale dell'art. 146, comma 3, del d.P.R. 30 maggio 2002, n. 115 nella parte in cui non prevede che sono spese anticipate dall'Erario «le spese ed onorari» al curatore – la questione risulta formulata in termini di alternativa irrisolta tra le due auspicate soluzioni – l'incostituzionalità dell'art. 146, comma 3, lettera c) «ovvero» dell'art. 146, comma 3, lettera d) del citato d.P.R. – e, dunque, in forma ancipite; 
    che tale formulazione comporta, per costante giurisprudenza di questa Corte (ex plurimis, ordinanze n. 62 del 2007; n. 363 del 2005), la manifesta inammissibilità della questione; 
    che la pronuncia di manifesta inammissibilità, per la formulazione ancipite della questione, è logicamente preliminare rispetto alla restituzione degli atti al giudice a quo, per la valutazione della già menzionata sentenza n. 174 del 2006, in quanto la pronuncia di inammissibilità per l'imprecisa indicazione del petitum da parte del rimettente si risolve nell'assoluta impossibilità di individuare il thema decidendum, mentre la restituzione degli atti presuppone la corretta prospettazione dei termini della questione ed ha l'esclusiva finalità di consentire al giudice a quo una nuova valutazione circa la (perdurante) rilevanza della questione medesima (ordinanza n. 289 del 2006, in un caso di ius superveniens). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 146, comma 3, lettera c), del d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia – Testo A) «ovvero» dell'art. 146, comma 3, lettera d) dello stesso d.P.R. n. 115 del 2002, sollevata dal Tribunale di Roma, sezione fallimentare, in riferimento agli articoli 3 e 36 della Costituzione, nonché «sotto il profilo della violazione dell'art. 39 l. f. in relazione all'art. 36 Cost.», con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 luglio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfio FINOCCHIARO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 20 luglio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
