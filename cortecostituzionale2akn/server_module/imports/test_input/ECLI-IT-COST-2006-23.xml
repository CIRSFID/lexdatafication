<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/23/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/23/"/>
          <FRBRalias value="ECLI:IT:COST:2006:23" name="ECLI"/>
          <FRBRdate date="23/01/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="23"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/23/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/23/ita@/!main"/>
          <FRBRdate date="23/01/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/23/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/23/ita@.xml"/>
          <FRBRdate date="23/01/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="27/01/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>23/01/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 415–bis del codice di procedura penale, promosso con ordinanza dell'11 febbraio 2005 dal Tribunale di Pistoia – sezione distaccata di Monsummano Terme, nel procedimento penale a carico di G.F. iscritta al n. 229 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 18, prima serie speciale, dell'anno 2005. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 16 novembre 2005 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che, con l'ordinanza in epigrafe, il Tribunale di Pistoia, sezione distaccata di Monsummano Terme, ha sollevato, in riferimento agli artt. 24, secondo comma, e 111, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 415-bis del codice di procedura penale, «nella parte in cui non prevede alcuna sanzione processuale per l'ipotesi di notifica dell'avviso di conclusione delle indagini preliminari oltre il termine di cui al comma 1 dello stesso art. 415-bis c.p.p., nel caso in cui la predetta notifica non sia stata preceduta dalla contestazione dell'addebito e non sussista l'effettiva necessità del compimento di altri atti investigativi, e ciò – quantomeno – nel caso in cui, come nella specie, in relazione alle particolari modalità di accertamento del fatto, sorga in dibattimento, per l'imputato, lo specifico onere di allegare e provare elementi a suo discarico, e l'assolvimento di tale onere sia reso, in misura apprezzabile, più gravoso, a causa dell'eccessiva ampiezza del lasso di tempo intercorso tra il momento dell'asserita commissione del reato e quello della tardiva effettuazione della prima contestazione dell'addebito, effettuata mediante la notifica dell'avviso di cui all'art. 415-bis c.p.p.»;  
    che il giudice a quo – premesso di procedere nei confronti di persona imputata del reato di evasione, per essere stata riconosciuta dai Carabinieri, il 21 febbraio 2001, fuori dall'abitazione ove era ristretta agli arresti domiciliari – evidenzia che, nella fase delle indagini preliminari, il pubblico ministero rimaneva «del tutto inerte», omettendo qualunque approfondimento investigativo e limitandosi a notificare l'avviso di conclusione delle indagini preliminari non solo oltre il termine di sei mesi richiamato dall'art. 415-bis, comma 1, cod. proc. pen., ma, addirittura, oltre il termine di durata massima delle stesse, pari, nel caso di specie, a diciotto mesi; 
    che, a parere del rimettente, da tale ritardo nella notifica dell'avviso di cui all'art. 415-bis cod. proc. pen. sarebbe derivata una compromissione del diritto di difesa, con conseguente violazione dell'art. 24, secondo comma, della Costituzione, non potendo l'imputato – per di più tossicodipendente e quindi in condizioni di «degrado psico-fisico» – essere ragionevolmente in grado, a distanza di oltre quattro anni, di ricostruire gli accadimenti del giorno di presunta commissione del delitto di evasione; 
    che risulterebbe altresì vulnerato il principio sancito dall'art. 111, terzo comma della Costituzione, a norma del quale la legge deve assicurare che la persona accusata di un reato disponga del tempo e delle condizioni necessarie per preparare la sua difesa: espressione, quest'ultima, la quale postula che il cittadino, destinatario dell'indagine, deve “nel più breve tempo possibile” – come recita il primo periodo del terzo comma, dell'art. 111 Cost. – essere posto «in grado di organizzare in modo adeguato la propria difesa, e ciò, in primis, raccogliendo gli elementi a suo discarico»; 
    che in proposito – sottolinea ancora il giudice a quo – se è vero che il diritto di difesa deve essere contemperato con le esigenze di interesse pubblico connesse all'accertamento dei reati, è altrettanto indubbia, nella specie, «l'inesistenza di qualsivoglia ulteriore necessità di indagine», una volta effettuato il “riconoscimento” in flagranza di reato da parte della polizia giudiziaria; 
    che, in punto di rilevanza, il Tribunale rimettente afferma conclusivamente che, in caso di accoglimento della questione, «verrebbe a crearsi, in dibattimento, una situazione del tutto lineare: la difesa potrebbe eccepire il mancato rispetto del termine di cui all'art. 415-bis c.p.p., l'accusa sarebbe tenuta a giustificare il ritardo mediante la dimostrazione di specifiche esigenze investigative, ed il giudicante, infine, sarebbe posto in grado di sindacare la fondatezza delle argomentazioni offerte dall'accusa, con la conseguente applicazione o no – a seconda dell'esito di tale sindacato – della sanzione processuale indicata da Codesta Corte nell'eventuale declaratoria di incostituzionalità»; 
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto dichiararsi inammissibile o in subordine infondata la questione: essa sarebbe, infatti, priva del requisito della rilevanza, posto che il ritardo nella notifica dell'avviso di conclusione delle indagini non avrebbe potuto comunque compromettere il diritto di richiedere in dibattimento le prove;  
    che in ogni caso – osserva conclusivamente l'Avvocatura – il diritto di difesa è diversamente strutturato in ragione delle diverse fasi processuali: sicché, essendo il dibattimento deputato alla attività di formazione della prova, l'eventuale ritardo nella notifica dell'avviso di cui all'art. 415-bis cod. proc. pen. lascerebbe «integro il diritto di difesa dell'imputato». 
    Considerato che il Tribunale di Pistoia, sezione distaccata di Monsummano Terme, dubitando della legittimità costituzionale dell'art. 415-bis del codice di procedura penale, «nella parte in cui non prevede alcuna sanzione processuale per l'ipotesi di notifica dell'avviso di conclusione delle indagini preliminari oltre il termine di cui al comma 1 dello stesso art. 415 bis c.p.p.», articola il quesito di costituzionalità in una gamma di eventualità e condizioni tali da renderlo pertinente alla sola fattispecie dedotta; 
    che, infatti, la censura della norma in questione è riferita, innanzitutto, alla sola ipotesi in cui l'avviso di conclusione delle indagini preliminari non sia stato preceduto da altra forma di contestazione dell'addebito all'indagato; ed, inoltre, all'ulteriore condizione che non sussista la effettiva necessità del compimento di altri atti investigativi, sì da rendere immotivati l'inerzia ed il ritardo del pubblico ministero;  
    che, quale ulteriore premessa, il giudice a quo auspica che, per un verso, l'incostituzionalità operi «quantomeno» nelle ipotesi in cui sorga per l'imputato in dibattimento «lo specifico onere di allegare e provare elementi a suo discarico»; e che, per un altro verso, assume che tale onere sia reso «in misura apprezzabile, più gravoso, a causa dell'eccessiva ampiezza del lasso temporale intercorso tra il momento dell'asserita commissione del reato e quello della tardiva effettuazione della prima contestazione dell'addebito», realizzata mediante la notifica dell'avviso di cui all'art. 415-bis cod. proc. pen.; 
    che, in tal modo, la questione di illegittimità costituzionale della norma impugnata viene invocata in termini sostanzialmente “descrittivi” della particolare vicenda processuale all'esame del rimettente, il quale sollecita, in realtà, più che la soluzione di un dubbio di compatibilità costituzionale, una decisione da adattare al caso di specie; 
    che – sotto un diverso, ma limitrofo, profilo – la questione così come prospettata ha indiscutibile origine da una situazione additata dallo stesso Tribunale rimettente come patologica e non attinente al normale funzionamento della disciplina processuale denunciata: laddove solo la corretta applicazione delle norme – e non certo un uso distorto di esse, con le consequenziali patologie di fatto derivanti – può essere alla base dello scrutinio di legittimità costituzionale; 
    che, infine, il giudice rimettente, con la pronuncia additiva sollecitata, ha integralmente devoluto a questa Corte la scelta del tipo di sanzione processuale da configurare, in presenza del superamento del limite temporale per la notifica dell'avviso di conclusione delle indagini preliminari di cui all'art. 415-bis cod. proc. pen.: una scelta, per di più, da calibrare in funzione delle stesse variabili con cui è costruito il quesito; così promuovendo un petitum che, rimettendo al giudice delle leggi il compito di scegliere ed addirittura plasmare un nuovo rimedio processuale, risulta del tutto indeterminato; 
    che pertanto la questione proposta risulta, sotto molteplici prospettive, manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 415-bis del codice di procedura penale, sollevata, in riferimento agli artt. 24, secondo comma, e 111, terzo comma, della Costituzione, dal Tribunale di Pistoia, sezione distaccata di Monsummano Terme, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta il 23 gennaio 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 27 gennaio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
