<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2012/175/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2012/175/"/>
          <FRBRalias value="ECLI:IT:COST:2012:175" name="ECLI"/>
          <FRBRdate date="02/07/2012" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="175"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2012/175/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2012/175/ita@/!main"/>
          <FRBRdate date="02/07/2012" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2012/175/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2012/175/ita@.xml"/>
          <FRBRdate date="02/07/2012" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/07/2012" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2012</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>QUARANTA</cc:presidente>
        <cc:relatore_pronuncia>Giorgio Lattanzi</cc:relatore_pronuncia>
        <cc:data_decisione>02/07/2012</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alfonso QUARANTA; Giudici : Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 335 del codice penale, promosso dal Tribunale di Napoli nel procedimento penale a carico di E.E. ed altro, con ordinanza depositata il 24 novembre 2011, iscritta al n. 25 del registro ordinanze 2012 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 9, prima serie speciale, dell'anno 2012.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 6 giugno 2012 il Giudice relatore Giorgio Lattanzi.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che il Tribunale di Napoli in composizione monocratica, con ordinanza depositata il 24 novembre 2011 (r.o. n. 25 del 2012), ha sollevato, in riferimento all'articolo 3 della Costituzione, questione di legittimità costituzionale dell'articolo 335 del codice penale «limitatamente ai casi in cui punisce penalmente la colposa agevolazione della sottrazione da parte di un custode che, se avesse compiuto dolosamente e direttamente la medesima sottrazione, non sarebbe andato incontro ad alcuna sanzione penale per avvenuta depenalizzazione della relativa condotta»;
 che il rimettente premette di procedere nei confronti di due imputati ai quali, in seguito alla modifica dell'imputazione avvenuta nell'udienza del 5 aprile 2011, era stato contestato il delitto previsto dall'art. 335 cod. pen., perché, per colpa, avevano agevolato la sottrazione di un ciclomotore del quale uno di essi era stato nominato custode dopo un sequestro amministrativo;
 che il giudice a quo richiama la sentenza delle sezioni unite della Corte di cassazione n. 1963/11 del 28 ottobre 2010, secondo la quale la condotta di chi circola abusivamente con un veicolo sottoposto a sequestro amministrativo integra il solo illecito amministrativo di cui all'art. 213 del codice della strada (decreto legislativo 30 aprile 1992, n. 285) e non già il reato previsto dall'art. 334 cod. pen., dato il carattere di specialità rivestito dalla prima norma rispetto alla seconda, ai sensi dell'art. 9 della legge 24 novembre 1981, n. 689 (Modiche al sistema penale);
 che, secondo il rimettente, mentre «la condotta di circolazione di un veicolo sottoposto a sequestro amministrativo è ormai mero illecito amministrativo ex art. 213 cit. sia che venga commessa dal custode o custode-proprietario, sia (e a maggior ragione) che venga commessa da un soggetto privo di tale qualifica», non potrebbe invece ritenersi depenalizzata la condotta riconducibile all'art. 335 cod. pen. quando l'agevolazione colposa riguardi un veicolo messo in circolazione nonostante il sequestro amministrativo in corso;
 che, infatti, «l'esclusione della norma penale ex art. 334 c.p. con riguardo alla circolazione di un veicolo sequestrato deriva solo ed esclusivamente dal concorso prevalente di una norma speciale che, sanzionando amministrativamente l'identica condotta della "circolazione-sottrazione", sterilizza l'efficacia applicativa della norma incriminatrice», laddove identica norma non esiste per l'ipotesi di agevolazione colposa della sottrazione;
 che, osserva ancora il giudice a quo, gli artt. 334 e 335 cod. pen. «viaggiano su due binari (talvolta) distinti. E se spesso l'art. 335 c.p. finisce per punire il concorso colposo (del custode) in un fatto doloso (del proprietario), suscettibile quest'ultimo di essere ricondotto all'art. 334 c.p., nulla toglie che l'incriminazione possa prescindere da una specifica rilevanza penale del fatto agevolato, per incentrare il disvalore sanzionato sulla sola omessa diligenza imputabile al custode»;
 che la questione non sarebbe manifestamente infondata perché l'assetto normativo venutosi a creare dopo la citata pronuncia delle sezioni unite della Corte di cassazione risulterebbe in contrasto con l'art. 3 Cost., in quanto, in applicazione del combinato disposto degli artt. 334 cod. pen. e 213 del d.lgs. n. 285 del 1992 (indicato dal rimettente quale tertium comparationis), il custode che circoli con un veicolo sottoposto a sequestro amministrativo o concorra dolosamente nella circolazione operata da altri (mediante affidamento volontario e consapevole del veicolo stesso) è soggetto alla sola sanzione amministrativa, laddove «il custode che per mera negligenza consenta ad altri di circolare con un veicolo sotto sequestro realizza (ancora oggi) il più grave illecito penale di cui all'art. 335 c.p., essendo esposto addirittura alla pena detentiva, per quanto in alternativa alla pecuniaria»;
 che, secondo il giudice a quo, il diverso trattamento punitivo «non appare sorretto da valori rispondenti ad un principio di ragionevolezza legislativa, essendo immanente nel nostro sistema il criterio generale per cui la condotta colposa esprime un disvalore nettamente meno grave della condotta dolosa» e alla luce dell'orientamento della giurisprudenza costituzionale secondo cui la discrezionalità del legislatore può formare oggetto del sindacato di legittimità costituzionale qualora si traduca in scelte manifestamente irragionevoli o arbitrarie; 
 che, inoltre, la diversità di trattamento non sarebbe ragionevole anche per l'identità del bene giuridico tutelato dalle due fattispecie previste dagli artt. 334 e 335 cod. pen.; 
 che tali argomentazioni sarebbero ulteriormente rafforzate dall'intervenuta depenalizzazione della fattispecie prevista dall'art. 350 cod. pen.; 
 che il giudice rimettente infine riferisce di non essere in grado di individuare «una strada ermeneuticamente sostenibile» che consenta di non applicare l'art. 335 cod. pen., dato che questa norma punisce l'agevolazione colposa di "qualsiasi sottrazione", anche di quelle ormai sanzionate solo in via amministrativa;
 che è intervenuto nel giudizio di costituzionalità il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha chiesto alla Corte di dichiarare l'infondatezza della questione, osservando che le argomentazioni del rimettente «si fondano esclusivamente su una interpretazione giurisprudenziale (...) della norma, che allo stato permane vigente ed anche suscettibile in futuro di una diversa interpretazione alla luce dei precedenti orientamenti molto oscillanti e dibattuti»;
 che, ad avviso dell'Avvocatura dello Stato, compete al legislatore nella sua ampia discrezionalità intervenire in modo razionale ed organico con scelte di depenalizzazione (già compiute, ad esempio, con riferimento all'art. 350 cod. pen.), idonee a garantire una ragionevole risposta sanzionatoria, coerente con il bene giuridico tutelato.
 Considerato che il Tribunale di Napoli, in composizione monocratica, ha sollevato, in riferimento all'articolo 3 della Costituzione, questione di legittimità costituzionale dell'articolo 335 del codice penale «limitatamente ai casi in cui punisce penalmente la colposa agevolazione della sottrazione da parte di un custode che, se avesse compiuto dolosamente e direttamente la medesima sottrazione, non sarebbe andato incontro ad alcuna sanzione penale per avvenuta depenalizzazione della relativa condotta»;
 che la questione è manifestamente inammissibile;
 che un'identica questione è stata esaminata da questa Corte, che, con la sentenza n. 58 del 2012 (successiva all'ordinanza del Tribunale di Napoli), l'ha dichiarata inammissibile «perché il giudice rimettente non ha preso in considerazione la possibilità di dare alla disposizione censurata un'interpretazione idonea a superare i prospettati dubbi di costituzionalità», avendo omesso di «verificare se il custode che abbia colposamente agevolato la circolazione abusiva di un veicolo sottoposto a sequestro amministrativo possa rispondere, ai sensi del combinato disposto degli artt. 5 della legge n. 689 del 1981 e 213, comma 4, del d.lgs. n. 285 del 1992, di concorso colposo nell'illecito amministrativo altrui, invece che dell'autonomo reato di violazione colposa dei doveri inerenti alla custodia di cose sottoposte a sequestro, previsto dall'art. 335 cod. pen.»; 
 che la sentenza n. 58 del 2012 ha rilevato che il tribunale rimettente avrebbe dovuto «verificare se il custode di un veicolo sottoposto a sequestro amministrativo che, per colpa, ne agevoli la circolazione abusiva da parte di un terzo, possa essere chiamato a rispondere - ai sensi del combinato disposto degli artt. 5 della legge n. 689 del 1981 e 213, comma 4, del d.lgs. n. 285 del 1992 - a titolo di concorso colposo nell'illecito amministrativo commesso dal terzo; il che farebbe escludere, nel caso di specie, la configurabilità dell'autonomo reato di violazione colposa dei doveri inerenti alla custodia di cose sottoposte a sequestro di cui all'art. 335 cod. pen. (Corte di cassazione, sezione sesta penale, 17 gennaio - 16 febbraio 2012, n. 6221)» e che «con tale interpretazione, rispetto al custode di un veicolo sottoposto a sequestro amministrativo l'assetto normativo venutosi a delineare a seguito della sentenza delle sezioni unite della Corte di cassazione del 28 ottobre 2010, n. 1963/2011, si sottrarrebbe alla censura di violazione dell'art. 3 Cost., non risultando affetto da irragionevolezza, e potrebbe invece essere ricondotto ai principi generali in materia di concorso di persone nell'illecito amministrativo dettati dalla legge n. 689 del 1981»;
 che analoghi rilievi possono essere formulati anche in relazione alla questione sollevata dall'attuale rimettente, il quale non ha preso in considerazione la possibilità di dare alla disposizione censurata un'interpretazione, indicata nella ricordata sentenza n. 58 del 2012 di questa Corte, idonea a superare i dubbi prospettati.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 335 del codice penale, sollevata, in riferimento all'articolo 3 della Costituzione, dal Tribunale di Napoli, con l'ordinanza indicata in epigrafe. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 2 luglio 2012.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Giorgio LATTANZI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 6 luglio 2012.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
