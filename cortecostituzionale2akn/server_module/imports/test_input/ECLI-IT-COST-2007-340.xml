<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/340/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/340/"/>
          <FRBRalias value="ECLI:IT:COST:2007:340" name="ECLI"/>
          <FRBRdate date="08/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="340"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/340/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/340/ita@/!main"/>
          <FRBRdate date="08/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/340/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/340/ita@.xml"/>
          <FRBRdate date="08/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/10/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>08/10/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 13, comma 2, del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'articolo 12 della legge 3 ottobre 2001, n. 366), promosso dal Tribunale di Catania, nel procedimento civile vertente tra C. M. ed altro e la Banca Monte dei Paschi di Siena s.p.a., con ordinanza del 17 gennaio 2006 iscritta al n. 240 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 29, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 6 giugno 2006 il Giudice relatore Francesco Amirante.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.–– Nel corso di un giudizio civile promosso da due soggetti privati contro un istituto di credito per la nullità di un contratto di acquisto di titoli mobiliari e per il rimborso delle perdite subite, il Tribunale di Catania ha sollevato, in riferimento agli artt. 3, 24 e 76 della Costituzione, questione di legittimità costituzionale dell'art. 13, comma 2, del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'articolo 12 della legge 3 ottobre 2001, n. 366). 
    Rileva il Tribunale che, a seguito della notifica dell'atto di citazione avvenuta il 1° aprile 2005, la banca convenuta ha notificato la propria comparsa di risposta il successivo 1° giugno 2005; in data 16 giugno 2005 gli attori, eccependo la tardività della notifica della comparsa, hanno notificato alla controparte l'istanza di fissazione di udienza, chiedendo al Tribunale di considerare non contestati i fatti così come narrati nell'atto di citazione. 
    Ciò premesso, il giudice a quo osserva che la disposizione impugnata effettivamente ricollega alla contumacia del convenuto (cui viene equiparata la tardiva costituzione) l'effetto di una sorta di ficta confessio, dovendosi intendere come non contestati i fatti affermati dall'attore, in tal modo innovando rispetto alla consolidata giurisprudenza per cui la contumacia nel processo civile non può assumere alcun significato probatorio. Tale scelta legislativa, peraltro, appare in contrasto, anzitutto, con l'art. 76 Cost., in quanto nell'art. 12, comma 2, lettera a), della legge n. 366 del 2001 manca ogni riferimento al rito contumaciale. Richiamando, in proposito, alcune sentenze di questa Corte sulla necessità che la legge di delegazione venga interpretata tenendo presenti le finalità ispiratrici della medesima, il Tribunale di Catania sottolinea che la riforma del rito contumaciale operata dalla norma in esame non risponde, se non per «mero accidente processuale», alla finalità di riduzione dei termini processuali, il che risulterebbe ancora più evidente in un processo con più convenuti dei quali almeno uno si sia costituito tempestivamente. E, d'altra parte, nessuna volontà di riforma dell'istituto della contumacia trapela dai lavori parlamentari, poiché la relazione di accompagnamento al disegno di legge delega per la riforma del diritto societario (presentato il 3 luglio 2001) non contiene alcun riferimento alla materia in oggetto; al contrario, un preciso richiamo alla contumacia è presente nel punto 23 del disegno di legge delega per la complessiva riforma del processo civile approvato dal Consiglio dei ministri in data 24 ottobre 2003. 
    Ritiene quindi il giudice remittente, per le ragioni indicate, che la norma in esame sia censurabile sotto il profilo dell'eccesso di delega. 
    Il meccanismo della ficta confessio previsto dall'impugnato art. 13, comma 2, in caso di tardiva notifica della comparsa di risposta appare al Tribunale di Catania, «in via subordinata», in contrasto anche con l'art. 3 Cost., in quanto contrario al canone della ragionevolezza, poiché attribuisce all'attore un privilegio processuale non riscontrabile in nessuno degli altri riti regolati dal nostro sistema processuale; e tale disparità non potrebbe trovare giustificazione neppure nella peculiarità delle controversie destinate ad essere trattate col cosiddetto rito societario, poiché l'art. 70-ter delle disposizioni di attuazione del codice di procedura civile consente, nell'accordo delle parti, che tale rito si applichi anche ai processi ordinari. 
    In via «ulteriormente gradata», infine, il Tribunale di Catania ravvisa un contrasto tra la censurata disposizione e l'art. 24 Cost., in quanto la «secca previsione normativa della non contestabilità dei fatti affermati dall'attore in caso di tardiva notificazione della comparsa di risposta» costituirebbe una sanzione processuale sproporzionata del comportamento del convenuto che, come nel caso di specie, ha notificato la propria comparsa di risposta con un solo giorno di ritardo rispetto al termine fissato per legge.  
    Nel rito in esame, infatti, non è neppure previsto l'obbligo (si veda l'art. 2, comma 1, lettera a, del d.lgs. n. 5 del 2003) che l'atto introduttivo contenga l'avvertimento al convenuto circa le conseguenze negative che si possono produrre a suo carico in caso di contumacia o tardiva costituzione. Ciò comporta, secondo il Tribunale, una lesione del diritto di difesa del convenuto. 
    Quanto alla rilevanza, infine, il giudice a quo osserva che essa senza dubbio sussiste nel giudizio pendente, poiché si deve stabilire se la tardiva notifica della comparsa di risposta determini o meno gli effetti di non contestazione fissati dalla disposizione sottoposta a scrutinio. 
    2.— È intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'infondatezza della questione. 
    Osserva l'Avvocatura dello Stato, richiamando numerose pronunce di questa Corte in ordine alla censura di eccesso di delega, che è da escludere la violazione dell'art. 76 della Costituzione. Rientra, a suo dire, nella fisiologia della delega legislativa il fatto che la legge si limiti a contenere i principi ed i criteri direttivi senza regolare integralmente tutti gli aspetti della fattispecie, sussistendo nel Governo delegato il potere di “riempimento” che la giurisprudenza costituzionale ha in più occasioni riconosciuto. La delega, d'altronde, non può eliminare ogni margine di scelta nel momento della sua attuazione, anche perché accade di frequente che il legislatore delegante faccia espresso riferimento a concetti come “clausole generali”, “ridefinizione”, “riordino” e “razionalizzazione” (sentenza n. 125 del 2003), indicando in tal modo criteri generici ma tuttavia sufficienti a delimitare il compito del legislatore delegato. 
    La norma impugnata non può, alla luce di siffatte considerazioni, essere considerata illegittima, perché l'art. 12, comma 2, della legge n. 366 del 2001 contiene criteri idonei e determinati: in esso si fa riferimento all'esigenza di una più rapida definizione dei procedimenti nelle materie ivi indicate, sicché non può lamentarsi una violazione dell'art. 76 Cost. 
    Sarebbe improprio, secondo l'Avvocatura dello Stato, invocare l'art. 3 Cost. per paragonare il trattamento riservato al contumace nel rito ordinario con quello regolato dalla disposizione in esame, perché la delega non ha vincolato il Governo a rispettare, in tutto e per tutto, l'ideologia che animava il codice di procedura vigente; anzi, ai fini della concentrazione del procedimento e della riduzione della sua durata complessiva, era necessario creare un modello processuale più agile, tale da liberare il giudice da una serie di impegni ripetitivi ed inutili; in vista di quest'obiettivo, tra l'altro, si è ritenuto opportuno affidare la fase iniziale del procedimento alla disponibilità delle parti, escludendo ogni intervento del giudice. 
    Sotto questo profilo, quindi, dovrebbe dirsi che la scelta di regolare diversamente l'istituto della contumacia si sia tradotta in un'attuazione piena della delega, contribuendo a determinare una maggiore concentrazione del procedimento ed una conseguente riduzione dei suoi tempi, anche in considerazione del fatto che il rito è destinato ad operare in controversie «che, per loro natura, necessitano di soluzioni immediate e che, di norma, non richiedono una complessa istruttoria».Considerato in diritto1.–– Il Tribunale di Catania, in composizione collegiale, ha sollevato, in riferimento all'art. 76 della Costituzione, in via subordinata in riferimento all'art. 3 Cost. e, in via ancor più gradata, in riferimento all'art. 24 Cost., questione di legittimità costituzionale dell'art. 13, comma 2, del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'articolo12 della legge 3 ottobre 2001, n. 366). 
    Il remittente riferisce che soggetti privati hanno convenuto in giudizio un istituto bancario per sentir dichiarare la nullità di un contratto di acquisto di titoli mobiliari con esso concluso e per la condanna al risarcimento dei danni subiti per la dismissione dei medesimi; che il convenuto ha notificato in ritardo la comparsa di costituzione e gli attori hanno presentato istanza di fissazione dell'udienza, la quale, ai sensi della disposizione censurata, comporta che i fatti dedotti dagli attori devono ritenersi come ammessi. 
    Secondo il remittente, nello stabilire la cosiddetta ficta confessio in caso di mancata o tardiva notifica della suddetta comparsa, il legislatore delegato è andato al di là della delega di cui all'art. 12, comma 2, lettera a), della legge n. 366 del 2001, la quale prevedeva soltanto la concentrazione dei procedimenti e la riduzione dei termini, ma non anche una così sostanziale modifica del procedimento contumaciale, contraria alla tradizione giuridica italiana. 
    Una innovazione come quella introdotta con la disposizione impugnata avrebbe richiesto una specifica direttiva, come è anche dimostrato dal fatto che nel disegno di legge di delega per la generale riforma del processo civile, approvato dal Consiglio dei ministri il 24 ottobre 2003 (Atto Camera n. 4578), al punto 23 è indicato come criterio direttivo quello cui è autonomamente, e quindi illegittimamente, ispirata la disposizione in scrutinio. 
    In via subordinata, il Tribunale di Catania deduce il contrasto della disposizione suddetta con l'art. 3 Cost., in quanto attribuisce un ingiustificato privilegio alla parte attrice nei procedimenti che si svolgono con il cosiddetto rito societario; in via ancor più gradata, il remittente lamenta la violazione dell'art. 24 Cost., in quanto dalla disposizione impugnata consegue l'irragionevole e perciò illegittima compressione del diritto di difesa della parte convenuta. 
    2.–– La questione è fondata con riferimento all'art. 76 della Costituzione. 
    Questa Corte ha più volte affermato che «il giudizio di conformità della norma delegata alla norma delegante, condotto alla stregua dell'art. 76 Cost., si esplica attraverso il confronto tra gli esiti di due processi ermeneutici paralleli: l'uno relativo alla norme che determinano l'oggetto, i principi  e i criteri direttivi indicati dalla delega, tenendo conto del complessivo contesto di norme in cui si collocano e individuando le ragioni e le finalità poste a fondamento della legge di delegazione; l'altro relativo alle norme poste dal legislatore delegato, da interpretarsi nel significato compatibile con i principi e criteri direttivi della delega» (ex plurimis sentenze n. 7 e n. 15 del 1999, n. 276, n. 163, n. 126, n. 425, n. 503 del 2000, n. 54 e n. 170 del 2007). E, in considerazione della varietà delle materie riguardo alle quali si può ricorrere alla delega legislativa, non è possibile enucleare una nozione rigida valevole per tutte le ipotesi di “principi e criteri direttivi”. In questo ordine d'idee si è anche affermato che «il Parlamento, approvando una legge di delegazione, non è certo tenuto a rispettare regole metodologicamente rigorose…» (sentenza n. 250 del 1991). 
    Siffatti principi, che la Corte ribadisce, vanno però applicati non disgiuntamente da altri che pure, come si è affermato, debbono presiedere allo scrutinio di legittimità costituzionale di disposizioni di provvedimenti legislativi delegati sotto il profilo della loro conformità alla legge di delegazione e che delimitano il cosiddetto potere di riempimento del legislatore delegato. Infatti, per quanta ampiezza possa a questo riconoscersi, «il libero apprezzamento del legislatore delegato non può mai assurgere a principio od a criterio direttivo, in quanto agli antipodi di una legislazione vincolata, quale è, per definizione, la legislazione su delega» (sentenza n. 68 del 1991; e, sul carattere derogatorio della legislazione su delega rispetto alla regola costituzionale di cui all'art. 70 Cost., cfr. anche la sentenza n. 171 del 2007). 
    Tutto ciò premesso, si rileva che la disposizione censurata – stabilendo che, se il convenuto non notifica la comparsa di risposta o lo fa tardivamente, i fatti affermati dall'attore si reputano non contestati – detta una regola del processo contumaciale in contrasto con la tradizione del diritto processuale italiano, nel quale alla mancata o tardiva costituzione mai è stato attribuito il valore di confessione implicita. 
    La legge di delegazione era finalizzata all'emanazione di norme che, senza modifiche della competenza per territorio o per materia, fossero dirette ad assicurare una più rapida ed efficace definizione di procedimenti in materia di diritto societario e di intermediazione finanziaria nonché in materia bancaria e creditizia (art. 12, comma 1, lettere a e b, della legge n. 366 del 2001). 
    Per raggiungere le suindicate finalità, si stabiliva che il Governo era delegato a dettare regole processuali che, in particolare, potessero prevedere «la concentrazione del procedimento e la riduzione dei termini processuali». 
    La censurata disposizione del decreto delegato, mentre è evidentemente estranea alla riduzione dei termini processuali, neppure può essere ritenuta conforme alla direttiva della concentrazione del procedimento. La considerazione della «più rapida ed efficace definizione dei procedimenti», indicata come finalità della delega, costituisce un utile criterio d'interpretazione sia della legge di delegazione, sia delle disposizioni delegate, ma non può sostituirsi alla valutazione dei principi e criteri direttivi, così come determinati dalla legge di delegazione. Tutto ciò anche a voler trascurare il rilievo secondo il quale non sempre l'introduzione della ficta confessio contribuisce alla rapida ed efficace definizione dei procedimenti. 
    L'accertamento della fondatezza della questione per violazione dell'art. 76 Cost. assorbe l'esame degli altri profili di illegittimità costituzionale, del resto dallo stesso remittente prospettati in via subordinata. 
    Deve essere, pertanto, dichiarata l'illegittimità costituzionale dell'art. 13, comma 2, del d.lgs. n. 5 del 2003, nella parte in cui stabilisce: «in quest'ultimo caso i fatti affermati dall'attore, anche quando il convenuto abbia tardivamente notificato la comparsa di costituzione, si intendono non contestati e il tribunale decide sulla domanda in base alla concludenza di questa».</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara l'illegittimità costituzionale dell'art. 13, comma 2, del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'articolo 12 della legge 3 ottobre 2001, n. 366), nella parte in cui stabilisce: «in quest'ultimo caso i fatti affermati dall'attore, anche quando il convenuto abbia tardivamente notificato la comparsa di costituzione, si intendono non contestati e il tribunale decide sulla domanda in base alla concludenza di questa». &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8 ottobre 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Francesco AMIRANTE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 12 ottobre 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
