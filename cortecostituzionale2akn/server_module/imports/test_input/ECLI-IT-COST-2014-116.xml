<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/116/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/116/"/>
          <FRBRalias value="ECLI:IT:COST:2014:116" name="ECLI"/>
          <FRBRdate date="05/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="116"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/116/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/116/ita@/!main"/>
          <FRBRdate date="05/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/116/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/116/ita@.xml"/>
          <FRBRdate date="05/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/05/2014" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2014</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>SILVESTRI</cc:presidente>
        <cc:relatore_pronuncia>Paolo Grossi</cc:relatore_pronuncia>
        <cc:data_decisione>05/05/2014</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Gaetano SILVESTRI; Giudici : Luigi MAZZELLA, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 2409 e 2476 del codice civile promosso dal Tribunale ordinario di Tivoli nel procedimento vertente tra Nicola Di Foggia ed altri e il Gruppo Agrantica Srl con ordinanza del 29 marzo 2012, iscritta al n. 236 del registro ordinanze 2013 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 45, prima serie speciale, dell'anno 2013.
          Visto l'atto di intervento del Presidente del Consiglio dei ministri;
          udito nella camera di consiglio del 26 marzo 2014 il Giudice relatore Paolo Grossi.
 Ritenuto che il Tribunale di Tivoli, con ordinanza del 29 marzo 2012, ha sollevato, in riferimento agli artt. 3 e 24 della Costituzione, questione di legittimità costituzionale degli artt. 2409 e 2476 del codice civile, nella parte in cui non consentono «l'utilizzo dello strumento del controllo giudiziario ex art. 2409 c. c. alle società a responsabilità limitata con finalità diverse da quelle di cui alla legge» 23 marzo 1981, n. 91 (Norme in materia di rapporti tra società e sportivi professionisti), «(cioè dalle attività sportive o a esse correlate)»; 
 che il Tribunale rimettente premette di essere investito dalla richiesta del collegio sindacale di una società a responsabilità limitata intesa a sollecitare l'adozione, a norma dell'art. 2409 cod. civ., dei provvedimenti necessari a carico della società, in ragione della situazione finanziaria del gruppo, degli impegni assunti e della condotta poco trasparente dell'amministratore, titolare anche di una quota consistente del capitale sociale, cui partecipano anche altri due soci; 
 che l'ordinanza di rimessione puntualizza come sia stata a lungo controversa in giurisprudenza la questione dell'ammissibilità della domanda di cui all'art. 2409 cod. civ. anche per le società a responsabilità limitata e come, alla luce della Relazione accompagnatoria al decreto legislativo 17 gennaio 2003, n. 6 (Riforma organica della disciplina delle società di capitali e società cooperative, in attuazione della legge 3 ottobre 2001, n. 366), risultasse evidente la volontà di escludere le società a responsabilità limitata dalla possibilità di attivare il controllo in questione, essendo la tutela dei soci direttamente esercitabile attraverso il promovimento di un'azione di responsabilità, con la correlativa possibilità di ottenere in quella sede la revoca dell'amministratore; 
 che il giudice rimettente dà atto che analoga questione di legittimità costituzionale è già stata scrutinata dalla Corte costituzionale nella sentenza n. 481 del 2005, la quale ha escluso la fondatezza della censura, limitata, peraltro, alla pretesa disparità di trattamento tra i soci di una società per azioni, per i quali la tutela ex art. 2409 cod. civ. era ammissibile, e quelli di una società a responsabilità limitata, per i quali, invece, essa era preclusa, essendo mancata l'occasione di pronunciarsi «in ordine alla ragionevolezza della discriminazione operata tra società a responsabilità limitata previste dalla legge 91/1981, il cui art. 13 ammette il controllo giudiziario ex art. 2409 c.c., e società a responsabilità limitata aventi un fine statutario o costitutivo diverso da quello indicato nella legge n. 91/1981»;
 che, ad avallare la tesi contraria all'ammissibilità del procedimento in questione per le società a responsabilità limitata, è sopraggiunta la sentenza della Corte di cassazione 13 gennaio 2010, n. 403, analiticamente descritta, pur segnalandosi come, anche dopo di questa, siano rimasti alcuni contrasti di giurisprudenza, specie in sede di merito, con pronunce anch'esse diffusamente prese in esame;
 che, secondo il rimettente, sarebbe impossibile accedere ad una interpretazione costituzionalmente orientata, dal momento che, oltre ai rilievi già posti in evidenza dalla Corte di cassazione nella predetta pronuncia, militerebbe la circostanza che il riferimento normativo con il quale l'art. 2477 rinviava alle disposizioni sulle società per azioni - ed attraverso il quale si poteva argomentare l'estensione dell'art. 2409 cod. civ. anche al collegio sindacale delle società a responsabilità limitata - sarebbe venuto meno nella nuova formulazione introdotta dall'art. 14, comma 13, della legge 12 novembre 2011, n. 183 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge di stabilità 2012);
 che ciò, dunque, legittimerebbe nuovi dubbi di costituzionalità, posto che negare ad un organo sindacale il potere di intervenire attraverso lo strumento offerto dall'art. 2409 cod. civ. su atti di mala gestio significherebbe esporlo «ad una responsabilità per fatto del terzo (l'amministratore), rispetto al quale non disporrebbe di alcun potere e, pertanto, ad una responsabilità che potrebbe addirittura considerarsi di natura oggettiva»;
 che, d'altra parte, «la possibilità di accesso» da parte dei soci alle informazioni non escluderebbe il rischio di abusi da parte dell'amministratore, specie se i soci siano consenzienti o se essi stessi siano anche amministratori, potendosi, in tal caso, generare un contrasto di interessi tra il patrimonio sociale e quello dei soci, con possibili ripercussioni per la società e per i terzi: ciò che renderebbe utile il ricorso, anche per le società diverse da quelle sportive, alla procedura di cui all'art. 2409 cod. civ. «al fine di garantire l'intervento di un controllore che agisca nell'interesse non dei soci bensì della società e soprattutto dei relativi creditori sociali»; 
 che tutto questo si tradurrebbe anche in una violazione del diritto di difesa da parte del collegio sindacale, senza che assuma risalto il riferimento al bilanciamento tra la impossibilità di esperire il rimedio di cui all'art. 2409 cod. civ. e l'accesso dei soci ai documenti sociali, affermato dalla richiamata sentenza della Corte costituzionale n. 481 del 2005, non essendosi questa pronuncia occupata del profilo relativo alla responsabilità del collegio sindacale nei confronti dei creditori e della disparità di trattamento nei confronti delle società a responsabilità limitata di natura sportiva;
 che, dunque, l'esclusione della possibilità di maggior tutela assicurata dal ricorso alla procedura di cui all'art. 2409 cod. civ., proprio nelle ipotesi in cui si crei un conflitto di interessi tra società e soci, parrebbe del tutto irragionevole;
 che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo, in via preliminare, la restituzione degli atti al giudice rimettente per ius superveniens e, in subordine, la declaratoria di infondatezza della questione; 
 che, infatti, secondo la difesa erariale, successivamente alla pronuncia dell'ordinanza di rimessione, l'art. 35, comma 2, lettere b) e c), del decreto-legge 9 febbraio 2012, n. 5 (Disposizioni urgenti in materia di semplificazione e di sviluppo), convertito, con modificazioni, dall'art. 1, comma 1, della legge 4 aprile 2012, n. 35, ha modificato l'art. 2477 cod. civ., il cui quinto comma ora recita: «Nel caso di nomina di un organo di controllo, anche monocratico, si applicano le disposizioni sul collegio sindacale previste per le società per azioni»; 
 che, perciò, si imporrebbe la restituzione degli atti per un riesame della rilevanza della questione, tenuto conto che l'interpretazione restrittiva circa l'applicabilità dell'art. 2409 cod. civ. alle società a responsabilità limitata derivava proprio dal tenore letterale dell'art. 2477 cod. civ. nel testo previgente;
 che, comunque, la questione sarebbe, nel merito, infondata, alla luce anche dei diversi passaggi delle vicende normative concernenti i controlli nelle società di capitali, ad opera, prima, della riforma del diritto societario attuata con d.lgs. n. 6 del 2003, poi dall'art. 14 della legge n. 183 del 2011 e, da ultimo, del citato d.l. n. 5 del 2012, nonché dei conseguenti orientamenti espressi dalla giurisprudenza di legittimità e della sentenza della Corte costituzionale n. 481 del 2005; 
 che, pertanto, i prospettati dubbi di legittimità costituzionale dovrebbero considerarsi superati sia dalla possibilità di operare un'interpretazione estensiva alla luce delle modifiche normative segnalate, sia dal riconoscimento - anche sulla base della richiamata sentenza della Corte di cassazione n. 403 del 2010 - della completezza della disciplina stabilita per le società a responsabilità limitata, oltre che delle differenze tra tale tipologia societaria e le società per azioni; 
 che, del resto, non si ravviserebbero lesioni sul piano costituzionale, potendo il singolo socio conseguire, in tempi rapidi, attraverso il procedimento cautelare, la revoca dell'amministratore ed essendo la presenza del collegio sindacale nella società a responsabilità a responsabilità limitata del tutto eventuale e, perciò, non indispensabile per la tutela della società, a differenza di quanto accade nella società per azioni e, inoltre, risultando il ruolo del revisore diversamente concepito rispetto a quello del sindaco, con la previsione di un diverso regime di controlli.
 Considerato che, con ordinanza del 29 marzo 2012, il Tribunale ordinario di Tivoli ha sollevato, in riferimento agli artt. 3 e 24 della Costituzione, questione di legittimità costituzionale degli artt. 2409 e 2476 del codice civile, nella parte in cui non consentono «l'utilizzo dello strumento del controllo giudiziario ex art. 2409 c.c. alle società a responsabilità limitata con finalità diverse da quelle di cui» alla legge 23 marzo 1981, n. 91 (Norme in materia di rapporti tra società e sportivi professionisti), vale a dire «dalle attività sportive o a esse correlate»; 
 che il Tribunale rimettente premette di essere investito a seguito della richiesta avanzata dai componenti del collegio sindacale di una società a responsabilità limitata, volta a sollecitare l'adozione, a norma dell'art. 2409 cod. civ., di opportuni provvedimenti a carico della società in questione, a causa della situazione finanziaria generata dalla condotta poco trasparente dell'amministratore, titolare anche di una quota consistente del capitale sociale, cui partecipano anche altri due soci;
 che il giudice rimettente sottolinea come, dopo un periodo di contrasto giurisprudenziale in ordine all'ammissibilità della domanda di cui all'art. 2409 cod. civ. anche per le società a responsabilità limitata, sia intervenuta una pronuncia della Corte di cassazione (sentenza 13 gennaio 2010, n. 403) che ne ha escluso l'ammissibilità per quel tipo di forma societaria;
 che, peraltro, un'eventuale interpretazione adeguatrice risulterebbe, nella specie, non praticabile, dal momento che, oltre ai rilievi della richiamata sentenza della Corte di cassazione, il rinvio previsto dall'art. 2477 cod. civ. alle disposizioni sulle società per azioni - attraverso il quale l'applicabilità dell'art. 2409 cod. civ. poteva considerarsi estesa al collegio sindacale delle società a responsabilità limitata - sarebbe venuto meno nella nuova formulazione introdotta dall'art. 14, comma 13, della legge 12 novembre 2011, n. 183 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge di stabilità 2012), a decorrere dal 1° gennaio 2012;
 che tutto ciò darebbe luogo, sotto un duplice e concorrente profilo, a dubbi di legittimità costituzionale non scrutinati da questa Corte nel giudizio definito con la sentenza n. 481 del 2005;
 che, infatti, da un lato, negare al collegio sindacale delle società a responsabilità limitata il potere di intervenire attraverso lo strumento offerto dall'art. 2409 cod. civ. su atti di mala gestio significherebbe esporlo «ad una responsabilità per fatto del terzo (l'amministratore), rispetto al quale non disporrebbe di alcun potere e, pertanto, ad una responsabilità che potrebbe addirittura considerarsi di natura oggettiva»; 
 che, d'altro canto, risulterebbero, in questa ipotesi, compromessi i parametri della ragionevolezza e del diritto di difesa, atteso che la possibilità di attivare il meccanismo del controllo da parte dei soci non rappresenterebbe una garanzia, né per la società né per i terzi, nelle ipotesi in cui i soci siano consenzienti rispetto alle scelte dell'amministratore o siano essi stessi anche amministratori;
 che risulterebbe, inoltre, irragionevole il diverso regime stabilito per le società sportive, stante l'identità della struttura societaria e l'impossibilità di giustificare tale divergenza in ragione della differente attività svolta;
 che la prospettazione del quadro normativo operata dal giudice a quo appare, per più versi, erronea e insufficiente, con la conseguenza di rendere la questione, per come proposta, non pertinente all'oggetto del decidere e allo stesso obiettivo perseguito dal provvedimento di rimessione;
 che, diversamente da quanto asserito, il testo della previsione di cui all'art. 14, comma 13, della richiamata legge n. 183 del 2011 - che, sostituendo l'art. 2477, quinto comma, cod. civ., enunciava il rinvio alle disposizioni sulle società per azioni - risultava, sul punto, invariato, al 1° gennaio 2012, rispetto a quello introdotto dall'art. 37, comma 26, del decreto legislativo 27 gennaio 2010, n. 39 (Attuazione della direttiva 2006/43/CE, relativa alle revisioni legali dei conti annuali e dei conti consolidati, che modifica le direttive 78/660/CEE e 83/349/CEE, e che abroga la direttiva 84/253/CEE), senza, del resto, variazioni rispetto ai testi dell'art. 2477 cod. civ. succedutisi a partire dalla riforma del 2003;
 che, oltre a ciò, il richiamato art. 2477 cod. civ. aveva, invece, subìto una significativa modifica, sicuramente innovativa in parte qua, ad opera del decreto-legge 9 febbraio 2012, n. 5 (Disposizioni urgenti in materia di semplificazione e di sviluppo), convertito, senza modificazioni per la parte che qui interessa, dall'art. 1, comma 1, della legge 4 aprile 2012, n. 35, entrato in vigore antecedentemente al deposito dell'ordinanza di rimessione ma da questa totalmente ignorato, posta l'espressa previsione, al quinto comma, che «Nel caso di nomina di un organo di controllo, anche monocratico, si applicano le disposizioni sul collegio sindacale previste per le società per azioni»;
 che tale inadeguata individuazione del contesto normativo incide in maniera decisiva sulla motivazione esibita per asseverare l'impossibilità di una interpretazione secundum constitutionem della disciplina denunciata oltre che la rilevanza della questione proposta;
 che, di conseguenza, la questione proposta deve essere dichiarata manifestamente inammissibile.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale degli artt. 2409 e 2476 del codice civile, sollevata, in riferimento agli artt. 3 e 24 della Costituzione, dal Tribunale ordinario di Tivoli con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 maggio 2014.&#13;
 F.to:&#13;
 Gaetano SILVESTRI, Presidente&#13;
 Paolo GROSSI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 7 maggio 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
