<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/160/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/160/"/>
          <FRBRalias value="ECLI:IT:COST:2014:160" name="ECLI"/>
          <FRBRdate date="21/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="160"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/160/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/160/ita@/!main"/>
          <FRBRdate date="21/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/160/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/160/ita@.xml"/>
          <FRBRdate date="21/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/06/2014" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2014</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>SILVESTRI</cc:presidente>
        <cc:relatore_pronuncia>Marta Cartabia</cc:relatore_pronuncia>
        <cc:data_decisione>21/05/2014</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Gaetano SILVESTRI; Giudici : Luigi MAZZELLA, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 8, comma 1, e 9, comma 2, della legge della Regione autonoma Sardegna 1° febbraio 2013, n. 2 (Autorizzazione all'intervento finanziario della SFIRS Spa per l'infrastrutturazione, il risparmio e l'efficientamento energetico dell'area industriale di Portovesme - Sulcis, incremento della dotazione finanziaria relativa agli interventi per il Parco geominerario e norme urgenti in materia di sostegno al reddito dei lavoratori in regime di ammortizzatori sociali), promosso dal Presidente del Consiglio dei ministri con ricorso notificato l'8-10 aprile 2013, depositato in cancelleria il 15 aprile 2013 ed iscritto al n. 53 del registro ricorsi 2013.
 Visto l'atto di costituzione della Regione autonoma Sardegna;
 udito nell'udienza pubblica del 6 maggio 2014 il Giudice relatore Marta Cartabia;
 uditi l'avvocato dello Stato Giovanni Palatiello per il Presidente del Consiglio dei ministri e l'avvocato Massimo Luciani per la Regione autonoma Sardegna.
 Ritenuto che, con ricorso notificato con il mezzo della posta l'8-10 aprile 2013 e depositato il successivo 15 aprile (reg. ric. n. 53 del 2013), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questione di legittimità costituzionale dell'intera legge della Regione autonoma Sardegna 1° febbraio 2013, n. 2 (Autorizzazione all'intervento finanziario della SFIRS Spa per l'infrastrutturazione, il risparmio e l'efficientamento energetico dell'area industriale di Portovesme - Sulcis, incremento della dotazione finanziaria relativa agli interventi per il Parco geominerario e norme urgenti in materia di sostegno al reddito dei lavoratori in regime di ammortizzatori sociali), per violazione degli artt. 81, quarto comma, e 117, terzo comma, della Costituzione;
 che, in particolare, la censura della difesa dello Stato si appunta sugli artt. 8, comma 1, e 9, comma 2, della legge reg. n. 2 del 2013 che provvedono alla copertura degli oneri derivanti, rispettivamente, dall'art. 7, comma 1, e dall'art. 9, con risorse a valere sulle unità previsionali di base (UPB) S04.06.005 e S06.06.004 del bilancio della Regione autonoma Sardegna per l'esercizio finanziario 2013;
 che tale copertura, così come prevista, risulterebbe inidonea per carenza assoluta del presupposto normativo autorizzatorio, considerato che, al momento della presentazione del ricorso, il bilancio regionale risultava non ancora approvato in via definitiva dal Consiglio regionale;
 che, di conseguenza, in forza delle disposizioni di cui alla legge della Regione autonoma Sardegna 17 dicembre 2012, n. 26 (Autorizzazione all'esercizio provvisorio del bilancio della Regione per l'anno 2013 e disposizioni urgenti) si è stabilito di provvedere alla copertura degli oneri, attraverso l'esercizio provvisorio del bilancio regionale, fino al 31 marzo 2013;
 che, in conclusione, il ricorrente sostiene che le disposizioni in esame si pongono in contrasto con il principio di equilibrio del bilancio di cui all'art. 81, quarto comma, Cost., nonché con quello del coordinamento della finanza pubblica di cui all'art. 117, terzo comma, Cost. e, pertanto, chiede che l'intera legge regionale sia dichiarata costituzionalmente illegittima;
 che nel giudizio dinanzi alla Corte si è costituita la Regione autonoma Sardegna, chiedendo che il ricorso sia dichiarato inammissibile e, comunque, non fondato;
 che la difesa regionale, dopo aver sostenuto l'inammissibilità del ricorso per la genericità delle censure, ne argomenta l'infondatezza;
 che la resistente ricorda che la copertura degli oneri previsti dalla legge attraverso il rinvio al bilancio annuale di previsione è prevista dalla legge della Regione autonoma Sardegna 2 agosto 2006, n. 11 (Norme in materia di programmazione, di bilancio e di contabilità della Regione autonoma della Sardegna. Abrogazione delle leggi regionali 7 luglio 1975, n. 27, 5 maggio 1983, n. 11 e 9 giugno 1999, n. 23);
 che tale modalità di copertura risulterebbe conforme all'art. 81, quarto comma, Cost., come dimostrerebbe la consolidata giurisprudenza della Corte costituzionale;
 che essa risulterebbe altresì conforme al decreto legislativo 28 marzo 2000, n. 76 (Princìpi fondamentali e norme di coordinamento in materia di bilancio e di contabilità delle regioni, in attuazione dell'articolo 1, comma 4, della legge 25 giugno 1999, n. 208), la cosiddetta legge generale sulla contabilità regionale;
 che, in conclusione, la difesa regionale rileva che lo stanziamento delle risorse nella legge finanziaria regionale e il conseguente inserimento delle somme in esame nel bilancio di previsione del 2013 determinerebbero la cessazione della materia del contendere, ovvero l'inammissibilità o l'improcedibilità del gravame per sopravvenuta carenza di interesse;
 che la medesima Regione autonoma, con memoria depositata in data 14 aprile 2014, ha ribadito tali rilievi;
 che, al riguardo, la resistente, in riferimento all'impugnazione dell'art. 8, comma 1, della legge reg. n. 2 del 2013, dà conto dell'approvazione, nelle more della trattazione del giudizio, della legge della Regione autonoma Sardegna 23 maggio 2013, n. 12 (Disposizioni per la formazione del bilancio annuale e pluriennale della Regione - legge finanziaria 2013), nonché della legge della Regione autonoma Sardegna 23 maggio 2013, n. 13 (Bilancio di previsione per l'anno 2013 e bilancio pluriennale per gli anni 2013-2015);
 che la resistente osserva, in particolare, che l'art. 5, comma 43, della legge reg. n. 12 del 2013 ha espressamente provveduto a fornire copertura alle spese derivanti dall'art. 7 della legge reg. n. 2 del 2013, mentre la legge reg. n. 13 del 2013 ha approvato il bilancio di previsione per l'anno 2013 e il bilancio pluriennale per gli anni 2013-2015, inserendo nelle UPB S04.06.005 e S06.06.004 le poste necessarie all'integrale copertura delle spese individuate dall'art. 8, comma 1, della legge impugnata;
 che la difesa regionale, in merito alla censura sull'art. 9, comma 2, della legge reg. n. 2 del 2013, afferma che gli oneri derivanti da tale disposizione sono stati oggetto di specifica copertura a seguito dell'approvazione della legge della Regione autonoma Sardegna 2 agosto 2013, n. 22 (Norme urgenti per l'attuazione dell'articolo 4 della legge regionale 29 aprile 2013, n. 10 - Disposizioni urgenti in materia di lavoro e nel settore sociale), e in particolare dell'art. 1, che ha modificato il bilancio della Regione Sardegna per gli anni 2013-2015, approvato con la citata legge reg. n. 13 del 2013, apportandovi le variazioni necessarie all'integrale copertura delle somme previste dall'art. 9, comma 2, della legge impugnata;
 che la memoria regionale conclude ribadendo che, in base alla giurisprudenza costituzionale più recente (sentenze n. 40 del 2014, n. 241 e n. 26 del 2013), l'intervenuta copertura finanziaria determinerebbe la cessazione della materia del contendere.
 Considerato che con il ricorso in epigrafe sono state promosse questioni di legittimità costituzionale degli artt. 8, comma 1, e 9, comma 2 della legge della Regione autonoma Sardegna 1° febbraio 2013, n. 2 (Autorizzazione all'intervento finanziario della SFIRS Spa per l'infrastrutturazione, il risparmio e l'efficientamento energetico dell'area industriale di Portovesme - Sulcis, incremento della dotazione finanziaria relativa agli interventi per il Parco geominerario e norme urgenti in materia di sostegno al reddito dei lavoratori in regime di ammortizzatori sociali) per violazione degli artt. 81, quarto comma, e 117, terzo comma, della Costituzione;
 che la legge della Regione autonoma Sardegna 22 marzo 2013, n. 6 (Proroga dell'autorizzazione all'esercizio provvisorio del bilancio della Regione per l'anno 2013), ha differito sino al 30 aprile 2013 il termine per l'esercizio provvisorio del bilancio per l'anno 2013, già autorizzato con legge della Regione autonoma Sardegna 17 dicembre 2012, n. 26 (Autorizzazione all'esercizio provvisorio del bilancio della Regione per l'anno 2013 e disposizioni urgenti);
 che, in seguito, l'art. 4 della legge della Regione autonoma Sardegna 29 aprile 2013, n. 10 (Disposizioni urgenti in materia di lavoro e nel settore sociale), ha soppresso l'art. 9, comma 1, della legge reg. Sardegna n. 2 del 2013 e ha destinato gli stanziamenti di cui al comma 2 del medesimo art. 9 all'attuazione di interventi contestualmente previsti dal citato art. 4 della legge reg. Sardegna n. 10 del 2013;
 che, successivamente, sono state approvate le leggi della Regione autonoma Sardegna 23 maggio 2013, n. 12 (Disposizioni per la formazione del bilancio annuale e pluriennale della Regione - legge finanziaria 2013), e n. 13 (Bilancio di previsione per l'anno 2013 e bilancio pluriennale per gli anni 2013-2015);
 che l'art. 5, comma 43, della legge reg. Sardegna n. 12 del 2013 ha previsto che gli oneri derivanti dall'applicazione dell'art. 7, commi 1 e 2, della legge reg. Sardegna n. 2 del 2013 trovino copertura a valere sugli stanziamenti iscritti nella UPB S04.06.005 e nella UPB S06.06.004;
 che l'art. 6 della legge reg. Sardegna n. 13 del 2013 ha autorizzato gli impegni, le liquidazioni e i pagamenti delle spese, per l'anno 2013, dal 1° gennaio al 31 dicembre, secondo lo stato di previsione della spesa annesso alla legge stessa, nel quale sono nuovamente presenti l'UPB S04.06.005 (Interventi di recupero ambientale e di valorizzazione delle aree minerarie - Investimenti) e l'UPB S06.06.004 (Fondo Regionale per l'Occupazione - Spese correnti);
 che l'art. 1 della legge della Regione Sardegna 2 agosto 2013, n. 22 (Norme urgenti per l'attuazione dell'articolo 4 della legge regionale 29 aprile 2013, n. 10 - Disposizioni urgenti in materia di lavoro e nel settore sociale), ha dettato ulteriori norme per l'attuazione dell'art. 4 della legge reg. Sardegna n. 10 del 2013, provvedendo alla copertura integrale dei relativi oneri per gli anni 2013, 2014 e 2015 mediante la riduzione dell'UPB S06.06.004 nel bilancio regionale per gli anni suddetti;
 che, tra le vicende normative ripercorse, ha rilievo preminente ai fini della decisione la legge reg. Sardegna n. 13 del 2013, il cui art. 6 autorizza, a partire da un momento anteriore al 7 febbraio 2013 - data di pubblicazione della legge reg. Sardegna n. 2 del 2013, nonché di entrata in vigore della legge stessa, ai sensi del suo art. 10 - gli impegni, le liquidazioni e i pagamenti delle spese di cui all'apposito stato di previsione, nel quale sono incluse le UPB alle quali fa riferimento la normativa impugnata;
 che perciò, a prescindere da ogni altra considerazione circa l'ammissibilità e il merito delle questioni, va dichiarata la cessazione della materia del contendere, la quale si verifica quando lo ius superveniens interviene sulle disposizioni impugnate in senso satisfattivo delle pretese fatte valere dal ricorrente (ex plurimis, sentenze n. 19 del 2013 e n. 297 del 2012);
 che, più specificamente, con riguardo alle leggi di spesa di cui sia in discussione la sufficiente copertura finanziaria, deve essere dichiarata la cessazione della materia del contendere quando sopraggiungano misure legislative idonee ad adeguare i pertinenti stanziamenti (sentenza n. 241 del 2013), in modo tale da sanare, con riferimento all'intero arco temporale di vigenza delle stesse, la mancanza di copertura dei relativi oneri.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara cessata la materia del contendere in ordine alle questioni di legittimità costituzionale degli artt. 8, comma 1, e 9, comma 2, della legge della Regione autonoma Sardegna 1° febbraio 2013, n. 2 (Autorizzazione all'intervento finanziario della SFIRS Spa per l'infrastrutturazione, il risparmio e l'efficientamento energetico dell'area industriale di Portovesme - Sulcis, incremento della dotazione finanziaria relativa agli interventi per il Parco geominerario e norme urgenti in materia di sostegno al reddito dei lavoratori in regime di ammortizzatori sociali), promosse dal Presidente del Consiglio dei ministri con il ricorso indicato in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 21 maggio 2014.&#13;
 F.to:&#13;
 Gaetano SILVESTRI, Presidente&#13;
 Marta CARTABIA, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 6 giugno 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
