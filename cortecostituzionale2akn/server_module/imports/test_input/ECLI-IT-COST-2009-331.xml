<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/331/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/331/"/>
          <FRBRalias value="ECLI:IT:COST:2009:331" name="ECLI"/>
          <FRBRdate date="02/12/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="331"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/331/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/331/ita@/!main"/>
          <FRBRdate date="02/12/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/331/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/331/ita@.xml"/>
          <FRBRdate date="02/12/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="11/12/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>DE SIERVO</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>02/12/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale della disposizione combinata degli artt. 429, comma 2, in relazione al comma 1, lettera f), dello stesso articolo, e 185, comma 3, del codice di procedura penale, promosso dal Giudice dell’udienza preliminare del Tribunale di Napoli nel procedimento penale a carico di C.N. ed altra con ordinanza del 6 novembre 2008, iscritta al n. 47 del registro ordinanze 2009 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 8, prima serie speciale, dell’anno 2009.
 	Visto l’atto di intervento del Presidente del Consiglio dei ministri;
 	udito nella camera di consiglio del 4 novembre 2009 il Giudice relatore Giuseppe Frigo.
 Ritenuto che, con ordinanza dell’8 novembre 2008, il Giudice dell’udienza preliminare del Tribunale di Napoli ha sollevato, in riferimento all’art. 111, secondo comma, della Costituzione, questione di legittimità costituzionale della disposizione combinata degli artt. 429, comma 2, in relazione al comma 1, lettera f), dello stesso articolo, e 185, comma 3, del codice di procedura penale, nella parte in cui – secondo il giudice rimettente – prescriverebbe «la rinnovazione dell’intera udienza preliminare, anziché del solo decreto che dispone il giudizio, nel caso di nullità del decreto predetto per mancanza o insufficiente indicazione del luogo della comparizione»;
 che il rimettente riferisce che, a seguito di svolgimento dell’udienza preliminare, era stato emesso, nei confronti di C.N., decreto che dispone il giudizio per il reato di cui agli artt. 110 e 368 del codice penale, commesso in Afragola il 4 e 5 ottobre 2004, con fissazione dell’udienza dibattimentale davanti al giudice monocratico del Tribunale di Napoli, sede distaccata di Afragola;
 che il giudice del dibattimento, riscontrando l’ipotesi prevista dall’art. 429, comma 2, cod. proc. pen., in relazione al comma 1, lettera f), dello stesso articolo, aveva dichiarato la nullità del decreto che dispone il giudizio per insufficiente indicazione del luogo della comparizione, essendo riportato come indirizzo della sede distaccata di Afragola quello proprio della sede centrale di Napoli;
 che la dichiarazione di nullità ha prodotto la regressione del procedimento, il quale, pertanto, si trova nella fase degli atti introduttivi dell’udienza preliminare;
 che, in punto di diritto, il giudice rimettente osserva che, secondo il consolidato e condivisibile indirizzo della giurisprudenza di legittimità, solo nel caso di nullità della notificazione del decreto che dispone il giudizio, il giudice del dibattimento può procedere, ai sensi dell’art. 143 disp. att. cod. proc. pen., alla rinnovazione dell’atto nullo; mentre, quando la nullità colpisce lo stesso decreto che dispone il giudizio, gli atti devono essere trasmessi, in forza della disposizione combinata degli artt. 429, comma 2, e 185, comma 3, cod. proc. pen., al giudice dell’udienza preliminare, perché provveda, previa fissazione dell’udienza, alla rinnovazione del decreto;
 che, ciò premesso, il giudice a quo assume che tale disciplina violi il principio di ragionevole durata del processo, sancito dall’art. 111, secondo comma, Cost., nella parte in cui risulta riferibile anche alla nullità derivante dalla mancata o insufficiente indicazione del luogo di comparizione;
 che la normativa censurata imporrebbe, infatti, di ripetere l’intera udienza preliminare anche quando – come nell’ipotesi considerata – «si siano verificate nullità del decreto che dispone il giudizio del tutto formali e che colpiscono solo l’atto finale della predetta udienza preliminare, in punti esclusivamente “esterni” al decreto stesso (indirizzo del luogo di comparizione) senza alcun collegamento con valutazioni del merito dei fatti», non diversamente che nel caso di nullità concernenti la notifica del provvedimento;
 che, in simile ipotesi, la ripetizione, senza alcun ragionevole motivo, dell’intera udienza preliminare – la quale può talora svilupparsi in numerose e complesse udienze – si porrebbe, dunque, in insanabile contrasto con il principio costituzionale evocato;
 che, da ultimo, la questione sarebbe rilevante nel giudizio a quo, giacché il suo accoglimento consentirebbe di rinnovare solo il decreto che dispone il giudizio, anziché l’intera udienza preliminare;
 che nel giudizio di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall’Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o infondata;
 che, ad avviso della difesa erariale, la questione sarebbe basata su una inesatta valutazione delle conseguenze della nullità del decreto che dispone il giudizio per erronea indicazione del luogo di comparizione; 
 che, in tale ipotesi, non si avrebbe affatto una regressione del giudizio, in quanto, attenendo la nullità alla vocatio in ius dell’imputato, la fattispecie dovrebbe ritenersi disciplinata dall’art. 143 disp. att. cod. proc. pen.: con la conseguenza che spetterebbe al giudice del dibattimento, rilevata la nullità, disporre la rinnovazione della citazione – ossia la notifica dell’originario decreto, integrato con un provvedimento di corretta indicazione del luogo di svolgimento del giudizio e della nuova data dell’udienza – in analogia a quanto previsto per la nullità della notificazione del medesimo decreto.
 Considerato che il giudice rimettente dubita della legittimità costituzionale della disposizione combinata degli artt. 429, comma 2, in relazione al comma 1, lettera f), dello stesso articolo, e 185, comma 3, del codice di procedura penale, avuto riguardo alle conseguenze della declaratoria di nullità del decreto che dispone il giudizio a causa della mancata o insufficiente indicazione del luogo della comparizione;
 che la questione coinvolge, quindi, la problematica degli effetti della dichiarazione di nullità e, in particolare, il quesito su quale giudice debba rinnovare l’atto nullo e in che misura la nullità di un atto (nella specie, il decreto che dispone il giudizio) si estenda ad altri atti del processo, nel quale ogni atto è collegato ad altri in una prospettiva teleologica (cosiddetta nullità derivata);
 che, per quanto riguarda il primo aspetto, il giudice a quo ritiene che, in caso di nullità del decreto che dispone il giudizio, gli atti debbano essere trasmessi al giudice dell’udienza preliminare affinché provveda alla rinnovazione dell’atto; 
 che tale assunto appare in sé corretto, in quanto la declaratoria di nullità del decreto che dispone il giudizio comporta necessariamente, ai sensi dell’art. 185, comma 3, cod. proc. pen., il regresso del procedimento alla fase precedente, non rientrando – diversamente da quanto ritenuto dall’Avvocatura generale dello Stato – tra i poteri del giudice del dibattimento procedere alla rinnovazione del decreto;
 che, peraltro, il giudice rimettente reputa che tale regressione implichi, ai fini della rinnovazione dell’atto nullo, la ripetizione dell’intera udienza preliminare, anche nel caso in cui il vizio consista nella mancata o insufficiente o erronea indicazione del luogo di comparizione e, dunque, un aspetto formale ed estrinseco, ancorché indispensabile, rispetto al contenuto dispositivo del provvedimento, così determinando un ingiustificato aggravio dei tempi del processo e la violazione del principio della sua ragionevole durata;
 che l’asserita violazione di tale principio appare, tuttavia, la conseguenza di un fallace presupposto interpretativo, in quanto, da un lato, la censurata disposizione combinata non implica affatto che a tutte le ipotesi di declaratoria di nullità del decreto che dispone il giudizio debba necessariamente fare seguito una integrale ripetizione dell’udienza preliminare e, dall’altro, il giudice rimettente non valuta e non indica in concreto a quali eventuali ulteriori atti dell’udienza preliminare debba semmai comunicarsi la nullità;
 che, in particolare, non si è considerato come – a differenza di quanto era stabilito nel codice di procedura penale previgente, ove l’art. 189 eccezionalmente prevedeva l’estensione della nullità agli atti anteriori o contemporanei connessi con quello viziato – ora l’art. 185 cod. proc. pen. circoscriva tale effetto ai soli atti successivi che siano legati all’atto viziato da un rapporto di dipendenza causale e ne costituiscano, quindi, la conseguenza logica e giuridica;
 che, estendendosi la nullità derivata ai soli atti successivi e non anche a quelli antecedenti, la declaratoria di nullità del decreto che dispone il giudizio per mancanza o insufficiente indicazione del luogo della comparizione non può invalidare anche atti del procedimento precedenti a quello oggetto di tale declaratoria, sicché la regressione del processo all’udienza preliminare si realizza fino al momento e all’atto in cui il giudice di tale udienza, avendo già dichiarata chiusa la discussione delle parti, procede alla deliberazione; anzi, e più precisamente, alla sola parte di questa che, di seguito alla disposizione del giudizio e all’indicazione del giudice a questo competente, attiene alla precisa individuazione e indicazione «del luogo, del giorno e dell’ora della comparizione»;
 che, di conseguenza, una corretta lettura della disciplina oggetto di rimessione avrebbe dovuto condurre il giudice a quo a una valutazione affatto diversa, onde la questione sollevata va dichiarata manifestamente infondata.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta infondatezza della questione di legittimità costituzionale della disposizione combinata degli artt. 429, comma 2, in relazione al comma 1, lettera f), dello stesso articolo, e 185, comma 3, del codice di procedura penale, sollevata, in riferimento all’art. 111, secondo comma, della Costituzione, dal Giudice dell’udienza preliminare del Tribunale di Napoli con l’ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 2 dicembre 2009.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria l'11 dicembre 2009.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
