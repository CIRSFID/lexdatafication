<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2003/371/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2003/371/"/>
          <FRBRalias value="ECLI:IT:COST:2003:371" name="ECLI"/>
          <FRBRdate date="17/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="371"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2003/371/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2003/371/ita@/!main"/>
          <FRBRdate date="17/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2003/371/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2003/371/ita@.xml"/>
          <FRBRdate date="17/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/12/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Fernanda Contri</cc:relatore_pronuncia>
        <cc:data_decisione>17/12/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 72 del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'articolo 15 della legge 8 marzo 2000, n. 53), promosso con ordinanza del 3 febbraio 2003 dal Tribunale di Genova sul ricorso proposto da Bartesaghi Maria Cleme contro Cassa nazionale previdenza e assistenza forense, iscritta al n. 209 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2003. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 12 novembre 2003 il Giudice relatore Fernanda Contri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. - Il Tribunale di Genova, con ordinanza in data 3 febbraio 2003, ha sollevato, in riferimento agli artt. 3, 31 e 37 della Costituzione, questione di legittimità costituzionale dell'art. 72 del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'articolo 15 della legge 8 marzo 2000, n. 53), nella parte in cui tale norma non prevede il diritto della libera professionista che abbia adottato un bambino a percepire l'indennità di maternità, anche se il minore abbia superato i sei anni e fino al compimento di dodici anni, se di nazionalità italiana, o della maggiore età, se straniero. 
    Il rimettente premette che il giudizio a quo ha ad oggetto la domanda di corresponsione del trattamento di maternità per l'adozione di un bambino straniero, proposta da una libera professionista nei confronti della Cassa nazionale di previdenza e assistenza forense, la quale aveva negato il pagamento sul rilievo del superamento dei sei anni di età da parte del minore al momento del suo ingresso nella famiglia adottiva. 
    Il giudice a quo, dopo aver rilevato che la chiara e univoca formulazione dell'art. 72 del d.lgs. n. 151 del 2001 - che riconosce il diritto all'indennità di maternità a favore della libera professionista, a condizione che il bambino adottato non abbia superato i sei anni di età - non consente interpretazioni diverse da quella letterale, osserva che detta interpretazione appare tuttavia in contrasto con gli artt. 3, 31 e 37 della Costituzione, i cui principi sono stati posti a base delle pronunce della Corte costituzionale, dirette ad ampliare la tutela medesima, sotto il profilo sia dei soggetti beneficiari delle provvidenze, sia dell'entità anche economica di esse.  
    Gli istituti innovativi introdotti dal legislatore a sostegno della famiglia, ispirati anch'essi ai citati principi costituzionali, hanno riguardato prevalentemente i lavoratori dipendenti, mentre il trattamento di maternità delle libere professioniste è rimasto disciplinato dalla legge 11 dicembre 1990, n. 379 (Indennità di maternità per le libere professioniste), le cui norme sono state integralmente trasfuse nel testo unico. Queste, nel prevedere la condizione del mancato superamento dei sei anni dell'adottato al momento di ingresso nella famiglia, per il sorgere del diritto all'indennità, non tengono conto della circostanza che tale età coincide con quella dell'inserimento obbligatorio del minore nella scuola elementare, da cui deriva un notevole impegno relazionale e intellettuale, soprattutto in rapporto ad un bambino adottato, specie se straniero, che deve affrontare nuove realtà affettive, culturali, linguistiche e ambientali. 
    Tali circostanze sono state considerate solo in relazione alle adozioni da parte delle lavoratrici dipendenti, poiché l'art. 36 del testo unico prevede il congedo parentale fino al compimento dei dodici anni di età del bambino e l'art. 27 stabilisce, nel caso di adozione internazionale, il diritto al congedo di maternità anche se il minore adottato abbia superato i sei anni e sino al compimento della maggiore età. 
    Il mancato innalzamento degli indicati limiti di età in relazione alle adozioni da parte delle libere professioniste costituisce, ad avviso del rimettente, una palese violazione del principio di eguaglianza, che dà luogo ad una irragionevole disparità di trattamento tra le due categorie di lavoratrici. 
    2. - Con atto depositato il 27 maggio 2003 è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata infondata.Considerato in diritto1. - La questione sottoposta dal Tribunale di Genova all'esame di questa Corte concerne la legittimità costituzionale, in relazione agli artt. 3, 31 e 37 della Costituzione, dell'art. 72 del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'articolo 15 della legge 8 marzo 2000, n. 53), nella parte in cui non prevede il diritto della libera professionista che abbia adottato un bambino a percepire l'indennità di maternità, anche se il minore abbia superato i sei anni e fino al compimento di dodici anni, se di nazionalità italiana, o della maggiore età, se straniero. 
    2. - Preliminarmente deve essere dichiarata la inammissibilità dell'intervento del Presidente del Consiglio dei ministri, in quanto effettuato oltre il termine di venti giorni dalla pubblicazione dell'ordinanza di rimessione nella Gazzetta Ufficiale. 
    3. - Il giudice a quo sollecita una pronuncia con la quale si estenda il diritto delle libere professioniste all'indennità di maternità in caso di adozione attraverso l'ampliamento della condizione cui è subordinato il diritto stesso, consistente nell'età dell'adottato, il cui limite dovrebbe essere spostato a dodici anni nel caso di adozione nazionale e al compimento della maggiore età relativamente a quella internazionale. 
    La prima delle due questioni poste dal rimettente è inammissibile per difetto di rilevanza.  
    Il giudizio a quo, come risulta espressamente dalla stessa ordinanza di rimessione, ha ad oggetto la domanda di corresponsione dell'indennità di maternità a seguito di adozione internazionale e pertanto l'ipotesi dell'adozione nazionale rimane del tutto estranea alla fattispecie dedotta in quel giudizio. 
    3.1. - La questione di legittimità costituzionale sollevata in relazione all'adozione internazionale è fondata. 
    3.2. -  Il trattamento di maternità a favore delle lavoratrici adottanti o affidatarie è stato introdotto dall'art. 6 della legge 9 dicembre 1977, n. 903 (Parità di trattamento tra uomini e donne in materia di lavoro), che, equiparando all'evento della nascita l'ingresso del minore adottato o affidato nella famiglia, ha previsto la facoltà per le predette lavoratrici di avvalersi dell'astensione obbligatoria dal lavoro, contemplata dall'art. 4, lettera c), della legge 30 dicembre 1971, n. 1204, e del relativo trattamento economico durante i primi tre mesi successivi all'effettivo ingresso del minore nella famiglia; tale norma subordinava il beneficio stesso al mancato superamento dei sei anni di età del minore al momento dell'adozione o dell'affidamento. 
    La previsione contenuta nel citato art. 6 della legge n. 903 del 1977 si riferiva ovviamente alle sole lavoratrici dipendenti, poiché al momento della sua emanazione le libere professioniste, così come le lavoratrici autonome, ancora non godevano dell'indennità di maternità.  
    Soltanto con le leggi 29 dicembre 1987, n. 546 (Indennità di maternità per le lavoratrici autonome), e 11 dicembre 1990, n. 379 (Indennità di maternità per le libere professioniste), sono state dettate norme a tutela della maternità delle sopra indicate categorie di lavoratrici e si è riconosciuto loro il diritto all'indennità di maternità anche per l'ingresso del minore adottato o affidato in preadozione, a condizione che non avesse superato i sei anni di età. 
    La riforma dell'adozione internazionale, attuata con la legge 31 dicembre 1998, n. 476 (Ratifica ed esecuzione della Convenzione per la tutela dei minori e la cooperazione in materia di adozione internazionale, fatta a L'Aja il 29 maggio 1993. Modifiche alla legge 4 maggio 1983, n. 184, in tema di adozione di minori stranieri), ha esteso le condizioni di fruibilità del beneficio, prevedendo il diritto all'astensione dal lavoro e al connesso trattamento economico di maternità a favore dei genitori adottivi e degli affidatari “anche se il minore adottato ha superato i sei anni di età” [art. 39-quater, lettera a), della legge 4 maggio 1983, n. 184, introdotto dall'art. 3 della legge n. 476 del 1998]. 
    Il trattamento di maternità in caso di adozione è ora contenuto nel testo unico emanato con decreto legislativo n. 151 del 2001, nel quale sono state riunite tutte le disposizioni in materia di tutela e sostegno della maternità e della paternità. 
    3.3. - Nonostante i ripetuti interventi legislativi, la disciplina del trattamento di maternità in caso di adozione internazionale risulta peraltro ancora disomogenea.  
    Il suddetto trattamento è riconosciuto senza alcun limite di età ma anzi con la espressa previsione che spetta anche se il minore ha superato i sei anni e fino al compimento della maggiore età, a favore delle lavoratrici dipendenti (art. 27, comma 1), delle lavoratrici autonome, delle coltivatrici dirette, delle mezzadre e colone, delle artigiane ed esercenti attività commerciali ed infine delle imprenditrici agricole a titolo principale (art. 67, comma 2). Alle libere professioniste, invece, la disposizione contenuta nell'art. 72 attribuisce l'indennità a condizione che il minore non abbia superato i sei anni di età, senza operare alcuna distinzione tra adozione nazionale e internazionale; onde il limite riguarda entrambe le ipotesi. 
    L'irragionevolezza della previsione normativa in esame è manifesta, poiché non è dato individuare elementi che giustifichino la differenza del trattamento di maternità delle libere professioniste rispetto a quello stabilito nella medesima ipotesi dell'adozione internazionale non solo per le lavoratrici dipendenti ma anche per le lavoratrici autonome, categoria senz'altro più affine a quella de qua.  
    Le ragioni che hanno indotto il legislatore del 1998 a superare il limite dei sei anni di età per il trattamento di maternità nell'adozione internazionale, come risulta dalla relazione al disegno di legge di ratifica ed esecuzione della Convenzione dell'Aja, sono essenzialmente individuabili nella valutazione relativa alle difficoltà derivanti dall'inserimento dei minori stranieri nella comunità familiare ed in quella scolastica, che aumentano in modo esponenziale con il crescere dell'età, richiedendo soprattutto nei primi tempi “un'assistenza particolare da parte dei nuovi genitori”. Se si considera che le medesime ragioni che hanno indotto all'ampliamento della tutela ricorrono in tutte le adozioni internazionali, indipendentemente dall'attività lavorativa dei genitori adottanti, risulta evidente come la limitazione, negli indicati termini, del diritto delle libere professioniste non solo sia priva di autonoma ratio, essendo piuttosto addebitabile ad un difetto di coordinamento delle norme trasfuse nel nuovo testo unico, ma urti in modo stridente con i principi costituzionali che impongono la tutela del minore. 
    La disposizione impugnata è quindi costituzionalmente illegittima, poiché nel caso di adozione internazionale non consente alle libere professioniste di percepire l'indennità di maternità nei tre mesi successivi all'ingresso del minore adottato nella famiglia, quando questi abbia superato i sei anni di età.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
     1) dichiara l'illegittimità costituzionale dell'art. 72 del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'articolo 15 della legge 8 marzo 2000, n. 53), nella parte in cui non prevede che nel caso di adozione internazionale l'indennità di maternità spetta nei tre mesi successivi all'ingresso del minore adottato o affidato, anche se abbia superato i sei anni di età; &#13;
    2) dichiara l'inammissibilità della questione di legittimità costituzionale dell'art. 72 del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'articolo 15 della legge 8 marzo 2000, n. 53), sollevata, in riferimento agli artt. 3, 31 e 37 della Costituzione, dal Tribunale di Genova con l'ordinanza in epigrafe, in relazione all'ipotesi dell'adozione nazionale. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 17 dicembre 2003. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Fernanda CONTRI, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 23 dicembre 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
