<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/150/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/150/"/>
          <FRBRalias value="ECLI:IT:COST:2014:150" name="ECLI"/>
          <FRBRdate date="19/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="150"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/150/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/150/ita@/!main"/>
          <FRBRdate date="19/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/150/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/150/ita@.xml"/>
          <FRBRdate date="19/05/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="28/05/2014" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2014</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>SILVESTRI</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>19/05/2014</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Gaetano SILVESTRI; Giudici : Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione della Camera dei deputati del 16 ottobre 2013 (doc. IV-quater n. 2), relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, Cost., delle opinioni espresse dall'on. Aniello Formisano nei confronti di Ciro Borriello, sindaco del Comune di Torre del Greco, promosso dal Tribunale ordinario di Torre Annunziata, con ricorso del 3 dicembre 2013, depositato in cancelleria il 3 febbraio 2014 ed iscritto al n. 1 del registro conflitti tra poteri dello Stato 2014, fase di ammissibilità. 
 Udito nella camera di consiglio del 7 maggio 2014 il Giudice relatore Giuseppe Frigo.
 Ritenuto che, con ricorso del 3 dicembre 2013, depositato il 3 febbraio 2014, il Tribunale ordinario di Torre Annunziata, in composizione monocratica, ha sollevato conflitto di attribuzione fra poteri dello Stato, chiedendo a questa Corte di dichiarare che non spettava alla Camera dei deputati di affermare, con deliberazione del 16 ottobre 2013 (doc. IV-quater n. 2), che le dichiarazioni rese dall'on. Aniello Formisano, deputato all'epoca dei fatti, nei confronti di Ciro Borriello - per le quali pende procedimento penale - concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, come tali insindacabili ai sensi dell'art. 68, primo comma, della Costituzione, e di annullare conseguentemente la predetta deliberazione della Camera dei deputati;
 che il ricorrente premette di essere investito del procedimento penale nei confronti di Aniello Formisano, imputato del reato di cui all'art. 595, terzo comma, del codice penale, «perché, quale ospite della trasmissione televisiva "Uno Mattina", andata in onda su Rai 1 in data 31.07.2012, offendeva l'onore e il decoro di Borriello Ciro, allorquando riferendosi al suo precedente mandato di Sindaco del Comune di Torre del Greco, lo definiva "delinquente di centro destra, perché tale era", andando ben oltre i limiti della critica politica esercitabile nell'ambito della dialettica tra partiti contrapposti»;
 che, ad avviso del ricorrente, le dichiarazioni oggetto del procedimento penale non sarebbero coperte dalla guarentigia di cui all'art. 68, primo comma, Cost., - come, invece, ritenuto dalla Camera dei deputati - non potendosi individuare, alla luce della giurisprudenza costituzionale e della Corte europea dei diritti dell'Uomo citata nel ricorso, uno specifico «nesso funzionale» tra le dichiarazioni rese extra moenia e l'attività parlamentare, ravvisabile solo se sussista una corrispondenza «sostanziale» e «cronologica» tra l'atto parlamentare e detta manifestazione di pensiero;
 che, in effetti, le dichiarazioni extra moenia oggetto del presente conflitto non potrebbero ritenersi funzionalmente collegate all'interrogazione parlamentare a risposta immediata del 5 novembre 2008, vertente sul contrasto alla diffusione e al radicamento della camorra, perché difetterebbero entrambi i presupposti richiesti dalla giurisprudenza costituzionale per l'applicazione dell'art. 68, primo comma, Cost; 
 che, infatti, quanto al requisito temporale, l'atto parlamentare individuato risale al 2008 e dunque a quattro anni prima del fatto; quanto al secondo requisito, non vi sarebbe corrispondenza tra le dichiarazioni rese extra moenia e l'attività parlamentare, dal momento che la menzionata interrogazione non riguardava specificamente l'ex sindaco Borriello e faceva solo genericamente riferimento a «uomini ed istituzioni degli enti locali»;
 che, infine, sussisterebbero sia i presupposti soggettivi del conflitto - essendo il Tribunale competente a decidere sul reato in questione - che oggettivi, lamentando il ricorrente la lesione della propria sfera di attribuzione, costituzionalmente garantita, in conseguenza della deliberazione della Camera dei deputati. 
 Considerato che in questa fase del giudizio, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), la Corte è chiamata a deliberare, senza contraddittorio, se il ricorso sia ammissibile in quanto vi sia «materia di un conflitto la cui risoluzione spetti alla sua competenza», sussistendone i requisiti soggettivo ed oggettivo e restando impregiudicata ogni ulteriore questione, anche in punto di ammissibilità; 
 che, sotto il profilo del requisito soggettivo, va riconosciuta la legittimazione del Tribunale ordinario di Torre Annunziata a promuovere conflitto di attribuzione tra poteri dello Stato, in quanto organo giurisdizionale, in posizione di indipendenza costituzionalmente garantita, competente a dichiarare definitivamente, nell'esercizio delle funzioni attribuitegli, la volontà del potere cui appartiene; 
 che, parimenti, deve essere riconosciuta la legittimazione della Camera dei deputati ad essere parte del presente conflitto, quale organo competente a dichiarare in modo definitivo la propria volontà in ordine all'applicazione dell'art. 68, primo comma, Cost.; 
 che, per quanto attiene al profilo oggettivo, il ricorrente lamenta la lesione della propria sfera di attribuzione, costituzionalmente garantita, in conseguenza di un esercizio ritenuto illegittimo, per inesistenza dei relativi presupposti, del potere spettante alla Camera dei deputati di dichiarare l'insindacabilità delle opinioni espresse da un membro di quel ramo del Parlamento ai sensi dell'art. 68, primo comma, Cost.; 
 che, dunque, esiste la materia di un conflitto la cui risoluzione spetta alla competenza di questa Corte.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 1) dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), il ricorso per conflitto di attribuzione tra poteri dello Stato indicato in epigrafe, proposto dal Tribunale ordinario di Torre Annunziata nei confronti della Camera dei deputati;  &#13;
 2) dispone: &#13;
 a) che la cancelleria di questa Corte dia immediata comunicazione della presente ordinanza al predetto giudice, che ha proposto il conflitto di attribuzione; &#13;
 b) che il ricorso e la presente ordinanza siano, a cura del ricorrente, notificati alla Camera dei deputati, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di trenta giorni previsto dall'art. 24, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 maggio 2014.&#13;
 F.to:&#13;
 Gaetano SILVESTRI, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 28 maggio 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
