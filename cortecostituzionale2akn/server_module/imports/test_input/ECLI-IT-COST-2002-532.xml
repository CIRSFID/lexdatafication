<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/532/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/532/"/>
          <FRBRalias value="ECLI:IT:COST:2002:532" name="ECLI"/>
          <FRBRdate date="06/12/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="532"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/532/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/532/ita@/!main"/>
          <FRBRdate date="06/12/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/532/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/532/ita@.xml"/>
          <FRBRdate date="06/12/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="18/12/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>06/12/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 284, comma 3, del codice di procedura penale, promosso con ordinanza dell'8 gennaio 2002 dal Tribunale di Napoli sezione per il riesame, nel procedimento penale a carico di C.L. iscritta al n. 259 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 22, prima serie speciale, dell'anno 2002. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 20 novembre 2002 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che il Tribunale di Napoli solleva, in riferimento agli artt. 1, 2, 3, 4, 13, secondo comma, 27, terzo comma, e 35 della Costituzione, questione di legittimità costituzionale dell'art. 284, comma 3, del codice di procedura penale, «nella parte in cui prevede che il giudice possa effettuare il giudizio sulla opportunità di concedere al detenuto agli arresti domiciliari l'autorizzazione ad assentarsi dal luogo di detenzione per lo svolgimento di una attività lavorativa soltanto qualora il detenuto “non possa altrimenti provvedere alle sue indispensabili esigenze di vita”, ovvero “versi in situazione di assoluta indigenza”»; 
    che il giudice rimettente - dopo aver sottolineato come l'art. 284, comma 3, cod. proc. pen. rappresenti esplicazione del generale principio sancito dall'art. 277 del medesimo codice, in tema di salvaguardia dei diritti della persona sottoposta a misure cautelari, così da doversi «perseguire un equilibrato bilanciamento dei contrapposti interessi coinvolti dal provvedimento coercitivo», e consentire al giudice la scelta di modalità attuative del regime cautelare atte a sacrificare il minimo indispensabile i diritti fondamentali della persona - richiama, quale termine di raffronto, il diverso regime che presiede alla autorizzazione al lavoro per i detenuti in carcere; 
    che, infatti, in base all'art. 20 dell'ordinamento penitenziario, il lavoro «nel caso di detenzione carceraria è “obbligatorio” e con finalità non produttive né afflittive, bensì di natura rieducativa»; che il detenuto, espiata parte della pena, può chiedere alla autorità penitenziaria di essere ammesso al lavoro all'esterno, come dipendente o lavoratore autonomo; che, infine, la direzione penitenziaria ammette il detenuto al lavoro all'esterno emanando un provvedimento amministrativo che deve essere approvato con decreto dal magistrato di sorveglianza; 
    che, inoltre, «gli articoli 47-ter, 47-quater e 47- quinquies» della legge 26 luglio 1975, n. 354, nel disciplinare l'istituto della detenzione domiciliare per il condannato, a differenza di quanto stabilito dalla norma impugnata, non prevedono che per poter svolgere una attività lavorativa esterna, il condannato debba trovarsi in stato di “assoluta indigenza”, ovvero nella “impossibilità di provvedere altrimenti alle proprie indispensabili esigenze di vita”; 
    che, pertanto, svelerebbe «tutta la sua irragionevolezza un impianto normativo il quale - mentre per il rilascio della autorizzazione del lavoro all'esterno dell'istituto penitenziario da parte del detenuto ristretto in carcere, ovvero del condannato in stato di detenzione domiciliare», non richiede le condizioni di cui innanzi si è detto - impone, invece, la sussistenza di tali requisiti ove la medesima autorizzazione riguardi la persona sottoposta alla misura cautelare degli arresti domiciliari, malgrado si tratti di soggetto «istituzionalmente ritenuto socialmente meno pericoloso»; 
    che, di conseguenza, tale irragionevole disciplina contrasterebbe, a parere del giudice  a quo, anche con il principio sancito dall'art. 27, terzo comma, della Costituzione, posto che - sottolinea il rimettente - «anche la custodia cautelare deve uniformarsi» al suddetto principio, secondo quanto sarebbe stato testualmente affermato da questa Corte (sentenza n. 173 del 1997); 
    che sarebbe violato anche l'art. 13, secondo comma, della medesima Carta, in quanto al giudice sarebbe imposto di dichiarare inammissibile, in via preliminare, la richiesta di autorizzazione al lavoro in mancanza della condizione soggettiva patrimoniale postulata dall'art. 284, comma 3, cod. proc. pen., senza poter in alcun modo «valutare il merito della richiesta stessa che, viceversa, meriterebbe di essere esaminata esclusivamente sotto il profilo della sua compatibilità con le esigenze di tutela sociale, alla stregua della pericolosità del detenuto»; 
    che, infine - posto che l'indicata condizione «di carattere soggettivo patrimoniale» precluderebbe al giudice di operare il doveroso bilanciamento tra il sacrificio di diritti fondamentali, come quello al lavoro, e le esigenze cautelari - ne deriverebbe, ad avviso del rimettente, «un irragionevole sacrificio della dignità umana e professionale», nonché una «disparità di trattamento fondata sulle sole condizioni economiche del detenuto», con la conseguente violazione degli artt. 1, primo comma, 2, 3, 4 e 35, della Costituzione; 
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dalla Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata non fondata. 
    Considerato che il giudice  a quo censura l'art. 284, comma 3, cod. proc. pen., nella parte in cui prevede che, nei confronti della persona sottoposta alla misura degli arresti domiciliari, il giudice possa autorizzare l'imputato ad assentarsi nel corso della giornata dal luogo dell'arresto per esercitare una attività lavorativa, soltanto qualora l'imputato stesso “non possa altrimenti provvedere alle sue indispensabili esigenze di vita” ovvero “versi in situazione di assoluta indigenza”; 
    che - secondo le prospettive del giudice a quo - il nucleo delle censure si concentra sulla pretesa, irragionevole disparità di trattamento che sarebbe dato riscontrare tra la disciplina prevista, in tema di autorizzazione al lavoro, per l'imputato sottoposto alla misura degli arresti domiciliari - per il quale varrebbe una presunzione di pericolosità affievolita - e quella prevista per chi è detenuto in carcere: e ciò perché, in base all'art. 20 dell'ordinamento penitenziario, «il lavoro, nel caso di detenzione carceraria, è “obbligatorio” e con finalità non produttive né afflittive, bensì di natura rieducativa»; non è subordinato alle condizioni invece prescritte dalla disposizione censurata; può essere autorizzato anche all'esterno del carcere in favore del detenuto che abbia «espiato parte della pena», a seguito di provvedimento amministrativo della direzione penitenziaria, approvato con  decreto del magistrato di sorveglianza, avendo di mira, come parametri, esclusivamente «quelli rieducativi (art. 27, terzo comma, Cost.), nel rispetto delle esigenze di tutela sociale»; 
    che analogo raffronto varrebbe anche con riferimento alla disciplina dettata in tema di «detenzione domiciliare del condannato», considerato che gli artt. 47-ter, 47-quater e 47-quinquies dell'ordinamento penitenziario non richiederebbero, per l'autorizzazione allo svolgimento di una attività lavorativa esterna, le condizioni prescritte dall'art. 284 comma 3, cod. proc. pen.; 
    che, peraltro, la prospettata analogia di situazioni, da cui discende l'intera gamma delle doglianze, si rivela palesemente erronea: e ciò, non soltanto sul piano più generale della assimilabilità di status fra loro eterogenei, quali sono quelli che contraddistinguono, da un lato, la condizione dell'imputato sottoposto ad una misura cautelare personale, e, dall'altro, quella del condannato in fase di esecuzione della pena; ma anche sul versante, più specifico, dei provvedimenti destinati ad incidere sulle rispettive sfere di coercizione, in vista della possibilità di svolgere una attività lavorativa; 
    che, quanto al primo aspetto,  è infatti agevole rilevare come la funzione rieducativa - cui è necessariamente informata l'intera fase esecutiva e sulla cui falsariga sono quindi plasmati gli istituti previsti dall'ordinamento penitenziario - si rivela non soltanto eccentrica, ma addirittura contraddittoria rispetto alle connotazioni che tipizzano l'intera gamma delle misure cautelari;  queste ultime - presupponendo la temporaneità e le garanzie postulate dall'art. 13 Cost. - evidentemente sono volte a presidiare esclusivamente i pericula libertatis previsti dalla legge: con esclusione, quindi, di qualsiasi finalità di “rieducazione” che, per gli imputati, equivarrebbe ad una palese elusione del principio di presunzione di non colpevolezza; 
    che, in effetti, questa Corte ha affermato (v. sentenza n. 173 del 1997, citata) non già - come assume il rimettente - che “la finalità rieducativa è assegnata dalla Costituzione a ogni pena e, dunque, anche alle misure cautelari”; bensì che tale finalità è assegnata, accanto ad ogni pena, “anche alle misure alternative previste in seno all'ordinamento penitenziario”; 
    che, di conseguenza, ben si giustifica la previsione dell'art. 15 dell'ordinamento penitenziario, secondo cui il lavoro é una componente essenziale del trattamento rieducativo, al punto - come rammenta lo stesso giudice a quo - da essere configurato come “obbligatorio per i condannati”, in base all'art. 20 dello stesso ordinamento; non senza rammentare, peraltro, come per il detenuto in carcere l'intero complesso delle misure che lo riguardano vale a distinguere nettamente la condizione di chi vi è sottoposto, rispetto alla posizione che caratterizza l'imputato agli arresti domiciliari; 
    che le considerazioni che precedono - in ordine alla eterogeneità e non comparabilità, sotto questo profilo, delle condizioni concernenti rispettivamente l'esecuzione della pena e le misure cautelari personali - valgono altresì con riferimento all'istituto della detenzione domiciliare: quest'ultima infatti è una misura alternativa che presuppone l'esecuzione della pena e che assume, per di più, connotazioni del tutto peculiari nel panorama di tali misure, avuto riguardo ai profili polifunzionali che la caratterizzano e che consentono, in sé, di distinguerla dall'apparentemente “simile” istituto degli arresti domiciliari (v. sentenza n. 165 del 1996); non senza sottolineare, peraltro, come lo stesso art. 47-ter  dell'ordinamento penitenziario richiami espressamente (al comma 4) proprio l'art. 284 cod. proc. pen., per determinare le “modalità” secondo le quali la detenzione domiciliare deve essere eseguita; 
    che, infine, la differenza fra la condizione dell'imputato sottoposto a custodia cautelare in carcere e quella dell'imputato agli arresti domiciliari rende ragione del fatto - peraltro non evocato dal rimettente - che la disciplina dell'art. 21 dell'ordinamento penitenziario sia applicabile al primo e non invece al secondo; 
    che rientra nella sfera della discrezionalità legislativa bilanciare l'esercizio di diritti fondamentali, come il diritto al lavoro, con la specifica natura e funzione delle singole misure di cautela personale: con l'ovvio limite rappresentato dal rispetto del principio di ragionevolezza, che nella specie - venendo in discorso una misura coercitiva equiparata, in tutto e per tutto, alla custodia cautelare in carcere - non può ritenersi esser stato in alcun modo vulnerato; 
    che, di conseguenza, la questione proposta deve essere dichiarata manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 284, comma 3, del codice di procedura penale, sollevata, in riferimento agli artt. 1, 2, 3, 4, 13, secondo comma, 27, terzo comma, e 35 della Costituzione, dal Tribunale di Napoli con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 dicembre 2002. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Giovanni Maria FLICK, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 18 dicembre 2002. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
