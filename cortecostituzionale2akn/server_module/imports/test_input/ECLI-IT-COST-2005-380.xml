<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/380/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/380/"/>
          <FRBRalias value="ECLI:IT:COST:2005:380" name="ECLI"/>
          <FRBRdate date="28/09/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="380"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/380/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/380/ita@/!main"/>
          <FRBRdate date="28/09/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/380/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/380/ita@.xml"/>
          <FRBRdate date="28/09/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/10/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CAPOTOSTI</cc:presidente>
        <cc:relatore_pronuncia>Romano Vaccarella</cc:relatore_pronuncia>
        <cc:data_decisione>28/09/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Piero Alberto CAPOTOSTI; Giudici: Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 669-quaterdecies del codice di procedura civile promosso con ordinanza del 3 maggio 2004 dal Tribunale di Lecce, sezione distaccata di Nardò, nel procedimento civile vertente tra Manno Cosimo ed altra contro Presicce Maurizio ed altra, iscritta al n. 702 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 36, prima serie speciale, dell'anno 2004. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 25 maggio 2005 il Giudice relatore Romano Vaccarella. 
      Ritenuto che, con ordinanza del 3 maggio 2004, il Tribunale di Lecce, sezione distaccata di Nardò, solleva, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 669-quaterdecies del codice di procedura civile, nella parte in cui prevede l'applicazione integrale della disciplina regolatrice del procedimento cautelare uniforme anche ai procedimenti di denuncia di nuova opera (e di danno temuto);  
      che il giudice a quo espone di avere emesso, nel corso di un giudizio di denuncia di nuova opera, provvedimento cautelare di sospensione dell'esecuzione del manufatto in via di realizzazione, disponendo il rinvio del processo «alla prima udienza di trattazione del giudizio di merito»; 
      che in detta udienza i convenuti hanno chiesto la revoca o la modifica dell'ordinanza nella parte – asseritamente viziata da nullità – in cui essa disponeva tale rinvio, sostenendo che il procedimento si sarebbe dovuto esaurire con la pronuncia del provvedimento di accoglimento o di rigetto dell'istanza cautelare, «mentre l'interessato poteva, in qualsiasi momento, instaurare il giudizio a cognizione ordinaria, proponendo la domanda di merito» ai sensi dell'art. 669-octies, cod. proc. civ.; 
      che il rimettente, in punto di rilevanza, osserva che effettivamente, in virtù del disposto dell'art. 669-quaterdecies cod. proc. civ., ai procedimenti di denuncia di nuova opera si applicano le norme sul procedimento cautelare uniforme, e segnatamente l'art. 669-octies cod. proc. civ., di modo che, ove non venisse accolta la sollevata eccezione di incostituzionalità, egli non potrebbe esimersi dal revocare l'ordinanza di accoglimento del 29 aprile 2003, nella parte in cui ha disposto il rinvio alla prima udienza di trattazione del giudizio di merito, anziché limitarsi a fissare il termine perentorio per l'instaurazione di detto giudizio; 
      che, quanto alla non manifesta infondatezza della questione, osserva che la disposta equiparazione dei procedimenti di denuncia di nuova opera e di danno temuto agli altri procedimenti cautelari, quali i procedimenti di sequestro e di urgenza, ex art. 700 cod. proc. civ., appare assolutamente irragionevole e in contrasto con l'art. 3 della Costituzione, per trattamento uguale di situazioni diseguali; 
      che, infatti, se il disposto dell'art. 669-octies cod. proc. civ. ben si adatta alla natura e alla funzione di questi ultimi, «in cui la celebrazione di un giudizio di merito si giustifica solo in ragione dell'emissione di un provvedimento cautelare di accoglimento», essa mal si concilia con la «strutturazione della norma di cui all'art. 1171 cod. civ.» la quale sembra esigere, a seguito della decisione di accoglimento ovvero di rigetto del ricorso, la necessità di una successiva fase e decisione di merito; 
      che tale conclusione sarebbe avvalorata dalla disposizione (art. 1171, secondo comma del cod. civ.) che prevede la possibilità per il giudice di ordinare le opportune cautele per il risarcimento del danno (prodotto dalla sospensione dell'opera o, reciprocamente, dalla sua demolizione o riduzione) in caso, rispettivamente, di rigetto o di accoglimento della domanda all'esito del giudizio a cognizione piena;  
      che analoga conferma offrirebbe la circostanza che, nella vigenza degli abrogati artt. 689 e 690 cod. proc. civ., la giurisprudenza di legittimità era ferma nel ritenere che, nelle azioni di nunciazione, il passaggio dalla fase cautelare a quella di merito avveniva sempre e automaticamente, indipendentemente cioè dal fatto che fossero state concesse o negate le cautele richieste e senza necessità della proposizione di una nuova domanda, «in aggiunta a quella formulata con il ricorso introduttivo», mentre, nel vigore della novella del 1990, la Suprema Corte a Sezioni unite ha statuito che i procedimenti di reintegrazione e di manutenzione hanno conservato tale assetto bifasico; 
      che, tenuto conto della struttura e della storia degli istituti processuali de quibus, della parziale identità dell'oggetto della tutela, nonché dell'esigenza di evitare un inutile allungamento della durata del processo, sarebbe ragionevole l'accostamento della disciplina dei procedimenti di denuncia di nuova opera e di danno temuto a quella dei procedimenti possessori, con i quali essi presentano un alto tasso di omogeneità, piuttosto che a quella dei sequestri e dei provvedimenti di urgenza; 
      che non è manifestamente infondato, pertanto il dubbio di compatibilità, con l'art. 3 della Costituzione, dell'art. 669-quaterdecies cod. proc. civ., nella parte in cui dispone l'applicazione integrale, ai procedimenti di denuncia di nuova opera (e di danno temuto), della disciplina regolatrice del procedimento cautelare uniforme, anziché prevedere, analogamente a quanto stabilito per i procedimenti possessori, che il relativo corpus normativo si applichi solo in quanto compatibile; 
      che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto dichiararsi inammissibile o manifestamente infondata la questione proposta, osservando, preliminarmente, che l'indubbia affinità esistente tra il procedimento nunciatorio e quello possessorio non esclude che quest'ultimo presenti, rispetto al primo, delle specificità tali da giustificare una disciplina parzialmente diversa; 
      che, poiché la denuncia di nuova opera può fondarsi tanto su una pretesa di natura petitoria, quanto su una pretesa di tipo possessorio, il giudizio di merito che segue alla concessione della cautela nunciatoria assumerà, a seconda dei casi, carattere petitorio o possessorio, con la conseguenza che, nei relativi procedimenti – a differenza di quanto accade in quelli possessori –, l'oggetto del giudizio di merito è diverso da quello del procedimento sommario, posto che con quest'ultimo si ordina cautelarmente la sospensione dell'opera nuova, mentre col primo si accerta se l'opera sospesa possa o meno continuare oppure se quanto già costruito debba essere distrutto o conservato; 
      che, in tale contesto, la difforme regolamentazione dei due istituti, che si riduce all'applicazione al solo procedimento nunciatorio dell'art. 669-octies cod. proc. civ., con conseguente esclusione della necessaria prosecuzione della fase interdittale in quella di merito, non è, a giudizio dell'Avvocatura, affatto irragionevole né in altro modo lesiva dei diritti costituzionalmente garantiti delle parti, tanto più che, per consolidata giurisprudenza costituzionale, il legislatore è libero di modulare gli istituti  processuali con la più ampia discrezionalità, col solo limite del rispetto del principio di ragionevolezza. 
      Considerato che il Tribunale di Lecce, sezione distaccata di Nardò, dubita della legittimità costituzionale, in riferimento all'art. 3 della Costituzione, dell'art. 669-quaterdecies cod. proc. civ., nella parte in cui dispone l'applicazione integrale, ai procedimenti di denuncia di nuova opera (e di danno temuto), della disciplina regolatrice del procedimento cautelare uniforme, anziché prevedere, analogamente a quanto stabilito per i procedimenti possessori, che il relativo corpus normativo si applichi solo in quanto compatibile; 
      che la questione così sollevata è manifestamente infondata, dal momento che l'asserita assimilabilità dei procedimenti nunciatori a quello possessorio non comporta per il legislatore un vincolo, pena la manifesta irragionevolezza della disciplina, a regolarne in modo identico il rapporto con il giudizio di merito e, in particolare, a limitarne la libertà di variamente articolare, accentuandolo ovvero attenuandolo (come da ultimo il legislatore ha fatto con la legge 14 maggio 2005, n. 80; nuovi comma sesto dell'art. 669-octies e comma quarto dell'art. 703 del codice di procedura civile), il rapporto di strumentalità dei provvedimenti interinali rispetto al giudizio di merito.   
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'articolo 669-quaterdecies del codice di procedura civile sollevata, in riferimento all'articolo 3 della Costituzione, dal Tribunale di Lecce, sezione distaccata di Nardò, con l'ordinanza in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il  28 settembre 2005.  &#13;
F.to:  &#13;
Piero Alberto CAPOTOSTI, Presidente  &#13;
Romano VACCARELLA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 7 ottobre 2005.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
