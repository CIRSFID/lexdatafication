<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/205/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/205/"/>
          <FRBRalias value="ECLI:IT:COST:2003:205" name="ECLI"/>
          <FRBRdate date="03/06/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="205"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/205/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/205/ita@/!main"/>
          <FRBRdate date="03/06/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/205/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/205/ita@.xml"/>
          <FRBRdate date="03/06/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="11/06/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Fernanda Contri</cc:relatore_pronuncia>
        <cc:data_decisione>03/06/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 9 della legge 30 luglio 1990, n. 217 (Istituzione del patrocinio a spese dello Stato per i non abbienti), promosso con ordinanza del 27 marzo 2002 dal Tribunale di Pisa nel procedimento penale a carico di Mascali Rosario, iscritta al n. 445 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 40, prima serie speciale, dell'anno 2002. 
      Udito nella camera di consiglio del 9 aprile 2003 il Giudice relatore Fernanda Contri. 
    Ritenuto che il Tribunale di Pisa, con ordinanza emessa il 27 marzo 2002, ha sollevato questione di legittimità costituzionale dell'art. 9 della legge 30 luglio 1990,  n. 217 (Istituzione del patrocinio a spese dello Stato per i non abbienti), nella parte in cui prevede che la scelta del difensore di fiducia di chi è stato ammesso al patrocinio a spese dell'erario possa avvenire solo tra gli iscritti ad uno degli albi degli avvocati del distretto di corte d'appello nel quale ha sede il giudice davanti al quale pende il procedimento, anziché prevedere che, nel caso di nomina di un difensore iscritto ad albi di altri distretti, venga escluso il rimborso delle spese di trasferta e comunque effettuate al di fuori del distretto da parte del difensore; quali parametri il giudice rimettente indica gli artt. 3 e 24, secondo e terzo comma, della Costituzione;  
    che il giudice a quo è investito dell'esame della richiesta di liquidazione delle competenze di un difensore, iscritto all'albo del foro di Catania, che ha prestato la sua opera a favore di un imputato di rapina aggravata davanti al medesimo Tribunale rimettente;  
    che il rimettente rileva che la nomina dovrebbe essere revocata, sussistendo il criterio limitativo di cui alla disposizione impugnata;  
    che, secondo il giudice a quo, la disposizione impugnata creerebbe una evidente disparità di trattamento fra soggetti abbienti e soggetti non abbienti, potendo i primi scegliere il difensore senza alcuna limitazione, e dovendo al contrario i secondi scegliere solo nell'ambito di un distretto che spesso è lontano dai loro luoghi di vita e di lavoro, con sacrificio del loro diritto di difesa;  
    che, come osserva ancora il giudice a quo, il giusto equilibrio tra l'esercizio del diritto di difesa e le esigenze di bilancio dello Stato - preso in considerazione dalla Corte con la sentenza n. 394 del 2000 - potrebbe essere raggiunto limitando il diritto al rimborso alle sole spese sostenute dall'avvocato nel distretto, con esclusione di quelle di trasferta, lasciando libera la scelta del difensore.  
    Considerato che il Tribunale di Pisa dubita delle legittimità dell'art. 9 della legge 30 luglio 1990, n. 217 (Istituzione del patrocinio a spese dello Stato per i non abbienti), nella parte in cui limita la possibilità di nomina del difensore di fiducia agli iscritti ad uno degli albi degli avvocati del distretto di corte d'appello nel quale ha sede il giudice davanti al quale pende il procedimento, anziché prevedere il solo rimborso delle spese sostenute nel distretto, per violazione degli artt. 3 e 24, secondo e terzo comma, della Costituzione;  
    che deve preliminarmente osservarsi che, dopo la proposizione della presente questione, è entrato in vigore il d.P.R. 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia), il cui art. 82, comma 2, prevede espressamente che, in caso di nomina di difensore iscritto ad un elenco di avvocati di un distretto diverso da quello ove ha sede il giudice che procede, non sono dovute le spese e le indennità di trasferta previste dalla tariffa professionale;  
    che l'art. 299 del d.P.R. n. 115 del 2002 cit. ha espressamente abrogato la disposizione denunciata dal Tribunale di Pisa, unitamente all'intero testo della legge 30 luglio 1990, n. 217;  
    che occorre quindi restituire gli atti al giudice rimettente perché valuti, alla luce della modifica legislativa intervenuta, la perdurante rilevanza della questione sollevata nel giudizio a quo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    ordina la restituzione degli atti al Tribunale di Pisa.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 giugno 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Fernanda CONTRI, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria l'11  giugno 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
