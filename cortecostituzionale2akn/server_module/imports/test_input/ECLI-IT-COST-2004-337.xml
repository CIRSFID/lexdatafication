<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/337/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/337/"/>
          <FRBRalias value="ECLI:IT:COST:2004:337" name="ECLI"/>
          <FRBRdate date="28/10/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="337"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/337/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/337/ita@/!main"/>
          <FRBRdate date="28/10/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/337/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/337/ita@.xml"/>
          <FRBRdate date="28/10/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/11/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MEZZANOTTE</cc:presidente>
        <cc:relatore_pronuncia>Franco Bile</cc:relatore_pronuncia>
        <cc:data_decisione>28/10/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Carlo MEZZANOTTE; Giudici: Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 23 luglio 2003, relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal senatore Raffaele (Lino) Jannuzzi nei confronti del magistrato Anna Maria Leone, promosso dal Tribunale di Milano, prima sezione civile, con ricorso depositato il 18 novembre 2003 ed iscritto al n. 256 del registro ammissibilità conflitti. 
    Udito nella camera di consiglio del 29 settembre 2004 il Giudice relatore Franco Bile.  
    Ritenuto che, nel corso di un giudizio civile, promosso da Anna Maria Leone, magistrato con funzioni di sostituto procuratore generale presso la Corte d'appello di Palermo, contro il senatore Raffaele (Lino) Jannuzzi, il Tribunale di Milano, in composizione monocratica, prima sezione civile, con ordinanza emessa il 13 novembre 2003, ha proposto conflitto di attribuzione tra poteri dello Stato avverso la delibera adottata il 23 luglio 2003, con la quale il Senato della Repubblica ha dichiarato che i fatti oggetto di quel processo civile concernono opinioni espresse dal convenuto, quale membro del Parlamento nell'esercizio delle sue funzioni, ai sensi dell'art. 68, primo comma, della Costituzione; 
    che il giudizio a quo concerne la richiesta di risarcimento dei danni asseritamente subiti dall'attrice in conseguenza di un articolo del senatore Jannuzzi pubblicato sul settimanale “Panorama” il 22 ottobre 2002, con il titolo «Pressione bassa ed udienze infinite» – riguardante il processo d'appello nei confronti del senatore Giulio Andreotti, all'epoca in corso a Palermo, nel quale la medesima sosteneva il ruolo di pubblico ministero d'udienza insieme ad altro magistrato – nel cui contesto (secondo l'attrice), al denunciato fine di esprimere ingiustificati e gravi giudizi sulla sua capacità professionale, si faceva falsamente riferimento ad un'abnorme dilatazione dei tempi processuali conseguente a mere esigenze personali della stessa e ad un errore in cui ella sarebbe incorsa nel richiamare l'appartenenza politica di un soggetto tra i molti implicati nel processo; 
    che il Tribunale ritiene che, nella specie, il potere valutativo non sia stato legittimamente esercitato dal Senato, a motivo dell'inesistenza nella condotta del senatore del necessario nesso funzionale fra le opinioni espresse e l'esercizio di funzioni parlamentari, non ravvisabile nel semplice collegamento di argomento e di contesto tra dichiarazioni e attività parlamentare; 
    che, al contrario, secondo il ricorrente, le affermazioni riportate nell'articolo in esame devono ritenersi prive di alcun nesso funzionale con atti rientranti nel mandato parlamentare, non risultando dai documenti di causa che il suo contenuto corrisponda a dichiarazioni espresse dal senatore in sede parlamentare, ovvero costituisca divulgazione di opinioni da lui manifestate nell'ambito di atti parlamentari tipici; 
    che, dunque, il Tribunale di Milano ricorrente chiede che questa Corte: a) «dichiari che non spettava al Senato della Repubblica il potere di qualificare come insindacabili le dichiarazioni contestate al senatore Raffaele (Lino) Jannuzzi, in quanto esercitato al di fuori delle ipotesi previste dall'art. 68, primo comma, Cost.»; b) «annulli la relativa deliberazione del Senato della Repubblica adottata in data 23 luglio 2003». 
    Considerato che in questa fase la Corte è chiamata, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, a deliberare preliminarmente, senza contraddittorio, se il ricorso sia ammissibile in quanto esista la materia di un conflitto la cui risoluzione spetti alla sua competenza, in riferimento ai requisiti soggettivo e oggettivo indicati nel primo comma dello stesso art. 37; 
    che, sotto l'aspetto soggettivo, il Tribunale di Milano, in composizione monocratica, è legittimato a sollevare conflitto di attribuzione tra poteri dello Stato, quale organo competente a dichiarare definitivamente – nel procedimento civile del quale esso è investito – la volontà del potere cui appartiene, in ragione dell'esercizio di funzioni giurisdizionali svolte in posizione di piena indipendenza, costituzionalmente garantita; 
    che anche il Senato della Repubblica, che ha adottato la deliberazione di insindacabilità delle opinioni espresse da un proprio membro, è legittimato ad essere parte del conflitto costituzionale, essendo competente a dichiarare definitivamente la volontà del potere che esso impersona, in relazione all'applicabilità della prerogativa stessa; 
    che, per quanto riguarda il profilo oggettivo del conflitto, il Tribunale di Milano lamenta la lesione delle proprie attribuzioni, costituzionalmente garantite, in conseguenza dell'adozione, da parte del Senato della Repubblica, di una deliberazione che ha affermato – in modo da esso ritenuto arbitrario, perché non corrispondente ai criteri stabiliti dalla Costituzione – l'insindacabilità delle opinioni espresse da un parlamentare, a norma dell'art. 68, primo comma, della Costituzione; 
    che, pertanto, esiste la materia di un conflitto costituzionale di attribuzione, la cui risoluzione spetta alla competenza di questa Corte.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara ammissibile, a norma dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzione proposto dal Tribunale di Milano, in composizione monocratica, prima sezione civile, nei confronti del Senato della Repubblica, con l'atto introduttivo indicato in epigrafe; &#13;
    dispone: &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al ricorrente Tribunale civile di Milano, in composizione monocratica, prima sezione civile; &#13;
    b) che, a cura del ricorrente, l'atto introduttivo e la presente ordinanza siano notificati al Senato della Repubblica, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati nella cancelleria di questa Corte entro il termine di venti giorni dalla notificazione, a norma dell'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 28 ottobre 2004. &#13;
F.to: &#13;
Carlo MEZZANOTTE, Presidente &#13;
Franco BILE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 10 novembre 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
