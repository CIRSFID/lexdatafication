<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/178/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/178/"/>
          <FRBRalias value="ECLI:IT:COST:2006:178" name="ECLI"/>
          <FRBRdate date="20/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="178"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/178/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/178/ita@/!main"/>
          <FRBRdate date="20/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/178/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/178/ita@.xml"/>
          <FRBRdate date="20/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="28/04/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Franco Bile</cc:relatore_pronuncia>
        <cc:data_decisione>20/04/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 38, commi 7 e 8, della legge 28 dicembre 2001, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2002), promosso con ordinanza dell'11 aprile 2005 dalla Corte dei conti, sezione giurisdizionale per la Regione Piemonte, sul ricorso proposto da Carla Lanza contro l'Istituto nazionale di previdenza per i dipendenti dell'amministrazione pubblica (INPDAP), iscritta al n. 515 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 42, prima serie speciale, dell'anno 2005. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 22 marzo 2006 il Giudice relatore Franco Bile. 
    Ritenuto che la Corte dei conti, sezione giurisdizionale per la Regione Piemonte, con ordinanza dell'11 aprile 2005, ha sollevato questione incidentale di legittimità costituzionale dell'art. 38, commi 7 e 8, della legge 28 dicembre 2001, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2002), sotto il profilo che tale normativa, dettando una nuova disciplina dell'indebito previdenziale erogato dall'Istituto nazionale per la previdenza sociale (INPS), come tale non applicabile anche ai trattamenti pensionistici erogati dall'Istituto nazionale di previdenza per i dipendenti dell'amministrazione pubblica (INPDAP), si pone in contrasto con il principio di eguaglianza di cui all'art. 3 della Costituzione, per ingiustificata disparità di trattamento di situazioni sostanzialmente analoghe; 
    che la questione è stata sollevata nel giudizio promosso dalla titolare di un trattamento pensionistico erogato dall'INPDAP, la quale, premesso che l'Istituto le aveva comunicato l'esistenza di un indebito in data 26 aprile 2003, ha contestato la pretesa dell'ente previdenziale al suo integrale recupero senza la riduzione prevista dal citato art. 38, commi 7 e 8, della legge n. 448 del 2001 sugli importi maturati nel  periodo compreso fra il 1° gennaio 1996 e il 31 dicembre 2000; 
    che l'INPDAP ha contestato la spettanza di tale riduzione; 
    che, secondo la Corte rimettente, la norma citata effettivamente fa riferimento ai soli trattamenti pensionistici erogati dall'INPS; 
    che però – a suo avviso – non sembrano sussistere ragioni che giustifichino la concessione del beneficio dell'irripetibilità, parziale o totale, delle somme indebitamente percepite soltanto in favore dei pensionati titolari di trattamenti erogati dall'INPS; 
    che la formulazione delle norme, della cui legittimità costituzionale la Corte rimettente dubita, è simile a quella in precedenza dettata dall'art. 1, commi 260 e 261, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), che aveva introdotto un'analoga, seppur non identica, disciplina della ripetizione delle somme erroneamente erogate per trattamenti pensionistici; 
    che, infatti, tale normativa dell'indebito previdenziale si riferiva in generale ai  pensionati che fruivano di trattamenti «a carico degli enti pubblici di previdenza obbligatoria», mentre il citato art. 38 della legge n. 448 del 2001 fa riferimento solo a quelli che percepiscono prestazioni «a carico dell'INPS»; 
    che ciò comporta – secondo la Corte rimettente – un'ingiustificata disparità di  trattamento per tutti i pensionati che godono delle prestazioni di tutti gli enti previdenziali pubblici diversi dall'INPS;  
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'infondatezza della questione ed osservando che rientra nella discrezionalità del legislatore limitare la deroga  temporanea all'ordinaria disciplina dell'indebito previdenziale ai soli trattamenti erogati dall'INPS. 
    Considerato che la questione di legittimità costituzionale è rilevante ai fini della decisione della controversia pendente innanzi alla Corte rimettente, poiché incide direttamente sulla fondatezza della domanda proposta dalla ricorrente, la quale contesta la pretesa dell'INPDAP all'integrale recupero dell'indebito previdenziale; 
    che la disposizione censurata ha introdotto, con riguardo alle sole prestazioni pensionistiche a carico dell'INPS e limitatamente ai periodi anteriori al 1° gennaio 2001, una speciale deroga all'ordinaria disciplina a regime, prevedendo una soglia reddituale – fissata in € 8.263,31 di reddito imponibile ai fini dell'IRPEF per l'anno 2000 – al di sotto della quale il percettore della prestazione previdenziale indebita non è tenuto a restituirla, sempre che non versi in una situazione di dolo; 
    che per livelli di reddito più elevati è poi riconosciuta una minore agevolazione, nel senso che non si fa luogo al recupero dell'indebito nei limiti di un quarto dell'importo percepito; 
    che – secondo la giurisprudenza di legittimità – questo nuovo criterio del reddito non è aggiuntivo, bensì sostitutivo degli ordinari presupposti dell'irripetibilità delle prestazioni previdenziali indebite;  
    che la disciplina introdotta dalla disposizione censurata è simmetrica ed in buona parte analoga a quella prevista dal citato art. 1, commi 260 e 261, della legge n. 662 del 1996, concernente, più in generale, i trattamenti previdenziali erogati dagli enti pubblici di previdenza obbligatoria; 
    che, in riferimento a tale ultima disciplina, questa Corte ha già chiarito che «le previsioni dell'art. 1 della legge 23 dicembre 1996, n. 662, commi 260-265, che hanno tra l'altro introdotto una soglia reddituale per scriminare la ripetibilità delle prestazioni previdenziali indebite, hanno carattere transitorio applicandosi solo ai periodi (e quindi agli indebiti previdenziali) anteriori al 1° gennaio 1996 e pertanto, per la loro marcata  specialità, non sono idonee ad essere estese al di là delle fattispecie per le quali sono previste» (ordinanza n. 448 del 2000);  
    che questa Corte ha altresì affermato che «non sussiste un'esigenza costituzionale che imponga per l'indebito previdenziale e per quello assistenziale un'identica disciplina, atteso che – pur operando in questa materia un principio di settore, onde la regolamentazione della ripetizione dell'indebito è tendenzialmente sottratta a quella generale del codice civile – rientra però nella discrezionalità del legislatore porre distinte discipline speciali adattandole alle caratteristiche dell'una o dell'altra prestazione» (ordinanza n. 264 del 2004);   
    che, da ultimo, questa Corte (sentenza n. 1 del 2006) ha riconosciuto il «carattere straordinario ed eccezionale» dell'intervento legislativo costituito dalla normativa censurata e quindi la sua intrinseca inidoneità a fungere da utile tertium comparationis per estendere tale disciplina derogatoria ai casi non inclusi; 
    che si tratta comunque di una disciplina suscettibile di applicazione solo retroattiva e limitata nel tempo, sicché essa non è più applicabile agli indebiti previdenziali maturati successivamente alla data suddetta del 1° gennaio 2001;  
    che, quindi, trattandosi di una disciplina del tutto speciale, rientra nella discrezionalità del legislatore definirne l'ambito di applicabilità nel senso della sua limitazione alle prestazioni previdenziali indebitamente erogate dall'INPS; 
    che, in ogni caso, non è possibile porre comparazioni tra sistemi previdenziali diversi – quale quello pubblico e quello privato – e, a maggior ragione, non è possibile una tale comparazione, sotto il profilo del rispetto del principio di eguaglianza, tra discipline derogatorie a carattere eccezionale e transitorio e con effetti unicamente retroattivi, quali sono quelle dettate dall'art. 1, commi 260 e 261, della legge n. 662 del 1996, applicabile ad entrambi i settori, e dall'art. 38, commi 7 e 8, della legge n. 448 del 2001, applicabile solo ai trattamenti previdenziali INPS; 
    che pertanto la questione è manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 38, commi 7 e 8, della legge 28 dicembre 2001, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2002), sollevata, in riferimento all'articolo 3 della Costituzione, dalla Corte dei conti, sezione giurisdizionale per la Regione Piemonte, con l'ordinanza indicata in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 aprile 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Franco BILE, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 28 aprile 2006.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
