<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/146/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/146/"/>
          <FRBRalias value="ECLI:IT:COST:2003:146" name="ECLI"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="146"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/146/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/146/ita@/!main"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/146/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/146/ita@.xml"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/04/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>09/04/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, promossi, nell'ambito di diversi procedimenti penali, dal Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro con ordinanze del 19 marzo, del 21 maggio e del 23 aprile 2002, iscritte rispettivamente al n. 304, al n. 384 e al n. 435 del registro ordinanze 2002 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 26, n. 36 e n. 40, prima serie speciale, dell'anno 2002. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 26 marzo 2003 il Giudice relatore Guido Neppi Modona. 
    Ritenuto che con tre ordinanze di identico tenore, in data 19 marzo, 23 aprile e 21 maggio 2002, il Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro ha sollevato, in riferimento agli artt. 3 e 111, secondo, quarto e quinto comma, della Costituzione, questione di legittimità dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, nella parte in cui richiede il consenso dell'imputato minorenne per la definizione del procedimento nell'udienza preliminare anche nell'ipotesi di sentenza di non luogo a procedere pienamente liberatoria o per improcedibilità; 
    che ad avviso del Tribunale rimettente la norma censurata, subordinando al consenso dell'imputato la definizione del processo minorile nell'udienza preliminare, ha inteso evidentemente dare attuazione al principio sancito dal quinto comma dell'art. 111 Cost., ma attraverso una scelta «alquanto singolare» ha previsto tale regola anche in relazione agli esiti favorevoli all'imputato, senza tenere conto che il suddetto principio costituzionale «postula una sorta di disponibilità del diritto al dibattimento in vista esclusivamente di una probabile pronuncia di colpevolezza» e dovrebbe perciò garantire all'imputato minorenne la possibilità di rinunciare alla definizione anticipata del processo solamente «nella prospettiva di una pronuncia dibattimentale […] più favorevole»; 
    che una interpretazione letterale della disposizione in esame condurrebbe a risultati evidentemente irrazionali, in quanto è intrinsecamente irragionevole prevedere il consenso del minorenne quale condizione per la pronuncia di una sentenza di non luogo a procedere «a carattere pienamente assolutorio» o di improcedibilità, mentre il consenso non è richiesto per la definizione del procedimento con una pronuncia di condanna a norma del comma 2 dello stesso art. 32 del d.P.R. n. 448 del 1988; 
    che sarebbe perciò «indispensabile un intervento» della Corte costituzionale volto ad affermare che «il diritto dell'imputato al contraddittorio dibattimentale dovrebbe incontrare il limite invalicabile rappresentato dal rispetto del principio del favor innocentiae» ed a consentire al giudice dell'udienza preliminare «di emettere una sentenza di non luogo a procedere nelle ipotesi di proscioglimento c.d. "pieno", poiché il dovere di declaratoria immediata costituisce un principio generale immanente al sistema processuale penale»; 
    che la norma censurata sarebbe inoltre illogica e  inconciliabile con i principi che presidiano il sistema processuale minorile, nel quale «si considera "fisiologica" la rinuncia alla pretesa punitiva nell'ottica dell'educazione del minore»; 
    che la differenza di disciplina intercorrente tra la nuova udienza preliminare minorile e l'udienza preliminare ordinaria renderebbe inoltre evidente la disparità di trattamento dei minorenni rispetto agli imputati adulti; 
    che nel giudizio instaurato con ordinanza iscritta al n. 304 del r.o. del 2002 è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che si è riportato all'atto di intervento prodotto nel giudizio relativo alla ordinanza iscritta al n. 566 del r.o. del 2001, decisa con la sentenza n. 195 del 2002. 
    Considerato che con tre ordinanze di uguale tenore il Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro dubita, in riferimento agli artt. 3 e 111, secondo, quarto e quinto comma della Costituzione, della legittimità costituzionale dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, nella parte in cui richiede il consenso dell'imputato minorenne per la definizione del procedimento nell'udienza preliminare anche nell'ipotesi di sentenza di non luogo a procedere pienamente liberatoria o per improcedibilità;  
    che, stante l'identità delle tre ordinanze, va disposta la riunione dei relativi giudizi; 
    che con sentenza n. 195 del 2002, depositata il 16 maggio 2002, questa Corte ha già accolto la medesima questione, dichiarando costituzionalmente illegittima la norma impugnata «nella parte in cui, in mancanza del consenso dell'imputato, preclude al giudice di pronunciare sentenza di non luogo a procedere che non presuppone un accertamento di responsabilità»; 
    che le questioni vanno pertanto dichiarate manifestamente inammissibili. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, sollevate, in riferimento agli artt. 3 e 111, secondo, quarto e quinto comma, della Costituzione, dal Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro, con le ordinanze in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 aprile 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 24 aprile 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
