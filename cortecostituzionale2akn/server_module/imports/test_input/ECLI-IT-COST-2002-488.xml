<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/488/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/488/"/>
          <FRBRalias value="ECLI:IT:COST:2002:488" name="ECLI"/>
          <FRBRdate date="20/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="488"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/488/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/488/ita@/!main"/>
          <FRBRdate date="20/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/488/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/488/ita@.xml"/>
          <FRBRdate date="20/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="26/11/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Gustavo Zagrebelsky</cc:relatore_pronuncia>
        <cc:data_decisione>20/11/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 116, comma 13, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso con ordinanza emessa il 14 dicembre 2001 dal Giudice di pace di Sorgono, nel procedimento civile vertente tra Andrea Murru e la Prefettura di Nuoro, iscritta al n. 105 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 12, prima serie speciale, dell'anno 2002. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 6 novembre 2002 il Giudice relatore Gustavo Zagrebelsky.    
    Ritenuto che, con ordinanza del 14 dicembre 2001, il Giudice di pace di Sorgono ha sollevato, in riferimento all'art. 3 della Costituzione, questione di costituzionalità dell'art. 116, comma 13, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), «nella parte in cui non differenzia l'ipotesi di guida di un motociclo di oltre 125 centimetri cubici con patente di categoria B [...] da quella di guida dello stesso motociclo senza patente alcuna»;   
    che il rimettente: (a) richiamata la sentenza n. 3 del 1997 di questa Corte, che ha ritenuto costituzionalmente illegittimo l'art. 116, comma 13, del codice della strada, nella parte in cui prevedeva una sanzione penale per coloro che, in possesso della patente di categoria B (abilitante alla guida di motocicli fino a 125 centimetri cubici e fino a 11 cavalli di potenza), avessero condotto motocicli con caratteristiche superiori, per ingiustificata disparità di trattamento rispetto a coloro che, anch'essi in possesso della sola patente di categoria B, avessero condotto autoveicoli di categoria superiore (come autobus e autosnodati), essendo questi puniti, secondo l'art. 125, comma 3, del codice della strada, soltanto con una sanzione amministrativa pecuniaria, e (b) rilevata l'intervenuta depenalizzazione del reato di guida senza patente (art. 116, comma 13, codice della strada) a opera dell'art. 19 del decreto legislativo 30 dicembre 1999, n. 507, ritiene che, pur dopo la citata depenalizzazione, persista, nella disciplina contenuta nello stesso art. 116, comma 13, una ingiustificata discriminazione, poiché tale norma stabilisce, per la fattispecie di guida di motociclo di cilindrata superiore a 125 centimetri cubici con la sola patente B (anziché la prevista patente A), una sanzione amministrativa pecuniaria - equivalente a quella prevista per  la  guida  senza  patente - da quattro a sedici milioni di lire, «enormemente  più severa» rispetto a quella (da lire duecentoquarantaduemilaquattrocento a lire novecentosessantanovemilaseicento) prevista, al tempo dell'ordinanza di rimessione, dall'art. 125 dello stesso codice per l'«analogo» comportamento di chi, con la  patente B, conduca un automezzo per il quale sia richiesta una patente superiore; 
    che nel giudizio così promosso è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per l'inammissibilità o l'infondatezza della questione. 
    Considerato che l'ordinanza del Giudice di pace di Sorgono non contiene elementi idonei a dimostrare la sussistenza del requisito della rilevanza della questione di costituzionalità sollevata, poiché non fornisce alcuna indicazione sui fatti che sono oggetto del giudizio cui il rimettente è chiamato; 
    che per tale omissione, che si risolve in un difetto di motivazione circa la rilevanza della questione, quest'ultima deve essere dichiarata, secondo il costante orientamento di questa Corte (per tutte, ordinanze n. 418 e n. 385 del 2002; n. 567 e n. 495 del 2000), manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 116, comma 13, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata, in riferimento all'art. 3 della Costituzione, dal Giudice di pace di Sorgono, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 novembre 2002. &#13;
    F.to: &#13;
    Cesare RUPERTO, Presidente &#13;
    Gustavo ZAGREBELSKY, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 26 novembre 2002. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
