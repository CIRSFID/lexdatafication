<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2014/198/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2014/198/"/>
          <FRBRalias value="ECLI:IT:COST:2014:198" name="ECLI"/>
          <FRBRdate date="07/07/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="198"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2014/198/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2014/198/ita@/!main"/>
          <FRBRdate date="07/07/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2014/198/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2014/198/ita@.xml"/>
          <FRBRdate date="07/07/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="11/07/2014" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2014</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>CASSESE</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>07/07/2014</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Sabino CASSESE; Giudici : Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 657, comma 4, del codice di procedura penale, promosso dal Tribunale di Lucera nel procedimento penale a carico di M.E. con ordinanza del 27 giugno 2013, iscritta al n. 233 del registro ordinanze 2013 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 45, prima serie speciale, dell'anno 2013.
 Visti l'atto di costituzione di M.E., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 20 maggio 2014 il Giudice relatore Giuseppe Frigo;
 uditi gli avvocati Raffaele Lepore e Mercurio Galasso per M.E. e l'avvocato dello Stato Maurizio Greco per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Con ordinanza depositata il 27 giugno 2013, il Tribunale di Lucera ha sollevato, in riferimento agli artt. 3, 13, primo comma, e 27, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 657, comma 4, del codice di procedura penale, in forza del quale, nella determinazione della pena detentiva da eseguire, sono computate soltanto la custodia cautelare subita o le pene espiate senza titolo dopo la commissione del reato per il quale è stata inflitta la pena che deve essere eseguita.
 Il giudice a quo premette di essere investito dell'incidente di esecuzione sollevato da un condannato, volto ad ottenere - a fronte del diniego tacito del pubblico ministero, funzionalmente competente ai sensi dell'art. 657 cod. proc. pen. - che dalla pena detentiva da espiare in forza di una sentenza dello stesso Tribunale di Lucera, divenuta irrevocabile il 20 maggio 2013, sia detratto il periodo di custodia cautelare ingiustamente subita dal richiedente per altri reati dal 17 giugno 1983 al 15 settembre 1986.
 Al riguardo, il Tribunale rimettente rileva che, in base alle risultanze degli atti, l'istante, nel lontano 1983, era stato sottoposto a «carcerazione preventiva» per tre anni e tre mesi, in quanto coinvolto in un "maxi-processo" per reati di criminalità organizzata, venendo poi assolto dalla Corte d'appello di Napoli dai reati ascrittigli per non aver commesso il fatto. 
 All'accoglimento della richiesta osterebbe, tuttavia, la disposizione censurata, in base alla quale l'ingiusta carcerazione, per poter essere computata in detrazione, deve seguire, e non già precedere, la commissione del reato per il quale vi è stata condanna alla pena da espiare. Nel caso in esame, di contro, i reati cui si riferisce la pena da eseguire sono stati commessi dal richiedente nel 2000 e, dunque, in epoca ampiamente successiva all'ingiusta custodia cautelare.
 Recependo l'eccezione formulata in via subordinata dal richiedente, il giudice a quo dubita, peraltro, della legittimità costituzionale dell'indicata condizione limitativa.
 La relativa previsione violerebbe, in specie, il principio di eguaglianza (art. 3 Cost.) e quello del favor libertatis (desumibile dal disposto dell'art. 13, primo comma, Cost.), determinando una ingiustificata disparità di trattamento fra i soggetti che abbiano ugualmente riportato una condanna definitiva a pena detentiva e subito una ingiusta carcerazione. A parità di situazione, la possibilità di "compensare" la seconda con la prima verrebbe, infatti, a dipendere da un fattore meramente casuale di natura temporale, quale l'anteriorità del reato, per il quale deve essere determinata la pena da eseguire, rispetto alla carcerazione ingiusta.
 La norma censurata violerebbe, inoltre, l'art. 27, terzo comma, Cost., giacché, impedendo di scomputare il periodo di ingiusta carcerazione a chi ha commesso il reato successivamente ad essa - ossia proprio al soggetto che, a ben guardare, più meriterebbe il beneficio, avendo subito l'ingiusta detenzione quando era incensurato - vanificherebbe la finalità rieducativa della pena ed ostacolerebbe il reinserimento del reo nel tessuto sociale.
 La disposizione in esame si porrebbe in contrasto con i principi di eguaglianza e di ragionevolezza anche sotto un ulteriore e diverso profilo: e, cioè, in quanto fondata su una presunzione assoluta non rispondente ad una regola di esperienza generalizzata.
 Alla luce delle indicazioni della relazione al progetto preliminare del codice di procedura penale, la preclusione denunciata sarebbe finalizzata, infatti, ad evitare che il diritto al recupero della detenzione ingiustamente sofferta si risolva in un incentivo alla commissione di azioni criminose. Tale paventato effetto criminogeno sarebbe presunto iuris et de iure dalla legge, non essendo il divieto derogabile neanche quando vi sia la prova certa che quel pericolo non si è concretizzato.
 Per consolidata giurisprudenza della Corte costituzionale, tuttavia, le presunzioni assolute, specie quando limitano un diritto fondamentale della persona - quale il diritto alla libertà personale - violano il principio di eguaglianza se sono arbitrarie e irrazionali, cioè se non rispondono a dati di esperienza generalizzati, riassumibili nella formula dell'id quod plerumque accidit. In particolare, l'irragionevolezza della presunzione assoluta si coglie tutte le volte in cui sia agevole formulare ipotesi di accadimenti reali contrari alla generalizzazione posta a base della presunzione stessa.
 Tale situazione si riscontrerebbe puntualmente nell'ipotesi in esame. Se è certamente possibile, infatti, che taluno sia spinto a delinquere dal proposito di recuperare la carcerazione subita ingiustamente, altrettanto frequente sarebbe il caso di chi - proprio per aver sofferto «la terribile esperienza di conoscere il carcere da innocente» - si guarda bene dal commettere reati solo per "riscuotere" il "credito" che da tale esperienza gli deriva.
 Per questo verso, si potrebbe anche ritenere - a parere del giudice a quo - che il vulnus ai parametri costituzionali derivi non dalla presunzione in sé, ma dal suo carattere assoluto, che implica un divieto indiscriminato e totale di scomputo. In simile prospettiva, la compatibilità costituzionale potrebbe essere ripristinata trasformando la presunzione in relativa: dichiarando, cioè, costituzionalmente illegittima la norma denunciata nella parte in cui non consente al giudice di derogare al divieto in presenza di elementi probatori di segno contrario alla presunzione stessa (quale, ad esempio, il lungo tempo trascorso tra l'ingiusta carcerazione e la successiva commissione del reato); elementi che sarebbe onere dell'interessato fornire.
 La questione sarebbe, altresì, rilevante nel giudizio a quo, posto che la preclusione prevista dal comma 4 dell'art. 657 cod. proc. pen. costituisce l'unico elemento ostativo all'accoglimento dell'istanza del condannato.
 La rilevanza non verrebbe meno neanche qualora la Corte costituzionale, «optando per l'alternativa» dianzi prospettata, si limitasse a "degradare" in relativa la presunzione assoluta insita nella norma censurata. Nella specie, infatti, la prova contraria risulterebbe senz'altro raggiunta, alla luce del lunghissimo lasso temporale intercorso tra la custodia cautelare ingiustamente sofferta dal richiedente e i fatti cui attiene la pena da espiare.
 2.- È intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o infondata.
 Ad avviso della difesa dello Stato, la questione sarebbe inammissibile, in quanto a carattere alternativo. Il rimettente avrebbe chiesto, infatti, al tempo stesso, un intervento ablatorio e uno di tipo additivo sulla norma censurata, senza chiarire se le due richieste siano in rapporto di «alternatività irrisolta» o di subordinazione.
 Quanto al merito, la questione sarebbe già stata affrontata e risolta dalla Corte costituzionale con la sentenza n. 442 del 1988, che ha dichiarato infondata una questione analoga relativa al previgente art. 271, quarto comma, del codice di procedura penale del 1930. Le considerazioni svolte nell'occasione dovrebbero condurre anche in questo caso a ritenere che la disciplina legislativa dell'istituto della cosiddetta fungibilità della detenzione ingiustamente patita «non contiene in alcun modo, regole irragionevolmente discriminatorie», né viola gli altri parametri invocati dal rimettente.
 3.- Si è costituito, altresì, M.E., condannato istante nel giudizio a quo, chiedendo l'accoglimento della questione.
 4.- Con successiva memoria, la parte privata - oltre a ripercorrere e sviluppare le censure formulate dal giudice rimettente, da essa pienamente condivise - ha rilevato come la previsione dell'art. 657, comma 4, cod. proc. pen. si ponga in contrasto anche con l'art. 24, quarto comma, Cost., che impone al legislatore di determinare «le condizioni e i modi per la riparazione degli errori giudiziari». Il divieto stabilito dalla disposizione denunciata vanificherebbe, infatti, «tali procedure riparatorie e lo stesso esercizio del diritto di difesa, di per sé ancora più incomprimibile se volto a tutelare la libertà della persona».
 La parte privata ha contestato, per altro verso, la fondatezza dell'eccezione di inammissibilità della questione, sollevata dal Presidente del Consiglio dei ministri: dalla lettura dell'ordinanza di rimessione risulterebbe, infatti, palese come il giudice a quo abbia chiesto un intervento ablatorio, accennando all'ulteriore richiesta a carattere additivo solo in via argomentativa e per completezza di trattazione.
 Non sarebbe altresì conferente, nel merito, il richiamo dell'Avvocatura dello Stato alla sentenza n. 442 del 1988, relativa all'art. 271, quarto comma, del codice di rito abrogato, trattandosi di pronuncia le cui argomentazioni - sintoniche con l'impostazione inquisitoria di detto codice - avrebbero perso di attualità a seguito del passaggio ad un modello processuale di tipo accusatorio, operato dal codice del 1988.Considerato in diritto1.- Il Tribunale di Lucera dubita della legittimità costituzionale dell'art. 657, comma 4, del codice di procedura penale, in forza del quale, nella determinazione della pena detentiva da eseguire, si tiene conto soltanto della custodia cautelare subita o delle pene espiate senza titolo dopo la commissione del reato per il quale la pena che deve essere eseguita è stata inflitta.
 Ad avviso del giudice a quo, la norma censurata violerebbe i principi di eguaglianza (art. 3 della Costituzione) e del favor libertatis (desumibile dall'art. 13, primo comma, Cost.), determinando una ingiustificata disparità di trattamento fra i soggetti che hanno riportato una condanna definitiva a pena detentiva e subito una ingiusta carcerazione. A parità di situazione, la possibilità di "compensare" la seconda con la prima verrebbe, infatti, a dipendere da un fattore meramente casuale di natura temporale, quale la circostanza che l'ingiusta carcerazione segua, e non già preceda, la commissione del reato per il quale deve essere determinata la pena da eseguire.
 Sarebbe violato, inoltre, l'art. 27, terzo comma, Cost., giacché la preclusione censurata - che colpirebbe il soggetto, in realtà, maggiormente meritevole del beneficio, avendo subito l'ingiusta detenzione quando era ancora incensurato - vanificherebbe la finalità rieducativa della pena ed ostacolerebbe il reinserimento del condannato nel tessuto sociale.
 La norma denunciata violerebbe i principi di eguaglianza e di ragionevolezza (art. 3 Cost.) anche perché fondata su una presunzione assoluta arbitraria: quella, cioè, che la possibilità di scomputare la detenzione ingiustamente sofferta dalla pena inflitta per un successivo reato si risolva in un incentivo a delinquere. Detta presunzione non esprimerebbe, infatti, una regola di esperienza generalizzata, essendo agevolmente ipotizzabili casi nei quali proprio la traumatica vicenda di aver conosciuto «il carcere da innocente», lungi dallo stimolare, distoglie chi l'ha subita dal commettere reati solo per "riscuotere" il corrispondente "credito di pena".
 Per questo verso, si potrebbe anche ritenere - secondo il giudice a quo - che il vulnus ai parametri costituzionali evocati derivi, anziché dalla presunzione in sé, dal suo carattere assoluto: prospettiva nella quale la compatibilità con la Costituzione andrebbe assicurata trasformando la presunzione in relativa e, segnatamente, dichiarando l'illegittimità costituzionale della norma censurata nella parte in cui non prevede che il giudice possa derogare al divieto quando l'interessato abbia offerto la prova che il temuto effetto criminogeno non si è, di fatto, realizzato. 
 2.- L'eccezione di inammissibilità formulata dall'Avvocatura dello Stato, sull'assunto che si tratti di questione "ancipite", non è fondata.
 Al di là di qualche ambiguità di ordine lessicale, dal complessivo tessuto motivazionale dell'ordinanza di rimessione emerge con sufficiente chiarezza che il rimettente chiede, in via principale, a questa Corte l'ablazione della norma censurata, prospettando solo in via subordinata un intervento di tipo additivo, che renda superabile la limitazione sancita dalla norma stessa in presenza di elementi probatori di segno contrario alla presunzione su cui essa - in assunto - si fonda.
 Gli interventi richiesti non si pongono, pertanto, in rapporto di alternatività irrisolta, ma di subordinazione fra loro: circostanza che rende la questione ammissibile (ex plurimis, sentenza n. 280 del 2011).
 3.- Sempre in via preliminare, va osservato che, per giurisprudenza costante di questa Corte, l'oggetto del giudizio di legittimità costituzionale in via incidentale è limitato alle disposizioni e ai parametri indicati nelle ordinanze di rimessione, non potendo essere presi in considerazione, oltre i limiti in queste fissati, ulteriori questioni o profili di costituzionalità dedotti dalle parti, eccepiti ma non fatti propri dal giudice a quo, oppure diretti ad ampliare o modificare successivamente il contenuto delle ordinanze stesse (ex plurimis, sentenze n. 310, n. 227 e n. 50 del 2010).
 Ne deriva che sono inammissibili, e non possono formare oggetto di esame in questa sede, le deduzioni della parte privata dirette ad estendere il thema decidendum, tramite la denuncia dell'asserito contrasto della norma censurata anche con l'ulteriore parametro costituito dall'art. 24, quarto comma, Cost.
 4.- Nel merito, la questione non è fondata.
 Come segnala la relazione al progetto preliminare del codice, l'art. 657 cod. proc. pen. adotta un «criterio di fungibilità» della carcerazione subita con la pena detentiva da espiare particolarmente ampio, «volto a ricomprendere tutti i periodi di privazione della libertà personale comunque sofferti senza effettiva giustificazione».
 Nel determinare la pena detentiva da eseguire in forza di una pronuncia definitiva di condanna, il pubblico ministero è tenuto, infatti, a computare tanto il periodo di custodia cautelare sofferta per lo stesso o per altro reato, anche se ancora in corso (comma 1 dell'art. 657 cod. proc. pen.), quanto il periodo di pena detentiva espiata senza titolo (s'intende, per altro reato), nel senso precisato dal comma 2: ossia «quando la relativa condanna è stata revocata, quando per il reato è stata concessa amnistia o quando è stato concesso indulto, nei limiti dello stesso».
 Tale regime di fungibilità - giustificato, sempre secondo la relazione al progetto preliminare, «dalla prevalenza del principio del favor libertatis cui deve essere improntata tutta la legislazione penale» - è suscettibile di configurare anche una riparazione "in forma specifica" per l'ingiusta privazione della libertà personale, come attestano le previsioni degli artt. 314, comma 4, e 643, comma 2, cod. proc. pen., che escludono il diritto all'ordinaria riparazione pecuniaria per quella parte della custodia cautelare o della detenzione che sia stata computata ai fini della determinazione della misura di una pena.
 Il meccanismo di "compensazione" incontra, peraltro, il limite di ordine temporale enunciato dalla norma oggi sottoposta a scrutinio (comma 4 dell'art. 657 cod. proc. pen.): limite che riprende, con gli opportuni adattamenti, quello già stabilito dal previgente art. 271, quarto comma, cod. proc. pen. del 1930. La fungibilità opera, cioè, soltanto per la custodia cautelare subita o le pene espiate dopo la commissione del reato per il quale deve essere determinata la pena da eseguire.
 Tale sbarramento temporale si giustifica alla luce di due ordini di considerazioni, tra loro strettamente correlati.
 In primo luogo - ed è questa la spiegazione tradizionale del divieto - esso è imposto dall'esigenza di evitare che l'istituto della fungibilità si risolva in uno stimolo a commettere reati, trasformando il pregresso periodo di carcerazione in una "riserva di impunità" utilizzabile per elidere le conseguenze di futuri illeciti penali, e che concreterebbe addirittura una sorta di "licenza di delinquere" quanto ai reati punibili in misura uguale o inferiore alla carcerazione sofferta. Come puntualmente si afferma nella relazione al progetto preliminare, «il recupero della detenzione ingiustamente sofferta deve funzionare come correttivo alle disfunzioni della macchina giudiziaria e compensazione dell'ingiusta carcerazione, ma non certo come incentivo alla commissione successiva di azioni criminose».
 In secondo luogo, poi - ma, in realtà, prima ancora - risponde ad una fondamentale esigenza logico-giuridica che la pena, ancorché scontata nella forma anomala dell'"imputazione" ad essa del periodo di ingiusta detenzione sofferta per altro reato, debba comunque seguire, e non già precedere, il fatto criminoso cui accede e che mira a sanzionare. È questa, infatti, la condizione indispensabile affinché la pena possa esplicare le funzioni sue proprie, e particolarmente quelle di prevenzione speciale e rieducativa. Una pena anticipata rispetto al reato, anziché sconsigliarne la commissione, rischierebbe - come detto - di incoraggiarla e, d'altro canto, non potrebbe in nessun caso costituire uno strumento di emenda del reo. Come questa Corte ha già avuto modo di rilevare, nel dichiarare infondata una questione di legittimità costituzionale parzialmente analoga avente ad oggetto il citato art. 271, quarto comma, del codice abrogato, «le finalità "rieducative" di cui al terzo comma dell'art. 27 Cost. [...] possono aver senso anche se riferite ad "altro" reato ma [...] certamente non possono mai riguardare un reato "da commettere"» (sentenza n. 442 del 1988).
 5.- Ciò posto, nessuna delle censure del giudice a quo coglie nel segno.
 Quanto, infatti, alla denunciata violazione dell'art. 3 Cost. per irragionevole disparità di trattamento, questa Corte ha già evidenziato come la situazione di chi ha sofferto la custodia cautelare (o espiato una pena senza titolo) dopo la commissione di altro reato non sia affatto identica, sotto il profilo che interessa, a quella di chi l'ha subita (o espiata) anteriormente. Solo per quest'ultimo soggetto la prospettiva di scomputare dalla pena il tempo della pregressa carcerazione può rientrare nel calcolo che conduce alla deliberazione criminosa; non per il primo, posto che «scontare, in avvenire, custodie cautelari o carcerazioni in esecuzione di pena non può in alcun modo motivare il soggetto a delinquere» (sentenza n. 442 del 1988).
 A ciò va aggiunto che - alla luce di quanto dianzi osservato - solo in rapporto a chi ha sofferto la detenzione ingiusta dopo la commissione del reato il meccanismo di compensazione con la pena da espiare è coerente con le funzioni proprie di quest'ultima.
 Sicché, in conclusione, «per diverse situazioni, dal punto di vista oggettivo e soggettivo, il legislatore ha [...] ragionevolmente previsto diverse discipline giuridiche» (sentenza n. 442 del 1988).
 6.- La preclusione censurata non viola, per analoghe ragioni, neppure l'art. 13, primo comma, Cost.
 La scelta legislativa di non privilegiare, nell'ipotesi considerata, il «favor libertatis» trova giustificazione, da un lato, nell'esigenza di evitare - per ragioni di difesa sociale e di tutela della collettività - che chi ha sofferto un periodo di custodia cautelare o di detenzione per altro reato, sia pure indebita, sia indotto a delinquere o, comunque, rinvenga motivi "favorevoli" alla commissione di reati nella possibilità di sottrarsi alle relative conseguenze sanzionatorie opponendo in compensazione un "credito di pena" precedentemente maturato (sentenza n. 442 del 1988); dall'altro, nella correlata esigenza di non creare le premesse per uno stravolgimento delle funzioni di prevenzione e di emenda che la pena dovrebbe esplicare.
 7.- Per quanto attiene, poi, alla denunciata violazione dell'art. 27, terzo comma, Cost., è già insito nelle considerazioni precedentemente svolte come tale precetto costituzionale, lungi dal collidere con la preclusione censurata, concorra a giustificarla, stante l'impossibilità di concepire una funzione rieducativa in rapporto a reati che debbano essere ancora commessi.
 8.- Non riscontrabile, infine, è l'asserito contrasto con i principi di eguaglianza e di ragionevolezza (art. 3 Cost.), conseguente al fatto che la norma censurata poggerebbe su una presunzione assoluta arbitraria, in quanto non rispondente ad una regola di esperienza generalizzata, quale il supposto effetto criminogeno del diritto a "recuperare" il periodo di ingiusta detenzione sofferto prima della commissione del reato: essendo, in fatto, ben possibile che la vicenda traumatica della carcerazione ingiusta abbia un opposto effetto dissuasivo e che, comunque, il successivo reato venga perpetrato per ragioni del tutto avulse dall'intento di "riscuotere" il "credito di pena".
 Al riguardo, è dirimente il rilievo che il giudice a quo qualifica come presunzione assoluta quella che, in realtà, è la ratio legis: o, meglio, una delle due rationes della limitazione denunciata (l'altra consistendo nell'evidenziata esigenza logico-giuridica che la pena segua, e non già preceda, il reato). Ratio peraltro non scalfita dalla eventualità, prospettata dal rimettente, che la "riserva di impunità", conseguente all'ipotetica rimozione della preclusione, possa in concreto non pesare sul processo motivazionale che induce a delinquere chi ne beneficia.
 Tali considerazioni escludono che possa accedersi anche all'intervento richiesto dal rimettente in via subordinata, volto a trasformare l'ipotetica presunzione assoluta in relativa.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara non fondata la questione di legittimità costituzionale dell'art. 657, comma 4, del codice di procedura penale, sollevata, in riferimento agli artt. 3, 13, primo comma, e 27, terzo comma, della Costituzione, dal Tribunale di Lucera con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 luglio 2014.&#13;
 F.to:&#13;
 Sabino CASSESE, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria l'11 luglio 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
