<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/5/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/5/"/>
          <FRBRalias value="ECLI:IT:COST:2010:5" name="ECLI"/>
          <FRBRdate date="11/01/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="5"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/5/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/5/ita@/!main"/>
          <FRBRdate date="11/01/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/5/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/5/ita@.xml"/>
          <FRBRdate date="11/01/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="14/01/2010" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2010</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Grossi</cc:relatore_pronuncia>
        <cc:data_decisione>11/01/2010</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Francesco AMIRANTE; Giudici : Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell’art. 36, comma 3 [recte: comma 1], della legge provinciale di Bolzano 28 ottobre 2001, n. 17 (Legge sui masi chiusi), come sostituito dall’art. 34 [recte: 33], comma 3, della legge della Provincia autonoma di Bolzano 23 giugno [recte: luglio] 2007, n. 6 (Modifiche di leggi provinciali in vari settori), promosso dal Tribunale regionale di giustizia amministrativa di Bolzano nel procedimento vertente tra Dissertori Pichler Erica e la Provincia autonoma di Bolzano con ordinanza del 16 marzo 2009, iscritta al n. 168 del registro ordinanze 2009 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 24, prima serie speciale, dell’anno 2009.
 Visto l’atto di costituzione di Dissertori Pichler Erica e quello, fuori termine, della Provincia autonoma di Bolzano;
 udito nell’udienza pubblica del 15 dicembre 2009 il Giudice relatore Paolo Grossi;
 udito l’avvocato Luigi Manzi per la Provincia autonoma di Bolzano. 
 Ritenuto che – nel corso di un giudizio, promosso per ottenere l’annullamento della decisione della Commissione provinciale per i masi chiusi con la quale la stessa ha autorizzato lo svincolo di un maso chiuso, disponendo la contestuale aggregazione delle relative particelle ad altri masi chiusi – il Tribunale regionale di giustizia amministrativa-Sezione autonoma per la Provincia di Bolzano, con ordinanza emessa il 16 marzo 2009, ha sollevato questione di legittimità costituzionale dell’art. 36, comma 3 [recte: comma 1], della legge provinciale di Bolzano 28 ottobre 2001, n. 17 (Legge sui masi chiusi), come sostituito dall’art. 34 [recte: 33], comma 3, della legge provinciale di Bolzano 23 giugno [recte: luglio] 2007, n. 6 (Modifiche di leggi provinciali in vari settori);
 che la norma viene censurata – «per contrasto con gli artt. 3, 41, 42 e 117 della Costituzione e con il principio di ragionevolezza (art. 97 della Costituzione)» – «nella parte in cui contestualmente all’atto di revoca della qualifica di maso chiuso la commissione locale per i masi chiusi deve disporre l’aggregazione delle particelle ad altri masi chiusi, anche nei casi in cui l’interessato non sia proprietario di altri masi chiusi»;
 che, affermata la rilevanza della questione di costituzionalità, dedotta dalla parte quale unico motivo di impugnazione dell’atto de quo, il rimettente esclude la praticabilità di «un’interpretazione costituzionalmente conforme della disposizione, attesa la sua formulazione letteralmente chiara e apodittica»;
 che, nel merito, il giudice a quo rileva che la disposizione censurata impone al proprietario di un maso chiuso, che richieda la revoca della qualifica di maso chiuso per difetto oggettivo di redditività dello stesso, l’aggregazione delle particelle già facenti parte del maso ad altri masi; con la conseguenza che il proprietario del maso svincolato, il quale non «sia proprietario anche di altri masi (il che è la regola), verrebbe costretto ad alienare a terzi (a titolo di compravendita, donazione o altro titolo) tutte le particelle produttive dell’ex maso chiuso»; e ciò – ad avviso del giudicante – rende dubbia la legittimità costituzionale della citata disposizione sia in relazione agli artt. 41 e 42 della Costituzione, sia in relazione al principio di ragionevolezza (art. 97 della Costituzione, applicabile non solo agli atti amministrativi, ma anche agli atti legislativi);
 che, infatti, il rimettente osserva che se i principi della libertà dell’iniziativa economica (art. 41) e del libero godimento della proprietà privata (art. 42) certamente consentono che agli stessi siano imposti dal legislatore dei limiti, ai fini di farli armonizzare con l’utilità sociale e rendere in tal modo possibile quella funzione sociale che non può disgiungersi dal godimento della proprietà, compresa la facoltà di poterne disporre liberamente, tuttavia non sembra che l’obbligo di aggregazione ad altri masi possa essere in qualche modo collegato alla funzione sociale dell’istituto del maso chiuso; infatti tale collegamento (che solo potrebbe giustificare deroghe anche al principio di uguaglianza) deve essere individuato nell’esigenza di garantire, attraverso la salvaguardia dell’unità del maso, per quanto sufficientemente redditizio, «la funzione oggettiva del maso nell’ambito della famiglia»;
 che quindi, secondo il rimettente, l’obbligo incondizionato di aggregare le particelle «svincolate» ad altri masi chiusi, porterebbe alla conseguenza – «almeno nell’ipotesi che il proprietario non disponesse di altri masi chiusi o non si trovassero terzi proprietari interessati all’acquisto» – che il maso rimarrebbe «chiuso» senza averne i presupposti di redditività, venendone così, irragionevolmente e senza il rispetto del criterio di proporzionalità, snaturata irrimediabilmente la ratio. Oltretutto, la norma apparirebbe «di dubbia attuabilità o addirittura inattuabile, oltre che nel caso della necessità dell’aggregazione delle particelle a masi chiusi di terzi, nel caso – tutt’altro che teorico – che nelle vicinanze non si trovino affatto dei masi chiusi a cui aggregare le particelle»;
 che si è costituita la ricorrente, concludendo per l’accoglimento della questione, sulla base di argomentazioni analoghe a quelle svolte nell’ordinanza di rimessione;
 che la parte ribadisce, in particolare, come la norma impugnata si ponga in contrasto con gli artt. 41 e 42 Cost. senza alcuna giustificazione connessa alla esigenza di tutela del maso chiuso, giacché l’obbligatoria contestuale aggregazione dei terreni già costituenti il maso a terzi proprietari di altri masi chiusi risulta totalmente estranea ad ogni ragione di salvaguardia della funzione oggettiva e della ratio ispiratrice dell’istituto, per la semplice considerazione che detto maso chiuso una volta “svincolato” non esiste più, per cui non può venire salvaguardato con la forzata vendita a terzi di tutte le particelle fondiarie.
 Considerato che il Tribunale regionale di giustizia amministrativa - Sezione autonoma per la Provincia di Bolzano censura l’art. 36, comma 1, della legge provinciale di Bolzano 28 ottobre 2001, n. 17 (Legge sui masi chiusi), come sostituito dall’art. 33, comma 3, della legge provinciale di Bolzano 23 luglio 2007, n. 6 (Modifiche di leggi provinciali in vari settori), «nella parte in cui contestualmente all’atto di revoca della qualifica di maso chiuso la commissione locale per i masi chiusi deve disporre l’aggregazione delle particelle ad altri masi chiusi, anche nei casi in cui l’interessato non sia proprietario di altri masi chiusi»;
 che, secondo il giudice a quo, la imposizione al proprietario di un maso chiuso, che ne richieda la revoca, della aggregazione delle particelle già facenti parte del maso ad altri masi, determinerebbe (senza alcun collegamento alla funzione sociale dell’istituto del maso chiuso) la violazione dei principi di libertà dell’iniziativa economica (art. 41 Cost.), di libero godimento della proprietà privata (art. 42 Cost.) e di ragionevolezza (in combinato disposto con l’art. 97 Cost.), in quanto: (a) il proprietario del maso svincolato, il quale non «sia proprietario anche di altri masi (il che è la regola), verrebbe costretto ad alienare a terzi (a titolo di compravendita, donazione o altro titolo) tutte le particelle produttive dell’ex maso chiuso»; (b) il maso – «almeno nell’ipotesi che il proprietario non disponesse di altri masi chiusi o non si trovassero terzi proprietari interessati all’acquisto» – rimarrebbe «chiuso» senza averne i presupposti di redditività; (c) la norma apparirebbe «di dubbia attuabilità o addirittura inattuabile, oltre che nel caso della necessità dell’aggregazione delle particelle a masi chiusi di terzi, nel caso – tutt’altro che teorico – che nelle vicinanze non si trovino affatto dei masi chiusi a cui aggregare le particelle»;
 che, tuttavia – formulate in tali termini le censure (non essendo, peraltro, in alcun modo argomentata l’ulteriore doglianza riferita genericamente all’art. 117 Cost.) – il rimettente omette di precisare se la parte ricorrente sia o meno proprietaria anche di altri masi, se la medesima non abbia reperito terzi proprietari di altri masi interessati all’acquisto, se nelle vicinanze del maso svincolato non si trovino dei masi chiusi a cui aggregare le particelle;
 che, in tal modo, l’ordinanza di rimessione – in quanto viziata da una carente descrizione della fattispecie, che determina l’insanabile astrattezza della questione (ordinanze n. 398 e n. 12 del 2008) – non consente a questa Corte di valutarne la rilevanza ai fini della definizione del giudizio a quo (ordinanze n. 127 e n. 79 del 2009);
 che sotto diverso profilo, come ulteriore motivo di inammissibilità, si aggiunge la mancata sperimentazione di una lettura costituzionalmente orientata della norma censurata, o quantomeno la mancata motivazione della impossibilità di pervenire ad essa;
 che, infatti, il rimettente si limita ad affermare acriticamente che «non appare possibile un’interpretazione costituzionalmente conforme della disposizione, attesa la sua formulazione letteralmente chiara ed apodittica», omettendo completamente di rilevare che lo stesso comma 1 del censurato art. 36 esplicitamente sancisce, nel periodo immediatamente successivo a quello oggetto del presente scrutinio di costituzionalità, che – sebbene «solo in casi eccezionali e debitamente fondati» – «si può prescindere da tale aggregazione»;
 che, così facendo, il rimettente non si pone il problema della natura e della portata di siffatta clausola derogatoria della regola imposta dalla norma censurata, né della eventuale applicabilità della eccezione al caso concreto, in ragione appunto delle riferite peculiarità degli effetti derivanti dallo specifico atto di revoca del maso chiuso sottoposto al suo giudizio;
 che, pertanto, la questione è manifestamente inammissibile.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 	dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell’art. 36, comma 1, della legge provinciale di Bolzano 28 ottobre 2001, n. 17 (Legge sui masi chiusi), come sostituito dall’art. 33, comma 3, della legge provinciale di Bolzano 23 luglio 2007, n. 6 (Modifiche di leggi provinciali in vari settori), sollevata – «per contrasto con gli artt. 3, 41, 42 e 117 della Costituzione e con il principio di ragionevolezza (art. 97 della Costituzione)» – dal Tribunale regionale di giustizia amministrativa - Sezione autonoma per la Provincia di Bolzano, con l’ordinanza indicata in epigrafe.&#13;
 	</p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 gennaio 2010.&#13;
 F.to:&#13;
 Francesco AMIRANTE, Presidente&#13;
 Paolo GROSSI, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 14 gennaio 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
