<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/147/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/147/"/>
          <FRBRalias value="ECLI:IT:COST:2008:147" name="ECLI"/>
          <FRBRdate date="07/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="147"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/147/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/147/ita@/!main"/>
          <FRBRdate date="07/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/147/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/147/ita@.xml"/>
          <FRBRdate date="07/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="16/05/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>07/05/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 301, primo comma, del codice di procedura civile, promosso dal Tribunale di Genova, nel procedimento civile vertente tra il Condominio di via Suardi n. 16 Busalla e la Mariuccia s.s. di Galvani Pietro &amp;amp; c., con ordinanza del 22 febbraio 2007 iscritta al n. 709 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 41, prima serie speciale, dell'anno 2007. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 12 marzo 2008 il Giudice relatore Francesco Amirante.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.— Nel corso di un giudizio di merito possessorio in cui, prima dell'udienza di precisazione delle conclusioni, il procuratore del ricorrente era risultato volontariamente cancellato dall'Albo degli avvocati di Genova, il Tribunale di quella città ha sollevato, con ordinanza del 22 febbraio 2007, questione di legittimità costituzionale, in riferimento all'art. 24, secondo comma, della Costituzione, dell'art. 301, primo comma, del codice di procedura civile, nella parte in cui non contempla tra le ipotesi di interruzione del processo, accanto a quelle della morte, radiazione o sospensione del procuratore, anche quella della cancellazione volontaria di quest'ultimo dall'albo professionale. 
    La questione è ritenuta rilevante poiché dalla sua soluzione il Tribunale fa dipendere  se dichiarare l'interruzione del processo per consentire alla parte rimasta priva del difensore di costituirsi con il patrocinio di nuovo procuratore legalmente esercente. 
    Secondo il remittente, la giurisprudenza consolidata della Corte di cassazione non ricollega alla cancellazione volontaria dall'albo del procuratore costituito gli stessi effetti che il primo comma dell'art. 301 cod. proc. civ. fa derivare dai fatti di morte, radiazione o sospensione, ritenendo la prima non assimilabile a dette ipotesi, tassativamente previste dalla citata norma, tutte costituite da eventi indipendenti dalla volontà del professionista o del cliente. La cancellazione è assimilata a quei casi (revoca della procura o rinunzia ad essa) riconducibili ad un comportamento volontario, cui il terzo comma dell'art. 301 citato non attribuisce efficacia interruttiva. 
    Peraltro il giudice a quo osserva che tale indirizzo è contrastato sia dalla giurisprudenza amministrativa sia da alcune sentenze di giudici di merito  e da una sentenza della Corte di cassazione, secondo cui la cancellazione dall'albo professionale da parte del procuratore costituito, anche se volontaria, determina, al pari della morte, della sospensione e della radiazione del difensore, l'interruzione del processo. Per giungere a tale risultato viene esclusa l'assimilabilità della cancellazione dall'albo alle ipotesi della revoca o della rinunzia alla procura, posto che la finalità di fatti volontari della parte o del procuratore, suscettibili di determinare una vera e propria paralisi processuale, con lesione della effettività del diritto di difesa dell'altra parte, non sarebbe ipotizzabile per la cancellazione volontaria dall'albo, essendo ben difficile che un evento che comporta la perdita dello jus postulandi, possa essere strumentalmente compiuto da un difensore. Ma, per il giudice a quo, la cancellazione volontaria dall'albo potrebbe essere seguita da una reiscrizione, sicché «i seri e fondati» argomenti della citata sentenza verrebbero a perdere «parte del loro mordente». Egli opina quindi che, in casi come quello di specie, si verifichi un vuoto nello ius postulandi della parte rimasta priva di difensore e un vulnus anche per l'altra parte, che non potrebbe effettuare le (più facili) notificazioni e comunicazioni al procuratore costituito. Di qui la prospettata lesione del parametro costituzionale che sancisce il principio di inviolabilità del diritto di difesa in ogni stato e grado del procedimento. 
    2.— È intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per l'inammissibilità, ovvero per la non fondatezza della questione. Sotto il primo profilo, il remittente non avrebbe espletato il compito di sperimentare un'interpretazione costituzionalmente orientata delle norme della cui legittimità egli dubita,  non avendo indicato le ragioni per cui l'unica interpretazione possibile sia solo quella non condivisa e ritenuta di dubbia costituzionalità, pur in presenza di un orientamento di segno opposto. 
    Nel merito, l'interveniente osserva che nel sistema vi sarebbero due gruppi di evenienze: quelle che incidono sullo status del procuratore e gli impediscono di svolgere la professione (dall'evento estremo, che è la morte, alla evenienza temporanea, che è la sospensione dall'albo) e quelle che riguardano unicamente il contratto d'opera; l'interruzione del processo nel primo caso si verifica, mentre nel secondo funziona il meccanismo di cui all'art. 85 cod. proc. civ. L'ipotesi della cancellazione volontaria dall'albo incide direttamente sullo status professionale, perché l'avvocato non può svolgere la professione; si avrebbe, dunque, uno dei casi di impedimento assoluto di cui all'endiadi della rubrica dell'art. 301, primo comma, cod. proc. civ., in quanto «morte o impedimento» sono espressioni che riguardano tutto ciò che determina insuperabile ostacolo all'esercizio della professione. Diversamente opinando, nel senso che la cancellazione volontaria dall'albo non è ipotesi di interruzione, bensì fattispecie assimilabile alla revoca della procura o alla rinuncia ad essa, la conseguenza sarebbe l'operatività dell'art. 85 cod. proc. civ., senza effetto pregiudizievole alcuno per le esigenze della difesa.Considerato in diritto1.— Il Tribunale di Genova, in composizione monocratica, nel corso di un procedimento civile nel quale era risultata la volontaria cancellazione dall'albo degli avvocati del procuratore di una delle parti, ha sollevato, in riferimento all'art. 24, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 301, primo comma, del codice di procedura civile, in quanto non include la cancellazione volontaria dall'albo del procuratore tra le cause di interruzione del processo. 
    Secondo il remittente, l'orientamento consolidato della Corte di cassazione, contraddetto da una sola pronuncia e dalla giurisprudenza amministrativa, esclude che la cancellazione dall'albo del procuratore produca l'interruzione del processo perché, ricollegandosi ad un fatto volontario, è assimilabile alla revoca della procura e alla rinuncia ad essa, ma non alla morte, alla radiazione o alla sospensione dall'albo. 
    La mancata interruzione, ad avviso del remittente, priva della difesa tecnica la parte il cui procuratore ha ottenuto la cancellazione dall'albo e il fatto che la norma censurata non preveda tale ultima evenienza quale causa d'interruzione la pone in contrasto con il precetto di cui all'art. 24 Cost., che sancisce l'inviolabilità del diritto di difesa. 
    2.— La questione non è ammissibile. 
    Il ragionamento del remittente non può essere, infatti, condiviso. 
    Egli non soltanto individua come diritto vivente un orientamento che pur riferisce essere contraddetto, nell'ambito della giurisprudenza ordinaria di legittimità  e di quella amministrativa, da un indirizzo contrario, ma erra, altresì, nella ricostruzione di quest'ultimo, facendo riferimento ad una sola pronuncia della Corte di cassazione e trascurandone altre, in particolare quella emessa dalle sezioni unite in sede di composizione di contrasto giurisprudenziale (Cass. S.U., sentenza 21 novembre 1996, n. 10284) ed ulteriori decisioni a questa successive.  
    In tal modo il giudice a quo trascura il principio, più volte affermato da questa Corte, secondo il quale una disposizione non può essere ritenuta costituzionalmente illegittima perché può essere interpretata in un senso che la ponga in contrasto con parametri costituzionali, ma soltanto se ne è impossibile una interpretazione conforme alla Costituzione (si vedano, da ultimo, la sentenza n. 379 del 2007 e le ordinanze n. 448 e n. 464 del 2007). 
    Tenuto conto della reale situazione giurisprudenziale, l'ordinanza di remissione si risolve nella irrituale richiesta di avallo di un indirizzo ermeneutico effettuata, oltretutto, in base ad una incompleta ricostruzione del quadro giurisprudenziale di riferimento.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara inammissibile la questione di legittimità costituzionale dell'art. 301, primo comma, del codice di procedura civile, sollevata, in riferimento all'art. 24, secondo comma, della Costituzione, dal Tribunale di Genova con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 maggio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Francesco AMIRANTE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 16 maggio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
