<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/369/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/369/"/>
          <FRBRalias value="ECLI:IT:COST:2003:369" name="ECLI"/>
          <FRBRdate date="10/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="369"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/369/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/369/ita@/!main"/>
          <FRBRdate date="10/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/369/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/369/ita@.xml"/>
          <FRBRdate date="10/12/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/12/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>10/12/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 13, comma 8, della legge 27 marzo 1992, n. 257 (Norme relative alla cessazione dell'impiego dell'amianto), come sostituito dall'art. 1, comma 1, del decreto-legge 5 giugno 1993, n. 169 (Disposizioni urgenti per i lavoratori del settore dell'amianto), convertito, con modificazioni, dalla legge 4 agosto 1993, n. 271, promosso con ordinanza del 4 luglio 2002 dal Tribunale di Napoli nel procedimento civile vertente tra l'INPS e De Sio Domenico ed altri, iscritta al n. 485 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 44, prima serie speciale, dell'anno 2002. 
    Visti l'atto di costituzione dell'INPS, nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 12 novembre 2003 il Giudice relatore Francesco Amirante. 
    Ritenuto che, nel corso di un giudizio nel quale quattro ex dipendenti della azienda Sofer di Pozzuoli - rispettivamente titolari, con decorrenza antecedente alla data di entrata in vigore della legge 27 marzo 1992, n. 257 (Norme relative alla cessazione dell'impiego dell'amianto), uno di pensione di vecchiaia, uno di pensione di anzianità e due di assegno di invalidità e tutti in possesso dell'attestazione di esposizione ultradecennale all'amianto rilasciata dall'INAIL - hanno chiesto il riconoscimento del beneficio della rivalutazione contributiva di cui all'art. 13, comma 8, della citata legge n. 257 del 1992, il Tribunale di Napoli, con ordinanza del 4 luglio 2002, ha sollevato, in riferimento agli artt. 3, 32 e 38 della Costituzione, questione di legittimità costituzionale dell'art. 13, comma 8, della menzionata legge n. 257 del 1992 - nel testo sostituito dall'art. 1, comma 1, del decreto-legge 5 giugno 1993, n. 169 (Disposizioni urgenti per i lavoratori del settore dell'amianto), convertito, con modificazioni, dalla legge 4 agosto 1993, n. 271 - nella parte in cui, secondo la giurisprudenza della Corte di cassazione, esclude che l'erogazione del beneficio della rivalutazione contributiva ivi prevista spetti ai lavoratori esposti all'amianto per oltre un decennio che fossero già titolari di pensione di vecchiaia o di anzianità al momento dell'entrata in vigore della citata legge n. 257 del 1992; 
    che il Tribunale remittente premette in fatto che, nel caso di specie, in base al suddetto orientamento, il beneficio di cui si discute potrebbe essere riconosciuto soltanto ai due ricorrenti titolari di assegno di invalidità mentre agli altri due dovrebbe essere negato; 
    che, quanto al merito della questione, il giudice a quo muove dalla premessa secondo cui - come, a suo avviso, può desumersi anche dalla sentenza di questa Corte n. 5 del 2000 e dall'iter legislativo relativo alla modifica apportata alla disposizione impugnata dall'art. 1, comma 1, del citato d.l. n. 169 del 1993 - la ratio principale della disposizione stessa sarebbe quella di attribuire un beneficio ai lavoratori che, loro malgrado, sono stati esposti per un lungo periodo all'azione di un materiale rivelatosi notevolmente pericoloso per la salute;  
    che, secondo la suddetta prospettazione, rispetto all'indicato fine sarebbe secondario quello di favorire il raggiungimento del diritto alla pensione ai lavoratori coinvolti nel processo di dismissione delle lavorazioni comportanti l'uso dell'amianto; 
    che il giudice remittente precisa che, viceversa, secondo il contestato orientamento della giurisprudenza della Corte di cassazione, tale ultima finalità avrebbe un ruolo primario; 
    che da tale premessa la stessa Corte di cassazione, con indirizzo univoco, ha tratto la conseguenza che il beneficio contributivo in oggetto può essere attribuito ai soggetti che al momento dell'entrata in vigore della legge n. 257 del 1992 fossero titolari di pensione o di assegno di invalidità, mentre non compete a coloro che alla suddetta data fruissero di pensione di vecchiaia o di anzianità (ovvero di inabilità ex legge 12 giugno 1984, n. 222); 
    che, ad avviso del Tribunale di Napoli, la norma impugnata così interpretata si pone, in primo luogo, in contrasto con il principio di uguaglianza di cui all'art. 3 della Costituzione, in quanto del tutto irragionevolmente riserva un trattamento deteriore - in riferimento alla tutela del bene della salute - a soggetti che si sono trovati in situazione peggiore rispetto a coloro ai quali il beneficio viene attribuito, essendo stati, i primi, sottoposti per un periodo di tempo più ampio (e, cioè, per tutta la durata della vita lavorativa) al rischio derivante dall'esposizione all'amianto; 
    che la norma stessa violerebbe, altresì, l'art. 32 della Costituzione in quanto, pur riconoscendo la pericolosità di una determinata attività produttiva, compenserebbe l'esposizione della salute al rischio di lesione soltanto in favore di alcuni soggetti e non di altri, mentre la salute è un bene che merita incondizionata tutela per ciascun individuo; 
    che il giudice a quo prospetta, inoltre, il contrasto con l'art. 38 della Costituzione sotto il profilo secondo cui la garanzia della tutela previdenziale in caso di infortunio o di invalidità - riconosciuta dal secondo comma della citata disposizione  costituzionale - imporrebbe l'attribuzione generalizzata del beneficio in argomento a tutti coloro che siano stati esposti all'amianto (alle richieste condizioni), sia che fossero in attività di servizio sia che avessero cessato l'attività lavorativa al momento dell'entrata in vigore della legge n. 257 del 1992; 
    che, nella parte conclusiva dell'ordinanza di rimessione, il Tribunale di Napoli, dopo aver precisato che le suesposte argomentazioni non possono ritenersi superate dall'art. 80, comma 25, della legge 23 dicembre 2000, n. 388 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2001), osserva che, nel giudizio de quo, tale ultima disposizione non è applicabile, non essendo stata proposta da nessuno dei ricorrenti la rinuncia all'azione giudiziaria cui essa si riferisce; 
    che, nel giudizio davanti alla Corte, si è costituito l'INPS chiedendo che la questione sia dichiarata inammissibile o infondata, sul rilievo che essa «non si discosta da quella già esaminata» e dichiarata non fondata con la sentenza n. 434 del 2002, di questa Corte, successiva all'ordinanza del Tribunale di Napoli; 
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso chiedendo la dichiarazione di inammissibilità della presente questione, in quanto identica a quella decisa con la citata sentenza n. 434 del 2002. 
    Considerato che il Tribunale di Napoli ha sollevato, in riferimento agli artt. 3, 32 e 38 della Costituzione,  questione di legittimità costituzionale dell'art. 13, comma 8, della legge 27 marzo 1992, n. 257 (come sostituito dall'art. 1, comma 1, del d.l. 5 giugno 1993, n. 169, convertito, con modificazioni, dalla legge 4 agosto 1993, n. 271), nell'interpretazione datane dalla giurisprudenza della Corte di cassazione nel senso della attribuibilità del beneficio contributivo ivi previsto ai soggetti che al momento dell'entrata in vigore della legge n. 257 del 1992 fossero titolari di pensione o di assegno di invalidità e della non riconoscibilità del beneficio stesso a coloro che alla suddetta data fruissero di pensione di vecchiaia o di anzianità;  
    che una questione identica a quella in oggetto, sollevata in riferimento agli artt. 3, primo comma, e 38, secondo comma, della Costituzione, è stata dichiarata non fondata da questa Corte con la sentenza n. 434 del 2002, successiva, come si è detto, all'odierna ordinanza di rimessione; 
    che quest'ultima non aggiunge profili ulteriori di doglianza rispetto a quelli già esaminati con la menzionata decisione, in quanto anche le censure riferite all'art. 32 della Costituzione sono state in tale sentenza implicitamente respinte, dovendosi esse considerare una derivazione dell'attribuzione alla disposizione impugnata della ratio principale di offrire un beneficio di carattere risarcitorio-indennitario ai lavoratori che, loro malgrado, sono stati esposti per un lungo periodo all'azione di un materiale rivelatosi notevolmente pericoloso per la salute, anziché di quella - ritenuta, da questa Corte, prevalente, in conformità con quanto affermato dalla Corte di cassazione, con orientamento consolidato assurto al rango di «diritto vivente» - di favorire il raggiungimento del diritto alla pensione ai lavoratori coinvolti nel processo di dismissione delle lavorazioni comportanti l'uso dell'amianto; 
    che, pertanto, la presente questione deve ritenersi manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 13, comma 8, della legge 27 marzo 1992, n. 257 (Norme relative alla cessazione dell'impiego dell'amianto), come sostituito dall'art. 1, comma 1, del decreto-legge 5 giugno 1993, n. 169 (Disposizioni urgenti per i lavoratori del settore dell'amianto), convertito, con modificazioni, dalla legge 4 agosto 1993, n. 271, sollevata, in riferimento agli artt. 3, 32 e 38 della Costituzione, dal Tribunale di Napoli con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 dicembre 2003. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Francesco AMIRANTE, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 19 dicembre 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
