<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/348/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/348/"/>
          <FRBRalias value="ECLI:IT:COST:2008:348" name="ECLI"/>
          <FRBRdate date="20/10/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="348"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/348/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/348/ita@/!main"/>
          <FRBRdate date="20/10/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/348/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/348/ita@.xml"/>
          <FRBRdate date="20/10/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/10/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>FLICK</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Tesauro</cc:relatore_pronuncia>
        <cc:data_decisione>20/10/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale degli artt. 1, 2 e 10 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), promossi con ordinanze del 14 novembre, del 18 aprile, del 9, del 10 (n. 3 ordinanze) e del 26 ottobre, dell'8, del 13, del 14 (n. 3 ordinanze) e del 17 novembre (n. 2 ordinanze), del 13 marzo, del 21 dicembre e del 2 novembre 2006 dalla Corte d'appello di Cagliari, sezione distaccata di Sassari, rispettivamente iscritte ai nn. 288, 464, da 471 a 482, 646, 731, e 732 del registro ordinanze 2007 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 17, 25, 37 e 43, prima serie speciale, dell'anno 2007. 
    Udito nella camera di consiglio del 24 settembre 2008 il Giudice relatore Giuseppe Tesauro.  
    Ritenuto che, con diciassette ordinanze sostanzialmente coincidenti nella parte motiva, la Corte d'appello di Cagliari, sezione distaccata di Sassari, ha sollevato, in riferimento agli artt. 3, 27, terzo comma, 111 e 112 della Costituzione, questione di legittimità costituzionale degli artt. 1 e 2 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), nella parte in cui non consentono al pubblico ministero di proporre appello avverso le sentenze di proscioglimento, nonché dell'art. 10 della medesima legge; 
    che, ai fini della rilevanza, la Corte d'appello rimettente premette che gli appelli di cui essa è investita, proposti dal pubblico ministero anteriormente alla data di entrata in vigore della legge n. 46 del 2006 contro sentenze di proscioglimento pronunciate dal Tribunale di Sassari, dal Tribunale di Tempio Pausania e dal Tribunale di Nuoro, dovrebbero essere dichiarati inammissibili ai sensi dell'art. 10, comma 2, della medesima legge; 
    che, a suo avviso, gli artt. 1 e 2 della legge n. 46 del 2006, i quali hanno rispettivamente sostituito l'art. 593 del codice di procedura penale e modificato l'art. 443, comma 1, dello stesso codice, eliminando l'appello avverso le sentenze di proscioglimento pronunciate all'esito del dibattimento o emesse a seguito di giudizio abbreviato, e l'art. 10 della citata legge, che prevede l'immediata applicabilità di tale regime ai procedimenti in corso alla data di entrata in vigore della legge, violerebbero, in primo luogo, il principio di uguaglianza (art. 3 Cost.) e il principio di parità delle parti nel processo (art. 111, secondo comma, Cost.); 
    che, infatti, la nuova disciplina dell'appello, privando il pubblico ministero della possibilità di impugnare le sentenze di proscioglimento «con lo stesso mezzo riconosciuto all'imputato avverso le sentenze di condanna», avrebbe determinato una ingiustificata disparità di trattamento in danno della pubblica accusa ed avrebbe alterato l'equilibrio dei poteri processuali delle parti; 
    che in senso contrario non potrebbe valere quanto in precedenza affermato dalla Corte costituzionale con riferimento alle limitazioni stabilite dall'art. 443, comma 3, cod. proc. pen. per il potere di appello del pubblico ministero avverso le sentenze di condanna, secondo cui la disparità di trattamento in danno della pubblica accusa trovava ragionevole giustificazione «alla luce del risultato perseguito con il ricorso al rito abbreviato e delle peculiarità di questo»; 
    che, diversamente, per le «sentenze di assoluzione, pur pronunciate a seguito di rito abbreviato», la preclusione all'appello non potrebbe dirsi ragionevole, «stante il perdurante interesse della parte pubblica all'accertamento della verità», dimostrato anche dalla conservazione al pubblico ministero del gravame contro le sentenze di condanna che modifichino il titolo del reato; 
    che il contrasto tra la censurata disciplina e gli artt. 3 e 111 Cost. appare alla Corte d'appello rimettente ancora più evidente ove si consideri che alla parte civile è stato invece conservato, anche dopo le modifiche recate dalla legge n. 46 del 2006 all'art. 576 cod. proc. pen., il potere di proporre appello contro le sentenze di assoluzione; 
    che sarebbero altresì violati gli artt. 27, terzo comma, e 112 Cost., in quanto la eliminazione del potere di appello del pubblico ministero avverso le sentenze di proscioglimento inciderebbe, rendendola «più difficoltosa», sulla «attuazione della ricerca della verità e, quindi dell'istanza di giustizia propria della collettività», di cui il principio della obbligatorietà dell'azione penale e il principio secondo cui la pena deve tendere alla rieducazione del condannato costituiscono «espressione». 
     Considerato che le questioni di costituzionalità sollevate dalla Corte d'appello di Cagliari, sezione distaccata di Sassari, hanno tutte ad oggetto la preclusione – conseguente alla sostituzione dell'art. 593 cod. proc. pen. ed alla modifica dell'art. 443, comma 1, cod. proc. pen. ad opera, rispettivamente, degli artt. 1 e 2 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento) – dell'appello contro le sentenze di proscioglimento emesse all'esito del dibattimento o pronunciate a seguito di giudizio abbreviato, e l'immediata applicabilità di tale regime, in forza dell'art. 10 della medesima legge, ai procedimenti in corso alla data di entrata in vigore della legge;  
    che, stante l'identità delle questioni, i relativi giudizi vanno riuniti per essere decisi con unica pronuncia; 
    che, successivamente alle ordinanze di rimessione, questa Corte, con sentenza n. 26 del 2007, ha dichiarato l'illegittimità costituzionale dell'art. 1 della legge n. 46 del 2006, nella parte in cui, sostituendo l'art. 593 del codice di procedura penale, esclude che il pubblico ministero possa appellare contro le sentenze di proscioglimento, fatta eccezione per le ipotesi previste dall'art. 603, comma 2, del medesimo codice, se la nuova prova è decisiva, e dell'art. 10, comma 2, della medesima legge, nella parte in cui prevede che l'appello proposto contro una sentenza di proscioglimento dal pubblico ministero prima della data di entrata in vigore della medesima legge è dichiarato inammissibile; 
    che, inoltre, con sentenza n. 320 del 2007, questa Corte ha dichiarato l'illegittimità costituzionale dell'art. 2 della legge n. 46 del 2006, nella parte in cui, modificando l'art. 443, comma 1, del codice di procedura penale, esclude che il pubblico ministero possa appellare contro le sentenze di proscioglimento emesse a seguito di giudizio abbreviato, e dell'art. 10, comma 2, nella parte in cui prevede che sia dichiarato inammissibile l'appello proposto dal pubblico ministero contro una sentenza di proscioglimento emessa a seguito di giudizio abbreviato, prima dell'entrata in vigore della medesima legge; 
    che, alla stregua delle richiamate pronunce, gli atti devono essere restituiti ai giudici rimettenti per un nuovo esame della rilevanza delle questioni.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti alla Corte d'appello di Cagliari, sezione distaccata di Sassari. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 ottobre 2008.  &#13;
F.to:  &#13;
Giovanni Maria FLICK, Presidente  &#13;
Giuseppe TESAURO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 23 ottobre 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
