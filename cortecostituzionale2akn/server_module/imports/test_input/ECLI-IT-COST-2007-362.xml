<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/362/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/362/"/>
          <FRBRalias value="ECLI:IT:COST:2007:362" name="ECLI"/>
          <FRBRdate date="24/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="362"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/362/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/362/ita@/!main"/>
          <FRBRdate date="24/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/362/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/362/ita@.xml"/>
          <FRBRdate date="24/10/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="31/10/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Franco Gallo</cc:relatore_pronuncia>
        <cc:data_decisione>24/10/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 202 e 203, primo e secondo comma, del regio decreto 16 marzo 1942, n. 267 (Disciplina del fallimento, del concordato preventivo, dell'amministrazione controllata e della liquidazione coatta amministrativa), promosso con ordinanza del 15 novembre 2004 dal Tribunale ordinario di Roma nel giudizio civile vertente tra il Consorzio agrario interprovinciale di Roma e Frosinone (CAIRF) in liquidazione coatta amministrativa e la s.p.a. Banca Nazionale del Lavoro, iscritta al n. 423 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 37, prima serie speciale, dell'anno 2005. 
    Visti l'atto di costituzione del CAIRF in liquidazione coatta amministrativa e quello, fuori termine, della s.p.a. Banca Nazionale del Lavoro, nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nell'udienza pubblica del 9 ottobre 2007 il Giudice relatore Franco Gallo; 
    uditi l'avvocato Achille Chiappetti per il CAIRF in liquidazione coatta amministrativa e l'avvocato dello Stato Paolo Cosentino per il Presidente del Consiglio dei ministri. 
    Ritenuto che, nel corso di un giudizio civile, il Tribunale ordinario di Roma, con ordinanza del 15 novembre 2004, ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale degli artt. 202 e 203, commi primo e secondo, del regio decreto 16 marzo 1942, n. 267 (Disciplina del fallimento, del concordato preventivo, dell'amministrazione controllata e della liquidazione coatta amministrativa), nella parte in cui individuano nell'accertamento giudiziario dello stato d'insolvenza dell'impresa, anziché nel decreto che dispone la liquidazione coatta amministrativa, il presupposto giuridico necessario per l'esperibilità delle azioni revocatorie fallimentari nell'àmbito di detta procedura e, pertanto, stabiliscono che la prescrizione di dette azioni decorre dal suddetto accertamento giudiziario e non dalla data di emissione del decreto di apertura della procedura medesima; 
    che, secondo quanto premesso in punto di fatto dal giudice rimettente: a) in data 24 gennaio 1992 era stata disposta la liquidazione coatta amministrativa del Consorzio agrario interprovinciale di Roma e Frosinone (CAIRF); b) con sentenza del 1° ottobre 1998, il Tribunale ordinario di Roma aveva dichiarato, ai sensi dell'art. 202 del regio decreto n. 267 del 1942 (legge fallimentare), lo stato di insolvenza del medesimo Consorzio; c) il commissario liquidatore, con atto di citazione notificato il 25 marzo 2003, aveva convenuto in giudizio la s.p.a. Banca Nazionale del Lavoro per ottenere la revoca, ai sensi dell'art. 67, secondo comma, della legge fallimentare, dei pagamenti effettuati dal consorzio debitore, a favore della convenuta, nell'anno anteriore al decreto di apertura della procedura concorsuale; c) la banca aveva eccepito la prescrizione dell'azione, per essere decorsi più di cinque anni dalla data di emissione del decreto di apertura della procedura di liquidazione coatta amministrativa; 
    che il giudice a quo premette poi, in punto di diritto, che gli artt. 197, 200, 202 e 203, commi primo e secondo, della legge fallimentare, nell'àmbito della liquidazione coatta amministrativa, consentono l'esercizio delle azioni revocatorie fallimentari solo dopo l'accertamento giudiziario dello stato d'insolvenza e che pertanto, in base alla chiara lettera di tali disposizioni e come confermato dal prevalente orientamento della giurisprudenza di merito, soltanto da tale accertamento decorre il termine di prescrizione delle suddette azioni revocatorie;  
    che, quanto alla non manifesta infondatezza della sollevata questione, il giudice a quo afferma che la suddetta disciplina della prescrizione dell'azione revocatoria fallimentare esercitata nel corso della procedura di liquidazione coatta amministrativa víola l'art. 3 Cost., perché è irragionevolmente diversa da quella dettata per il fallimento ed è inoltre intrinsecamente irragionevole, nella parte in cui non prevede che l'accertamento dello stato di insolvenza dell'impresa, contenuto nel decreto amministrativo di apertura della procedura di liquidazione coatta amministrativa, sia sufficiente a legittimare il commissario liquidatore ad esercitare l'azione revocatoria ed a far decorrere, di conseguenza, il termine di prescrizione di tale azione; 
    che, in particolare, quanto alla denunciata disparità di trattamento rispetto alla disciplina del fallimento, il giudice a quo muove dal rilievo che l'art. 202 della legge fallimentare consente – con effetti da lui ritenuti «confliggenti con un elementare principio di certezza del diritto e delle situazioni giuridiche» – che lo stato di insolvenza di un'impresa sottoposta a liquidazione coatta amministrativa possa essere giudizialmente accertato anche dopo l'apertura della procedura, con riferimento al tempo in cui è stata ordinata la liquidazione, con la conseguenza che, in tale ipotesi, alcuni effetti scaturiscono direttamente dal provvedimento amministrativo che dà inizio alla procedura, mentre altri, tra cui l'esperibilità delle azioni revocatorie e la decorrenza della prescrizione di tali azioni, hanno la loro fonte nel provvedimento giurisdizionale dichiarativo dello stato d'insolvenza; 
    che detta disciplina, secondo il rimettente, comporterebbe una ingiustificata diversità di trattamento rispetto al fallimento: in questo, «la individuazione degli atti pregiudizievoli ai creditori», assoggettabili a revocatoria, «decorre a ritroso dall'accertamento dello stato di insolvenza (cosiddetto periodo sospetto) e cioè dalla sentenza dichiarativa di fallimento», con la quale si apre la procedura fallimentare; nella liquidazione coatta amministrativa, invece, «da un lato si fa riferimento – per la individuazione degli atti revocabili – alla data del decreto amministrativo, dall'altro – per l'esercizio dell'azione – alla data della sentenza dichiarativa dell'insolvenza», che può intervenire anche molto tempo dopo l'apertura della procedura di liquidazione coatta; 
    che, quanto alla intrinseca irragionevolezza della norma denunciata, il giudice a quo afferma che lo stato di insolvenza dell'impresa, cioè «il presupposto oggettivo che costituisce il fondamento dell'azione revocatoria invero risulta essere già stato accertato con l'emissione del decreto amministrativo cosí da imporre che da quel momento il commissario liquidatore sia legittimato all'azione revocatoria», come risulterebbe in modo evidente dal testo dell'art. 2545-terdecies del codice civile, introdotto con la riforma della normativa sulle società cooperative, il quale ha sostituito, quale presupposto oggettivo della liquidazione coatta amministrativa di dette società, l'«insolvenza della società» alla «insufficienza delle attività per il pagamento dei debiti», prevista dal previgente art. 2540 cod. civ., tanto da rendere priva di senso una sentenza di accertamento di uno stato di insolvenza dell'impresa già accertato dall'autorità governativa; 
    che, in ordine alla rilevanza della questione, il giudice a quo afferma che l'applicazione delle norme denunciate, «in relazione alla eccezione di prescrizione sollevata dalla parte convenuta, è determinante ai fini della decisione»;  
    che si è costituito il Consorzio agrario interprovinciale di Roma e Frosinone (CAIRF) in liquidazione coatta amministrativa, in persona del commissario liquidatore, parte attrice nel giudizio a quo, il quale ha concluso per l'infondatezza della questione; 
    che, secondo detto Consorzio, le situazioni messe a raffronto dal giudice rimettente (cioè la sentenza di fallimento ed il decreto con cui viene disposta l'apertura della liquidazione coatta amministrativa) sono eterogenee e, perciò, non comparabili tra loro; 
    che la stessa parte rileva, altresí, che la Corte costituzionale, in una questione analoga, ha già affermato, con la sentenza n. 301 del 2005, che: a) non è irragionevole la scelta del legislatore di consentire, durante la pendenza della procedura di liquidazione coatta amministrativa, l'emissione – senza limiti di tempo – di una sentenza dichiarativa dello stato di insolvenza; b) non è arbitrario sostenere che, in base al principio di cui all'art. 2935 cod. civ., il termine prescrizionale delle azioni revocatorie fallimentari decorre dalla data della sentenza dichiarativa dello stato di insolvenza e non da quella del precedente decreto di messa in liquidazione coatta amministrativa dell'impresa; c) la decorrenza della prescrizione delle azioni revocatorie fallimentari dalla sentenza dichiarativa dello stato di insolvenza non comporta la lesione del generale interesse alla certezza delle situazioni giuridiche; 
    che la s.p.a. Banca Nazionale del Lavoro, parte convenuta nel giudizio a quo, ha depositato, nel presente giudizio di legittimità costituzionale, una memoria fuori termine; 
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata inammissibile, per insufficiente motivazione sulla rilevanza, o, comunque, infondata, perché l'accertamento giudiziario dello stato d'insolvenza – in quanto si svolge con le garanzie del contraddittorio e del diritto di difesa (art. 24 Cost.) ed è suscettibile di passare in giudicato – costituisce «l'unico accertamento che può fungere da presupposto per l'esercizio dell'azione revocatoria fallimentare, tanto nel fallimento, quanto nella liquidazione coatta amministrativa», con conseguente decorrenza del termine di prescrizione delle azioni revocatorie dalla data della sentenza dichiarativa dello stato d'insolvenza, in forza del principio generale di cui all'art. 2935 cod. civ., secondo cui la prescrizione comincia a decorrere solo dal giorno in cui il diritto può essere fatto valere. 
    Considerato che il Tribunale ordinario di Roma dubita, in riferimento all'art. 3 della Costituzione, della legittimità degli artt. 202 e 203, commi primo e secondo, del regio decreto 16 marzo 1942, n. 267 (Disciplina del fallimento, del concordato preventivo, dell'amministrazione controllata e della liquidazione coatta amministrativa), nella parte in cui individuano nell'accertamento giudiziario dello stato d'insolvenza dell'impresa anziché nel decreto che dispone la liquidazione coatta amministrativa, il presupposto giuridico necessario per l'esperibilità delle azioni revocatorie fallimentari nell'àmbito di detta procedura e, pertanto, stabiliscono che la prescrizione di tali azioni decorre dal suddetto accertamento giudiziario e non dalla data di emissione del decreto di apertura della procedura medesima; 
    che, ad avviso del giudice rimettente, le disposizioni censurate violano l'art. 3 Cost., perché, stabilendo che il termine di prescrizione delle azioni revocatorie decorre dalla data della sentenza di accertamento dello stato di insolvenza dell'impresa già posta in liquidazione coatta amministrativa, pongono una disciplina: a) ingiustificatamente diversa da quella dettata per il fallimento, nel quale detto termine decorre dalla data di apertura della procedura concorsuale (cioè dalla data della sentenza dichiarativa del fallimento); b) intrinsecamente irragionevole, in quanto esige un accertamento giudiziario dello stato di insolvenza dell'impresa, nonostante che detto accertamento sia già contenuto nel decreto amministrativo di apertura della procedura di liquidazione coatta amministrativa; 
    che la questione è manifestamente infondata; 
    che il rimettente formula le sue censure muovendo da due diverse premesse interpretative; 
    che la prima premessa – secondo cui la prescrizione dell'azione revocatoria decorre dalla sentenza di accertamento dello stato di insolvenza emessa nel corso della procedura di liquidazione coatta amministrativa – è pacificamente condivisa dalla giurisprudenza della Corte di cassazione (ex plurimis, sentenza. n. 14279 del 2006) ed è già stata considerata plausibile e non arbitraria da questa Corte (sentenza n. 301 del 2005); 
    che, peraltro, a tale premessa il giudice a quo fa seguire, nel suo percorso argomentativo, una seconda premessa manifestamente erronea, affermando la sostanziale corrispondenza tra la sentenza dichiarativa del fallimento o dello stato di insolvenza, da un lato, ed il decreto di messa in liquidazione coatta amministrativa, dall'altro, e da ciò deducendo l'illegittimità costituzionale della normativa denunciata, nella parte in cui non consente che le azioni revocatorie siano esperibili sin dal momento dell'apertura della procedura di liquidazione coatta amministrativa e non stabilisce che la prescrizione di tali azioni decorra da tale momento; 
    che, contrariamente a quanto affermato dal rimettente, l'accertamento giurisdizionale dello stato di insolvenza non è assimilabile alla valutazione delle condizioni economiche dell'impresa effettuata dall'autorità governativa di vigilanza; 
    che, infatti, il decreto di liquidazione coatta amministrativa è emesso all'esito di un procedimento amministrativo il quale, a differenza dell'accertamento giudiziale dello stato di insolvenza, non offre le garanzie del rispetto del contraddittorio e del diritto di difesa, né produce gli effetti del giudicato; 
    che il rimettente, operando tale assimilazione, pone pertanto a raffronto situazioni eterogenee; 
    che devono essere invece assimilate la sentenza dichiarativa di fallimento e la sentenza dichiarativa dello stato di insolvenza dell'impresa in liquidazione coatta amministrativa, in ragione della loro comune natura giurisdizionale; 
    che da tali rilievi consegue, in primo luogo, l'insussistenza della denunciata disparità di trattamento, perché, tanto nel fallimento quanto nella liquidazione coatta amministrativa apertasi senza previo accertamento giudiziario dello stato di insolvenza, le azioni revocatorie sono esperibili solo dopo la dichiarazione giudiziale di detto stato ed è pertanto coerente con tale regime che esse siano soggette a un termine di prescrizione decorrente dall'accertamento dell'insolvenza; 
    che dagli stessi rilievi consegue, in secondo luogo, l'insussistenza della denunciata intrinseca irragionevolezza della normativa censurata: detto accertamento giudiziario non costituisce, infatti, una inutile duplicazione della valutazione dell'autorità governativa di vigilanza effettuata in sede di emissione del decreto di messa in liquidazione coatta amministrativa, in quanto non è irragionevole che tale valutazione compiuta dall'autorità governativa abbia una valenza minore, per funzione ed effetti, rispetto al successivo accertamento giudiziario dello stato di insolvenza.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 202 e 203, commi primo e secondo, del regio decreto 16 marzo 1942, n. 267 (Disciplina del fallimento, del concordato preventivo, dell'amministrazione controllata e della liquidazione coatta amministrativa), sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale ordinario di Roma con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 ottobre 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Franco GALLO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 31 ottobre 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
