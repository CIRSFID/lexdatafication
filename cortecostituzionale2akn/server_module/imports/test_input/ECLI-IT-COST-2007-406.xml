<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/406/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/406/"/>
          <FRBRalias value="ECLI:IT:COST:2007:406" name="ECLI"/>
          <FRBRdate date="21/11/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="406"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/406/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/406/ita@/!main"/>
          <FRBRdate date="21/11/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/406/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/406/ita@.xml"/>
          <FRBRdate date="21/11/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="30/11/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Sabino Cassese</cc:relatore_pronuncia>
        <cc:data_decisione>21/11/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 8 e 10, comma 1 e 2, del decreto del Presidente della Repubblica 24 novembre 1971, n. 1199 (Semplificazione dei procedimenti in materia di ricorsi amministrativi) promosso con ordinanza del 24 gennaio 2007 dal Tribunale amministrativo regionale della Puglia sul ricorso proposto da Benedetto Elia contro l'A.u.s.l. Bari/4 ed altri, iscritta al n. 411 del registro ordinanze del 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 22, prima serie speciale, dell'anno 2007. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 24 ottobre 2007 il Giudice relatore Sabino Cassese.  
    Ritenuto che il Tribunale amministrativo regionale della Puglia – sede di Bari – ha sollevato: a) in riferimento agli artt. 3 e 97 della Costituzione, questione di legittimità costituzionale dell'art. 8 del decreto del Presidente della Repubblica 24 novembre 1971, n. 1199 (Semplificazione dei procedimenti in materia di ricorsi amministrativi), in via principale; b) in riferimento all'art. 25 della Costituzione, questione di legittimità costituzionale dell'art. 10, primo e secondo comma, dello stesso decreto, in via subordinata; 
    che l'art. 8 individua gli atti avverso i quali è esperibile il ricorso straordinario al Presidente della Repubblica e pone il principio di alternatività tra ricorso straordinario e azione giudiziaria dinanzi al giudice amministrativo, mentre l'art. 10 disciplina la trasposizione del ricorso straordinario nella sede giurisdizionale amministrativa (primo comma) e individua i casi di rimessione degli atti alla sede giustiziale (secondo comma); 
    che il giudice rimettente conosce, in sede di trasposizione, della controversia instaurata da colui che, avendo precedentemente proposto ricorso straordinario al Capo dello Stato, si è costituito in seguito all'opposizione presentata da controparte; 
    che, con riferimento alla rilevanza della questione di costituzionalità, il Tar ritiene che la controversia pendente, avendo ad oggetto l'inquadramento di un dipendente AUSL, appartiene alla giurisdizione ordinaria, ai sensi dell'art. 63 del decreto legislativo 30 marzo 2001, n. 165 (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche), e che il radicamento della giurisdizione del giudice amministrativo per effetto della opzione per il ricorso straordinario, sostenuto dall'amministrazione, aprirebbe la via ai dubbi di costituzionalità; 
    che, in conclusione, sostiene il rimettente, la «sicura insussistenza della giurisdizione di questo Tribunale, contrariamente a quanto si potrebbe ritenere, non vale ad escludere la rilevanza [….] ma anzi ne costituisce essa stessa il fondamento». Infatti, sulla base della legge vigente (art. 10, secondo comma, d.P.R. n. 199 del 1971), ritenuta la carenza di giurisdizione a favore del giudice ordinario, il giudice amministrativo non può che rimettere gli atti alla sede amministrativa contenziosa, con conseguente impossibilità che la controversia venga conosciuta dal giudice che avrebbe la giurisdizione. Da ciò «i dubbi [di costituzionalità che] si riverberano altresì sulla stessa proponibilità del rimedio del ricorso straordinario per controversie del tipo di quella in oggetto»; 
    che, quanto alla non manifesta infondatezza, il Tar ritiene che l'art. 8 del d.P.R. n. 1199 del 1971, nella parte in cui consente – secondo l'interpretazione consolidata del Consiglio di Stato (adunanza generale n. 9 del 1999) – il ricorso straordinario al Presidente della Repubblica anche contro gli atti emessi dall'amministrazione nell'ambito di un rapporto di lavoro di tipo privatistico, assoggettati alla disciplina del diritto comune e attribuiti alla cognizione del giudice ordinario, ai quali sia estraneo l'esercizio di pubblici poteri, violi gli artt. 3 e 97 Cost., sul presupposto di un fondamento costituzionale del ricorso straordinario, rinvenuto nei principi generali di buon andamento e imparzialità, in quanto il ricorso costituirebbe, per l'amministrazione, un mezzo ulteriore di garanzia della legalità e dell'imparzialità della propria azione e, per i cittadini, uno strumento aggiuntivo di tutela dei propri diritti e interessi, la cui protezione è speculare ai principi ricordati (sentenza n. 298 del 1986); da ciò la necessità di limitare il rimedio del procedimento amministrativo contenzioso agli atti adottati nell'esercizio di poteri autoritativi, stante lo stretto legame con il controllo istituzionale sul corretto esercizio dei pubblici poteri; 
    che, in via subordinata, il giudice rimettente prospetta, in riferimento all'art. 25 della Costituzione, questione di costituzionalità dell'art. 10, primo e secondo comma, dello stesso decreto; del primo comma, nella parte in cui non prevede – per le controversie relative ad atti amministrativi nell'ambito di rapporti di tipo privatistico, assoggettati al diritto comune e attribuiti alla cognizione del giudice ordinario, ai quali sia estraneo l'esercizio di pubblici poteri – che, a seguito dell'opposizione, il ricorrente che intenda insistere nel ricorso debba depositare l'atto di costituzione in giudizio nella cancelleria del giudice ordinario competente, anziché in quella del giudice amministrativo, e che il giudizio prosegua in sede giurisdizionale secondo le norme del codice di procedura civile anziché del testo unico delle leggi del Consiglio di Stato; del secondo comma, nella parte in cui prevede che il giudice amministrativo dispone la rimessione degli atti al ministero competente anche qualora riconosca che il ricorso è inammissibile per difetto di giurisdizione; 
    che, secondo il rimettente, la prevista costituzione dinanzi al giudice amministrativo di colui che ha proposto ricorso straordinario, qualora, in seguito all'opposizione dei controinteressati, intenda insistere nel ricorso, sottrarrebbe irreversibilmente alla giurisdizione del giudice ordinario controversie pacificamente rientranti nella giurisdizione di detto giudice, per effetto della regressione del procedimento alla fase amministrativa contenziosa; 
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile per difetto di rilevanza, avendo il giudice rimettente ritenuto il proprio difetto di giurisdizione, o, in subordine, manifestamente infondata. 
    Considerato che il Tribunale amministrativo regionale della Puglia – sede di Bari – ha sollevato questione di legittimità costituzionale dell'art. 8 del decreto del Presidente della Repubblica 24 novembre 1971, n. 1199 (Semplificazione dei procedimenti in materia di ricorsi amministrativi), in riferimento agli artt. 3 e 97 della Costituzione, in via principale e, subordinatamente, questione di legittimità costituzionale dell'art. 10, primo e secondo comma, dello stesso decreto, in riferimento all'art. 25 della Costituzione; 
    che, rispetto ad entrambe le questioni, il giudice rimettente afferma la sicura insussistenza della propria giurisdizione, sostenendo che tale circostanza «contrariamente a quanto si potrebbe ritenere, non vale ad escludere la rilevanza [….] ma anzi ne costituisce essa stessa il fondamento», argomentando poi in ordine ai dubbi di costituzionalità; 
    che tale assunto si fonda sul presupposto che la «regressione» del procedimento alla fase amministrativa contenziosa sottrarrebbe definitivamente la controversia alla cognizione del giudice ordinario; 
    che siffatto convincimento non è sorretto da alcuna idonea argomentazione; 
    che, pertanto, la motivazione sulla rilevanza delle questioni sollevate è insufficiente, con conseguente manifesta inammissibilità. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 8 del decreto del Presidente della Repubblica 24 novembre 1971, n. 1199 (Semplificazione dei procedimenti in materia di ricorsi amministrativi), e dell'art. 10, primo e secondo comma, dello stesso decreto, sollevate in riferimento agli artt. 3 e 97 Cost. la prima, all'art. 25, Cost. la seconda, dal Tribunale amministrativo regionale della Puglia – sede di Bari – con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 21 novembre 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Sabino CASSESE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 30 novembre 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
