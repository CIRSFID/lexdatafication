<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/136/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/136/"/>
          <FRBRalias value="ECLI:IT:COST:2016:136" name="ECLI"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="136"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/136/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/136/ita@/!main"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/136/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/136/ita@.xml"/>
          <FRBRdate date="18/05/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/06/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Nicolò Zanon</cc:relatore_pronuncia>
        <cc:data_decisione>18/05/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 131, comma 4, lettera c), del decreto del Presidente della Repubblica 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia - Testo A), promosso dal Tribunale ordinario della Spezia nel procedimento civile promosso da D.P.A. nei confronti di B.L. con ordinanza dell'11 agosto 2015, iscritta al n. 263 del registro ordinanze 2015 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2015. 
 Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
 udito nella camera di consiglio del 18 maggio il Giudice relatore Nicolò Zanon.
 Ritenuto che, con l'ordinanza menzionata in epigrafe, il Tribunale ordinario della Spezia ha sollevato, in riferimento agli artt. 3, 24, 101 e 111 della Costituzione, questioni di legittimità costituzionale dell'art. 131, comma 4, lettera c), del decreto del Presidente della Repubblica 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia - Testo A); 
 che, in base alla disposizione censurata, sono anticipate dall'erario «le spese sostenute per l'adempimento dell'incarico» da parte dei consulenti tecnici di parte e ausiliari del magistrato; 
 che le questioni di legittimità costituzionale sono state sollevate nell'ambito del giudizio civile instaurato per l'accertamento della «sussistenza del difetto di veridicità dell'atto di riconoscimento effettuato dal Sig. D.P. nell'atto di nascita della bambina M.D.P.» e, quindi, per l'annullamento dell'atto «di riconoscimento medesimo con conseguente perdita del cognome da parte della convenuta e conseguente venir meno in capo al Sig. D.P. dei diritti e dei doveri ex art. 261 c.c.»; 
 che parte attrice, nel corso del giudizio principale, ha prodotto copia del provvedimento di ammissione al patrocinio a spese dello Stato e, contestualmente, il consulente tecnico d'ufficio ha rappresentato di non essere in grado di anticipare le spese necessarie per procedere all'esame dei campioni ematici delle parti, indicate in «&amp;#8364; 2.000,00 + IVA»;
 che il giudice a quo, in punto di rilevanza delle questioni, ha osservato che, avendo l'attore impugnato l'atto di riconoscimento della paternità per difetto di veridicità, la consulenza tecnica d'ufficio diretta a verificare la compatibilità del DNA paterno con quello della figlia riconosciuta costituirebbe l'unica attività istruttoria utile per tale verifica;
 che, tuttavia, l'anticipazione delle indicate spese di consulenza non potrebbe essere posta a carico né dello Stato, prevedendo la disposizione denunciata «il rimborso delle spese già "sostenute"», né dell'attore, essendo egli stato ammesso al patrocinio a spese dello Stato;
 che, secondo il giudice rimettente, non sarebbe possibile imporre l'anticipazione delle stesse a carico della parte convenuta, la quale non avrebbe interesse allo svolgimento delle indagini peritali, il cui esito, in astratto, potrebbe rivelarsi ad essa sfavorevole;
 che il consulente tecnico d'ufficio, solo dopo l'avvenuto espletamento dell'incarico, potrebbe ottenere un decreto di liquidazione del compenso e delle spese e, dunque, fino a tale momento non sarebbe munito di un titolo esecutivo in forza del quale esigere coattivamente da parte convenuta - che non lo voglia spontaneamente anticipare - l'importo corrispondente alle spese che devono essere sostenute proprio per svolgere le indagini;
 che, pertanto, il consulente tecnico d'ufficio non avrebbe altra scelta che anticipare le spese attingendo il danaro necessario dal proprio patrimonio;
 che, ove l'ausiliario affermi di non avere la disponibilità economica per effettuare detta anticipazione - come accaduto, nel caso di specie, nel corso dell'udienza del 16 aprile 2015 - risulterebbe impossibile conferirgli l'incarico, «non avendo il giudice il potere di obbligarlo a stipulare un contratto di mutuo perché si assicuri la liquidità necessaria»; né, in tale ipotesi, risulterebbe possibile nominare, come proposto da parte attrice, «un altro C.T.U., scegliendolo tra personale in servizio presso strutture universitarie pubbliche, sul presupposto (erroneo) che potrebbe utilizzare il laboratorio e le strutture dell'università senza sostenere spese vive»;
 che, alla luce delle considerazioni svolte, il giudice rimettente ritiene che il giudizio principale non possa essere definito indipendentemente dalla risoluzione delle questioni di legittimità costituzionale della norma censurata, la quale consente il rimborso al consulente tecnico d'ufficio delle sole spese da quest'ultimo già sostenute ovvero già anticipate e non anche di quelle che devono ancora essere sostenute;
 che il giudice a quo, in punto di non manifesta infondatezza, ha osservato, innanzitutto, che un'interpretazione adeguatrice secundum constitutionem non sarebbe possibile, alla luce del chiaro tenore letterale della disposizione in esame (è richiamata l'ordinanza della Corte costituzionale n. 57 del 2006);
 che l'art. 131, comma 4, lettera c), del d.P.R. n. 115 del 2002 contrasterebbe «con i principi contenuti negli artt. 3 e 24 Cost., anche in combinato disposto tra loro», in quanto l'esercizio del diritto di difesa per i soggetti meno abbienti risulterebbe precluso, limitato ovvero condizionato, dipendendo la possibilità di istruire il procedimento dalla capacità economica del consulente tecnico d'ufficio;
 che, inoltre, la disposizione censurata - non prevedendo che lo Stato anticipi quantomeno le spese vive ancora da sostenersi - comporterebbe che le modalità di espletamento delle indagini peritali possano essere influenzate da una valutazione di economicità da parte del consulente, di fatto indotto o costretto ad anticipare personalmente le spese nella minor misura possibile;
 che la norma de qua contrasterebbe anche con i principi contenuti nell'art. 111 Cost., perché il consulente tecnico d'ufficio, nel caso in cui non sia in grado di anticipare le spese, sarebbe costretto a richiedere prestiti o a risparmiare nel corso del tempo le somme necessarie per l'espletamento dell'incarico, con conseguente allungamento dei tempi del processo;
 che la stessa norma si porrebbe in contrasto pure con gli artt. 101 e 111 Cost., «in combinato disposto», in quanto la prevista necessità di anticipare le spese da sostenere per l'espletamento dell'incarico comporterebbe che il giudice individui il consulente tecnico d'ufficio, alter ego del magistrato nell'ambito del cosiddetto contraddittorio tecnico, sulla base non già della professionalità e della necessaria turnazione degli incarichi, bensì della capacità economica del medesimo; inoltre la necessaria anticipazione delle spese vive da parte del consulente tecnico d'ufficio, comportando il depauperamento, anche solo temporaneo, del patrimonio dell'ausiliario, inciderebbe indirettamente sulla sua indipendenza;
 che il giudice a quo ha dato atto che la Corte costituzionale, con ordinanza n. 209 del 2008, ha già dichiarato manifestamente infondata la questione di legittimità costituzionale dell'art. 131, comma 4, lettera c), del d.P.R. n. 115 del 2002, la quale, tuttavia, sarebbe stata sollevata in riferimento a parametri costituzionali solo parzialmente coincidenti con quelli attualmente evocati e, comunque, sulla base di argomentazioni diverse;
 che, alla luce delle considerazioni svolte, il Tribunale ordinario della Spezia ha chiesto che sia dichiarata costituzionalmente illegittima la norma contenuta nell'art. 131, comma 4, lettera c), del d.P.R. n. 115 del 2002, nella parte in cui prevede che sono anticipate dall'erario «le spese sostenute per l'adempimento dell'incarico da parte di questi ultimi [consulenti tecnici di parte e ausiliari del magistrato]», anziché prevedere che siano anticipate dall'erario «le spese sostenute o, qualora i consulenti tecnici di parte e gli ausiliari del magistrato chiedano l'anticipazione, le spese da sostenere per l'adempimento dell'incarico da parte di questi ultimi»;
 che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la manifesta infondatezza delle questioni sollevate; 
 che la difesa statale evidenzia come la Corte costituzionale, con ordinanza n. 209 del 2008, abbia già dichiarato manifestamente infondata un'analoga questione di legittimità costituzionale;
 che l'Avvocatura generale richiama la giurisprudenza della Corte di cassazione, in base alla quale la consulenza tecnica d'ufficio, quale ausilio fornito al giudice da un collaboratore esterno, anziché mezzo di prova in senso proprio, è un atto compiuto nell'interesse generale della giustizia e, dunque, nell'interesse comune delle parti (è citata la sentenza della Corte di cassazione, sezione terza civile, 17 gennaio 2013, n. 1023);
 che la difesa statale mette in luce come il giudicante possa, quindi, disporre che le spese della consulenza tecnica d'ufficio siano sopportate in solido dalle parti del giudizio, indipendentemente dall'esito dello stesso, oppure, attesa la provvisorietà dell'anticipo riconosciuto al consulente tecnico per l'espletamento dell'incarico, possa prevedere, ove una delle parti sia stata ammessa al patrocinio a spese dello Stato, che le spese di consulenza siano anticipate dalla parte abbiente, non sussistendo perciò alcuna violazione dell'art. 24 Cost.;
 che, inoltre, ad avviso della difesa statale, nel caso in cui l'ausiliario non abbia la possibilità di anticipare le spese, il giudice potrebbe ravvisare un «giusto motivo» di astensione e, conseguentemente, procedere alla sua sostituzione, manifestandosi così l'inconsistenza delle censure riferite alla violazione dell'art. 101 Cost., non risultando in alcun modo ostacolata la funzione giudiziaria;
 che, infine, l'evocazione del parametro di cui all'art. 111 Cost. sarebbe inconferente, poiché, come già stabilito dalla ricordata ordinanza n. 209 del 2008 della Corte costituzionale, la disposizione censurata, disciplinando il procedimento di liquidazione delle spese sostenute dall'ausiliario del magistrato, non è idonea a incidere sui tempi di celebrazione del processo, cui lo stesso procedimento è accessorio. 
 Considerato che il Tribunale ordinario della Spezia dubita, in riferimento agli artt. 3, 24, 101 e 111 Cost., della legittimità costituzionale dell'art. 131, comma 4, lettera c), del decreto del Presidente della Repubblica 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia - Testo A); 
 che, ad avviso del rimettente, la disposizione censurata, nella parte in cui non prevede che siano anticipate dall'erario anche le spese ancora da sostenersi per l'adempimento dell'incarico, qualora i consulenti tecnici di parte e gli ausiliari del magistrato ne chiedano l'anticipazione, violerebbe gli «artt. 3 e 24 Cost., anche in combinato disposto tra loro», perché precluderebbe, limiterebbe o, comunque, condizionerebbe l'esercizio del diritto di difesa dei soggetti meno abbienti;
 che, in particolare, la norma farebbe dipendere la possibilità di istruire il procedimento dalla capacità economica del consulente tecnico di ufficio, e comporterebbe, altresì, che le modalità di espletamento delle indagini peritali possano essere influenzate da una valutazione di economicità da parte del consulente tecnico d'ufficio, di fatto indotto o costretto ad anticipare personalmente le spese nella minor misura possibile;
 che la medesima disposizione sarebbe in contrasto anche con l'art. 111 Cost., perché il consulente tecnico d'ufficio, nel caso in cui non sia in grado di anticipare le spese, sarebbe costretto a richiedere prestiti o a risparmiare nel corso del tempo le somme necessarie per l'espletamento dell'incarico, con conseguente allungamento dei tempi del processo;
 che, da ultimo, la norma censurata violerebbe anche «i principi contenuti negli artt. 101 e 111 Cost., in combinato disposto tra loro», giacché, da un lato, indurrebbe il giudice ad individuare il consulente tecnico d'ufficio sulla base, non già della professionalità e della necessaria turnazione degli incarichi, bensì della capacità economica del medesimo; dall'altro, la necessaria anticipazione delle spese vive da parte del consulente tecnico d'ufficio, comportando il depauperamento - seppure temporaneo - del suo patrimonio, inciderebbe indirettamente anche sulla sua indipendenza;
 che l'ordinanza di rimessione muove da presupposti interpretativi palesemente erronei, frutto, inoltre, di un'incompleta ricostruzione del quadro normativo di riferimento;
 che, in primo luogo, infatti, il giudice a quo esclude la possibilità di porre l'anticipazione delle spese di consulenza - peraltro non obbligatoria, ma rimessa ad una valutazione discrezionale a norma dell'art. 8, comma 1, del d.P.R. n. 115 del 2002 - a carico di parte convenuta, assumendo che quest'ultima non avrebbe alcun interesse a consentire lo svolgimento delle indagini peritali, il cui esito, in astratto, potrebbe rivelarsi ad essa sfavorevole;
 che, così argomentando, il giudice rimettente trascura del tutto la funzione assolta dalla consulenza tecnica d'ufficio, la quale non costituisce un vero e proprio mezzo di prova, è sottratta alla disponibilità delle parti, ed è finalizzata all'acquisizione, da parte del giudice - al cui prudente apprezzamento è affidata - di un parere tecnico necessario per la valutazione di elementi probatori già acquisiti, ovvero per l'accertamento di fatti rilevabili unicamente con l'ausilio di specifiche cognizioni o strumentazioni tecniche;
 che, in secondo luogo, il giudice a quo trascura di considerare che, in base all'art. 63 del codice di procedura civile, il consulente tecnico - individuato tra coloro che, per propria scelta, si sono iscritti nel relativo albo - ha l'obbligo di prestare il suo ufficio, tranne che ricorra un giusto motivo di astensione, e non può rifiutare di eseguire gli incarichi ricevuti, avendo prestato con quella iscrizione un assenso preventivo alla nomina;
 che, in definitiva, gli erronei presupposti interpretativi e l'incompleta ricostruzione del quadro normativo di riferimento minano irrimediabilmente l'iter logico-argomentativo posto a fondamento della valutazione di non manifesta infondatezza delle questioni di legittimità costituzionale sollevate, in guisa tale da determinare la loro manifesta inammissibilità (ex multis, con riguardo alla erroneità del presupposto interpretativo, sentenza n. 241 del 2015 e ordinanza n. 187 del 2015, nonché sentenze n. 218 del 2014 e n. 249 del 2011; con riguardo all'incompleta ricostruzione del quadro normativo di riferimento, sentenze n. 60, n. 27 e n. 18 del 2015; ordinanze n. 209, n. 115 e n. 90 del 2015).
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 131, comma 4, lettera c), del decreto del Presidente della Repubblica 30 maggio 2002, n. 115 (Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia - Testo A), sollevate, in riferimento agli artt. 3, 24, 101 e 111 della Costituzione, dal Tribunale ordinario della Spezia, con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 18 maggio 2016. &#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Nicolò ZANON, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 10 giugno 2016.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
