<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/93/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/93/"/>
          <FRBRalias value="ECLI:IT:COST:2009:93" name="ECLI"/>
          <FRBRdate date="11/03/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="93"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/93/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/93/ita@/!main"/>
          <FRBRdate date="11/03/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/93/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/93/ita@.xml"/>
          <FRBRdate date="11/03/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="27/03/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Franco Gallo</cc:relatore_pronuncia>
        <cc:data_decisione>11/03/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Francesco AMIRANTE; Giudici: Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 57 del decreto del Presidente della Repubblica 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), promosso con ordinanza del 24 giugno 2008 dal Giudice di pace di Marcianise nel procedimento civile vertente tra Maria Letizia e la s.p.a. Gest Line – Gruppo Equitalia, iscritta al n. 372 del registro ordinanze 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2008. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio dell'11 marzo 2009 il Giudice relatore Franco Gallo. 
  
    Ritenuto che, con ordinanza del 24 giugno 2008 pronunciata nel corso di un giudizio civile vertente tra Maria Letizia e la s.p.a. Gest Line Gruppo Equitalia, il giudice di pace di Marcianise ha proposto, in riferimento agli articoli 3 e 24 della Costituzione, questione di legittimità dell'art. 57 del d.P.R. 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), «nella parte in cui esclude la possibilità di proporre opposizione all'esecuzione ex art. 615 c.p.c. in materia di riscossione esattoriale»; 
    che il rimettente si limita ad affermare che la norma censurata si pone in contrasto con gli evocati parametri, perché «non permette al singolo cittadino di far valere le proprie ragioni ed eccezioni in sede di opposizione all'esecuzione»; 
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata inammissibile o, comunque, infondata; 
    che la difesa erariale rileva: a) in punto di ammissibilità, che il rimettente omette di descrivere la fattispecie al suo esame; b) in punto di merito, che – a differenza di quanto sembra sostenere il giudice a quo – la norma censurata non esclude del tutto la proponibilità dell'opposizione all'esecuzione da parte del debitore esecutato e lascia, quindi, spazio per un intervento giurisdizionale; 
    che, con memoria depositata il 20 febbraio 2009, la s.p.a. Equitalia Polis (nuova denominazione della s.p.a. Gest Line – Gruppo Equitalia) si è costituita fuori termine. 
    Considerato che il rimettente dubita, in riferimento agli articoli 3 e 24 della Costituzione, della legittimità dell'art. 57 del d.P.R. 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), «nella parte in cui esclude la possibilità di proporre opposizione all'esecuzione ex art. 615 c.p.c. in materia di riscossione esattoriale»; 
    che la questione è manifestamente inammissibile, per omessa descrizione della fattispecie oggetto del giudizio a quo; 
    che infatti il giudice a quo si limita ad affermare, in punto di non manifesta infondatezza, che la norma censurata «non permette al singolo cittadino di far valere le proprie ragioni ed eccezioni in sede di opposizione all'esecuzione», senza fornire alcun chiarimento circa il caso sottoposto al suo esame e circa la rilevanza della questione; 
    che, secondo la costante giurisprudenza di questa Corte, l'insufficiente descrizione della fattispecie impedisce, come nel caso di specie, di vagliare l'effettiva applicabilità della norma denunciata al caso dedotto nel giudizio principale e, pertanto, si risolve in carente motivazione sulla rilevanza della questione sollevata, determinandone la manifesta inammissibilità (ex plurimis, ordinanze n. 35 del 2009, nn. 300 e 266 del 2008). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, commi 1 e 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 57 del d.P.R. 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), sollevata, in riferimento agli artt. 3 e 24 della Costituzione, dal giudice di pace di Marcianise con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 marzo 2009.  &#13;
F.to:  &#13;
Francesco AMIRANTE, Presidente  &#13;
Franco GALLO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 27 marzo 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
