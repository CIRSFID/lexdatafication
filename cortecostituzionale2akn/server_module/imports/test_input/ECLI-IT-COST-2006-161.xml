<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/161/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/161/"/>
          <FRBRalias value="ECLI:IT:COST:2006:161" name="ECLI"/>
          <FRBRdate date="05/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="161"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/161/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/161/ita@/!main"/>
          <FRBRdate date="05/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/161/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/161/ita@.xml"/>
          <FRBRdate date="05/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="14/04/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Maria Rita Saulle</cc:relatore_pronuncia>
        <cc:data_decisione>05/04/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 19 del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), promosso con ordinanza dell'8 febbraio 2005 dal Giudice di Pace di Roma, sul ricorso proposto da D. V., iscritta al n. 274 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica  n. 21, prima serie speciale, dell'anno 2005. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio dell'8 marzo 2006 il Giudice relatore Maria Rita Saulle. 
  
    Ritenuto che il Giudice di pace di Roma, con ordinanza emessa l'8 febbraio 2005, ha sollevato, in riferimento all'art. 2 della Costituzione (in relazione all'art. 8 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, resa esecutiva con legge 4 agosto 1955, n. 848), questione di legittimità costituzionale dell'art. 19 del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui non prevede il divieto di espulsione per quegli stranieri «il cui lavoro in Italia costituisce unico sostegno per la famiglia di origine»; 
    che, secondo il giudice a quo, in casi simili, proprio l'esecuzione del provvedimento di espulsione «sgretolerebbe il senso morale della famiglia» e «creerebbe una fonte di povertà con tutte le conseguenze del caso»; 
    che, a parere del rimettente, non sarebbe possibile negare il «soggiorno ad una donna che con il suo onesto lavoro intende sostenere cinque fratelli e i genitori che versano nella più assoluta miseria», se non violando i principi umanitari; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la declaratoria di inammissibilità o comunque di infondatezza della questione; 
    che, secondo l'Avvocatura, il rimettente ha omesso di descrivere la fattispecie oggetto del suo giudizio, con la conseguenza che la dedotta questione di costituzionalità deve ritenersi inammissibile, risultando del tutto carente anche la motivazione sulla rilevanza della stessa; 
    che, la parte pubblica, nel merito ritiene la questione infondata, poiché sussiste un'ampia discrezionalità del legislatore nel regolare la materia dell'immigrazione, limitata solo dal vincolo che le scelte non risultino manifestamente irragionevoli, stante l'ineludibile compito dello Stato di presidiare le proprie frontiere con regole che tutelino la collettività nazionale; 
    che, tali scelte possono anche comportare, a seguito del bilanciamento con detto prevalente interesse, il sacrificio di valori costituzionalmente protetti, come quelli richiamati nell'ordinanza di rimessione; 
    che, secondo la difesa erariale, la previsione del divieto di espulsione nei confronti dello straniero, entrato clandestinamente nel territorio dello Stato, ove svolge «un lavoro costituente l'unico sostegno economico per la famiglia d'origine», comporterebbe una scelta irragionevole da parte del legislatore, con conseguente ingiustificato sacrificio dei valori costituzionali considerati dalle norme in materia di immigrazione; 
    che, quanto all'evocato art. 8 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, l'Avvocatura rileva che i diritti in esso previsti possono, in determinati casi, essere limitati dalla legge. 
    Considerato che il Giudice di pace di Roma, ha sollevato, in riferimento all'art. 2 della Costituzione (in relazione all'art. 8 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, resa esecutiva con legge 4 agosto 1955, n. 848), questione di legittimità costituzionale dell'art. 19 del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui non prevede il divieto di espulsione per quegli stranieri «il cui lavoro in Italia costituisce unico sostegno per la famiglia di origine»; 
    che tanto la rilevanza quanto la non manifesta infondatezza della questione sono apoditticamente affermate dal rimettente, senza motivazione alcuna ed in difetto di una pur minima descrizione della fattispecie dedotta in giudizio;  
    che, pertanto, la questione stessa va dichiarata, per entrambi i motivi, manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 19 del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), sollevata, in riferimento all'art. 2 della Costituzione (in relazione all'art. 8 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, resa esecutiva con legge 4 agosto 1955, n. 848), dal Giudice di pace di Roma con l'ordinanza in epigrafe. &#13;
       </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 aprile 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 14 aprile 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
