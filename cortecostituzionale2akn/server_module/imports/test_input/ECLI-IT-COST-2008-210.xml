<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/210/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/210/"/>
          <FRBRalias value="ECLI:IT:COST:2008:210" name="ECLI"/>
          <FRBRdate date="09/06/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="210"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/210/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/210/ita@/!main"/>
          <FRBRdate date="09/06/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/210/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/210/ita@.xml"/>
          <FRBRdate date="09/06/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/06/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maria Napolitano</cc:relatore_pronuncia>
        <cc:data_decisione>09/06/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 1 e 2 della legge 25 novembre 2003, n. 339 (Norme in materia di incompatibilità dell'esercizio della professione di avvocato), promosso con ordinanza del 23 agosto 2007 dal Giudice di pace di Cortona nel procedimento civile vertente tra Jarubowska Edyta Joanna e Maneggia Alessandro, iscritta al n. 792 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2007. 
      Visto l'atto di costituzione di Jarubowska Edyta Joanna nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 7 maggio 2008 il Giudice relatore Paolo Maria Napolitano. 
  
    Ritenuto che il Giudice di pace di Cortona ha sollevato questione di legittimità costituzionale, in riferimento agli artt. 3, 4, 35 e 41 della Costituzione, dell'art. 1 della legge 25 novembre 2003, n. 339 (Norme in materia di incompatibilità dell'esercizio della professione di avvocato), nella parte in cui prevede che il regime di incompatibilità fra impiego pubblico ed esercizio della professione forense si applichi anche ai dipendenti pubblici a tempo parziale che risultino già iscritti negli albi degli avvocati alla data di entrata in vigore della legge n. 339 del 2003, nonché dell'art. 2 della citata legge, nella parte in cui prevede soltanto un breve periodo di moratoria per esercitare l'opzione imposta fra impiego pubblico ed esercizio della professione; 
    che il rimettente nel descrivere la fattispecie al suo esame si limita a dire «che l'attrice ha manifestato espressamente, negli atti di causa, la propria volontà di avvalersi dei suoi difensori per la trattazione della causa» e «che gli stessi difensori hanno manifestato la volontà di continuare nella conduzione della causa e che entrambe le volontà, della parte e dei difensori, sono meritevoli di considerazione, trattandosi di diritti assoluti e costituzionalmente garantiti»; 
    che si è costituita in giudizio Jarubowska Edyta Joanna, parte attrice nel giudizio a quo; 
    che, quanto alla rilevanza della questione, la difesa della parte privata sottolinea che, nelle more del processo, i propri difensori sono stati cancellati dall'albo dell'ordine degli avvocati di Perugia, ai sensi dell'art. 2 della legge n. 339 del 2003, essendo al contempo pubblici dipendenti in part time non superiore al 50%; 
    che, quanto alla manifesta infondatezza, la difesa della parte privata indica come tertium comparationis la disciplina relativa a coloro che, dopo aver commesso determinati reati, possono godere dell'indulto in virtù dell'art. 1 della legge 31 luglio 2006, n. 241 (Concessione di indulto), e ritiene che sussista una evidente disparità di trattamento tra chi ha beneficiato dell'indulto e chi, solo perché dipendente pubblico part time, si vede inflitta la sanzione della cancellazione dall'albo degli avvocati; 
    che la parte costituita chiede, in via subordinata, che la Corte «sollevi innanzi a se stessa la questione di legittimità costituzionale degli articoli 1 e 2 della legge n. 339 del 2003 per violazione dell'art. 25 della Cost. in quanto, secondo detto articolo, nessuno può essere punito se non in forza di una legge entrata in vigore prima del fatto commesso e, appunto, in questo caso ai dipendenti pubblici part time già iscritti agli Albi degli Avvocati prima dell'entrata in vigore della legge 339/03 è stata applicata la pena della cancellazione dall'albo degli avvocati proprio con una legge successiva al fatto commesso»; 
    che, in via ulteriormente subordinata, chiede alla Corte di sollevare questione di legittimità costituzionale innanzi a se stessa degli artt. 1 e 2 della legge n. 339 del 2003, per contrasto con gli artt. 3, 4, 35 e 41 Cost., nella parte in cui non prevedono un regime transitorio che salvaguardi la posizione di chi risulti già iscritto negli albi degli avvocati alla data di entrata in vigore della legge n. 339 del 2003; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione venga dichiarata inammissibile o infondata; 
    che l'Avvocatura dello Stato eccepisce, in via preliminare, l'inammissibilità della questione avendo il rimettente omesso completamente sia di illustrare i termini della controversia ai fini della valutazione della rilevanza, sia di argomentare la presunta non manifesta infondatezza della stessa, ed essendosi, invece, limitato a rinviare genericamente, dichiarando di condividerla, alla relativa eccezione formulata dalla parte, non fornendo, neppure sinteticamente, gli argomenti interpretativi necessari per valutare i profili di asserita incostituzionalità delle norme denunciate, le quali senza alcuna argomentazione, sono «lasciate completamente all'esame e al giudizio della Corte»; 
    che, nel merito, la difesa statale richiama la sentenza di questa Corte n. 390 del 2006 con la quale, sia pure in relazione ad una questione in parte diversa rispetto a quella in esame, si è ritenuto ragionevole il nuovo regime di incompatibilità introdotto dagli articoli 1 e 2 della legge n. 339 del 2003, e che, con riferimento alla questione di coloro che erano iscritti negli albi degli avvocati ed esercitavano la professione sulla base della disciplina preesistente, sottolinea che il legislatore, con l'art. 2 della normativa in parola, ha comunque riconosciuto la posizione differenziata di tali soggetti, attribuendo loro la facoltà di optare tra il mantenimento del rapporto di impiego e l'esercizio della professione forense entro il termine di 36 mesi dall'entrata in vigore della legge stessa. 
    Considerato che il Giudice di pace di Cortona dubita, in riferimento agli artt. 3, 4, 35 e 41 della Costituzione, della legittimità costituzionale dell'art. 1 della legge 25 novembre 2003, n. 339 (Norme in materia di incompatibilità dell'esercizio della professione di avvocato), nella parte in cui prevede che il regime di incompatibilità fra impiego pubblico ed esercizio della professione forense si applichi anche ai dipendenti pubblici a tempo parziale che risultino già iscritti negli Albi degli avvocati alla data di entrata in vigore della legge n. 339 del 2003, nonché dell'art. 2 della citata legge, nella parte in cui prevede soltanto un breve periodo di moratoria per esercitare l'opzione imposta fra impiego pubblico ed esercizio della professione; 
    che la questione è manifestamente inammissibile; 
    che il rimettente ha omesso ogni descrizione della fattispecie oggetto del giudizio principale e non ha motivato sulla non manifesta infondatezza delle questioni sollevate; 
    che pertanto l'ordinanza di rimessione è inidonea a dare valido ingresso al giudizio di legittimità costituzionale in quanto priva dei requisiti minimi a tal fine necessari (ex plurimis: ordinanze nn. 277, 33 e 14 del 2006).  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>  &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale degli artt. 1 e 2 della legge 25 novembre 2003, n. 339 (Norme in materia di incompatibilità dell'esercizio della professione di avvocato), sollevata, in riferimento agli artt. 3, 4, 35 e 41 della Costituzione, dal Giudice di pace di Cortona con l'ordinanza in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 giugno 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Paolo Maria NAPOLITANO, Redattore  &#13;
Gabriella MELATTI, Cancelliere  &#13;
Depositata in Cancelleria il 13 giugno 2008.  &#13;
Il Cancelliere  &#13;
F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
