<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2017/254/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2017/254/"/>
          <FRBRalias value="ECLI:IT:COST:2017:254" name="ECLI"/>
          <FRBRdate date="07/11/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="254"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2017/254/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2017/254/ita@/!main"/>
          <FRBRdate date="07/11/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2017/254/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2017/254/ita@.xml"/>
          <FRBRdate date="07/11/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/12/2017" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2017</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Mario Rosario Morelli</cc:relatore_pronuncia>
        <cc:data_decisione>07/11/2017</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 29, comma 2, del decreto legislativo 10 settembre 2003, n. 276 (Attuazione delle deleghe in materia di occupazione e mercato del lavoro, di cui alla legge 14 febbraio 2003, n. 30), promosso dalla Corte di appello di Venezia nel procedimento vertente tra Elica spa e H. A. e altri, con ordinanza del 14 luglio 2016, iscritta al n. 247 del registro ordinanze 2016 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 49, prima serie speciale, dell'anno 2016. 
 Visti l'atto di costituzione di T. B., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 7 novembre 2017 il Giudice relatore Mario Rosario Morelli;
 uditi l'avvocato Carlo Cester per T. B. e l'avvocato dello Stato Gabriella D'Avanzo per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Con l'ordinanza in epigrafe, la Corte di appello di Venezia - chiamata a decidere sul gravame proposto da una società committente avverso la statuizione di primo grado con la quale era stata condannata al pagamento di retribuzioni non corrisposte dall'impresa (sua) subfornitrice, ai lavoratori di quest'ultima - ha sollevato, premessane la rilevanza, questione incidentale di legittimità costituzionale della norma, applicata dal primo giudice, di cui all'art. 29, comma 2, del decreto legislativo 10 settembre 2003, n. 276 (Attuazione delle deleghe in materia di occupazione e mercato del lavoro, di cui alla legge 14 febbraio 2003, n. 30).
 Secondo la Corte rimettente detta norma - nel disporre che «In caso di appalto di opere o di servizi, il committente imprenditore o datore di lavoro è obbligato in solido con l'appaltatore, nonché con ciascuno degli eventuali subappaltatori [...] a corrispondere ai lavoratori i trattamenti retributivi [...] in relazione al periodo di esecuzione del contratto di appalto» - «non è suscettibile di essere applicata oltre i casi espressamente previsti (appalto e subappalto), né la natura della disposizione e la diversità di fattispecie contrattuale tra subappalto e subfornitura, consente un'interpretazione costituzionalmente orientata della stessa».
 Ma ciò, appunto, ne farebbe sospettare il contrasto con l'art. 3 della Costituzione poiché, ad avviso del giudice a quo, non sarebbe ragionevole che, nel fenomeno diffuso della esternalizzazione e della parcellizzazione del processo produttivo, i dipendenti del subfornitore siano privati di una garanzia legale di cui, per contro, possono godere i dipendenti di un appaltatore e subappaltatore. E potrebbe, nel contempo, innescare la violazione dell'art. 36 Cost., per il profilo della inadeguatezza della retribuzione, anche alla luce dei principi in materia di condizioni di lavoro giuste ed eque, di cui all'art. 31 della Carta dei diritti fondamentali dell'Unione europea, proclamata a Nizza il 7 dicembre 2000 e, in una versione adattata, a Strasburgo il 12 dicembre 2007.
 2.- Nel giudizio innanzi a questa Corte si è costituita una delle lavoratrici, parte del processo principale, per aderire alla prospettazione dell'ordinanza di rimessione, «non essendovi ragioni per trattare diversamente fattispecie che o coincidono sostanzialmente o comunque assolvono alla medesima funzione economico-sociale».
 3.- È anche intervenuto il Presidente del Consiglio dei ministri, per il tramite dell'Avvocatura generale dello Stato, che ha concluso per l'inammissibilità o comunque per l'infondatezza della questione sollevata, sostenendo che «il giudice a quo ben avrebbe potuto risolvere la stessa in via esegetica, interpretando l'art. 29, d.lgs. n. 276 del 2003, conformemente ai parametri costituzionali rispetto ai quali sussisterebbe, in ipotesi, il paventato contrasto».
 4.- Sia la parte privata che il Presidente del Consiglio dei ministri hanno anche depositato successive memorie, in entrambe le quali si sottolinea la possibilità di una interpretazione costituzionalmente orientata della disposizione denunciata.
 In particolare, secondo la difesa dell'interveniente, sarebbe «corretto ritenere che, quanto meno sotto il profilo delle garanzie giuslavoristiche e previdenziali, la subfornitura costituisca una specie del contratto di appalto e, come tale, benefici delle tutele di cui all'art. 29, comma 2, del d.lgs. n. 276/2003».Considerato in diritto1.- L'art. 29 del decreto legislativo 10 settembre 2003, n. 276 (Attuazione delle deleghe in materia di occupazione e mercato del lavoro, di cui alla legge 14 febbraio 2003, n. 30), nel suo comma 2 e per la parte che qui rileva ai fini dello scrutinio di legittimità costituzionale, testualmente dispone che «In caso di appalto di opere o di servizi, il committente imprenditore o datore di lavoro è obbligato in solido con l'appaltatore, nonché con ciascuno degli eventuali subappaltatori entro il limite di due anni dalla cessazione dell'appalto, a corrispondere ai lavoratori i trattamenti retributivi, comprese le quote di trattamento di fine rapporto, nonché i contributi previdenziali e i premi assicurativi dovuti in relazione al periodo di esecuzione del contratto di appalto [...]». 
 2.- Sul presupposto che la garanzia della responsabilità solidale del committente per i crediti retributivi e contributivi dei lavoratori "indiretti", testualmente riferita ai dipendenti dell'appaltatore e del subappaltatore, non si estenda anche ai dipendenti del subfornitore, per la diversità di fattispecie contrattuale tra appalto e subfornitura, la Corte di appello di Venezia ravvisa in ciò una irragionevole disparità di trattamento ed un vulnus alla effettività e adeguatezza della retribuzione in danno di detta ultima categoria di lavoratori. E denuncia, pertanto, il contrasto della su riferita disposizione con l'art. 3 della Costituzione, nonché con l'art. 36 Cost., evocando, a supporto argomentativo della violazione di tale secondo parametro, anche i principi sulle giuste ed eque condizioni di lavoro, di cui all'art. 31 della Carta dei diritti fondamentali dell'Unione europea, proclamata a Nizza il 7 dicembre 2000 e, in una versione adattata, a Strasburgo il 12 dicembre 2007.
 3.- Della questione così sollevata l'Avvocatura generale dello Stato ha preliminarmente eccepito l'inammissibilità, addebitando alla rimettente l'omesso esperimento del tentativo di interpretazione della disposizione denunciata in modo conforme a Costituzione. 
 L'eccezione non è fondata.
 Il giudice a quo - nel premettere che «la soluzione della causa dipende dalla interpretazione [...] della norma di cui all'art. 29 legge [recte: decreto legislativo] 276/03» - non ha mancato, infatti, di prospettarsi, e di verificare, la possibilità di una lettura costituzionalmente orientata di tale disposizione ed ha ritenuto di escluderla: soluzione, questa, la cui condivisibilità o meno attiene al merito - ma preclude, evidentemente, la chiesta declaratoria di inammissibilità - della questione in esame.
 4.- La legge 18 giugno 1998, n. 192 (Disciplina della subfornitura nelle attività produttive) risponde ad una funzione regolativa dell'integrazione della prestazione del subfornitore nel processo produttivo dell'impresa committente «in conformità a progetti esecutivi, conoscenze tecniche e tecnologiche, modelli o prototipi» forniti dall'impresa medesima.
 Dottrina e giurisprudenza di merito sono tuttora divise sulla configurazione giuridica e sul più corretto inquadramento sistematico del contratto di subfornitura, in particolare, quanto al profilo della sua autonomia o meno rispetto al contratto di appalto di cui all'art. 1655 del codice civile.
 Tra tali due figure negoziali vi sarebbe, infatti, secondo un primo orientamento, un rapporto di species a genus, nel senso che la subfornitura non altro costituirebbe che un "sottotipo", se non un equivalente, del contratto di appalto, ovvero uno schema generale di protezione nel quale possono rientrare plurime figure negoziali in senso trasversale, tra cui l'appalto. Secondo altro indirizzo interpretativo vi sarebbe, invece, tra i rispettivi schemi negoziali, una sostanziale differenza. E proprio la "dipendenza tecnologica", presente nel contratto di subfornitura, segnerebbe il discrimine rispetto all'appalto che comporta, invece, una autonomia dell'appaltatore nella scelta delle modalità operative attraverso le quali conseguire il risultato richiesto ed atteso dal committente.
 Anche la giurisprudenza di legittimità non è al riguardo univoca.
 In un caso (sezione seconda, sentenza 29 maggio 2008, n. 14431), la Corte di cassazione ha affermato, infatti, che il rapporto di subfornitura, enucleato al fine di dare adeguata tutela, a fronte di abusi che determinino un eccessivo squilibrio nei diritti e negli obblighi delle parti, alle imprese che lavorino in stato di dipendenza economica rispetto ad altre, riguarda il fenomeno meramente economico della cosiddetta integrazione verticale fra imprese, ma «è riferibile ad una molteplicità di figure negoziali; a volte estremamente eterogenee, da individuarsi caso per caso, potendo assumere i connotati del contratto di somministrazione, della vendita di cose future, [quindi, anche] dell'appalto d'opera o di servizi ecc.».
 In altra successiva occasione, in cui veniva in rilievo la figura dell'«abuso di dipendenza economica» di cui all'art. 9 della legge n. 192 del 1998, la motivazione della decisione (sezioni unite, ordinanza 25 novembre 2011, n. 24906) lascia presupporre - sulla stessa linea di interrelazione tra le categorie negoziali in esame - l'attribuzione di una portata estensiva dello schema di tutela apprestato per la subfornitura.
 Mentre, in una più recente pronuncia (sezione terza, sentenza 25 agosto 2014, n. 18186), la stessa Corte è incline ad attribuire connotati di specificità al contratto di subfornitura, come forma «non paritetica» di cooperazione imprenditoriale, «nella quale la dipendenza economica del subfornitore si palesa, oltre che sul piano del rapporto commerciale e di mercato [...] anche su quello delle direttive tecniche di esecuzione», in quanto «l'inserimento del subfornitore - sebbene in forza di un'opzione organizzativa di esternalizzazione - in un determinato livello del processo produttivo proprio del committente [...] non può non implicare l'assoggettamento della prestazione di subfornitura all'osservanza di più o meno penetranti (a seconda della natura della lavorazione e del prodotto) direttive tecniche del committente. Quelle stesse direttive tecniche che questi avrebbe dovuto osservare ove avesse optato per mantenere all'interno della propria organizzazione l'intero ciclo di produzione». 
 Ciò che, appunto, sempre secondo il giudice della nomofilachia, «diversifica il rapporto di subfornitura commerciale [...] dall'appalto d'opera o di servizi, nel quale l'appaltatore è chiamato, nel raggiungimento del risultato, ad una prestazione rispondente ad autonomia non solo organizzativa ed imprenditoriale, ma anche tecnico-esecutiva; con quanto ne deriva in ordine alla maggior ampiezza della sua responsabilità per i vizi della cosa e la sua non perfetta rispondenza a quanto convenuto».
 5.- La Corte rimettente non ignora la riferita duplicità di opzioni interpretative in tema di subfornitura e si dichiara anzi «consapevole del dibattito sorto tra gli studiosi del diritto», in occasione dell'entrata in vigore della legge n. 192 del 1998.
 Ma ciò che trascura poi di considerare è che ciascuno degli orientamenti emersi, in chiave di riconducibilità o meno del contratto di subfornitura alla cornice concettuale e disciplinatoria dell'appalto e del subappalto, è comunque aperto, e non chiuso, all'estensione della responsabilità solidale del committente ai crediti di lavoro dei dipendenti del subfornitore.
 Una tale estensione costituisce naturale corollario della tesi che configura la subfornitura come "sottotipo" dell'appalto e, a maggior ragione, di quella che sostanzialmente equipara i due negozi.
 Ma, anche nel contesto del diverso orientamento, che considera la subfornitura come "tipo" negoziale autonomo, tale premessa interpretativa non è, per lo più, ritenuta preclusiva della applicazione, in via analogica, della disposizione censurata in favore dei dipendenti del subfornitore.
 All'obiezione per cui la natura eccezionale della norma sulla responsabilità solidale del committente osterebbe ad una sua applicazione estensiva in favore di una platea di soggetti diversi dai dipendenti dell'appaltatore o subappaltatore (ai quali soltanto la norma stessa fa testuale riferimento) si replica, infatti, che l'eccezionalità della responsabilità del committente è tale rispetto alla disciplina ordinaria della responsabilità civile - che esige di correlarsi alla condotta di un soggetto determinato - ma non lo è più se riferita all'ambito, ove pur distinto, ma comunque omogeneo in termini di lavoro indiretto, dei rapporti di subfornitura.
 Ciò in quanto la ratio dell'introduzione della responsabilità solidale del committente - che è quella di evitare il rischio che i meccanismi di decentramento, e di dissociazione fra titolarità del contratto di lavoro e utilizzazione della prestazione, vadano a danno dei lavoratori utilizzati nell'esecuzione del contratto commerciale - non giustifica una esclusione (che si porrebbe, altrimenti, in contrasto con il precetto dell'art. 3 Cost.) della predisposta garanzia nei confronti dei dipendenti del subfornitore, atteso che la tutela del soggetto che assicura una attività lavorativa indiretta non può non estendersi a tutti i livelli del decentramento.
 In tal senso venendo anche in rilievo - lo sottolinea la difesa di parte attrice nel giudizio a quo - la considerazione che le esigenze di tutela dei dipendenti dell'impresa subfornitrice, in ragione della strutturale debolezza del loro datore di lavoro, sarebbero da considerare ancora più intense e imprescindibili che non nel caso di un "normale" appalto.
 6.- Alla stregua di quanto precede, la norma denunciata è, pertanto, interpretabile - e va correttamente, dunque, interpretata - in modo costituzionalmente adeguato e coerente agli evocati parametri di riferimento: nel senso, appunto, che il committente è obbligato in solido (anche) con il subfornitore relativamente ai crediti lavorativi, contributivi e assicurativi dei dipendenti di questi.
 L'odierna questione - sollevata sul presupposto di una (sia pur letteralmente possibile, ma comunque errata) esegesi contra Constitutionem della norma stessa - va dichiarata, pertanto, non fondata.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara non fondata, nei sensi di cui in motivazione, la questione di legittimità costituzionale dell'art. 29, comma 2, del decreto legislativo 10 settembre 2003, n. 276 (Attuazione delle deleghe in materia di occupazione e mercato del lavoro, di cui alla legge 14 febbraio 2003, n. 30), sollevata, in riferimento agli artt. 3 e 36 della Costituzione, dalla Corte di appello di Venezia, con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 novembre 2017.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Mario Rosario MORELLI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 6 dicembre 2017.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
