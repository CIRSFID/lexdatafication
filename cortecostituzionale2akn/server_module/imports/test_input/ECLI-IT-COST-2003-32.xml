<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/32/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/32/"/>
          <FRBRalias value="ECLI:IT:COST:2003:32" name="ECLI"/>
          <FRBRdate date="16/01/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="32"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/32/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/32/ita@/!main"/>
          <FRBRdate date="16/01/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/32/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/32/ita@.xml"/>
          <FRBRdate date="16/01/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="04/02/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>16/01/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 459 del codice di procedura penale, promosso, nell'ambito di un procedimento penale, dal Tribunale di Cosenza con ordinanza del 20 febbraio 2002, iscritta al n. 272 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica, n. 24, prima serie speciale, dell'anno 2002. 
    Visto l'atto di costituzione dell'imputato nel procedimento a quo; 
    udito nell'udienza pubblica del 3 dicembre 2002 il Giudice relatore Guido Neppi Modona; 
    udito l'avvocato Antonio Cersosimo per la parte costituita. 
    Ritenuto che il Tribunale di Cosenza ha sollevato, in riferimento agli artt. 3, 24 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 459 del codice di procedura penale, nella parte in cui non prevede che, prima di chiedere al giudice per le indagini preliminari l'emissione del decreto penale di condanna, il pubblico ministero debba fare notificare all'indagato l'avviso di cui all'art. 415-bis cod. proc. pen.;  
    che il rimettente, premesso che è investito del giudizio a seguito di opposizione a un decreto penale di condanna e che nel corso del dibattimento il difensore dell'imputato ha eccepito l'illegittimità costituzionale dell'art. 459 cod. proc. pen., rileva in primo luogo che la disciplina censurata si pone in contrasto con l'art. 111 Cost.;  
    che infatti, mentre nel procedimento ordinario l'imputato è normalmente posto in condizioni di conoscere l'accusa a suo carico, perlomeno al termine delle indagini, mediante l'avviso di cui all'art. 415-bis cod. proc. pen., nel procedimento per decreto l'imputato viene a conoscenza dell'accusa «solo ed esclusivamente all'atto della notificazione della pronuncia di condanna», in violazione del terzo comma dell'art. 111 Cost., che assicura all'accusato il diritto di essere informato, nel più breve tempo possibile, della natura e dei motivi dell'accusa elevata a suo carico; 
    che la condanna per decreto violerebbe inoltre il principio del contraddittorio nella formazione della prova, enunciato dall'art. 111, quarto comma, Cost., in quanto si innesta in un procedimento caratterizzato da una struttura fortemente inquisitoria si fonda sul materiale investigativo raccolto unilateralmente dal pubblico ministero;  
    che, infine, nel procedimento per decreto il consenso dell'imputato alla formazione della prova fuori dal contraddittorio verrebbe ad essere configurato come tacito e posticipato: «successivo, non solo alla formazione ed all'acquisizione della prova, ma anche al suo utilizzo, e persino, alla pronuncia di condanna», in palese contrasto con il quinto comma dell'art. 111 Cost.;  
    che, nell'ambito di un sistema normativo che prevede che all'imputato sia dato l'avviso di chiusura delle indagini preliminari, addirittura a pena di nullità, prima del decreto di citazione a giudizio ex art. 552 cod. proc. pen., la disciplina del procedimento per decreto violerebbe anche gli artt. 3 e 24 Cost., per l'irragionevole disparità di trattamento tra l'imputato nei cui confronti è emesso il decreto di condanna, al quale è assicurata «esclusivamente una tutela successiva che può concretamente attuarsi soltanto con il rimedio dell'opposizione», e l'imputato tratto a giudizio con decreto di citazione, e perché il diritto di difesa dell'imputato viene  ridotto e compresso, potendo essere esercitato solo in sede di opposizione, quando il decreto, che è sostanzialmente una sentenza di condanna, è ormai stato emesso; 
    che si è costituito l'imputato nel procedimento a quo, rappresentato e difeso dall'avvocato Antonio Cersosimo, insistendo per l'accoglimento della questione; 
    che la parte, nello sviluppare le argomentazioni svolte nell'ordinanza di rimessione, sottolinea in particolare come la disciplina del procedimento per decreto, consentendo di pervenire ad un'affermazione di responsabilità dell'imputato senza che questi abbia avuto conoscenza dell'accusa a suo carico e abbia preventivamente espresso il consenso alla utilizzazione del materiale investigativo raccolto unilateralmente dal pubblico ministero, violerebbe l'art. 111 Cost., nonché il diritto di difesa dell'imputato; 
    che, nonostante la Corte costituzionale abbia sinora escluso che la mancata previsione dell'invito a comparire determini l'illegittimità del rito monitorio, la portata del nuovo art. 111 Cost. non consentirebbe più di ritenere conforme a Costituzione un procedimento che garantisce il contraddittorio solo dopo la condanna, subordinandolo altresì alla proposizione dell'atto di opposizione, sulla sola base della circostanza che si tratta di un procedimento speciale, a contraddittorio eventuale e differito, circoscritto ad ipotesi di reato di lieve entità; 
    che neppure l'esistenza di altri riti speciali potrebbe rendere ragione della disciplina del procedimento per decreto, dal momento che negli altri riti la rinuncia al contraddittorio nella formazione della prova è frutto (come nel patteggiamento) di una scelta dell'imputato, mentre nel decreto penale la specialità del rito e la conseguente elusione del contraddittorio vengono subiti dall'imputato, che può reagire solo dopo la condanna, proponendo opposizione; 
    che la recente previsione dell'avviso della conclusione delle indagini preliminari, stabilito a pena di nullità del decreto di citazione a giudizio (artt. 415-bis e 552 cod. proc. pen.), determinerebbe infine un'evidente disparità di trattamento tra gli imputati che vengono tratti a giudizio nelle forme ordinarie e quelli nei cui confronti è emesso decreto penale di condanna. 
    Considerato che il rimettente dubita, in riferimento agli artt. 3, 24 e 111 della Costituzione, della legittimità costituzionale dell'art. 459 del codice di procedura penale, nella parte in cui non prevede che prima di chiedere al giudice per le indagini preliminari l'emissione del decreto di condanna il pubblico ministero debba fare notificare all'indagato l'avviso di cui all'art. 415-bis cod. proc. pen.;  
    che il rimettente lamenta che la disciplina censurata determina un'irragionevole disparità di trattamento tra l'imputato nei cui confronti è emesso il decreto di condanna, al quale è assicurata «esclusivamente una tutela successiva che può concretamente attuarsi soltanto con il rimedio dell'opposizione», e l'imputato tratto a giudizio con decreto di citazione, destinatario dell'avviso di cui all'art. 415-bis cod. proc. pen.; sacrifica il diritto di difesa dell'imputato, che può essere «esercitato solo in sede di opposizione, quando ormai il decreto, che è sostanzialmente una sentenza di condanna, è stato emesso»; consente che l'imputato venga a conoscenza dell'accusa a suo carico solamente all'atto della notificazione della pronuncia di condanna, in contrasto con il diritto ad essere informato nel più breve tempo possibile "della natura e dei motivi dell'accusa elevata a suo carico" (art. 111, terzo comma, Cost.); consente che si addivenga ad una condanna esclusivamente sulla base del materiale di investigazione del pubblico ministero e che l'imputato esprima il consenso alla formazione della prova su tale materiale solo dopo la pronuncia di condanna, violando così sia il principio del contraddittorio nella formazione della prova, sia il principio che demanda alla legge di regolare i casi in cui, per consenso dell'imputato, la formazione della prova non ha luogo nel contraddittorio (art. 111, quarto e quinto comma, Cost.); 
    che, precedentemente alla modifica dell'art. 111 Cost., questa Corte, nell'affrontare analoghe questioni di legittimità costituzionale sollevate in riferimento agli artt. 3 e 24 Cost., ha affermato che la specificità del procedimento monitorio, configurato come rito a contraddittorio eventuale e differito, improntato a criteri di economia processuale e di massima speditezza, non si pone in contrasto né con il principio di eguaglianza né con il diritto di difesa; 
    che, in particolare, la Corte ha osservato che, stante la peculiarità del procedimento per decreto, «l'esigenza di garantire la conoscenza dell'indagine [...] si trasferisce [...] sulla fase processuale, conseguente all'esercizio dell'opposizione, operando il decreto solo quale mezzo di contestazione dell'accusa definitiva [...], che è essenziale per garantire il diritto di difesa», e che «il decreto penale costituisce una decisione preliminare, soggetta a opposizione, cosicché l'esperimento dei mezzi di difesa, con la stessa ampiezza dei procedimenti ordinari, si colloca nel vero e proprio giudizio che segue all'opposizione» (v. ordinanza n. 432 del 1998 ed i precedenti ivi menzionati, nonché le successive ordinanze n. 325, n. 326 e n. 458 del 1999); 
    che, proprio in relazione alla mancata previsione dell'avviso di cui all'art. 415-bis cod. proc. pen., questa Corte, con riferimento a questioni di costituzionalità concernenti la disciplina del giudizio immediato, ha di recente ribadito che le forme di esercizio del diritto di difesa possono essere modulate in relazione alle caratteristiche dei singoli procedimenti speciali, rilevando che l'avviso si porrebbe in antinomia con i presupposti che giustificano la costruzione di tale rito alla stregua di criteri di massima celerità e semplificazione, senza il filtro dell'udienza preliminare, «analogamente agli altri procedimenti speciali - giudizio direttissimo e decreto penale di condanna - nei quali, per ragioni diverse, non è previsto l'avviso di conclusione delle indagini» (ordinanza n. 203 del 2002); 
    che l'innesto della disciplina dell'avviso di conclusione delle indagini nel procedimento monitorio ne snaturerebbe la struttura e le finalità, inserendovi una procedura incidentale che potrebbe determinare una notevole dilatazione temporale, e si sostanzierebbe in una garanzia che, oltre ad essere costituzionalmente non imposta, si rivelerebbe del tutto incongrua rispetto ai caratteri del rito speciale (per analoghe considerazioni v. ordinanza citata n. 432 del 1998); 
    che, in definitiva, le censure prospettate dal rimettente in relazione ai parametri di cui agli artt. 3 e 24 Cost. non contengono argomentazioni tali da indurre la Corte a discostarsi dalle conclusioni raggiunte nelle precedenti decisioni in materia; 
    che, quanto alle censure riferite all'art. 111 Cost. - premesso in via generale che il dettato del terzo comma non esclude che il diritto dell'indagato di essere informato nel più breve tempo possibile dei motivi dell'accusa possa essere variamente modulato dal legislatore ordinario in relazione ai singoli riti alternativi -, questa Corte ha avuto recentemente occasione di affermare che la disciplina del procedimento per decreto non si pone in contrasto con il secondo e terzo comma di tale norma, in quanto il decreto penale, al di là della denominazione formale di "decreto di condanna", costituisce una sorta di decisione "preliminare", destinata ad essere posta nel nulla ove sia proposta opposizione ed a svolgere in tale caso la mera funzione di informazione dei motivi dell'accusa (v. ordinanza n. 8 del 2003), sì da consentire all'imputato di operare la scelta tra la richiesta del giudizio immediato, del giudizio abbreviato ovvero dell'applicazione della pena; 
    che neppure è ravvisabile la dedotta violazione dell'art. 111, quarto e quinto comma, Cost., in quanto - a prescindere dal rilievo che la disciplina dell'avviso della conclusione delle indagini non interferisce né con il principio del contraddittorio nella formazione della prova, né con il principio che demanda alla legge di regolare i casi in cui la formazione della prova non ha luogo in contraddittorio per consenso dell'imputato - ove con l'atto di opposizione l'imputato chieda il giudizio immediato, la prova si formerà in dibattimento nel contraddittorio tra le parti, e i risultati delle indagini potranno essere utilizzati entro i limiti e nel rispetto delle regole che disciplinano in via generale i rapporti tra le fasi delle indagini preliminari e del giudizio; se, poi, l'imputato chiede il giudizio abbreviato o l'applicazione della pena, ovvero, non opponendosi, presta acquiescenza al decreto penale di condanna, mediante tali scelte manifesta anche il consenso alla utilizzazione degli atti di indagine raccolti dal pubblico ministero, non diversamente dagli altri casi in cui l'indagato, dopo essere venuto a conoscenza del procedimento a suo carico, opera la scelta di attivare i riti alternativi;  
    che la questione va pertanto dichiarata manifestamente infondata in relazione a tutti i parametri evocati dal rimettente.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 459 del codice di procedura penale, sollevata, in riferimento agli artt. 3, 24 e 111 della Costituzione, dal Tribunale di Cosenza, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 16 gennaio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 4 febbraio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
