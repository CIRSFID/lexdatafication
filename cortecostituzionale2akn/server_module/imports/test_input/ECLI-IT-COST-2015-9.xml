<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2015/9/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2015/9/"/>
          <FRBRalias value="ECLI:IT:COST:2015:9" name="ECLI"/>
          <FRBRdate date="26/01/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="9"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2015/9/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2015/9/ita@/!main"/>
          <FRBRdate date="26/01/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2015/9/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2015/9/ita@.xml"/>
          <FRBRdate date="26/01/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="30/01/2015" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2015</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CRISCUOLO</cc:presidente>
        <cc:relatore_pronuncia>Giuliano Amato</cc:relatore_pronuncia>
        <cc:data_decisione>26/01/2015</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alessandro CRISCUOLO; Giudici : Paolo Maria NAPOLITANO, Giuseppe FRIGO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 6, comma 2, e 7, commi 1 e 8, lettere d), e), g) ed i), nonché dell'art. 8, commi 1 e 2, della legge della Regione Abruzzo 4 gennaio 2014, n. 5 (Interventi regionali per la promozione delle attività di cooperazione allo sviluppo e partenariato internazionale), promosso dal Presidente del Consiglio dei ministri con ricorso notificato in data 11 marzo 2014, depositato nella cancelleria della Corte il 18 marzo 2014 ed iscritto al n. 24 del registro ricorsi 2014.
 	Udito nella camera di consiglio del 14 gennaio 2015 il Giudice relatore Giuliano Amato.
 Ritenuto che il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso, in riferimento all'art. 117, secondo 
 comma, lettera a), della Costituzione, questioni di legittimità costituzionale degli artt. 6, comma 2, e 7, commi 1 e 8, lettere d), e), g) ed i), nonché dell'art. 8, commi 1 e 2, della legge della Regione Abruzzo 4 gennaio 2014, n. 5 (Interventi regionali per la promozione delle attività di cooperazione allo sviluppo e partenariato internazionale);
 che la parte ricorrente denuncia in primo luogo che l'art. 6, comma 2, della richiamata legge regionale, laddove prevede che gli interventi regionali in materia di cooperazione allo sviluppo si attuino per mezzo di «iniziative proprie» della Regione, progettate, predisposte e realizzate «anche» avvalendosi della collaborazione dei soggetti territoriali nazionali, sarebbe invasivo della competenza esclusiva dello Stato in materia di politica estera, ponendosi in contrasto con l'art. 117, secondo comma, lettera a), Cost., in quanto incide nella politica estera nazionale, che è prerogativa esclusiva dello Stato;
 che viene inoltre denunciata l'illegittimità dell'art. 7, commi 1 e 8, lettere d), e), g) ed i), della medesima legge regionale n. 5 del 2014, in quanto lesivo dell'art. 117, secondo comma, lettera a), Cost.;
 che in particolare le disposizioni censurate - nel prevedere un potere di determinazione regionale degli obiettivi di cooperazione solidale e degli interventi di emergenza, nonché dei destinatari dei benefici sulla base di criteri fissati dalla stessa Regione - incidono, ad avviso della ricorrente, nella materia della cooperazione internazionale allo sviluppo e sono parimenti invasive della competenza esclusiva dello Stato in materia di politica estera, in aperto contrasto con l'art. 117, secondo comma, lettera a), Cost.;
 che infine l'art. 8, commi 1 e 2, della stessa legge regionale n. 5 del 2014, violerebbe l'art. 117, secondo comma, lettera a), Cost., poiché - nel prevedere interventi d'urgenza e di protezione civile da realizzare con modalità deliberate dalla Giunta regionale - si porrebbe in contrasto con la richiamata competenza esclusiva dello Stato in materia di politica estera;
 che con atto depositato il 5 agosto 2014, l'Avvocatura dello Stato ha dato atto che con legge regionale 28 aprile 2014, n. 28 (Modifiche alla legge regionale del 4 gennaio 2014, n. 5 - Interventi regionali per la promozione delle attività di cooperazione allo sviluppo e partenariato internazionale), è stata disposta l'abrogazione del comma 2 dell'art. 6 della legge reg. Abruzzo n. 5 del 2014 e la modificazione degli artt. 7 e 8 della medesima legge, nel senso indicato dal Governo; la parte ricorrente ha ritenuto che, pertanto, siano venute meno le ragioni per proseguire il giudizio di costituzionalità, non sussistendo più l'interesse alla decisione sul ricorso; ed invero, con delibera del 10 luglio 2014, il Consiglio dei ministri ha dichiarato di rinunciare all'impugnazione della legge della Regione Abruzzo n. 5 del 2014;
 che la Regione Abruzzo non si è costituita nell'ambito del presente giudizio.
 Considerato che il Presidente del Consiglio dei ministri ha promosso, in riferimento all'art. 117, secondo comma, lettera a), della Costituzione, questione di legittimità costituzionale degli artt. 6, comma 2, e 7, commi 1 e 8, lettere d), e), g) ed i), nonché dell'art. 8, commi 1 e 2, della legge della Regione Abruzzo 4 gennaio 2014, n. 5 (Interventi regionali per la promozione delle attività di cooperazione allo sviluppo e partenariato internazionale);
 che la Regione Abruzzo non si è costituita;
 che, nelle more del giudizio, è entrata in vigore la legge regionale 28 aprile 2014, n. 28 (Modifiche alla legge regionale 4 gennaio 2014, n. 5 - Interventi regionali per la promozione delle attività di cooperazione allo sviluppo e partenariato internazionale), la quale ha disposto  l'abrogazione del comma 2 dell'art. 6 della legge della Regione Abruzzo n. 5 del 2014 e la modificazione degli artt. 7 e 8 della medesima legge, nel senso indicato dal Governo;
 che con atto depositato il 5 agosto 2014, l'Avvocatura dello Stato - dato atto del venir meno delle ragioni che avevano indotto alla proposizione del ricorso - ha dichiarato di rinunciare all'impugnativa;
 che, in mancanza di costituzione in giudizio della Regione resistente, l'intervenuta rinuncia al ricorso determina, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (ex plurimis, ordinanze n. 246, n. 103 e n. 34 del 2014, n. 164 e n. 55 del 2013).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara l'estinzione del processo.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 26 gennaio 2015.&#13;
 F.to:&#13;
 Alessandro CRISCUOLO, Presidente&#13;
 Giuliano AMATO, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 30 gennaio 2015.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
