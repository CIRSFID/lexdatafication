<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/265/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/265/"/>
          <FRBRalias value="ECLI:IT:COST:2008:265" name="ECLI"/>
          <FRBRdate date="07/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="265"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/265/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/265/ita@/!main"/>
          <FRBRdate date="07/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/265/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/265/ita@.xml"/>
          <FRBRdate date="07/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/07/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>07/07/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 593, comma 2, del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), promosso con ordinanza del 13 giugno 2006 dalla Corte d'appello di Messina nel procedimento penale a carico di G.G. ed altra, iscritta al n. 304 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 18, prima serie speciale, dell'anno 2007. 
      Udito nella camera di consiglio del 21 maggio 2008 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che con l'ordinanza in epigrafe la Corte d'appello di Messina ha sollevato, in riferimento agli artt. 3, 111, secondo comma, e 112 della Costituzione, questione di legittimità costituzionale dell'art. 593, comma 2, del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui preclude al pubblico ministero la possibilità di appellare contro le sentenze di proscioglimento»; 
    che la Corte rimettente premette di essere investita dell'appello proposto dal pubblico ministero, prima dell'entrata in vigore della legge n. 46 del 2006, avverso la sentenza di non luogo a procedere «per non aver commesso il fatto» e perché «il fatto non sussiste» emessa dal Giudice per le indagini preliminari, in funzione di Giudice dell'udienza preliminare, del Tribunale di Messina; 
    che, ai sensi dell'art. 10 della medesima legge, l'appello dovrebbe essere dichiarato inammissibile con ordinanza non impugnabile; 
    che, nel merito, la Corte d'appello lamenta la lesione degli artt. 3, 111, secondo comma, e 112 Cost.; 
    che, in particolare, la disciplina censurata, privando il pubblico ministero e l'imputato della possibilità di proporre appello avverso le sentenze di proscioglimento, solo apparentemente soddisferebbe l'esigenza di parità garantita dall'art. 111 Cost., atteso che, per un verso, i limiti all'appello delle sentenze di proscioglimento assumono «preponderanza e rilievo centrale» solo per il pubblico ministero (poiché già in precedenza all'imputato era inibito l'appello di sentenze di proscioglimento con formula piena) e che, per l'altro, solo l'organo della pubblica accusa ha interesse ad impugnare tali sentenze;  
    che la previsione della possibilità di appello nel caso disciplinato dall'art. 603, comma 2, cod. proc. pen. non consentirebbe di ritenere superabili i prospettati dubbi di costituzionalità, trattandosi di «ipotesi praticamente inattuabile», in quanto legata alla sopravvenienza di prove decisive nel ristretto lasso temporale tra la pronuncia della sentenza di primo grado e la scadenza del termine per appellare; 
    che la disparità che in tal modo si determina fra il pubblico ministero, cui è impedito l'appello delle sentenze di proscioglimento, e l'imputato, abilitato ad appellare le sentenze di condanna, non troverebbe giustificazione alcuna nella tutela di altri interessi costituzionalmente rilevanti (in particolare, né in esigenze di accelerazione dell'iter processuale né nella particolare posizione istituzionale del pubblico ministero); 
    che la scelta legislativa sarebbe, inoltre, censurabile sul piano della ragionevolezza, in quanto è stato conservato in capo al pubblico ministero il potere di proporre appello avverso le sentenze di condanna, «al solo scopo di ottenere una pena diversa da quella comminata»; 
    che, infine, la Corte d'appello rimettente ritiene violato anche il principio dell'obbligatorietà dell'azione penale: consapevole dell'orientamento della giurisprudenza costituzionale che, dopo la sentenza n. 177 del 1971, ha sempre negato che il potere di impugnazione del pubblico ministero costituisca estrinsecazione dell'azione penale, il giudice a quo invoca un mutamento di indirizzo da parte della Corte che tenga conto delle prerogative e delle attribuzioni istituzionali del pubblico ministero, come disciplinate dagli artt. 73 e 74 delle norme sull'ordinamento giudiziario e richiamate dagli artt. 102, 107, 108 e 112 Cost. 
    Considerato che la Corte d'appello di Messina dubita, in riferimento agli artt. 3, 111, secondo comma, e 112 della Costituzione, della legittimità costituzionale dell'art. 593, comma 2, del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui preclude al pubblico ministero la possibilità di appellare contro le sentenze di proscioglimento»; 
    che l'art. 593 cod. proc. pen. disciplina, al comma 2, l'appello del pubblico ministero e dell'imputato avverso le sentenze dibattimentali di proscioglimento, stabilendo - per effetto delle modifiche introdotte dall'art. 1 della legge n. 46 del 2006 ed immediatamente applicabili in forza dell'art. 10 della medesima legge - che l'appello è consentito solo nell'ipotesi di cui all'art. 603, comma 2, cod. proc. pen., se la nuova prova è decisiva;  
    che dalla stessa ordinanza di rimessione risulta che la Corte rimettente è investita dell'appello proposto dal pubblico ministero avverso la sentenza di non luogo a procedere pronunciata, ai sensi dell'art. 425 cod. proc. pen., dal Giudice per le indagini preliminari, in funzione di Giudice dell'udienza preliminare; 
    che il regime di impugnazione delle sentenze di non luogo a procedere è disciplinato dall'art. 428 cod. proc. pen. (sostituito dall'art. 4 della legge n. 46 del 2006); 
    che, dunque, la Corte rimettente sottopone a scrutinio di costituzionalità una norma (l'art. 593 cod. proc. pen.) di cui non deve fare applicazione nel giudizio a quo; 
    che l'inesatta indicazione della norma oggetto di censura (aberratio ictus) comporta, per costante giurisprudenza di questa Corte, la manifesta inammissibilità della questione (ex plurimis, ordinanze n. 79 e 150 del 2008).  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi  &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
     dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 593, comma 2, del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), sollevata, in riferimento agli artt. 3, 111, secondo comma, e 112 della Costituzione, dalla Corte d'appello di Messina, con l'ordinanza in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 luglio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 10 luglio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
