<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/22/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/22/"/>
          <FRBRalias value="ECLI:IT:COST:2008:22" name="ECLI"/>
          <FRBRdate date="28/01/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="22"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/22/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/22/ita@/!main"/>
          <FRBRdate date="28/01/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/22/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/22/ita@.xml"/>
          <FRBRdate date="28/01/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="08/02/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>28/01/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 12 della legge 3 ottobre 2001, n. 366 (Delega al Governo per la riforma del diritto societario) e degli articoli da 2 a 17 del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'art. 2 della legge 3 ottobre 2001, n. 366), promosso dal giudice relatore del Tribunale di Napoli, nel procedimento civile vertente tra R. M. e il San Paolo Banco di Napoli s.p.a. ed altro, con ordinanza del 18 maggio 2006 iscritta al n. 378 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 21, prima serie speciale, dell'anno 2007. 
    Udito nella camera di consiglio del 12 dicembre 2007 il Giudice relatore Francesco Amirante. 
    Ritenuto che, nel corso un giudizio promosso da un privato nei confronti di un istituto di credito per la dichiarazione di nullità di alcuni documenti di investimento in titoli azionari, il Giudice relatore del Tribunale di Napoli ha sollevato, in riferimento all'art. 76 della Costituzione, questione di legittimità costituzionale dell'art. 12 della legge 3 ottobre 2001, n. 366 (Delega al Governo per la riforma del diritto societario), «nella parte in cui, in relazione al giudizio ordinario di primo grado in materia societaria, non indica i principi e criteri direttivi che avrebbero dovuto guidare le scelte del legislatore delegato» e, «per derivazione», degli articoli da 2 a 17 del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'art. 2 della legge 3 ottobre 2001, n. 366); 
    che il giudice a quo ricorda, innanzitutto, che l'art. 76 Cost. stabilisce che la delega delle funzioni legislative al Governo non può avvenire se non con la determinazione di principi e criteri direttivi, per un tempo limitato e con definizione dell'oggetto; 
    che, dopo aver trascritto il testo dell'impugnato art. 12 ed averne estrapolato i principi e criteri direttivi, il remittente osserva come, con tale disposizione, il legislatore si sia limitato ad indicare le materie nelle quali il Governo poteva intervenire, l'obiettivo di rendere più rapida ed efficace la definizione dei procedimenti, il divieto di modificare la competenza per territorio e materia, la tendenziale collegialità del procedimento, la possibilità di valutare l'atteggiamento delle parti in sede di tentativo di conciliazione e di dettare regole atte a favorire la riduzione dei termini e la concentrazione del procedimento; 
    che siffatta delega, però, «non ha indicato, con sufficiente determinazione, i principi ed i criteri direttivi» ai quali il legislatore si sarebbe dovuto attenere, in quanto l'art. 12 in esame non ha fornito alcuna indicazione in ordine allo schema processuale da adottare, sicché il legislatore è stato lasciato libero di creare un nuovo modello processuale che esula completamente dallo schema del procedimento ordinario disciplinato dal codice di procedura civile; 
    che il legislatore delegato, quindi, «in forza di una delega assolutamente carente sotto il profilo dell'indicazione di criteri direttivi, ha potuto creare una disciplina interamente nuova per il processo societario di cognizione», così prefigurando ed anticipando, in pratica, il nuovo rito ordinario quale risulta dal testo della Commissione ministeriale per la riforma del processo civile; 
    che proprio tali connotati della legge delega fanno sì che essa sia in contrasto con il parametro costituzionale invocato, il che impone – ad avviso del giudice a quo – di sollevare questione di legittimità costituzionale dell'art. 12 della legge n. 366 del 2001 e, «per derivazione», degli articoli da 2 a 17 del d. lgs. n. 5 del 2003; 
    che detta questione sarebbe rilevante perché dall'esito della decisione di questa Corte dipende l'applicabilità dell'intera disciplina impugnata alla controversia in corso; 
    che, in via subordinata, il Giudice relatore del Tribunale di Napoli ha sollevato d'ufficio, sempre in riferimento all'art. 76 Cost., questione di legittimità costituzionale degli articoli da 2 a 17 del decreto legislativo n. 5 del 2003, «perché difformi dai principi e criteri direttivi dettati dalla legge di delega n. 366 del 2001»; 
    che il remittente rileva come, per evitare il sospetto di incostituzionalità della legge delega, questa debba necessariamente essere interpretata nel senso che il legislatore delegante, indicando il principio di «concentrazione del procedimento», abbia fatto riferimento alle scansioni previste nel processo ordinario, che si svolge attraverso la successione di più udienze fisse ed obbligatorie (artt. 180, 183, 184, 189 c.p.c.), sicché «la generica indicazione del legislatore delegante del principio di “riduzione dei termini processuali”» avrebbe dovuto (e potuto) essere riempita di contenuto dal legislatore delegato solo attraverso la riduzione dei termini previsti nel giudizio di cognizione ordinario; 
    che solo una simile lettura, «estremamente riduttiva e per questo proposta in via subordinata rispetto all'altra, dei principi fissati dal legislatore delegante» sarebbe idonea a fugare i sospetti di illegittimità costituzionale dell'art. 12 della legge n. 366 del 2001, risultando però evidente, in questo modo, l'illegittimità costituzionale degli articoli da 2 a 17 del d. lgs. n. 5 del 2003, emanati eccedendo i limiti della delega contenuta nell'art. 12 della legge n. 366 del 2001; 
    che anche detta questione, secondo il remittente, sarebbe rilevante, per le stesse ragioni indicate nella motivazione della questione proposta in via principale. 
    Considerato che questa Corte – già investita del vaglio di costituzionalità di identiche questioni, sollevate dal medesimo giudice – ne ha dichiarato la manifesta inammissibilità, escludendo (considerate le modalità e le argomentazioni con le quali sono state prospettate) l'asserito nesso di subordinazione logico-giuridica della seconda rispetto alla prima ed affermando, invece, la radicale contraddizione tra l'interpretazione “subordinata”, esposta dal remittente a sostegno della legittimità della legge di delega (da esso compiutamente argomentata e quasi “suggerita” alla Corte), e la diversa lettura della medesima norma premessa alla questione “principale” (ordinanze n. 209 e n. 360 del 2006, n. 70 e n. 343 del 2007);  
    che anche le presenti questioni (sollevate in modo identico alle precedenti) presentano gli stessi difetti di prospettazione, in quanto il remittente non solo non adempie l'obbligo di ricercare un'interpretazione costituzionalmente orientata di ciascuna delle norme impugnate, ma propone, nel medesimo contesto motivazionale, due opzioni ermeneutiche sostanzialmente alternative, così inammissibilmente demandando alla Corte la scelta fra queste; 
    che, inoltre, va anche richiamato l'indirizzo già espresso da questa Corte (si veda la sentenza n. 321 del 2007) secondo il quale la legittimazione del giudice relatore a sollevare questioni di legittimità costituzionale, nella specifica situazione dei giudizi assoggettati al nuovo rito societario riservati alla competenza del collegio, incontra precisi limiti; 
    che la questione, pertanto, è manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 12 della legge 3 ottobre 2001, n. 366 (Delega al Governo per la riforma del diritto societario), e, «per derivazione», degli articoli da 2 a 17 del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'art. 2 della legge 3 ottobre 2001, n. 366), sollevate, in riferimento all'art. 76 della Costituzione, dal Giudice relatore del Tribunale di Napoli con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 28 gennaio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Francesco AMIRANTE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria l'8 febbraio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
