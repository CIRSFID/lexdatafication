<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/56/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/56/"/>
          <FRBRalias value="ECLI:IT:COST:2008:56" name="ECLI"/>
          <FRBRdate date="10/03/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="56"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/56/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/56/ita@/!main"/>
          <FRBRdate date="10/03/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/56/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/56/ita@.xml"/>
          <FRBRdate date="10/03/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/03/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>10/03/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'articolo 7-quater, commi 1 e 2, del decreto-legge 31 gennaio 2005, n. 7 (Disposizioni urgenti per l'università e la ricerca, per i beni e le attività culturali, per il completamento di grandi opere strategiche, per la mobilità dei pubblici dipendenti, e per semplificare gli adempimenti relativi a imposte di bollo e tasse di concessione, nonché altre misure urgenti), introdotto, in sede di conversione, dalla legge 31 marzo 2005, n. 43, promossi dal Tribunale di Roma in due procedimenti civili vertenti, rispettivamente, tra l'Azienda Policlinico Umberto I e l'Alse Medica s.r.l., e la stessa Azienda e la Kemihospital s.p.a. ed altri, con ordinanze del 5 maggio e del 3 luglio 2006, iscritte ai nn. 403 e 620 del registro ordinanze 2007 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 22 e 36, prima serie speciale, dell'anno 2007. 
    Visti gli atti di costituzione dell'Azienda Policlinico Umberto I; 
    udito nella camera di consiglio del 13 febbraio 2008 il Giudice relatore Francesco Amirante. 
    Ritenuto che, nel corso di due giudizi di opposizione all'esecuzione, il Tribunale di Roma, con due ordinanze di identico contenuto, ha sollevato, in riferimento agli articoli 3, primo comma, 24, 25, 97, 101, 102, 104 e 113, secondo comma, della Costituzione, questione di legittimità costituzionale dell'articolo 7-quater, commi 1 e 2, del decreto-legge 31 gennaio 2005, n. 7 (Disposizioni urgenti per l'università e la ricerca, per i beni e le attività culturali, per il completamento di grandi opere strategiche, per la mobilità dei pubblici dipendenti, e per semplificare gli adempimenti relativi a imposte di bollo e tasse di concessione, nonché altre misure urgenti), introdotto, in sede di conversione, dalla legge 31 marzo 2005, n. 43; 
    che la disposizione censurata appare al remittente in contrasto con i richiamati parametri costituzionali in quanto, stabilendo l'inefficacia, nei confronti dell'Azienda ospedaliera Policlinico Umberto I, di titoli – quali, appunto, i decreti ingiuntivi non opposti – «ormai coperti dal giudicato», violerebbe alcuni «elementari principi di civiltà giuridica», come il principio di ragionevolezza, quello di eguaglianza, il principio di rispetto delle funzioni del potere giudiziario e quello della difesa giurisdizionale dei diritti e degli interessi, sicuramente lesi da una norma in grado di incidere retroattivamente sul giudicato; 
    che si è costituita, in entrambi i giudizi, l'Azienda Policlinico Umberto I, chiedendo che la questione venga dichiarata inammissibile per difetto di rilevanza e/o non fondata nel merito; 
    che è anche intervenuto, in tutti e due i giudizi, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per la non fondatezza della questione. 
    Considerato che il Tribunale di Roma solleva, in entrambi i giudizi, questione di legittimità costituzionale dell'art. 7-quater, commi 1 e 2, del decreto-legge 31 gennaio 2005, n. 7 (Disposizioni urgenti per l'università e la ricerca, per i beni e le attività culturali, per il completamento di grandi opere strategiche, per la mobilità dei pubblici dipendenti, e per semplificare gli adempimenti relativi a imposte di bollo e tasse di concessione, nonché altre misure urgenti), introdotto, in sede di conversione, dalla legge 31 marzo 2005, n. 43; 
    che i giudizi, pertanto, possono essere riuniti e decisi con un unico provvedimento; 
    che questa Corte, con la sentenza n. 364 del 2007, successiva alle ordinanze indicate in epigrafe, ha dichiarato l'illegittimità costituzionale dell'intero art. 7-quater attualmente censurato, ravvisandone il contrasto con gli artt. 3, 24, 102 e 113 Cost.; 
    che, pertanto, alla luce di tale intervento – da ritenere, a tutti gli effetti, come ius superveniens – è necessario disporre la restituzione degli atti al giudice a quo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti al Tribunale di Roma. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 marzo 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Francesco AMIRANTE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 13 marzo 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
