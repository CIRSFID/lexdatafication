<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/1998/343/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/1998/343/"/>
          <FRBRalias value="ECLI:IT:COST:1998:343" name="ECLI"/>
          <FRBRdate date="14/07/1998" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="343"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/1998/343/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/1998/343/ita@/!main"/>
          <FRBRdate date="14/07/1998" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/1998/343/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/1998/343/ita@.xml"/>
          <FRBRdate date="14/07/1998" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/07/1998" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>1998</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GUIZZI</cc:presidente>
        <cc:relatore_pronuncia>Fernanda Contri</cc:relatore_pronuncia>
        <cc:data_decisione>14/07/1998</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: prof. Francesco GUIZZI; Giudici: prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>Ordinanza</docType>nel giudizio di legittimità costituzionale dell'art. 51 della legge 12 febbraio 1968, n. 132 (Enti ospedalieri e assistenza ospedaliera), promosso con ordinanza emessa il 15 aprile 1997 dal Consiglio di Stato sul ricorso proposto da Guarnieri Giovanbattista ed altri contro il Ministero della sanità ed altra, iscritta al n. 790 del registro ordinanze 1997 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 47, prima serie speciale, dell'anno 1997. 
Visto l'atto di intervento del Presidente del Consiglio dei Ministri, 
Udito nella camera di consiglio del 22 aprile il giudice relatore Fernanda Contri. 
Ritenuto che, nel corso di un giudizio amministrativo di appello promosso da Giovanbattista Guarnieri ed altri rappresentanti legali di case di cura private, il Consiglio di Stato, Sezione IV, ha sollevato, in riferimento agli artt. 41, 42, 87, 117 e 118 della Costituzione, questione di legittimità costituzionale dell'art. 51 della legge 12 febbraio 1968, n. 132 (Enti ospedalieri e assistenza ospedaliera), che attribuiva al Ministro della sanità il potere di regolare, con proprio decreto, i requisiti tecnici e costruttivi - nonché le attrezzature - delle case di cura private, l'ordinamento dei servizi e del personale delle medesime, i requisiti necessari per l'esercizio in esse della funzione di direttore sanitario responsabile; 
che gli appellanti nel procedimento a quo avevano precedentemente impugnato, davanti al TAR del Lazio, il regolamento emanato, sulla base della disposizione denunciata, con decreto del Ministro della sanità del 5 agosto 1977 (Determinazione dei requisiti tecnici sulle case di cura private); 
che con sentenza n. 334 del 1979, il TAR del Lazio respingeva il ricorso proposto da Guarnieri ed altri per chiedere l'annullamento del citato decreto ministeriale che, fra l'altro, faceva obbligo alle case di cura private già autorizzate di adeguarsi alla nuova normativa da esso recata entro otto anni dalla data della sua pubblicazione, a pena di revoca dell'autorizzazione; 
che, secondo il collegio rimettente, la sopravvenienza di una complessa normativa in materia di ordinamento regionale non aveva fatto venir meno la competenza regolamentare del Ministro della sanità, giacché, dapprima, l'art. 6, numero 6, del d.P.R. 14 gennaio 1972, n. 4 (Trasferimento alle Regioni a statuto ordinario delle funzioni amministrative statali in materia di assistenza sanitaria ed ospedaliera e dei relativi personali ed uffici) - emanato in forza della delega conferita al Governo dall'art. 17 della legge n. 281 del 1970 (Provvedimenti finanziari per l'attuazione delle Regioni a statuto ordinario) - riservava allo Stato "la normativa tecnica relativa alle case di cura private", successivamente, l'art. 43 della legge 23 dicembre 1978, n. 833 (Istituzione del servizio sanitario nazionale), rimetteva alla legislazione regionale la disciplina e la vigilanza sulle case di cura private stabilendo, tuttavia, al quarto comma, che nel frattempo resta in vigore, tra gli altri, l'art. 51 della legge n. 132 del 1968 e l'impugnato regolamento ministeriale; 
che nell'ordinanza di rimessione si ricorda che l'emanazione, in base all'art. 5 della legge n. 833 del 1978, del d.P.C.m. 27 giugno 1986 (Atto di indirizzo e coordinamento dell'attività amministrativa delle regioni in materia di requisiti delle case di cura private), recante una nuova disciplina dell'intera materia, "non sembra avere implicato una sopravvenuta causa di improcedibilità del ricorso per implicita intervenuta tacita abrogazione dell'impugnato regolamento, stante il permanere dell'interesse dei ricorrenti al suo annullamento, quanto meno con riferimento agli effetti prodotti nel periodo di relativa vigenza", ciò che comporterebbe la perdurante rilevanza della questione sollevata; 
che, sotto il profilo della non manifesta infondatezza della questione prospettata, secondo la Sezione rimettente, il regolamento ministeriale del 5 agosto 1977 - emanato sulla base della disposizione denunciata - ha inciso, "sotto il profilo statico, sul modo di godimento della proprietà delle case di cura e, sotto un profilo dinamico, sull'attività imprenditoriale che attraverso le case di cura viene esercitata", in contrasto con le riserve di legge di cui, rispettivamente, agli artt. 42, secondo comma, e 41 della Costituzione; 
che l'art. 51 della legge n. 132 del 1968 viene denunciato anche in riferimento agli artt. 117 e 118 della Costituzione, in quanto la materia assegnata alla potestà regolamentare del Ministro è tra quelle riservate alla potestà legislativa della regionale; 
che, in riferimento all'art. 87, quinto comma, della Costituzione, infine, il giudice a quo ritiene non manifestamente infondata la questione di legittimità costituzionale dell'art. 51 della legge n. 132 del 1968, non sembrando, a suo avviso, la posizione costituzionale dei Ministri "compatibile con la competenza ad emanare norme regolamentari di interesse generale, aventi efficacia nell'ordinamento complessivo dello Stato"; 
che nel giudizio davanti a questa Corte, ha spiegato intervento il Presidente del Consiglio dei Ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, per dedurre l'inammissibilità e l'infondatezza della questione sollevata dalla IV Sezione del Consiglio di Stato. 
Considerato che, secondo la giurisprudenza di questa Corte, sull'autorità rimettente grava l'onere di indicare i concreti elementi della fattispecie sottoposta al suo esame e di esporre le argomentazioni che fondano il giudizio di rilevanza: onere particolarmente rigoroso qualora le norme oggetto di censura siano abrogate o comunque modificate (v. le ordinanze nn. 79 del 1988 e 419 del 1977); 
che l'ordinanza di rimessione, emessa il 15 aprile 1997, a distanza di oltre sedici anni dal deposito del ricorso in appello, non offre elementi descrittivi della fattispecie concreta e della posizione, originaria ed attuale, delle case di cura ricorrenti nel giudizio a quo - le quali non si sono costituite nel presente giudizio - ulteriori rispetto al mero accenno al ricorso in appello a suo tempo proposto contro la sentenza del TAR del Lazio, con la quale è stato rigettato il ricorso proposto dai rappresentanti legali delle case di cura avverso il decreto ministeriale 5 agosto del 1977, e rispetto alla apodittica affermazione secondo la quale l'ipotetico accoglimento della questione di legittimità costituzionale comporterebbe l'annullamento ex tunc del decreto ministeriale emanato sulla base della disposizione impugnata, "con integrale soddisfazione dell'interesse dai ricorrenti dedotto"; 
che pertanto risulta del tutto carente la motivazione della perdurante rilevanza della questione sollevata. 
Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
Dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 51 della legge 12 febbraio 1968, n. 132 (Enti ospedalieri e assistenza ospedaliera), sollevata, in riferimento agli artt. 41, 42, 87, 117 e 118 della Costituzione, dal Consiglio di Stato, Sezione IV, con l'ordinanza indicata in epigrafe. &#13;
</p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 14 luglio 1998. &#13;
Il Presidente: Guizzi &#13;
Il redattore: Contri &#13;
Il cancelliere: Fruscella &#13;
Depositata in cancelleria il 24 luglio 1998. &#13;
Il cancelliere: Fruscella</block>
    </conclusions>
  </judgment>
</akomaNtoso>
