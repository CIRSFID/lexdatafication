<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/80/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/80/"/>
          <FRBRalias value="ECLI:IT:COST:2016:80" name="ECLI"/>
          <FRBRdate date="08/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="80"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/80/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/80/ita@/!main"/>
          <FRBRdate date="08/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/80/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/80/ita@.xml"/>
          <FRBRdate date="08/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/04/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Silvana Sciarra</cc:relatore_pronuncia>
        <cc:data_decisione>08/03/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giuseppe FRIGO, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 13 e 14, comma 9, della legge della Regione Veneto 28 novembre 2014, n. 37 (Istituzione dell'Agenzia veneta per l'innovazione nel settore primario), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 3-10 febbraio 2015, depositato in cancelleria il 10 febbraio 2015 ed iscritto al n. 21 del registro ricorsi 2015.
 Visto l'atto di costituzione della Regione Veneto;  
 udito nell'udienza pubblica dell'8 marzo 2016 il Presidente Paolo Grossi in luogo e con l'assenso del Giudice relatore Silvana Sciarra.
 Ritenuto che, con ricorso spedito per la notificazione il 3 febbraio 2015 e depositato il successivo 10 febbraio, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni principali di legittimità costituzionale degli artt. 13 e 14, comma 9, della legge della Regione Veneto 28 novembre 2014, n. 37 (Istituzione dell'Agenzia veneta per l'innovazione nel settore primario), in riferimento all'art. 97, primo e terzo (recte: secondo e quarto) comma, della Costituzione, ai «generali principi» dettati, anche «in materia di progressione di carriera», dal decreto legislativo 30 marzo 2001, n. 165 (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche), all'art. 35 dello stesso decreto legislativo e all'art. 1, comma 563, della legge 27 dicembre 2013, n. 147 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2014);
 che il ricorrente ha premesso che l'art. 1 della legge della Regione Veneto n. 37 del 2014 ha istituito l'Agenzia veneta per l'innovazione nel settore primario, definendola «ente strumentale della Regione del Veneto, dotata di personalità giuridica di diritto pubblico» (comma 1), ed ha previsto che essa, «Nei limiti delle funzioni proprie, individuate all'articolo 2, [...] subentra nei rapporti giuridici attivi e passivi della Azienda regionale Veneto Agricoltura» (comma 3), la quale viene, al contempo, «soppressa e posta in liquidazione nei tempi e secondo le modalità previste dalla presente legge» (comma 2);
 che lo stesso ricorrente ha evidenziato che tale Azienda regionale Veneto Agricoltura era definita dall'art. 1, comma 3, della legge della Regione Veneto 5 settembre 1997, n. 35 (Istituzione dell'azienda regionale per i settori agricolo, forestale e agro-alimentare "Veneto Agricoltura"), istitutiva della stessa, «Ente di diritto pubblico economico dotato di personalità giuridica propria», con la conseguenza che, a fronte della soppressione di un ente così qualificato, viene costituito un ente pubblico strumentale della Regione, il quale, a norma dell'art. 1, comma 2, del d.lgs. n. 165 del 2001, è, al pari di questa, una pubblica amministrazione;
 che, passando ad illustrare il contenuto delle disposizioni impugnate, il Presidente del Consiglio dei ministri ha rappresentato che l'art. 13 della legge regionale n. 37 del 2014, sotto la rubrica «Norme transitorie», regola le sorti del personale già in servizio nell'Azienda regionale Veneto Agricoltura, stabilendo che «Il personale in servizio nella soppressa Azienda regionale Veneto Agricoltura, che risulti in possesso dei requisiti richiesti dalla vigente normativa, è inquadrato nella qualifica funzionale del contratto collettivo nazionale di lavoro del comparto regioni-autonomie locali corrispondente a quella occupata» (comma 1); e che «Il restante personale in servizio, non in possesso dei requisiti di cui al comma 1, sino alla data di cessazione, mantiene il contratto di lavoro in essere e, per quanto attiene al trattamento economico si avrà riguardo al contratto collettivo nazionale di lavoro del comparto regioni-autonomie locali» (comma 2);
 che, quanto all'altra disposizione impugnata dell'art. 14, comma 9, della legge regionale n. 37 del 2014, essa stabilisce che «Le funzioni della soppressa Azienda non attribuite all'Agenzia e non oggetto di dismissione sono esercitate dalle competenti strutture della Giunta regionale, cui vengono assegnate le corrispondenti risorse strumentali ed umane »;
 che, ad avviso del ricorrente, dalle due disposizioni impugnate si evince che il personale della soppressa Azienda regionale Veneto Agricoltura sarà, per una parte, chiamato a svolgere le funzioni dell'Agenzia veneta per l'innovazione nel settore primario presso tale ente pubblico di nuova istituzione e, per altra parte, assegnato alle competenti strutture della Giunta regionale per svolgervi le funzioni, già di competenza della detta Azienda, non attribuite all'Agenzia e non oggetto di dismissione;
 che, secondo il Presidente del Consiglio dei ministri, l'art. 13 della legge regionale n. 37 del 2014, violerebbe, anzitutto, l'art. 97, secondo e quarto comma, Cost., nonché «i generali principi» del d.lgs. n. 165 del 2001 «che ne rappresentano l'attuazione», perché, stabilendo implicitamente il passaggio del personale già in servizio nella soppressa Azienda regionale Veneto Agricoltura, ente pubblico economico, nella dotazione organica dell'Agenzia veneta per l'innovazione nel settore primario, ente strumentale della Regione Veneto, sulla base del mero riscontro del «possesso dei requisiti richiesti dalla vigente normativa», fa accedere tale personale ad un impiego in un'amministrazione pubblica senza concorso;
 che, in secondo luogo, la stessa impugnata disposizione contrasterebbe con l'art. 97, secondo e quarto comma, Cost., anche perché, non limitando l'inquadramento nelle qualifiche funzionali del contratto collettivo nazionale di lavoro del comparto Regioni-autonomie locali da essa previsto al personale già in servizio nella soppressa Azienda regionale sulla base di un contratto di lavoro a tempo indeterminato, potrebbe essere diretta alla stabilizzazione anche del personale precario, senza il previo superamento, da parte di questo, di un concorso;
 che, in terzo luogo, l'art. 13 della legge regionale n. 37 del 2014 violerebbe l'art. 97, secondo e quarto comma, Cost., nonché i «principi generali dettati in materia di progressione di carriera» dal d.lgs. n. 165 del 2001 «che ne rappresentano l'attuazione», perché, in difetto di una puntuale e predefinita corrispondenza tra le qualifiche funzionali dei rilevanti comparti di contrattazione collettiva, consentirebbe inquadramenti in qualifiche superiori a quella già rivestita dal lavoratore nella soppressa Azienda regionale senza il previo superamento, da parte dello stesso, di un concorso;
 che la medesima impugnata disposizione contrasterebbe, in quarto luogo, con l'art. 35 del d.lgs. n. 165 del 2001, il quale, in attuazione dell'art. 97 Cost., prevede che l'assunzione nelle amministrazioni pubbliche può avvenire solo mediante procedure concorsuali o, nei soli casi in esso previsti, particolari procedure selettive, nonché con l'osservanza, in ogni caso, dei principi di imparzialità, parità di genere, pubblicità e trasparenza;
 che l'art. 13 della legge regionale n. 37 del 2014 si porrebbe in contrasto, infine, anche con l'art. 1, comma 563, della legge n. 147 del 2013, atteso che il divieto, da questo dettato, secondo cui la mobilità di personale tra le società controllate direttamente o indirettamente dalle pubbliche amministrazioni o dai loro enti strumentali «non può [...] avvenire tra le [dette] società [...] e le pubbliche amministrazioni» (terzo periodo), deve ritenersi applicabile, stante l'identità di ratio, anche al passaggio di personale da un ente pubblico economico (quale la soppressa Azienda regionale Veneto Agricoltura) alle pubbliche amministrazioni (quale è l'Agenzia veneta per l'innovazione nel settore primario);
 che, secondo il Presidente del Consiglio dei ministri, l'art. 14, comma 9, della legge regionale n. 37 del 2014, sarebbe affetto da vizi analoghi a quelli dell'art. 13 della stessa legge;
 che detta impugnata disposizione violerebbe, in specie, l'art. 97, secondo e quarto comma, Cost., «i principi stabiliti nel D.Lgs. n. 165 del 2001 (in particolare [...] al già richiamato art. 35)», nonché l'art. 1, comma 563, terzo periodo, della legge n. 147 del 2013, perché, stabilendo l'assegnazione alle competenti strutture della Giunta regionale delle risorse umane necessarie all'esercizio delle funzioni della soppressa Azienda regionale Veneto Agricoltura non attribuite all'Agenzia veneta per l'innovazione nel settore primario e non dismesse in sede di liquidazione di questa e, quindi, implicitamente, il passaggio automatico e riservato di personale dell'anzidetta Azienda, ente pubblico economico, alle dipendenze della Regione, nei ruoli della medesima, fa accedere tale personale ad un impiego nell'amministrazione pubblica senza concorso;
 che, sempre ad avviso del ricorrente, l'impugnato art. 14, comma 9, lederebbe l'art. 97 Cost. e il d.lgs. n. 165 del 2001, anche perché, qualora avesse inteso comprendere nel menzionato passaggio di personale già in servizio nella soppressa Azienda regionale Veneto Agricoltura anche quello precario, determinerebbe la stabilizzazione di questo senza il previo superamento, da parte dello stesso, di un concorso;
 che, con atto depositato il 16 marzo 2015, si è costituita nel giudizio la Regione Veneto, chiedendo che le questioni proposte siano dichiarate inammissibili o, comunque, infondate;
 che, in prossimità della pubblica udienza, la Regione Veneto ha depositato una memoria illustrativa con la quale, nel ribadire le difese esposte nel proprio atto di costituzione in giudizio, ha rappresentato che, successivamente alla proposizione del ricorso, le disposizioni impugnate sono state modificate dai commi 4 e 5 dell'art. 57 della legge della Regione Veneto 27 aprile 2015, n. 6 (Legge di stabilità regionale per l'esercizio 2015);
 che, quanto all'impugnato art. 13, il comma 4 dell'art. 57 della legge regionale n. 6 del 2015, senza cambiarne la rubrica, lo ha sostituito con il seguente: «Ferma restando l'attuale consistenza organica, il personale in servizio nella soppressa Azienda regionale Veneto Agricoltura mantiene il contratto di lavoro in essere e, per quanto riguarda le dinamiche contrattuali, segue il contratto collettivo nazionale di lavoro del comparto regioni-autonomie locali» (comma 1); «L'Agenzia veneta per l'innovazione nel settore primario, concorre al contenimento della spesa pubblica, osservando le medesime disposizioni di riduzione della spesa applicabili alla Regione» (comma 2);
 che, quanto all'impugnato art. 14, comma 9, il comma 5 dell'art. 57 della legge regionale n. 6 del 2015 ha previsto la sostituzione delle parole «risorse umane e strumentali» con le seguenti: «risorse strumentali ed umane, previa partecipazione, per queste ultime, a concorso pubblico nell'ambito della programmazione annuale del personale», di tal ché, a seguito della citata modificazione, la disposizione impugnata stabilisce attualmente che «Le funzioni della soppressa Azienda non attribuite all'Agenzia e non oggetto di dismissione sono esercitate dalle competenti strutture della Giunta regionale, cui vengono assegnate le corrispondenti risorse strumentali ed umane, previa partecipazione, per queste ultime, a concorso pubblico nell'ambito della programmazione annuale del personale»;
 che la difesa della Regione Veneto ha inoltre evidenziato che l'art. 57 della legge regionale n. 6 del 2015 ha modificato anche un'altra disposizione della legge regionale n. 37 del 2014 rilevante ai fini dello scrutinio delle promosse questioni costituita dal comma 1 dell'art. 1;
 che tale comma è stato modificato dal comma 1 del detto art. 57, il quale ha sostituito le parole «ente strumentale» con le parole «ente pubblico economico strumentale», di tal ché, a seguito della citata modificazione, l'art. 1, comma 1, della legge regionale n. 37 del 2014, stabilisce attualmente che «È istituita l'Agenzia veneta per l'innovazione nel settore primario, di seguito denominata Agenzia, quale ente pubblico economico strumentale della Regione del Veneto, dotata di personalità giuridica di diritto pubblico e di autonomia amministrativa, organizzativa, contabile e patrimoniale, nei limiti previsti dalla presente legge»;
 che, secondo la Regione Veneto, le menzionate sopravvenienze legislative determinerebbero, prima ancora della completa elisione delle ragioni di censura avanzate dal ricorrente, la cessazione della materia del contendere, in quanto le disposizioni impugnate, oltre ad essere state modificate in senso satisfattivo delle dette ragioni, non avrebbero avuto applicazione nel periodo compreso tra la loro entrata in vigore e la loro  modificazione ad opera dell'art. 57 della legge regionale n. 6 del 2015;
 che il 1° marzo 2016 il Presidente del Consiglio dei ministri ha depositato atto di rinuncia al ricorso, approvata dal Consiglio dei ministri nella seduta del 26 febbraio 2016 con la motivazione che le dette sopravvenienze normative e la produzione, da parte della Regione Veneto, di documentazione comprovante la mancata applicazione medio tempore delle disposizioni impugnate avevano fatto venire meno le ragioni dell'impugnazione;
 che, con atto depositato il 7 marzo 2016, la Regione Veneto ha depositato atto di accettazione della rinuncia al ricorso, autorizzata dalla Giunta regionale con deliberazione del 3 marzo 2016.
 Considerato che il Presidente del Consiglio dei ministri ha promosso questioni principali di legittimità costituzionale degli artt. 13 e 14, comma 9, della legge della Regione Veneto 28 novembre 2014, n. 37 (Istituzione dell'Agenzia veneta per l'innovazione nel settore primario), in riferimento all'art. 97, primo e terzo (recte: secondo e quarto) comma, della Costituzione, ai «generali principi» dettati, anche «in materia di progressione di carriera», dal decreto legislativo 30 marzo 2001, n. 165 (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche), all'art. 35 dello stesso decreto legislativo e all'art. 1, comma 563, della legge 27 dicembre 2013, n. 147 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2014);
 che, previa delibera del Consiglio dei ministri del 26 febbraio 2016, il ricorrente ha rinunciato al ricorso;
 che la Regione Veneto ha accettato tale rinuncia;
 che, ai sensi dell'art. 23, delle norme integrative per i giudizi davanti alla Corte costituzionale, nei giudizi di legittimità costituzionale in via principale, la rinuncia al ricorso, qualora sia accettata dalla parte costituita, determina l'estinzione del processo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p/>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso </block>
    </conclusions>
  </judgment>
</akomaNtoso>
