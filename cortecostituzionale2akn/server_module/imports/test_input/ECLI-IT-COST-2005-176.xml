<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2005/176/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2005/176/"/>
          <FRBRalias value="ECLI:IT:COST:2005:176" name="ECLI"/>
          <FRBRdate date="02/05/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="176"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2005/176/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2005/176/ita@/!main"/>
          <FRBRdate date="02/05/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2005/176/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2005/176/ita@.xml"/>
          <FRBRdate date="02/05/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="04/05/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>CONTRI</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>02/05/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Fernanda CONTRI; Giudici: Piero Alberto CAPOTOSTI, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 31 gennaio 2001 relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal senatore Giulio Andreotti nei confronti del dott. Mario Almerighi, promosso con ricorso del Giudice per l'udienza preliminare del Tribunale di Perugia, notificato il 23 gennaio 2003, depositato in Cancelleria il 12 febbraio 2003 ed iscritto al n. 1 del registro dei conflitti 2003. 
    Visto l'atto di costituzione del Senato della Repubblica; 
    udito nell'udienza pubblica dell'8 marzo 2005 il Giudice relatore Francesco Amirante; 
    udito l'avvocato Giuseppe Morbidelli per il Senato della Repubblica.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Con ricorso del 5 ottobre 2001 il Giudice per l'udienza preliminare del Tribunale di Perugia ha proposto conflitto di attribuzione nei confronti del Senato della Repubblica in riferimento alla delibera del 31 gennaio 2001 (documento IV-quater, n. 59) con la quale si è affermato che le opinioni espresse dal senatore Giulio Andreotti nei confronti del dott. Mario Almerighi e divulgate dalla stampa e da alcune televisioni – per le quali pende procedimento penale a carico del senatore, per il reato di diffamazione aggravata, a seguito di querela sporta dal dott. Almerighi – costituiscono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e sono, pertanto, insindacabili alla luce dell'art. 68, primo comma, della Costituzione. 
    Osserva il Giudice ricorrente che, a seguito di una deposizione testimoniale resa dal dott. Almerighi davanti al Tribunale di Palermo in occasione di un processo penale a carico del senatore Andreotti, quest'ultimo aveva compiuto una serie di dichiarazioni – diffuse da alcuni quotidiani e da alcune televisioni in data 25, 26 ottobre e 4 novembre 1999 – in conseguenza delle quali era stato querelato dal dott. Almerighi. Instauratosi il relativo procedimento penale per diffamazione aggravata, era intervenuta la delibera del Senato sopra menzionata, impugnata dal ricorrente al momento dell'udienza preliminare. 
    Secondo il G.u.p. di Perugia, infatti, la prerogativa dell'insindacabilità non sarebbe stata legittimamente invocata dal Senato in considerazione dell'assoluta estraneità del comportamento tenuto dal senatore Andreotti rispetto ai concetti di “opinione” e di “esercizio delle funzioni” proprie dei componenti del Parlamento. Pur essendo indubbio che il processo penale svoltosi a Palermo ha avuto una serie di importanti ripercussioni sulla vita politica nazionale, tuttavia ciò non consente, secondo il ricorrente, di collegare le dichiarazioni rese dal senatore con una qualche forma di attività parlamentare; nella vicenda, infatti, il senatore non avrebbe fatto altro che esprimere le proprie opinioni personali, senza alcun collegamento con la funzione svolta.  
    La giurisprudenza costituzionale, invece, ha da tempo identificato il perno della prerogativa dell'insindacabilità nel c.d. nesso funzionale tra le dichiarazioni e l'attività parlamentare, richiedendo che vi sia un'identità sostanziale tra l'opinione espressa nell'esercizio delle funzioni e quella manifestata all'esterno (sentenze n. 10, n. 11, n. 56, n. 58 e n. 82 del 2000), prescindendo anche dalla sede in cui quest'ultima sia stata divulgata. Nel caso specifico, invece, le dichiarazioni del senatore Andreotti, oltre a contenere «valutazioni prettamente soggettive» che comportano l'attribuzione di un reato ben preciso, ossia quello di falsa testimonianza (“Almerighi ha detto infamie”, “si tratta di un falso testimone” etc.), si risolverebbero anche nell'emissione di un giudizio di colpevolezza, che certamente spetta all'Autorità giudiziaria (“la verità è emersa in modo molto netto e questo scredita la testimonianza di Almerighi”). È evidente, secondo il ricorrente, che la prerogativa di cui all'art. 68, primo comma, Cost. come non si estende fino a coprire gli insulti, non può neppure riguardare dichiarazioni che, risolvendosi nell'attribuzione di illeciti penali, non possono certamente essere espressione della funzione parlamentare.  
    Le dichiarazioni, tra l'altro, oltre a caratterizzarsi per la pesantezza di alcune espressioni (“Almerighi è pazzo, dica quello che vuole. Mi procura solo divertimento”), farebbero anche emergere un interesse del tutto personale (e patrimoniale) del senatore Andreotti verso la vicenda, interesse sicuramente estraneo alla funzione parlamentare (“magari mi fa un'azione penale: mi farebbe guadagnare forse qualche cosa in sede civile”, “potrei guadagnare qualcosa in sede civile e ciò non guasterebbe”). 
    Dall'insieme di tutte queste considerazioni emerge chiaramente, secondo il G.u.p. del Tribunale di Perugia, che la delibera di insindacabilità assunta dal Senato si risolve in un'estensione abusiva della garanzia prevista dalla Costituzione, oltre che in una lesione del principio costituzionale di autonomia della magistratura e del fondamentale principio di uguaglianza. Il ricorrente, quindi, chiede a questa Corte di dichiarare che non spetta al Senato della Repubblica la valutazione della condotta attribuita al senatore Andreotti, con conseguente annullamento della delibera in oggetto. 
    2.— Il conflitto così proposto è stato giudicato ammissibile con ordinanza n. 4 del 2003, regolarmente notificata al Senato in data 23 gennaio 2003; il ricorrente ha poi provveduto al deposito presso la cancelleria di questa Corte il successivo 12 febbraio 2003. 
    3.— A seguito della notifica si è costituito il Senato della Repubblica, chiedendo che il conflitto proposto venga dichiarato inammissibile o comunque respinto nel merito. 
    Il Senato ricorda, prima di tutto, che la vicenda in questione è stata al centro di un ampio dibattito parlamentare, svoltosi prima davanti alla Giunta per le immunità e poi in aula, nel corso del quale lo stesso senatore Andreotti si è opposto «al diniego della domanda di autorizzazione a procedere». Dal dibattito è emerso che la Procura della Repubblica di Palermo aveva centrato la propria tesi accusatoria sul convincimento che il senatore, attraverso il legame col giudice Corrado Carnevale, avesse esercitato la sua influenza in favore di alcuni noti componenti dell'associazione mafiosa: di qui «l'evidenza del collegamento tra le dichiarazioni, oggetto della querela contro il senatore Andreotti, ed una specifica attività parlamentare». 
    Ciò posto, nella memoria difensiva si osserva che il ricorso è privo di fondamento, perché i pretesi interventi del senatore presso il giudice Carnevale sono stati oggetto di ampio dibattito parlamentare, nel quale l'interessato ha dimostrato la propria totale estraneità, sicché le dichiarazioni contro il comportamento del dott. Almerighi non sarebbero altro che «il seguito ed anzi il doveroso sviluppo di quanto affermato già dal senatore Andreotti in aula». 
    Oltre a ciò, il Senato rileva che, nel valutare la posizione del senatore Andreotti, non si può trascurare il fatto che egli è un senatore a vita; le sue opinioni, quindi, dovrebbero essere valutate non solo con riguardo alla funzione parlamentare, ma anche in relazione alla sua «investitura per meriti eccezionali che, in estrema sintesi, radica un rapporto di rappresentanza con la Patria intera, ed anzi di espressione qualificatissima della Patria». Tale particolare posizione imporrebbe la ricomprensione nella tutela dell'art. 68 Cost. anche delle «forme di autotutela volte ad affermare la perdurante legittimità della sua investitura», con conseguente riconoscimento di una tutela “rinforzata” della posizione del senatore a vita. 
    Il Senato, pertanto, chiede a questa Corte di dichiarare che il fatto oggetto del procedimento penale concerne opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, ricadendo in tal modo nell'ambito della prerogativa di cui all'art. 68, primo comma, della Costituzione. 
    4.— In data 22 febbraio 2005 il Senato della Repubblica, richiamandosi ai documenti depositati il precedente 18 febbraio 2005, ha presentato una memoria nella quale, ribadendo una serie di considerazioni già svolte, ha insistito nell'accoglimento delle prospettate conclusioni. 
    Nella memoria il Senato sostiene di aver fatto un uso corretto della prerogativa dell'insindacabilità di cui all'art. 68, primo comma, della Costituzione, essendo indubbia la connessione tra gli atti di critica e quelli di funzione parlamentare. Nella specie, attesa l'enorme rilevanza politica del processo penale svoltosi a Palermo nei confronti del senatore Andreotti, il Senato ebbe ad occuparsi della vicenda nel corso di varie sedute, nelle quali il senatore imputato sostenne sempre la propria assoluta estraneità ai fatti a lui contestati. Nel corso di tale dibattito egli criticò anche aspramente i calunniatori, osservando come il processo contro di lui fosse animato da intenti denigratori nei suoi confronti. 
    I fatti oggetto del presente conflitto derivano dalla testimonianza resa dal dott. Almerighi nel citato processo palermitano, testimonianza che, secondo la difesa del Senato, sarebbe stata smentita dalle successive verifiche processuali, come poi evidenziato nelle sentenze di primo e di secondo grado emesse dal Tribunale e dalla Corte d'appello di Palermo. La conseguente reazione del sen. Andreotti «non è altro che il seguito, ed anzi il doveroso sviluppo di quanto già affermato in aula», sicché le affermazioni riportate dai giornali e dalla televisione non sarebbero altro che un modo per ribadire la propria totale estraneità ai fatti contestati. 
    Il Senato, infine, dopo aver richiamato le più recenti sentenze costituzionali in argomento, ricorda che la nozione di nesso funzionale si presta ad un'intrinseca flessibilità e che, comunque, l'art. 68, primo comma, Cost. dovrebbe essere letto in modo da consentire «al parlamentare la piena libertà di espressione».Considerato in diritto1.–– Il Giudice per l'udienza preliminare del Tribunale di Perugia ha sollevato conflitto di attribuzioni tra poteri dello Stato in relazione alla deliberazione del Senato della Repubblica del 31 gennaio 2001 (doc. IV-quater, n. 59), con la quale l'Assemblea ha approvato la proposta della Giunta per le autorizzazioni a procedere di dichiarare che i fatti per i quali pende procedimento penale nei confronti del senatore a vita Giulio Andreotti davanti al ricorrente concernono opinioni espresse dal suddetto quale membro del Parlamento e ricadono pertanto nell'ipotesi di cui all'art. 68, primo comma, della Costituzione. 
    Il ricorrente espone in fatto che nell'intervista rilasciata all'ANSA il 25 ottobre 1999, riportata da alcuni quotidiani e da alcune emittenti televisive nello stesso giorno o in quello successivo, e nell'intervista rilasciata al settimanale “L'Espresso” del 4 novembre 1999, il senatore aveva affermato la falsità della deposizione resa nel processo a suo carico dal magistrato Mario Almerighi relativamente ad un asserito colloquio telefonico tra il senatore stesso ed il Ministro della giustizia dell'epoca, nel quale il primo avrebbe invitato l'altro a non far nulla contro il magistrato Corrado Carnevale, nei cui confronti era stato presentato un esposto al Consiglio superiore della magistratura. Nelle menzionate interviste il senatore aveva affermato che, se anche altri avevano detto il falso, era molto grave che a rendere dichiarazioni false fosse un magistrato, sicché della vicenda doveva essere investito il Consiglio superiore della magistratura; che l'Almerighi doveva essere pazzo e che in sede civile sarebbe stato condannato a risarcirgli il danno.  
    Almerighi aveva proposto querela e contro il senatore si procedeva per il reato di diffamazione a mezzo stampa.  
    Secondo il ricorrente le dichiarazioni suindicate non sono legate da nesso funzionale con l'attività parlamentare del senatore a vita, ma costituiscono espressione di un interesse privato di quest'ultimo. 
    Il Senato della Repubblica si è costituito sostenendo l'infondatezza della tesi del ricorrente, in quanto le dichiarazioni incriminate non sarebbero altro che la specificazione di quanto affermato dal senatore Andreotti nel dibattito parlamentare avente ad oggetto la concessione dell'autorizzazione a procedere a suo carico, cui era seguito il processo davanti al Tribunale di Palermo, nel corso del quale il dott. Almerighi aveva reso le deposizioni qualificate false dal senatore. Secondo la difesa del Senato, inoltre, in considerazione del titolo d'investitura dei senatori a vita, il nesso funzionale delle loro opinioni con l'attività parlamentare deve essere valutato con maggiore larghezza rispetto ai criteri seguiti relativamente ai deputati e ai senatori eletti. 
    2.–– Il ricorso per conflitto di attribuzione è fondato. 
    Questa Corte, nel delimitare i confini dell'immunità spettante ai parlamentari in virtù dell'art. 68, primo comma, della Costituzione, ha da tempo adottato il criterio del c.d. nesso funzionale che deve legare le opinioni espresse dai componenti delle Camere all'attività parlamentare, affermando l'insufficienza di un contesto politico cui esse possano riferirsi, contesto che, su un diverso piano, può rilevare in sede di merito. 
    D'altro canto, è stato ritenuto che la qualifica di attività parlamentare non sia subordinata al suo estrinsecarsi necessariamente in atti tipici della funzione o alla sua localizzazione e che sono quindi coperte dall'immunità anche le divulgazioni all'esterno del Parlamento di opinioni espresse nello svolgimento di attività qualificabili come parlamentari (cfr. tra le più recenti, sentenze n. 10 e n. 11 del 2000, n. 120, n. 246, n. 298, n. 347, n. 348 del 2004, n. 28 del 2005). 
    La tesi della difesa del Senato, secondo la quale l'immunità di cui all'art. 68, primo comma, della Costituzione avrebbe, in riferimento ai senatori a vita, un'ampiezza maggiore di quella attribuita ai parlamentari eletti non trova riscontro in alcuna norma costituzionale, né essa può essere desunta dalla asserita maggior importanza del titolo d'investitura dei senatori a vita rispetto a quello dei senatori eletti, sicché è alla stregua dei criteri appena menzionati che deve essere accertata e valutata l'esistenza degli elementi di fatto alla quale è subordinato il riconoscimento dell'immunità. 
    La difesa del Senato ha prodotto numerosi atti del processo svoltosi davanti agli organi giudiziari palermitani a carico, tra gli altri, del senatore Andreotti, e nel quale è risultata la sua non colpevolezza; ha prodotto, inoltre, il verbale delle sedute del Senato del 13 maggio e del 29 luglio 1993 e i verbali delle deposizioni del dott. Mario Almerighi del 22 luglio 1993, del 3 dicembre 1994 e del 9 giugno 1997, nonché la dichiarazione spontanea del senatore Andreotti. Tra tali atti, la difesa del Senato indica l'intervento di quest'ultimo in sede di discussione in Assemblea, nella seduta del 13 maggio 1993, sul rilascio dell'autorizzazione a procedere, come l'atto parlamentare del quale le esternazioni del senatore Andreotti del 1999, oggetto dell'imputazione, rappresenterebbero la mera divulgazione. 
    La tesi non può essere accolta. 
    Le dichiarazioni del senatore in Assemblea, cui si appella la difesa del Senato, per il loro contenuto, valutato anche in considerazione dell'epoca in cui furono rese, sono tali da escludere che le affermazioni fatte poi nelle interviste in questione ne rappresentino la mera divulgazione. 
    Nelle dichiarazioni suindicate, di oltre sei anni precedenti le interviste, non è mai nominato il dott. Almerighi mentre, oltre un generico riferimento alle “invenzioni di pentiti”, ricorre più volte il nome del “pentito” Marino Mannoia. Occorre, inoltre, osservare che delle tre deposizioni del querelante, i cui verbali sono stati prodotti dalla difesa del Senato, due sono successive alle dichiarazioni del senatore in sede parlamentare e neppure l'altra, soltanto di una settimana precedente, vi è menzionata. Nè si può quindi ritenere che il senatore Andreotti, nel rivendicare davanti all'Assemblea non soltanto la propria innocenza, ma anche i propri meriti politici nella lotta alla criminalità organizzata, quando tacciava di falsità coloro che lo accusavano si sia riferito seppure implicitamente anche al dott. Almerighi. 
    Dalle considerazioni esposte consegue il rilievo della inesistenza di qualsivoglia collegamento tra le dichiarazioni per le quali il senatore a vita è imputato e le opinioni da lui espresse in sede parlamentare. 
    Si deve pertanto concludere che non spetta al Senato della Repubblica affermare, nella vicenda in questione, che le dichiarazioni rese dal senatore Giulio Andreotti costituiscono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e sono, pertanto, insindacabili alla luce dell'art. 68, primo comma, della Costituzione. 
    La delibera di insindacabilità deve essere, pertanto, annullata.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara che non spetta al Senato della Repubblica affermare che le opinioni espresse dal senatore Giulio Andreotti per le quali pende procedimento penale a suo carico, per il reato di diffamazione aggravata, davanti al Giudice per l'udienza preliminare del Tribunale di Perugia, costituiscono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
    annulla, per l'effetto, la delibera di insindacabilità adottata dal Senato della Repubblica nella seduta del 31 gennaio 2001 (documento IV-quater, n. 59). &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 2 maggio 2005. &#13;
F.to: &#13;
Fernanda CONTRI, Presidente &#13;
Francesco AMIRANTE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 4 maggio 2005. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
