<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2010/47/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2010/47/"/>
          <FRBRalias value="ECLI:IT:COST:2010:47" name="ECLI"/>
          <FRBRdate date="08/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="47"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2010/47/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2010/47/ita@/!main"/>
          <FRBRdate date="08/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2010/47/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2010/47/ita@.xml"/>
          <FRBRdate date="08/02/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/02/2010" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2010</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>DE SIERVO</cc:presidente>
        <cc:relatore_pronuncia>Alessandro Criscuolo</cc:relatore_pronuncia>
        <cc:data_decisione>08/02/2010</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'articolo 372 del codice penale, promosso dal Tribunale di Trento nel procedimento penale a carico di V. P., con ordinanza del 9 maggio 2008, iscritta al n. 81 del registro ordinanze 2009 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 12, prima serie speciale, dell’anno 2009.
 Visto l’atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 13 gennaio 2010 il Giudice relatore Alessandro Criscuolo.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.— Il Tribunale di Trento in composizione monocratica, con ordinanza depositata il 9 maggio 2008, ha sollevato, in riferimento agli articoli 3 e 27 della Costituzione «e in relazione al principio di ragionevolezza», questione di legittimità costituzionale dell’articolo 372 del codice penale (falsa testimonianza), «laddove viene comminato il minimo edittale in anni due di reclusione, anziché in altra pena, di eguale specie, ma nella misura più bassa».
 Il rimettente premette di essere chiamato a decidere in un procedimento penale a carico di V. P., imputato del delitto di cui all’art. 372 cod. pen. perché, all’udienza del 25 ottobre 2006, dichiarava, contrariamente al vero, che N. D. non aveva partecipato al furto di quattro cerchioni di un’automobile.
 Il giudice a quo riferisce che la falsità delle dichiarazioni rese dall’imputato è emersa dall’istruttoria dibattimentale, in quanto un maresciallo dei Carabinieri aveva affermato di aver visto anche N. D. partecipare all’azione di impossessamento dei cerchioni, unitamente all’imputato V. P. (nei cui confronti il procedimento iscritto per il delitto di furto era stato definito ai sensi dell’art. 444 del codice di procedura penale, con sentenza divenuta irrevocabile al tempo della testimonianza).
 In sede di discussione il rappresentante del pubblico ministero aveva chiesto la condanna ad anni tre di reclusione, mentre la difesa aveva insistito per l’assoluzione.
 2.— Ciò premesso, il giudicante dubita della legittimità costituzionale del citato art. 372, «in relazione da un canto agli artt. 3 e 27 della Costituzione, dall’altro ai principi di ragionevolezza e di proporzione quantitativa che devono presiedere alla funzione legiferante in merito all’entità della pena criminale sanzionante gli illeciti penali».
 Egli afferma che la questione è rilevante perché «da essa dipende, nei confronti del giudicabile, l’applicazione in concreto della pena in esito alla istruzione probatoria dibattimentale svolta».
 3.— Inoltre, la questione, ad avviso del rimettente, non appare manifestamente infondata per più motivi.
 In primo luogo, egli pone in evidenza che con l’art. 11, comma 2, del decreto-legge 8 giugno 1992, n. 306 (Modifiche urgenti al nuovo codice di procedura penale e provvedimenti di contrasto alla criminalità mafiosa) convertito, con modificazioni, dalla legge 7 agosto 1992, n. 356, l’originaria pena detentiva per il delitto di falsa testimonianza, determinata nel minimo in sei mesi e nel massimo in tre anni di reclusione, è stata aumentata nel minimo a due anni e nel massimo a sei anni di reclusione, con la conseguenza che il minimo ed il massimo edittale sono stati, rispettivamente, quadruplicati e raddoppiati.
 Tale inasprimento sanzionatorio si pone, secondo il giudice a quo, in contrasto con il principio di proporzionalità, in quanto il minimo edittale ora previsto, costituendo pena inevitabile anche per le infrazioni più modeste, non è adeguato al disvalore del fatto tipico.
 L’attuale trattamento sanzionatorio, poi, è tale per cui il reato di falsa testimonianza è punito in modo più grave rispetto ad altre fattispecie criminose «considerate da sempre come ontologicamente similari» quali la frode processuale, il favoreggiamento personale, anche con riguardo all’aggravante di cui al capoverso dell’art. 378 cod. pen., e la simulazione di reato.
 Si tratta, ad avviso del rimettente, di fattispecie delittuose «che per lontana tradizione, sia la legge, che la dottrina e la giurisprudenza hanno reputato avvinte dalla identica oggettività giuridica, che si è detto risiedere nell’interesse dello Stato alla retta amministrazione della giustizia che deve quanto più possibile cogliere nel segno, nonché nella necessità diretta ad impedire che la giustizia possa essere indirizzata su falsa strada».
 L’ordinanza pone in rilievo che la pena prevista per il reato di falsa testimonianza è la stessa prevista per il reato di calunnia, connotato da maggiore gravità rispetto al primo, per la sussistenza non solo dell’aggressione all’interesse al corretto funzionamento della giustizia, cioè al medesimo interesse tutelato dal reato di cui all’art. 372 cod. pen., ma anche per la sussistenza della lesione alla libertà e all’onore della persona falsamente incolpata di una condotta criminosa.
 Il rimettente ritiene che un’altra ragione di «stridente irragionevolezza sostanziale» della pena prevista per il reato di falsa testimonianza risieda nel raffronto con quella prevista per il reato di favoreggiamento personale di cui all’art. 378 cod. pen., anche con riferimento all’ipotesi aggravata del capoverso, che sanziona il reo con la reclusione non inferiore nel minimo a due anni, prevedendo, nel massimo, la pena di quattro anni di reclusione, quando il delitto commesso è quello previsto dall’art. 416-bis cod. pen.
 Tale diversa previsione costituisce per il rimettente «una grave aporia della legge» perché colui che, come l’imputato del giudizio a quo, abbia mentito in ordine al furto di quattro cerchioni di una automobile è punito come o più gravemente di chi ha eluso le investigazioni (recte: abbia aiutato taluno ad eludere le investigazioni) dell’autorità in un delitto di mafia, vale a dire nella stragrande maggioranza dei casi in efferati delitti di sangue.
 Secondo il Tribunale, nel caso di specie difetta la necessaria proporzionalità tra disvalore del singolo fatto tipico e sanzioni edittali per esso comminate.
 Il rimettente rileva, poi, che, mentre nel reato di favoreggiamento la gravità edittale del delitto si distingue in relazione alla gravità del reato presupposto, ciò non accade per il delitto di falsa testimonianza in quanto la misura della pena è la stessa qualunque sia il reato in relazione al quale è dichiarato il falso, quindi, «sia se si riferisca ad un reato la cui posta è l’ergastolo, sia se riguardi una contravvenzione punita con la pena dell’ammenda».
 Con l’aumento del minimo edittale, dunque, il legislatore del 1992, ad avviso del giudice a quo, ha azzerato ogni rapporto tra misura della pena, offesa al bene giuridico e modalità di aggressione «appiattendo tutta la vasta gamma delle condotte possibili – dalle più gravi alle meno gravi – su un disvalore d’evento e d’azione rispetto ai quali non appare razionale una uniformità sanzionatoria “dal basso”».
 Per il rimettente ciò determina, nel quadro di una più attenta considerazione dell’art. 27 Cost., che è precluso al giudice l’esercizio concreto della discrezionalità vincolata di cui all’art. 133 cod. pen., essendogli interdetta l’opera di adeguamento della pena alle circostanze oggettive e soggettive del reato, sicché il minimo edittale si presenta «sperequato in eccesso rispetto a quella platea statisticamente estesa di soggetti agenti che non presentano le stigmate della personalità criminale».
 Sotto tale aspetto egli sostiene, infatti, che l’aumento del minimo edittale della pena per il delitto di falsa testimonianza è stato varato nell’ambito di una legislazione “emergenziale”, ad avviso del rimettente «ritagliata per una tipologia di persone ad alta valenza criminale» e, dunque, finalizzato a reprimere il mendacio nell’ambito del crimine organizzato, con la conseguenza che il minimo edittale in questione sarebbe modellato su «un tipo criminologico d’autore (cioè il mafioso e le carriere criminali) che non si può dire costituisca il proprium del delitto di falsa testimonianza».
 Il trattamento sanzionatorio del delitto di falsa testimonianza si pone, dunque, in contrasto con l’art. 27, terzo comma, Cost., perché l’irrogazione di pene sproporzionate al grado di effettivo disvalore dei fatti e alla personalità del reo compromette la finalità rieducativa della pena stessa.
 Il rimettente osserva, infine, che, coincidendo il minimo edittale di due anni con il limite di pena oltre il quale non è usufruibile la sospensione condizionale della pena ai sensi dell’art. 163 cod. pen., «è giocoforza lamentare che ciò comprime al massimo, di fatto quasi annullandolo, il margine di operatività dell’anzidetto beneficio, accordabile al reo primario che ne possa essere meritevole».
 4.— Con atto depositato in data 14 aprile 2009 è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall’Avvocatura generale dello Stato, che ha sostenuto la non fondatezza della questione.
 La difesa erariale ha, in primo luogo, osservato che l’inasprimento del trattamento sanzionatorio per il delitto di falsa testimonianza, introdotto dal d.l. n. 306 del 1992, convertito, con modificazioni, dalla legge n. 356 del 1992, non è stato dettato da motivazioni emergenziali, come affermato dal rimettente, quanto piuttosto dal mutamento del processo penale che, con l’entrata in vigore del nuovo codice di procedura, è divenuto di tipo accusatorio, con la conseguenza che la prova testimoniale ha assunto un ruolo primario. All’interesse per la veridicità della prova, dunque, ha fatto seguito l’innalzamento dei limiti edittali della pena per il delitto in questione.
 Pertanto, si deve escludere che sia stato violato l’art. 3 Cost. in base al rilievo che il minimo edittale previsto per il delitto di falsa testimonianza sarebbe irragionevole.
 Né la violazione del citato parametro può derivare dal diverso trattamento sanzionatorio previsto per altre figure criminose. Per quanto concerne il delitto di calunnia, la difesa erariale pone in rilievo che la pena della reclusione che va due a sei anni, cioè la stessa prevista per il delitto di falsa testimonianza, concerne le ipotesi meno gravi, mentre il trattamento sanzionatorio è fortemente inasprito per le violazioni più gravi.
 Quanto al confronto con il delitto di frode processuale, punito meno gravemente, la difesa erariale rileva che la ragione del diverso trattamento sanzionatorio risiede non solo nel dato che nel delitto di falsa testimonianza si trasgredisce un dovere di solidarietà sociale, ma anche nella considerazione secondo cui l’art. 374 cod. pen. è posto a tutela di mezzi di prova meno diffusi della prova testimoniale; inoltre, il delitto di frode processuale può essere commesso con meno facilità e minore speranza d’impunità rispetto a quello di cui all’art. 372 cod. pen.
 Anche con riferimento al delitto di favoreggiamento personale, punito meno gravemente, la difesa erariale osserva che soltanto con riferimento alla falsa testimonianza viene in rilievo un dovere di solidarietà sociale, la cui “sacralità” è sottolineata dalla previsione di specifiche formalità; inoltre, solo in quest’ultima fattispecie criminosa si realizza la violazione dell’interesse alla correttezza di un tipo di prova di assoluta importanza, qual è quella testimoniale.
 L’Avvocatura generale prosegue osservando che la questione sollevata dal rimettente non è fondata, anche con riferimento agli ulteriori profili di violazione degli artt. 3 e 27 Cost.
 Invero, il giudice può adeguare la pena alla scarsa gravità del fatto, concedendo le circostanze attenuanti generiche, così graduando la pena rispetto al disvalore del reato.
 La difesa erariale, infine, sostiene che l’inasprimento del trattamento sanzionatorio consente, comunque, la possibilità di applicare il beneficio della sospensione condizionale della pena alla luce delle possibili riduzioni di pena conseguenti alla scelta di dette attenuanti o di riti premiali.Considerato in diritto1.— Il Tribunale di Trento dubita della legittimità costituzionale, in riferimento agli articoli 3 e 27 della Costituzione, dell’articolo 372 del codice penale (falsa testimonianza) «laddove viene comminato il minimo edittale in anni due di reclusione, anziché in altra pena, di eguale specie, ma nella misura più bassa».
 Il rimettente, chiamato a decidere in un procedimento penale a carico di V. P., imputato del delitto di falsa testimonianza, perché, contrariamente a quanto emerso dall’istruttoria dibattimentale, dichiarava che N. D. non aveva partecipato al furto di quattro cerchioni di un’automobile, solleva il dubbio di legittimità costituzionale nei termini sopra indicati, ponendo in rilievo che la questione è rilevante in quanto «da essa dipende nei confronti del giudicabile, l’applicazione della pena in esito alla istruzione probatoria dibattimentale svolta».
 Ad avviso del giudice a quo il trattamento sanzionatorio previsto dall’art. 372 cod. pen. viola l’art. 3 Cost. in relazione ad una pluralità di profili.
 In primo luogo, esso si pone in contrasto con il principio di proporzionalità in quanto il minimo edittale, determinato in due anni di reclusione, non è adeguato al disvalore del fatto tipico, costituendo una pena inevitabile anche per le infrazioni più modeste.
 La disposizione censurata, poi, nel prevedere il citato minimo edittale viola il principio di ragionevolezza. Infatti, il reato di falsa testimonianza ha subito un inasprimento sanzionatorio introdotto dall’art. 11, comma 2, del decreto-legge 8 giugno 1992, n. 306 (Modifiche urgenti al nuovo codice di procedura penale e provvedimenti di contrasto alla criminalità mafiosa), convertito, con modificazioni, dalla legge 7 agosto 1992, n. 356.
 Per effetto di tale modifica, che ha sostituito l’originaria pena detentiva, stabilita nel minimo in sei mesi e nel massimo in tre anni di reclusione, il delitto de quo oggi è punito più gravemente di fattispecie assimilabili, come la frode processuale (art. 374 cod. pen.), il favoreggiamento personale (art. 378 cod. pen.) anche con riguardo all’ipotesi in cui il delitto al quale la condotta si riferisce è quello previsto e punito dall’art. 416-bis cod. pen., inoltre, la simulazione di reato (art. 367 cod. pen.).
 Secondo il rimettente, si tratta di tipi di reato caratterizzati da una identica oggettività giuridica consistente, così come per il delitto di falsa testimonianza, nell’interesse dello Stato alla retta amministrazione della giustizia. 
 In particolare, tra il delitto di cui all’art. 372 cod. pen. e quello di favoreggiamento personale, punito con la pena della reclusione fino a quattro anni e, nell’ipotesi aggravata, con la reclusione non inferiore a due anni, sussiste un irragionevole trattamento sanzionatorio in quanto costituisce «una grave aporia della legge» prevedere che chi, come l’imputato, abbia commesso una infrazione modesta è punito come o più gravemente di chi abbia aiutato taluno ad eludere le investigazione dell’autorità. 
 Un altro profilo di irragionevolezza risiede nella constatazione che, mentre in questa ultima fattispecie di reato la gravità edittale è graduata in relazione alla gravità del reato presupposto, ed è prevista la pena della multa nel caso in cui quest’ultimo è una contravvenzione, nel delitto di falsa testimonianza la misura della pena è unica qualunque sia il reato in relazione al quale è dichiarato il falso.
 Ad avviso del giudice a quo, poi, la disposizione denunziata viola l’art. 3 Cost. in quanto non è ragionevole punire il delitto di falsa testimonianza con la stessa pena prevista per il delitto di calunnia, connotato da maggiore gravità, in quanto offensivo non solo dell’interesse al corretto funzionamento dell’amministrazione della giustizia, ma anche dell’onore di colui che è falsamente incolpato di un reato.
 Infine, il minimo edittale previsto dalla disposizione censurata si pone in contrasto anche con l’art. 27, terzo comma, Cost., in particolare con la finalità rieducativa della pena, in quanto si presenta sperequato in eccesso rispetto a quei soggetti che «non presentano le stigmate della personalità criminale», non apparendo, pertanto, proporzionato al grado di effettivo disvalore dei fatti e alla personalità del reo.
 2.— La questione non è fondata.
 2.1.— Si deve premettere che l’art. 11, comma 2, del d.l. n. 306 del 1992, convertito, con modificazioni, dalla legge n. 356 del 1992, ha elevato la misura della sanzione prevista per il delitto di falsa testimonianza, sostituendo alla originaria pena stabilita, nel minimo, in sei mesi di reclusione e, nel massimo, in tre anni di reclusione, quella più grave determinata in due anni di reclusione nel minimo ed in sei anni di reclusione nel massimo. Ciò secondo valutazioni operate in sede legislativa sulla gravità del fatto, che ne hanno progressivamente accentuato l’antigiuridicità.
 La modifica normativa del trattamento sanzionatorio per il delitto in questione, come si rileva dai lavori parlamentari, si inquadra nell’ambito di una più ampia esigenza avvertita dal legislatore di provvedere alla «ristrutturazione» dei delitti contro l’attività giudiziaria, approntando specifici strumenti di tutela del corretto svolgimento delle indagini preliminari e per salvaguardare la genuinità della prova.
 In questo quadro, dunque, l’innalzamento del trattamento sanzionatorio, previsto per il delitto di cui all’art. 372 cod. pen., è stato dettato dalla necessità di preservare la veridicità della prova, non soltanto con riferimento ai procedimenti per reati di criminalità organizzata, in relazione ai quali essa risulta particolarmente esposta al pericolo di intimidazioni, ma soprattutto in relazione all’attuale modello di processo penale di tipo tendenzialmente accusatorio, con una disciplina che, prevedendo la formazione della prova in via prevalente in dibattimento, nel contraddittorio delle parti, ha attribuito ruolo primario alla testimonianza.
 Orbene, come questa Corte ha affermato con giurisprudenza costante, è possibile censurare la discrezionalità del legislatore in ordine alla individuazione delle condotte punibili ed alla determinazione del trattamento sanzionatorio soltanto nel caso in cui la stessa sia stata esercitata in modo manifestamente irragionevole, arbitrario o radicalmente ingiustificato (ex multis: sentenze n. 161 del 2009, n. 22 del 2007, n. 394 del 2006, n. 325 del 2005; ordinanze n. 41 del 2009 e n. 52 del 2008). 
 Nel caso in esame si deve escludere, alla luce delle considerazioni ora svolte, che la determinazione del minimo edittale per il delitto de quo violi il principio di ragionevolezza o di proporzionalità, in quanto l’inasprimento della pena risulta giustificato dalle suddette esigenze; alle quali, peraltro, si deve aggiungere il rilievo che l’illecito in questione presenta un disvalore intrinseco che gli attribuisce carattere di gravità, anche se la circostanza oggetto di mendacio o di reticenza non desta particolare allarme sociale. Infatti, la falsa testimonianza turba comunque il normale svolgimento del processo, ne compromette lo scopo che è quello di pervenire a sentenze giuste, costituisce ostacolo all’accertamento giudiziale.
 Rientra, poi, nella discrezionalità del legislatore anche la facoltà di modulare il trattamento sanzionatorio in riferimento al dilagare di un fenomeno criminoso che si intende reprimere. Sotto tale profilo, il rimettente trascura di considerare la diversa incidenza del delitto in questione con riferimento alle molteplici realtà territoriali, nel momento in cui asserisce che «nessuna evidenza sociologica ha mai esaltato la recrudescenza nella società italiana» del delitto di falsa testimonianza.
 Né si può giungere ad altra conclusione per l’asserita disparità di trattamento, ravvisata dal giudice a quo con riguardo ad ipotesi di reato assimilabili al delitto di falsa testimonianza, quali la frode processuale, il favoreggiamento personale, anche con riferimento alla previsione dettata dal capoverso dell’art. 378 cod. pen., e la simulazione di reato, e con riguardo a fattispecie connotate da maggiore gravità, come il delitto di calunnia.
 Le fattispecie poste a confronto, invero, ancorché catalogate tra i delitti contro l’attività giudiziaria, non hanno la stessa oggettività giuridica, sicché deve escludersi che il differente trattamento sanzionatorio, rispetto al delitto di falsa testimonianza, sia il frutto di una scelta arbitraria o ingiustificata (ex multis: sentenza n. 161 del 2009; ordinanze n. 229 del 2006, n. 170 del 2006, n. 45 del 2006 e n. 438 del 2001).
 Infatti, con la disposizione che sanziona il reato di falsa testimonianza il legislatore, come si è detto, ha inteso tutelare lo specifico interesse alla veridicità della prova testimoniale ed alla completezza della stessa in considerazione del ruolo primario svolto nel sistema processuale. 
 La previsione che sanziona la frode processuale (art. 374 cod. pen.) tutela, invece, la genuinità di fonti attraverso le quali si forma il convincimento del giudice in ordine agli elementi di prova desumibili da atti di ispezione di luoghi, di cose o persone, dall’esperimento giudiziale, dalla perizia o dalla consulenza tecnica. In tale ipotesi di reato, dunque, l’alterazione avviene non direttamente innanzi all’autorità giudiziaria, ma in un momento precedente all’assunzione o alla valutazione della prova. Inoltre, in molti casi, si tratta di atti irripetibili, sottratti alla verifica del dibattimento.
 Sanzionando, poi, il delitto di simulazione di reato, il legislatore si è posto l’obiettivo di evitare che gli organi destinati all’accertamento e alla repressione dei reati siano attivati inutilmente, con dispendio di energie e sviamento dalle loro funzioni istituzionali.
 Attraverso i delitti di favoreggiamento, personale e reale (artt. 378 e 379 cod. pen.), il legislatore ha inteso salvaguardare il regolare svolgimento del procedimento penale nella fase delle indagini e delle ricerche, in quanto le condotte che integrano le dette ipotesi criminose tendono a fuorviare o ad ostacolare l’attività di accertamento e repressione dei reati.
 Con riferimento al delitto di calunnia, il giudice a quo ha omesso di considerare che la maggiore gravità di quest’ultimo rispetto alla falsa testimonianza – derivante dall’essere reato plurioffensivo in quanto lesivo sia dell’interesse a che l’attività giudiziaria non sia tratta in inganno, sia della libertà e dell’onore della persona falsamente incolpata – trova sanzione nel più severo trattamento previsto quando la falsa incolpazione concerne reati puniti con pena superiore nel massimo a dieci anni o con altra pena più grave o se dal fatto è derivata una  condanna alla reclusione nella misura determinata dalla norma medesima.
 Come si vede, le indicate ipotesi di reato, pur presentando tratti comuni che ne giustificano la collocazione nella categoria dei delitti contro l’attività giudiziaria, non hanno carattere del tutto omogeneo, sicché il diverso trattamento sanzionatorio ad esse riservato costituisce legittimo esercizio della discrezionalità legislativa.
 2.2.— Anche la censura mossa con riferimento all’art. 27, terzo comma, Cost. non è fondata.
 La disposizione impugnata, prevedendo un significativo divario tra il minimo ed il massimo edittale della pena, consente al giudice di graduare quest’ultima in relazione alla gravità del fatto e, quindi, di adeguare il trattamento punitivo al diverso disvalore delle singole violazioni rientranti nella previsione della norma, così realizzando la finalità rieducativa cui la pena stessa deve tendere. E nella stessa prospettiva, non va trascurato il potere affidato al giudice di riconoscere le circostanze attenuanti e, segnatamente, le attenuanti generiche, così ulteriormente adeguando la misura della pena alla personalità del reo e alla gravità del fatto.
 Infine, non si può condividere l’argomento del rimettente, secondo cui «coincidendo il minimo edittale (anni due di reclusione) con il limite di pena oltre il quale non è usufruibile la sospensione condizionale ex art. 163 cod. pen., è giocoforza lamentare che ciò comprime al massimo, di fatto quasi annullandolo, il margine di operatività del suddetto beneficio, accordabile al reo primario che ne possa essere meritevole». Infatti, a parte il rilievo che la soglia minima della pena già consente la concessione del beneficio, nonostante l’innalzamento del minimo edittale il giudice può comunque applicare la sospensione condizionale della pena, sia attraverso la già rilevata possibilità di riconoscere le circostanze attenuanti (ovviamente, qualora ne ricorrano i presupposti), sia per effetto di riduzioni della pena stessa conseguenti alla scelta di riti alternativi.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara non fondata la questione di legittimità costituzionale dell’articolo 372 del codice penale, sollevata, in riferimento agli articoli 3 e 27 della Costituzione, dal Tribunale di Trento, con l’ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte Costituzionale, Palazzo della Consulta, l'8 febbraio 2010.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Alessandro CRISCUOLO, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 12 febbraio 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
