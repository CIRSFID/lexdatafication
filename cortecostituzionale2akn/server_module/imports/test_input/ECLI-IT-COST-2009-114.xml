<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/114/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/114/"/>
          <FRBRalias value="ECLI:IT:COST:2009:114" name="ECLI"/>
          <FRBRdate date="20/04/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="114"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/114/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/114/ita@/!main"/>
          <FRBRdate date="20/04/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/114/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/114/ita@.xml"/>
          <FRBRdate date="20/04/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/04/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>20/04/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Francesco AMIRANTE; Giudici: Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 2, commi 458, 459 e 460, della legge 24 dicembre 2007, n. 244 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2008), promosso dalla Regione Veneto con ricorso notificato il 26 febbraio 2008, depositato in cancelleria il 5 marzo 2008 ed iscritto al n. 19 del registro ricorsi 2008. 
    Visto l'atto di costituzione del Presidente del Consiglio dei ministri; 
    udito nell'udienza pubblica del 10 marzo 2009 il Giudice relatore Alfio Finocchiaro; 
    uditi gli avvocati Luigi Manzi, Mario Bertolissi e Ezio Zanon per la Regione Veneto e l'avvocato dello Stato Gabriella D'Avanzo per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. – Con ricorso notificato il 26 febbraio 2008 e depositato il successivo 5 marzo, la Regione Veneto ha impugnato diverse disposizioni della legge 24 dicembre 2007, n. 244 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2008), tra le quali, l'art. 2, commi 458, 459 e 460, per violazione degli artt. 117, comma terzo, 118, 119 e in via subordinata degli artt. 5, 120 della Costituzione, e dell'art. 11 della legge costituzionale 18 ottobre 2001, n. 3. 
    Le norme impugnate istituiscono un fondo statale per l'organizzazione e il funzionamento di asili-nido presso enti e reparti del Ministero della Difesa, disciplinano la programmazione e la progettazione degli stessi, prevedono l'accessibilità agli asili-nido di minori non figli di dipendenti dell'Amministrazione della difesa. 
    La ricorrente ritiene che per individuare la materia alla quale attengono le norme in questione sia indispensabile richiamare le sentenze della Corte costituzionale n. 370 del 2003 e n. 320 del 2004, dalle quali risulterebbe evidente che la disciplina normativa in materia di asili-nido deve essere ricondotta a materie (prevalentemente alla materia «istruzione», in parte anche alla materia «tutela del lavoro») rientranti tra quelle di potestà legislativa concorrente (art. 117, terzo comma, Cost.), in relazione alle quali spetta allo Stato la determinazione dei principi fondamentali e alle Regioni la fissazione della normativa di dettaglio. 
    Pertanto, le norme di cui ai commi 458, 459 e 460 dell'art. 2 della legge finanziaria per l'anno 2008, che sono riconducibili ad una materia di potestà legislativa concorrente, violerebbero l'art. 117, terzo comma, della Costituzione, stante la loro natura di norme di dettaglio. 
    Le predette norme risulterebbero lesive, altresì, dell'art. 119 della Costituzione, il quale non consente allo Stato di istituire e disciplinare finanziamenti a destinazione vincolata né nelle materie di potestà legislativa concorrente (art. 117, terzo comma, Cost.), né nelle materie di potestà legislativa residuale delle Regioni (art. 117, quarto comma, Cost.), sia che questi fondi prevedano la diretta attribuzione di risorse a Regioni, Province, Città metropolitane o Comuni, sia che prevedano la diretta attribuzione di risorse a soggetti privati, persone fisiche o giuridiche, poiché «il ricorso a finanziamenti ad hoc rischierebbe di divenire uno strumento indiretto, ma pervasivo, di ingerenza dello Stato nell'esercizio delle funzioni degli enti locali, e di sovrapposizione di politiche e di indirizzi governati centralmente a quelli legittimamente decisi dalle Regioni negli ambiti materiali di propria competenza» (sentenza n. 16 del 2004). 
    Osserva la ricorrente che la Corte costituzionale ha precisato: che tali fondi «non solo debbono essere aggiuntivi rispetto al finanziamento integrale (...) delle funzioni spettanti ai Comuni o agli altri enti, e riferirsi alle finalità di perequazione e di garanzia enunciate nella norma costituzionale, o comunque a scopi diversi dal normale esercizio delle funzioni, ma debbono essere indirizzati a determinati Comuni o categorie di Comuni (o Province, Città metropolitane, Regioni)»; e che «l'esigenza di rispettare il riparto costituzionale delle competenze legislative fra Stato e Regioni comporta altresì che, quando tali finanziamenti riguardino ambiti di competenza delle Regioni, queste siano chiamate ad esercitare compiti di programmazione e di riparto dei fondi all'interno del proprio territorio» (sentenze n. 16 del 2004 e n. 222 del 2005). Da quanto detto conseguirebbe anche la violazione dell'art. 118 della Costituzione. 
    In subordine, peraltro, considerato che potrebbero ravvisarsi delle interferenze con materie di potestà legislativa esclusiva dello Stato (art. 117, secondo comma, Cost.), quale, per esempio, quella relativa a «difesa e Forze armate» (art. 117, secondo comma, lettera d), si censurano le norme de quibus anche per violazione del principio di leale collaborazione tra Stato e Regione, desumibile, in particolare, dagli artt. 5 e 120, secondo comma, Cost. e 11 della legge costituzionale n. 3 del 2001. La Corte costituzionale ha infatti riconosciuto che «la complessità della realtà sociale da regolare comporta che, di frequente, le normative non possano essere riferite nel loro insieme ad una sola materia, perché concernono situazioni non omogenee, ricomprese in materie diverse sotto il profilo della competenza legislativa» (sentenza n. 133 del 2006). Conseguentemente, essa ha ritenuto che «per le ipotesi in cui ricorra una “concorrenza di competenze”, la Costituzione non prevede espressamente un criterio di composizione delle interferenze. In tal caso – ove (...) non possa ravvisarsi la sicura prevalenza di un complesso normativo rispetto ad altri, che renda dominante la relativa competenza legislativa – si deve ricorrere al canone della “leale collaborazione”, che impone alla legge statale di predisporre adeguati strumenti di coinvolgimento delle regioni, a salvaguardia delle loro competenze» (sentenze nn. 50 e 219 del 2005). 
    2. – Si è costituito in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata infondata, in quanto la norma oggetto di impugnazione è espressione della competenza legislativa esclusiva dello Stato ex art. 117, secondo comma, lettera n), della Costituzione, in materia di norme generali sull'istruzione, con la conseguenza che non può essere addotta la giurisprudenza della Corte costituzionale in tema di preclusione per lo Stato ad istituire finanziamenti a destinazione vincolata, preclusione che va riferita esclusivamente alle materie di potestà legislativa concorrente o residuale delle Regioni. 
    3. – Con memoria depositata il 25 febbraio 2009, la regione Veneto ha insistito per l'accoglimento del ricorso. Afferma la ricorrente che non può essere accolta la tesi dell'Avvocatura generale dello Stato secondo cui ci si trova in presenza di disposizioni normative dettate in materia di “norme generali sull'istruzione”, in quanto sarebbero tali solo quelle che ne definiscono l'assetto organizzativo fondamentale, come dimostra il d.lgs. 19 febbraio 2004, n. 59 (Definizione delle norme generali relative alla scuola dell'infanzia e al primo ciclo dell'istruzione, a norma dell'articolo 1 della L. 28 marzo 2003, n. 53), le cui norme nulla hanno a che vedere con quelle oggetto di impugnazione.Considerato in diritto1. – La Regione Veneto dubita della legittimità costituzionale dell'art. 2, commi 458, 459 e 460, della legge 24 dicembre 2007, n. 244 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2008), nella parte in cui istituisce un fondo per l'organizzazione e il funzionamento di servizi socio-educativi per la prima infanzia destinati ai minori di età fino a 36 mesi presso enti e reparti del Ministero della difesa, per violazione: a) dell'art. 117, terzo comma, della Costituzione stante la loro natura di norme di dettaglio, poiché, come affermato dalle sentenze n. 370 del 2003 e n. 320 del 2004 della Corte costituzionale, la disciplina in materia di asili-nido deve essere ricondotta a materie (prevalentemente alla materia «istruzione», ma in parte anche alla materia «tutela del lavoro»), rientranti tra quelle di potestà legislativa concorrente (art. 117, terzo comma, Cost.), materie, dunque, in relazione alle quali spetta allo Stato la determinazione dei principi fondamentali e alle Regioni la fissazione della normativa di dettaglio; b) degli artt. 118 e 119 della Costituzione, in quanto quest'ultima norma non consentirebbe allo Stato di istituire e disciplinare finanziamenti a destinazione vincolata nelle materie di potestà legislativa concorrente, poiché «il ricorso a finanziamenti ad hoc rischierebbe di divenire uno strumento indiretto, ma pervasivo, di ingerenza dello Stato nell'esercizio delle funzioni delle Regioni e degli enti locali, e di sovrapposizione di politiche e di indirizzi governati centralmente a quelli legittimamente decisi dalle Regioni negli ambiti materiali di propria competenza» (sentenza n. 16 del 2004 della Corte costituzionale): dalla violazione dell'art. 119 Cost. conseguirebbe, de plano, la violazione dell'art. 118 Cost. 
    La Regione ricorrente, in subordine, deduce che qualora si ritenesse che nel caso di specie siano coinvolte anche materie di potestà legislativa esclusiva dello Stato quali, per esempio, «difesa e Forze armate» (art. 117, comma 2, lettera d), la normativa impugnata sarebbe incostituzionale per violazione degli artt. 5 e 120 della Costituzione e 11 della legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte seconda della Costituzione), norme che costituirebbero espressione del principio di «leale collaborazione», in quanto in presenza di una concorrenza di competenze e in assenza di una sicura prevalenza di un complesso normativo rispetto ad altri, si deve ricorrere al canone della «leale collaborazione». 
    2. – La questione non è fondata. 
    2.1. – Le disposizioni impugnate prevedono: al comma 458, che «Per l'organizzazione e il funzionamento di servizi socio-educativi per la prima infanzia destinati ai minori di età fino a 36 mesi, presso enti e reparti del Ministero della difesa, è istituito un fondo con una dotazione di 3 milioni di euro per ciascuno degli anni 2008, 2009 e 2010»; al comma 459, che «La programmazione e la progettazione relativa ai servizi di cui al comma 458, nel rispetto delle disposizioni normative e regolamentari vigenti nelle regioni presso le quali sono individuate le sedi di tali servizi, viene effettuata in collaborazione con il Dipartimento per le politiche della famiglia della Presidenza del Consiglio dei ministri, sentito il comitato tecnico-scientifico del Centro nazionale di documentazione e di analisi per l'infanzia e l'adolescenza, di cui al decreto del Presidente della Repubblica 14 maggio 2007, n. 103»; al comma 460, che «I servizi socio-educativi di cui al comma 458 sono accessibili anche da minori che non siano figli di dipendenti dell'Amministrazione della difesa e concorrono ad integrare l'offerta complessiva del sistema integrato dei servizi socio-educativi per la prima infanzia e del relativo Piano straordinario di intervento di cui all'art. 1, comma 1259, della legge 27 dicembre 2006, n. 296, come modificato dal comma 457».  
    La Regione sostiene che gli asili-nido rientrano in una materia – l'istruzione – compresa tra quelle di competenza concorrente tra Stato e Regioni (art. 117, comma 3, Cost.) e dunque, in base all'art. 119 Cost., lo Stato non avrebbe competenza a legiferare e a “gestire denaro” in una materia di competenza concorrente.  
    L'Avvocatura dello Stato replica che gli asili-nido rientrerebbero nella materia «norme generali sull'istruzione» (art. 117, comma 2, lettera n), da non confondersi con la materia «istruzione» di cui al comma 3 dell'art. 117 Cost.) e che pertanto lo Stato avrebbe tutto il diritto di legiferare istituendo fondi in tale materia e di gestirli. 
    Questa Corte, con varie decisioni (sentenze n. 320 del 2004, n. 370 del 2003), ha negato che la disciplina degli asili-nido possa essere ricondotta alle materie di competenza residuale delle Regioni ai sensi del quarto comma dell'art. 117 Cost., ma ha ritenuto – sulla base di una ricostruzione dell'evoluzione normativa del settore – che, utilizzando un criterio di prevalenza, la relativa disciplina ricada nell'ambito della competenza legislativa concorrente di cui all'art. 117, terzo comma, della Costituzione, fatti salvi, naturalmente, gli interventi del legislatore statale che trovino legittimazione nei titoli “trasversali” di cui all'art. 117, secondo comma, della Costituzione. 
    Dagli enunciati principi deriva che le norme impugnate, poiché sono funzionali ad una migliore organizzazione dei servizi a favore dei dipendenti del Ministero della Difesa, non sono invasive delle competenze regionali, rientrando nella materia dell'ordinamento e organizzazione amministrativa dello Stato, riservata alla competenza esclusiva di quest'ultimo, ai sensi dell'art. 117, secondo comma, lettera g) della Costituzione. 
    Tale competenza si estende, con riferimento in particolare a quanto dispone il comma 458 – che prevede, per l'organizzazione e il funzionamento dei servizi socio-educativi per la prima infanzia ai minori di età fino a trentasei mesi, presso enti e reparti del Ministero della Difesa l'istituzione di un fondo con la relativa dotazione – anche al potere di istituire fondi destinati all'organizzazione e al funzionamento dei relativi servizi. 
    Inoltre, quanto alle censure prospettate nei confronti dei commi successivi, deve anche tenersi presente, con riferimento al comma 459, che la programmazione e la progettazione relativa ai servizi socio-educativi per la prima infanzia viene effettuata «nel rispetto delle disposizioni normative e regolamentari vigenti nelle regioni presso le quali sono individuate le sedi di tali servizi» e, quindi, con salvaguardia delle relative competenze regionali.  
    Il comma 460, poi, oltre ad affermare il diritto di accesso ai servizi socio-educativi da parte di minori che non siano figli di dipendenti dell'Amministrazione della difesa, dispone che tali servizi «concorrono ad integrare l'offerta complessiva del sistema integrato dei servizi socio-educativi per la prima infanzia e del relativo Piano straordinario di intervento di cui all'articolo 1, comma 1259, della legge 27 dicembre 2006, n. 206, come modificato dal comma 457», con richiamo, cioè, all'intesa fra Stato e Conferenza unificata di cui all'articolo 8 del decreto legislativo n. 281 del 1997, che realizza il pieno coinvolgimento delle Regioni nella programmazione e progettazione dei servizi.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riservata a separate pronunce ogni decisione sulle ulteriori questioni di legittimità costituzionale della legge 24 dicembre 2007, n. 244 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2008), sollevate dalla Regione Veneto con il ricorso indicato in epigrafe; &#13;
    dichiara non fondata la questione di legittimità costituzionale dell'art. 2, commi 458, 459 e 460 della legge n. 244 del 2007, sollevata dalla Regione Veneto, per violazione degli art. 5, 117, comma terzo, 118, 119 e 120 della Costituzione e dell'art. 11 della legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte seconda della Costituzione), con il ricorso indicato in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 aprile 2009.  &#13;
F.to:  &#13;
Francesco AMIRANTE, Presidente  &#13;
Alfio FINOCCHIARO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 24 aprile 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
