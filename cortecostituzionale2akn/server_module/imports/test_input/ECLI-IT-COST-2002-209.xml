<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/209/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/209/"/>
          <FRBRalias value="ECLI:IT:COST:2002:209" name="ECLI"/>
          <FRBRdate date="20/05/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="209"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/209/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/209/ita@/!main"/>
          <FRBRdate date="20/05/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/209/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/209/ita@.xml"/>
          <FRBRdate date="20/05/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/05/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Gustavo Zagrebelsky</cc:relatore_pronuncia>
        <cc:data_decisione>20/05/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Francesco AMIRANTE,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 171-ter, comma 1, lettera c), della legge 22 aprile 1941, n. 633 (Protezione del diritto d'autore e di altri diritti connessi al suo esercizio), nel testo modificato dal decreto legislativo 16 novembre 1994, n. 685 (Attuazione della direttiva 92/100/CEE concernente il diritto di noleggio, il diritto di prestito e taluni diritti connessi al diritto d'autore in materia di proprietà intellettuale), promossi con due ordinanze emesse il 21 dicembre 2000 dal Tribunale di Milano, iscritte ai nn. 645 e 707 del registro ordinanze 2001 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 36 e n. 38, prima serie speciale, dell'anno 2001. 
      Visti gli atti di intervento e di costituzione della SIAE nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 13 febbraio 2002 il Giudice relatore Gustavo Zagrebelsky. 
      Ritenuto che con ordinanza del 21 dicembre 2000 (r.o. 645/2001), emessa nel corso di un procedimento penale per il reato di vendita di supporti (musicassette e CD) sprovvisti del contrassegno della Società italiana degli autori e degli editori (SIAE), il Tribunale di Milano ha sollevato, in riferimento all'art. 25, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 171-ter, comma 1, lettera c), della legge 22 aprile 1941, n. 633 (Protezione del diritto d'autore e di altri diritti connessi al suo esercizio), nella sua formulazione antecedente le modifiche introdotte con la legge 18 agosto 2000, n. 248 (Nuove norme di tutela del diritto di autore);  
      che il rimettente rammenta preliminarmente che, in ordine a tale questione, già da lui sollevata in precedenza, questa Corte aveva disposto, con ordinanza n. 547 del 2000, la restituzione degli atti al giudice a quo perché considerasse l'incidenza della citata legge n. 248 del 2000 rispetto al giudizio innanzi a esso pendente; 
      che il Tribunale esclude che le disposizioni di cui alla legge n. 248 del 2000, con cui è stata tra l'altro parzialmente riformulata la norma impugnata, esplichino alcuna incidenza sul giudizio cui è chiamato, trattandosi di disposizioni, nel loro complesso, meno favorevoli sotto il profilo sanzionatorio e dunque inapplicabili nel giudizio di merito, alla stregua del principio di irretroattività della legge più sfavorevole in materia penale; 
      che, richiamata la disamina - svolta nella precedente ordinanza di rimessione - della normativa concernente la tutela penale delle opere protette dal diritto d'autore, in particolare relativamente ai supporti fonografici, alle videocassette e alle musicassette, il rimettente osserva che le condotte incriminate dalla norma impugnata sono punibili, secondo la dizione di essa, solo qualora abbiano a oggetto supporti privi del contrassegno SIAE apposti «ai sensi della [stessa] legge» n. 633 del 1941 «e del regolamento di esecuzione», adottato con regio decreto 18 maggio 1942, n. 1369 (Approvazione del regolamento per l'esecuzione della legge 22 aprile 1941, n. 633, per la protezione del diritto di autore e di altri diritti connessi al suo esercizio); 
      che, circa gli effetti di tale rinvio, il rimettente ribadisce che, in difetto di una disciplina legislativa o regolamentare ad hoc che stabilisca le modalità di apposizione del contrassegno SIAE sui supporti in parola - disciplina a suo avviso non rinvenibile né nella legge n. 633 del 1941, né nel relativo regolamento di esecuzione -, la condotta punibile risulta in realtà incompleta dal punto di vista della descrizione della fattispecie, rendendo imperfetta la norma incriminatrice, e che tale «vuoto normativo» non potrebbe essere colmato - come invece è avvenuto nella prassi della giurisprudenza - dall'attività interpretativa del giudice; 
      che, a sostegno della propria tesi, il giudice a quo ricorda che alla conclusione dell'irrilevanza penale delle condotte in argomento era giunta inizialmente la giurisprudenza di legittimità e che tale orientamento interpretativo è successivamente mutato, consolidandosi la diversa tesi della punibilità dei fatti di vendita di supporti privi di contrassegno SIAE e individuandosi, da parte della stessa giurisprudenza, le modalità di apposizione di detto contrassegno nell'art. 12 del regolamento di esecuzione della legge; 
      che, ad avviso del rimettente, tale ultimo indirizzo interpretativo appare diretto a «salvare» l'applicabilità di norme «mal concepite», giacché esso si avvale del ricorso a norme, topograficamente e strutturalmente estranee alla materia oggetto dell'incriminazione contestata, che sono svolgimento attuativo di altre disposizioni della legge sul diritto d'autore, riguardanti lo specifico settore delle opere letterarie su supporto cartaceo;  
      che, di conseguenza, il fatto dedotto in giudizio - la vendita di supporti musicali sprovvisti di contrassegno SIAE - ricadrebbe, secondo la lettura che il rimettente reputa più corretta, nell'area del penalmente irrilevante; 
      che, in considerazione del fatto che la giurisprudenza si è indirizzata nel senso opposto, valorizzando l'art. 12 del citato regolamento di esecuzione della legge, e poiché dunque la norma «vive» in questo ultimo senso, il Tribunale di Milano solleva questione di costituzionalità dell'art. 171-ter, comma 1, lettera c), della legge n. 633 del 1941, in quanto tale disposizione violerebbe il principio costituzionale di tassatività della fattispecie penale (art. 25, secondo comma, della Costituzione), mancando la determinazione normativa delle modalità di apposizione del contrassegno SIAE, alle quali fa riferimento la norma impugnata; 
      che, a sostegno di quanto già dedotto nella sua originaria ordinanza di rimessione, il giudice a quo trae conferma dell'assenza, all'epoca dei fatti per cui procede, «di un regolamento atto a disciplinare in modo esauriente le attività di apposizione del timbro su tutti i prodotti producibili nell'attuale realtà tecnologica», dalla lettera dell'art. 181-bis della legge n. 633 del 1941, introdotto dall'art. 10 della legge n. 248 del 2000, il quale prevede che con successivo decreto del Presidente del Consiglio dei ministri siano individuati «i tempi, le caratteristiche e la collocazione» del contrassegno SIAE [disciplina nel frattempo adottata con d.P.C.m. 11 luglio 2001, n. 338 (Regolamento di esecuzione delle disposizioni relative al contrassegno della Società italiana degli autori e degli editori (S.I.A.E.) di cui all'articolo 181-bis della legge 22 aprile 1941, n. 633, come introdotto dall'articolo 10 della legge 18 agosto 2000, n. 248, recante nuove norme di tutela del diritto d'autore)]; 
      che, con altra ordinanza di pari data (r.o. 707/2001), emessa nel corso di altro procedimento penale, lo stesso giudice a quo ha sollevato, in riferimento al medesimo parametro costituzionale, identica questione di legittimità costituzionale dell'art. 171-ter, comma 1, lettera c), della legge n. 633 del 1941, sempre nel testo antecedente le modifiche introdotte dalla legge n. 248 del 2000; 
      che la questione, riproposta anch'essa a seguito della restituzione degli atti disposta con la citata ordinanza n. 547 del 2000 di questa Corte, è sorretta dalle medesime argomentazioni della precedente, circa la persistente rilevanza e la non manifesta infondatezza della questione; 
      che in entrambi i giudizi così promossi è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'inammissibilità e, in subordine, per l'infondatezza della questione; 
      che nel primo giudizio (r.o. 645/2001) la SIAE ha depositato atto di intervento e nel secondo giudizio (r.o. 707/2001), quale parte civile nel giudizio penale principale, ha depositato atto di costituzione, concludendo, in entrambi, per l'inammissibilità o comunque per il rigetto della questione.  
      Considerato che il Tribunale di Milano, nel corso di due distinti procedimenti penali aventi a oggetto il reato di vendita di supporti fonografici privi del contrassegno della Società italiana degli autori e degli editori (SIAE), ha sollevato, in riferimento all'art. 25, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 171-ter, comma 1, lettera c), della legge 22 aprile 1941, n. 633 (Protezione del diritto d'autore e di altri diritti connessi al suo esercizio), nella sua formulazione anteriore alle modifiche introdotte con la legge 18 agosto 2000, n. 248 (Nuove norme di tutela del diritto di autore); 
    che l'identità delle questioni, riguardanti la medesima disposizione di legge e sollevate in riferimento al medesimo parametro costituzionale, rende opportuna la riunione dei relativi giudizi, perché siano decisi congiuntamente; 
      che il giudice a quo, a seguito della restituzione degli atti da parte di questa Corte, ha integrato le motivazioni delle precedenti ordinanze di rimessione, argomentando sulla persistente necessità di fare applicazione nel giudizio principale della disposizione già denunciata, sicché la questione sollevata con entrambe le ordinanze è, sotto il profilo della motivazione circa la rilevanza, ammissibile; 
      che la premessa da cui muove il rimettente nel sollevare la questione si fonda sull'asserita inapplicabilità della disciplina dettata dall'art. 12 del regio decreto 18 maggio 1942, n. 1369 (Approvazione del regolamento per l'esecuzione della legge 22 aprile 1941, n. 633, per la protezione del diritto di autore e di altri diritti connessi al suo esercizio), alla apposizione del contrassegno SIAE su supporti fonografici, essendo la stessa - a suo avviso - riferibile esclusivamente alle opere letterarie cartacee, e sulla conseguente carenza di contenuto della norma incriminatrice impugnata, facendo essa rinvio - quanto alle modalità di apposizione del contrassegno SIAE - a una disciplina che il giudice a quo reputa inesistente; 
      che da tale premessa interpretativa il giudice non trae l'unica conseguenza che allo stesso sembra possibile secondo il tenore della norma censurata, vale a dire quella della irrilevanza penale della condotta ascritta all'imputato, che «dovrebbe essere mandato assolto [...] perché il fatto non è preveduto dalla legge come reato», apparendogli preclusa tale opzione dalla consolidata giurisprudenza di legittimità, orientata in senso opposto;  
      che, così come prospettata, la questione, sollevata in modo del tutto contraddittorio rispetto alle sue premesse, si configura come una impropria utilizzazione del giudizio di legittimità costituzionale, dichiaratamente attivato per contrastare una interpretazione che l'ordinanza di rimessione, lungi dal far propria e dal porre a fondamento del dubbio di costituzionalità, mostra di non condividere affatto, con il paradossale risultato di sottoporre a censura una soluzione interpretativa alla quale è contestualmente negata ogni plausibilità (ordinanze n. 443 e n. 199 del 2001); 
      che la questione, sollevata per una finalità estranea alla logica del giudizio incidentale di costituzionalità, deve pertanto essere dichiarata manifestamente inammissibile. 
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
      dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 171-ter, comma 1, lettera c), della legge 22 aprile 1941, n. 633 (Protezione del diritto d'autore e di altri diritti connessi al suo esercizio), nel testo modificato dal decreto legislativo 16 novembre 1994, n. 685 (Attuazione della direttiva 92/100/CEE concernente il diritto di noleggio, il diritto di prestito e taluni diritti connessi al diritto d'autore in materia di proprietà intellettuale), sollevate, in riferimento all'art. 25, secondo comma, della Costituzione, dal Tribunale di Milano con le ordinanze indicate in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 maggio 2002. &#13;
F.to: &#13;
Cesare RUPERTO, Presidente &#13;
Gustavo ZAGREBELSKY, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 23 maggio 2002. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
