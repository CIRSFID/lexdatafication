<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/312/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/312/"/>
          <FRBRalias value="ECLI:IT:COST:2005:312" name="ECLI"/>
          <FRBRdate date="07/07/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="312"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/312/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/312/ita@/!main"/>
          <FRBRdate date="07/07/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/312/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/312/ita@.xml"/>
          <FRBRdate date="07/07/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="22/07/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CAPOTOSTI</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>07/07/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Piero Alberto CAPOTOSTI; Giudici: Fernanda CONTRI, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale degli artt. 2, 20 e 29 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), promossi, nell'ambito di diversi procedimenti penali, dal Giudice di pace di Cairo Montenotte con ordinanza del 2 novembre 2004 e dal Giudice di pace di Pisciotta con ordinanza del 28 ottobre 2004, rispettivamente iscritte ai numeri 64 e 85 del registro ordinanze 2005 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 9, prima serie speciale, dell'anno 2005. 
    Udito nella camera di consiglio del 6 luglio 2005 il Giudice relatore Guido Neppi Modona.  
    Ritenuto che il Giudice di pace di Cairo Montenotte (ordinanza iscritta al n. 64 del registro ordinanze del 2005) ha sollevato, su eccezione della difesa, questione di legittimità costituzionale dell'art. 2 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui esclude i riti alternativi nel procedimento davanti al giudice di pace; 
    che il rimettente premette che la difesa dell'imputato ha chiesto di sollevare questione di legittimità costituzionale sostenendo che la disposizione censurata, nella parte in cui esclude i riti alternativi, «contrasta con la ragione di fondo dell'impianto processuale prefigurato dalla legge-delega» relativa alla riforma del processo penale, che tende «a favorire l'adozione dei riti alternativi, per contenere i carichi processuali», e che non prevede «alcuna discriminazione ai fini dell'applicazione del rito abbreviato e del patteggiamento, a seconda del giudice che li applica»; 
    che la difesa dell'imputato ritiene pertanto che l'art. 2 del decreto legislativo n. 274 del 2000 violi l'art. 3 della Costituzione, per disparità di trattamento tra «le ipotesi delittuose di competenza del giudice penale monocratico che prevedono la punizione del reato con la sola pena pecuniaria e quelle di competenza del giudice di pace, che altrettanto sono punibili soltanto con pena pecuniaria», nonché per «disparità di trattamento rispetto all'opponente a decreto penale di condanna»;  
    che ad avviso del rimettente le questioni non sono manifestamente infondate «sia in rapporto all'art. 111 della Carta costituzionale, con riferimento ai principi del giusto processo, sia in rapporto all'art. 3 della medesima legge fondamentale, relativamente all'eguaglianza di tutti i cittadini davanti alla legge»; 
    che, infine, il giudice a quo ritiene rilevante la questione «ai fini della definizione del processo di che trattasi, ben diversa, nella sostanza, a seconda della soluzione che verrà fornita»; 
    che il Giudice di pace di Pisciotta (ordinanza iscritta al n. 85 del registro ordinanze del 2005) ha sollevato, in riferimento agli artt. 3, 24 e 76 Cost., questione di legittimità costituzionale dell'art. 2 del decreto legislativo n. 274 del 2000, «nella parte in cui esclude nel procedimento penale davanti al giudice di pace la possibilità dell'applicazione della pena su richiesta delle parti», nonché degli artt. 20 e 29 del medesimo decreto legislativo, nella parte in cui non prevedono «che la citazione a giudizio, disposta dalla polizia giudiziaria contenga anche l'avviso che l'imputato può presentare la richiesta di cui all'art. 444 del codice di procedura penale, prima della dichiarazione di apertura del dibattimento […] e che l'imputato, alternativamente alla prevista domanda di oblazione, ovvero il pubblico ministero, possano, prima della dichiarazione di apertura del dibattimento, presentare domanda di applicazione della pena su richiesta»;  
    che il rimettente premette che un soggetto, tratto a giudizio per rispondere del reato di lesioni colpose gravi, aveva presentato richiesta di applicazione della pena a norma dell'art. 444 cod. proc. pen., «previa eccezione di incostituzionalità dell'art. 2 del decreto legislativo n. 274 del 2000», nella parte in cui tale disposizione esclude nel procedimento penale davanti al giudice di pace l'istituto dell'applicazione della pena su richiesta delle parti; 
    che, quanto alla rilevanza, il giudice a quo osserva che sussisterebbero le condizioni per valutare positivamente la richiesta in quanto la qualificazione giuridica del fatto appare corretta, condivisibile la richiesta di applicazione delle attenuanti generiche e la comparazione delle stesse con le aggravanti e, di conseguenza, congrua la determinazione della pena; 
    che, inoltre, lo stesso pubblico ministero ha dichiarato che, ove non fosse sussistito il divieto posto dall'art. 2 del decreto legislativo in esame, avrebbe espresso il proprio consenso; 
    che, nel merito, con specifico riferimento all'art. 76 Cost. il rimettente rileva che l'art. 17 della legge-delega 24 novembre 1999, n. 468, operando un mero rinvio alle norme del Libro VIII del codice di procedura penale, non solo non contiene alcuna preclusione espressa dell'istituto del patteggiamento, ma, al contrario, prevede che il legislatore delegato avrebbe potuto discostarsi «da tale procedimento solo al fine di una maggiore semplificazione, resa necessaria dalla competenza del giudice di pace»;  
    che, pertanto, l'espressa esclusione del patteggiamento nel procedimento davanti al giudice di pace, alla luce delle finalità della legge di delegazione e considerata la stessa natura dell'istituto, ragionevolmente ritenuto il principale rito deflativo tra i giudizi speciali, integrerebbe una palese violazione della delega da parte del legislatore delegato; 
    che, ad avviso del giudice a quo, la disciplina censurata si pone altresì in contrasto con l'art. 3 Cost., perché, irragionevolmente, consente il patteggiamento per reati molto gravi puniti con la sanzione della reclusione e non lo consente, invece, per i reati di competenza del giudice di pace, puniti, al più, con la pena della permanenza domiciliare;  
    che l'art. 3 sarebbe violato anche per irragionevolezza, ponendosi la disciplina censurata in assoluta controtendenza rispetto al sistema e alla più recente normativa concernente lo stesso istituto (viene richiamata la legge 12 giugno 2003, n. 134); 
    che la lesione del principio di eguaglianza e l'irragionevolezza della disciplina sarebbero particolarmente evidenti nelle ipotesi in cui reati di competenza del giudice di pace vengono giudicati per connessione dal tribunale, con conseguente applicabilità dell'istituto del patteggiamento ai reati di competenza del giudice di pace; 
    che, infine, il rimettente chiede che la Corte, ai sensi dell'art. 27 della legge 11 marzo 1953, n. 87, dichiari l'illegittimità costituzionale anche degli artt. 20 e 29 del medesimo decreto, nella parte in cui non prevedono «che la citazione a giudizio disposta dalla polizia giudiziaria contenga anche l'avviso che l'imputato può presentare la richiesta di cui all'art. 444 cod. proc. pen. prima della dichiarazione di apertura del dibattimento […] e che l'imputato, alternativamente alla prevista domanda di oblazione, ovvero il pubblico ministero, possano, prima della dichiarazione di apertura del dibattimento, presentare domanda di applicazione della pena su richiesta». 
    Considerato che entrambi i rimettenti sollevano questione di legittimità costituzionale dell'art. 2 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui esclude, nel procedimento davanti al giudice di pace, l'istituto dell'applicazione della pena su richiesta delle parti (ordinanza n. 85 del registro ordinanze del 2005) o, in genere, i riti deflativi (ordinanza n. 64 del registro ordinanze del 2005); 
    che, inoltre, il Giudice di pace di Pisciotta chiede alla Corte, in via consequenziale, ai sensi dell'art. 27 della legge n. 87 del 1953, che sia dichiarata l'illegittimità costituzionale anche degli artt. 20 e 29 del medesimo decreto legislativo, nella parte in cui non prevedono «che la citazione a giudizio, disposta dalla polizia giudiziaria contenga […] l'avviso che l'imputato può presentare la richiesta di cui all'art. 444 cod. proc. pen., prima della dichiarazione di apertura del dibattimento […] e che l'imputato, alternativamente alla prevista domanda di oblazione, ovvero il pubblico ministero, possano, prima della dichiarazione di apertura del dibattimento, presentare domanda di applicazione della pena su richiesta»;  
    che pertanto, avendo entrambe le ordinanze di rimessione come oggetto l'omessa previsione di riti speciali nel procedimento davanti al giudice di pace, deve essere disposta la riunione dei relativi giudizi; 
    che la questione sollevata dal Giudice di pace di Cairo Montenotte difetta della descrizione della fattispecie oggetto del giudizio a quo ed è del tutto carente di motivazione in ordine alla rilevanza e alla non manifesta infondatezza; 
    che non può valere a colmare tali lacune il semplice rinvio alle richieste della difesa dell'imputato, giacché il giudice deve rendere esplicite le ragioni che lo portano a dubitare della costituzionalità della norma con una motivazione autosufficiente (v., da ultimo, ordinanza n. 92 del 2005); 
    che la questione deve pertanto essere dichiarata manifestamente inammissibile; 
    che la questione sollevata dal Giudice di pace di Pisciotta è posta in riferimento agli artt. 3, 24 e 76 della Costituzione; 
    che analoga questione concernente l'espressa esclusione dell'istituto dell'applicazione della pena su richiesta delle parti nel procedimento davanti al giudice di pace, sollevata in riferimento agli artt. 3, 24, 76 e 77 Cost., è già stata dichiarata manifestamente infondata con ordinanza n. 228 del 2005; 
    che, in particolare, con riferimento agli artt. 3 e 24 Cost., la Corte ha affermato che «l'istituto del patteggiamento, così come delineato dal codice di procedura penale, mal si concilierebbe con il costante coinvolgimento della persona offesa» e che «le caratteristiche del procedimento davanti al giudice di pace consentono di ritenere che l'esclusione dell'applicabilità dei riti alternativi sia frutto di una scelta non irragionevole del legislatore delegato, comunque tale da non determinare una ingiustificata disparità di trattamento»; 
    che, inoltre, con specifico riferimento agli artt. 76 e 77 Cost., nella menzionata ordinanza si rileva che «il legislatore delegato ha delineato un procedimento già di per sé caratterizzato da una accentuata semplificazione rispetto al procedimento davanti al giudice monocratico» e che quindi «è proprio la struttura complessiva del procedimento davanti al giudice di pace, accompagnata da specifiche forme di definizione alternativa, che consente di escludere che la omessa previsione del patteggiamento integri una violazione della legge-delega»; 
    che non risultando evocati profili diversi o aspetti ulteriori rispetto a quelli già valutati da questa Corte con la pronuncia richiamata la questione deve essere dichiarata manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 2 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), sollevata, in riferimento agli artt. 3 e 111 della Costituzione, dal Giudice di pace di Cairo Montenotte con l'ordinanza in epigrafe; &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 2, 20 e 29 del medesimo decreto legislativo 28 agosto 2000, n. 274, sollevata, in riferimento agli artt. 3, 24 e 76 della Costituzione, dal Giudice di pace di Pisciotta con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 luglio 2005.  &#13;
F.to:  &#13;
Piero Alberto CAPOTOSTI, Presidente  &#13;
Guido NEPPI MODONA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 22 luglio 2005.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
