<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/68/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/68/"/>
          <FRBRalias value="ECLI:IT:COST:2005:68" name="ECLI"/>
          <FRBRdate date="13/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="68"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/68/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/68/ita@/!main"/>
          <FRBRdate date="13/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/68/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/68/ita@.xml"/>
          <FRBRdate date="13/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="29/01/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ONIDA</cc:presidente>
        <cc:relatore_pronuncia>Annibale Marini</cc:relatore_pronuncia>
        <cc:data_decisione>13/01/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Valerio ONIDA; Giudici: Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 46, comma 3, del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), promosso con ordinanza del 3 dicembre 2003 dalla Commissione tributaria regionale di Napoli sui ricorsi riuniti proposti da Esposito Enrica contro il Comune di San Giorgio a Cremano, iscritta al n. 505 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 23, prima serie speciale, dell'anno 2004. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 15 dicembre 2004 il Giudice relatore Annibale Marini. 
    Ritenuto che la Commissione tributaria regionale di Napoli, con ordinanza depositata il 3 dicembre 2003, nel corso di un giudizio di appello avente ad oggetto la statuizione relativa alla compensazione delle spese conseguente all'estinzione del giudizio per cessazione della materia del contendere, determinata dall'annullamento dell'atto impositivo in via di autotutela, ha sollevato, in riferimento all'art. 111 della Costituzione, questione di legittimità costituzionale dell'art. 46, comma 3, del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), «nella parte in cui è disposta la compensazione delle spese anche per il caso in cui la cessazione della materia del contendere consegua ad annullamento dell'atto impositivo disposta di ufficio dalla P.A., in sede di autotutela»; 
    che – ad avviso del rimettente – la norma impugnata, ponendo a carico del contribuente le spese anticipate per la lite anche quando la pubblica amministrazione, in corso di giudizio, ritiri o annulli l'atto impugnato, si porrebbe in contrasto con l'art. 111 della Costituzione e, in particolare, con i principi del giusto processo e dell'effettività della tutela giurisdizionale; 
    che la previsione di necessaria compensazione delle spese non sarebbe d'altro canto giustificata né dalle peculiarità del processo tributario né da esigenze di snellezza, in quanto l'accertamento della soccombenza virtuale non potrebbe dirsi di ostacolo ad una celere definizione della lite; 
    che sarebbe altresì violato – quanto meno con riguardo a ciascuna singola fattispecie processuale – il principio di parità tra le parti, essendo attribuita ad una di esse la possibilità di determinare, nel corso del processo, la cessazione della materia del contendere senza incorrere nella condanna alle spese; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la declaratoria di inammissibilità o, comunque, di non fondatezza della questione; 
    che – ad avviso dell'Avvocatura – l'ordinanza di rimessione sarebbe perplessa e contraddittoria quanto alla descrizione della fattispecie processuale; 
    che, nel merito, il giudice a quo, con riferimento al parametro di cui al nuovo art. 111 della Costituzione, in realtà riproporrebbe, invocando i principi del «giusto processo» e delle «condizioni di parità delle parti nel contraddittorio», le stesse censure già scrutinate dalla Corte, riguardo alla medesima norma, con riferimento agli artt. 3, 24, 75, 76, 97 e 113 della Costituzione, e giudicate non fondate; 
    che, d'altro canto, il rimettente non inquadrerebbe correttamente la fattispecie normativa, in quanto la norma denunciata riguarda indistintamente tutte le ipotesi di cessazione della materia del contendere ed il riferimento alla sola ipotesi derivante dall'adozione di un atto di autotutela da parte dell'amministrazione finanziaria sarebbe inadeguato per una congrua valutazione della legittimità della norma stessa nel suo complesso; 
    che non vi sarebbe, infine, alcun principio costituzionale che imponga la condanna alle spese di lite in caso di soccombenza anche solo virtuale.  
    Considerato che l'art. 46, comma 3, del decreto legislativo n. 546 del 1992 dispone la compensazione, tra le parti, delle spese del giudizio, estinto a norma del comma 1 dello stesso articolo, «nei casi di definizione delle pendenze tributarie previsti dalla legge e in ogni altro caso di cessazione della materia del contendere»; 
     che il rimettente invoca la declaratoria di illegittimità costituzionale della norma suddetta, con riferimento esclusivo all'ipotesi di cessazione della materia del contendere conseguente ad annullamento dell'atto impositivo disposto di ufficio dall'amministrazione finanziaria in sede di autotutela; 
    che la pronuncia di incostituzionalità, nei termini prospettati dal rimettente, determinerebbe una evidente lesione del principio di eguaglianza tra le parti, lasciando inalterata la disciplina della compensazione delle spese, prevista dalla norma censurata, per tutte le altre ipotesi di cessazione della materia del contendere, ed in particolare per quelle conseguenti al riconoscimento, da parte del contribuente, della fondatezza della pretesa tributaria; 
    che l'incostituzionalità conseguente all'accoglimento della questione rende quest'ultima improponibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 46, comma 3, del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), sollevata, in riferimento all'art. 111 della Costituzione, dalla Commissione tributaria regionale di Napoli, con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 gennaio 2005. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Annibale MARINI, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 29 gennaio 2005. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
