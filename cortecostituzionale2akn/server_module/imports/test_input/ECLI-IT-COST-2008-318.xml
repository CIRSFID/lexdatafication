<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/318/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/318/"/>
          <FRBRalias value="ECLI:IT:COST:2008:318" name="ECLI"/>
          <FRBRdate date="29/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="318"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/318/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/318/ita@/!main"/>
          <FRBRdate date="29/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/318/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/318/ita@.xml"/>
          <FRBRdate date="29/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="30/07/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maria Napolitano</cc:relatore_pronuncia>
        <cc:data_decisione>29/07/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale del combinato disposto degli artt. 511, 514 e 525, comma 2, del codice di procedura penale, promossi con quattro ordinanze del 12 gennaio 2006, tre ordinanze del 26 gennaio 2006 e due ordinanze del 9 febbraio 2006 dal Tribunale di Genova, iscritte ai nn. da 4 a 12 del registro ordinanze 2008 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 7, prima serie speciale, dell'anno 2008. 
      Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 9 luglio 2008 il Giudice relatore Paolo Maria Napolitano. 
    Ritenuto che il Tribunale di Genova, con nove ordinanze di identico tenore (r.o. nn. 4, 5, 6, 7, 8, 9, 10, 11 e 12 del 2008), ha sollevato questione di legittimità costituzionale, in riferimento agli artt. 3, 25, 101 e 111 della Costituzione, del combinato disposto degli artt. 511, 514 e 525, comma 2, del codice di procedura penale – come interpretati dalle sezioni unite della Corte di cassazione con sentenza del 15 gennaio 1999, n. 2 – nella parte in cui «non prevedono che, nel caso di mutamento totale o parziale del giudicante, le dichiarazioni assunte nella precedente istruzione dibattimentale, quando l'esame del dichiarante possa aver luogo e sia stato richiesto da una delle parti, siano utilizzabili per la decisione mediante semplice lettura, dopo l'applicazione degli artt. 190 e 190-bis cod. proc. pen.»; 
    che il rimettente, in punto di non manifesta infondatezza,  premette che le sezioni unite della Corte di cassazione hanno, con la sentenza 15 gennaio 1999, n. 2, affermato il principio che, nel caso di rinnovazione del dibattimento a causa del mutamento della persona del giudice monocratico o della composizione del giudice collegiale, la testimonianza raccolta dal giudicante nella sua originaria composizione, sebbene ritualmente trasfusa nei verbali agli atti del fascicolo per il dibattimento, non è utilizzabile per la decisione mediante semplice lettura, quando l'esame del dichiarante possa aver luogo e sia stato (anche solo genericamente) richiesto da una parte; 
    che, secondo il giudice a quo, l'interpretazione data dalla Cassazione non appare affatto imposta dalla lettera della norma, poiché la dizione «a meno che l'esame non abbia luogo», con la quale si conclude il comma 2 dell'art. 525 (recte art. 511) cod. proc. pen., può riferirsi anche all'ipotesi in cui, per qualsiasi motivo (tra cui l'esercizio dei poteri/doveri stabiliti dagli artt. 190 e 190-bis cod. proc. pen.), esso non abbia effettivamente luogo; 
    che tale interpretazione, prosegue il rimettente, si risolve nell'esaltazione dell'oralità quale apodittico canone e fonte di legittimità della prova, in un contesto sistematico in cui, per contro, non solo manca alcuna norma che consenta una tale conclusione, ma, addirittura, vi sono «plurime, inequivoche e insuperabili indicazioni del carattere solo tendenziale del principio dell'oralità, quali l'incidente probatorio e, soprattutto, il giudizio di appello»; 
    che, a suo dire, una conferma della possibile diversa lettura dell'art. 511, comma 2, cod. proc. pen. si ricaverebbe dal nuovo testo dell'art. 190-bis, comma 1, cod. proc. pen., come sostituito dall'art. 3 della legge n. 63 del 2001, in base al quale, quando le precedenti dichiarazioni siano state assunte nel contraddittorio con la parte nei cui confronti le dichiarazioni stesse devono essere utilizzate, «l'esame è ammesso solo se riguarda fatti o circostanze diversi da quelli oggetto delle precedenti dichiarazioni ovvero se il giudice o taluna delle parti lo ritengono necessario sulla base di specifiche esigenze»; 
    che, in definitiva, il rimettente ritiene che vi sia un'indicazione univoca e reiterata dell'oggettiva volontà del legislatore che siano pienamente utilizzati gli atti acquisiti al processo, nel rispetto delle norme e, in particolare, del contraddittorio, anche nel caso di mutamento della persona fisica del giudicante, in assenza di una precedente norma contraria; 
    che, pertanto, la norma censurata, imponendo il riesame del teste già sentito nel pieno rispetto del contraddittorio, senza l'indicazione specifica di ragioni da sottoporre al vaglio previsto dagli artt. 190 e 190-bis cod. proc. pen., determina una evidente disparità di trattamento, in contrasto con l'art. 3 Cost., laddove tale obbligo di riesame è escluso per situazioni di maggiore rischio per «la genuinità e terzietà» dell'acquisizione della prova; 
    che, a parere del Tribunale di Genova, l'integrale ripetizione di tutte le prove orali già assunte nella massima pienezza del contraddittorio, senza altra ragione che quella del garantire l'oralità quale mezzo necessario di conoscenza del giudice, concretizza una violazione anche degli artt. 25 e 101 Cost., parametri costituzionali che regolano l'esercizio della funzione giurisdizionale, consentendo di incidere negativamente anche sull'efficienza del processo (intesa quale necessaria attitudine del sistema processuale a conseguire, attraverso meccanismi normativi idonei allo scopo, l'accertamento dei fatti e delle responsabilità) costituente bene costituzionalmente tutelato; 
    che, infine, risulterebbe violato anche l'art. 111, secondo comma, Cost., poiché si determina un evidente allungamento della durata del processo, senza che alcuna ragione di tutela di beni e interessi, individuali o collettivi, tutelati costituzionalmente o anche solo da legge ordinaria, lo giustifichi; 
    che, quanto alla rilevanza della questione, in base all'attuale sistema si dovrebbe procedere alla rinnovazione dell'istruttoria dibattimentale con relativa ingiustificata dilatazione dei tempi del processo; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione venga dichiarata inammissibile o infondata; 
    che l'Avvocatura dello Stato evidenzia come identiche questioni di costituzionalità siano state già ampiamente affrontate dalla Corte costituzionale e dichiarate manifestamente infondate (vengono citate le ordinanze n. 67 del 2007, n. 418 del 2004 e n. 59 del 2002). 
    che, inoltre, il principio del buon andamento dell'amministrazione della giustizia è applicabile esclusivamente agli aspetti organizzativi del servizio e non alla disciplina del processo; 
    che, quanto al secondo profilo di illegittimità, a parere dell'Avvocatura dello Stato, non può essere condiviso il presupposto interpretativo da cui parte il rimettente, che porta a svalutare il principio del contraddittorio, inteso in termini oggettivi, nel senso cioè di metodo attraverso il quale il giudice, terzo ed imparziale, forma il proprio convincimento. 
    Considerato che il Tribunale di Genova, con nove ordinanze di identico tenore (r.o. nn. 4, 5, 6, 7, 8, 9, 10, 11 e 12 del 2008), ha sollevato questione di legittimità costituzionale, in riferimento agli artt. 3, 25, 101 e 111 della Costituzione, del combinato disposto degli artt. 511, 514 e 525, comma 2, del codice di procedura penale – come interpretati dalle sezioni unite della Corte di cassazione con sentenza del 15 gennaio 1999, n. 2 – nella parte in cui «non prevedono che, nel caso di mutamento totale o parziale del giudicante, le dichiarazioni assunte nella precedente istruzione dibattimentale, quando l'esame del dichiarante possa aver luogo e sia stato richiesto da una delle parti, siano utilizzabili per la decisione mediante semplice lettura, dopo l'applicazione degli artt. 190 e 190-bis cod. proc. pen.»; 
    che le ordinanze di rimessione sollevano la medesima questione di costituzionalità onde i relativi giudizi vanno riuniti per essere definiti con unica decisione;  
    che una questione identica a quella odierna è già stata sottoposta dal medesimo Tribunale all'esame di questa Corte, che l'ha dichiarata manifestamente infondata con l'ordinanza n. 67 del 2007; 
    che, in quell'occasione, la Corte ha avuto modo di ribadire che il legislatore, nel definire la disciplina del processo e la conformazione dei relativi istituti, gode di ampia discrezionalità, il cui esercizio è censurabile, sul piano della legittimità costituzionale, solo ove le scelte operate trasmodino nella manifesta irragionevolezza e nell'arbitrio (ex plurimis, sentenze n. 379 del 2005 e n. 180 del 2004; ordinanze n. 389 e n. 215 del 2005, n. 265 del 2004);  
    che la disciplina ricavabile dalle disposizioni sottoposte a scrutinio viene a correlarsi al principio di immediatezza, che ispira l'impianto del codice di rito e di cui la tradizionale regola dell'immutabilità del giudice rappresenta strumento attuativo; principio il quale postula – salve le deroghe espressamente previste dalla legge – l'identità tra il giudice che acquisisce le prove e quello che decide (ordinanze n. 431 e n. 399 del 2001);  
    che, inoltre, la norma censurata non può qualificarsi, di per sé, come manifestamente irrazionale ed arbitraria e che l'eventuale individuazione di presidi normativi volti a prevenirne il possibile uso strumentale e dilatorio è affidata alle scelte discrezionali del legislatore; 
    che non si concretizza alcuna lesione del principio di «non dispersione dei mezzi di prova», quale aspetto del bene dell'«efficienza del processo», riconducibile all'area di tutela degli artt. 25 e 101 Cost., giacché in nessun caso la prova dichiarativa precedentemente assunta va "dispersa", essendo sempre possibile acquisirla tramite lettura del relativo verbale: con l'unica differenza che, nel caso in cui il riesame del dichiarante sia possibile e la parte ne abbia fatto richiesta, la lettura dovrà seguire tale riesame; mentre, in caso contrario, la prova verrà recuperata a mezzo della sola lettura; 
    che il principio di ragionevole durata del processo (art. 111, secondo comma, Cost.) deve essere contemperato con il complesso delle altre garanzie costituzionali, rilevanti nel processo penale: garanzie la cui attuazione positiva – che il legislatore avrebbe inteso operare, nella specie, tramite la previsione di un regime allineato al principio di immediatezza – non è sindacabile sul terreno costituzionale, ove frutto di scelte non prive di una valida ratio giustificativa (ordinanze n. 418 del 2004 e n. 399 del 2001);  
    che non è ravvisabile neanche la violazione del principio di eguaglianza, avuto riguardo al diverso trattamento che – a parere dei rimettenti – la legge processuale riserverebbe a fattispecie identiche o similari, perchè l'art. 190-bis cod. proc. pen. non può essere utilmente evocato quale tertium comparationis, stante il suo carattere di eccezionalità (ordinanze n. 418 del 2004 e n. 73 del 2003); 
    che le presenti ordinanze di rimessione non aggiungono, rispetto alla precedente, profili nuovi o diversi di censura; 
    che, pertanto, la presente questione deve parimenti ritenersi manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale degli articoli 511, 514, 525, comma 2, del codice di procedura penale, sollevata, in riferimento agli artt. 3, 25, 101 e 111 della Costituzione, dal Tribunale di Genova, con le ordinanze indicate in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 29 luglio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Paolo Maria NAPOLITANO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 30 luglio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
