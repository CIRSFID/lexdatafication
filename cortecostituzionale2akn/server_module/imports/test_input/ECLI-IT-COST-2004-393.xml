<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/393/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/393/"/>
          <FRBRalias value="ECLI:IT:COST:2004:393" name="ECLI"/>
          <FRBRdate date="13/12/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="393"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/393/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/393/ita@/!main"/>
          <FRBRdate date="13/12/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/393/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/393/ita@.xml"/>
          <FRBRdate date="13/12/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="17/12/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ONIDA</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>13/12/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 1 del decreto-legge 8 febbraio 2003, n. 18 (Disposizioni urgenti in materia di giudizio necessario secondo equità), convertito, con modificazioni, nella legge 7 aprile 2003, n. 63, promossi con ordinanze del 4 novembre 2003 dal Giudice di pace di Genzano di Roma e del 26 settembre 2003 dal Giudice di pace di Napoli nei procedimenti civili vertenti tra S. P. e La Fondiaria – SAI s.p.a. e tra B. L. e le Assicurazioni Generali s.p.a., iscritte rispettivamente ai numeri 307 e 571 del registro ordinanze 2004 e pubblicate nella Gazzetta Ufficiale della Repubblica numeri 17 e 25, prima serie speciale, dell'anno 2004. 
    Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 17 novembre 2004 il Giudice relatore Francesco Amirante. 
    Ritenuto che nel corso di un giudizio instaurato, con atto di citazione per l'udienza del 16 maggio 2003, nei confronti della s.p.a. La Fondiaria – SAI per ottenerne la condanna alla ripetizione di una somma versata per un premio relativo ad una polizza per responsabilità civile autoveicoli, risultata in eccedenza in conseguenza della costituzione di un “cartello” tra le imprese del settore già sanzionato dall'Autorità garante della concorrenza e del mercato, il Giudice di pace di Genzano di Roma ha sollevato, con ordinanza del 4 novembre 2003 (r.o. n. 307 del 2004), questione di legittimità costituzionale, in riferimento agli artt. 3 e 24 Cost., dell'art. 1 della legge 7 aprile 2003, n. 63 (recte: dell'art. 1 del decreto-legge 8 febbraio 2003, n. 18, recante “Disposizioni urgenti in materia di giudizio necessario secondo equità”, convertito, con modificazioni, nella legge 7 aprile 2003, n. 63), nella parte in cui, sostituendo il secondo comma dell'art. 113 cod. proc. civ., esclude il giudizio secondo equità per tutte le controversie relative ai contratti conclusi secondo le modalità di cui all'art. 1342 cod. civ., indipendentemente dal loro valore; 
    che il giudice a quo, per quel che riguarda la rilevanza della questione, precisa che dalla sua definizione dipende l'applicabilità o meno, al giudizio in corso, del vecchio testo del secondo comma dell'art. 113 del codice di procedura civile;  
    che il remittente, dall'insieme delle norme che hanno via via disciplinato il giudizio secondo equità ampliandone la sfera di applicazione e dalla sentenza n. 716 del 1999 delle Sezioni unite della Corte di cassazione, desume che la possibilità di ottenere una decisione secondo equità «nelle cause di valore non superiore al vecchio milione di lire» – nelle quali, non a caso, l'art. 82, primo comma, cod. proc. civ., consente alla parte di stare in giudizio personalmente – è una delle particolari configurazioni del diritto di difesa costituzionalmente garantito; 
    che, quanto alla violazione del principio di uguaglianza, la norma impugnata introduce, ad avviso del remittente, una disparità di trattamento tra i giudizi che si svolgono davanti al giudice di pace, irragionevole in quanto non giustificata dalla specificità della materia trattata, ma dal tipo di contratto utilizzato, che è suscettibile di essere applicato da parte del contraente “forte” a qualsiasi tipologia di transazione commerciale, con evidente danno dei contraenti deboli (cioè dei consumatori); 
    che, d'altra parte, la natura di contraenti “forti” rivestita dai grandi gruppi di imprese che gestiscono i contratti di massa – contratti che riguardano, nei casi di maggior rilievo, prestazioni delle quali il singolo cittadino non può assolutamente fare a meno – dà conto della ragione per la quale, in simili materie, è indispensabile poter usufruire del giudizio secondo equità, che garantisce al consumatore una giustizia del caso concreto, non fondata sulla necessità della rigorosa applicazione delle norme di diritto, ed è in grado di tutelare, senza spese, la posizione dei soggetti meno abbienti; 
    che, conseguentemente, la possibilità che in simili casi venga introdotto il giudizio di appello (a seguito dell'eliminazione del giudizio di equità) andrebbe, altresì, a vanificare la tutela costituzionale del diritto di difesa di tali ultimi soggetti, in contrasto con l'art. 24, terzo comma, Cost.; 
    che, nel corso di un giudizio instaurato nei confronti della s.p.a. Generali Assicurazioni, il Giudice di pace di Napoli, con ordinanza del 26 settembre 2003 (r.o. n. 571 del 2004), ha sollevato, in riferimento all'art. 24, terzo comma, Cost., questione di legittimità costituzionale dell'art. 1 del d.l. n. 18 del 2003, nella parte in cui istituisce il grado di appello nelle controversie relative ai c.d. contratti di massa anche di importo inferiore ai millecento euro e determina così una limitazione dell'ambito di applicabilità dell'art. 82, primo comma, cod. proc. civ. rispetto ai soggetti non abbienti; 
    che il remittente, in punto di rilevanza della questione, si limita ad affermare che la risoluzione della stessa condiziona la definizione del giudizio in corso, del quale, peraltro, non fornisce alcuna concreta indicazione; 
    che, nel merito, il giudice a quo osserva, principalmente, che con l'entrata in vigore del d.l. n. 18 del 2003, essendo stato istituito il doppio grado di giudizio anche per le cause di valore inferiore ai millecento euro riguardanti contratti di massa, non solo si è limitato in modo sostanziale il diritto di difesa dei non abbienti garantito dal parametro invocato, ma si è altresì esposto lo Stato all'eventualità di un eccessivo esborso per spese di giustizia (per il gratuito patrocinio), a fronte di cause di modico valore; 
    che in entrambi i giudizi davanti alla Corte è intervenuto, con atti di contenuto identico, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha chiesto una dichiarazione di inammissibilità delle questioni in ragione del fatto che, nei giudizi a quibus, le parti private sono assistite da avvocati, così dimostrando di non essere “non abbienti”, e ha sostenuto, nel merito, la manifesta infondatezza delle censure, in quanto sarebbe poco convincente l'equazione istituita dai giudici remittenti tra causa di modico valore e causa del non abbiente, così come sarebbero del tutto ipotetiche le critiche relative a maggiori esborsi cui si esporrebbe lo Stato e, comunque, in quanto la posizione del contraente “debole” avrebbe maggiore tutela in un giudizio secondo diritto che non in un giudizio secondo equità. 
    Considerato che il Giudice di pace di Genzano di Roma dubita, in riferimento agli artt. 3 e 24 Cost., della legittimità costituzionale dell'art. 1 del decreto-legge 8 febbraio 2003, n. 18 (Disposizioni urgenti in materia di giudizio necessario secondo equità), convertito con modificazioni, dalla legge 7 aprile 2003, n. 63, nella parte in cui sottrae al giudizio secondo equità tutte le controversie relative ai contratti conclusi secondo le modalità di cui all'art. 1342 cod. civ., indipendentemente dal loro valore; 
    che analoga questione è prospettata, in riferimento al solo art. 24, terzo comma, Cost., dal Giudice di pace di Napoli con riguardo all'art. 1 del d.l. n. 18 del 2003, per la parte in cui istituisce il grado di appello nelle controversie relative ai c.d. contratti di massa anche di importo inferiore ai millecento euro e determina così una limitazione dell'ambito di applicabilità dell'art. 82, primo comma, cod. proc. civ. in favore dei soggetti non abbienti; 
    che le due ordinanze pongono questioni analoghe, sicché i relativi giudizi vanno riuniti per essere congiuntamente decisi; 
    che l'ordinanza del Giudice di pace di Napoli, che non contiene alcun riferimento alla legge di conversione n. 63 del 2003 pur essendo ad essa successiva, non fornisce neppure alcuna descrizione della fattispecie concreta oggetto del giudizio a quo, onde la questione da essa sollevata va dichiarata manifestamente inammissibile per carenza di motivazione sulla rilevanza, solo apoditticamente affermata dal remittente (v., da ultimo, ordinanza n. 302 del 2004); 
    che anche la motivazione sulla rilevanza contenuta nell'ordinanza del Giudice di pace di Genzano di Roma non può considerarsi esauriente in quanto, ancorché in essa si richiami espressamente la legge n. 63 del 2003, non viene tuttavia precisata la data di notifica dell'atto di citazione, mentre tale elemento è necessario per lo svolgimento del prescritto controllo preliminare sulla rilevanza, dal momento che l'art. 1-bis del citato d.l. n. 18 del 2003, introdotto dalla legge di conversione n. 63 del 2003, pubblicata nella Gazzetta Ufficiale del 10 aprile 2003, ha stabilito che le disposizioni di cui all'art. 1 dello stesso decreto si applicano ai giudizi instaurati con citazioni notificate dal 10 febbraio 2003; 
    che, pertanto, anche la questione sollevata dal Giudice di pace di Genzano di Roma deve essere dichiarata manifestamente inammissibile (v. ord. n. 299 del 2004). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 1 del decreto-legge 8 febbraio 2003, n. 18 (Disposizioni urgenti in materia di giudizio necessario secondo equità), convertito, con modificazioni, nella legge 7 aprile 2003, n. 63, sollevate, in riferimento agli artt. 3 e 24 della Costituzione, dai Giudici di pace di Genzano di Roma e di Napoli con le ordinanze indicate in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 dicembre 2004. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Francesco AMIRANTE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 17 dicembre 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
