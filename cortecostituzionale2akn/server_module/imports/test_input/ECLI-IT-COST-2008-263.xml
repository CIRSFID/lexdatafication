<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/263/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/263/"/>
          <FRBRalias value="ECLI:IT:COST:2008:263" name="ECLI"/>
          <FRBRdate date="07/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="263"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/263/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/263/ita@/!main"/>
          <FRBRdate date="07/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/263/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/263/ita@.xml"/>
          <FRBRdate date="07/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/07/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>07/07/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), e dell'art. 10 della stessa legge, promossi, con ordinanze del 20 aprile e dell'8 agosto 2006, dalla Corte d'appello di Trieste nei procedimenti penali a carico di L. A. e di M. F., iscritte ai nn. 136 e 330 del registro ordinanze 2007 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 13 e 19, prima serie speciale, dell'anno 2007. 
    Udito nella camera di consiglio del 21 maggio 2008 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che, con due ordinanze sostanzialmente coincidenti nella parte motiva (r.o. nn. 136 e 330 del 2007), la Corte di appello di Trieste ha sollevato, in riferimento agli artt. 3 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento) – nella parte in cui non consente al pubblico ministero di proporre appello avverso le sentenze di proscioglimento, se non nel caso previsto dall'art. 603, comma 2, cod. proc. pen., ossia quando sopravvengano o si scoprano nuove prove dopo il giudizio di primo grado e sempre che tali prove risultino decisive – e dell'art. 10 della medesima legge, recante il relativo regime transitorio; 
    che la Corte rimettente premette di essere investita degli appelli proposti dal pubblico ministero, prima dell'entrata in vigore della legge n. 46 del 2006, avverso sentenze di non luogo a procedere pronunciate (per difetto di querela e perché il fatto non è previsto dalla legge come reato) rispettivamente dal Giudice per le indagini preliminari, in funzione del Giudice dell'udienza preliminare, del Tribunale di Gorizia (r.o. n. 136 del 2007) e di Udine (r.o. n. 330 del 2007); 
    che in forza dell'art. 10 della legge n. 46 del 2006 – il cui art. 1, sostituendo l'art. 593 cod. proc. pen., ha sottratto al pubblico ministero il potere di appellare le sentenze di proscioglimento – i giudizi dovrebbe essere definiti con ordinanze non impugnabili di inammissibilità; 
    che, nel merito, la disciplina censurata si porrebbe in contrasto con gli artt. 3 e 111 Cost., per violazione dei principi della parità fra le parti e della ragionevole durata del processo; 
    che, quanto al primo profilo, la Corte d'appello rimettente rileva che, secondo la costante giurisprudenza della Corte costituzionale, la previsione di limiti al potere di impugnazione del pubblico ministero – di per sé non in contrasto con la Costituzione – deve trovare una «ragionevole giustificazione» nella peculiare posizione istituzionale del pubblico ministero, nella funzione allo stesso affidata e nelle esigenze connesse alla corretta amministrazione della giustizia; 
    che il giudice a quo osserva che nei lavori preparatori della legge, e segnatamente nella relazione di accompagnamento alla proposta di legge, le ragioni dell'intervento normativo sono ricondotte esclusivamente alla necessità di dare attuazione al principio affermato dall'art. 2 del Protocollo addizionale n. 7 alla Convenzione europea per la salvaguardia dei diritti e delle libertà fondamentali, adottato a Strasburgo il 22 novembre 1984 ratificato e reso esecutivo con legge 9 aprile 1990, n. 98, con riferimento al «diritto al doppio grado di giurisdizione in materia penale per chiunque venga dichiarato colpevole di una infrazione penale da un tribunale»; 
    che tali ragioni si paleserebbero, tuttavia, non solo estranee a quelle che, secondo la giurisprudenza richiamata, potrebbero legittimare una limitazione dei poteri di impugnazione del pubblico ministero, ma anche «del tutto prive di fondamento», atteso che la Corte costituzionale ha ripetutamente affermato che «il doppio grado di giurisdizione di merito non forma oggetto di garanzia costituzionale» e che l'art. 2 sopra menzionato «non legittima una interpretazione per cui il riesame ad opera di un tribunale superiore debba coincidere con un giudizio di merito»; 
    che la limitazione del potere di impugnazione del pubblico ministero non sarebbe giustificata neppure dalla circostanza che l'appello è formalmente precluso anche all'imputato, «ben diverso essendo il rispettivo interesse sostanziale a proporre impugnazione avverso una sentenza di proscioglimento»; 
    che, pertanto, la disciplina censurata, in quanto introduce una limitazione dei poteri del pubblico ministero priva di idonee ragioni giustificative, si porrebbe in contrasto con gli artt. 3 e 111 Cost.;  
    che sarebbe, inoltre, violato il principio della durata ragionevole del processo sancito dall'art. 111, secondo comma, Cost., atteso che la legge n. 46 del 2006, eliminando l'appello avverso le sentenze di proscioglimento e ampliando i motivi di ricorso per cassazione, ha determinato un aumento dei gradi di giudizio, con conseguente allungamento dei tempi processuali e rischio di prescrizione dei reati; 
    che ciò risulterebbe tanto più evidente in relazione alla disciplina transitoria contenuta nell'art. 10 della legge n. 46 del 2006, poiché la previsione di una «indiscriminata declaratoria di inammissibilità» degli appelli proposti prima dell'entrata in vigore della legge, «derogando al principio tempus regit actum che governa la materia processuale, non solo sacrifica ineludibilmente un atto di gravame tempestivamente proposto, costringendo la parte interessata a presentarne un altro, ma comporta l'inevitabile differimento della presentazione di esso all'eseguita notifica del provvedimento di inammissibilità e, pertanto, ad un termine futuro ed incerto». 
    Considerato che la Corte d'appello di Trieste dubita, in riferimento agli artt. 3 e 111 della Costituzione, della legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), nella parte in cui non consente al pubblico ministero di proporre appello avverso le sentenze di proscioglimento, al di fuori delle ipotesi previste dall'art. 603, comma 2, dello stesso codice, e dell'art. 10 della medesima legge; 
    che, stante l'identità delle questioni proposte, i relativi giudizi vanno riuniti per essere decisi con unica pronuncia; 
    che l'art. 593 cod. proc. pen. disciplina, al comma 2, l'appello del pubblico ministero e dell'imputato avverso le sentenze dibattimentali di proscioglimento, stabilendo - per effetto delle modifiche introdotte dall'art. 1 della legge n. 46 del 2006 ed immediatamente applicabili in forza dell'art. 10 della medesima legge - che l'appello è consentito solo nell'ipotesi di cui all'art. 603, comma 2, cod. proc. pen., se la nuova prova è decisiva;  
    che dalle ordinanze di rimessione risulta che la Corte rimettente è investita degli appelli proposti dal pubblico ministero avverso sentenze di non luogo a procedere pronunciate, ai sensi dell'art. 425 cod. proc. pen., dal Giudice per le indagini preliminari, in funzione di Giudice dell'udienza preliminare; 
    che il regime di impugnazione delle sentenze di non luogo a procedere è disciplinato dall'art. 428 cod. proc. pen. (sostituito dall'art. 4 della legge n. 46 del 2006); 
    che, dunque, la Corte rimettente sottopone a scrutinio di costituzionalità una norma (l'art. 593 cod. proc. pen.) – unitamente al relativo regime transitorio – di cui non deve fare applicazione nei giudizi a quibus; 
    che l'inesatta indicazione della norma oggetto di censura (aberratio ictus) comporta, per costante giurisprudenza di questa Corte, la manifesta inammissibilità della questione (ex plurimis, ordinanze n. 79 e 150 del 2008).  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi  &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi,  &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), e dell'art. 10 della medesima legge, sollevate, in riferimento agli art. 3 e 111 della Costituzione, dalla Corte d'appello di Trieste, con le ordinanze in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 luglio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 10 luglio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
