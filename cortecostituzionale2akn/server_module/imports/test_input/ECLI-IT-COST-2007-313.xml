<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/313/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/313/"/>
          <FRBRalias value="ECLI:IT:COST:2007:313" name="ECLI"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="313"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/313/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/313/ita@/!main"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/313/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/313/ita@.xml"/>
          <FRBRdate date="10/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="20/07/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Franco Gallo</cc:relatore_pronuncia>
        <cc:data_decisione>10/07/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 2 e 3, commi 1 e 2, della legge della Regione Piemonte 21 aprile 2006, n. 14 (Legge finanziaria per l'anno 2006), promosso con ricorso del Presidente del Consiglio dei ministri notificato il 14 giugno 2006, depositato in cancelleria il 20 giugno 2006 ed iscritto al n. 77 del registro ricorsi 2006. 
    Udito nella camera di consiglio del 4 luglio 2007 il Giudice relatore Franco Gallo. 
    Ritenuto che, con ricorso notificato il 14 giugno 2006 e depositato il 20 giugno successivo, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso – in riferimento agli artt. 117, secondo comma, lettera e), e 119 della Costituzione – questioni di legittimità costituzionale: a) dell'art. 2 della legge della Regione Piemonte 21 aprile 2006, n. 14 (Legge finanziaria per l'anno 2006), in relazione agli articoli 2, 3, 4, 16 e 24 del decreto legislativo 15 dicembre 1997, n. 446 (Istituzione dell'imposta regionale sulle attività produttive, revisione degli scaglioni, delle aliquote e delle detrazioni dell'Irpef e istituzione di una addizionale regionale a tale imposta, nonché riordino della disciplina dei tributi locali); b) dell'art. 3, comma 1, della stessa legge regionale n. 14 del 2006, in relazione all'art. 17, comma 16, della legge 27 dicembre 1997, n. 449 (Misure per la stabilizzazione della finanza pubblica), al decreto interministeriale 27 dicembre 1997 (Tariffe delle tasse automobilistiche) e all'art. 24 del decreto legislativo 30 dicembre 1992, n. 504 (Riordino della finanza degli enti territoriali, a norma dell'articolo 4 della legge 23 ottobre 1992, n. 421); c) dell'art. 3, comma 2, della medesima legge regionale, in relazione alla legge 21 novembre 2000, n. 342 (Misure in materia fiscale), «in particolare art. 63»;  
    che il ricorrente, dopo una sintetica disamina del contenuto delle norme denunciate, rileva che esse illegittimamente dispongono agevolazioni o mutano i presupposti di imposta in relazione a tributi statali, quali l'IRAP e le tasse automobilistiche; 
    che, in particolare, quanto al censurato art. 2 della legge n. 14 del 2006, il ricorrente lamenta che il legislatore regionale ha configurato una temporanea esenzione dall'IRAP per le «aziende della filiera avicola», in tal modo introducendo una deroga rispetto al regime sostanziale di detta imposta quale contenuto nel citato d.lgs. n. 446 del 1997, ed in particolare nell'art. 3, che individua i soggetti passivi del tributo con riferimento al precedente art. 2 e non prevede l'esonero dal pagamento dell'imposta per le aziende della filiera avicola; 
    che, disponendo tale esenzione, il legislatore regionale avrebbe esorbitato dal limitato spazio di autonomia normativa regionale, delineato dagli artt. 16 e 24 del medesimo d.lgs. n. 446 del 1997, violando così la competenza legislativa esclusiva, attribuita in materia allo Stato dagli evocati parametri; 
    che, quanto al censurato art. 3, comma 1, della legge n. 14 del 2006, il ricorrente lamenta che il legislatore regionale ha operato una modifica sostanziale del criterio di imposizione previsto dalla legge statale in materia di tasse automobilistiche, in quanto ha previsto un'articolata modulazione dell'ammontare del tributo in funzione unicamente del numero dei kilowatt del veicolo, eliminando il riferimento alla diversa tipologia dei veicoli quale indicata dalla disciplina statale;  
    che, così facendo, il legislatore regionale si sarebbe sostanzialmente discostato da quanto previsto a livello statale dal d.lgs. n. 504 del 1992 ed avrebbe, perciò, violato l'art. 117, secondo comma, lettera e), Cost.; 
    che secondo la difesa erariale, infatti, «se è vero che ex art. 24, comma 1 del d.lgs. n. 504 del 1992 […] sussiste una limitata potestà normativa della regione in materia, è altrettanto vero ed incontestabile che essa può spaziare solo nei limiti della forbice di variazione (dal 90 al 110 per cento) della misura degli importi della tassa vigenti nell'anno precedente, ma non si estende certamente alla non consentita sostituzione del criterio di tassazione previsto dal legislatore statale con altro criterio al medesimo non conforme»; 
    che, quanto al censurato art. 3, comma 2, della legge n. 14 del 2006, il ricorrente lamenta che il legislatore regionale, in violazione degli evocati parametri, ha disposto l'esenzione dalla tassa automobilistica regionale per le «motociclette ed auto storiche, iscritte ai rispettivi albi», senza peraltro alcuna specificazione di quali «albi» si tratti, e soprattutto senza richiedere che la relativa iscrizione sia operata previa individuazione dei relativi requisiti soggettivi ed oggettivi con determinazione dell'Automobilclub storico italiano o della Federazione motociclistica italiana; 
    che per contro – evidenzia la difesa erariale – la norma statale che prevede agevolazioni in materia di tasse automobilistiche (art. 63 della legge n. 342 del 2000) ha riguardo solo ai veicoli di particolare interesse storico e non di mero interesse storico (oltre che a quelli di particolare interesse collezionistico, ignorati dalla norma regionale) e postula che la loro individuazione, in ragione dei requisiti soggettivi e oggettivi previsti dalla legge, sia operata, con propria determinazione, dall'Automobilclub storico italiano e, per i motoveicoli, anche dalla Federazione motociclistica italiana; 
    che la Regione Piemonte non si è costituita nel presente giudizio; 
    che, il 27 febbraio 2007, l'Avvocatura generale dello Stato, nell'interesse del Presidente del Consiglio dei ministri, ha depositato una memoria nella quale ha dichiarato di rinunciare al ricorso, in quanto le disposizioni censurate sono state abrogate dall'art. 13, comma 2, della legge regionale 13 novembre 2006, n. 35 (Assestamento al bilancio di previsione per l'anno finanziario 2006 e modifiche della legge regionale 21 aprile 2006, n. 14 – legge finanziaria per l'anno 2006). 
    Considerato che, in mancanza di costituzione in giudizio della parte resistente, la rinuncia al ricorso comporta, ai sensi dell'art. 25 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (ex plurimis, ordinanze n. 11, n. 99, n. 163 e n. 418 del 2006).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi  &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>  &#13;
    dichiara estinto il processo. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 luglio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Franco GALLO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 20 luglio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
