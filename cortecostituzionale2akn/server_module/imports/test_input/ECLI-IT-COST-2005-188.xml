<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/188/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/188/"/>
          <FRBRalias value="ECLI:IT:COST:2005:188" name="ECLI"/>
          <FRBRdate date="02/05/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="188"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/188/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/188/ita@/!main"/>
          <FRBRdate date="02/05/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/188/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/188/ita@.xml"/>
          <FRBRdate date="02/05/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="04/05/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CAPOTOSTI</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>02/05/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Piero Alberto CAPOTOSTI; Giudici: Fernanda CONTRI, Guido NEPPI MODONA, Annibale MARINI, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 459, comma 1, del codice di procedura penale, promosso con ordinanza del 22 ottobre 2003 dal Tribunale di Roma nel procedimento penale a carico di C.A., iscritta al n. 1144 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 2, prima serie speciale, dell'anno 2004. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri. 
    Udito nella camera di consiglio del 6 aprile 2005 il Giudice relatore Giovanni Maria Flick.  
    Ritenuto che il Tribunale di Roma, in sede di opposizione a decreto penale di condanna, ha sollevato, in riferimento agli artt. 24 e 111, secondo e terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 459, comma 1, del codice di procedura penale, «nella parte in cui non prevede una sanzione processuale alla inosservanza del termine di sei mesi per la presentazione della richiesta di decreto penale»; 
    che il giudice  a quo – dopo aver sottolineato, in punto di fatto, che nella specie l'iscrizione del nominativo dell'imputato nel registro delle notizie di reato risale al 1999; mentre la richiesta di emissione del decreto penale di condanna, poi opposto, è stata depositata presso la cancelleria del giudice per le indagini preliminari il 12 gennaio 2001, e quindi ben oltre la scadenza del termine previsto dall'art. 459, comma 1, cod. proc. pen. – ha sottolineato come tale termine, secondo la ormai consolidata giurisprudenza di legittimità, abbia natura meramente ordinatoria; con la conseguenza che, essendo la sua inosservanza priva di sanzione processuale, «il decreto di condanna emesso dal giudice a fronte di una richiesta tardiva non è comunque invalido né può essere revocato»; 
    che, ad avviso del giudice rimettente, tuttavia, la mancanza di sanzioni per la ipotesi di inosservanza del termine di sei mesi, entro il quale deve essere formulata la richiesta del decreto penale di condanna, verrebbe a ledere, ad un tempo, sia il diritto di difesa, poiché il diverso atteggiarsi di esso nel procedimento monitorio trova la sua giustificazione – secondo quanto affermato da questa Corte – nella specificità di tale procedimento, «improntato a criteri di economia processuale e di massima speditezza»; sia il principio della durata ragionevole del processo – sancito dall'art. 111, secondo comma, Cost. – «in quanto non porrebbe limiti temporali sanzionabili ad un procedimento speciale a struttura estremamente semplificata, quale il procedimento per decreto»; sia, infine, il principio in forza del quale l'accusato ha diritto ad essere informato nel più breve tempo possibile della natura e dei motivi della accusa  a suo carico – a norma dell'art. 111, terzo comma, della medesima Carta – «in quanto questo diritto verrebbe ad essere ritardato senza limiti di tempo»; 
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dalla Avvocatura generale dello Stato, la quale ha chiesto dichiararsi infondata la questione, sul rilievo che la inosservanza del termine di sei mesi, di cui alla norma impugnata, risulta comunque assoggettata alla generale sanzione prevista per il mancato rispetto del termine per le indagini preliminari: sanzione costituita dalla inutilizzabilità degli atti di indagine compiuti dal pubblico ministero successivamente alla scadenza del termine stesso. 
    Considerato che il Tribunale di Roma dubita della legittimità costituzionale dell'art. 459, comma 1, cod. proc. pen., nella parte in cui – alla stregua della interpretazione ad esso data dal «diritto vivente» – non è prevista alcuna sanzione di natura processuale per la ipotesi in cui la richiesta di emissione del decreto penale di condanna sia stata formulata, dal pubblico ministero, dopo lo spirare del termine di sei mesi dalla data in cui il nominativo della persona, cui il reato è attribuito, è stato iscritto nel registro delle notizie di reato; 
    che, ad avviso del giudice rimettente, tale consolidata interpretazione del dato normativo si porrebbe in contrasto con il diritto di difesa, poiché il differimento del relativo esercizio alla fase della opposizione trova ragion d'essere e giustificazione, sul piano costituzionale, solo in rapporto ai caratteri di economia processuale e di massima celerità che dovrebbero caratterizzare il procedimento per decreto; 
    che, allo stesso modo,  risulterebbero compromessi anche il principio della durata ragionevole del processo e quello del diritto dell'imputato ad essere tempestivamente informato della accusa a suo carico – rispettivamente previsti dal secondo e dal terzo comma dell'art. 111 Cost. – in quanto la mancanza di apposita sanzione determinerebbe una dilazione  sine die dell'iter processuale; 
    che, tuttavia, alla stregua dell'indicato quadro di riferimento, il giudice rimettente – anziché individuare uno specifico quesito di costituzionalità, delineando con esattezza la pronuncia additiva sollecitata in riferimento alla norma o alle norme coinvolte nel dubbio di legittimità – si è limitato, tanto nella motivazione che nel dispositivo della ordinanza di rimessione, a devolvere a questa Corte sia la scelta del tipo di sanzione da configurare, in presenza del superamento del limite temporale stabilito dall'art. 459, comma 1, del codice di rito; sia la scelta dell'atto o degli atti su cui essa dovrebbe produrre effetti; sia, infine, la scelta delle conseguenze che, quale epilogo delle già indicate opzioni,  dovrebbero scaturire sul piano processuale; 
    che, infatti, altro sarebbe configurare come inammissibile la richiesta di decreto penale di condanna, ove formulata “tardivamente” dal pubblico ministero; altro è prevedere la nullità – determinandone, poi, il relativo regime – o della richiesta in quanto tale, o dello stesso decreto penale di condanna; salvo poi a verificare, in tale ultima ipotesi, se da essa debba o meno conseguire una statuizione con effetti regressivi per l'intero procedimento (situazione, questa, che è l'unica a presentare  rilevanza per il giudice  a quo), anche nella ipotesi in cui il vizio sia rilevato nel corso del giudizio di opposizione, considerato che quest'ultimo postula in ogni caso il venir meno del decreto penale di condanna, che deve essere revocato a norma dell'art. 464, comma 3, cod. proc. pen.; 
    che, pertanto, essendo del tutto indeterminato il petitum formalmente devoluto a questa Corte e risultando, altresì, contraddittorio ed ambiguo lo stesso quadro normativo che il rimettente intenderebbe veder modificato in ipotesi di accoglimento del quesito, la questione proposta deve essere dichiarata manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 459, comma 1, del codice di procedura penale, sollevata, in riferimento agli artt. 24 e 111, secondo e terzo comma, della Costituzione, dal Tribunale di Roma con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 2 maggio 2005. &#13;
F.to: &#13;
Piero Alberto CAPOTOSTI, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 4 maggio 2005. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
