<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/205/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/205/"/>
          <FRBRalias value="ECLI:IT:COST:2010:205" name="ECLI"/>
          <FRBRdate date="07/06/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="205"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/205/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/205/ita@/!main"/>
          <FRBRdate date="07/06/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2010/205/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2010/205/ita@.xml"/>
          <FRBRdate date="07/06/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/06/2010" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2010</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>07/06/2010</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Francesco AMIRANTE; Giudici : Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 525, comma 2, del codice di procedura penale, promosso dal Tribunale di Roma nel procedimento penale a carico di A. M., con ordinanza del 28 aprile 2009, iscritta al n. 279 del registro ordinanze 2009 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 47, prima serie speciale, dell'anno 2009.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 26 maggio 2010 il Giudice relatore Giuseppe Frigo.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che, con ordinanza del 28 aprile 2009, il Tribunale di Roma, in composizione monocratica, ha sollevato, in riferimento agli artt. 3, 101 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 525, comma 2, del codice di procedura penale, «nella parte in cui prevede che alla deliberazione debbano concorrere a pena di nullità assoluta gli stessi giudici che hanno partecipato al dibattimento»;
 che il rimettente premette che, nel giudizio a quo, una parte dell'istruzione dibattimentale, consistita nell'escussione di alcuni testi, è stata espletata dinanzi ad altro giudice-persona fisica, che in precedenza era «assegnatario del ruolo»;
 che a fronte del consenso del pubblico ministero all'utilizzazione delle prove già assunte ai sensi dell'art. 511 cod. proc. pen., la difesa ha invece chiesto che l'istruzione dibattimentale svolta venga rinnovata in ossequio all'art. 525, comma 2, cod. proc. pen.;
 che il giudice a quo dubita, tuttavia, della legittimità costituzionale di tale disposizione sotto plurimi profili;
 che il rimettente assume preliminarmente che il problema della perdurante valenza probatoria dell'attività istruttoria dibattimentale già compiuta, nel caso di mutamento della persona fisica del giudicante, sarebbe stato già risolto dalla Corte costituzionale in senso opposto a quello indicato dalle sezioni unite della Corte di cassazione nella sentenza 15 gennaio 1999-17 febbraio 1999, n. 2;
 che, infatti, la Corte costituzionale, con la sentenza n. 17 del 1994, ha dichiarato infondata la questione di legittimità costituzionale della disposizione combinata degli artt. 238 e 512 cod. proc. pen., ritenendo ammissibile, ai sensi dell'art. 511 cod. proc. pen., l'acquisizione mediante lettura (o indicazione sostitutiva) dei verbali delle dichiarazioni rese dai testi escussi dinanzi al precedente collegio o al diverso giudice-persona fisica, a prescindere dall'esame del dichiarante (come confermerebbe altresì l'ordinanza di questa Corte n. 99 del 1996);
 che - ciò posto - la disposizione censurata si porrebbe in contrasto, anzitutto, con i principi di eguaglianza e di ragionevolezza (art. 3 Cost.), tenuto conto del fatto che l'utilizzabilità di atti di natura probatoria formatisi davanti ad un diverso giudice è prevista da numerose disposizioni del codice di rito;
 che essa è considerata, in specie, nell'art. 238 cod. proc. pen., il quale disciplina l'acquisizione dei verbali di prove di altri procedimenti penali, assunte tanto nell'incidente probatorio che nel dibattimento; nell'art. 26 cod. proc. pen., secondo cui i verbali delle prove assunte dinanzi ad altro giudice, incompetente per materia, conservano la loro validità; nell'art. 33-nonies cod. proc. pen., in forza del quale l'inosservanza delle disposizioni sulla composizione collegiale o monocratica del tribunale non determina l'inutilizzabilità delle prove già acquisite; e, ancora, nell'art. 42 cod. proc. pen., ove si prevede che, nelle ipotesi di astensione e ricusazione, il provvedimento che accoglie la relativa dichiarazione stabilisce se ed in quale parte gli atti compiuti dinanzi al giudice astenutosi o ricusato conservino efficacia;
 che a maggior ragione, pertanto, nell'ipotesi «fisiologica» di mutamento della persona fisica del giudice - ad esempio, per effetto di tramutamenti o aspettative - gli atti in questione dovrebbero rimanere efficaci;
 che la norma impugnata si porrebbe in contrasto anche con l'art. 101 Cost., in base al quale - così come è letto dal rimettente - «tutti i giudici sono uguali dinanzi alla legge»; 
 che il giudice chiamato a sostituire il collega dovrebbe essere considerato, perciò, «a quest'ultimo uguale», tanto più che non si versa nemmeno nella situazione di «sospetto» contemplata dall'art. 42 cod. proc. pen., con riguardo ai casi di astensione o di ricusazione; 
 che detto principio non sarebbe, di contro, rispettato nella situazione in esame, stante il diverso trattamento riservato al giudice subentrato rispetto a quello previsto per i casi, dianzi indicati, di prove acquisite dinanzi ad altri giudici;
 che risulterebbe violato, infine, l'art. 111 Cost., in forza del quale il processo deve avere una ragionevole durata: ritenere il giudice vincolato, nel caso in esame, dalla richiesta di parte di rinnovazione dell'istruzione dibattimentale - e ciò sebbene i verbali delle prove testimoniali facciano già parte del fascicolo per il dibattimento, trattandosi di prove assunte nel contraddittorio tra le parti - significherebbe, infatti, dilatare irrazionalmente i tempi processuali, favorendo l'estinzione dei reati per prescrizione; esito, questo, tanto meno accettabile in una situazione di «emergenza», quale quella indotta dalle numerose condanne dello Stato italiano ad opera della Corte europea dei diritti dell'uomo, per l'eccessiva durata dei processi;
 che nel giudizio di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o infondata;
 	che, a parere della difesa dello Stato, la questione sarebbe inammissibile, avendo il rimettente omesso di descrivere compiutamente la fattispecie concreta e di motivare sulla rilevanza: in particolare, il giudice a quo non avrebbe precisato se il riesame dei testi escussi sia o meno necessario nella specie (e ciò, considerata la sussistenza di «situazioni di maggior rischio per la genuinità della prova in cui detto riesame è escluso»); se la rinnovazione dibattimentale sia o meno possibile e con quale prevedibile allungamento dei tempi processuali; se, infine e soprattutto, tale allungamento comporti il rischio della prescrizione del reato per cui si procede;
 	che, nel merito, la questione sarebbe comunque priva di fondamento, in quanto basata su censure già più volte disattese dalla Corte costituzionale.
 	Considerato che il Tribunale di Roma dubita, in riferimento agli artt. 3, 101 e 111 della Costituzione, della legittimità costituzionale dell'art. 525, comma 2, del codice di procedura penale, nella parte in cui prevede che «alla deliberazione debbano concorrere a pena di nullità assoluta i medesimi giudici che hanno partecipato al dibattimento»;
 	che, al di là della generica formulazione del petitum, il rimettente si duole segnatamente della disciplina delle modalità di rinnovazione del dibattimento dopo il mutamento del giudice-persona fisica, quale risultante alla luce dell'interpretazione accolta dalle sezioni unite della Corte di cassazione (sentenza 15 gennaio 1999-17 febbraio 1999, n. 2) e recepita dalla giurisprudenza di legittimità successiva;
 che, alla stregua di tale consolidato indirizzo interpretativo, il principio di immutabilità del giudice, enunciato dalla norma censurata, impone di procedere all'integrale rinnovazione del dibattimento ogni qualvolta intervengano cambiamenti della persona del giudice monocratico o della composizione del collegio;
 che, in tale evenienza - conformemente a quanto già affermato da questa Corte con la sentenza n. 17 del 1994 - i verbali delle prove assunte dal precedente giudice, in quanto documentativi di un'attività legittimamente compiuta, confluiscono nel fascicolo per il dibattimento a disposizione del nuovo giudice e, quindi, possono essere utilizzati per la decisione attraverso lo strumento della lettura;
 che siffatta conclusione va tuttavia coordinata con quanto previsto dell'art. 511, comma 2, cod. proc. pen., secondo cui «la lettura dei verbali di dichiarazioni è disposta solo dopo l'esame della persona che le ha rese, a meno che l'esame non abbia luogo»: dal che si desume come - al di fuori dell'ipotesi eccezionale regolata dall'art. 190-bis cod. proc. pen. - il nuovo giudice sia legittimato ad utilizzare le prove dichiarative precedentemente assunte, tramite semplice lettura, solo qualora detto esame non si compia, o per volontà delle parti - manifestata anche implicitamente, con la mancata richiesta di una nuova escussione del dichiarante - ovvero per sopravvenuta impossibilità di essa;
 che in sostanza, dunque, il nuovo giudice deve procedere alla riassunzione della prova dichiarativa ove una parte ne faccia richiesta, sempre che l'atto non risulti impossibile: e proprio da siffatto "vincolo" scaturirebbe - secondo il giudice a quo - la lesione dei parametri costituzionali evocati;
 che, ciò puntualizzato, l'eccezione di inammissibilità della questione per insufficiente descrizione della fattispecie e difetto di motivazione sulla rilevanza, formulata dall'Avvocatura generale dello Stato, non è fondata;
 che la descrizione della vicenda processuale, contenuta nell'ordinanza di rimessione, è in effetti assai sintetica, ma comunque sufficiente a consentire la verifica della rilevanza;
 che il giudice a quo riferisce, infatti, di dover rinnovare il dibattimento nel processo principale, essendo subentrato ad un collega a seguito di normali avvicendamenti negli uffici giudiziari; deduce, altresì, che davanti al precedente giudice erano stati escussi alcuni testimoni e che mentre il pubblico ministero ha consentito all'utilizzazione delle relative dichiarazioni, nel dibattimento rinnovato, tramite semplice lettura dei verbali, la difesa ha chiesto, invece, un nuovo esame dei dichiaranti: richiesta che il rimettente dovrebbe accogliere, in base al consolidato indirizzo interpretativo dianzi ricordato;
 che l'assenza di situazioni che rendano impossibile il riesame dei testimoni e la circostanza che non si versi nelle ipotesi soggette all'eccezionale regime derogatorio di cui all'art. 190-bis cod. proc. pen. (se tale, almeno, è il senso del riferimento della difesa dello Stato alle «situazioni di maggior rischio per la genuinità della prova in cui detto riesame è escluso») possono ritenersi implicite in quanto riferito;
 che tanto meno, poi - contrariamente a quanto assume l'Avvocatura dello Stato - può ritenersi che il giudice a quo fosse tenuto a quantificare il prevedibile allungamento dei tempi processuali prodotto dalla ripetizione dell'esame e a precisare se esso rischiasse effettivamente di provocare la prescrizione del reato oggetto di giudizio;
 che, in tal modo, si confonde, infatti, la valutazione di rilevanza con quella di non manifesta infondatezza: il rimettente censura, in riferimento al principio di ragionevole durata del processo, un effetto negativo ascrivibile in termini generali alla norma censurata, che non deve necessariamente ricorrere nella situazione di specie; la questione resta in ogni caso rilevante, perché il suo accoglimento eviterebbe al rimettente di dover accogliere l'istanza della difesa di rinnovazione dell'esame dei testi;
 che, quanto al merito, non sussiste, in realtà, alcun contrasto fra la costante giurisprudenza di legittimità e il precedente di questa Corte citato dal rimettente (sentenza n. 17 del 1994), nel quale, anzi, si sottolinea che, in caso di sostituzione del giudice in corso di dibattimento, la lettura del verbale del precedente esame testimoniale è legittima solo dopo nuovo esame del teste, salvo che questo non abbia luogo (come nel caso di sopravvenuta morte del teste medesimo); regola, questa, poi ribadita anche nell'ordinanza n. 99 del 1996, pure evocata dal rimettente;
 che, per converso, questa Corte ha già più volte dichiarato manifestamente infondate questioni analoghe a quella odierna, con pronunce successive alla citata decisione delle sezioni unite della Corte di cassazione, totalmente ignorate dal giudice a quo (ordinanze n. 318 del 2008; n. 67 del 2007; n. 418 del 2004; n. 73 del 2003; n. 59 del 2002; n. 431 e n. 399 del 2001);
 che la Corte ha rilevato, in specie, come la disciplina oggetto di scrutinio si correli al principio di immediatezza, che ispira l'impianto del codice di rito e di cui la regola dell'immutabilità del giudice costituisce strumento attuativo: principio che postula - salve le deroghe espressamente previste - l'identità tra il giudice che acquisisce le prove e il giudice che decide;
 che la disciplina censurata risulta, in tale ottica, tutt'altro che irrazionale: la parte che chiede la rinnovazione dell'esame del dichiarante esercita infatti il proprio diritto, garantito dal principio di immediatezza, «all'assunzione della prova davanti al giudice chiamato a decidere» (ordinanze n. 318 del 2008, n. 67 del 2007 e n. 418 del 2004);
 che, con specifico riferimento alle censure dall'odierno rimettente, questa Corte ha escluso, altresì, che sia configurabile una violazione dell'art. 3 Cost., sotto il profilo del diverso trattamento riservato a fattispecie identiche o similari;
 che, in particolare, si è rilevato come sia erroneo il richiamo, quale tertium comparationis, all'art. 238 cod. proc. pen., in tema di acquisizione dei verbali di prove provenienti da altro procedimento, il quale non consente affatto - in presenza della richiesta di nuovo esame avanzata da una delle parti - di utilizzare mediante lettura le precedenti dichiarazioni assunte da diverso giudice (ordinanza n. 399 del 2001; in senso conforme, ordinanze n. 59 del 2002 e n. 431 del 2001);
 che il comma 5 dell'art. 238 cod. proc. pen. fa, infatti, espressamente salvo il diritto delle parti di ottenere l'esame delle persone le cui dichiarazioni sono state acquisite; mentre l'art. 511-bis cod. proc. pen., nel prevedere che il giudice dia lettura dei verbali degli atti indicati dall'art. 238, richiama il comma 2 dell'art. 511 cod. proc. pen., che, come già ricordato, prescrive che sia data lettura dei verbali di dichiarazioni solo dopo l'esame del dichiarante, salvo che questo non abbia luogo;
 che parimenti non probante è stato ritenuto il riferimento agli artt. 26 e 33-nonies cod. proc. pen., i quali stabiliscono, rispettivamente, che «l'inosservanza delle norme sulla competenza non produce l'inefficacia delle prove già acquisite», e che «l'inosservanza delle disposizioni sulla composizione collegiale o monocratica del tribunale non determina [...] l'inutilizzabilità delle prove già acquisite»;
 che nelle ipotesi indicate si assiste, infatti, ad un cambiamento delle persone fisiche dei giudicanti: con la conseguenza che possono ritenersi comunque applicabili, in difetto di indicazioni contrarie, le regole valevoli in via generale in caso di mutamento del giudice, ivi compresa quella sottoposta a censura (ordinanza n. 67 del 2007);
 che tale considerazione è pienamente estensibile anche alle ulteriori fattispecie dell'astensione e della ricusazione del giudice, invocate dall'odierno rimettente;
 che la previsione dell'art. 42, comma 2, cod. proc. pen. - secondo la quale «il provvedimento che accoglie la dichiarazione di astensione o di ricusazione dichiara se e in quale parte gli atti compiuti precedentemente dal giudice astenutosi o ricusato conservano efficacia» - vale, difatti, a delimitare l'area del possibile "recupero" dell'attività istruttoria già espletata, ma non esclude che, entro tale area - assistendosi, di nuovo, ad un mutamento della persona fisica del giudicante - trovino applicazione le regole generali relative a tale evenienza;
 che questa Corte ha già avuto modo, per altro verso, di qualificare - in rapporto ad analoga censura - come «del tutto incongrue» le considerazioni sulla cui base il rimettente prospetta la violazione dell'art. 101 Cost. (ordinanza n. 399 del 2001);
 che nella norma costituzionale ora citata non si legge affatto - come vuole il rimettente - che «tutti i giudici sono uguali dinanzi alla legge», ma che i giudici «sono soggetti soltanto alla legge»: principio che non risulta minimamente scalfito dall'applicabilità della disciplina in questione, volta a tutela di un diverso valore (quello di immediatezza);
 che quanto, poi, alla ragionevole durata del processo (art. 111, secondo comma, Cost.) - in assunto compromessa dalla necessità di rinnovare prove acquisite nella pienezza del contraddittorio - la Corte ha già reiteratamente rilevato come detto principio debba essere contemperato, alla luce dello stesso richiamo al concetto di «ragionevolezza» che compare nella formula normativa, con il complesso delle altre garanzie costituzionali, rilevanti nel processo penale: garanzie la cui attuazione positiva è insindacabile, ove frutto - come nella specie - di scelte non prive di una valida ratio (ordinanze n. 318 del 2008, n. 67 del 2007, n. 418 del 2004 e n. 399 del 2001);
 che, in senso contrario, il giudice a quo rimarca come l'eccessiva durata dei processi sia stata causa di reiterate condanne dello Stato italiano da parte della Corte europea dei diritti dell'uomo, e perciò fonte di «notevoli esborsi per l'Erario»: fenomeno che, colorandosi dei tratti di una vera e propria «emergenza per il paese», imporrebbe il «ripudio di ogni attività ripetitiva eliminabile senza arrecare pregiudizio ai diritti fondamentali delle parti», legittimando, così, la convinzione che i tempi siano ormai «maturi» per la declaratoria di illegittimità costituzionale dell'art. 525, comma 2, cod. proc. pen. nei sensi auspicati;
 che, in replica a tali considerazioni, va peraltro osservato che il diritto «all'assunzione della prova davanti al giudice chiamato a decidere» - diritto che, in base alla ricordata giurisprudenza di questa Corte, la parte esercita nel chiedere la rinnovazione dell'esame del dichiarante - si raccorda, almeno per quanto attiene all'imputato, anche alla garanzia prevista dall'art. 111, terzo comma, Cost., nella parte in cui riconosce alla «persona accusata di un reato [...] la facoltà, davanti al giudice, di interrogare o di far interrogare le persone che rendono dichiarazioni a suo carico» e «di ottenere la convocazione e l'interrogatorio di persone a sua difesa nelle stesse condizioni dell'accusa»;
 che viene quindi in rilievo, a tale riguardo, quanto reiteratamente affermato proprio dalla Corte europea dei diritti dell'uomo - dalle cui censure, secondo il rimettente, l'accoglimento della questione dovrebbe mettere l'Italia al riparo - in relazione all'omologa previsione dell'art. 6, paragrafo 3, lettera d), della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, firmata a Roma il 4 novembre 1950 e ratificata con legge 4 agosto 1955, n. 848 (previsione che è servita da modello a quella dell'art. 111 Cost., dianzi ricordata): e, cioè, che la possibilità, per l'imputato, di confrontarsi con i testimoni in presenza del giudice che dovrà poi decidere sul merito delle accuse costituisce una garanzia del processo equo, in quanto permette a quest'ultimo di formarsi un'opinione circa la credibilità dei testimoni fondata su un'osservazione diretta del loro comportamento; con la conseguenza che ogni mutamento di composizione dell'organo giudicante deve comportare, di norma, una nuova audizione del testimone le cui dichiarazioni possano apparire determinanti per l'esito del processo (sentenza 27 settembre 2007, Reiner e altri contro Romania; sentenza 30 novembre 2006, Grecu contro Romania; sentenza 10 febbraio 2005, Graviano contro Italia; sentenza 4 dicembre 2003, Milan contro Italia; sentenza 9 luglio 2002, P. K. contro Finlandia);
 che la ratio giustificatrice della rinnovazione della prova non si richiama, dunque, ad una presunta incompletezza o inadeguatezza della originaria escussione, ma si fonda sulla opportunità di mantenere un diverso e diretto rapporto tra giudice e prova, particolarmente quella dichiarativa, non garantito dalla semplice lettura dei verbali: vale a dire la diretta percezione, da parte del giudice deliberante, della prova stessa nel momento della sua formazione, così da poterne cogliere tutti i connotati espressivi, anche quelli di carattere non verbale, particolarmente prodotti dal metodo dialettico dell'esame e del controesame; connotati che possono rivelarsi utili nel giudizio di attendibilità del risultato probatorio, così da poterne poi dare compiutamente conto nella motivazione ai sensi di quanto previsto dall'art. 546 comma 1, lettera e), cod. proc. pen.;
 che è ben vero che l'anzidetto diritto della parte alla nuova audizione non è assoluto, ma "modulabile" (entro limiti di ragionevolezza) dal legislatore: nei ricordati precedenti, questa Corte ha fatto, in effetti, riferimento alla possibilità che il legislatore introduca «presidi normativi volti a prevenire il possibile uso strumentale e dilatorio» del diritto in questione (ordinanze n. 318 del 2008 e n. 67 del 2007); mentre la stessa Corte di Strasburgo ha riconosciuto che esso ammette eccezioni;
 che ciò non toglie, tuttavia, che il riesame del dichiarante, in presenza di una richiesta di parte, continui a rappresentare la regola: il che priva di ogni validità la ricordata convinzione del rimettente, riguardo al fatto che «i tempi [siano] maturi» per l'accoglimento della questione di costituzionalità proposta;
 che tale regola nel processo penale costituisce uno dei profili del diritto alla prova, strumento necessario del diritto di azione e di difesa, da riconoscere lungo l'arco di tutto il complesso procedimento probatorio, quale diritto alla ricerca della prova, alla sua introduzione nel processo, alla partecipazione diretta alla sua acquisizione davanti al giudice terzo e imparziale, da ultimo alla sua valutazione ai fini della decisione da parte dello stesso giudice;
 che si tratta di regola costituente uno degli aspetti essenziali del modello processuale accusatorio, espresso dal vigente codice di procedura penale;
 che per tali ragioni la sua osservanza è presidiata dalla massima sanzione processuale, vale a dire dalla nullità assoluta;
 che la questione va dichiarata, pertanto, manifestamente infondata.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 525, comma 2, del codice di procedura penale, sollevata, in riferimento agli artt. 3, 101 e 111 della Costituzione, dal Tribunale di Roma con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 giugno 2010.&#13;
 F.to:&#13;
 Francesco AMIRANTE, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 10 giugno 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
