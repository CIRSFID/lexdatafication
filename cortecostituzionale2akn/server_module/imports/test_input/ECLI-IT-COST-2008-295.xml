<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/295/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/295/"/>
          <FRBRalias value="ECLI:IT:COST:2008:295" name="ECLI"/>
          <FRBRdate date="09/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="295"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/295/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/295/ita@/!main"/>
          <FRBRdate date="09/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/295/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/295/ita@.xml"/>
          <FRBRdate date="09/07/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="18/07/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maddalena</cc:relatore_pronuncia>
        <cc:data_decisione>09/07/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 159, comma 3, del decreto legislativo 22 gennaio 2004, n. 42 (Codice dei beni culturali e del paesaggio, ai sensi dell'articolo 10 della legge 6 luglio 2002, n. 137), come sostituito dall'art. 26 del decreto legislativo 24 marzo 2006, n. 157 (Disposizioni correttive ed integrative al decreto legislativo 22 gennaio 2004, n. 42, in relazione al paesaggio), promossi con due ordinanze del 5 novembre 2007 dal Consiglio di Stato, sezione sesta giurisdizionale, sui ricorsi proposti dal Ministero per i beni e le attività culturali contro la Rustica Sedes s.r.l. ed altri e il Comune di S. Anastasia, iscritte ai nn. 13 e 14 del registro ordinanze 2008 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 8, prima serie speciale, dell'anno 2008. 
    Visti l'atto di costituzione di Italia Nostra nonché l'atto di intervento, fuori termine, del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 9 luglio 2008 il Giudice relatore Paolo Maddalena. 
    Ritenuto che, con due ordinanze di identico tenore, pronunciate il 5 novembre 2007 – ed iscritte, rispettivamente, al n. 13 ed al n. 14 del registro ordinanze dell'anno 2008 – il Consiglio di Stato in sede giurisdizionale, sezione sesta, ha sollevato, in riferimento agli artt. 3, 76 e 118 della Costituzione, questione di legittimità costituzionale dell'art. 159, comma 3, del decreto legislativo 22 gennaio 2004, n. 42 (Codice dei beni culturali e del paesaggio, ai sensi dell'articolo 10 della legge 6 luglio 2002, n. 137), come sostituito dall'art. 26 del d.lgs. 24 marzo 2006, n. 157 (Disposizioni correttive ed integrative al decreto legislativo 22 gennaio 2004, n. 42, in relazione al paesaggio); 
    che, come emerge dagli atti di promovimento degli incidenti di costituzionalità, nei giudizi a quibus si controverte sugli appelli, proposti dal Ministero per i beni e le attività culturali, avverso le sentenze del Tribunale amministrativo regionale per la Campania, rese entrambe il 6 dicembre 2006, con le quali sono stati accolti i ricorsi, disgiuntamente proposti dal Comune di S. Anastasia e da altri interessati, avverso il decreto del 13 giugno 2006, n. 11978, con cui la Soprintendenza per i beni ambientali ed architettonici di Napoli e provincia aveva annullato il nulla osta paesaggistico n. 2265 del 13 gennaio 2006, rilasciato dal Comune medesimo per l'attuazione di un «piano particolareggiato di iniziativa privata»;  
    che, espone il rimettente, il TAR per la Campania, con le predette sentenze, «sulla premessa che l'esame dell'organo statale doveva essere necessariamente ed esclusivamente limitato a motivi di legittimità, non potendo impingere in apprezzamenti di merito», riteneva fondato il profilo di violazione di legge ed eccesso di potere dedotto nel ricorso contro la determinazione tutoria negativa; 
    che, si evidenzia nelle ordinanze di rimessione, negli atti di appello viene dedotta l'erroneità delle sentenze di primo grado in base al rilievo per cui, a seguito dell'entrata in vigore del citato art. 159, comma 3, la Soprintendenza, in sede di verifica dell'autorizzazione paesaggistica, non avrebbe necessità di indicare specifici vizi di violazione di legge da cui sarebbe affetta detta autorizzazione; in definitiva, non sussisterebbe più «il vincolo della delimitazione del potere di cognizione dell'autorizzazione, oggetto del riscontro, ai soli parametri di legittimità, (inclusi peraltro tutti i possibili profili di eccesso di potere), quale affermato costantemente dalla giurisprudenza amministrativa»; 
    che, ad avviso del rimettente, la tesi prospettata dall'appellante risulterebbe «suffragata dalla lettura dello stesso art. 159, comma 3, nel suo testo attuale», giacché il rilievo della “non conformità” alle prescrizioni della legislazione di tutela «implica inevitabilmente il diretto apprezzamento dei fatti rilevanti nel caso concreto, già considerati nell'ambito dell'autorizzazione oggetto di controllo, al fine di esprimere su di essi un giudizio di “valore”», come sarebbe, appunto, quello «ancorato a “concetti indeterminati”, da connotare cioè in base ad apprezzamenti tecnici complessi, alla stregua di regole non giuridiche e proprie delle discipline estetico-ambientali, suscettibili di diversi esiti applicativi: si abbia riguardo ai concetti di “alterazione”, “pregiudizio”, “compatibilità”, “coerenza”, rapportate ai “valori paesaggistici” ed agli “obiettivi di qualità paesaggistica”, in base alla lettera dell'art. 146, commi 1, 4 e 5, del d.lgs. n. 42\2004»; 
    che, ciò premesso sulla portata della denunciata disposizione di cui all'art. 159, comma 3, il giudice a quo motiva sulla rilevanza delle questioni adducendo che, là dove appunto si intenda la disposizione censurata «come estensiva dell'ambito del controllo demandato alla soprintendenza fino agli aspetti c.d. “di merito”», gli appelli dovrebbero trovare accoglimento, con conseguente riforma delle sentenze impugnate; 
    che, in punto di non manifesta infondatezza, il Consiglio di Stato rimettente rammenta che il potere devoluto all'organo statale sull'autorizzazione affidata alla Regione, ed agli organi da essa delegati, era stato configurato, fin dall'emanazione dell'art. 82, comma 9, del d.P.R. 24 luglio 1977, n. 616 (Attuazione della delega di cui all'art. 1 della legge 22 luglio 1975, n. 382), alla stregua di un “diritto vivente” costituito «da una giurisprudenza amministrativa ormai trentennale», in termini di potere di «annullamento, […] costantemente limitato al solo rilievo di vizi di legittimità»; 
    che, secondo il giudice a quo, dovrebbe dubitarsi, pertanto, che la «innovazione legislativa» costituita dalla norma denunciata, la quale comporta una così rilevante «ridislocazione dei poteri di valutazione “in merito”», possa esser stata prevista nella norma di delega di cui all'art. 10 della legge 6 luglio 2002, n. 137, il cui comma 1 prevede soltanto gli obiettivi «della codificazione, cioé della ricognizione delle norme vigenti, e del riassetto, cioè della razionalizzazione di tale corpo normativo», mentre tra i criteri di attuazione della delega, di cui alla lettera d) del comma 2 dello stesso art. 10, non è presente «alcuna previsione che riguardi l'estensione e le modalità del “controllo” demandato agli organi statali relativamente agli atti autorizzativi qui in rilievo»; 
    che, ad avviso del rimettente, ne dovrebbe conseguire un vulnus all'art. 76 Cost., «in ordine alla necessaria corrispondenza delle norme delegate a “principi e criteri direttivi” stabiliti dalla legge-delega»; 
    che, prosegue il giudice a quo, analoga violazione dell'art. 76 Cost. si configurerebbe per il fatto che lo strumento utilizzato per addivenire ad una siffatta modificazione dei rapporti Stato-Regione, «in tema di poteri di autorizzazione paesaggistica», sia stato quello dei decreti comunque previsti dal comma 4 dello stesso art. 10, che possono sì apportare «disposizioni correttive ed integrative dei decreti legislativi di cui al comma 1», ma pur sempre «nel rispetto degli stessi principi e criteri direttivi»; 
    che nelle ordinanze di rimessione si sostiene, inoltre, che, pur prescindendo dal rispetto di detti principi e criteri direttivi, dovrebbe dubitarsi che «una innovazione di tale portata possa farsi rientrare nel concetto di “disposizione correttiva ed integrativa”», il quale non può «implicare l'adozione, come nel caso, di una soluzione normativa che, su un punto essenziale e qualificante la complessiva disciplina in discorso, risulti in sostanziale contraddizione con quella originariamente assunta in sede di emanazione del decreto legislativo di prima attuazione della delega»; 
    che, anche sotto tale ulteriore e diverso profilo, il rimettente prospetta, quindi, la violazione dell'art. 76 Cost., «connessa al meccanismo peculiare prescelto dalla legge di delega per stabilizzare la corretta applicazione delle norme delegate»; 
    che il giudice a quo dubita, infine, «che sia conforme al principio di sussidiarietà stabilito dall'art. 118 Cost., come criterio di attribuzione delle funzioni amministrative, anche in correlazione al principio di ragionevolezza di cui all'art. 3 Cost.», la previsione di «un potere di controllo in forma c.d. di “tutela”, esteso cioè anche al merito, che consenta allo Stato una costante e generalizzata autonoma rivalutazione delle determinazioni operate dalla regione e dagli enti territoriali delegati, in particolare dai comuni, assorbendosi, agli effetti pratici, in modo altrettanto costante e generalizzato, e rendendolo così potenzialmente instabile ed irrilevante, il punto di vista degli enti, costituzionalmente dotati di autonomia, che sono primariamente coinvolti nel “governo del territorio” su cui si colloca il bene interessato dall'autorizzazione paesaggistica»; 
    che, nel giudizio promosso con ordinanza iscritta al registro ordinanze n. 13 del 2008, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, è intervenuto con atto depositato in data 19 marzo 2008 e, dunque, oltre i venti giorni dalla pubblicazione dell'ordinanza di rimessione nella Gazzetta Ufficiale del 13 febbraio 2008, n. 8, essendo stata la stessa ordinanza notificata il 26 novembre 2007; 
    che, nel giudizio promosso con ordinanza iscritta al registro ordinanze n. 14 del 2008, si è costituita Italia Nostra ONLUS – Associazione Nazionale per la Tutela del patrimonio Storico, Artistico e Naturale della Nazione, interveniente ad adiuvandum  nel giudizio a quo, la quale ha concluso per l'inammissibilità o, comunque, l'infondatezza della sollevata questione. 
    Considerato che il Consiglio di Stato, con due ordinanze di identico tenore, ha sollevato, in riferimento agli artt. 3, 76 e 118 della Costituzione, questione di legittimità costituzionale dell'art. 159, comma 3, del decreto legislativo 22 gennaio 2004, n. 42 (Codice dei beni culturali e del paesaggio, ai sensi dell'articolo 10 della legge 6 luglio 2002, n. 137), come sostituito dall'art. 26 del d.lgs. 24 marzo 2006, n. 157 (Disposizioni correttive ed integrative al decreto legislativo 22 gennaio 2004, n. 42, in relazione al paesaggio); 
    che i giudizi, in quanto riguardanti la stessa norma, oggetto di identiche censure da parte di entrambe le ordinanze di rimessione, devono essere riuniti per essere decisi con un'unica pronuncia; 
    che, in via preliminare, l'intervento del Presidente del Consiglio dei ministri nel giudizio promosso con ordinanza iscritta al registro ordinanze n. 13 del 2008, deve essere dichiarato inammissibile in quanto tardivo (tra le tante, ordinanza n. 199 del 2006), essendo avvenuto fuori termine, ai sensi dell'art. 25, commi secondo e terzo, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), e dell'art. 3, comma 2, delle Norme integrative per i giudizi davanti alla Corte costituzionale; 
    che, sempre in via preliminare, deve essere dichiarata inammissibile la costituzione di Italia Nostra ONLUS – Associazione Nazionale per la Tutela del patrimonio Storico, Artistico e Naturale della Nazione, nel giudizio promosso con ordinanza iscritta al registro ordinanze n. 14 del 2008;  
    che, difatti, le parti legittimate a costituirsi nel giudizio incidentale sono solo quelle che rivestivano la qualità di parte del giudizio a quo al momento della pubblicazione dell'ordinanza di rimessione (sentenze n. 531 del 1988 e n. 220 del 1988), là dove Italia Nostra ONLUS è intervenuta, ad adiuvandum, nel giudizio a quo solo a seguito di atto notificato, a mezzo di servizio postale, il 20 novembre 2007 e depositato il successivo 21 novembre 2007, successivamente, dunque, alla pubblicazione dell'ordinanza di rimessione in data 5 novembre 2007; 
    che, tanto premesso, deve osservarsi che, successivamente alle ordinanze di rimessione, la disciplina del procedimento di autorizzazione in via transitoria dettata dalla norma denunciata è stata innovata per effetto dell'art. 2, lettera hh), del d.lgs. 26 marzo 2008, n. 63 (Ulteriori disposizioni integrative e correttive del decreto legislativo 22 gennaio 2004, n. 42, in relazione al paesaggio), che ha sostituito il previgente art. 159 del d.lgs. n. 42 del 2004, come sostituito dall'art. 26 del d.lgs. n. 157 del 2006, su cui, peraltro, questa Corte si è già pronunciata, in precedente occasione, con la sentenza n. 367 del 2007, depositata successivamente all'emissione delle ordinanze di rimessione; 
    che, pertanto, alla luce della ricordata sopravvenienza normativa si impone la restituzione degli atti al giudice rimettente, per una rinnovata valutazione della rilevanza e della non manifesta infondatezza della questione dal medesimo sollevata.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti al Consiglio di Stato, sezione sesta giurisdizionale.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 luglio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Paolo MADDALENA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 18 luglio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
