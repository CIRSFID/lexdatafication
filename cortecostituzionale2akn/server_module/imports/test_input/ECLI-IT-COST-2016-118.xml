<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/118/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/118/"/>
          <FRBRalias value="ECLI:IT:COST:2016:118" name="ECLI"/>
          <FRBRdate date="23/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="118"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/118/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/118/ita@/!main"/>
          <FRBRdate date="23/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/118/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/118/ita@.xml"/>
          <FRBRdate date="23/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="26/05/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Giorgio Lattanzi</cc:relatore_pronuncia>
        <cc:data_decisione>23/03/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Giuseppe FRIGO, Alessandro CRISCUOLO, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 360 del codice di procedura penale, promosso dalla Corte d'assise d'appello di Roma nel procedimento penale a carico di L.P., con ordinanza del 25 giugno 2015, iscritta al n. 245 del registro ordinanze 2015 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 47, prima serie speciale, dell'anno 2015.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 23 marzo 2016 il Giudice relatore Giorgio Lattanzi.
 Ritenuto che, con ordinanza del 25 giugno 2015 (r.o. n. 245 del 2015), la Corte d'assise d'appello di Roma ha sollevato, in riferimento agli artt. 24 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 360 del codice di procedura penale, «ove non prevede che le garanzie difensive approntate da detta norma riguardano le attività di individuazione e prelievo dei reperti utili per la ricerca del DNA»;
 che il giudice rimettente premette di essere investito del giudizio di rinvio conseguente alla sentenza della Corte di cassazione del 4 novembre 2014, che aveva annullato la sentenza della Prima Corte d'assise d'appello di Roma del 21 maggio 2013, accogliendo l'impugnazione del Procuratore generale relativa alla derubricazione in concorso anomalo, ai sensi dell'art. 116 del codice penale, dell'originaria contestazione di omicidio volontario;
 che, in particolare, il giudice di legittimità, nel rinviare «per nuova valutazione sul punto» il processo alla Corte d'assise d'appello, aveva ritenuto utilizzabili, diversamente dal giudice di appello, gli accertamenti tecnici «disposti dalla Procura della Repubblica per la individuazione di tracce del DNA sui prelievi che i CC, previa ispezione disposta dalla stessa Procura sui luoghi teatro del delitto, avevano effettuato il 5 novembre 2012»; 
 che la difesa aveva «sollevato», tra l'altro, in riferimento agli artt. 3, 24 e 111 Cost., questione di legittimità costituzionale dell'art. 360 cod. proc. pen. «nella formulazione codificata dalla giurisprudenza di legittimità», che limitava l'applicazione delle garanzie difensive previste dalla predetta norma «alle sole "valutazioni di laboratorio" su tracce genetiche escludendo dalle garanzie medesime la pregressa fase, "irripetibile" di raccolta ed asportazione delle tracce genetiche»;
 che, in punto di rilevanza, il giudice rimettente osserva che, poiché non vi era contestazione sul fatto che l'imputato, il 5 maggio 2012, all'atto del prelievo delle tracce genetiche, rivestisse la qualità di indagato di reato anche se formalmente non ancora iscritto nel registro ex art. 335 cod. proc. pen., l'eventuale accoglimento della questione invaliderebbe due dei principali elementi di prova sulla base dei quali il primo giudice era pervenuto all'affermazione della penale responsabilità del medesimo per l'omicidio volontario ascrittogli, vale a dire «la presenza di tracce del sangue della vittima nell'appartamento della nonna» del citato imputato (ove questi dimorava in quei giorni) e «la commistione di materiale biologico della vittima e dell'imputato sulle scale che portavano dal piano ove era collocato l'appartamento» della persona offesa «a quello ove si trovava l'appartamento della nonna»; 
 che la distinzione operata nel processo dal giudice di legittimità tra "rilievi" ed "accertamenti", e sulla base della quale solo rispetto a questi ultimi rileverebbe il requisito dell'irripetibilità, sarebbe conforme a un consolidato orientamento giurisprudenziale;
 che la difesa dubita della legittimità costituzionale di tale distinzione laddove essa sia ritenuta applicabile anche ai prelievi di materiale biologico e osserva che le operazioni di asporto e raccolta di tracce di materiale genetico non potrebbero essere qualificate come mere attività esecutive, perché gli esperti incaricati di tali attività sarebbero tenuti al rispetto di severi protocolli cautelari, «quali la delimitazione dei percorsi di accesso e di camminamento, l'uso di tute "ad hoc", il cambiamento di strumenti e dotazione in corso d'opera, il filmaggio delle operazioni»; 
 che, nella prospettazione della difesa, il diritto dell'indagato ad interloquire, tramite il suo difensore, sulla valutazione del materiale genetico raccolto dovrebbe potersi esercitare anche in relazione alla fase di asporto e prelievo delle tracce, circa il rispetto dei protocolli adottati per evitare contaminazioni e alterazioni, altrimenti verrebbero violati l'art. 3 Cost. ed inoltre il diritto di difesa e il principio del giusto processo;
 che il giudice rimettente ha condiviso la prospettazione della difesa, ritenendo che le attività di asporto e prelievo di tracce di materiale biologico attinenti alla ricerca del DNA non siano «omologabili» ad altre più tradizionali operazioni di repertazione;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile e comunque non fondata;
 che secondo la difesa dello Stato la questione sarebbe inammissibile perché il giudice a quo, nel giudizio di rinvio, dovrebbe uniformarsi al principio di diritto espresso dalla Corte di cassazione, che avrebbe condiviso l'interpretazione della citata giurisprudenza, essendosi, sul punto, formato anche il giudicato interno;
 che inoltre l'ordinanza «adduce solo, ma non allega» le circostanze sulla base delle quali si dovrebbe ritenere rilevante la questione;
 che, in particolare, secondo l'Avvocatura generale dello Stato: «a) non è allegata, né riportata in ordinanza, l'imputazione, talché non è possibile cogliere gli aspetti fattuali della vicenda sostanziale, tra i quali la condotta materiale contestata all'imputato; b) non è chiarito in ordinanza per quali motivi il Giudice di primo grado ha delibato una colpevolezza diretta dell'imputato, mentre il giudice di appello, la cui decisione è stata cassata con rinvio, ha ritenuto trattarsi di concorso cd. anomalo; c) né viene chiarito in ordinanza, in quale misura l'accertamento tecnico sul D.N.A. abbia influito su tale decisione e quale, dunque, ne sia la rilevanza».
 Considerato che, con ordinanza del 25 giugno 2015 (r.o. n. 245 del 2015), la Corte d'assise d'appello di Roma in un giudizio di rinvio ha sollevato, in riferimento agli artt. 24 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 360 del codice di procedura penale, «ove non prevede che le garanzie difensive approntate da detta norma riguardano le attività di individuazione e prelievo dei reperti utili per la ricerca del DNA»;
 che la Corte di cassazione, annullando sul punto la sentenza di appello impugnata, aveva ritenuto utilizzabili gli accertamenti tecnici «disposti dalla Procura della Repubblica per la individuazione di tracce del DNA sui prelievi che i CC, previa ispezione disposta dalla stessa Procura sui luoghi teatro del delitto, avevano effettuato il 5 novembre 2012»;
 che il giudice rimettente ritiene che a tali prelievi, per le loro particolari caratteristiche, dovrebbero applicarsi le garanzie stabilite dall'art. 360 cod. proc. pen. per gli accertamenti tecnici non ripetibili e ha sospettato di illegittimità costituzionale questa norma perché non lo prevede;
 che l'Avvocatura generale dello Stato ha eccepito l'inammissibilità della questione perché il giudice di rinvio deve uniformarsi «al principio di diritto espresso dalla Suprema Corte di Cassazione, [...] essendosi poi, tra l'altro, sul punto, [formato] anche giudicato interno»; 
 che l'eccezione è priva di fondamento, perché, secondo la costante giurisprudenza di questa Corte, se è vero che il giudice di rinvio è tenuto ad applicare la norma nel senso indicato dalla Corte di cassazione, è anche vero che nel farne applicazione il giudice non può non essere legittimato ad eccepire l'illegittimità costituzionale, dato che la norma alla quale si riferisce il principio di diritto affermato dalla Corte di cassazione deve essere ulteriormente applicata nel giudizio di rinvio (sentenze n. 293 del 2013 e    n. 204 del 2012);
 che nel caso in esame, a quanto si desume dall'ordinanza di rimessione, la Corte di cassazione, diversamente dal giudice di appello, ha ritenuto che il prelievo di materiale biologico costituisce un atto di indagine rimesso alla polizia giudiziaria, e non un accertamento tecnico non ripetibile, sicché non può negarsi che il giudice di rinvio, nel valutare tale atto, sia legittimato a sollevare la questione di legittimità costituzionale diretta a estendere ad esso le garanzie difensive previste dall'art. 360 cod. proc. pen. per gli accertamenti tecnici non ripetibili; 
 che la questione è, nondimeno, manifestamente inammissibile per altre ragioni; 
 che infatti, come ha rilevato l'Avvocatura generale dello Stato, «l'ordinanza di rimessione adduce solo, ma non allega, le circostanze sulla base delle quali ritenere rilevante la questione dedotta», perché contiene una carente descrizione della fattispecie, che non consente di verificare se e in che misura la questione di legittimità costituzionale sollevata sia effettivamente rilevante nel giudizio a quo;
 che, come si evince dall'ordinanza di rimessione, il prelievo del materiale biologico è avvenuto nel contesto di un'ispezione che, unitamente al prelievo, è stata svolta con «un atto ispettivo "a sorpresa" [...] proprio al fine di evitare l'alterazione delle tracce del reato», atto che, come ha riconosciuto il giudice rimettente, è avvenuto in applicazione dell'art. 364, comma 5, cod. proc. pen.;
 che per le caratteristiche dell'ispezione e del contestuale prelievo non era necessario l'avviso al difensore, perciò il giudice rimettente avrebbe dovuto chiarire quale garanzia difensiva nella specie sarebbe mancata;
 che la lacunosità della descrizione della fattispecie e, correlativamente, dell'indicazione delle ragioni della rilevanza della questione di legittimità costituzionale ne comportano la manifesta inammissibilità;
 che inoltre il giudice rimettente non precisa quali garanzie difensive, previste dall'art. 360 cod. proc. pen., andrebbero estese all'acquisizione del materiale biologico e, più in particolare, non chiarisce se l'estensione dovrebbe riguardare tutta la procedura regolata dall'art. 360 cod. proc. pen., ivi compresa la parte relativa alla nomina del consulente tecnico, al conferimento dell'incarico e alla riserva, riconosciuta all'indagato, di promuovere incidente probatorio;
 che quindi il thema decidendum è indeterminato, dando luogo ad un'ulteriore ragione di manifesta inammissibilità della questione. 
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 360 del codice di procedura penale, sollevata, in riferimento agli artt. 24 e 111 della Costituzione, dalla Corte d'assise d'appello di Roma, con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 marzo 2016.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente e Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 26 maggio 2016.&#13;
 Il Cancelliere&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
