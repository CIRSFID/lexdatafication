<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/142/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/142/"/>
          <FRBRalias value="ECLI:IT:COST:2006:142" name="ECLI"/>
          <FRBRdate date="03/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="142"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/142/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/142/ita@/!main"/>
          <FRBRdate date="03/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/142/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/142/ita@.xml"/>
          <FRBRdate date="03/04/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/04/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>03/04/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 13, commi 3 e 3-quater, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), come modificato dalla legge 30 luglio 2002, n. 189 (Modifiche alla normativa in materia di immigrazione e di asilo), promossi con ordinanze del 20 febbraio 2003 dal Tribunale di Ascoli Piceno, sezione distaccata di S. Benedetto del Tronto, del 30 luglio 2003 (due ordinanze) dal Tribunale di Lucera e del 13 luglio 2004 dal Tribunale di Pesaro, rispettivamente iscritte ai numeri 437, 1006 e 1007 del registro ordinanze 2003 e al n. 87 del registro ordinanze 2005 e pubblicate nella Gazzetta Ufficiale della Repubblica numeri 28 e 48, prima serie speciale, dell'anno 2003 e n. 10, prima serie speciale, dell'anno 2005. 
      Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 22 febbraio 2006 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che con l'ordinanza in epigrafe il Tribunale di Ascoli Piceno, sezione distaccata di S. Benedetto del Tronto, ha sollevato questione di legittimità costituzionale, in riferimento agli artt. 3 e 24 della Costituzione, dell'art. 13, commi 3 e 3-quater, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), come modificato dalla legge 30 luglio 2002, n. 189 (Modifiche alla normativa in materia di immigrazione e di asilo), nella parte in cui non consente al giudice del dibattimento di emettere sentenza di non luogo a procedere qualora, a seguito del rilascio del nulla osta, sia acquisita la prova dell'avvenuta espulsione dell'imputato; 
    che il giudice a quo premette, in punto di fatto, che un cittadino extracomunitario, imputato del reato di cui all'art. 385 del codice penale, era stato tratto a giudizio con citazione diretta per l'udienza del 28 marzo 2003; 
    che la Questura di Ancona, con domanda pervenuta il 7 febbraio 2003, aveva chiesto il rilascio del nulla osta all'espulsione dell'imputato, ai sensi dell'art. 13, comma 3, del d.lgs. n. 286 del 1998; 
    che, ad avviso del rimettente, la norma ora citata risulterebbe lesiva – avuto riguardo anche al disposto del comma 3-quater del medesimo art. 13 – tanto del principio di eguaglianza che del diritto di difesa dello straniero; 
    che sarebbe evidente, infatti, l'ingiustificata disparità di trattamento tra lo straniero che ha commesso un reato per il quale è prevista l'udienza preliminare e lo straniero che è stato tratto a giudizio con procedimento direttissimo o con citazione diretta ai sensi dell'art. 550 del codice di procedura penale; 
    che mentre nel primo caso, infatti, le disposizioni censurate consentono al giudice dell'udienza preliminare di pronunciare, a seguito dell'acquisizione della prova dell'avvenuta espulsione, sentenza di non luogo a procedere ex art. 425 cod. proc. pen.; nel secondo caso, lo straniero non avrebbe invece diritto né ad una tale pronuncia, né tanto meno a quella di proscioglimento prima del dibattimento ai sensi dell'art. 469 cod. proc. pen., risultando l'una e l'altra precluse – alla luce di quanto stabilito dall'art. 13, comma 3-quater, del d.lgs. n. 286 del 1998 – dall'avvenuta emissione del provvedimento che dispone il giudizio; 
    che sussisterebbe, pertanto, un «vuoto normativo» tra il provvedimento ora indicato e la sentenza di primo grado, atto a compromettere i parametri costituzionali evocati, posto che lo straniero tratto a giudizio con citazione diretta, una volta espulso, non avrebbe la possibilità di difendersi, né di venire prosciolto con la formula di non doversi procedere; 
    che la questione sarebbe altresì rilevante, in quanto, per le considerazioni dianzi indicate, l'eventuale rilascio del nulla osta all'espulsione comporterebbe la violazione di diritti costituzionalmente protetti dell'imputato; 
    che con le due ordinanze indicate in epigrafe, di identico tenore, il Tribunale di Lucera ha sollevato questione di legittimità costituzionale, in riferimento agli artt. 3, 24, secondo comma, e 111 Cost., dell'art. 13, comma 3-quater, del d.lgs. n. 286 del 1998, limitatamente all'inciso «se non è stato ancora emesso il provvedimento che dispone il giudizio» ed alla parte in cui non prevede che, acquisita la prova dell'avvenuta espulsione, il giudice del dibattimento emetta sentenza di non luogo a procedere; 
    che il rimettente riferisce che l'imputato – cittadino extracomunitario tratto in arresto per il reato di cui all'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998 – era stato presentato per la convalida della misura; 
    che, disposta la convalida, era stato rilasciato anche il nulla osta all'espulsione amministrativa, a norma dell'art. 13, commi 3 e 3-bis, del d.lgs. n. 286 del 1998, e si era quindi proceduto a giudizio direttissimo, obbligatorio in relazione al reato contestato; 
    che, avendo l'imputato chiesto ed ottenuto termine a difesa, ai sensi dell'art. 558, comma 7, cod. proc. pen., alla successiva udienza era stata acquisita prova della sua espulsione; 
    che, ciò premesso, il rimettente osserva come le norme che prevedono il rilascio del nulla osta all'espulsione, all'atto della convalida dell'arresto, escludono che il giudice possa negarlo per consentire all'imputato di essere presente nel processo; 
    che l'art. 13, comma 3-quater, del d.lgs. n. 286 del 1998 stabilisce, a sua volta, che nei confronti degli imputati, per i quali sia acquisita la prova dell'avvenuta esecuzione dell'espulsione, debba pronunciarsi sentenza di non luogo a procedere, salvo che sia stato già emesso il provvedimento che dispone il giudizio; 
    che nella specie, essendo stato disposto il giudizio direttissimo, non sarebbe dunque più possibile la pronuncia della sentenza dianzi indicata: e ciò quantunque l'imputato risulti assente per causa indipendente dalla sua volontà; 
    che tale meccanismo contrasterebbe con diversi precetti costituzionali; 
    che esso determinerebbe, infatti, una disparità di trattamento tra gli imputati del tutto irragionevole, in quanto dipendente da fattori meramente casuali, ossia dalla circostanza che le condizioni per il rilascio del nulla osta all'espulsione si concretizzino prima del rinvio a giudizio, ovvero contestualmente o successivamente a questo; 
    che la sperequazione censurata non sarebbe giustificabile neppure con una finalità di deflazione processuale, posto che, anche rispetto all'imputato già rinviato a giudizio, l'espulsione previo nulla osta dell'autorità giudiziaria potrebbe aver luogo prima che sia aperto il dibattimento; 
    che la previsione per cui il dibattimento può proseguire contro lo straniero già espulso violerebbe, altresì, il diritto di difesa, non potendosi ritenere applicabili, nell'ipotesi considerata, le disposizioni in tema di legittimo impedimento dell'imputato (artt. 484, comma 2-bis, e 420-ter cod. proc. pen.): e ciò in quanto, da un lato, ove si facesse applicazione di dette disposizioni, si determinerebbe una sospensione a tempo indefinito del processo in dipendenza di provvedimenti amministrativi; e, dall'altro lato, la lettera dell'art. 13, comma 3-quater, del d.lgs. n. 286 del 1998 lascerebbe intendere che si debba comunque procedere nei confronti degli imputati espulsi, già rinviati a giudizio; 
    che sarebbe violato, da ultimo, l'art. 111 Cost., il quale enuncia, con riguardo al processo penale, una serie di principi e di regole di garanzia – principio del contraddittorio, diritto a disporre di condizioni adeguate per preparare la propria difesa, diritto di interrogare o fare interrogare i dichiaranti a carico, diritto all'interprete – che presuppongono la facoltà dell'imputato di presenziare al processo a suo carico, risultando dunque incompatibili con le norme che limitano tale facoltà; 
    che con l'ordinanza in epigrafe il Tribunale di Pesaro ha sollevato, in riferimento all'art. 3 Cost., questione di legittimità costituzionale dell'art. 13, comma 3-quater, del d.lgs. n. 286 del 1998; 
    che il rimettente ritiene rilevante e non manifestamente infondata la questione prospettata dalla difesa dell'imputato, stante la palese e non giustificata disparità di trattamento tra gli indagati extracomunitari espulsi, nei cui confronti si procede per reati che prevedono l'udienza preliminare, in ordine ai quali soltanto, se non è stato ancora emesso il provvedimento che dispone il giudizio, deve essere pronunciata sentenza di non luogo a procedere; e tutti gli altri indagati, nonché gli imputati tratti a giudizio con citazione diretta – come nel caso di specie – i quali non potrebbero ottenere analoga sentenza; 
    che nei giudizi di costituzionalità relativi alle ordinanze del Tribunale di Ascoli Piceno e del Tribunale di Lucera è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che le questioni siano dichiarate inammissibili o infondate. 
    Considerato che le ordinanze di rimessione sollevano analoghe questioni, onde i relativi giudizi vanno riuniti per essere definiti con unica pronuncia; 
    che la questione sollevata dal Tribunale di Ascoli Piceno è manifestamente inammissibile, essendo la sua rilevanza nel giudizio a quo solo futura ed ipotetica; 
    che il Tribunale rimettente invoca, difatti, una pronuncia che consenta anche al giudice del dibattimento di emettere la sentenza di non luogo a procedere per avvenuta espulsione dell'imputato, di cui all'art. 13, comma 3-quater, del d.lgs. 25 luglio 1998, n. 286; 
    che, tuttavia, il giudice a quo formula il quesito di costituzionalità prima del dibattimento, in sede di decisione sulla richiesta di rilascio del nulla osta all'espulsione di uno straniero ancora presente nel territorio nazionale; 
    che, di conseguenza, la questione risulta palesemente prematura, giacché nel giudizio principale il problema dell'asserita esigenza costituzionale di estendere l'ambito applicativo della norma denunciata nei termini dianzi indicati non è attuale; 
    che tale problema avrà ragione di porsi, infatti, solo dopo l'avvenuta esecuzione dell'espulsione e sempre che questa abbia concretamente luogo prima che il dibattimento sia concluso: evenienza che – giova aggiungere – ove si consideri che nella specie l'imputato risultava citato a giudizio a poco più di un mese dalla data dell'ordinanza di rimessione, non può considerarsi ineluttabile non solo in punto di fatto, attese le difficoltà che possono concretamente frapporsi alla materiale attuazione dei provvedimenti espulsivi; ma neppure in punto di diritto, posto che l'art. 14 del d.lgs. n. 286 del 1998 contempla, in presenza di determinati presupposti, la possibilità del trattenimento preliminare dello straniero espellendo presso un centro di permanenza temporanea fino ad un massimo di sessanta giorni (oltre che del ricorso al meccanismo dell'intimazione, penalmente sanzionata, a lasciare il territorio nazionale); 
    che con riguardo, poi, alla questione sollevata dal Tribunale di Lucera, il giudice a quo dubita, analogamente, della legittimità costituzionale dell'art. 13, comma 3-quater, del d.lgs. n. 286 del 1998, nella parte in cui subordina la pronuncia della sentenza di non luogo a procedere per avvenuta espulsione alla condizione negativa espressa dall'inciso «se non è ancora stato emesso il provvedimento che dispone il giudizio»: impedendo, così, al giudice del dibattimento la declaratoria di improcedibilità; 
    che, ad avviso del rimettente, la norma impugnata si porrebbe in contrasto, anzitutto, con l'art. 3 Cost., determinando una disparità di trattamento fra gli imputati in dipendenza di fattori puramente casuali, legati al rapporto cronologico tra esecuzione dell'espulsione e rinvio a giudizio (l'imputato “beneficia” dell'improcedibilità solo se la prima si realizza anteriormente al secondo); 
    che, in proposito, questa Corte ha peraltro reiteratamente affermato che rientra nella discrezionalità del legislatore, una volta individuata una causa estintiva del reato, stabilire gli effetti ed i limiti temporali di essa, in relazione allo stato dell'azione penale (cfr. sentenza n. 85 del 1998; ordinanze n. 219 del 1997; n. 137 e n. 294 del 1996); 
    che il principio è stato ritenuto estensibile anche alle cause sopravvenute di non punibilità legate a condotte lato sensu riparatorie, valendo anche in relazione a queste il rilievo che si tratta di una non punibilità dovuta a ragioni di politica criminale, e non già conseguente alla caduta dell'antigiuridicità per cause intrinseche al nucleo sostanziale dell'illecito (cfr. ordinanza n. 155 del 2005); 
    che ad analoga conclusione deve peraltro pervenirsi anche in rapporto all'istituto contemplato dall'art. 13, comma 3-quater, del d.lgs. n. 286 del 1998: istituto nel quale può scorgersi – al lume delle correnti ricostruzioni – una condizione di procedibilità atipica, che trova la sua ratio nel diminuito interesse dello Stato alla punizione di soggetti ormai estromessi dal proprio territorio, in un'ottica similare – anche se non identica – a quella sottesa alle previsioni degli artt. 9 e 10 cod. pen., non disgiunta, peraltro, da esigenze deflattive del carico penale; 
    che, sotto questo profilo, non può considerarsi, di per sé, manifestamente irrazionale ed arbitraria la scelta del legislatore di circoscrivere l'operatività della condizione di procedibilità in questione – sulla base della discrezionale valutazione che solo in detta ipotesi l'interesse al perseguimento del colpevole diminuisca a tal segno da giustificare la paralisi dell'azione penale (la quale potrà riprendere il suo libero corso unicamente ove lo straniero rientri illegalmente nel territorio dello Stato, entro i termini indicati dal comma 3-quinquies dell'art. 13 del d.lgs. n. 286 del 1998) – ai soli casi in cui non sia stata ancora instaurata la fase del giudizio, la quale ultima presuppone che gli elementi di accusa già raccolti abbiano raggiunto una adeguata consistenza; 
    che, in tale prospettiva, la disparità di trattamento denunciata dal rimettente – legata al gioco dei molteplici fattori che possono rendere più o meno celeri, nei singoli casi concreti, vuoi l'esecuzione dell'espulsione vuoi il corso dell'azione penale – viene a risolversi in una disparità di mero fatto, inidonea, come tale, per costante giurisprudenza di questa Corte, a fondare un giudizio di violazione del principio di eguaglianza (cfr., ex plurimis, ordinanze n. 155 del 2005 e n. 173 del 2003); 
    che per tal verso, dunque, la questione deve essere dichiarata manifestamente infondata; 
    che, quanto alle residue censure, il Tribunale rimettente afferma in modo puramente assiomatico che l'imputato espulso vedrebbe compromesso il proprio diritto di difesa e le ulteriori garanzie accordate dall'art. 111 Cost., sul presupposto di una sua facoltà di partecipazione personale al processo, senza prendere minimamente in considerazione – anche solo al fine di sindacarne la congruità – lo specifico strumento deputato, negli intenti del legislatore, a scongiurare l'anzidetto vulnus: vale a dire la facoltà di rientro in Italia per l'esercizio del diritto di difesa, che – sulla falsariga del previgente art. 7, comma 12-quinquies, del decreto-legge 30 dicembre 1989, n. 416 (Norme urgenti in materia di asilo politico, di ingresso e soggiorno dei cittadini extracomunitari e di regolarizzazione dei cittadini extracomunitari ed apolidi già presenti nel territorio dello Stato), convertito, con modificazioni, in legge 28 febbraio 1990, n. 39 – l'art. 17 del d.lgs. n. 286 del 1998 riconosce allo straniero espulso sottoposto a procedimento penale; 
    che, sotto tale profilo, la questione deve ritenersi pertanto manifestamente inammissibile per inadeguata ponderazione del quadro normativo; 
    che quanto, infine, alla questione sollevata dal Tribunale di Pesaro, l'ordinanza di rimessione difetta della descrizione della fattispecie concreta oggetto del giudizio a quo ed è del tutto priva di motivazione in ordine alla rilevanza della questione: carenze, queste, che implicano la manifesta inammissibilità della questione stessa (ex plurimis, ordinanze n. 432 e n. 333 del 2005). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 13, commi 3 e 3-quater, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), come modificato dalla legge 30 luglio 2002, n. 189 (Modifiche alla normativa in materia di immigrazione e di asilo), sollevata, in riferimento agli artt. 3 e 24 della Costituzione, dal Tribunale di Ascoli Piceno, sezione distaccata di S. Benedetto del Tronto, con l'ordinanza indicata in epigrafe; &#13;
    dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'art. 13, comma 3-quater, del citato decreto legislativo n. 286 del 1998 sollevate, in riferimento all'art. 3 della Costituzione, dal Tribunale di Lucera con le ordinanze indicate in epigrafe; &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale del medesimo art. 13, comma 3-quater, del decreto legislativo n. 286 del 1998 sollevate, in riferimento agli artt. 24, secondo comma, e 111 della Costituzione, dal Tribunale di Lucera con le ordinanze indicate in epigrafe; &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dello stesso art. 13, comma 3-quater, del decreto legislativo n. 286 del 1998 sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale di Pesaro con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 aprile 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 7 aprile 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
