<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/222/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/222/"/>
          <FRBRalias value="ECLI:IT:COST:2016:222" name="ECLI"/>
          <FRBRdate date="21/09/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="222"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/222/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/222/ita@/!main"/>
          <FRBRdate date="21/09/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/222/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/222/ita@.xml"/>
          <FRBRdate date="21/09/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/10/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Aldo Carosi</cc:relatore_pronuncia>
        <cc:data_decisione>21/09/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 545, quarto comma, del codice di procedura civile, promosso dal Tribunale ordinario di Viterbo, in funzione di giudice dell'esecuzione, con ordinanza del 2 dicembre 2015, iscritta al n. 79 del registro ordinanze 2016 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 16, prima serie speciale, dell'anno 2016.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 21 settembre 2016 il Giudice relatore Aldo Carosi.
 Ritenuto che il Tribunale ordinario di Viterbo, in funzione di giudice dell'esecuzione, con ordinanza in data 2 dicembre 2015, ha sollevato questione di legittimità costituzionale dell'articolo 545, quarto comma, del codice di procedura civile, per violazione degli artt. 1, 2, 3, 4 e 36 della Costituzione, nella parte in cui non prevede l'impignorabilità assoluta di quella parte della retribuzione necessaria a garantire al lavoratore i mezzi indispensabili alle sue esigenze di vita, e, in via subordinata, nella parte in cui non prevede le medesime limitazioni in materia di pignoramento di crediti tributari disposte dall'art. 72-ter (Limiti di pignorabilità) del decreto del Presidente della Repubblica 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), come introdotto dall'art. 3, comma 5, lettera b), del decreto-legge 2 marzo 2012, n. 16 (Disposizioni urgenti in materia di semplificazioni tributarie, di efficientamento e potenziamento delle procedure di accertamento), convertito, con modificazioni, dall'art. 1, comma 1, della legge 26 aprile 2012, n. 44;
 che, secondo quanto riferito dal giudice a quo, la questione è sorta nell'ambito di una procedura esecutiva promossa da Banca Mediolanum spa, ai danni del signor M.C., debitore della somma complessiva di euro 6.053,48, oltre alle spese della procedura esecutiva;
 che il terzo pignorato ha reso dichiarazione positiva del suo obbligo di corrispondere al debitore uno stipendio mensile rispettivamente di euro 600,00 (al netto delle ritenute previste dalla legge), comprensivi di assegni familiari per euro 136,54;
 che secondo il Tribunale rimettente si devono considerare l'art. 22 del d.P.R. 30 maggio 1955, n. 797 (Approvazione del testo unico delle norme concernenti gli assegni familiari), a mente del quale gli «[...] assegni familiari non possono essere sequestrati, pignorati o ceduti se non per causa di alimenti a favore di coloro per i quali gli assegni sono corrisposti», nonché l'art. 545, quarto comma, cod. proc. civ. secondo il quale, nel disciplinare i crediti impignorabili, stabilisce che «Tali somme possono essere pignorate nella misura di un quinto per i tributi dovuti allo Stato, alle province e ai comuni, ed in eguale misura per ogni altro credito»;
 che, pertanto, prosegue il giudice a quo, lo stipendio dell'esecutato sarebbe pignorabile fino ad un quinto, ammontante nel caso di specie ad euro 92,69, per cui resterebbero nella disponibilità del medesimo euro 370,66, non risultando agli atti che questi disponga di altre fonti di sostentamento. Al riguardo, osserva il Tribunale ordinario di Viterbo che se, invece, fosse applicabile alla fattispecie oggetto del giudizio il limite indicato dall'art. 72-ter del d.P.R. n. 602 del 1973, essendo la somma dovuta a titolo di stipendio inferiore ad euro 2.500,00 mensili, la stessa sarebbe pignorabile nel limite di un decimo e non di un quinto;
 che il rimettente dubita, quindi, della legittimità costituzionale dell'art. 545, quarto comma, cod. proc. civ., nella parte in cui non prevede l'impignorabilità assoluta di quella parte della retribuzione necessaria a garantire al lavoratore i mezzi indispensabili alle sue esigenze di vita;
 che lo stesso giudice deduce anche la violazione del principio di eguaglianza per disparità di trattamento sia in relazione al diverso regime afferente al pensionato, quale consolidatosi a seguito della sentenza della Corte n. 506 del 2002, sia, in via subordinata, in relazione al regime della riscossione dei crediti erariali fissato dall'art. 72-ter del d.P.R. n. 602 del 1973, come introdotto dall'art. 3, comma 5, lettera b), del d.l. n. 16 del 2012, convertito, con modificazioni, dall'art. 1, comma 1, della l. n. 44 del 2012;
 che è intervenuto il Presidente del Consiglio dei ministri per eccepire la non fondatezza della questione.
 Considerato che la questione sollevata risulta analoga a quella di cui è stata dichiarata la non fondatezza in riferimento agli artt. 3 e 36 della Costituzione, con sentenza di questa Corte n. 248 del 2015;
 che tale sentenza precisava, tra l'altro, che «la tutela della certezza dei rapporti giuridici, in quanto collegata agli strumenti di protezione del credito personale, non consente di negare in radice la pignorabilità degli emolumenti ma di attenuarla per particolari situazioni la cui individuazione è riservata alla discrezionalità del legislatore», mentre, con riguardo alla questione sollevata in riferimento all'art. 3 Cost., sia in relazione al regime di impignorabilità delle pensioni, sia - in via subordinata - all'art. 72-ter del d.P.R. n. 602 del 1973, le argomentazioni del giudice rimettente non sono state condivise «in ragione della eterogeneità dei tertia comparationis rispetto alla disposizione impugnata»;
 che relativamente alla norma impugnata con riferimento agli artt. 1, 2 e 4 Cost., la precitata sentenza n. 248 del 2015 ha ritenuto l'inammissibilità delle censure in quanto prive di un'argomentazione esaustiva sulle ragioni del preteso contrasto con le norme invocate;
 che - stante l'identità di contenuto tra l'ordinanza di rimessione oggetto della richiamata pronuncia del 2015 e quella odierna - le reiterate questioni vanno dichiarate manifestamente infondata con riferimento agli artt. 3 e 36 Cost., e manifestamente inammissibile con riguardo agli artt. 1, 2 e 4 Cost., per le stesse ragioni;
 che in questo senso la Corte si è già pronunciata con ordinanza n. 70 del 2016.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, commi 1 e 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 1) dichiara manifestamente inammissibile la questione di legittimità costituzionale dell'art. 545, quarto comma, del codice di procedura civile, sollevata, in riferimento agli artt. 1, 2 e 4 della Costituzione, dal Tribunale ordinario di Viterbo, in funzione di giudice dell'esecuzione, con l'ordinanza indicata in epigrafe;&#13;
 2) dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 545, quarto comma, cod. proc. civ., sollevata, in riferimento agli artt. 3 e 36 Cost., dal Tribunale ordinario di Viterbo, in funzione di giudice dell'esecuzione, con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 21 settembre 2016.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Aldo CAROSI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 12 ottobre 2016.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
