<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/426/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/426/"/>
          <FRBRalias value="ECLI:IT:COST:2005:426" name="ECLI"/>
          <FRBRdate date="16/11/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="426"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/426/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/426/ita@/!main"/>
          <FRBRdate date="16/11/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/426/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/426/ita@.xml"/>
          <FRBRdate date="16/11/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="25/11/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Alfonso Quaranta</cc:relatore_pronuncia>
        <cc:data_decisione>16/11/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Annibale MARINI; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 1, comma 2, lettera d), 2, comma 3, lettere c) ed e), 4, comma 3, lettere e), f) ed m) della legge della Regione Friuli-Venezia Giulia 25 ottobre 2004, n. 25 (Interventi a favore della sicurezza e dell'educazione stradale), promosso con ricorso del Presidente del Consiglio dei ministri notificato il 13 dicembre 2004, depositato in cancelleria il successivo 21 dicembre ed iscritto al n. 113 del registro ricorsi 2004. 
    Visto l'atto di costituzione della Regione autonoma Friuli-Venezia Giulia; 
    udito nell'udienza pubblica dell'11 ottobre 2005 il Giudice relatore Alfonso Quaranta;  
    uditi l'avvocato dello Stato Gabriella Palmieri per il Presidente del Consiglio dei ministri e l'avvocato Giandomenico Falcon per la Regione autonoma Friuli-Venezia Giulia.  
    Ritenuto che con ricorso depositato il 21 dicembre 2004 il Presidente del Consiglio dei ministri ha impugnato alcune disposizioni della legge della Regione Friuli-Venezia Giulia 25 ottobre 2004, n. 25 (Interventi a favore della sicurezza e dell'educazione stradale), ritenendo la legge censurabile «nel suo complesso ed in relazione alle specifiche disposizioni impugnate»; 
    che il ricorso contiene tre distinte censure; 
    che la prima riguarda l'art. 1, comma 2, lettera d), della legge regionale, secondo cui l'azione della Regione in tema di sicurezza ed educazione stradale mira a «coordinare sul territorio le azioni di soggetti che a vario titolo operano nel campo della sicurezza e dell'educazione stradale, ferme restando le competenze istituzionali di legge»;  
    che detta norma, secondo il ricorrente, «eccede la competenza statutaria della Regione, la quale ha soltanto competenza legislativa esclusiva nella materia della “viabilità” (art. 4, comma 1, punto 9, dello Statuto di autonomia)», mentre la sicurezza stradale attiene alla materia “ordine pubblico e sicurezza”, riservata allo Stato dall'art. 117, secondo comma, lettera h), della Costituzione; 
    che la seconda censura – fondata sui «medesimi motivi sopra esposti» – riguarda l'art. 2, comma 3, lettere c) ed e), della stessa legge, che indica le finalità del piano regionale della sicurezza stradale, tra cui il rafforzamento dell'azione di prevenzione, controllo e repressione, attraverso un coordinamento tra le forze di polizia, ed il miglioramento delle regole e dei controlli su veicoli, conducenti e servizi di trasporto; 
    che la terza censura riguarda l'art. 4, comma 3, lettere e), f) ed m), che comprende fra i componenti della Consulta regionale della sicurezza stradale il comandante del Comando Regione Carabinieri o suo delegato, il dirigente del Compartimento polizia stradale del Friuli-Venezia Giulia o suo delegato e un rappresentante dei Comandi provinciali dei vigili del fuoco, designato d'intesa fra gli stessi; 
    che la norma, secondo il ricorrente, inserendo in un organo regionale dipendenti dell'amministrazione statale, violerebbe l'art. 117, secondo comma, lettera g), della Costituzione, che attribuisce alla competenza legislativa esclusiva statale la materia dell'ordinamento degli organi e degli uffici dello Stato; 
    che la Regione Friuli-Venezia Giulia si è costituita depositando una memoria in cui sostiene l'inammissibilità e l'infondatezza del ricorso; 
    che in prossimità dell'udienza pubblica la Regione ha depositato una memoria illustrativa, deducendo – nell'ordine – l'inammissibilità del ricorso in quanto utilizza un parametro relativo al riparto delle funzioni legislative fra lo Stato e le Regioni a statuto ordinario; la cessazione della materia del contendere per effetto della sopravvenuta legge regionale 1° agosto 2005, n. 16 recante “Modifiche alla legge regionale 25 ottobre 2004, n. 25 (Interventi a favore della sicurezza e dell'educazione stradale)”, non impugnata dallo Stato, che ha abrogato le prime due norme censurate ed eliminato dalla terza le parti impugnate; e comunque l'infondatezza del ricorso nel merito; 
    che in data 7 ottobre 2005 l'Avvocatura generale dello Stato ha depositato la delibera con cui il Consiglio dei ministri ha rinunciato al ricorso; 
    che la Regione Friuli-Venezia Giulia ha accettato detta rinuncia.  
    Considerato che, ai sensi dell'art. 25 delle norme integrative per i giudizi dinanzi a questa Corte, la rinuncia al ricorso, seguita dall'accettazione della controparte, comporta l'estinzione del processo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi  &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara estinto il processo. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 16 novembre 2005.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Alfonso QUARANTA, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 25 novembre 2005.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
