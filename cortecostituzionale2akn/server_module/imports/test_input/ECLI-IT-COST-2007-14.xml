<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/14/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/14/"/>
          <FRBRalias value="ECLI:IT:COST:2007:14" name="ECLI"/>
          <FRBRdate date="10/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="14"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/14/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/14/ita@/!main"/>
          <FRBRdate date="10/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/14/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/14/ita@.xml"/>
          <FRBRdate date="10/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="26/01/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>10/01/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 198, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso con ordinanza del 10 maggio 2005 dal Giudice di pace di Milano, nel procedimento civile vertente tra Nonnis Marzano Alighiero e il Comune di Milano, iscritta al n. 114 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio dell'11 ottobre 2006 il Giudice relatore Alfio Finocchiaro. 
    Ritenuto che nel corso di un giudizio di opposizione a due verbali di accertamento di infrazioni all'art. 7, comma 14, del d.lgs. 30 aprile 1992, n. 285 (Nuovo codice della strada), per circolazione in zona a traffico limitato, promosso da Nonnis Marzano, il Giudice di pace di Milano, con ordinanza 10 maggio 2006, ha sollevato questione di legittimità costituzionale dell'art. 198, comma 2, dello stesso decreto legislativo, per violazione del principio di ragionevolezza; 
    che l'accertamento delle due infrazioni, secondo il giudice rimettente, era avvenuto in Milano, Corso Garibaldi, alla stessa data (29 febbraio 2004), a distanza di 31 secondi; 
    che, dovendosi comminare al trasgressore, in base all'art. 198, comma 2, del codice della strada, una sanzione per ogni violazione accertata, in deroga al principio di cui al comma 1, secondo il quale, per più violazioni della stessa disposizione, la sanzione è unica e può essere aumentata fino al triplo, ritiene il rimettente di sollevare la questione di legittimità costituzionale della norma indicata, nella parte in cui non consente al giudice di applicare, comunque, per più violazioni di una stessa disposizione, la sanzione prevista dalla legge, sia pure aumentata fino al triplo; 
    che la disposizione censurata contrasterebbe con il principio costituzionale di ragionevolezza, mancando la proporzionalità tra le sanzioni da applicare e la gravità delle violazioni commesse, conseguendone un trattamento ingiusto e irrazionale; 
    che, quanto alla rilevanza della questione, il giudice a quo osserva che, nella vigenza della norma impugnata, il ricorso in opposizione ai verbali di accertamento dovrebbe essere rigettato, e che le sanzioni irrogabili, dello stesso o di diverso importo, sarebbero comprese tra € 68,25 e € 275,10, laddove, se la norma venisse dichiarata illegittima, la sanzione da irrogare sarebbe unica, e aumentata fino al triplo; 
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo la declaratoria di inammissibilità e comunque la pronuncia di infondatezza della questione sollevata. 
    Considerato che il Giudice di pace di Milano dubita della legittimità costituzionale dell'art. 198, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), nella parte in cui, per le infrazioni commesse nelle zone a traffico limitato, non consente al giudice, in caso di più violazioni della stessa disposizione, di irrogare una sola sanzione sia pure aumentata fino al triplo, per violazione del principio di ragionevolezza di cui all'art. 3 della Costituzione; 
    che il giudice rimettente, in una fattispecie in cui le violazioni sono state accertate, sulla stessa strada, a distanza di 31 secondi l'una dall'altra, non ha in alcun modo motivato sull'applicabilità o meno del principio contenuto nell'art. 8-bis, comma 4, della legge 24 novembre 1981, n. 689 (Modifiche al sistema penale), secondo cui «Le violazioni amministrative successive alla prima non sono valutate, ai fini della reiterazione, quando sono commesse in tempi ravvicinati e riconducibili ad una programmazione unitaria»; 
    che proprio la contiguità temporale tra i due accertamenti e il fatto che siano stati compiuti lungo la stessa via, evidenziano che il giudice a quo è partito da un erroneo presupposto interpretativo, affermando la necessità dell'applicazione, nella fattispecie in esame, di due distinte sanzioni, senza esporre le ragioni per le quali non si ritiene potersi configurare non solo un'unica condotta, ma anche un'unica violazione, con il conseguente superamento del dubbio di costituzionalità sollevato, dal momento che non ad ogni accertamento deve necessariamente corrispondere una contravvenzione, trattandosi di condotte (la circolazione in zona vietata) di durata; 
    che tale vizio dell'ordinanza determina, sulla base della costante giurisprudenza di questa Corte, la manifesta infondatezza della questione (ordinanze nn. 118, 54 e 1 del 2005). 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 198, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata, in riferimento all'art. 3 della Costituzione, dal Giudice di pace di Milano, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 gennaio 2007. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Alfio FINOCCHIARO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 26 gennaio 2007. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
