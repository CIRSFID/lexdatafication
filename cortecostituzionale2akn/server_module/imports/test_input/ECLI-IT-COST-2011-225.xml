<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/225/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/225/"/>
          <FRBRalias value="ECLI:IT:COST:2011:225" name="ECLI"/>
          <FRBRdate date="19/07/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="225"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/225/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/225/ita@/!main"/>
          <FRBRdate date="19/07/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/225/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/225/ita@.xml"/>
          <FRBRdate date="19/07/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="21/07/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>QUARANTA</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>19/07/2011</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alfonso QUARANTA; Giudici : Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 275, comma 3, del codice di procedura penale, come modificato dall'art. 2 del decreto-legge 23 febbraio 2009, n. 11 (Misure urgenti in materia di sicurezza pubblica e di contrasto alla violenza sessuale, nonché in tema di atti persecutori), convertito, con modificazioni, dalla legge 23 aprile 2009, n. 38, promosso dal Giudice per le indagini preliminari del Tribunale di Torino nel procedimento penale a carico di V.G., con ordinanza del 3 dicembre 2010, iscritta al n. 54 del registro ordinanze 2011 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 14, prima serie speciale, dell'anno 2011.
 Udito nella camera di consiglio del 6 luglio 2011 il Giudice relatore Giuseppe Frigo.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che il Giudice per le indagini preliminari del Tribunale di Torino, con ordinanza del 3 dicembre 2010, ha sollevato, in riferimento agli artt. 3, 13 e 27, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 275, comma 3, del codice di procedura penale, come modificato dall'art. 2 del decreto-legge 23 febbraio 2009, n. 11 (Misure urgenti in materia di sicurezza pubblica e di contrasto alla violenza sessuale, nonché in tema di atti persecutori), convertito, con modificazioni, dalla legge 23 aprile 2009, n. 38, nella parte in cui non consente di applicare misure cautelari meno afflittive della custodia in carcere nei confronti della persona raggiunta da gravi indizi di colpevolezza in ordine al delitto di cui all'art. 575 del codice penale; 
 che il giudice rimettente è investito della decisione sull'istanza di sostituzione della custodia cautelare in carcere con gli arresti domiciliari presso una struttura sanitaria, proposta dal difensore di una persona sottoposta ad indagini per il reato di omicidio volontario commesso in danno di una zia convivente;
 che, in punto di fatto, il giudice a quo - premesso che la custodia cautelare in carcere era stata disposta a seguito di arresto in flagranza effettuato nell'abitazione dell'indagato, il quale aveva chiamato telefonicamente le forze dell'ordine affermando di aver soffocato la zia - riferisce che i Carabinieri, al momento dell'intervento, avevano trovato l'indagato in stato confusionale e con i segni evidenti di un tentativo di suicidio;
 che, nel corso dell'interrogatorio reso all'udienza di convalida dell'arresto, l'indagato aveva confessato di avere soffocato la zia e di avere, poi, tentato il suicidio in quanto non sapeva come affrontare la necessità di lasciare il proprio alloggio a seguito di una procedura di sfratto;
 che nel corso delle indagini preliminari, il pubblico ministero aveva disposto una consulenza tecnica sulla capacità di intendere e di volere dell'indagato, dalla quale era emerso che la sua personalità presentava «vulnerabilità ed aree di fragilità, pur in assenza di un franco quadro psicopatologico»;
 che il difensore, a sostegno dell'istanza di sostituzione della misura custodiale in atto, aveva evidenziato che le esigenze cautelari, ove ritenute ancora sussistenti, avrebbero potuto essere adeguatamente salvaguardate con la misura degli arresti domiciliari presso una struttura sanitaria, tenuto anche conto degli esiti di una consulenza di parte, attestante che l'indagato era affetto da «disturbo passivo-dipendente della personalità e da depressione di tipo mascherata»;
 che, ad avviso del giudice a quo, le ragioni addotte dalla difesa, unitamente alle particolarità della vicenda concreta - seppure inidonee a dimostrare il venir meno delle esigenze cautelari, connesse al pericolo di commissione di reati della stessa specie - farebbero ritenere effettivamente adeguata a soddisfarle la meno costrittiva misura degli arresti domiciliari presso una struttura sanitaria;
 che all'accoglimento dell'istanza osterebbe, tuttavia, la preclusione, introdotta dalla novella legislativa modificativa dell'art. 275, comma 3, cod. proc. pen., in forza della quale, quando sussistono gravi indizi di colpevolezza per una serie di reati, tra cui quello di omicidio volontario, «è applicata la custodia cautelare in carcere, salvo che siano acquisiti elementi dai quali risulti che non sussistono esigenze cautelari»;
 che, secondo il giudice rimettente, tale disposizione presenterebbe, però, profili di illegittimità costituzionale, con riferimento agli artt. 3, 13 e 27, secondo comma, Cost.;
 che, al riguardo, il giudice a quo rileva come questa Corte, con la sentenza n. 265 del 2010, abbia già dichiarato costituzionalmente illegittima la norma censurata, per contrasto con gli artt. 3, 13, primo comma, e 27, secondo comma, Cost., nella parte in cui - nel prevedere che, quando sussistono gravi indizi di colpevolezza in ordine ai delitti di cui agli artt. 600-bis, primo comma, 609-bis e 609-quater cod. pen., è applicata la custodia cautelare in carcere, salvo che siano acquisiti elementi dai quali risulti che non sussistono esigenze cautelari - non fa salva, altresì, l'ipotesi in cui siano acquisiti elementi specifici, in relazione al caso concreto, dai quali risulti che le esigenze cautelari possono essere soddisfatte con altre misure; 
 che, ad avviso del giudice a quo, le medesime considerazioni svolte dalla Corte nella citata sentenza varrebbero anche in relazione al delitto di omicidio volontario;
 che, infatti, pur non essendo possibile una gradazione della lesione del bene giuridico protetto dall'art. 575 cod. pen., i fatti concreti, riferibili a detto paradigma punitivo, potrebbero presentare connotazioni profondamente differenziate - in ragione del contesto in cui sono maturati, della personalità dell'agente, dell'elemento soggettivo o dei motivi a delinquere - tali da rendere necessaria una verifica in concreto del grado delle esigenze cautelari da soddisfare e la conseguente scelta della misura idonea a tal fine; 
 che, in questa prospettiva, la presunzione censurata si porrebbe in contrasto sia con il principio di uguaglianza, sancito dall'art. 3 Cost., per l'irrazionale assoggettamento ad un medesimo regime cautelare di situazioni differenti; sia con il principio di inviolabilità della libertà personale, enunciato dall'art. 13 Cost., in quanto determinerebbe il sacrificio di detto bene primario sulla base di una valutazione predeterminata che non tiene conto delle peculiarità dei casi concreti; sia, infine, con la presunzione di non colpevolezza, espressa dall'art. 27, secondo comma, Cost., in quanto attribuirebbe alla misura cautelare tratti funzionali tipici della pena, applicabile solo a seguito di un giudizio definitivo di responsabilità.
 Considerato che il Giudice per le indagini preliminari del Tribunale di Torino ha sollevato, in riferimento agli artt. 3, 13 e 27, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 275, comma 3, del codice di procedura penale, come modificato dall'art. 2 del decreto-legge 23 febbraio 2009, n. 11 (Misure urgenti in materia di sicurezza pubblica e di contrasto alla violenza sessuale, nonché in tema di atti persecutori), convertito, con modificazioni, dalla legge 23 aprile 2009, n. 38, nella parte in cui non consente di applicare misure cautelari meno afflittive della custodia in carcere nei confronti della persona raggiunta da gravi indizi di colpevolezza in ordine al delitto di cui all'art. 575 del codice penale;
 che, al di là della formulazione del petitum, il giudice a quo chiede, nella sostanza, di estendere al delitto di omicidio volontario la declaratoria di illegittimità costituzionale della norma censurata già pronunciata da questa Corte con la sentenza n. 265 del 2010, in riferimento a taluni delitti a sfondo sessuale: sentenza con la quale la presunzione assoluta di adeguatezza della sola custodia in carcere a soddisfare le esigenze cautelari relative a tali delitti, sancita dal novellato art. 275, comma 3, cod. proc. pen., è stata trasformata in presunzione solo relativa, superabile in presenza di elementi specifici che dimostrino l'idoneità allo scopo di altre misure;
 che, successivamente all'ordinanza di rimessione, questa Corte è già intervenuta nei sensi auspicati dal rimettente con la sentenza n. 164 del 2011, dichiarando l'illegittimità costituzionale della norma censurata, nella parte in cui - nel prevedere che, quando sussistono gravi indizi di colpevolezza in ordine al delitto di cui all'art. 575 cod. pen., è applicata la custodia cautelare in carcere, salvo che siano acquisiti elementi dai quali risulti che non sussistono esigenze cautelari - non fa salva, altresì, l'ipotesi in cui siano acquisiti elementi specifici, in relazione al caso concreto, dai quali risulti che le esigenze cautelari possono essere soddisfatte con altre misure; 
 che, dunque, la questione va dichiarata manifestamente inammissibile per sopravvenuta mancanza di oggetto, giacché, a seguito della sentenza da ultimo citata, la norma censurata dal giudice a quo - ossia quella che impedisce, per il delitto di omicidio volontario, di applicare misure diverse e meno afflittive della custodia carceraria, in presenza di specifici elementi che ne rivelino l'idoneità a soddisfare le esigenze cautelari - è già stata rimossa dall'ordinamento con efficacia ex tunc (ex plurimis, sentenza n. 80 del 2011, ordinanza n. 306 del 2010).
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 275, comma 3, del codice di procedura penale, come modificato dall'art. 2 del decreto-legge 23 febbraio 2009, n. 11 (Misure urgenti in materia di sicurezza pubblica e di contrasto alla violenza sessuale, nonché in tema di atti persecutori), convertito, con modificazioni, dalla legge 23 aprile 2009, n. 38, sollevata, in riferimento agli artt. 3, 13 e 27, secondo comma, della Costituzione, dal Giudice per le indagini preliminari del Tribunale di Torino, con l'ordinanza in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 luglio 2011.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 21 luglio 2011.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
