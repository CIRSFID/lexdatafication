<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/49/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/49/"/>
          <FRBRalias value="ECLI:IT:COST:2013:49" name="ECLI"/>
          <FRBRdate date="13/03/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="49"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/49/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/49/ita@/!main"/>
          <FRBRdate date="13/03/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/49/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/49/ita@.xml"/>
          <FRBRdate date="13/03/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="20/03/2013" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2013</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GALLO</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maria Napolitano</cc:relatore_pronuncia>
        <cc:data_decisione>13/03/2013</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Franco GALLO; Giudici : Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 1, comma 219, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), promosso dal Tribunale ordinario di Alessandria nel procedimento vertente tra l'Azienda Sanitaria Locale (ASL) di Alessandria e l'Istituto Nazionale per l'Assicurazione contro gli Infortuni sul Lavoro (INAIL) ed altra con ordinanza dell'8 marzo 2011, iscritta al n. 174 del registro ordinanze 2011 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 36, prima serie speciale, dell'anno 2011.
 Visto l'atto di costituzione dell'INAIL nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
 udito nell'udienza pubblica del 27 febbraio 2013 il Giudice relatore Paolo Maria Napolitano;
 uditi l'avvocato Lorella Frasconà per l'INAIL e l'avvocato dello Stato Diana Ranucci per il Presidente del Consiglio dei ministri.
 Ritenuto che, con ordinanza dell'8 marzo 2011, il Tribunale ordinario di Alessandria, in funzione di giudice del lavoro, ha sollevato questione incidentale di legittimità costituzionale dell'articolo 1, comma 219, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), nella parte in cui prevede l'esonero dal pagamento delle somme aggiuntive, delle maggiorazioni e degli interessi legali, previsti dal comma 217 del medesimo articolo 1 della legge n. 662 del 1996, nel caso di omesso versamento di contributi previdenziali, esclusivamente in favore delle amministrazioni dello Stato, centrali e periferiche, e degli enti locali e non anche in favore delle Aziende sanitarie locali (di seguito ASL);
 che il rimettente riferisce di essere chiamato a giudicare sulla opposizione proposta dalla ASL di Alessandria avverso due cartelle esattoriali, aventi ad oggetto il pagamento di sanzioni civili ed interessi, emesse a seguito di un accertamento, eseguito dall'Istituto Nazionale per l'Assicurazione contro gli Infortuni sul Lavoro (di seguito INAIL) nel giugno 2001, nel corso del quale era emerso che la predetta ASL aveva commesso delle irregolarità nelle dichiarazioni relative alle attività svolte dai propri dipendenti con conseguente omissione di versamenti previdenziali;  
 che, nell'opporsi a tali atti la ASL - oltre a contestare la sussistenza dell'omesso versamento, riconoscendo esclusivamente l'esistenza di irregolarità formali irrilevanti quanto all'ammontare dei versamenti dovuti, ed a formulare altre eccezioni di carattere preliminare - contestava la legittimità costituzionale del comma 219 dell'art. 1 della legge n. 662 del 1996, in quanto non applicabile ad essa ASL, assumendo che la detta disposizione fosse lesiva del principio di uguaglianza, di cui all'art. 3 Cost., e di quello di buon andamento della pubblica Amministrazione, di cui all'art. 97 Cost., poiché comportava lo sviamento dai fini istituzionali delle somme necessarie per il pagamento delle intimate sanzioni pecuniarie ed interessi;
 che, prosegue il rimettente, nel costituirsi nel giudizio a quo l'INAIL, oltre a contestare la fondatezza delle eccezioni formulate dalla parte ricorrente, negava l'applicabilità alla fattispecie dell'art. 1, comma 219, della legge n. 662 del 1996 in quanto la ASL ricorrente non era né un'amministrazione dello Stato né un ente locale;
 che il rimettente, ritenuta negativa la prognosi sulla fondatezza delle residue eccezioni formulate dalla ASL, si concentra su quella relativa alla possibile illegittimità costituzionale dell'art. 1, comma 219, della legge n. 662 del 1996;
 che, riguardo alla non manifesta infondatezza della questione di legittimità costituzionale di detta norma, il Tribunale di Alessandria osserva che non vi è dubbio che la ASL non sia né un'amministrazione dello Stato né un ente locale, nell'accezione, presupposta dalla disposizione in questione, di ente locale territoriale;
 che, pertanto, ad avviso del rimettente, la disposizione in questione, esonerando dal pagamento di sanzioni civili ed interessi gli enti locali e non le ASL, ha l'effetto di determinare un deteriore trattamento normativo di queste ultime; 
 che nella giurisprudenza della Corte costituzionale, con riferimento al regime di pignorabilità dei rispettivi beni, sulla base dell'affermata omogeneità fra enti locali e unità sanitarie locali prima e aziende sanitarie locali poi, è stata dichiarata la illegittimità costituzionale delle diverse discipline applicabili agli uni e alle altre;
 che, ribadita tale omogeneità fra enti locali e ASL, il rimettente ritiene non manifestamente infondato il dubbio di legittimità costituzionale dell'art. 1, comma 219, della legge n. 662 del 1996, nella parte in cui non prevede, come per gli enti locali, anche per le ASL l'esonero dal pagamento di somme aggiuntive, maggiorazioni ed interessi;
 che, ad avviso del rimettente, essendo le aziende sanitarie locali preposte alla cura di interessi pubblici, la destinazione di risorse finanziarie al pagamento delle predette somme aggiuntive e degli interessi, comportando il mancato utilizzo di quelle per l'espletamento dei fini istituzionali, «giustifica anche il dubbio di violazione dell'art. 97 Cost. quale principio di buon andamento della Pubblica amministrazione sotto il profilo della allocazione delle risorse per gli scopi istituzionali del soggetto pubblico»;
 che, riguardo alla rilevanza della questione, il rimettente osserva che solo ove fosse dichiarata la illegittimità costituzionale della norma censurata, con l'estensione del regime di esonero anche a favore della ASL, il ricorso dalla stessa presentato sarebbe accolto;
 che, ancora con riferimento alla rilevanza nel giudizio a quo della presente questione, il rimettente ritiene privo di rilievo il fatto che, secondo l'opinione prevalente, a seguito della entrata in vigore dell'art. 116, comma 11, della legge 23 dicembre 2000, n. 388 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge finanziaria per il 2001), il regime esonerativo sia venuto meno anche per amministrazioni statali ed enti locali;
 che infatti, chiarisce il giudice a quo, l'assoggettamento alle sanzioni civili ed agli interessi per lo Stato e gli enti locali si verifica per le omissioni contributive intervenute a partire dal 1° gennaio 2001, data di entrata in vigore della citata legge n. 388 del 2000, mentre i crediti portati dalle cartelle di pagamento sono  precedenti a tale data;
 che, da ultimo, il rimettente osserva che non vi è spazio per un'interpretazione costituzionalmente orientata della norma che estenda la portata dell'art. 1, comma 219, anche alle aziende sanitarie locali, in quanto, trattandosi di norma eccezionale, essa non è suscettibile di interpretazione estensiva;
 che si è costituita in giudizio l'INAIL, concludendo per la inammissibilità ovvero per l'infondatezza della questione;
 che secondo la difesa dell'Istituto il legislatore, nell'esercizio della sua discrezionalità, ha esentato, con disposizione avente i caratteri della eccezionalità, le amministrazioni centrali e gli enti locali dal pagamento delle somme in questione;
 che tale ricordata disposizione esonerativa non può dirsi irragionevole atteso che essa si applica ad una serie di soggetti «i cui pagamenti ricadono sul bilancio dello Stato»;
 che, stante l'oggettiva diversità esistente fra le amministrazioni dello Stato e gli enti locali, da una parte, e le aziende sanitarie dall'altra, non può dirsi esistente una disparità di trattamento costituzionalmente rilevante; 
 che, segnala la difesa dell'INAIL, sebbene sia vero che le aziende sanitarie fanno parte della pubblica amministrazione, tuttavia in tale locuzione sono comprese numerose altre entità, per lo più escluse dal campo di applicazione della norma;
 che, precisa la difesa dell'INAIL, imporre alle amministrazioni statali il pagamento delle sanzioni in questione si ridurrebbe, trattandosi di un ente strumentale dell'amministrazione statale, ad una "partita di giro" entro il bilancio dello Stato;
 che lo stesso risultato si otterrebbe riguardo agli enti locali, essendo questi parte del «bilancio pubblico allargato»;
 che analoga ratio non sarebbe, invece, ravvisabile in relazione alle aziende sanitarie, che, a decorrere dal 1993, hanno perso il carattere di enti strumentali delle Regioni e degli Enti locali, acquisendo autonomia imprenditoriale, cosa che induce a ritenere che si tratti di enti pubblici economici;
 che, aggiunge la difesa dell'ente previdenziale, le precedenti decisioni della Corte costituzionale richiamate dal rimettente non sarebbero pertinenti, in quanto relative alla normativa applicabile ai rapporti fra enti locali e aziende sanitarie da una parte e soggetti privati dall'altra, mentre il soggetto con cui la ASL ora si relaziona è un altro ente pubblico;
 che l'eventuale accoglimento della questione determinerebbe il rischio di un ulteriore allargamento della disciplina esonerativa in favore degli altri enti pubblici economici;
 che, relativamente all'infondatezza della questione di legittimità costituzionale argomentata con riferimento all'art. 97 Cost., la difesa dell'INAIL osserva che il rimettente non ha considerato che dall'eventuale accoglimento di essa conseguirebbe uno scompenso nel bilancio dell'ente previdenziale; 
 che, nell'equilibrare le opposte esigenze, il legislatore avrebbe tenuto, ragionevolmente, in maggiore considerazione le esigenze dell'ente previdenziale rispetto a quelle della azienda sanitaria, ente economico con gestione privatistica, tenuta al pagamento di somme aggiuntive e di interessi a causa della sua scarsa diligenza gestionale;
 che é intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dalla Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o infondata;
 che l'Avvocatura, richiamata sia la giurisprudenza della Corte costituzionale che quella del giudice amministrativo, rileva che effettivamente non è errato attribuire la medesima natura agli enti locali territoriali ed alle aziende sanitarie; 
 che la questione di legittimità costituzionale sollevata dal rimettente sarebbe però, comunque, infondata in quanto, basandosi sulla pretesa di estendere a soggetti non riguardati dalla norma censurata il regime dalla medesima previsto, non considera che il diverso trattamento normativo riveniente dalla norma censurata - avente peraltro il carattere della eccezionalità - è frutto di una scelta discrezionale del legislatore, insindacabile ove non trasmodi nella arbitrarietà, in questo caso non ravvisabile se si consideri che gli enti locali sono soggetti ad una normativa specifica, ontologicamente diversa rispetto a quella delle ASL.
 Considerato che il Tribunale ordinario di Alessandria, in funzione di giudice del lavoro, dubita della legittimità costituzionale dell'art. 1, comma 219, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), secondo il quale sono esonerate dal pagamento delle somme aggiuntive e delle maggiorazioni previste dal comma 217 dello stesso art. 1 della legge n. 662 del 1996 - nel caso di mancato o ritardato pagamento di contributi o premi previdenziali ovvero di evasione di essi connessa a registrazioni e denunce non veritiere - le amministrazioni statali, locali e periferiche, nonché gli enti locali, e non anche le aziende sanitarie locali (di seguito ASL);
 che il Tribunale rimettente, ritenuta omogenea la situazione giuridica di enti locali e di ASL, ravvisa in siffatta disciplina i caratteri della ingiustificata disparità di trattamento, in violazione dell'art. 3 Cost.;
 che il rimettente ritiene altresì che l'assoggettamento delle ASL al predetto obbligo costituirebbe anche violazione dell'art. 97 Cost., sotto il profilo della necessaria salvaguardia del buon andamento della pubblica amministrazione, in quanto comporterebbe lo sviamento delle risorse finanziarie delle ASL per scopi diversi da quelli propri di tali enti e la sottrazione di tali risorse al soddisfacimento di detti scopi;
 che, quanto al profilo della rilevanza, deve ritenersi non implausibile l'interpretazione del giudice rimettente, il quale afferma che essa non è venuta meno per effetto dell'art. 116, comma 11, della legge 23 dicembre 2000, n. 388 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge finanziaria 2001), in base al quale, secondo la lettura datane dal rimettente, a decorrere dalla sua entrata in vigore, non sarebbe più applicabile alle amministrazioni dello Stato ed agli enti locali il precedente più favorevole regime esonerativo;
 che a tale conclusione si giunge poiché, concernendo l'accertamento eseguito dall'INAIL nei confronti della ASL di Alessandria un periodo temporale precedente alla entrata in vigore della disposizione ultima citata, la quale non esplica un'efficacia retroattiva, la asserita disparità di trattamento sarebbe, con riferimento al detto periodo, ancora riscontrabile;
 che la questione di legittimità costituzionale ora in esame trae origine dall'assunto, enunciato dal giudice a quo, secondo il quale sussisterebbe una sostanziale omogeneità soggettiva fra gli enti locali, diretti destinatari della disposizione legislativa oggetto di censura, e le ASL;
 che corollario di tale assunto sarebbe la illegittimità costituzionale della disposizione legislativa medesima che, accordando un particolare trattamento, esonerativo dalla applicazione di determinati obblighi, alle sole amministrazioni dello Stato, centrali e periferiche, ed agli enti locali - nella riconosciuta accezione di enti locali territoriali -, violerebbe in maniera ingiustificata il principio di eguaglianza non accordando il medesimo trattamento di favore alle aziende sanitarie locali;
 che l'assunto in questione non è fondato;
 che, anzi, è evidente la distinzione fra le due tipologie di soggetti, pur ambedue appartenenti al più ampio genere della soggettività pubblica, sol che si consideri, fra i numerosi indici di diversità riscontrabili fra essi, che gli uni, gli enti locali territoriali, sono soggetti giuridici esponenziali di una determinata comunità radicata su di un territorio e sono costituiti a fini amministrativi di carattere tendenzialmente generale, mentre le aziende sanitarie, soggetti funzionali aventi evidentemente finalità di carattere esclusivamente settoriale, non sono espressive di alcuna comunità;
 che, ancora, mentre gli enti locali territoriali sono dotati, sia pure in forma meno spiccata rispetto allo Stato, di poteri autoritativi che esercitano attraverso gli strumenti del diritto amministrativo, le aziende sanitarie si caratterizzano, secondo il prevalente e consolidato orientamento interpretativo, per essere enti pubblici economici esercenti la loro attività utendo iure privatorum (Corte di cassazione, sezioni unite, 30 gennaio 2008, n. 2031; Consiglio di Stato, sez. VI, 14 dicembre 2004, n. 5924; Consiglio di Stato, sez. V, 9 maggio 2001, n. 2609);
 che non conduce ad una diversa conclusione l'analisi di precedenti decisioni assunte da questa Corte, ed invocate dal giudice a quo a sostegno della sua tesi (si tratta delle sentenze n. 211 del 2003, n. 69 del 1998 e n. 285 del 1995) - nelle quali si richiama la «omogeneità delle situazioni giuridiche riferibili, rispettivamente alle unità sanitarie locali e agli enti locali» (sentenza n. 211 del 2003) - laddove si consideri che la richiamata omogeneità non deve intendersi, come invece fatto dal rimettente, riferita ad una pretesa omogeneità soggettiva fra gli enti in esame ma alla omogeneità della situazione giuridica in cui essi si trovavano rispetto alla disciplina normativa allora scrutinata, essendo ambedue da ascriversi nell'ambito dei debitori inadempienti nei confronti di un terzo assoggettati, in ragione di tale rapporto obbligatorio non adempiuto, a procedura esecutiva;
 che, infatti, il predetto richiamo deve essere inquadrato, onde coglierne il reale significato e la effettiva portata, come pertinente al particolare contesto normativo allora oggetto di scrutinio, in quanto nell'occasione si esaminava la compatibilità costituzionale del diverso regime di pignorabilità di somme di spettanza di enti locali e di aziende sanitarie - nell'ottica del diverso grado di tutela apprestato nel corso della procedura esecutiva ai terzi che rivestivano la qualifica di creditore degli uni o delle altre - al fine di garantire il rispetto del principio della par condicio creditorum; 
 che, pertanto, esclusa la sovrapponibilità fra le due tipologie di enti pubblici in discorso, deve escludersi che sia riscontrabile un vizio di costituzionalità nel fatto che il legislatore, facendo uso dei propri poteri, abbia inteso applicare solo a taluno di essi un regime più favorevole di quello generalmente applicato;
 che neppure può essere fonte di discriminazione costituzionalmente rilevante il fatto che il legislatore non abbia esteso ad altri soggetti tale privilegio, in quanto, per costante giurisprudenza di questa Corte, non è fonte di illegittimità costituzionale il limite alla estensione di norme che, come quella ora in esame, costituiscono deroghe a principi generali (ex multis: sentenza n. 131 del 2009);
 che, esclusa la violazione dell'art. 3 Cost. da parte della disposizione censurata, deve, parimenti, escludersi che essa, imponendo, a determinate condizioni, alle ASL il pagamento delle somme previste dal comma 217 dell'art. 1 della legge n. 662 del 1996, determini - per il solo fatto che le somme necessarie per fare fronte agli impegni finanziari derivanti dall'applicazione del citato comma 217 siano sottratte agli scopi istituzionali delle ASL - la violazione del principio di buon andamento della pubblica amministrazione di cui all'art. 97 Cost.;
 che tale principio, infatti, non può dirsi violato dal compimento di alcun atto la cui esecuzione sia imposta ad una pubblica amministrazione da una disposizione di legge di per sé legittima, atteso che, a pena di un'insanabile contraddizione ed incoerenza dell'ordinamento, cardine fondamentale su cui ruota il concetto di buon andamento della pubblica amministrazione deve essere il rispetto da parte di quest'ultima dei legittimi precetti legislativi.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara manifestamente infondata la questione di legittimità costituzionale dell'articolo 1, comma 219, della legge 23 dicembre 1996, n. 662 (Misure di razionalizzazione della finanza pubblica), sollevata, in riferimento agli artt. 3 e 97 Cost., dal Tribunale ordinario di Alessandria, in funzione di giudice del lavoro, con l'ordinanza in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 marzo 2013.&#13;
 F.to:&#13;
 Franco GALLO, Presidente&#13;
 Paolo Maria NAPOLITANO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 20 marzo 2013.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
