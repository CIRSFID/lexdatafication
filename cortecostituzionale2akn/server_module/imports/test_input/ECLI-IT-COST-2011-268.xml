<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/268/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/268/"/>
          <FRBRalias value="ECLI:IT:COST:2011:268" name="ECLI"/>
          <FRBRdate date="05/10/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="268"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/268/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/268/ita@/!main"/>
          <FRBRdate date="05/10/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/268/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/268/ita@.xml"/>
          <FRBRdate date="05/10/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/10/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>QUARANTA</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>05/10/2011</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alfonso QUARANTA; Giudici : Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 219-bis, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), introdotto dall'art. 3, comma 48, della legge 15 luglio 2009, n. 94 (Disposizioni in materia di sicurezza pubblica), promosso dal Giudice di pace di Cagliari nel procedimento vertente tra F. R. e il Comune di Villasimius con ordinanza dell'11 marzo 2010, iscritta al n. 18 del registro ordinanze 2011 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 6, prima serie speciale, dell'anno 2011.
 Udito nella camera di consiglio del 21 settembre 2011 il Giudice relatore Alfio Finocchiaro.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che con ordinanza dell'11 marzo 2010 il Giudice di pace di Cagliari ha sollevato questione di legittimità costituzionale dell'art. 219-bis del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), introdotto dall'art. 3, comma 48, della legge 15 luglio 2009, n, 94 (Disposizioni in materia di sicurezza pubblica), con riferimento all'art. 3 della Costituzione, nella parte in cui dispone che se il conducente è persona munita di patente di guida, nell'ipotesi in cui il nuovo codice della strada stabilisca le sanzioni amministrative accessorie del ritiro, della sospensione o della revoca della patente di guida, le stesse sanzioni amministrative accessorie si applicano anche quando le violazioni sono commesse alla guida di un veicolo per il quale non è richiesta la patente di guida;
 che il rimettente riferisce che in tal modo si verrebbe a determinare una disparità di trattamento tra coloro che hanno una abilitazione alla guida (e come tali sanzionabili anche con le sanzioni accessorie) e coloro invece che ne sono sprovvisti (quindi non sanzionabili con uguali sanzioni).
 Considerato che il Giudice di pace di Cagliari dubita della legittimità costituzionale dell'art. 219-bis, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada) - introdotto dall'art. 3, comma 48, della legge 15 luglio 2009, n. 94 (Disposizioni in materia di sicurezza pubblica) - nella parte in cui prevede «l'applicazione delle sanzioni amministrative accessorie del ritiro, sospensione o revoca della patente di guida per chi commette violazioni» del codice della strada «conducendo un veicolo per cui la patente non è richiesta» e nella parte in cui stabilisce «che trovano altresì applicazione le disposizioni di cui all'art. 126-bis, in materia di patente a punti», per violazione dell'art. 3 della Costituzione;
 che, successivamente all'ordinanza di rimessione, la norma censurata è stata abrogata dall'art. 43, comma 6, della legge 29 luglio 2010, n. 120 (Disposizioni in materia di sicurezza stradale), con decorrenza dal 30 luglio 2010;
 che, a prescindere dai molteplici profili di inammissibilità della questione, per l'assoluta carenza di motivazione in punto di rilevanza e di non manifesta infondatezza occorre restituire gli atti al giudice rimettente, perché operi una nuova valutazione della rilevanza e della non manifesta infondatezza della questione (ex plurimis ordinanze n. 201 del 2011; n. 145 e n. 38 del 2010).</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 ordina la restituzione degli atti al Giudice di pace di Cagliari.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 ottobre 2011.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Alfio FINOCCHIARO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 12 ottobre 2011.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
