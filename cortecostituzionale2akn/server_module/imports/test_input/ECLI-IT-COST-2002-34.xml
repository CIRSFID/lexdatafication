<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/34/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/34/"/>
          <FRBRalias value="ECLI:IT:COST:2002:34" name="ECLI"/>
          <FRBRdate date="14/02/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="34"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/34/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/34/ita@/!main"/>
          <FRBRdate date="14/02/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/34/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/34/ita@.xml"/>
          <FRBRdate date="14/02/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="26/02/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Gustavo Zagrebelsky</cc:relatore_pronuncia>
        <cc:data_decisione>14/02/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: 
 Presidente: Cesare RUPERTO; 
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, 
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, 
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria 
FLICK, Francesco AMIRANTE;</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>Ordinanza</docType>nel  giudizio di legittimità costituzionale dell'art. 402 del codice 
penale promosso con ordinanza emessa il 6 ottobre 2000 dalla Corte di 
cassazione  nel  procedimento  penale  a  carico di A.G., iscritta al 
n. 137  del  registro  ordinanze  2001  e  pubblicata  nella Gazzetta 
Ufficiale della Repubblica n. 9, 1ª serie speciale, dell'anno 2001. 
    Udito  nella  camera  di consiglio del 16 gennaio 2002 il giudice 
relatore Gustavo Zagrebelsky. 
    Ritenuto  che  con  ordinanza  del  6 ottobre  2000  la  Corte di 
cassazione  ha sollevato questione di costituzionalità dell'art. 402 
del  codice  penale  (Vilipendio  della  religione  dello  Stato), in 
riferimento  agli  artt. 3,  primo  comma,  e  8,  primo comma, della 
Costituzione; 
        che,  premesse  le  vicende  del giudizio di merito, la Corte 
rimettente  sottolinea  in  primo luogo la rilevanza della questione, 
trattandosi  di verificare la legittimità costituzionale della norma 
incriminatrice oggetto della contestazione all'imputato; 
        che,  quanto  alla  non  manifesta  infondatezza, la Corte di 
cassazione   muove   dalla   prima   decisione   resa   dalla   Corte 
costituzionale  sull'art. 402 cod. pen. (sentenza n. 39 del 1965) con 
la  quale  era  stata  rigettata  una questione di costituzionalità, 
riferita  agli artt. 3, 8, 19 e 20 della Costituzione, essenzialmente 
sul   rilievo   che  la  tutela  penale  rafforzata  della  religione 
cattolica,  rispetto  alle altre confessioni, trovava giustificazione 
nella  sua connotazione di religione professata dalla maggioranza dei 
cittadini,  e  dunque  nella maggiore  ampiezza  e  intensità  delle 
reazioni  sociali  alle  offese  che  alla stessa religione potessero 
essere rivolte; 
        che  tuttavia  - prosegue la Corte di cassazione - la nozione 
di "religione dello Stato", cui si riferisce la norma in esame, oltre 
a  essere divenuta incompatibile con il principio supremo di laicità 
dello  Stato, quale affermato dalle sentenze n. 203 del 1989 e n. 149 
del 1995 della Corte costituzionale, è stata comunque superata dalle 
modifiche  concordatarie  del  1984,  là  dove  si  afferma  che "si 
considera non più in vigore il principio, originariamente richiamato 
dai  Patti lateranensi, della religione cattolica come sola religione 
dello Stato italiano" (punto 1 del Protocollo addizionale all'accordo 
di  modifica  del  Concordato, ratificato con la legge 25 marzo 1985, 
n. 121); 
        che,  a tale riguardo, la Cassazione rimettente rileva che la 
Corte  costituzionale  ha ritenuto che l'espressione "religione dello 
Stato"  utilizzata  nel  codice  penale,  una  volta  venuta  meno la 
possibilità  di  attribuirle  l'originario significato, non ha altro 
senso se non quello di un semplice "tramite linguistico" con il quale 
viene  indicata  la  religione  cattolica (sentenze n. 925 del 1988 e 
n. 440 del 1995); 
        che,  tutto  ciò posto, e richiamati taluni passaggi di più 
recenti  decisioni  della Corte costituzionale, che hanno sempre più 
nettamente  abbandonato  il  criterio "quantitativo" iniziale, fino a 
escludere  in  modo  esplicito  l'ammissibilità  di differenziazioni 
nella  tutela  del  sentimento  religioso (sentenze n. 440 del 1995 e 
n. 329  del 1997), la Corte di cassazione dubita della compatibilità 
tra l'art. 402 cod. pen. - che mantiene una effettiva discriminazione 
tra  confessioni  religiose,  tutelando  esclusivamente  la religione 
cattolica   -  e  i  principi  di  uguaglianza  e  di  parità  delle 
confessioni religiose espressi dagli artt. 3 e 8 della Costituzione. 
    Considerato  che  la  Corte di cassazione solleva, in riferimento 
agli  artt. 3,  primo  comma,  e  8, primo comma, della Costituzione, 
questione  di  legittimità  costituzionale  dell'art. 402 del codice 
penale (Vilipendio della religione dello Stato); 
        che,  con  sentenza n. 508 del 2000, successiva all'ordinanza 
di  rimessione,  questa  Corte,  chiamata  a pronunciarsi su identica 
questione sollevata dalla stessa Cassazione rimettente, ha dichiarato 
l'illegittimità   costituzionale   dell'art. 402   cod.   pen.,  per 
violazione degli artt. 3 e 8 della Costituzione; 
        che,   pertanto,  essendo  stata  la  norma  denunciata  già 
dichiarata  incostituzionale,  la  questione  sollevata  deve  essere 
dichiarata manifestamente inammissibile. 
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, 
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi 
davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
                        LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p/>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso </block>
    </conclusions>
  </judgment>
</akomaNtoso>
