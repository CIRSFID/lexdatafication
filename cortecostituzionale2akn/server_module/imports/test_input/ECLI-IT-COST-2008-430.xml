<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/430/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/430/"/>
          <FRBRalias value="ECLI:IT:COST:2008:430" name="ECLI"/>
          <FRBRdate date="15/12/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="430"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/430/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/430/ita@/!main"/>
          <FRBRdate date="15/12/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/430/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/430/ita@.xml"/>
          <FRBRdate date="15/12/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/12/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>FLICK</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Tesauro</cc:relatore_pronuncia>
        <cc:data_decisione>15/12/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione della Camera dei deputati del 2 febbraio 2005 (Doc. IV-quater, n. 112), relativa alla insindacabilità, ai sensi dell'articolo 68, primo comma, della Costituzione, delle opinioni espresse dal deputato Guglielmo Rositani nei confronti di Mauro Meli, promosso con ricorso del Giudice dell'udienza preliminare del Tribunale di Cagliari, notificato il 12 giugno 2008, depositato in cancelleria il 29 luglio 2008 ed iscritto al n. 17 del registro conflitti tra poteri dello Stato 2007, fase di merito. 
    Visto l'atto di costituzione della Camera dei deputati; 
    udito nella camera di consiglio del 3 dicembre 2008 il Giudice relatore Giuseppe Tesauro. 
    Ritenuto che il Giudice dell'udienza preliminare presso il Tribunale di Cagliari, con ricorso del 21 novembre 2007, ha promosso conflitto di attribuzione fra poteri dello Stato nei confronti della Camera dei deputati, in relazione alla delibera adottata nella seduta del 2 febbraio 2005 (Doc. IV-quater, n. 112), con la quale è stato dichiarato, in conformità alla proposta della Giunta per le autorizzazioni, che i fatti oggetto del procedimento penale a carico del deputato Guglielmo Rositani concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, come tali insindacabili ai sensi dell'articolo 68, primo comma, della Costituzione; 
    che il giudice ricorrente premette che il deputato Rositani è imputato in ordine al reato di diffamazione a mezzo stampa, per aver offeso la reputazione di Mauro Meli, all'epoca sovrintendente del Teatro lirico di Cagliari, mediante dichiarazioni rese il 3 marzo 2003 nel corso di una conferenza stampa presso il Consiglio regionale della Sardegna, successivamente riprese dai quotidiani “L'Unione sarda” e “La Nuova Sardegna”, dall'ANSA e da alcune emittenti televisive locali; 
    che, ad avviso del ricorrente, le anzidette dichiarazioni non presenterebbero alcuna sostanziale corrispondenza di significato rispetto all'interrogazione a risposta immediata del 7 dicembre 2002, richiamata nella relazione della Giunta per le autorizzazioni e, dunque, non potrebbero considerarsi coperte dalla prerogativa di cui all'art. 68, primo comma, della Costituzione; 
    che, pertanto, il giudice ritiene illegittima la delibera della Camera dei deputati del 2 febbraio 2005 e chiede alla Corte costituzionale di disporne l'annullamento; 
    che il conflitto è stato dichiarato ammissibile con ordinanza n. 141 del 2008, depositata il 14 maggio 2008 e notificata alla Camera dei deputati, a cura del ricorrente, il 12 giugno 2008; 
    che lo stesso ricorrente ha provveduto al prescritto deposito degli atti presso la cancelleria di questa Corte, avvalendosi del servizio postale, con plico spedito il 23 luglio 2008, pervenuto il successivo 29 luglio; 
    che nel giudizio si è costituita la Camera dei deputati, chiedendo di dichiarare il ricorso inammissibile e comunque infondato; 
    che in una successiva memoria la Camera dei deputati ha eccepito l'improcedibilità del giudizio, dovuta al deposito del ricorso oltre il termine di cui all'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. 
    Considerato che il ricorso è stato notificato alla Camera dei deputati, unitamente all'ordinanza che ha dichiarato ammissibile il conflitto, in data 12 giugno 2008, e che gli atti sono stati depositati presso la cancelleria di questa Corte con plico spedito il 23 luglio 2008, oltre il termine di venti giorni dall'ultima notificazione previsto dall'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale; 
    che tale deposito deve considerarsi tardivo, essendo il predetto termine perentorio (da ultimo, ordinanze n. 253 e n. 134 del 2007);  
    che, pertanto, il giudizio deve essere dichiarato improcedibile.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara improcedibile il giudizio per conflitto di attribuzione tra poteri dello Stato promosso dal Giudice dell'udienza preliminare presso il Tribunale di Cagliari nei confronti della Camera dei deputati, con il ricorso indicato in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 15 dicembre 2008.  &#13;
F.to:  &#13;
Giovanni Maria FLICK, Presidente  &#13;
Giuseppe TESAURO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 19 dicembre 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
