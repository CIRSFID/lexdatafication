<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/6/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/6/"/>
          <FRBRalias value="ECLI:IT:COST:2005:6" name="ECLI"/>
          <FRBRdate date="10/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="6"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/6/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/6/ita@/!main"/>
          <FRBRdate date="10/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/6/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/6/ita@.xml"/>
          <FRBRdate date="10/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="11/01/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CONTRI</cc:presidente>
        <cc:relatore_pronuncia>Alfonso Quaranta</cc:relatore_pronuncia>
        <cc:data_decisione>10/01/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Fernanda CONTRI; Giudici: Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 4, comma 167, della legge 24 dicembre 2003, n. 350 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2004), promosso con ricorso della Regione Lazio, notificato il 24 febbraio 2004, depositato in cancelleria il 3 marzo 2004 ed iscritto al n. 30 del registro ricorsi 2004. 
    Udito nell'udienza pubblica del 14 dicembre 2004 il Giudice relatore Alfonso Quaranta. 
    Ritenuto che, con ricorso notificato in data 24 febbraio 2004 e depositato presso la cancelleria della Corte il successivo 3 marzo, la Regione Lazio ha impugnato l'art. 4, comma 167, della legge 24 dicembre 2003, n. 350 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2004), il quale prevede che al fine di «potenziare la ricerca biomedica in Italia e in particolare nelle aree territoriali di cui all'obiettivo 2, è assegnato all'Università campus bio-medico (CBM), di cui all'articolo 19 del decreto del Presidente della Repubblica 28 ottobre 1991, (…) l'importo di 20 milioni di euro per l'anno 2004 e di 30 milioni di euro per l'anno 2005 per la realizzazione di un policlinico universitario»; 
    che la ricorrente ritiene che tale norma si porrebbe in contrasto con gli artt. 97, 114, primo comma, 117, terzo comma, 118, primo comma, e 119 della Costituzione, nonché con il «principio di ragionevolezza, sussidiarietà e leale collaborazione». In particolare, essendo l'oggetto della disciplina della disposizione impugnata relativo alla «ricerca biomedica», lo stesso rientrerebbe nell'ambito della materia della “ricerca scientifica e tecnologica” ricadente nella potestà legislativa concorrente ex art. 117, terzo comma, della Costituzione; 
    che il legislatore statale si sarebbe, pertanto, dovuto limitare a fissare i principî fondamentali e non anche spingersi, come è avvenuto nel caso in esame, a dettare «un precetto puntuale e mirato che concretizza una palese violazione della sfera di autonomia regionale garantita dalla Costituzione»;  
    che la ricorrente evidenzia, inoltre, la mancanza delle condizioni che la giurisprudenza costituzionale (si richiama quanto statuito nella sentenza n. 303 del 2003) ha indicato perché si possa realizzare, ex art. 118 della Costituzione, un'attrazione a livello statale delle funzioni amministrative e legislative; 
    che, infatti: «a) non è prevista alcuna forma di coinvolgimento della Regione sul cui territorio sono destinati a prodursi gli effetti giuridici dell'intervento statale; b) l'intervento dello Stato (…) è motivato in modo del tutto generico e non è fondato su una reale ed effettiva esigenza di tutela di un interesse unitario della Repubblica non suscettibile di localizzazione a livello regionale; c) non sono rispettati i parametri di proporzionalità e ragionevolezza, in quanto la realizzazione di un Policlinico universitario da parte dell'Università “Campus Biomedico” di Roma può concretamente alterare e vanificare la complessa attività di programmazione delineata dalla Regione Lazio in materia sanitaria e di tutela della salute e in materia di diritto allo studio (…)»; 
    che il Presidente del Consiglio dei ministri non si è costituito in giudizio; 
    che in data 24 novembre 2004 la Regione Lazio ha depositato atto di rinuncia al ricorso. 
    Considerato che, in assenza di parti costituite, la rinuncia al ricorso comporta, ai sensi dell'art. 25 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara estinto il processo. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 gennaio 2005. &#13;
F.to: &#13;
Fernanda CONTRI, Presidente &#13;
Alfonso QUARANTA, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria l'11  gennaio 2005. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
