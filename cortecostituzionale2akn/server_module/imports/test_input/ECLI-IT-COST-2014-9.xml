<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/9/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/9/"/>
          <FRBRalias value="ECLI:IT:COST:2014:9" name="ECLI"/>
          <FRBRdate date="15/01/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="9"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/9/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/9/ita@/!main"/>
          <FRBRdate date="15/01/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/9/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/9/ita@.xml"/>
          <FRBRdate date="15/01/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/01/2014" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2014</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>SILVESTRI</cc:presidente>
        <cc:relatore_pronuncia>Mario Rosario Morelli</cc:relatore_pronuncia>
        <cc:data_decisione>15/01/2014</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Gaetano SILVESTRI; Giudici : Luigi MAZZELLA, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 23, comma 12, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso dal Giudice di pace di Verona, nel procedimento vertente tra la Pubbliuno Srl e il Comune di Bussolengo, con ordinanza del 5 febbraio 2013, iscritta al n. 122 del registro ordinanze 2013 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 23, prima serie speciale, dell'anno 2013.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 6 novembre 2013 il Giudice relatore Mario Rosario Morelli.
 Ritenuto che il Giudice di pace di Verona, nel corso di un giudizio di opposizione a processo verbale per contestata violazione dell'art. 23, commi 6 e 12, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), con ordinanza del 5 febbraio 2013, ha sollevato, in riferimento all'art. 3 Cost., questione di legittimità costituzionale del citato art. 23, comma 12, del d.lgs. n. 285 del 1992; 
 che il giudice a quo, premesso che all'opponente era stata irrogata, in relazione alla collocazione di un cartello pubblicitario in modo difforme da quanto previsto nel provvedimento autorizzatorio, una sanzione pecuniaria di euro 1.376,55, pari al triplo di quella (asseritamente) prevista in relazione alla violazione consistente nella collocazione di un cartello in totale assenza di titolo autorizzativo - donde la rilevanza della questione -, ha ravvisato la violazione dei principi di uguaglianza e di ragionevolezza nella discriminazione che, in forza dell'applicazione della norma impugnata, verrebbe a crearsi a carico di chi, in possesso di una autorizzazione, per il solo fatto di non rispettare in parte le indicazioni in essa contenute (ad esempio la distanza dei cartelli pubblicitari dalla strada o le dimensioni degli stessi debordanti il massimo consentito), si trovi esposto - fermo restando in entrambe le ipotesi l'obbligo di rimozione dei cartelli - ad una sanzione pecuniaria più onerosa rispetto a chi installi cartelli senza essere munito di autorizzazione;
 che nel giudizio innanzi alla Corte è intervenuto il Presidente del Consiglio dei ministri, per il tramite dell'Avvocatura generale dello Stato, che ha concluso per la manifesta inammissibilità o infondatezza della questione, deducendo, sotto il primo profilo, la omessa individuazione, da parte del giudice a quo, del tertium comparationis, oltre che il carattere manipolativo della pronuncia richiesta alla Corte, e sottolineando, nel merito, il maggior rilievo che, ai fini della valutazione della gravità degli illeciti contemplati dall'art. 23 del codice della strada, dovrebbe assumere, rispetto alla esistenza o meno dell'autorizzazione, il pericolo arrecato alla sicurezza della circolazione stradale. 
 Considerato che l'ordinanza di rimessione è priva della indicazione espressa della disposizione assunta quale tertium comparationis, poiché il giudice a quo denuncia la violazione dell'art. 3 Cost. per la irragionevolezza della previsione di una sanzione più severa a carico di chi, in possesso di autorizzazione alla installazione di cartelli pubblicitari, non ne rispetti talune prescrizioni rispetto a chi non sia affatto munito di tale autorizzazione, ma non individua la norma rispetto alla quale la previsione di cui alla disposizione impugnata comminerebbe tale più severa sanzione pecuniaria; 
 che, anche considerando la identificabilità, pur in assenza di espressa indicazione da parte del rimettente, della norma che disciplina la fattispecie dallo stesso presa in considerazione quale tertium comparationis - da individuare presumibilmente nell'art. 23, comma 11, del codice della strada - restano pur sempre le ulteriori lacune della ordinanza, che ha omesso una valutazione completa del quadro normativo di effettivo riferimento, dalla quale sarebbero emerse altre fattispecie sanzionatorie, come quella di cui all'art. 23, commi 7 e 13-bis, dello stesso codice della strada, che punisce con sanzione pecuniaria più elevata la abusiva collocazione di insegne pubblicitarie lungo gli itinerari internazionali, le autostrade e le strade extraurbane principali; 
 che nemmeno risulta in alcun modo considerata dal giudice a quo la possibilità del concorso tra gli illeciti amministrativi di cui agli artt. 23, commi 1 e 11, e 25, commi 1 e 5, del nuovo codice della strada; 
 che, pertanto, la questione deve essere dichiarata manifestamente inammissibile.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, commi 1 e 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 23, comma 12, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata, in riferimento all'art. 3 della Costituzione, dal Giudice di pace di Verona con l'ordinanza indicata in epigrafe. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 15 gennaio 2014.&#13;
 F.to:&#13;
 Gaetano SILVESTRI, Presidente&#13;
 Mario Rosario MORELLI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 23 gennaio 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
