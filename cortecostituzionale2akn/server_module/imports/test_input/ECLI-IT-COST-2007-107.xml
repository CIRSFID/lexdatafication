<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/107/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/107/"/>
          <FRBRalias value="ECLI:IT:COST:2007:107" name="ECLI"/>
          <FRBRdate date="19/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="107"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/107/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/107/ita@/!main"/>
          <FRBRdate date="19/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/107/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/107/ita@.xml"/>
          <FRBRdate date="19/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/03/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maddalena</cc:relatore_pronuncia>
        <cc:data_decisione>19/03/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 16 (rectius: dell'art. 16, comma 8) della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2003), promosso con ordinanza del 20 marzo 2006 dalla Commissione tributaria provinciale di Modena sul ricorso promosso da Valeria Zanni contro l'Agenzia delle entrate, Ufficio di Modena, iscritta al n. 418 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 43, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 21 febbraio 2007 il Giudice relatore Paolo Maddalena. 
    Ritenuto  che con ordinanza emessa il 20 marzo 2006 la Commissione tributaria provinciale di Modena ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 16 (rectius: dell'art. 16, comma 8) della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2003); 
    che dinanzi alla Commissione tributaria provinciale il contribuente ha impugnato il provvedimento di diniego della domanda di definizione della lite fiscale pendente, in grado d'appello, dinanzi alla Commissione tributaria regionale; 
    che il giudice a quo dubita della legittimità costituzionale della norma censurata – che imporrebbe una declaratoria di incompetenza – perché, prevedendo che il provvedimento di diniego è impugnabile dinanzi all'organo giurisdizionale presso cui pende la lite, essa irragionevolmente darebbe luogo ad una disparità di trattamento in relazione al grado di giudizio, in contrasto con l'art. 3 della Costituzione; 
    che, difatti, nel caso in cui la lite di cui si chiede la definizione sia pendente in primo grado, la controversia sul diniego può essere conosciuta nel doppio grado di merito, al contrario di quanto avviene allorché la lite di cui si è chiesta la definizione penda in secondo grado; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, chiedendo che la questione sia dichiarata manifestamente infondata; 
    che, secondo l'Avvocatura, la concentrazione, disposta dalla norma denunciata, della cognizione dell'impugnazione del diniego presso lo stesso giudice investito della lite pendente, di cui il contribuente abbia chiesto la definizione, corrisponderebbe ad evidenti esigenze di razionalità e di economia processuale; 
    che, ad avviso della difesa erariale, la risoluzione della controversia circa la definibilità della lite comporta l'individuazione della natura e dell'oggetto della lite stessa, con la conseguente e pienamente ragionevole opportunità, anche al fine di evitare pronunce contraddittorie, che tale indagine sia riservata al giudice chiamato a conoscere il merito della controversia, in relazione alla fase in cui esso pende: il che sarebbe in coerenza con l'esigenza di assicurare la ragionevole durata del processo, ai sensi dell'art. 111 della Costituzione, giacché la generalizzata attribuzione della competenza a conoscere dell'impugnazione del diniego al primo giudice si tradurrebbe in un irragionevole ritardo nella definizione della lite di merito già pendente avanti al giudice di grado superiore. 
    Considerato che la questione di legittimità costituzionale, sollevata in riferimento all'art. 3 della Costituzione, investe l'art. 16, comma 8, della legge 27 dicembre 2002, n. 289, là dove prevede che il provvedimento di diniego della definizione della lite fiscale pendente è impugnabile dinanzi all'organo giurisdizionale presso cui pende la lite; 
    che la norma censurata si inserisce in un sistema, disciplinato dallo stesso comma 8 dell'art. 16, che pone una stretta connessione tra domanda di definizione e lite fiscale pendente, prevedendo: la sospensione del processo, per effetto della presentazione della domanda di definizione; l'estinzione del giudizio, a seguito della comunicazione, da parte dell'ufficio competente, della regolarità della domanda di definizione e del pagamento integrale di quanto dovuto; la prosecuzione della lite fiscale in conseguenza del provvedimento di diniego della definizione; 
    che, in quest'ambito, l'attribuzione alla cognizione del giudice investito della lite fiscale pendente della competenza sull'impugnazione del diniego di definizione rientra tra le scelte, non arbitrarie o non manifestamente irragionevoli, del legislatore, attesi gli effetti che sulla sorte del giudizio principale è in grado di dispiegare la soluzione dell'impugnativa del diniego di definizione; 
    che, pertanto, non sussiste il denunciato vizio di legittimità costituzionale, tanto più che, per costante giurisprudenza di questa Corte, la garanzia del doppio grado della giurisdizione di merito non ha copertura costituzionale generalizzata (sentenza n. 288 del 1997; ordinanze n. 84 del 2003 e n. 585 del 2000); 
    che la questione deve essere dichiarata manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 16, comma 8, della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2003), sollevata, in riferimento all'art. 3 della Costituzione, dalla Commissione tributaria provinciale di Modena con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 marzo 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Paolo MADDALENA, Redattore  &#13;
Gabriella MELATTI, Cancelliere  &#13;
Depositata in Cancelleria il 23 marzo 2007.  &#13;
Il Cancelliere  &#13;
F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
