<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/194/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/194/"/>
          <FRBRalias value="ECLI:IT:COST:2008:194" name="ECLI"/>
          <FRBRdate date="21/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="194"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/194/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/194/ita@/!main"/>
          <FRBRdate date="21/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/194/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/194/ita@.xml"/>
          <FRBRdate date="21/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/06/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>21/05/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 5, comma 3, del d.lgs. 26 maggio 1997, n. 153 (Integrazione dell'attuazione della direttiva 91/308/CEE in materia di riciclaggio dei capitali di provenienza illecita), promossi con ordinanze del 30 marzo 2007 dal Tribunale di Cagliari, del 17 maggio 2007 dal Tribunale di Novara e dal 10 luglio 2007 dal Tribunale di Mondovì, rispettivamente iscritte ai nn. 600, 718 e 785 del registro ordinanze 2007 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 35, 41 e 48, prima serie speciale, dell'anno 2007. 
    Visti gli atti di costituzione di F.G. e di B.G. nonché gli atti di intervento del Presidente del Consiglio dei ministri; 
    udito nell'udienza pubblica del 6 maggio 2008 il Giudice relatore Giovanni Maria Flick; 
    uditi gli avvocati Oreste Dominioni e Stefano Guadalupi per F. G., Stefano Guadalupi per B. G. e l'avvocato dello Stato Giuseppe Fiengo per il Presidente del Consiglio dei ministri. 
    Ritenuto che, con le tre ordinanze indicate in epigrafe, di analogo tenore, il Tribunale di Cagliari, il Tribunale di Novara e il Tribunale di Mondovì hanno sollevato, in riferimento agli artt. 76 e 77 della Costituzione, questione di legittimità costituzionale dell'art. 5, comma 3, del d.lgs. 26 maggio 1997, n. 153 (Integrazione dell'attuazione della direttiva 91/308/CEE in materia di riciclaggio dei capitali di provenienza illecita), nelle parti in cui configura come delitto la fattispecie criminosa ivi descritta e commina pene superiori ai «limiti edittali» indicati nella legge delega 6 febbraio 1996, n. 52 (Disposizioni per l'adempimento di obblighi derivanti dall'appartenenza dell'Italia alle Comunità europee – legge comunitaria 1994); 
    che i giudici a quibus – investiti di processi nei confronti di persone imputate del delitto previsto dalla norma denunciata, per avere eseguito operazioni di incasso e trasferimento di fondi, per conto di intermediari finanziari, senza la prescritta iscrizione nell'elenco degli agenti in attività finanziaria; ovvero imputate di concorso nel medesimo delitto – riferiscono che i difensori degli imputati avevano eccepito l'illegittimità costituzionale di detta norma, per contrasto con gli artt. 25, 76 e 77 Cost.; 
    che, al riguardo, i rimettenti muovono da una ricostruzione preliminare del quadro normativo, rilevando, in specie, come la disposizione denunciata sia stata emanata sulla base della delega legislativa conferita al Governo dalla legge n. 52 del 1996 ai fini dell'integrazione dell'attuazione della direttiva n. 91/308/CEE, relativa alla prevenzione dell'uso del sistema finanziario a scopo di riciclaggio dei proventi di attività illecite; 
    che – in correlazione alle statuizioni dell'art. 12 della direttiva – l'art. 15, comma 1, lettera c), della legge delega prevedeva, tra i principi e criteri direttivi, quello di «estendere […], in tutto od in parte, l'applicazione delle disposizioni di cui al decreto-legge 3 maggio 1991, n. 143» (Provvedimenti urgenti per limitare l'uso del contante e dei titoli al portatore nelle transazioni e prevenire l'uso del sistema finanziario a scopo di riciclaggio), convertito, con modificazioni, nella legge 5 luglio 1991, n. 197, «a quelle attività particolarmente suscettibili di utilizzazione a fini di riciclaggio per il fatto di realizzare l'accumulazione o il trasferimento di ingenti disponibilità economiche o finanziarie o risultare comunque esposte ad infiltrazioni da parte della criminalità organizzata»;  
    che la medesima norma di delega demandava, altresì, «la formazione o l'integrazione dell'elenco di tali attività o categorie di imprese» a uno o più decreti legislativi, da emanare entro due anni dalla data di entrata in vigore del decreto attuativo della delega stessa; 
    che, dando attuazione alla delega, l'art. 5 del d.lgs. n. 153 del 1997 ha esteso le previsioni del d.l. n. 143 del 1991 ai soggetti che svolgono le attività individuate nei decreti legislativi emanati ai sensi del citato art. 15, comma 1, lettera c), della legge n. 52 del 1996 (comma 1); ed ha stabilito che, ai fini di tali attività, «è istituito un elenco di operatori, suddiviso per categorie, tenuto dal Ministro del tesoro, che si avvale dell'Ufficio italiano dei cambi» (comma 2); 
    che il comma 3 dello stesso art. 5 – recante la norma incriminatrice impugnata – punisce, quindi, con la reclusione da sei mesi a quattro anni e con la multa da euro 2.065 a euro 10.329 chiunque esercita le attività in questione senza essere iscritto nell'elenco; 
    che l'individuazione delle attività rilevanti è stata operata dall'art. 1 del d.lgs. 25 settembre 1999, n. 374 (Estensione delle disposizioni in materia di riciclaggio dei capitali di provenienza illecita ed attività finanziarie particolarmente suscettibili di utilizzazione a fini di riciclaggio, a norma dell'articolo 15 della legge 6 febbraio 1996, n. 52), il quale ha stabilito che le disposizioni del d.l. n. 143 del 1991 si applichino, tra le altre, all'«agenzia in attività finanziaria prevista dall'articolo 106 del decreto legislativo 1° settembre 1993, n. 385, recante il testo unico delle leggi in materia bancaria e creditizia»; attività che a sua volta abbraccia – in virtù dell'art. 4, comma 1, lettera a), del d.m. 6 luglio 1994 (che specifica, ai sensi del comma 4 del citato art. 106, il contenuto delle attività indicate nel comma 1 dello stesso articolo) – anche quella di intermediazione finanziaria esercitata mediante «incasso e trasferimento di fondi»: ossia l'attività oggetto dell'imputazione; 
    che – ravvisata, di conseguenza, la rilevanza della questione – i giudici a quibus reputano manifestamente infondate le deduzioni delle difese relative tanto al supposto vizio di eccesso di delega, in cui il legislatore sarebbe incorso nella individuazione della condotta incriminata, stante l'assenza di un'omologa fattispecie di abusivismo nella normativa di riferimento (quella del d.l. n. 143 del 1991); quanto alla pretesa violazione del principio della riserva in legge in materia penale, avuto riguardo al fatto che – secondo i difensori – la scelta di configurare come reato l'esercizio abusivo dell'attività di incasso e trasferimento di fondi sarebbe stata operata da una fonte regolamentare (il d.m. 6 luglio 1994); 
    che, al contrario, i rimettenti condividono l'ulteriore dubbio di costituzionalità prospettato dalle difese, sul piano dell'eccesso di delega, relativamente alla scelta del legislatore delegato di configurare la fattispecie criminosa de qua come delitto e all'entità della pena per essa comminata; 
    che ad avviso dei rimettenti, difatti, il legislatore delegato doveva ritenersi abilitato ad introdurre unicamente fattispecie criminose di tipo contravvenzionale, sanzionate con pene non eccedenti i limiti edittali che connotano le ipotesi di reato previste dal d.l. n. 143 del 1991; 
    che a tale conclusione indurrebbe non soltanto il riferimento di ordine generale, contenuto nella norma di delega, all'applicazione, in tutto o in parte, del decreto-legge ora citato; ma anche la specifica previsione del comma 2 dell'art. 15 della legge n. 52 del 1996, in forza della quale, «in sede di riordinamento normativo, ai sensi dell'art. 8, delle materie concernenti il trasferimento di denaro contante e di titoli al portatore, nonché il riciclaggio dei capitali di provenienza illecita, potrà procedersi al riordino delle sanzioni amministrative e penali previste nelle leggi richiamate al comma 1, nei limiti massimi ivi contemplati»: leggi tra le quali figura proprio il d.l. n. 143 del 1991; 
    che alla data di entrata in vigore della legge delega – osservano ancora i rimettenti – il decreto-legge in questione contemplava, tra gli illeciti penali (art. 5, commi 4 e 6), unicamente fattispecie di tipo contravvenzionale punite con pene inferiori a quella comminata dalla norma impugnata (in specie: arresto da sei mesi ad un anno, congiunto ovvero alternativo all'ammenda); 
    che la figura delittuosa originariamente prevista dall'art. 6, comma 9, del d.l. n. 143 del 1991 – che puniva l'esercizio delle attività di cui al comma 1 dello stesso articolo da parte di soggetti non iscritti nell'apposito elenco, o per i quali non sussistessero le condizioni di iscrizione, colpendola con pena uguale a quella stabilita dalla norma contestata – era stata, infatti, abrogata dall'art. 161 del d.lgs. n. 385 del 1993: onde dovrebbe escludersi che la norma di delega intendesse riferirsi anche a tale previsione punitiva; 
    che, secondo i Tribunali di Cagliari e di Novara, inoltre, il criterio direttivo risulterebbe egualmente violato, almeno in rapporto al livello della pena, ove pure si ritenesse che il legislatore delegante – nel richiamare, all'art. 15, comma 2, della legge n. 52 del 1996, i limiti massimi delle sanzioni contemplate dalle leggi indicate nel comma 1 – avesse inteso evocare, oltre al d.l. n. 143 del 1991, anche il decreto-legge 28 giugno 1990, n. 167 (Rilevazione a fini fiscali di taluni trasferimenti da e per l'estero di denaro, titoli e valori), convertito, con modificazioni, nella legge 4 agosto 1990, n. 227, richiamato nel comma 1 dell'art. 15 della legge delega; l'art. 5 del citato d.l. n. 167 del 1990 prevede, difatti, una figura delittuosa di false indicazioni agli intermediari, ma punita con pena comunque inferiore a quella comminata dalla norma denunciata (reclusione da sei mesi ad un anno e multa da lire un milione a lire dieci milioni); 
    che nei giudizi di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente infondata, per essere i giudici a quibus incorsi in errore nell'individuazione della «norma interposta», la quale andrebbe identificata, non nell'art. 15 della legge n. 52 del 1996, ma nell'art. 3, comma 1, lettera c), della legge stessa, alla cui stregua la disposizione denunciata dovrebbe ritenersi pienamente rispettosa della delega; 
    che si sono altresì costituiti G. F. (nel giudizio relativo all'ordinanza r.o. n. 600 del 2007 e nel giudizio relativo all'ordinanza r.o. n. 718 del 2007) e G. B. (nel giudizio relativo all'ordinanza r.o. n. 785 del 2007), imputati nei processi a quibus, i quali hanno chiesto, sulla base di identiche considerazioni, l'accoglimento della questione; 
    che la parte privata G. F. ha depositato, altresì, memoria, volta segnatamente a contrastare le deduzioni dell'Avvocatura generale dello Stato. 
    Considerato che le ordinanze di rimessione sollevano questioni identiche, onde i relativi giudizi vanno riuniti per essere definiti con unica decisione; 
    che i giudici a quibus dubitano della legittimità costituzionale, in riferimento agli artt. 76 e 77 della Costituzione, dell'art. 5, comma 3, del d.lgs. 26 maggio 1997, n. 153 (Integrazione dell'attuazione della direttiva 91/308/CEE in materia di riciclaggio dei capitali di provenienza illecita), nelle parti in cui configura come delitto la fattispecie criminosa ivi descritta e commina pene superiori ai «limiti edittali» indicati nella legge delega 6 febbraio 1996, n. 52; 
    che, ad avviso dei rimettenti, il legislatore delegato doveva ritenersi abilitato ad introdurre unicamente fattispecie criminose di tipo contravvenzionale, punite con pene non superiori a quelle previste per i reati contemplati dal d.l. 3 maggio 1991, n. 143, convertito, con modificazioni, nella legge 5 luglio 1991, n. 197; ovvero, in subordine – secondo i Tribunali di Cagliari e di Novara – anche dal d.l. 28 giugno 1990, n. 167, convertito, con modificazioni, nella legge 4 agosto 1990, n. 227: nel qual caso il criterio di delega risulterebbe violato almeno per quanto concerne la misura della pena; 
    che i rimettenti valutano, tuttavia, la sussistenza del dedotto vizio di eccesso di delega esclusivamente alla luce dei criteri di delega specifici dettati dall'art. 15 della legge n. 52 del 1996 ai fini dell'integrazione dell'attuazione della direttiva 91/308/CEE, senza tenere conto dei criteri generali stabiliti dalla medesima legge in tema di disciplina delle sanzioni: criteri – questi ultimi – la cui applicabilità non è affatto esclusa dai primi; 
    che, secondo un approccio tipico delle «leggi comunitarie», la legge n. 52 del 1996 ha delegato, difatti, il Governo ad emanare i decreti legislativi recanti le norme necessarie per dare attuazione ad un complesso di direttive comunitarie, indicate nell'allegato A alla medesima legge (art. 1): direttive fra le quali è compresa la direttiva 91/308/CEE, in tema di prevenzione dell'uso del sistema finanziario a scopo di riciclaggio dei proventi di attività illecite; 
    che la medesima legge n. 52 del 1996 reca, altresì, all'art. 3, un insieme di criteri e principi direttivi «generali»: valevoli, cioè, per tutti i decreti legislativi da emanare, salvi i principi specifici dettati dai successivi articoli in relazione alle singole materie e in aggiunta a quelli contenuti nelle direttive da attuare; 
    che, con particolare riguardo all'assetto sanzionatorio, la lettera c) del citato art. 3 – ripetendo una formula ricorrente nelle «leggi comunitarie» - stabiliva che il legislatore delegato potesse introdurre sanzioni amministrative e penali per le infrazioni alle disposizioni dei decreti legislativi, ove necessario al fine di assicurarne l'osservanza: entro il limite – quanto alle sanzioni penali – dell'ammenda fino a lire duecento milioni e dell'arresto fino a tre anni, e sempre che le infrazioni ledessero o esponessero a pericolo «interessi generali dell'ordinamento interno del tipo di quelli tutelati dagli artt. 34 e 35 della legge 24 novembre 1981, n. 689»; 
    che la medesima disposizione soggiungeva, tuttavia, che, «in ogni caso, in deroga ai limiti sopra indicati, per le infrazioni alle disposizioni dei decreti legislativi saranno previste sanzioni penali o amministrative identiche a quelle eventualmente già comminate dalle leggi vigenti per le violazioni che siano omogenee e di pari offensività rispetto alle infrazioni medesime»; 
    che – come emerge chiaramente dalla relazione integrativa allo schema del d.lgs. n. 153 del 1997 – è proprio sulla base del criterio generale di delega da ultimo indicato, e non già di quelli specifici di cui all'art. 15 della medesima legge, che il legislatore delegato ha inteso emanare la norma incriminatrice di cui si discute: e ciò sul rilievo che la fattispecie di abusivismo contemplata da tale norma risulterebbe omogenea e di pari offensività rispetto al delitto di abusiva attività finanziaria, previsto dall'art. 132 del d.lgs. 1° settembre 1993, n. 385, nonché al delitto di abusivo esercizio dell'attività di mediazione creditizia, previsto dall'art. 16, comma 7, della legge 7 marzo 1996, n. 108 (Disposizioni in materia di usura); delitti al cui trattamento sanzionatorio è stato quindi allineato quello della figura di reato di cui si discute; 
    che – conformemente a quanto eccepito dall'Avvocatura dello Stato – i ricorrenti hanno individuato, dunque, in modo errato la norma di delega alla cui stregua va apprezzata la sussistenza del dedotto vizio di eccesso di delega, svolgendo, di conseguenza, argomentazioni inconferenti ai fini di tale valutazione: il che rende le questioni sollevate manifestamente inammissibili (sentenza n. 382 del 2004; ordinanza n. 72 del 2003); 
    che, per altro verso – secondo la costante giurisprudenza di questa Corte – nel giudizio di legittimità costituzionale in via incidentale, non possono essere presi in considerazione, oltre i limiti del thema decidendum fissato dall'ordinanza di rimessione, ulteriori questioni o profili di costituzionalità dedotti dalle parti, tanto ove siano stati già eccepiti, ma non fatti propri dal giudice a quo; quanto ove risultino comunque diretti ad ampliare o modificare successivamente il contenuto della predetta ordinanza (ex plurimis, sentenze n. 134 del 2003, n. 49 e n. 330 del 1999; ordinanze n. 44 e n. 219 del 2001);  
    che non può essere presa dunque in esame, nella specie, la censura prospettata da tutte le parti private costituite, secondo la quale il legislatore avrebbe ecceduto dalla delega anche nell'individuare la fattispecie sanzionata penalmente: trattandosi di deduzione rispetto alla quale opera, in questa sede, la preclusione derivante dalla valutazione di manifesta infondatezza formulata dai giudici a quibus;  
    che altrettanto deve dirsi anche per l'ulteriore censura, svolta dalla parte privata G. F., stando alla quale la norma impugnata non sarebbe rispettosa neppure del criterio direttivo di cui all'art. 3, comma 1, lettera c), della legge n. 52 del 1996, in quanto l'infrazione punita dall'art. 5, comma 3, del d.lgs. n. 153 del 1997 non potrebbe ritenersi omogenea e di pari offensività rispetto a violazioni previste da «leggi vigenti», e segnatamente rispetto a quella contemplata dall'art. 132 del d.lgs. n. 385 del 1993: trattandosi di censura del tutto distinta rispetto a quelle svolte nelle ordinanze di rimessione. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 5, comma 3, del d.lgs. 26 maggio 1997, n. 153 (Integrazione dell'attuazione della direttiva 91/308/CEE in materia di riciclaggio dei capitali di provenienza illecita), sollevate, in riferimento agli artt. 76 e 77 della Costituzione, dal Tribunale di Cagliari, dal Tribunale di Novara e dal Tribunale di Mondovì con le ordinanze indicate in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 21 maggio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 6 giugno 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
