<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/140/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/140/"/>
          <FRBRalias value="ECLI:IT:COST:2003:140" name="ECLI"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="140"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/140/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/140/ita@/!main"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/140/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/140/ita@.xml"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/04/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Romano Vaccarella</cc:relatore_pronuncia>
        <cc:data_decisione>09/04/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 3, rectius: commi 181, 184 e 186, della legge 28 dicembre 1995, n. 549 (Misure di razionalizzazione della finanza pubblica), promosso con ordinanza del 14 febbraio 2002 (n. due ordinanze) dalla Commissione tributaria provinciale di Milano e dell'art. 3, commi 184 e rectius: 186, della stessa legge 28 dicembre 1995, n. 549, promosso con ordinanza del 17 gennaio 2002 dalla Commissione tributaria provinciale di Napoli, rispettivamente iscritte ai nn. 442, 443 e 533 del registro ordinanze 2002 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 40 e 48, prima serie speciale, dell'anno 2002. 
      Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 26 febbraio 2003 il Giudice relatore Romano Vaccarella. 
 Ritenuto che, con due ordinanze, datate 11 dicembre 2001, sostanzialmente identiche, la Commissione tributaria provinciale di Milano solleva questione di legittimità costituzionale dell'art. 3 della legge 28 dicembre 1995 n. 549 (Misure di razionalizzazione della finanza pubblica) - rectius, come sembra emergere dalla motivazione, dei commi 181, 184 e 186 dell'art. 3 cit. - nel corso di due giudizi promossi avverso avvisi di accertamento, ai fini ILOR del 1995, di maggior reddito conseguente all'applicazione dei parametri previsti dal d.P.C.m. 29 gennaio 1996; 
      che il rimettente rileva come la rettifica delle dichiarazioni avviene in base ad accertamenti basati su criteri statistici, con conseguente collocamento del contribuente all'interno di taluno dei c.d. “gruppi massimamente omogenei” su base statistico-probabilistica; 
      che la norma denunciata appare contrastante: a) con l'art. 23 Cost. perché il potere di disciplinare la materia viene delegato ad una fonte normativa secondaria; b) con l'art. 24 Cost. per l'impossibilità di utilizzare la prova testimoniale (art. 7, comma quarto, del d.lgs. 31 dicembre 1992 n. 546, recante “Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413”) per vincere la presunzione rappresentata dai parametri e perché gli Uffici possono utilizzare le dichiarazioni raccolte in sede istruttoria; c) con l'art. 53 Cost., perché la tassazione non si baserebbe sulla effettiva capacità contributiva, ma su inattendibili “accertamenti” statistici; d) con l'art. 95 Cost., per non essere quella della determinazione dei parametri materia rientrante nelle funzioni del Presidente del Consiglio dei ministri; 
      che, costituitosi in giudizio a mezzo dell'Avvocatura generale dello Stato, il Presidente del Consiglio dei ministri deduce l'inammissibilità della questione per omesso esame della rilevanza e, in ogni caso, la manifesta infondatezza ricordando la giurisprudenza di questa Corte circa la natura della riserva di legge di cui all'art. 23 Cost., e deducendone l'inconsistenza delle censure svolte in riferimento agli artt. 24, 53 e 95 Cost.; 
      che, con ordinanza 17 gennaio 2002, la Commissione tributaria provinciale di Napoli, in un giudizio di impugnazione di un avviso di accertamento, ai fini ILOR 1995, di maggior reddito conseguente all'applicazione dei parametri di cui al d.P.C.m. 29 gennaio 1996, solleva questione di legittimità costituzionale dell'art. 3, comma 184 (rectius, come sembra emergere dalla motivazione, anche del comma 186) della citata legge n. 549 del 1995; 
      che, ad avviso del rimettente, la norma denunciata, attribuendo al Ministro (e non al Presidente del Consiglio) il potere di elaborare i parametri violerebbe la riserva di legge di cui all'art. 23 Cost.; 
      che, inoltre, sarebbe violato l'art. 24 Cost. in quanto il sistema statistico non solo non terrebbe conto dell'impresa marginale, ma creerebbe “una presunzione assoluta-aprioristica alla quale non è possibile controbattere non esistendo la prova negativa”, e sarebbe conseguentemente violato anche l'art. 53 Cost. (se non altro perché il sistema dei parametri sarebbe peggiorativo, in quanto più grossolano, dei previgenti “indici di redditività” definiti dagli Ispettorati compartimentali delle Imposte dirette); 
      che, costituitosi in giudizio a mezzo dell'Avvocatura generale dello Stato, il Presidente del Consiglio dei ministri deduce l'inammissibilità della questione per omesso esame della rilevanza e, in ogni caso, la manifesta infondatezza per le medesime ragioni sopra ricordate. 
      Considerato che i giudizi, per la loro evidente connessione, devono essere riuniti; 
      che le questioni sollevate con riferimento all'art. 23 Cost. (e, conseguentemente, agli artt. 53 e 95 Cost.) sono manifestamente infondate, non essendo dedotta ragione alcuna che possa indurre questa Corte a discostarsi da quanto statuito - in coerenza con la propria giurisprudenza sulla natura della riserva di legge di cui all'art. 23 Cost. - con riguardo all'art. 3 della legge n. 549 del 1995 (sentenza n. 105 del 2003); 
      che manifestamente infondata è, altresì, la questione sollevata dalla Commissione tributaria provinciale di Napoli, in riferimento all'art. 24 Cost., sotto il profilo che la norma impugnata creerebbe una “presunzione assoluta-aprioristica”, dal momento che «i “parametri” prevedono un sistema basato su presunzione semplice la cui idoneità probatoria è rimessa alla valutazione del giudice di merito» (sentenza n. 105 del 2003); 
      che la questione sollevata dalla Commissione tributaria provinciale di Milano, in riferimento all'art. 24 Cost., relativamente all'esclusione della prova testimoniale, è manifestamente inammissibile, non risultando in alcun modo motivata la rilevanza nei giudizi a quibus della questione concernente la prova testimoniale. 
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      riuniti i giudizi, &#13;
      dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 3, commi 181, 184 e 186, della legge 28 dicembre 1995 n. 549 (Misure di razionalizzazione della finanza pubblica) sollevata, in riferimento agli artt. 23, 53 e 95 Cost., dalla Commissione tributaria provinciale di Milano e dell'art. 3, commi 184 e 186, della stessa legge 28 dicembre 1995, n. 549, con riferimento agli artt. 23, 24 e 53 Cost., dalla Commissione tributaria provinciale di Napoli con le ordinanze in epigrafe; &#13;
      dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 3, commi 181, 184 e 186, della legge 28 dicembre 1995, n. 549 (Misure di razionalizzazione della finanza pubblica) sollevata, in riferimento all'art. 24 Cost., dalla Commissione tributaria provinciale di Milano con le ordinanze in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 aprile 2003. &#13;
F.to: &#13;
Riccardo CHIEPPA, Presidente &#13;
Romano VACCARELLA, Redattore &#13;
Maria Rosaria FRUSCELLA, Cancelliere &#13;
Depositata in Cancelleria il 24 aprile 2003. &#13;
Il Cancelliere &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
