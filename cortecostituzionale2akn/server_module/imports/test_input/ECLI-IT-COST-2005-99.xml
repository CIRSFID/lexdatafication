<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/99/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/99/"/>
          <FRBRalias value="ECLI:IT:COST:2005:99" name="ECLI"/>
          <FRBRdate date="24/02/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="99"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/99/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/99/ita@/!main"/>
          <FRBRdate date="24/02/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/99/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/99/ita@.xml"/>
          <FRBRdate date="24/02/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/03/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CONTRI</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>24/02/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Fernanda CONTRI; Giudici: Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 14, commi 5-ter e 5-quinquies, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), aggiunti dall'art. 13, comma 1, della legge 30 luglio 2002, n. 189 (Modifiche alla normativa in materia di immigrazione e di asilo), promossi con ordinanze del 18 novembre, del 30 ottobre e del 13 novembre 2003 dal Tribunale di Reggio Emilia, rispettivamente iscritte ai nn. 113, 194 e 195 del registro ordinanze 2004 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 11 e 13, prima serie speciale, dell'anno 2004. 
    Udito nella camera di consiglio del 26 gennaio 2005 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che con le tre ordinanze indicate in epigrafe il Tribunale di Reggio Emilia ha sollevato questioni di legittimità costituzionale: 
    a) dell'art. 14, comma 5-ter, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), aggiunto dall'art. 13, comma 1, della legge 30 luglio 2002, n. 189 (Modifiche alla normativa in materia di immigrazione e di asilo), in riferimento all'art. 25 della Costituzione, «nella parte in cui prevede l'assenza di un giustificato motivo quale elemento costitutivo della fattispecie incriminatrice», ivi contemplata, dello straniero che si trattiene nel territorio dello Stato in violazione dell'ordine impartito dal questore ai sensi del comma 5-bis dello stesso art. 14; 
    b) dell'art. 14, comma 5-quinquies, del d.lgs. n. 286 del 1998 – anch'esso aggiunto dall'art. 13, comma 1, della legge n. 189 del 2002 – in riferimento all'art. 13 della Costituzione, nella parte in cui prevede l'arresto obbligatorio per il reato di cui al precedente comma 5-ter; 
    che il giudice a quo – chiamato a pronunciarsi sulla convalida di arresto disposto per il reato di cui all'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998 – ritiene che la formula «senza giustificato motivo» difetti del necessario requisito della determinatezza, demandando all'interprete il compito di completare il contenuto della norma incriminatrice, in relazione tanto alla natura che alla gravità delle motivazioni idonee a giustificare la permanenza dello straniero nel territorio dello Stato; 
    che l'arresto obbligatorio per tale reato, d'altro canto – non potendo essere seguito dall'adozione di misure cautelari, stante la natura contravvenzionale della fattispecie criminosa – sarebbe posto unicamente a garanzia dell'esecuzione del provvedimento amministrativo di espulsione: con conseguente violazione dell'art. 13 Cost., che limita il potere della polizia giudiziaria di comprimere provvisoriamente la libertà personale a «casi eccezionali di necessità ed urgenza, indicati tassativamente dalla legge». 
    Considerato che, successivamente alle ordinanze di rimessione, questa Corte, con sentenza n. 223 del 2004, ha dichiarato costituzionalmente illegittimo l'art. 14, comma 5-quinquies, del decreto legislativo 25 luglio 1998, n. 286, nella parte in cui stabilisce che per il reato previsto dal comma 5-ter del medesimo art. 14 è obbligatorio l'arresto dell'autore del fatto; 
    che entrambe le disposizioni impugnate dal giudice rimettente – tanto, cioè, la norma sostanziale di cui al comma 5-ter che quella processuale di cui al comma 5-quinquies del citato art. 14 – sono state quindi modificate dal decreto-legge 14 settembre 2004, n. 241 (Disposizioni urgenti in materia di immigrazione), convertito, con modificazioni, in legge 12 novembre 2004, n. 271; 
    che, in particolare, il reato di ingiustificato trattenimento dello straniero nel territorio dello Stato, di cui al comma 5-ter dell'art. 14, è stato trasformato in delitto, punito con la reclusione da uno a quattro anni, nel caso di espulsione disposta per ingresso illegale nel territorio dello Stato ai sensi dell'art. 13, comma 2, lettere a) e c), del d.lgs. n. 286 del 1998, ovvero per non aver tempestivamente richiesto il permesso di soggiorno «in assenza di cause di forza maggiore», o per essere stato il permesso revocato o annullato; conservando l'originaria natura contravvenzionale nella sola ipotesi residuale di espulsione disposta perché il permesso di soggiorno è scaduto da più di sessanta giorni e non ne è stato chiesto il rinnovo; 
    che, a sua volta, il comma 5-quinquies dell'art. 14 è stato modificato nel senso della limitazione dell'arresto obbligatorio alle fattispecie di ingiustificato trattenimento previste dal primo periodo del comma 5-ter, ossia a quelle trasformate in delitto; 
    che gli atti debbono essere pertanto restituiti al giudice rimettente per un nuovo esame della rilevanza della questione.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti al Tribunale di Reggio Emilia. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 febbraio 2005. &#13;
F.to: &#13;
Fernanda CONTRI, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 10 marzo 2005. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
