<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/19/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/19/"/>
          <FRBRalias value="ECLI:IT:COST:2009:19" name="ECLI"/>
          <FRBRdate date="26/01/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="19"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/19/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/19/ita@/!main"/>
          <FRBRdate date="26/01/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/19/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/19/ita@.xml"/>
          <FRBRdate date="26/01/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="30/01/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>FLICK</cc:presidente>
        <cc:relatore_pronuncia>Maria Rita Saulle</cc:relatore_pronuncia>
        <cc:data_decisione>26/01/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'articolo 42, comma 5, del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e paternità, a norma dell'art. 15 della legge 8 marzo 2000, n. 53), promosso con ordinanza del 26 marzo 2008 dal Tribunale di Tivoli nel procedimento civile vertente tra C.F. e l'Istituto superiore «Zambeccari», iscritta al n. 244 del registro ordinanze 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 35, prima serie speciale, dell'anno 2008. 
    Visto l'atto di costituzione di C.F.; 
    udito nell'udienza pubblica del 2 dicembre 2008 il Giudice relatore Maria Rita Saulle; 
    udito l'avvocato Giampaolo Ruggiero per C.F.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. – Con ordinanza del 26 marzo 2008, il Tribunale di Tivoli, sezione lavoro, ha sollevato questione di legittimità costituzionale dell'art. 42, comma 5, del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e paternità, a norma dell'art. 15 della legge 8 marzo 2000, n. 53), per violazione degli artt. 2, 3 e 32 della Costituzione. 
    Ad avviso del Tribunale rimettente, la norma contrasterebbe con i citati parametri costituzionali «nella parte in cui esclude dal novero dei soggetti legittimati a fruire del congedo ivi previsto il figlio convivente, in assenza di altri soggetti idonei a prendersi cura della persona affetta» da disabilità grave. 
    1.1. – Nell'ordinanza di rimessione si precisa che il giudizio principale ha ad oggetto il ricorso proposto ai sensi dell'art. 700 del codice di procedura civile avverso il provvedimento con il quale un Istituto statale di istruzione superiore aveva respinto l'istanza avanzata da un proprio dipendente – inquadrato come collaboratore scolastico a tempo indeterminato – finalizzata ad ottenere il riconoscimento del diritto al congedo straordinario retribuito per poter assistere la madre in situazione di disabilità grave, certificata ai sensi dell'art. 3, comma 3, della legge 5 febbraio 1992, n. 104 (Legge-quadro per l'assistenza, l'integrazione sociale e i diritti delle persone handicappate), in quanto unico soggetto convivente. 
    Il rigetto dell'istanza da parte dell'Amministrazione, afferma il rimettente, è stato motivato in ragione della mancata menzione espressa, nella disposizione censurata, del figlio del genitore disabile tra i soggetti legittimati alla fruizione del congedo straordinario retribuito. 
    2. – In punto di non manifesta infondatezza, il Tribunale rimettente osserva che questa Corte, con le sentenze n. 233 del 2005 e n. 158 del 2007, ha esteso il beneficio in esame; con la prima pronuncia, ai fratelli o alle sorelle conviventi nell'ipotesi in cui i genitori siano impossibilitati a provvedere all'assistenza del figlio in situazione di disabilità grave perché totalmente inabili; con la seconda pronuncia, al coniuge convivente del disabile. 
    In particolare, ad avviso del giudice a quo, rileverebbe nel caso di specie l'affermazione di questa Corte secondo la quale la «ratio legis della disposizione normativa in esame consiste nel favorire l'assistenza al soggetto con handicap grave mediante la previsione del diritto ad un congedo straordinario - rimunerato in misura corrispondente all'ultima retribuzione e coperto da contribuzione figurativa - che, all'evidente fine di assicurare continuità nelle cure e nell'assistenza ed evitare vuoti pregiudizievoli alla salute psicofisica del soggetto diversamente abile, è riconosciuto non solo in capo alla lavoratrice madre o in alternativa al lavoratore padre ma anche, dopo la loro scomparsa, a favore di uno dei fratelli o delle sorelle conviventi» (sentenza n. 233 del 2005). Il rimettente sottolinea, altresì, che, sempre secondo questa Corte, «l'interesse primario cui è preposta la norma in questione – ancorché sistematicamente collocata nell'ambito di un corpo normativo in materia di tutela e sostegno della maternità e paternità – è quello di assicurare in via prioritaria la continuità nelle cure e nell'assistenza del disabile che si realizzino in ambito familiare, indipendentemente dall'età e dalla condizione di figlio dell'assistito»  (sentenza n. 158 del 2007). 
    3. – Alla luce di tali premesse, secondo il Tribunale di Tivoli, l'esclusione del figlio del disabile dal novero dei soggetti legittimati a fruire del congedo retribuito previsto dall'art. 42, comma 5, del d.lgs. n. 151 del 2001, in mancanza di altre persone idonee ad occuparsi dello stesso, contrasterebbe in primo luogo con l'art. 3 della Costituzione, posto che lo «status di figlio è fonte dell'obbligo alimentare previsto dall'art. 433 del codice civile, nell'ambito del quale il figlio medesimo è collocato in via prioritaria rispetto allo stesso genitore dell'avente diritto»; di conseguenza, il mancato riconoscimento del relativo diritto nei confronti del figlio convivente, rispetto a quanto previsto per i genitori, il coniuge ed i fratelli conviventi, determinerebbe un'ingiustificata disparità di trattamento del figlio rispetto agli altri congiunti del disabile.  
    In secondo luogo, sempre ad avviso del giudice a quo, detta esclusione violerebbe anche l'art. 2 Cost., «che richiede il rispetto dei doveri inderogabili di solidarietà e la conseguente predisposizione di misure che consentano l'esercizio dei medesimi», nonché l'art. 32 Cost., poiché il diritto alla salute non verrebbe sufficientemente tutelato a causa della mancata garanzia ad un «soggetto lavoratore, avente lo status di unico convivente con persona affetta da stabile disabilità», della «predisposizione di idonee misure finalizzate alla prestazione della necessaria assistenza». 
    4. – In punto di rilevanza, infine, il Tribunale di Tivoli osserva che «la pretesa azionata dal ricorrente non può che essere esaminata in riferimento» alla disposizione censurata, risultando altresì dagli atti di causa che «l'istante è l'unico soggetto convivente con la madre […] riconosciuta affetta da handicap grave, ai sensi dell'art. 3, comma 3, legge n. 104 del 1992, dalla competente commissione della AUSL locale» e che il rigetto da parte della autorità scolastica dell'istanza di concessione del congedo straordinario avanzata dal ricorrente è motivata unicamente dalla mancata inclusione, nel novero dei soggetti legittimati, del figlio del disabile. 
    5. – Con memoria depositata in data 17 luglio 2008, si è costituito in giudizio il ricorrente nel giudizio a quo, chiedendo che la questione di legittimità costituzionale sia accolta. 
    La parte privata, dopo aver ribadito la ricostruzione dei fatti e le argomentazioni svolte dal giudice rimettente, deduce in particolare che la disparità di trattamento determinata dall'esclusione del figlio di un disabile dai soggetti legittimati a poter usufruire del congedo straordinario retribuito riserverebbe «irragionevolmente una minor tutela sia al nucleo familiare del disabile […], rispetto a quella riservata alla sua famiglia di origine, sia al diritto alla salute dello stesso, la cui realizzazione è assicurata anche attraverso il sostegno economico della famiglia che lo assiste».Considerato in diritto1. – Il Tribunale di Tivoli, in funzione di giudice del lavoro, dubita della legittimità costituzionale dell'art. 42, comma 5, del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e paternità, a norma dell'art. 15 della legge 8 marzo 2000, n. 53), «nella parte in cui esclude dal novero dei soggetti legittimati a fruire del congedo ivi previsto il figlio convivente, in assenza di altri soggetti idonei a prendersi cura della persona affetta» da disabilità grave, per contrasto con gli artt. 2, 3 e 32 della Costituzione. 
    Ad avviso del giudice rimettente, infatti, la norma censurata, riconoscendo il diritto al congedo straordinario retribuito esclusivamente ai genitori della persona in situazione di disabilità grave, o, in alternativa, in caso di loro scomparsa o impossibilità (dopo la sentenza n. 233 del 2005 di questa Corte), ai fratelli e sorelle con essa conviventi, nonché (dopo la successiva sentenza n. 158 del 2007) al coniuge convivente del disabile, si porrebbe in contrasto con l'art. 3, primo comma, Cost., determinando un ingiustificato trattamento deteriore di un soggetto, il figlio convivente, tenuto ai medesimi obblighi di assistenza morale e materiale nei confronti del disabile. 
    La norma in questione, al contempo, contrasterebbe con l'art. 2 Cost., il quale, imponendo il rispetto dei doveri inderogabili di solidarietà, richiederebbe la predisposizione di misure idonee a consentirne l'adempimento, nonché con l'art. 32 Cost., in quanto la garanzia del diritto alla salute, ivi prevista, risulterebbe vanificata dalla mancata previsione del diritto al congedo straordinario a favore dell'unico soggetto convivente con la persona affetta da stabile disabilità e bisognosa della necessaria assistenza. 
    2. – La questione è fondata. 
    2.1. – Questa Corte ha operato un primo vaglio della norma censurata relativa all'istituto del congedo straordinario, dichiarando l'illegittimità costituzionale dell'art. 42, comma 5, del d.lgs. n. 151 del 2001, nella parte in cui non prevedeva il diritto di uno dei fratelli o delle sorelle conviventi con un disabile grave a fruire del congedo ivi indicato, nell'ipotesi in cui i genitori fossero impossibilitati a provvedere all'assistenza del figlio handicappato perché totalmente inabili (sentenza n. 233 del 2005). 
    In quell'occasione la Corte ha sottolineato che il congedo straordinario retribuito si iscrive negli interventi economici integrativi di sostegno alle famiglie che si fanno carico dell'assistenza della persona diversamente abile, evidenziando altresì il rapporto di stretta e diretta correlazione di detto istituto con le finalità perseguite dalla legge n. 104 del 1992, ed in particolare con quelle di tutela della salute psico-fisica della persona handicappata e di promozione della sua integrazione nella famiglia. 
    2.2. – Questa Corte ha poi dichiarato l'illegittimità costituzionale della medesima disposizione, nella parte in cui non includeva nel novero dei soggetti beneficiari, ed in via prioritaria rispetto agli altri congiunti indicati dalla norma, il coniuge convivente della persona in situazione di disabilità grave (sentenza n. 158 del 2007). 
    Con tale pronuncia si è posta in evidenza la ratio dell'istituto del congedo straordinario retribuito, alla luce dei suoi presupposti e delle vicende normative che lo hanno caratterizzato, rilevandosi che «sin dal momento della sua introduzione, […] l'istituto in questione mirava a tutelare una situazione di assistenza della persona con handicap grave già in atto, pur limitando l'ambito di operatività del beneficio ai componenti (genitori e, in caso di loro scomparsa, fratelli) della sola famiglia di origine del disabile». Conseguentemente, si è affermato che «l'interesse primario cui è preposta la norma in questione – ancorché sistematicamente collocata nell'ambito di un corpo normativo in materia di tutela e sostegno della maternità e paternità – è quello di assicurare in via prioritaria la continuità nelle cure e nell'assistenza del disabile che si realizzino in ambito familiare, indipendentemente dall'età e dalla condizione di figlio dell'assistito». 
    Sulla base di tali premesse, questa Corte ha ritenuto che il trattamento riservato dalla norma censurata al lavoratore coniugato con un disabile, che versi in situazione di gravità e con questo convivente, ometteva di considerare le situazioni di compromissione delle capacità fisiche, psichiche e sensoriali, tali da «rendere necessario un intervento assistenziale permanente, continuativo e globale nella sfera individuale o in quella di relazione» – secondo quanto previsto dall'art. 3 della legge n. 104 del 1992 – che si fossero realizzate in dipendenza di eventi successivi alla nascita ovvero in esito a malattie di natura progressiva. In tal modo la stessa norma avrebbe comportato un inammissibile impedimento all'effettività dell'assistenza ed integrazione del disabile stesso nell'ambito di un nucleo familiare in cui ricorrono le medesime esigenze che l'istituto in questione è deputato a soddisfare, in violazione degli artt. 2, 3, 29 e 32 Cost. 
    2.3. – I principi appena richiamati sono applicabili anche all'ipotesi oggetto del presente giudizio. 
    La disposizione censurata, omettendo di prevedere tra i beneficiari del congedo straordinario retribuito il figlio convivente, anche qualora questi sia l'unico soggetto in grado di provvedere all'assistenza della persona affetta da handicap grave, viola gli artt. 2, 3 e 32 Cost., ponendosi in contrasto con la ratio dell'istituto. Questa, infatti, come sopra evidenziato, consiste essenzialmente nel favorire l'assistenza al disabile grave in ambito familiare e nell'assicurare continuità nelle cure e nell'assistenza, al fine di evitare lacune nella tutela della salute psico-fisica dello stesso, e ciò a prescindere dall'età e dalla condizione di figlio di quest'ultimo. 
    Inoltre, la suddetta omissione determina un trattamento deteriore dell'unico figlio convivente del disabile – allorché sia anche il solo soggetto in grado di assisterlo – rispetto agli altri componenti del nucleo familiare di quest'ultimo espressamente contemplati dalla disposizione oggetto di censura; trattamento deteriore che, diversificando situazioni omogenee, quanto agli obblighi inderogabili di solidarietà derivanti dal legame familiare, risulta privo di ogni ragionevole giustificazione.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara l'illegittimità costituzionale dell'art. 42, comma 5, del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e paternità, a norma dell'art. 15 della legge 8 marzo 2000, n. 53), nella parte in cui non include nel novero dei soggetti legittimati a fruire del congedo ivi previsto il figlio convivente, in assenza di altri soggetti idonei a prendersi cura della persona in situazione di disabilità grave. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 26 gennaio 2009.  &#13;
F.to:  &#13;
Giovanni Maria FLICK, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 30 gennaio 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
