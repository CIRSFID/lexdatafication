<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/129/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/129/"/>
          <FRBRalias value="ECLI:IT:COST:2018:129" name="ECLI"/>
          <FRBRdate date="23/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="129"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/129/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/129/ita@/!main"/>
          <FRBRdate date="23/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/129/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/129/ita@.xml"/>
          <FRBRdate date="23/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/06/2018" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2018</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Giulio Prosperetti</cc:relatore_pronuncia>
        <cc:data_decisione>23/05/2018</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 11, comma 9, della legge della Regione autonoma Friuli-Venezia Giulia 4 agosto 2017, n. 31 (Assestamento del bilancio per gli anni 2017-2019 ai sensi dell'articolo 6 della legge regionale 10 novembre 2015, n. 26), promosso con ricorso del Presidente del Consiglio dei ministri, notificato il 9-13 ottobre 2017, depositato in cancelleria il 13 ottobre 2017, iscritto al n. 82 del registro ricorsi 2017 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2017.
 Udito nella camera di consiglio del 23 maggio 2018 il Giudice relatore Giulio Prosperetti.
 Ritenuto che, con ricorso notificato il 9-13 ottobre 2017 e depositato il 13 ottobre 2017 (reg. ric. n. 82 del 2017), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso, in riferimento all'art. 117, terzo comma, della Costituzione, nonché agli artt. 4, numero 1), e 68, secondo comma, della legge costituzionale 31 gennaio 1963, n. 1 (Statuto speciale della Regione Friuli-Venezia Giulia), questione di legittimità costituzionale dell'art. 11, comma 9, della legge della Regione autonoma Friuli-Venezia Giulia 4 agosto 2017, n. 31 (Assestamento del bilancio per gli anni 2017-2019 ai sensi dell'articolo 6 della legge regionale 10 novembre 2015, n. 26);
 che, secondo il ricorrente, la norma regionale esenta le amministrazioni del comparto unico del pubblico impiego regionale e locale dal limite dell'ammontare complessivo delle risorse destinate annualmente al trattamento economico accessorio del personale, anche di livello dirigenziale, posto dall'art. 23, comma 2, del decreto legislativo 25 maggio 2017, n. 75, recante «Modifiche e integrazioni al decreto legislativo 30 marzo 2001, n. 165, ai sensi degli articoli 16, commi 1, lettera a), e 2, lettere b), c), d) ed e) e 17, comma 1, lettere a), c), e), f), g), h), l) m), n), o), q), r), s) e z), della legge 7 agosto 2015, n. 124, in materia di riorganizzazione delle amministrazioni pubbliche»;
 che il limite così stabilito dalla citata disposizione statale costituirebbe principio fondamentale in materia di coordinamento della finanza pubblica, come tale non derogabile dalla legislazione regionale, ivi compresa quella delle Regioni a statuto speciale;
 che, pertanto, la disposizione impugnata violerebbe sia l'art. 117, terzo comma, Cost., che l'art. 4, numero 1), e l'art. 68, secondo comma, dello statuto speciale della Regione autonoma Friuli-Venezia Giulia, secondo cui la potestà legislativa regionale in materia di stato giuridico ed economico del personale addetto agli uffici e agli enti regionali deve svolgersi in armonia con la Costituzione e con i principi generali dell'ordinamento giuridico della Repubblica;
 che la Regione autonoma Friuli-Venezia Giulia non si è costituita;
 che, nelle more del giudizio, la norma impugnata è stata abrogata dall'art. 12, comma 10, della legge della Regione autonoma Friuli-Venezia Giulia 10 novembre 2017, n. 37 (Disposizioni urgenti in materia di programmazione contabilità).
 Considerato che, con atto notificato in data 12 gennaio 2018 e depositato nella cancelleria di questa Corte il 15 gennaio 2018, il Presidente del Consiglio dei ministri ha dichiarato di rinunciare al ricorso, in conformità alla delibera adottata dal Consiglio dei ministri nella seduta del 29 dicembre 2017, per essere venute meno le ragioni che avevano indotto all'impugnazione della disposizione regionale in oggetto;
 che, in mancanza di costituzione in giudizio della Regione resistente, l'intervenuta rinuncia al ricorso in via principale determina, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (ex plurimis, ordinanze n. 60 e n. 55 del 2018; n. 112 e n. 100 del 2017; n. 137 e n. 27 del 2016).
 Visti l'art. 26, secondo comma, della legge 11 marzo 1953, n. 87, e gli artt. 9, comma 2, e 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara estinto il processo. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 maggio 2018.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Giulio PROSPERETTI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 13 giugno 2018.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
