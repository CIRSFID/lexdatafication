<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2006/287/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2006/287/"/>
          <FRBRalias value="ECLI:IT:COST:2006:287" name="ECLI"/>
          <FRBRdate date="03/07/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="287"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2006/287/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2006/287/ita@/!main"/>
          <FRBRdate date="03/07/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2006/287/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2006/287/ita@.xml"/>
          <FRBRdate date="03/07/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="14/07/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Luigi Mazzella</cc:relatore_pronuncia>
        <cc:data_decisione>03/07/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 3, primo comma, della legge 19 febbraio 1981, n. 27 (Provvidenze per il personale di magistratura), promosso con ordinanza del 24 maggio 2005 dal Tribunale amministrativo regionale della Puglia, sezione di Lecce, sul ricorso proposto da I. C. contro il Ministero dell'economia e delle finanze ed altro, iscritta al n. 481 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 40, prima serie speciale, dell'anno 2005. 
      Udito nella camera di consiglio del 21 giugno 2006 il Giudice relatore Luigi Mazzella.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fattoNel corso di un giudizio promosso da un magistrato ordinario contro il Ministero dell'economia e delle finanze ed il Ministero della giustizia, il Tribunale amministrativo regionale della Puglia, sezione di Lecce, con ordinanza del 24 maggio 2005, ha sollevato, in riferimento agli artt. 3 e 36 della Costituzione, questione di legittimità costituzionale dell'art. 3 della legge 19 febbraio 1981, n. 27 (Provvidenze per il personale di magistratura), nella parte in cui esclude la corresponsione dell'indennità da esso prevista durante il periodo di congedo straordinario per malattia. 
      Il rimettente espone che la parte privata ha proposto ricorso diretto ad ottenere l'annullamento del provvedimento con il quale era stato accertato a suo carico un debito verso lo Stato a titolo di ripetizione dell'indennità contemplata dall'art. 3 della legge n. 27 del 1981 (cosiddetta indennità giudiziaria) erogata nel corso di due periodi di congedo straordinario per malattia. 
      Il TAR, premesso che l'indennità in questione ha natura prevalentemente retributiva e che la sua corresponsione si giustifica in relazione agli oneri che il magistrato incontra nello svolgimento della propria attività, afferma che la predetta disposizione normativa violerebbe l'art. 36 Cost., sia perché quegli oneri non sono connessi a particolari periodi dell'anno, sia in considerazione della consistente entità della decurtazione operata. 
      A parere del rimettente l'art. 3 della legge n. 27 del 1981 contrasterebbe, inoltre, con l'art. 3 Cost. per molteplici motivi. 
      Un primo profilo di illegittimità è individuato dal TAR nella circostanza della concessione al personale appartenente ai ruoli delle cancellerie e delle segreterie giudiziarie dell'indennità di amministrazione (nella quale è confluita l'indennità giudiziaria, ai sensi dell'art. 34 del contratto collettivo nazionale di lavoro del personale del comparto Ministeri del 16 maggio 1995), anche durante i periodi di assenza per malattie di durata superiore ai quindici giorni. In proposito il rimettente ritiene che la diversità del regime della regolamentazione del rapporto di lavoro delle categorie poste a confronto (magistrati, da un lato, e personale dirigente delle cancellerie e delle segreterie, dall'altro) non valga ad escludere la prospettata violazione dell'art. 3 Cost., perché il fatto che un tipo di rapporto sia regolato dalla legge e l'altro dal contratto collettivo non esimerebbe il legislatore che regola il primo dal dovere di rispettare il suddetto principio costituzionale. 
    L'art. 3 della legge n. 27 del 1981, poi, contrasterebbe con l'art. 3 Cost. anche perché, per il primo giorno di malattia, determina l'integrale venir meno dell'indennità da esso prevista, a differenza delle altre componenti retributive che, con riferimento a quel giorno, subiscono solamente una parziale decurtazione. 
    Infine, ulteriore aspetto di possibile irragionevolezza della norma è ravvisato dal rimettente nel fatto che la mancata erogazione dell'indennità è connessa ad una causa non imputabile al magistrato, consistente in un  evento che reca pregiudizio al bene fondamentale della salute. 
    Quanto alla rilevanza della questione, il giudice a quo evidenzia che la pretesa creditoria della parte privata è preclusa dalla norma censurata che non prevede la corresponsione dell'indennità giudiziaria nei periodi in contestazione.Considerato in diritto1. – Il Tribunale amministrativo regionale della Puglia, sezione di Lecce, ha sollevato, con riferimento agli artt. 3 e 36 della Costituzione, questione di legittimità costituzionale dell'art. 3 della legge 19 febbraio 1981, n. 27 (Provvidenze per il personale di magistratura), nella parte in cui esclude la corresponsione dell'indennità da esso prevista durante il periodo di congedo straordinario per malattia. 
    A parere del rimettente, la norma denunciata contrasterebbe con l'art. 3 Cost., perché determina un'ingiustificata disparità di trattamento rispetto al personale delle cancellerie e delle segreterie giudiziarie che invece gode dell'indennità contemplata dalla disposizione impugnata anche nel periodo di assenza per malattia di durata superiore a quindici giorni, perché irrazionalmente la norma stabilisce l'integrale soppressione dell'indennità per il primo giorno di malattia (laddove le altre componenti retributive, per quel giorno, vengono solamente decurtate parzialmente) e perché la sospensione dell'erogazione dell'indennità è collegata a causa non imputabile al magistrato. 
    L'art. 36 Cost., a sua volta, sarebbe violato sia perché l'indennità giudiziaria ha natura prevalentemente retributiva e la sua erogazione si giustificherebbe in relazione agli oneri (non connessi a particolari periodi dell'anno) che il magistrato incontra nello svolgimento della sua attività, sia in ragione della consistente entità dell'indennità e, quindi, della riduzione che il trattamento economico del magistrato subisce durante i periodi di assenza  per malattia. 
      2. – La questione non è fondata. 
      A proposito della lamentata violazione dell'art. 3 Cost. per ingiustificata disparità di trattamento tra magistrati e impiegati amministrativi delle cancellerie e delle segreterie giudiziarie, questa Corte, proprio in materia di indennità giudiziaria, ha già escluso la possibilità di istituire un simile raffronto a causa della mancanza di omogeneità tra le due categorie di dipendenti e del diverso meccanismo di determinazione dei rispettivi trattamenti retributivi (sentenza n. 15 del 1995; ordinanze n. 167 e n. 33 del 1996, n. 451 e n. 98 del 1995). 
    Alle considerazioni richiamate si deve aggiungere che le differenze di regime giuridico tra le due categorie di dipendenti statali si sono accentuate a seguito della riforma del pubblico impiego, stante la diversità ormai riscontrabile sul piano delle fonti della disciplina dei rispettivi rapporti di impiego (il rapporto di lavoro degli impiegati è disciplinato in gran parte – ed in particolare per la materia del trattamento economico – da fonti contrattuali, quello dei magistrati esclusivamente dalla legge). 
    Il rimettente sostiene che la disposizione impugnata contrasta con l'art. 3 Cost. anche perché comporta l'integrale decurtazione dell'indennità per il primo giorno di malattia, mentre – in virtù dell'art. 40, primo comma, del d. P. R. 10 gennaio 1957, n. 3 (Testo unico delle disposizioni concernenti lo statuto degli impiegati civili dello Stato) – le altre componenti retributive per quello stesso giorno vengono decurtate solo di un terzo. La diversa entità della decurtazione che subisce l'indennità giudiziaria non vale, però, a rendere irragionevole la disciplina dell'emolumento dettata dall'art. 3 della legge n. 27 del 1981, rientrando nella discrezionalità del legislatore la determinazione dell'ammontare del trattamento complessivamente assicurato al magistrato durante il primo giorno di assenza per malattia, nel rispetto del limite minimo stabilito dall'art. 38, secondo comma, Cost. 
    Né assume rilevanza la circostanza, pure evidenziata dal giudice a quo, secondo la quale la decurtazione dell'indennità giudiziaria sarebbe collegata ad una causa (la malattia) non imputabile al magistrato. Infatti la Costituzione non impone di attribuire al dipendente assente per malattia lo stesso trattamento economico di cui gode in costanza di attività lavorativa, essendo sufficiente che al lavoratore siano assicurati mezzi adeguati anche durante il periodo di malattia. 
    Non sussiste neppure la denunciata violazione dell'art. 36 Cost. 
    Come ripetutamente affermato da questa Corte, al fine di verificare la legittimità delle norme in tema di trattamento economico dei lavoratori dipendenti in relazione al disposto dell'art. 36 (e, in particolare, la conformità della retribuzione ai requisiti costituzionali della proporzionalità e della sufficienza), occorre far riferimento, non già alle singole componenti di quel trattamento, ma alla retribuzione nel suo complesso (tra le più recenti affermazioni di un simile principio, si vedano le sentenze n. 470 del 2002 e n. 164 del 1994 e, con specifico riferimento all'indennità giudiziaria, le ordinanze n. 33 del 1996 e n. 98 del 1995).</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      dichiara non fondata la questione di legittimità costituzionale dell'art. 3 della legge 19 febbraio 1981, n. 27 (Provvidenze per il personale di magistratura), sollevata, in riferimento agli artt. 3 e 36 della Costituzione, dal Tribunale amministrativo regionale della Puglia, sezione di Lecce, con l'ordinanza indicata in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 luglio 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Luigi MAZZELLA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 14 luglio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
