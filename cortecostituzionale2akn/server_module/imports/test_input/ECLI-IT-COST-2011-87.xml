<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/87/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/87/"/>
          <FRBRalias value="ECLI:IT:COST:2011:87" name="ECLI"/>
          <FRBRdate date="07/03/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="87"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/87/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/87/ita@/!main"/>
          <FRBRdate date="07/03/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/87/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/87/ita@.xml"/>
          <FRBRdate date="07/03/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="11/03/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>DE SIERVO</cc:presidente>
        <cc:relatore_pronuncia>Alfonso Quaranta</cc:relatore_pronuncia>
        <cc:data_decisione>07/03/2011</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione della Camera dei deputati del 28 ottobre 2009 (doc. IV-ter n. 10-A), relativa alla insindacabilità, ai sensi dell'articolo 68, primo comma, della Costituzione, delle opinioni espresse dall'onorevole Carmine Santo Patarino nei confronti del dottor Nicola Putignano, promosso dal Giudice dell'udienza preliminare presso il Tribunale ordinario di Taranto, con ricorso depositato in cancelleria il 7 giugno 2010 ed iscritto al n. 5 del registro conflitti tra poteri dello Stato 2010, fase di ammissibilità. 
 Udito nella camera di consiglio del 9 febbraio 2011 il Giudice relatore Alfonso Quaranta.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che con ricorso depositato presso la cancelleria della Corte il 7 giugno 2010, il Giudice dell'udienza preliminare presso il Tribunale ordinario di Taranto ha promosso conflitto di attribuzione tra poteri dello Stato; 
 che il ricorrente ha chiesto a questa Corte di dichiarare che non spettava alla Camera dei deputati il potere di affermare che i fatti per i quali è in corso procedimento penale nei confronti dell'on. Carmine Santo Patarino, per il delitto di cui all'art. 595 del codice penale, concernono opinioni espresse da un parlamentare nell'esercizio delle sue funzioni, annullando, per l'effetto, la deliberazione adottata dalla medesima Camera dei deputati il 28 ottobre 2009 (doc. IV-ter n. 10-A);
 che, deduce il ricorrente, con tale deliberazione la Camera dei deputati - nel recepire le conclusioni contenute nella relazione della Giunta per le autorizzazioni a procedere, secondo cui «l'operazione urbanistica e commerciale promossa dal Putignano insiste (...) con straordinaria rilevanza sull'area nella quale Patarino è eletto» - ha affermato che «i fatti per i quali è in corso il procedimento concernono opinioni espresse da un membro del Parlamento nell'esercizio delle proprie funzioni, ai sensi dell'art. 68, primo comma, della Costituzione»;
 che, rileva il G.u.p. presso il Tribunale ordinario di Taranto, la predetta deliberazione della Camera dei deputati, nel fare proprie le determinazioni espresse dalla Giunta per le autorizzazioni a procedere, «ha ravvisato nella vicenda una reazione del Putignano per via giudiziaria sproporzionata e quasi intimidatoria nei confronti di chi - a torto o a ragione - si è erto tutore istituzionale di coloro che temono per la loro attività e per la sostenibilità dello sviluppo della loro terra», ritenendo per tali ragioni le dichiarazioni espresse dall'on. Patarino non sindacabili a norma dell'art. 68, primo comma, Cost.;
 che, tanto premesso in fatto, il ricorrente evidenzia come - ai sensi dell'art. 3, comma 1, della legge 20 giugno 2003, n. 140 (Disposizioni per l'attuazione dell'art. 68 della Costituzione nonché in materia di processi penali nei confronti delle alte cariche dello Stato) - la garanzia della insindacabilità, oltre che per gli «atti tipici» dei membri delle Camere, debba intendersi estesa ad «ogni altra attività di ispezione, di divulgazione, di critica e di denuncia politica, connessa alla funzione di parlamentare, espletata anche fuori del Parlamento»;
 che, tuttavia, osserva sempre il G.u.p. ricorrente, la Corte costituzionale - investita dello scrutinio di costituzionalità della norma da ultimo citata - ha precisato che essa si sarebbe limitata a rendere esplicito il contenuto dell'art. 68 Cost., specificando quali atti, pur non tipici, debbono ritenersi connessi alla funzione parlamentare;
 che, nel caso di specie, la condotta ascritta all'on. Patarino non ricadrebbe nell'ambito di applicazione della citata disposizione costituzionale, giacché al parlamentare è contestato di aver denigrato il dott. Nicola Putignano, con denuncia presentata alla Procura della Repubblica presso il Tribunale ordinario di Taranto; 
 che, in particolare, il Putignano (già senatore della Repubblica e consigliere del Comune di Castellaneta) sarebbe stato accusato «di aver inviato al Sindaco di Castellaneta una lettera raccomandata contenente "intimidazioni minacciose", che dovevano essere interpretate come rivolte "ad estorcere provvedimenti amministrativi"» in favore del medesimo Putignano, nonché «di non gradire che da parte degli amministratori vi fosse il rispetto della legge e la trasparenza, essendo lo stesso ed il gruppo di "Nuova Concordia" abituati da sempre a fare il cattivo tempo per il loro interesse aziendale»;
 che il parlamentare, inoltre, si sarebbe lasciato «andare ad altre numerose espressioni rivolte a screditare e squalificare la condotta del Putignano», anche «nella qualità di amministratore del gruppo "Nuova Concordia", in specie per quanto relativo al rapporto tra lo stesso e l'amministrazione comunale di Castellaneta», rinviando, sul punto, il G.u.p. presso il Tribunale di Taranto «al contenuto della denuncia e ai passi della stessa contenenti espressioni offensive dell'altrui reputazione»;
 che - come affermato dalla Corte costituzionale - «il mero "contesto politico" o, in ogni modo, l'attinenza a temi di rilievo generale dibattuti in Parlamento, non connota di per sé le dichiarazioni come espressive della funzione parlamentare» (sentenza n. 134 del 2008);
 che, invece, dalla relazione della Giunta per le autorizzazioni a procedere della Camera dei deputati non emergerebbe alcun atto tipico della funzione parlamentare cui ricondurre la denuncia, che si assume diffamatoria, presentata dall'on. Patarino, visto che tale relazione reca soltanto un generico riferimento all'impegno politico profuso dal deputato della circoscrizione il cui territorio è stato interessato dall'operazione urbanistica e commerciale promossa dal Putignano;
 che il ricorrente, pertanto, ritiene che le dichiarazioni oggetto del procedimento penale non siano riferibili alla funzione parlamentare del Patarino.
 Considerato che, in questa fase del giudizio, la Corte è chiamata, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), a deliberare, senza contraddittorio, se il ricorso sia ammissibile in quanto vi sia «materia di un conflitto la cui risoluzione spetti alla sua competenza», sussistendone i requisiti soggettivo ed oggettivo e restando impregiudicata ogni ulteriore questione, anche in punto di ammissibilità; 
 che, sotto il profilo del requisito soggettivo, va riconosciuta la legittimazione del Tribunale ordinario di Taranto, in funzione di Giudice dell'udienza preliminare, a promuovere conflitto di attribuzione tra poteri dello Stato, in quanto organo giurisdizionale, in posizione di indipendenza costituzionalmente garantita, competente a dichiarare definitivamente la volontà del potere cui appartiene nell'esercizio delle funzioni attribuitegli; 
 che, parimenti, deve essere riconosciuta la legittimazione della Camera dei deputati ad essere parte del presente conflitto, quale organo competente a dichiarare in modo definitivo la propria volontà in ordine all'applicazione dell'art. 68, primo comma, della Costituzione; 
 che, per quanto attiene al profilo oggettivo, il ricorrente lamenta la lesione della propria sfera di attribuzione, costituzionalmente garantita, in conseguenza di un esercizio ritenuto illegittimo, per inesistenza dei relativi presupposti, del potere spettante alla Camera dei deputati di dichiarare l'insindacabilità delle opinioni espresse da un membro di quel ramo del Parlamento ai sensi dell'art. 68, primo comma, della Costituzione; 
 che, dunque, esiste la materia di un conflitto la cui risoluzione spetta alla competenza di questa Corte.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara ammissibile, ai sensi dell'articolo 37 della legge 11 marzo 1953, n. 87, il ricorso per conflitto di attribuzione tra poteri dello Stato indicato in epigrafe, proposto dal Giudice dell'udienza preliminare presso il Tribunale ordinario di Taranto, nei confronti della Camera dei deputati; &#13;
 dispone: &#13;
 a) che la Cancelleria di questa Corte dia immediata comunicazione al Giudice dell'udienza preliminare presso il Tribunale ordinario di Taranto, della presente ordinanza; &#13;
 b) che il ricorso e la presente ordinanza siano, a cura del ricorrente, notificati alla Camera dei deputati, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati, con la prova dell'avvenuta notifica, presso la Cancelleria della Corte entro il termine di trenta giorni previsto dall'art. 24, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 marzo 2011.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Alfonso QUARANTA , Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria l'11 marzo 2011.&#13;
 Il Cancelliere&#13;
 F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
