<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2018/138/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2018/138/"/>
          <FRBRalias value="ECLI:IT:COST:2018:138" name="ECLI"/>
          <FRBRdate date="08/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="138"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2018/138/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2018/138/ita@/!main"/>
          <FRBRdate date="08/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2018/138/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2018/138/ita@.xml"/>
          <FRBRdate date="08/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="27/06/2018" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2018</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Aldo Carosi</cc:relatore_pronuncia>
        <cc:data_decisione>08/05/2018</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 6, comma 2, della legge della Regione Piemonte 26 aprile 2017, n. 7 (Disposizioni in materia di disostruzione pediatrica e di rianimazione cardiopolmonare), promosso dal Presidente del Consiglio dei ministri, con ricorso notificato il 30 giugno-4 luglio 2017, depositato in cancelleria il 7 luglio 2017, iscritto al n. 48 del registro ricorsi 2017 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 31, prima serie speciale, dell'anno 2017.
 Visto l'atto di costituzione della Regione Piemonte;
 udito nell'udienza pubblica dell'8 maggio 2018 il Giudice relatore Aldo Carosi;
 udito l'avvocato dello Stato Enrico De Giovanni per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Con ricorso notificato il 30 giugno-4 luglio 2017, il Presidente del Consiglio dei ministri ha promosso, in riferimento all'art. 81, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 6, comma 2, della legge della Regione Piemonte 26 aprile 2017, n. 7 (Disposizioni in materia di disostruzione pediatrica e di rianimazione cardiopolmonare).
 Il ricorrente, dopo aver evidenziato che la legge reg. Piemonte n. 7 del 2017 prevede percorsi formativi e informativi volti a promuovere la diffusione, al di fuori dell'ambiente ospedaliero, delle tecniche salvavita, della prevenzione primaria, della disostruzione delle vie aeree in ambito pediatrico con rianimazione cardiopolmonare e degli elementi di primo soccorso (artt. 2, 3 e 4), lamenta che gli oneri connessi non ricevano la necessaria copertura per l'esercizio 2018. Infatti, l'impugnato art. 6 della legge regionale, dopo averli quantificati in euro 100.000,00 annui (comma 1), dispone che a essi, per il biennio 2017-2018, si provveda con le risorse finanziarie di cui alla missione 20, programma 20.03, del bilancio di previsione 2017-2019 (comma 2). Tale posta di bilancio, tuttavia, non avrebbe alcuna capienza per l'esercizio 2018. Ne deriverebbe l'illegittimità costituzionale della norma di copertura finanziaria e, conseguentemente (si cita, in particolare, la sentenza n. 214 del 2012), delle disposizioni sostanziali generatrici della spesa, ovvero, nella fattispecie, dell'intera legge.
 2.- Si è costituita in giudizio la Regione Piemonte, chiedendo il rigetto del ricorso.
 Secondo la resistente, le spese previste dall'intervento legislativo in considerazione avrebbero natura "continuativa e ricorrente", incidendo su una pluralità indefinita di esercizi finanziari, con la conseguenza che la relativa copertura potrebbe essere fornita con la legge di bilancio di previsione per l'esercizio 2018, attingendo al «Fondo di riserva per le autorizzazioni di spesa delle leggi permanenti di natura corrente», istituito dall'art. 21 della legge della Regione Piemonte 11 aprile 2001, n. 7 (Ordinamento contabile della Regione Piemonte).
 3.- Con memoria illustrativa depositata in prossimità dell'udienza, il Presidente del Consiglio dei ministri ha negato la natura continuativa delle spese previste dalla legge reg. Piemonte n. 7 del 2017, in quanto destinate a verificarsi solo nel biennio 2017-2018. Sostiene che, comunque, il difetto di copertura riguarderebbe anche l'esercizio 2019.Considerato in diritto1.- Con il ricorso indicato in epigrafe, il Presidente del Consiglio dei ministri ha promosso, in riferimento all'art. 81, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 6, comma 2, della legge della Regione Piemonte 26 aprile 2017, n. 7 (Disposizioni in materia di disostruzione pediatrica e di rianimazione cardiopolmonare).
 La citata legge regionale prevede percorsi formativi e informativi volti a promuovere la diffusione, al di fuori dell'ambiente ospedaliero, delle tecniche salvavita, della prevenzione primaria, della disostruzione delle vie aeree in ambito pediatrico con rianimazione cardiopolmonare e degli elementi di primo soccorso (artt. 2, 3 e 4).
 Il ricorrente lamenta che, per l'esercizio 2018, gli oneri connessi non ricevano la necessaria copertura da parte della disposizione censurata.
 Secondo il ricorrente la norma impugnata «non assicurando idonea copertura finanziaria, viola il principio fondamentale di copertura finanziaria delle leggi enunciato dall'art. 81, terzo comma, Cost., con conseguente illegittimità costituzionale, come affermato dalla Corte costituzionale nella sentenza n. 214 del 2012, dell'intera legge».
 La Regione Piemonte, costituitasi in giudizio, sostiene che le spese previste dall'intervento legislativo in considerazione avrebbero natura continuativa, asserendo che la copertura finanziaria possa essere fornita con la legge di bilancio di previsione relativa all'esercizio successivo. 
 Nella memoria illustrativa, depositata in prossimità dell'udienza, il Presidente del Consiglio dei ministri nega la natura continuativa delle spese previste dalla legge reg. Piemonte n. 7 del 2017 e sostiene che, nel caso in cui gli oneri fossero riferiti al triennio 2017-2019, il difetto di copertura riguarderebbe anche l'esercizio 2019.
 2.- Occorre sottolineare preliminarmente che il ricorso censura le modalità di copertura della spesa necessaria per i nuovi servizi relativamente al solo esercizio 2018, ma fa discendere da tale preteso vizio l'invalidità dell'intera legge.
 Peraltro, nella memoria depositata in prossimità dell'udienza, il Presidente del Consiglio dei ministri sostiene che il vizio di copertura riguarderebbe anche l'ultimo anno del bilancio triennale in vigore al momento della promulgazione della legge, cioè l'esercizio 2019, laddove fosse accettato l'assunto della difesa regionale secondo cui «gli oneri si manifestano sul triennio anziché sul biennio».
 Non è dubbio, anche per esplicita ammissione del legislatore regionale, che i servizi istituiti con la legge impugnata siano generatori di nuova spesa. Lo stesso art. 6, comma 1, della legge medesima, nell'indicare le risorse necessarie per la realizzazione dei percorsi formativi e informativi, quantifica gli oneri complessivi «per ciascun anno del bilancio di previsione finanziario 2017-2019» in euro 100.000, individuando nella missione 13 (Tutela della salute), programma 13.08 (Politica regionale unitaria per la tutela della salute), del bilancio 2017 la posta di imputazione della spesa.
 Il successivo comma 2 del medesimo art. 6 precisa, tuttavia, che «[a]gli oneri di cui al comma 1, si provvede per il biennio 2017-2018 con le risorse finanziarie della missione 20 programma 20.03 del bilancio di previsione finanziario 2017-2019». 
 Alla luce della formulazione letterale dell'art. 6, appare assolutamente priva di fondamento la difesa della Regione circa la natura "continuativa e ricorrente" della spesa, poiché non tiene in considerazione che i nuovi servizi, in quanto tali, comportano un nuovo onere che deve, a sua volta, trovare copertura sia all'interno del bilancio annuale che in quello triennale.
 Ai fini dello scrutinio della questione di legittimità costituzionale promossa dal Presidente del Consiglio dei ministri occorre invece individuare esattamente il significato dell'art. 6 della legge reg. Piemonte n. 7 del 2017 e, in particolare, il rapporto tra il comma 2, laddove è disposta la realizzazione dei percorsi formativi e informativi con le risorse finanziarie provenienti dalla missione 20.03 «Fondi e accantonamenti - Altri fondi» - che è ascrivibile alla categoria dei fondi di riserva successivamente meglio descritta - per il biennio 2017-2018, e il comma 1, il quale prevede oneri «complessivamente pari a 100.000 euro per ciascun anno del bilancio di previsione finanziario 2017-2019», ovvero per il triennio 2017-2019, con imputazione al predetto programma 13.08.
 Alla singolare formulazione della norma occorre conferire l'unica accezione conforme a Costituzione e, ancor prima, a elementari canoni di razionalità. I richiamati enunciati normativi non possono che significare che il legislatore regionale - dopo aver quantificato, con specificazione annuale, la spesa del triennio - si è preoccupato di prelevare le risorse dal fondo di riserva 2017 per il biennio 2017-2018.
 L'esposta interpretazione trova fondamento nella natura della partita di spesa, da cui il legislatore regionale attinge le risorse per la copertura degli oneri del biennio, vale a dire la missione 20 (Fondi e accantonamenti), programma 20.03 (Altri fondi), del Titolo 1 della spesa. Si tratta di un fondo di riserva dal quale è possibile soltanto attingere risorse per creare o implementare altre partite di spesa, ma su cui è preclusa l'imputazione diretta.
 Da ciò consegue che la copertura di 200.000 euro per il biennio 2017-2018 avviene - secondo il legislatore regionale - prelevando detta somma dallo stanziamento della missione 20, programma 03, che presentava, al Titolo 1 (Spese correnti) del bilancio di previsione 2017, uno stanziamento di competenza di euro 722.686.572,75 e uno stanziamento di cassa di euro 210.566.265,73, entrambi capienti rispetto alla somma del fabbisogno del biennio 2017-2018. Ne deriva che la somma così prelevata dal fondo e attribuita alla missione 13, programma 13.08, è idonea a coprire sia l'onere del 2017 che quello del 2018, le cui risorse sono entrambe allocate alla competente partita di spesa.
 I principi contabili non vietano di coprire - attraverso l'utilizzazione del fondo di riserva - la spesa relativa a un esercizio successivo a quello in cui si effettua detto prelievo. Si tratta di una pratica che non collide con il principio di copertura di cui all'art. 81, terzo comma, Cost. poiché si limita alla conservazione, per l'impiego successivo, di una risorsa esistente e disponibile al momento di deliberazione della spesa.
 3.- Alla luce di tali premesse, la questione di legittimità costituzionale promossa dal Presidente del Consiglio dei ministri non è fondata, riguardando il solo esercizio 2018, per il quale - come già evidenziato - il prelievo dal fondo di riserva 2017 era corredato da idoneo stanziamento.
 Non può essere peraltro preso in considerazione il profilo di censura relativo all'esercizio 2019, proposto dal ricorrente solo nella memoria illustrativa e del tutto estraneo all'originario thema decidendum delineato nel ricorso.
 Non assume rilievo, quindi, l'eventuale indagine circa l'esistenza, o meno, di valida copertura per l'esercizio 2019.
 3.1.- Nondimeno, non si può non rilevare che, nel momento in cui viene deliberata l'assunzione di un nuovo servizio e quantificata la corrispondente spesa per il triennio relativo al coevo bilancio triennale, attraverso il prelievo da uno specifico fondo congruente con tali finalità, si pone in essere una variazione di bilancio che deve essere illustrata nella sua complessiva neutralità. 
 In altre parole, una legge che istituisce un nuovo servizio, coprendone la spesa attraverso il prelievo da un fondo di riserva, è un atto che incide sull'articolazione del bilancio, mutandone - sia pure in modo compensativo - le singole componenti.
 Per questo motivo la variazione dovrebbe essere illustrata in modo completo ed esaustivo, non limitandosi alla dimensione del prelievo dal fondo e all'assegnazione al pertinente programma, bensì corredandola dei nuovi stanziamenti conseguenti all'operazione modificativa.
 Tale regola non è meramente formale, ma si collega teleologicamente alla garanzia degli equilibri e al principio di trasparenza.
 Sotto il primo profilo, è evidente che il mancato contestuale aggiornamento degli stanziamenti può costituire una causa di squilibrio nel caso in cui successive variazioni non tengano conto della precedente rideterminazione; sotto il profilo della trasparenza, una simile prassi è idonea a creare pericolose zone d'ombra nel corso della gestione finanziaria.
 È stato già affermato per quel che concerne il bilancio consuntivo delle Regioni - ma le considerazioni valgono per tutti gli atti normativi che incidono sul bilancio di previsione, istituendo nuove spese - che la «sofisticata articolazione» degli schemi finanziari deve esse compensata - nel testo della legge - «da una trasparente, corretta, univoca, sintetica e inequivocabile indicazione» delle componenti che incidono sulle risultanze del bilancio (sentenza n. 274 del 2017).
 4.- In conclusione, pur nel problematico contesto che ha caratterizzato la genesi e l'articolazione della legge impugnata, e ferma restando per la Regione l'impossibilità di dar seguito per gli esercizi successivi al 2018 a spese non coperte nelle forme di legge (nel caso in cui a ciò non si sia provveduto), la questione promossa dal Presidente del Consiglio dei ministri non è fondata poiché: a) la copertura della spesa dell'esercizio 2018, l'unica oggetto di censura nel ricorso, risulta conforme a legge; b) il preteso difetto di copertura dell'esercizio 2019 è stato evocato solo nella memoria illustrativa del ricorrente e, non essendo incluso nell'originario thema decidendum, esula dal presente giudizio.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara non fondata, nei sensi di cui in motivazione, la questione di legittimità costituzionale dell'art. 6, comma 2, della legge della Regione Piemonte 26 aprile 2017, n. 7 (Disposizioni in materia di disostruzione pediatrica e di rianimazione cardiopolmonare), promossa, in riferimento all'art. 81, terzo comma, della Costituzione, dal Presidente del Consiglio dei ministri, con il ricorso indicato in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8 maggio 2018.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Aldo CAROSI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 27 giugno 2018.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
