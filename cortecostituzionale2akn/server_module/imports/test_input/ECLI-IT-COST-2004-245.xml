<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2004/245/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2004/245/"/>
          <FRBRalias value="ECLI:IT:COST:2004:245" name="ECLI"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="245"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2004/245/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2004/245/ita@/!main"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2004/245/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2004/245/ita@.xml"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="20/07/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>08/07/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 291 del codice civile, promosso con ordinanza dell'8 luglio 2000 dal Tribunale di La Spezia sul ricorso proposto da Bassi Luigi, iscritta al n. 759 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 39, prima serie speciale, dell'anno 2003. 
    Udito nella camera di consiglio del 26 maggio 2004 il Giudice relatore Alfio Finocchiaro.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Il Tribunale di La Spezia, nel corso di un procedimento per l'adozione di maggiorenne, con ordinanza dell'8 luglio 2000, ha sollevato questione di legittimità costituzionale dell'art. 291 del codice civile, in riferimento all'art. 3 della Costituzione, nella parte in cui consente l'adozione di persona maggiore di età da parte di chi abbia discendenti naturali riconosciuti in età minore.  
    Il giudice a quo espone che il ricorrente, il quale ha chiesto, ai sensi dell'art. 291 cod. civ., l'adozione della figlia maggiorenne della sua attuale convivente, nata dal matrimonio di questa con persona deceduta, ha dichiarato di non avere avuto figli dal proprio precedente matrimonio - di cui era stata già dichiarata la cessazione degli effetti civili con sentenza passata in giudicato - nonché di avere un figlio naturale riconosciuto minore, nato dall'unione more uxorio con la madre della maggiorenne di cui chiedeva l'adozione. 
    Il Tribunale - poiché l'art. 291 cod. civ., come risulta dopo la sentenza della Corte costituzionale n. 557 del 1988, consente l'adozione di maggiorenne a chi non ha discendenti legittimi o legittimati e a chi ha figli legittimi o legittimati maggiorenni che abbiano prestato il consenso, precludendo l'adozione a chi ha discendenti legittimi o legittimati minori, perché non in grado di esprimere il consenso, mentre permette l'adozione di maggiorenne da parte di chi ha discendenti naturali riconosciuti in età minore - prospetta una disparità di trattamento tra la posizione del figlio minore legittimo o legittimato e quella del figlio minore naturale riconosciuto dell'adottante e solleva d'ufficio la questione di costituzionalità. 
    Quanto alla rilevanza, il giudice rimettente sottolinea che dalla decisione della Corte costituzionale dipende l'accoglimento o il rigetto della domanda di adozione. 
    2.- Nel giudizio innanzi a questa Corte non vi è stata costituzione di parti private, né è intervenuto il Presidente del Consiglio dei ministri.Considerato in diritto1.- Il Tribunale di La Spezia, nel corso di un procedimento diretto all'adozione di maggiorenne da parte di soggetto con figlio minore naturale riconosciuto, ha sollevato d'ufficio questione di legittimità costituzionale dell'art. 291 cod. civ. - quale risulta dopo la sentenza della Corte Costituzionale n. 557 del 1988 - che consente l'adozione di persona maggiore di età da parte di chi abbia discendenti naturali riconosciuti in età minore, per violazione dell'art. 3 della Costituzione, sussistendo disparità di trattamento tra la posizione del figlio minore legittimo o legittimato e quella del figlio minore naturale riconosciuto dell'adottante, atteso che l'adozione di persona maggiore d'età è preclusa nel primo caso mentre è consentita nel secondo. 
    2.- La questione è fondata. 
    L'art. 291, primo comma, cod. civ., nel testo sostituito dall'art. 1 della legge 5 giugno 1967, n. 431, sull'adozione speciale, disponeva che l'adozione è permessa alle persone che non hanno discendenti legittimi o legittimati, che hanno compiuto gli anni trentacinque e che superano di almeno diciotto anni l'età di coloro che intendono adottare.  
    Successivamente, questa Corte, con sentenza n. 557 del 1988, ha dichiarato la illegittimità costituzionale della norma citata nella parte in cui non consente l'adozione a persone che abbiano discendenti legittimi o legittimati maggiorenni e consenzienti. 
    A seguito della pronuncia di incostituzionalità, la dottrina e la giurisprudenza sono concordi nell'interpretare l'art. 291 cod. civ. nel senso che il divieto di adozione di maggiorenni si applica a coloro che hanno figli legittimi o legittimati minori, o, se maggiorenni, non consenzienti, e non anche a coloro che hanno figli naturali riconosciuti. 
    Questa interpretazione, imposta dal tenore della disposizione, evidenzia una illegittima disparità disparità di trattamento fra figli legittimi e figli naturali riconosciuti ed in pregiudizio dei secondi, in quanto le ragioni di indole morale e patrimoniale, che consentono ai primi di opporsi all'adozione, valgono anche per i figli naturali. 
    D'altro canto, nella situazione presa in esame non sono ipotizzabili profili di incompatibilità con i diritti dei membri della famiglia legittima che giustifichino un trattamento normativo differenziato. 
     Da quanto precede deriva che deve dichiararsi l'illegittimità costituzionale dell'art. 291 cod. civ., nella parte in cui non prevede che l'adozione di maggiorenni non possa essere pronunciata in presenza di figli naturali riconosciuti dall'adottante, minorenni o, se maggiorenni, non consenzienti.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara l'illegittimità costituzionale dell'art. 291 del codice civile nella parte in cui non prevede che l'adozione di maggiorenni non possa essere pronunciata in presenza di figli naturali, riconosciuti dall'adottante, minorenni o, se maggiorenni, non consenzienti. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte Costituzionale, Palazzo della Consulta  l'8  luglio 2004. &#13;
F.to: &#13;
Gustavo ZAGREBELSKY, Presidente &#13;
Alfio FINOCCHIARO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 20 luglio 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
