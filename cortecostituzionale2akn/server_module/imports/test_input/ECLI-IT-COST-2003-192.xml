<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/192/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/192/"/>
          <FRBRalias value="ECLI:IT:COST:2003:192" name="ECLI"/>
          <FRBRdate date="23/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="192"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/192/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/192/ita@/!main"/>
          <FRBRdate date="23/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/192/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/192/ita@.xml"/>
          <FRBRdate date="23/05/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="04/06/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>23/05/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 20, comma 1, del decreto legislativo 19 dicembre 1994, n. 758 (Modificazioni alla disciplina sanzionatoria in materia di lavoro), promosso, nell'ambito di un procedimento penale, dal Tribunale di Bari con ordinanza del 29 gennaio 2002, iscritta al n. 410 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 38, prima serie speciale, dell'anno 2002. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 9 aprile 2003 il Giudice relatore Guido Neppi Modona. 
    Ritenuto che il Tribunale di Bari ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 20, comma 1, del decreto legislativo 19 dicembre 1994, n. 758 (Modificazioni alla disciplina sanzionatoria in materia di lavoro), nella parte in cui prevede che la prescrizione al contravventore per l'eliminazione delle irregolarità riscontrate possa essere impartita dall'organo di vigilanza solamente ove questi agisca nell'esercizio di funzioni di polizia giudiziaria; 
    che il Tribunale premette che l'imputato - nei cui confronti procede per i reati di cui agli artt. 221 del regio decreto 27 luglio 1934, n. 1265 (per aver esercitato l'attività di elettrauto in locali privi del certificato di agibilità, condotta depenalizzata ex art. 70 del decreto legislativo 30 dicembre 1999, n. 507), 40 del d.P.R. 19 marzo 1956, n. 303 (per non aver individuato nei locali di lavoro una zona da destinare ad armadietti-spogliatoio) e 40 del decreto legislativo 15 agosto 1991, n. 277 (per non aver esibito la relazione fonometrica relativa all'attività svolta) - risulta aver «tempestivamente regolarizzato la situazione della propria officina, secondo quanto prescritto dall'organo di vigilanza»; 
    che la regolarizzazione non sarebbe tuttavia riconducibile alla prescrizione prevista dall'art. 20, comma 1, del d. lgs. n. 758 del 1994, in quanto avvenuta a seguito di un controllo operato da personale che, «pur facendo parte del personale ispettivo di cui all'art. 21 della legge 23 dicembre 1978, n. 833, era privo della qualifica di agente di polizia giudiziaria» e agiva con compiti di polizia di sicurezza; 
    che, in caso di prescrizione ritualmente impartita in base alla disposizione censurata, il contravventore può essere ammesso al pagamento in sede amministrativa di una somma pari al quarto del massimo dell'ammenda, cui consegue l'estinzione della contravvenzione, ex artt. 21, comma 2, e 24, comma 1, del d. lgs. n. 758 del 1994; 
    che ad avviso del Tribunale «la limitazione della possibilità di impartire la prescrizione di cui all'art. 20 ai soli casi in cui l'organo di vigilanza agisca nell'esercizio delle funzioni di polizia giudiziaria» si porrebbe in contrasto con l'art. 3 Cost., in quanto sarebbe irragionevole far dipendere la estinzione della contravvenzione «da circostanze accidentali e non ascrivibili in alcun modo alla volontà ed al comportamento dell'interessato», quali il fatto che l'accertamento della contravvenzione sia avvenuto nell'ambito di attività di polizia amministrativa o sia dovuto all'operato di personale ispettivo che non riveste la qualifica di agente di polizia giudiziaria;  
    che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata infondata, sulla base delle considerazioni svolte dalla Corte costituzionale nella sentenza n. 19 del 1998 e nelle ordinanze n. 205 del 1999 e n. 416 del 1998. 
    Considerato che il Tribunale di Bari censura, in riferimento all'art. 3 della Costituzione, l'art. 20, comma 1, del decreto legislativo 19 dicembre 1994, n. 758, nella parte in cui prevede che la prescrizione al contravventore per l'eliminazione delle irregolarità riscontrate possa essere impartita solamente dall'organo di vigilanza che agisce nell'esercizio di funzioni di polizia giudiziaria, ritenendo irragionevole far dipendere l'estinzione della contravvenzione da circostanze accidentali e non ascrivibili all'interessato, quali la qualifica di organo di polizia giudiziaria del soggetto accertatore; 
    che nel sollevare la questione il rimettente ha trascurato di prendere in considerazione che situazioni sostanzialmente analoghe a quella esposta, nelle quali la procedura descritta dagli artt. 20 e seguenti del d. lgs. n. 758 del 1994 non è stata ritualmente seguita, ma l'autore dell'illecito ha comunque provveduto a eliminare le violazioni contestategli, sono state prese in esame dalla sentenza n. 19 del 1998 e dalle ordinanze n. 205 del 1999 e n. 416 del 1998;  
    che la Corte, individuata la ratio della disciplina dettata dal d. lgs. n. 758 del 1994 nella duplice esigenza di «assicurare l'effettività dell'osservanza delle misure di prevenzione e di protezione in tema di sicurezza e di igiene del lavoro» e di «conseguire una consistente deflazione processuale», ha affermato che, nel caso in cui le conseguenze dannose o pericolose del reato risultino eliminate per effetto di una regolarizzazione spontanea o a seguito dell'osservanza di prescrizioni irritualmente impartite, non vi sono ostacoli a che il contravventore venga ammesso al pagamento della somma determinata a norma dell'art. 21 del d. lgs. n. 758 del 1994, così da poter usufruire dell'estinzione del reato disciplinata dall'art. 24 del medesimo decreto, ferma restando «la discrezionalità dell'autorità giudiziaria di adottare le soluzioni più idonee» per «ricondurre situazioni sostanzialmente omogenee a quelle espressamente previste dalla legge nell'alveo della procedura disciplinata dagli artt. 20 e ss. del decreto legislativo in esame» (v. la sentenza e le ordinanze sopra menzionate); 
    che a tali principi si è uniformata la giurisprudenza di legittimità, che ha tra l'altro affermato che la sequenza che prende l'avvio dalle prescrizioni di cui all'art. 20 del d. lgs. n. 758 del 1994 è presupposto procedimentale che condiziona lo sviluppo dell'azione penale, sicché il giudice non può pronunciare sentenza di condanna senza aver previamente verificato che all'imputato sia stato assicurato l'esercizio delle facoltà previste dalla disciplina in esame; 
    che la questione va pertanto dichiarata manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 20, comma 1, del decreto legislativo 19 dicembre 1994, n. 758 (Modificazioni alla disciplina sanzionatoria in materia di lavoro), sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale di Bari, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 maggio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 4 giugno 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
