<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2013/276/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2013/276/"/>
          <FRBRalias value="ECLI:IT:COST:2013:276" name="ECLI"/>
          <FRBRdate date="18/11/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="276"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2013/276/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2013/276/ita@/!main"/>
          <FRBRdate date="18/11/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2013/276/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2013/276/ita@.xml"/>
          <FRBRdate date="18/11/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="20/11/2013" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2013</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>SILVESTRI</cc:presidente>
        <cc:relatore_pronuncia>Marta Cartabia</cc:relatore_pronuncia>
        <cc:data_decisione>18/11/2013</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Gaetano SILVESTRI; Giudici : Luigi MAZZELLA, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'articolo 866, comma 1, del decreto legislativo 15 marzo 2010, n. 66 (Codice dell'ordinamento militare), promosso dal Tribunale amministrativo regionale per il Lazio, nel procedimento vertente tra R.A. e il Ministero della difesa, Comando generale dell'Arma dei Carabinieri, con ordinanza del 28 marzo 2013, iscritta al n. 128 del registro ordinanze 2013 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 23, prima serie speciale, dell'anno 2013.
 Udito nella camera di consiglio del 23 ottobre 2013 il Giudice relatore Marta Cartabia.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.- Con ordinanza del 28 marzo 2013 il Tribunale amministrativo regionale per il Lazio, sede di Roma, ha sollevato, su istanza di parte, questione di legittimità costituzionale dell'articolo 866, comma 1, del decreto legislativo 15 marzo 2010, n. 66 (Codice dell'ordinamento militare). La norma impugnata, rubricata «Condanna penale», prevede che: «1. La perdita del grado, senza giudizio disciplinare, consegue a condanna definitiva, non condizionalmente sospesa, per reato militare o delitto non colposo che comporti la pena accessoria della rimozione o della interdizione temporanea dai pubblici uffici, oppure una delle pene accessorie di cui all'articolo 19, comma 1, numeri 2) e 6) del codice penale». Il giudice a quo dubita che l'art. 866, comma 1, del d.lgs. n. 66 del 2010 violi gli artt. 3, 4, 35 e 97 della Costituzione.
 1.1.- Il giudizio principale ha ad oggetto il ricorso promosso da R.A., maresciallo capo dell'Arma dei Carabinieri, contro il decreto del Ministero della difesa - Direzione generale per il personale militare del 3 febbraio 2012, con il quale viene disposta, a decorrere dal 10 gennaio 2012, la perdita del grado ai sensi degli artt. 866, comma 1, e 867, comma 5, del d.lgs. n. 66 del 2010, e, per l'effetto, la cessazione dal servizio permanente del ricorrente, con conseguente iscrizione nel ruolo dei militari di truppa dell'esercito italiano. Il ricorrente ricorda che il Tribunale di Roma, con la decisione del 3 ottobre 2006, n. 17364, lo ha riconosciuto colpevole del reato di peculato continuato, condannandolo alla pena di due anni e quattro mesi di reclusione, nonché alla pena accessoria della interdizione temporanea dai pubblici uffici. La sentenza è stata confermata in appello ed è divenuta definitiva a seguito della sentenza della Corte di cassazione del 16 gennaio 2012.
 2.- Il rimettente ravvisa la rilevanza della questione di legittimità costituzionale dell'art. 866, comma 1, del d.lgs. n. 66 del 2010, in quanto soltanto un suo eventuale accoglimento, comportante la conseguente caducazione della norma sottoposta al vaglio di costituzionalità, consentirebbe l'annullamento del decreto impugnato.
 Il giudice a quo richiama l'attenzione in ordine alla illegittimità costituzionale, più volte affermata dalla Corte costituzionale, di una norma che preveda una automatica cessazione del rapporto di pubblico impiego nel caso della emanazione di una sentenza irrevocabile di condanna senza che sia possibile valutare da parte dell'amministrazione procedente la gravità del reato commesso, la sua rilevanza rispetto all'attività svolta in concreto dal dipendente, il vantaggio che possa derivare per l'amministrazione dall'eventuale mantenimento in servizio dello stesso.
 Quanto alla non manifesta infondatezza, il Tribunale rimettente osserva che la destituzione automatica mal si concilia con l'esigenza di tutela del diritto al lavoro, violando gli artt. 4 e 35 della Costituzione. A parere del TAR deve trovare applicazione, anche nei confronti del pubblico dipendente che presta servizio nelle forze armate, l'art. 9 della legge 7 febbraio 1990, n. 19 (Modifiche in tema di circostanze, sospensione condizionale della pena e destituzione dei pubblici dipendenti), in base al quale «1. Il pubblico dipendente non può essere destituito di diritto a seguito di condanna penale. È abrogata ogni contraria disposizione di legge. 2. La destituzione può essere sempre inflitta all'esito del procedimento disciplinare [...]». Per cui l'estinzione del rapporto di lavoro o di impiego, anche nel caso di condanna penale irrevocabile, potrebbe essere pronunciata solo a seguito di un procedimento disciplinare nel rispetto delle garanzie del diritto di difesa del pubblico dipendente.
 3.- Il Presidente del Consiglio dei ministri non è intervenuto in giudizio.Considerato in diritto1.- Il Tribunale amministrativo regionale per il Lazio ha sollevato questione di legittimità costituzionale dell'articolo 866, comma 1, del decreto legislativo 15 marzo 2010, n. 66 (Codice dell'ordinamento militare), nella parte in cui prevede che la «perdita del grado, senza giudizio disciplinare, consegue a condanna definitiva, non condizionalmente sospesa, per reato militare o delitto non colposo che comporti la pena accessoria della rimozione o della interdizione temporanea dai pubblici uffici», per violazione degli artt. 3, 4, 35 e 97 della Costituzione.
 2. - La questione è inammissibile, per insufficiente determinazione dei termini del giudizio e carenza di motivazione.
 Il giudice rimettente si limita ad osservare che la disposizione impugnata «reintroduce» un'ipotesi di estinzione automatica del rapporto di lavoro a seguito di condanna penale, senza previo giudizio disciplinare, in contrasto con il principio generale contenuto nell'art. 9 della legge 7 febbraio 1990, n. 19 (Modifiche in tema di circostanze, sospensione condizionale della pena e destituzione dei pubblici dipendenti), il quale afferma che «1. Il pubblico dipendente non può essere destituito di diritto a seguito di condanna penale. 2. È abrogata ogni contraria disposizione di legge. La destituzione può essere sempre inflitta all'esito del procedimento disciplinare [...]».
 Il TAR sembra, pertanto, voler instaurare un giudizio di legittimità costituzionale basato sulla violazione del principio di eguaglianza, ma la comparazione tra la disposizione dell'ordinamento militare impugnata e il principio generale applicabile al pubblico impiego, richiamato come tertium comparationis, non può essere correttamente effettuata in mancanza di alcuni elementi essenziali, che l'ordinanza di rimessione omette di illustrare.
 Il giudice rimettente, infatti, non specifica quali siano gli effetti della perdita del grado per gli appartenenti ai ruoli dell'Arma dei Carabinieri, né chiarisce in che misura essa possa essere assimilata alla destituzione del pubblico impiegato. D'altra parte, sarebbe stato necessario specificare le ragioni per cui il principio generale dettato per il pubblico impiego sarebbe di per sé applicabile anche agli appartenenti ai ruoli dell'Arma dei Carabinieri, senza che assuma rilievo la peculiarità delle funzioni dell'Arma stessa, che attengono alla pubblica sicurezza e all'ordine pubblico.
 Inoltre, a sostegno della prospettata questione di legittimità costituzionale, il TAR ha genericamente richiamato il disfavore mostrato dalla Corte costituzionale per le disposizioni di legge che comportano l'automatica cessazione del rapporto di pubblico impiego in seguito a condanna penale, senza tenere conto della più recente evoluzione normativa in materia di reati contro la pubblica amministrazione, disposta a partire dalla legge 27 marzo 2001, n. 97 (Norme sul rapporto tra procedimento penale e procedimento disciplinare ed effetti del giudicato penale nei confronti dei dipendenti delle amministrazioni pubbliche), successivamente modificata. Negli anni più recenti, il legislatore ha inasprito le pene per tale tipo di reati, tra cui quello di peculato che viene in rilievo nel caso di specie, oltre a rendere più severe le sanzioni accessorie relative all'interdizione dai pubblici uffici e ad incidere altresì, sempre nel senso di un maggior rigore, sulle conseguenze relative al rapporto di lavoro con la pubblica amministrazione, anche attraverso la previsione di ipotesi di estinzione automatica a seguito di condanna in sede penale, come risulta dal nuovo testo dell'art. 32-quinquies del codice penale. La disposizione impugnata deve essere necessariamente valutata in riferimento a tale contesto normativo, tanto più che essa è contenuta nel nuovo ordinamento militare, di cui al già citato d.lgs. n. 66 del 2010, ed è quindi coeva a questa linea legislativa particolarmente severa nei confronti dei reati contro la pubblica amministrazione.
 Alla luce di questa evoluzione normativa, il generico riferimento dell'ordinanza di rimessione al principio generale in tema di destituzione del pubblico impiegato, contenuto nella legge n. 19 del 1990, e alla giurisprudenza costituzionale che intorno ad esso si è sviluppata, è del tutto insufficiente per una corretta prospettazione della questione di legittimità costituzionale portata all'esame della Corte.
 Infine, il richiamo ai parametri di cui agli artt. 4 e 35 Cost. è privo di motivazione.
 Il mancato esame da parte del remittente dei profili sopra indicati e l'incompleta ricostruzione del quadro normativo di riferimento compromettono l'iter logico argomentativo posto a fondamento della sollevata censura e ne precludono lo scrutinio (ex plurimis ordinanze n. 174 del 2012, n. 50 del 2011 e sentenza n. 356 del 2010).
 Ne deriva l'inammissibilità della questione di legittimità costituzionale avente ad oggetto l'art. 866, comma 1, del d.lgs. n. 66 del 2010.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara inammissibile la questione di legittimità costituzionale dell'articolo 866, comma 1, del decreto legislativo 15 marzo 2010, n. 66 (Codice dell'ordinamento militare), sollevata, in riferimento agli articoli 3, 4, 35, 97 della Costituzione, dal Tribunale amministrativo per il Lazio con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 18 novembre 2013.&#13;
 F.to:&#13;
 Gaetano SILVESTRI, Presidente&#13;
 Marta CARTABIA, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 20 novembre 2013.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
