<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/240/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/240/"/>
          <FRBRalias value="ECLI:IT:COST:2011:240" name="ECLI"/>
          <FRBRdate date="19/07/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="240"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/240/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/240/ita@/!main"/>
          <FRBRdate date="19/07/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/240/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/240/ita@.xml"/>
          <FRBRdate date="19/07/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="22/07/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>QUARANTA</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>19/07/2011</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alfonso QUARANTA; Giudici : Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 630 del codice penale, promosso dal Giudice dell'udienza preliminare del Tribunale di Venezia nel procedimento penale a carico di O.O.H. ed altri, con ordinanza del 13 dicembre 2010, iscritta al n. 39 del registro ordinanze 2011 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 11, prima serie speciale, dell'anno 2011.
 Visti l'atto di costituzione di O.O.H., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 5 luglio 2011 il Giudice relatore Giuseppe Frigo;
 	uditi l'avvocato Fabio Pinelli per O.O.H. e l'avvocato dello Stato Massimo Giannuzzi per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che, con ordinanza del 13 dicembre 2010, il Giudice dell'udienza preliminare del Tribunale di Venezia ha sollevato, in riferimento agli artt. 3 e 27, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 630 del codice penale, nella parte in cui non prevede, in relazione al delitto di sequestro di persona a scopo di estorsione, la circostanza attenuante delineata dall'art. 3 della legge 26 novembre 1985, n. 718 (Ratifica ed esecuzione della convenzione internazionale contro la cattura degli ostaggi, aperta alla firma a New York il 18 dicembre 1979), in forza della quale «se il fatto è di lieve entità si applicano le pene previste dall'art. 605 del codice penale aumentate dalla metà ai due terzi»; 
 che il giudice a quo riferisce di essere investito del giudizio abbreviato nei confronti di tre cittadini nigeriani, imputati del reato previsto dalla norma censurata, per avere privato della libertà personale un loro connazionale, allo scopo di ottenere, come prezzo della liberazione, il versamento della somma di 2.100 euro;
 che, al riguardo, il rimettente rileva come il delitto di sequestro di persona a scopo di estorsione fosse originariamente punito dall'art. 630 cod. pen., nella sua forma semplice, con la reclusione da otto a quindici anni, oltre la multa;
 che il decreto-legge 21 marzo 1978, n. 59 (Norme penali e processuali per la prevenzione e la repressione di gravi reati), convertito, con modificazioni, dalla legge 18 maggio 1978, n. 191, e indi la legge 30 dicembre 1980, n. 894 (Modifiche all'articolo 630 del codice penale), nel sostituire la norma incriminatrice, hanno, peraltro, energicamente inasprito la risposta punitiva, stabilendola nella reclusione da venticinque a trenta anni;
 che l'aumento della pena edittale rispondeva all'esigenza di contrastare il «dilagante fenomeno», manifestatosi in quegli anni, dei sequestri di persona a scopo di estorsione posti in essere da spietate organizzazioni criminali, le quali chiedevano riscatti elevatissimi e sottoponevano le vittime a trattamenti inumani, spesso sopprimendole anche dopo il pagamento della somma richiesta; 
 che in rapporto a tali fatti - nonostante il successivo smantellamento dell'«industria dei sequestri» - la pena edittale in questione apparirebbe tuttora adeguata e ragionevole;
 che nella fattispecie descritta dall'art. 630 cod. pen. rientrerebbero, tuttavia, anche episodi di ben minore gravità, come attesterebbe la vicenda oggetto del giudizio a quo;
 che, nel caso di specie, infatti - secondo quanto emergerebbe dalle risultanze processuali - il sequestrato era stato affrontato nottetempo da alcuni connazionali (parte dei quali successivamente identificati negli imputati), che, dopo averlo malmenato, lo avevano costretto a salire su un'autovettura e condotto in un appartamento;
 che i sequestratori avevano, quindi, ripetutamente richiesto o fatto richiedere dal sequestrato, tramite telefono cellulare, a una cognata dello stesso sequestrato la consegna della somma di 2.100 euro, quale condizione per la sua liberazione: somma costituente il corrispettivo di una cessione di sostanza stupefacente, in precedenza effettuata a un amico della persona offesa;
 che il sequestrato era stato trattenuto presso l'abitazione dalle ore 2 alle ore 21 del 27 dicembre 2009, allorché, con la scusa di aver bisogno di mangiare, aveva indotto i sequestratori ad accompagnarlo in un bar presso la stazione ferroviaria, dove era stato liberato dalla polizia, giunta sul posto a seguito delle intercettazioni telefoniche nel frattempo operate;
 che, ad avviso del rimettente, nel fatto ora descritto sarebbero ravvisabili gli estremi del contestato delitto di sequestro di persona a scopo di estorsione;
 che non potrebbe, infatti, aderirsi all'indirizzo giurisprudenziale, ispirato da ragioni sostanzialmente equitative, secondo il quale, in situazioni quali quella considerata, non si configurerebbe la fattispecie criminosa unitaria di cui all'art. 630 cod. pen., ma il concorso tra i reati semplici di sequestro di persona e di estorsione (artt. 605 e 629 cod. pen.);
 che la validità di tale soluzione è stata, infatti, esclusa dalle Sezioni unite della Corte di cassazione, con pronuncia che il rimettente reputa giuridicamente ineccepibile (sentenza 17 dicembre 2003-20 gennaio 2004, n. 962), la quale ha, tra l'altro, chiarito che la finalità di ingiusto profitto, richiesta per la configurabilità del delitto di cui all'art. 630 cod. pen., ricorre anche quando l'agente miri a conseguire, come prezzo per la liberazione del sequestrato, una prestazione patrimoniale pretesa in esecuzione di un precedente rapporto illecito;
 che il fatto oggetto di giudizio sarebbe, tuttavia, «sicuramente lieve», tenuto conto della breve durata della privazione della libertà personale del sequestrato (meno di ventiquattro ore), dell'estemporaneità dell'azione criminosa (scaturita da un incontro fortuito e non preceduta da una qualche organizzazione di mezzi), dell'assenza di maltrattamenti della vittima (dopo l'aggressione iniziale) e della modestia della somma pretesa come riscatto (somma che gli imputati consideravano, peraltro, loro dovuta, sia pure per «affari illeciti»);
 che rispetto a un simile episodio, ben lontano dai modelli avuti di mira dal legislatore con le novelle degli anni 1978-1980, la pena minima di venticinque anni di reclusione - superiore alla durata massima della reclusione stabilita dall'art. 23 cod. pen. (ventiquattro anni) e addirittura più severa di quella prevista per l'omicidio - si rivelerebbe manifestamente sproporzionata per eccesso;
 che, nell'introdurre norme incriminatrici che comminano pene elevate nel minimo, il legislatore si sarebbe, in effetti, sempre premurato di prevedere specifiche circostanze attenuanti per i fatti di lieve o minore entità, che consentano di adeguare la pena alla reale gravità del fatto concreto: quali, ad esempio, quelle delineate nell'art. 609-bis, terzo comma, cod. pen., quanto al delitto di violenza sessuale; nell'art. 73, comma 5, del d.P.R. 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza), per i reati in materia di stupefacenti; nell'art. 5 della legge 2 ottobre 1967, n. 895 (Disposizioni per il controllo delle armi), relativamente ai reati in tema di armi; o nell'art. 311 cod. pen., in rapporto ai delitti contro la personalità dello Stato, ivi compreso quello di sequestro di persona a scopo di terrorismo o di eversione (art. 289-bis cod. pen.);
 che una circostanza attenuante di tal fatta è prevista anche dall'art. 3 della legge n. 718 del 1985, in relazione a una fattispecie criminosa analoga - secondo il rimettente - a quella considerata;
 che la disposizione ora citata punisce, infatti, con la reclusione da venticinque a trenta anni «chiunque, fuori dei casi indicati dagli articoli 289-bis e 630 del codice penale, sequestra una persona o la tiene in suo potere minacciando di ucciderla, di ferirla o di continuare a tenerla sequestrata al fine di costringere un terzo, sia questi uno Stato, una organizzazione internazionale tra più governi, una persona fisica o giuridica od una collettività di persone fisiche, a compiere un qualsiasi atto o ad astenersene, subordinando la liberazione della persona sequestrata a tale azione od omissione»;
 che il medesimo art. 3 della legge n. 718 del 1985 stabilisce, peraltro, al terzo comma, che «se il fatto è di lieve entità si applicano le pene previste dall'art. 605 del codice penale aumentate dalla metà a due terzi»;
 che, omettendo di prevedere una circostanza attenuante di analogo tenore, la norma denunciata si porrebbe, quindi, in contrasto con i principi di eguaglianza e di ragionevolezza (art. 3 Cost.), sanzionando con la medesima elevatissima pena episodi criminosi di gravità assai diversa, e comprometterebbe, altresì, la finalità rieducativa della pena (art. 27, terzo comma, Cost.), la quale esige che il trattamento sanzionatorio sia proporzionato all'effettivo disvalore del fatto;
 che la questione sarebbe rilevante nel giudizio a quo, giacché il suo accoglimento consentirebbe, tramite l'applicazione dell'auspicata attenuante, di adeguare la pena inflitta agli imputati alla reale gravità del fatto loro ascritto;
 che, nel giudizio di costituzionalità, è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile per difetto di motivazione sulla rilevanza e per il suo carattere di mera critica a scelte di politica criminale, ovvero, in subordine, infondata;
 che si è costituito, altresì, uno degli imputati nel giudizio a quo, il quale - sulla scorta di articolate considerazioni - ha chiesto che l'art. 630 cod. pen. sia dichiarato costituzionalmente illegittimo nella parte in cui non prevede la circostanza attenuante indicata dal rimettente, «ovvero [...] altra circostanza ad effetto comune per i fatti di lieve entità».
 Considerato che il Giudice dell'udienza preliminare del Tribunale di Venezia denuncia come contraria agli artt. 3 e 27, terzo comma, della Costituzione la mancata estensione al delitto di sequestro di persona a scopo di estorsione della circostanza attenuante prevista dall'art. 3, terzo comma, della legge 26 novembre 1985, n. 718, in rapporto al reato - in assunto analogo - di cosiddetto sequestro di ostaggi: attenuante in forza della quale «se il fatto è di lieve entità si applicano le pene previste dall'art. 605 del codice penale aumentate dalla metà ai due terzi»;
 che l'eccezione di inammissibilità della questione per difetto di motivazione sulla rilevanza, formulata dall'Avvocatura generale dello Stato, è infondata;
 che il rimettente deduce, infatti, in modo argomentato che, alla luce delle peculiarità del fatto oggetto del giudizio a quo, gli imputati potrebbero fruire - in caso di accoglimento della questione - dell'attenuante dianzi indicata: attenuante che comporterebbe, d'altro canto, una riduzione della pena di gran lunga superiore a quella che potrebbe eventualmente derivare dalle attenuanti comuni di cui agli artt. 62, numeri 4), 5) e 6), 62-bis e 114, primo comma, cod. pen., la cui applicabilità - secondo la difesa dello Stato - il giudice a quo avrebbe omesso di valutare;
 che parimenti infondata è l'ulteriore eccezione di inammissibilità, formulata dall'Avvocatura dello Stato sull'assunto che la questione si risolverebbe in una mera critica a scelte di politica criminale: trattandosi, al contrario, della deduzione di una violazione dei principi di eguaglianza e di ragionevolezza e della finalità rieducativa della pena basata su un ben preciso tertium comparationis;
 	che, nel merito, il giudice a quo - denunciando la violazione dei principi di eguaglianza e di ragionevolezza, nonché della funzione rieducativa della pena - ravvisa la necessità costituzionale di omologare, in parte qua, il trattamento sanzionatorio del sequestro di persona a scopo di estorsione a quello del cosiddetto sequestro di ostaggi;
 che questa Corte ha già avuto modo, peraltro, di scrutinare una questione di legittimità costituzionale parzialmente analoga a quella odierna, volta a censurare, in riferimento ai medesimi parametri (oltre che all'art. 27, primo comma, Cost.), l'art. 630 cod. pen. nella parte in cui stabilisce la pena minima di «anni venticinque di reclusione in difetto di circostanza attenuante speciale per i fatti di minore entità o gravità»;
 che, nel dichiarare la questione manifestamente inammissibile per oscurità e indeterminatezza del petitum - vizio non riscontrabile nel caso oggi in esame - la Corte ha incidentalmente rilevato come la disciplina del sequestro di ostaggi - anche in quell'occasione evocata dal giudice rimettente - non costituisca comunque un tertium comparationis idoneo a dimostrare il vulnus costituzionale denunciato, trattandosi di fattispecie criminosa «più ampia e generica rispetto al delitto di cui all'art. 630 cod. pen.» (ordinanza n. 163 del 2007);
 che tale carattere di maggiore comprensività - attestato anche dalla clausola di sussidiarietà espressa con cui la norma incriminatrice del sequestro di ostaggi esordisce («fuori dei casi indicati negli articoli 289-bis e 630 del codice penale») - si coglie segnatamente in rapporto all'obiettivo della condotta, identificato nel generico «fine di costringere un terzo [...] a compiere un qualsiasi atto o ad astenersene»: finalità rispetto alla quale lo scopo di conseguire un ingiusto profitto, previsto dall'art. 630 cod. pen., rappresenta una species negativamente connotata;
 che, per questo verso, se la cattura di ostaggi può manifestarsi in episodi di maggiore gravità rispetto al sequestro di persona a scopo di estorsione (la prestazione richiesta per la liberazione dell'ostaggio potrebbe consistere, infatti, nel compimento di atti governativi o politici molto più rilevanti del pagamento di un riscatto), essa si presta, tuttavia, a qualificare penalmente anche sequestri di persona effettuati a scopo "dimostrativo" o a sostegno di rivendicazioni sociali, etiche o politiche (persino "nobili", da un punto di vista astratto);
 che proprio in tale prospettiva si giustifica - nella valutazione del legislatore - la previsione di una attenuante ad effetto speciale, quale quella del terzo comma dell'art. 3 della legge n. 718 del 1985, grazie alla cui applicazione la pena minima per il delitto in questione - parificata, quanto all'ipotesi base, a quella del sequestro di persona a scopo di estorsione - può scendere a soli nove mesi di reclusione;
 che l'accoglimento del petitum del giudice rimettente provocherebbe, d'altra parte, una sperequazione di segno contrario a quella denunciata: a seguito di esso, infatti, la pena minima applicabile per il sequestro di persona a scopo di estorsione finirebbe per risultare sensibilmente inferiore a quella irrogabile, ai sensi degli artt. 56, terzo comma, e 629 cod. pen., per l'estorsione, anche solo tentata, attuata con modalità diverse e meno pregne di disvalore rispetto alla privazione dell'altrui libertà personale;
 che non può essere presa in considerazione, in questa sede, la richiesta della parte privata di assumere come termine di confronto, nello scrutinio di costituzionalità, anche il delitto di sequestro di persona a scopo di terrorismo o di eversione (art. 289-bis cod. pen.): ciò, nella prospettiva di estendere al reato che interessa - in luogo dell'attenuante ad effetto speciale indicata dal rimettente - l'attenuante ad effetto comune di cui all'art. 311 cod. pen., applicabile alla generalità dei delitti contro la personalità dello Stato e, dunque, anche alla predetta fattispecie criminosa;
 che una simile operazione comporterebbe, infatti - come rilevato anche dall'Avvocatura dello Stato - una sostanziale modifica dei termini della questione, quali risultanti dalle censure e dalle richieste formulate nell'ordinanza di rimessione, le quali, per costante giurisprudenza di questa Corte, segnano i limiti del giudizio di legittimità costituzionale in via incidentale (ex plurimis, nel senso che non possano essere presi in considerazione ulteriori profili di illegittimità costituzionale dedotti dalle parti, volti ad ampliare o modificare il thema decidendum fissato dall'ordinanza di rimessione, sentenze n. 56 del 2009 e n. 86 del 2008);
 che non potrebbe essere valorizzato, in senso contrario, il richiamo all'art. 311 cod. pen., pure rinvenibile nella motivazione della suddetta ordinanza: esso è contenuto, infatti, nell'ambito di un elenco di riferimenti esemplificativi a norme che prevedono attenuanti, variamente articolate, per i fatti di minore o lieve entità (art. 609-bis, terzo comma, cod. pen.; art. 73, comma 5, del d.P.R. n. 309 del 1990; art. 5 della legge n. 895 del 1967); il che non toglie che il termine di comparazione specificamente evocato dal rimettente resti l'art. 3 della legge n. 718 del 1985 e che il petitum sia calibrato esclusivamente su tale fattispecie;
 che - impregiudicato, quindi, il diverso tema prospettato dalla parte privata - la questione va dichiarata, alla luce delle considerazioni che precedono, manifestamente infondata.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 630 del codice penale, sollevata, in riferimento agli artt. 3 e 27, terzo comma, della Costituzione, dal Giudice dell'udienza preliminare del Tribunale di Venezia con l'ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 luglio 2011.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 22 luglio 2011.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
