<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/91/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/91/"/>
          <FRBRalias value="ECLI:IT:COST:2016:91" name="ECLI"/>
          <FRBRdate date="23/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="91"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/91/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/91/ita@/!main"/>
          <FRBRdate date="23/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/91/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/91/ita@.xml"/>
          <FRBRdate date="23/03/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="22/04/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Giuliano Amato</cc:relatore_pronuncia>
        <cc:data_decisione>23/03/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Giuseppe FRIGO, Alessandro CRISCUOLO, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito degli artt. 1, 2, 3, 4, 5, 6 e 6-bis del Regolamento per la tutela giurisdizionale dei dipendenti, approvato con deliberazione dell'Ufficio di Presidenza della Camera dei deputati 28 aprile 1988, modificato dalla deliberazione dell'Ufficio di Presidenza della Camera dei deputati 6 ottobre 2009, n. 77, promosso dal Tribunale ordinario di Roma, sezione seconda lavoro, con ordinanza-ricorso depositata in cancelleria il 18 dicembre 2015 ed iscritta al n. 4 del registro conflitti tra poteri dello Stato 2015, fase di ammissibilità.
 Udito nella camera di consiglio del 23 marzo 2016 il Giudice relatore Giuliano Amato.
 Ritenuto che il Tribunale ordinario di Roma, sezione seconda lavoro, con ordinanza del 26 ottobre 2015, ha promosso conflitto di attribuzione tra poteri dello Stato nei confronti della Camera dei deputati, in relazione alla deliberazione degli articoli da 1 a 6-bis del Regolamento per la tutela giurisdizionale dei dipendenti 28 aprile 1988, secondo il testo coordinato con le modifiche approvate dall'Ufficio di Presidenza con deliberazione 6 ottobre 2009, n. 77, resa esecutiva con decreto del Presidente della Camera dei deputati 15 ottobre 2009, n. 781;
 che i richiamati articoli disciplinano la costituzione degli organi giurisdizionali interni di primo e secondo grado ed il procedimento dinanzi ad essi;
 che tali disposizioni vengono contestate nella parte in cui, violando gli artt. 3, primo comma, 24, primo comma, 102, secondo comma, quest'ultimo in combinato disposto con la VI disposizione transitoria, 108, secondo comma, e 111, primo e secondo comma, della Costituzione, precludono ai dipendenti l'accesso alla tutela giurisdizionale in riferimento alle controversie di lavoro insorte con la Camera dei deputati;
 che il Tribunale ordinario di Roma premette di essere investito della decisione in ordine al ricorso proposto da 175 dipendenti della Camera dei deputati, al fine di ottenere l'accertamento dell'illiceità o dell'illegittimità del comportamento dell'amministrazione che ha introdotto limiti alle progressioni di carriera, oltreché della nullità o dell'illegittimità sia della delibera n. 102 del 2014 dell'Ufficio di Presidenza, con cui sono state approvate le disposizioni volte a introdurre tali limiti, sia del decreto n. 824 del 2014 della Presidente della Camera dei deputati, che ha reso esecutiva tale delibera;
 che al Tribunale ordinario di Roma vengono altresì richieste la disapplicazione di entrambi i richiamati atti, la condanna della Camera dei deputati all'esatto adempimento delle obbligazioni contrattuali assunte, ai sensi dell'art. 1372 del codice civile, nonché ogni altro provvedimento che si renda necessario, ai sensi dell'art. 63, comma 2, del decreto legislativo 30 marzo 2001, n. 165 (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche), per assicurare il pieno rispetto delle posizioni soggettive dei dipendenti;
 che il Tribunale ordinario di Roma riferisce che i ricorrenti hanno ritenuto sussistere la competenza del giudice ordinario, e, in particolare, del giudice del lavoro, ai sensi dell'art. 409, quinto comma, del codice di procedura civile; essi ritengono che, da un lato, l'art. l del Regolamento per la tutela giurisdizionale dei dipendenti della Camera dei deputati preveda una mera facoltà di adire gli organi giurisdizionali interni per la tutela di diritti ed interessi legittimi dei lavoratori; dall'altro che, sulla scorta degli argomenti esposti dalla Corte di cassazione, sezioni unite civili, nell'ordinanza 6 maggio 2013, n. 10400, nonché dalla Corte costituzionale nella sentenza n. 120 del 2014, sia da escludere la competenza della Camera dei deputati ad adottare provvedimenti giurisdizionali nella materia dei rapporti di lavoro con i propri dipendenti;
 che la Camera dei deputati, invece, ha sollevato eccezione di difetto di giurisdizione, affermando che i propri organi giurisdizionali hanno competenza esclusiva in relazione alle controversie insorte con i dipendenti;
 che è stato richiamato, in proposito, l'orientamento della Corte costituzionale (sentenza n. 154 del 1985) e della Corte di cassazione (sezioni unite civili, sentenze 27 maggio 1999, n. 317 e 10 giugno 2004, n. 11019), secondo cui i regolamenti parlamentari, sui quali si fonda l'autodichia, sono fonti normative di rango primario e dunque sostanzialmente parificate alle leggi ordinarie, in quanto dispiegano la loro efficacia nella sfera di azione interna alle assemblee legislative, riservata alla loro autonomia per ragioni di garanzia dell'indipendenza delle assemblee stesse;
 che il Tribunale ordinario di Roma richiama, altresì, l'ordinanza 19 dicembre 2014, n. 26934, con cui le sezioni unite civili della Corte di cassazione hanno sollevato conflitto di attribuzione tra i poteri dello Stato, chiedendo dichiararsi che non spettava al Senato della Repubblica deliberare gli articoli da 72 a 84 del titolo 2° (Contenzioso) del Testo unico delle norme regolamentari dell'Amministrazione riguardanti il personale del Senato della Repubblica;
 che, ad avviso del ricorrente, le ampie e condivisibili argomentazioni esposte dalla Corte di cassazione a sostegno della citata ordinanza sarebbero sostanzialmente sovrapponibili alla fattispecie al suo esame;
 che anche per la Camera dei deputati, infatti, non sarebbe dubitabile che nell'attuale assetto ordinamentale gli organi giurisdizionali interni abbiano competenza esclusiva sulle controversie affidate alla loro cognizione;
 che l'argomento letterale in senso contrario addotto dai ricorrenti, secondo cui la formulazione dell'art. l del Regolamento per la tutela giurisdizionale prevederebbe soltanto una mera facoltà per ciascun dipendente di adire l'organo giurisdizionale, ove ritenga lesi i propri diritti o interessi legittimi, sarebbe alquanto labile;
 che, infatti, nell'interpretazione ormai da lungo consolidata, gli organi di giustizia costituiti all'interno delle Camere hanno non solo natura giurisdizionale, ma anche competenza esclusiva nelle materie loro riservate (sentenze n. 154 del 1985 e n. 120 del 2014; nonché Corte di cassazione, sezioni unite civili, sentenza n. 317 del 1999);
 che, pertanto, nel presente giudizio non sarebbe possibile procedere all'esame della controversia nel merito, giacché la competenza giurisdizionale spetterebbe, in via esclusiva, agli organi giurisdizionali interni della Camera dei deputati;
 che l'autodichia della Camera dei deputati ha fondamento nell'art. 12, comma 3, lettere d) ed f), del suo Regolamento, ai sensi del quale sono emanati i regolamenti subprimari, tra i quali quello sulla tutela giurisdizionale;
 che, dunque, competente a decidere in primo grado sui ricorsi presentati dai dipendenti della Camera dei deputati è la Commissione giurisdizionale per il personale;
 che di tale Commissione il regolamento sulla tutela giurisdizionale disciplina composizione e modalità di formazione (art. 3); procedimento (art. 4); modalità di decisione (art. 5); impugnazione delle sentenze (art. 6);
 che tale complesso di disposizioni, ad avviso del Tribunale, costituisce un sistema del tutto autonomo ed interno per la risoluzione delle controversie insorte con il personale dipendente, al punto da non consentire non solo il ricorso al giudice, ma neppure il controllo generale di legittimità che la Costituzione affida alla Corte di cassazione, ai sensi dell'art. 111, settimo comma, Cost.;
 che, il Tribunale ordinario di Roma richiama, in particolare, l'ordinanza della Corte di cassazione, sezioni unite civili, 19 dicembre 2014, n. 26934, evidenziando che la disciplina della competenza giurisdizionale della Camera dei deputati presenterebbe i medesimi profili di illegittimità che hanno giustificato la proposizione del conflitto di attribuzione, da parte della Corte di cassazione, nei confronti del Senato della Repubblica; 
 che anche l'autodichia della Camera dei deputati, infatti, sarebbe in contrasto con il principio di eguaglianza (art. 3, primo comma, Cost.), di cui è espressione il diritto di ognuno di agire in giudizio per la tutela dei propri diritti e interessi legittimi (art. 24, primo comma, Cost.);
 che, pertanto, non essendo possibile un'interpretazione delle richiamate disposizioni subregolamentari tale da fugare ogni dubbio circa il contrasto con principi fondamentali dell'ordinamento costituzionale, posto che il sistema giurisdizionale della Camera dei deputati esclude, per unanime interpretazione, ogni possibilità di ricorso all'autorità giudiziaria (ordinaria o amministrativa), il Tribunale ritiene necessario sollevare conflitto di attribuzione nei confronti della Camera dei deputati;
 che solo la Corte costituzionale, infatti, può valutare se la disciplina sulle controversie dei dipendenti della Camera dei deputati sia effettivamente in contrasto con gli artt. 3, 24, l02, quest'ultimo in combinato disposto con la VI disposizione transitoria, 108, secondo comma, e 111, primo e secondo comma, Cost.;
 che, secondo il Tribunale, la sussistenza del potere esclusivo degli organi interni della Camera dei deputati determina la compressione o l'impedimento del potere giurisdizionale del giudice ordinario; 
 che, pertanto, sarebbe evidente l'interesse a ricorrere a questa Corte, dovendo il giudice adito dare una risposta di giustizia agli attuali ricorrenti, mentre ciò gli è precluso dall'esistenza delle richiamate norme del Regolamento per la tutela giurisdizionale dei dipendenti;
 che, quanto al requisito soggettivo, la natura di potere dello Stato di ogni giudice dell'ordinamento giudiziario è stata più volte riconosciuta (ex multis, ordinanza n. 286 del 2014);
 che, dunque, il Tribunale ordinario di Roma chiede - previa dichiarazione dell'ammissibilità del conflitto - che la Corte costituzionale dichiari che non spettava alla Camera dei deputati deliberare gli articoli da l a 6-bis del Regolamento per la tutela giurisdizionale dei dipendenti, nella parte in cui, violando gli artt. 3, primo comma, 24, primo comma, 102, secondo comma, quest'ultimo in combinato disposto con la VI disposizione transitoria, l08, secondo comma, e 111, primo e secondo comma, Cost., precludono l'accesso dei dipendenti della Camera dei deputati alla tutela giurisdizionale in riferimento alle controversie di lavoro insorte con la Camera stessa.
 Considerato che il Tribunale ordinario di Roma, sezione seconda lavoro, con ordinanza del 26 ottobre 2015, ha promosso conflitto di attribuzione tra poteri dello Stato nei confronti della Camera dei deputati, in relazione alla deliberazione degli articoli da 1 a 6-bis del Regolamento per la tutela giurisdizionale dei dipendenti 28 aprile 1988, secondo il testo coordinato con le modifiche approvate dall'Ufficio di Presidenza con deliberazione 6 ottobre 2009, n. 77, resa esecutiva con decreto del Presidente della Camera dei deputati 15 ottobre 2009, n. 781;
 che le disposizioni censurate disciplinano la costituzione degli organi giurisdizionali interni di primo e secondo grado ed il procedimento dinanzi ad essi; 
 che tali disposizioni vengono contestate nella parte in cui, con l'asserita violazione degli artt. 3, primo comma, 24, primo comma, 102, secondo comma, quest'ultimo in combinato disposto con la VI disposizione transitoria, 108, secondo comma, e 111, primo e secondo comma, della Costituzione, precludono ai dipendenti l'accesso alla tutela giurisdizionale in riferimento alle controversie di lavoro insorte con la Camera dei deputati; 
 che, in questa fase del giudizio, questa Corte è chiamata, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), a deliberare, senza contradditorio, se il ricorso sia ammissibile in quanto vi sia «materia di un conflitto la cui risoluzione spetti alla sua competenza», sussistendone i requisiti soggettivo e oggettivo e restando impregiudicata ogni ulteriore questione anche in punto di ammissibilità;
 che, a tale fine, nel caso in esame non rileva la forma dell'ordinanza rivestita dall'atto introduttivo, bensì la sua rispondenza ai contenuti previsti dall'art. 37 della legge n. 87 del 1953 e dall'art. 24, comma 1, delle norme integrative per i giudizi davanti alla Corte costituzionale (ex plurimis, sentenza n. 315 del 2006; ordinanze n. 137 del 2015, n. 271 e n. 161 del 2014, n. 296 e n. 151 del 2013, n. 229 del 2012, n. 402 del 2006 e n. 129 del 2005);
 che neppure rileva la distanza di tempo dall'adozione dell'atto oggetto del conflitto, giacché questa Corte ha già avuto occasione di affermare che «non esiste alcun termine per sollevare i conflitti di attribuzione tra poteri, ed ha individuato la ratio di questa mancanza nell'esigenza - avvertita dal legislatore in ragione del livello precipuamente politico-costituzionale di tal genere di controversie - di favorirne al massimo la composizione, svincolandola dall'osservanza di termini di decadenza» (sentenza n. 58 del 2004; nello stesso senso, sentenza n. 116 del 2003; ordinanze n. 137 del 2015 e n. 61 del 2000);
 che, sotto il profilo del requisito soggettivo, va riconosciuta la legittimazione del Tribunale ordinario di Roma a sollevare conflitto di attribuzione tra poteri dello Stato, alla luce della consolidata giurisprudenza costituzionale secondo la quale «i singoli organi giurisdizionali, esplicando le loro funzioni in situazione di piena indipendenza, costituzionalmente garantita, sono da considerare legittimati - attivamente e passivamente - ad esser parti di conflitti di attribuzione» (ex plurimis, ordinanza n. 228 del 1975);
 che, parimenti, deve essere riconosciuta la legittimazione della Camera dei deputati ad essere parte del presente conflitto, quale organo competente a dichiarare in modo definitivo la volontà del potere cui appartiene (ex plurimis, ordinanza n. 37 del 1998);
 che, per quanto attiene al profilo oggettivo, il Tribunale ricorrente lamenta la lesione della propria sfera di attribuzioni, costituzionalmente garantita, in conseguenza della mancanza, per inesistenza dei relativi presupposti, del potere della Camera dei deputati di deliberare norme regolamentari che precludano l'accesso dei propri dipendenti alla tutela giurisdizionale in riferimento alle controversie di lavoro; 
 che, dunque, esiste la materia di un conflitto la cui risoluzione spetta alla competenza di questa Corte;
 che, ai sensi dell'art. 37, quarto comma, della legge n. 87 del 1953, va disposta la notificazione del ricorso e della presente ordinanza anche al Senato della Repubblica, stante l'identità della posizione costituzionale dei due rami del Parlamento in relazione alle questioni di principio da trattare (ordinanze n. 327, n. 241 e n. 104 del 2011, n. 211 del 2010, n. 8 del 2008, n. 186 e n. 185 del 2005, n. 178 del 2001, n. 102 del 2000 e n. 470 del 1995).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 1) dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), il conflitto di attribuzione tra poteri dello Stato, proposto dal Tribunale ordinario di Roma, sezione  seconda lavoro, nei confronti della Camera dei deputati, con l'ordinanza indicata in epigrafe;&#13;
 2) dispone:&#13;
 a) che la cancelleria di questa Corte dia immediata comunicazione della presente ordinanza al Tribunale ordinario di Roma, sezione seconda lavoro;&#13;
 b) che il ricorso e la presente ordinanza, siano notificati, a cura del ricorrente, alla Camera dei deputati e al Senato della Repubblica, in persona dei rispettivi Presidenti, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di trenta giorni dall'ultima notificazione, a norma dell'art. 24, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 marzo 2016.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Giuliano AMATO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 22 aprile 2016.&#13;
 Il Cancelliere&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
