<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/495/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/495/"/>
          <FRBRalias value="ECLI:IT:COST:2002:495" name="ECLI"/>
          <FRBRdate date="20/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="495"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/495/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/495/ita@/!main"/>
          <FRBRdate date="20/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/495/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/495/ita@.xml"/>
          <FRBRdate date="20/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="28/11/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Fernanda Contri</cc:relatore_pronuncia>
        <cc:data_decisione>20/11/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 4 della legge 29 dicembre 1987, n. 546 (Indennità di maternità per le lavoratrici autonome), promosso con ordinanza emessa il 22 febbraio 2002 dal Tribunale di Pinerolo nel procedimento civile vertente tra Furbatto Elena e l'INPS, iscritta al n. 162 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2002. 
    Visti l'atto di costituzione dell'INPS nonché l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nell'udienza pubblica dell'8 ottobre 2002 il Giudice relatore Fernanda Contri; 
    udito l'avvocato Franco Jeni per l'INPS,      nonché l'Avvocato dello Stato Massimo Mari per il Presidente del Consiglio dei ministri.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. - Il Tribunale di Pinerolo, con ordinanza emessa il 22 febbraio 2002, ha sollevato, in riferimento agli artt. 3, 31 e 37 della Costituzione, questione di legittimità costituzionale dell'art. 4 della legge 29 dicembre 1987, n. 546 (Indennità di maternità per le lavoratrici autonome), nella parte in cui limita la misura dell'indennizzo per maternità spettante alle lavoratrici autonome ai tre mesi successivi alla data effettiva del parto e non consente loro, in caso di parto prematuro, di ottenere un'ulteriore indennità, corrispondente al lasso di tempo che intercorre tra la data del parto effettivo e quella del parto presunto. 
    Il giudice a quo precisa anzitutto che, non potendosi applicare ratione temporis la nuova disciplina dettata dal decreto legislativo n. 151 del 26 marzo 2001 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'articolo 15 della legge 8 marzo 2000, n. 53), la fattispecie è ancora regolata dalla legge n. 546 del 1987, il cui art. 4 prevede la corresponsione dell'indennità per i due mesi antecedenti la data presunta del parto e per i tre mesi successivi alla data effettiva del parto; tale norma peraltro non è stata modificata dalla legge n. 53 del 2000 (Disposizioni per il sostegno della maternità e della paternità, per il diritto alla cura e alla formazione e per il coordinamento dei tempi delle città), che ha disciplinato le ipotesi di parti prematuri con esclusivo riferimento all'art. 4 della legge n. 1204 del 1971 (Tutela delle lavoratrici madri) e, quindi, alle sole lavoratrici subordinate. 
    Il rimettente, richiamando i principi espressi dalla Corte costituzionale in tema di tutela della maternità (sentenze n. 361 del 2000, n. 270 del 1999 e n. 181 del 1993), afferma che la previsione contenuta nell'art. 4 della legge n. 546 del 1987 sembra porsi in contrasto con gli artt. 3, 31 e 37 della Costituzione.  
    Ad avviso del giudice a quo, il legislatore, pur potendo differenziare la misura del sostegno economico disposto a favore delle lavoratrici madri in relazione alla natura del rapporto di lavoro, ha riconosciuto il diritto delle lavoratrici autonome a ricevere l'indennità di maternità per il medesimo periodo previsto per le lavoratrici subordinate; ciò nonostante ha poi irragionevolmente omesso di integrare la tutela in relazione alle ipotesi di parto prematuro, intervenendo solo a favore delle lavoratrici subordinate. La lacuna normativa per le ipotesi di parto prematuro, rilevata dalla Corte costituzionale nella sentenza n. 270 del 1999, è stata colmata dal legislatore solo per la categoria delle lavoratrici subordinate e non per quella delle lavoratrici autonome, in quanto il comma 4 dell'art. 4 della legge n. 1204 del 1971, introdotto dalla legge n. 53 del 2000, non ha modificato la disciplina prevista dalla legge n. 546 del 1987.  
    Il rimettente indica poi come tertium comparationis l'art. 68, comma 2, del d.lgs. n. 151 del 2001, il cui tenore letterale sembra attribuire alle lavoratrici autonome il diritto all'indennità di maternità per i due mesi antecedenti la data del parto, sia esso prematuro o a termine, e per i tre mesi successivi alla data effettiva del parto. 
    In merito alla dedotta violazione degli artt. 31 e 37 della Costituzione, il giudice a quo ricorda che nella citata sentenza n. 270 del 1999 è stata rilevata la irragionevolezza della rigidità della normativa con riferimento all'ipotesi del parto prematuro, contrastante con il principio della parità di trattamento, con il valore della protezione della famiglia e con quello della tutela del minore. Ad avviso del rimettente, il pregiudizio dei beni protetti dall'art. 31 della Costituzione, interpretato alla luce dei principi enunciati dall'art. 37 della Costituzione, non potrebbe comunque evitarsi, pur essendo la lavoratrice autonoma libera di astenersi dal lavoro anche nel periodo successivo al terzo mese dalla nascita, poiché l'adempimento da parte della donna della sua essenziale funzione familiare imporrebbe di assicurare alla predetta lavoratrice autonoma un adeguato sostegno economico, che disincentivi la precoce ripresa del lavoro a seguito di parto prematuro.  
    2. - Nel giudizio innanzi alla Corte si è costituito l'Istituto nazionale della previdenza sociale, chiedendo che la questione sia dichiarata infondata. 
    Secondo l'INPS, relativamente all'asserita violazione del principio di eguaglianza, non sussiste omogeneità fra le situazioni poste a confronto, poiché per le lavoratrici autonome non è previsto alcun obbligo di astensione dal lavoro, restando libere di proseguire l'attività lavorativa ovvero di sospenderla.  
    Sarebbe pertanto costituzionalmente legittima una diversa tutela delle due categorie di lavoratrici, che giustifica altresì una diversa imposizione contributiva. Anche perché la diversità di disciplina è stata ritenuta legittima dalla Corte costituzionale, che con la sentenza n. 181 del 1993 ha dichiarato inammissibile la questione relativa alla mancata estensione alle lavoratrici autonome del diritto all'indennità di maternità nell'ipotesi di astensione anticipata dal lavoro, essendo riservata alla discrezionalità del legislatore la previsione di una disciplina analoga a quella stabilita per le lavoratrici subordinate dall'art. 5 della legge n. 1204 del 1971. 
    Le stesse considerazioni dovrebbero valere per la questione in esame, che sarebbe infondata anche con riferimento all'art. 31 della Costituzione, non essendovi alcuna lesione dei principi connessi alla tutela della famiglia delle lavoratrici autonome. 
    La censura prospettata dal giudice a quo con riferimento all'art. 37 della Costituzione sarebbe poi inammissibile e infondata, per l'estraneità ai lavoratori autonomi dei precetti in esso contenuti, riguardanti esclusivamente i lavoratori subordinati.  
    3. - È intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la inammissibilità e comunque per l'infondatezza della questione. 
    Ad avviso dell'Avvocatura, benché la nuova disciplina contenuta nell'art. 68 del d. lgs. n. 151 del 2001 attesti la volontà del legislatore di migliorare la tutela della maternità delle lavoratrici autonome, prevedendo la corresponsione dell'indennità per i due mesi antecedenti la data effettiva del parto, tuttavia la norma impugnata non presenterebbe i profili di incostituzionalità dedotti dal rimettente, in considerazione della diversità delle situazioni inerenti al lavoro subordinato rispetto a quello autonomo. In particolare, nell'ipotesi di parto prematuro della lavoratrice subordinata, esaminata dalla Corte nella sentenza n. 270 del 1999, il rapporto che si instaura tra la madre ed il bambino potrebbe essere effettivamente compromesso qualora il bambino fosse affidato alle cure della madre quando essa è obbligata a riprendere l'attività lavorativa; l'assenza di tali obblighi nel lavoro autonomo e la minore rigidità nella gestione del lavoro giustificano invece la previsione di una differente disciplina, come ha rilevato la Corte con la sentenza n. 181 del 1993.  
    La difesa erariale sostiene poi l'erroneità della concezione unitaria dell'indennità di maternità, assunta dal rimettente, rilevando che le indennità antecedenti e successive al parto sono previste con riferimento ad esigenze diverse, sia sotto il profilo sanitario che sociale; pertanto, non sarebbe consentita l'unificazione dell'indennità per il periodo corrispondente alla somma dei due diversi periodi considerati dalla norma.Considerato in diritto1. - La questione sollevata dal Tribunale di Pinerolo ha ad oggetto l'art. 4 della legge 29 dicembre 1987, n. 546 (Indennità di maternità per le lavoratrici autonome), nella parte in cui limita la misura dell'indennizzo per maternità spettante alle lavoratrici autonome ai tre mesi successivi alla data effettiva del parto e non consente loro, in caso di parto prematuro, di ottenere un'ulteriore indennità, corrispondente al lasso di tempo che intercorre tra la data del parto effettivo e quella del parto presunto.  
    La asserita illegittimità costituzionale dell'anzidetta norma è individuata dal giudice rimettente principalmente nella violazione dell'art. 3 Cost., perché il legislatore, nel riconoscere il diritto all'indennità di maternità delle lavoratrici autonome per lo stesso periodo previsto per le lavoratrici subordinate, avrebbe omesso di integrare la tutela in relazione al parto prematuro, determinando così una disparità di trattamento rispetto alla nuova previsione del d.lgs. n. 151 del 2001; ed inoltre, nella violazione degli artt. 31 e 37 Cost., che impongono la protezione della maternità e del minore, anche con misure economiche, che disincentivino la precoce ripresa del lavoro a seguito del parto prematuro. 
    2. -  La questione è infondata, nei sensi di seguito precisati. 
    3. - L'art. 11 della legge n. 53 del 2000 ha introdotto una nuova previsione per le lavoratrici subordinate, disponendo che in caso di parto prematuro i giorni non goduti di astensione obbligatoria prima del parto si aggiungono al periodo di astensione obbligatoria post partum; tale previsione riguarda esclusivamente la predetta categoria di lavoratrici, essendo stato modificato, con l'aggiunta di due commi, il solo art. 4 della legge n. 1204 del 1971, che detta disposizioni a tutela delle lavoratrici madri subordinate, e non la legge n. 546 del 1987. 
    Con il decreto legislativo n. 151 del 2001 è stato emanato il testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, che all'art. 68 ha modificato gli artt. 3, 4 e 5 della legge n. 546 del 1987, prevedendo la corresponsione dell'indennità giornaliera per i due mesi antecedenti la data del parto e per i tre mesi successivi alla stessa per tutte le categorie di lavoratrici autonome. L'eliminazione del riferimento alla data presunta del parto ha determinato la conseguenza che, qualunque sia la data del parto, resta fissato comunque in cinque mesi il periodo di erogazione dell'indennità di maternità. 
    A seguito dell'anzidetta modifica normativa, la lavoratrice autonoma, che partorisce prematuramente, ha diritto al trattamento di maternità per il periodo complessivo di cinque mesi, indipendentemente dalla circostanza che il parto sia avvenuto a termine o sia stato prematuro; mentre la lavoratrice che pure ha partorito prematuramente, ma prima dell'entrata in vigore del testo unico, si vedrebbe corrispondere un'indennità ridotta, in quanto l'art. 86, comma 2, del testo unico ha espressamente disposto l'abrogazione della legge n. 546 del 1987 non già con effetto retroattivo ma solo a decorrere dalla data di entrata in vigore del decreto legislativo stesso (27 aprile 2001).  
    4. - Analoga questione di legittimità costituzionale dell'art. 3 della legge n. 546 del 1987, riguardante il trattamento di maternità delle coltivatrici dirette, colone e mezzadre nell'ipotesi di parto prematuro, è stata di recente affrontata da questa Corte, che, con la sentenza n. 197 del 2002, preso atto dell'evoluzione del sistema normativo, ha rilevato che si è progressivamente attuata una più estesa protezione della maternità in quanto tale piuttosto che della lavoratrice madre, ed ha sottolineato come tale evoluzione si ponga in continuità con i principi ripetutamente espressi dalla giurisprudenza costituzionale in ordine alla tutela della maternità, giungendo quindi alla conclusione che l'applicazione di questi stessi principi non può non obbligare l'interprete ad una lettura della norma conforme a Costituzione.  
    La medesima ratio decidendi che ha ispirato l'anzidetta pronuncia ricorre nella questione in esame sorta dall'applicazione dell'art. 4 della legge n. 546 del 1987, a proposito della quale si impongono le stesse considerazioni in relazione alla diversa categoria di lavoratrici, essendo identica la disciplina del trattamento di maternità per essa dettata. 
    L'art. 4 della legge n. 546 del 1987 deve essere dunque interpretato nel senso, conforme a Costituzione, che l'indennità spetta in ogni caso per la durata complessiva di mesi cinque.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara non fondata, nei sensi di cui in motivazione, la questione di legittimità costituzionale dell'art. 4 della legge 29 dicembre 1987, n. 546 (Indennità di maternità per le lavoratrici autonome), sollevata, in riferimento agli artt. 3, 31 e 37 della Costituzione, dal Tribunale di Pinerolo con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 novembre 2002. &#13;
    F.to: &#13;
    Cesare RUPERTO, Presidente &#13;
    Fernanda CONTRI, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 28 novembre 2002. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
