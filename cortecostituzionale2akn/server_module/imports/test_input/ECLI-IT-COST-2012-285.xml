<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2012/285/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2012/285/"/>
          <FRBRalias value="ECLI:IT:COST:2012:285" name="ECLI"/>
          <FRBRdate date="05/12/2012" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="285"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2012/285/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2012/285/ita@/!main"/>
          <FRBRdate date="05/12/2012" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2012/285/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2012/285/ita@.xml"/>
          <FRBRdate date="05/12/2012" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/12/2012" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2012</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>QUARANTA</cc:presidente>
        <cc:relatore_pronuncia>Giorgio Lattanzi</cc:relatore_pronuncia>
        <cc:data_decisione>05/12/2012</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alfonso QUARANTA; Giudici : Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 649, terzo comma, del codice penale, promosso dal Tribunale di Alessandria nel procedimento penale a carico di B.L., con ordinanza depositata il 17 gennaio 2012, iscritta al n. 82 del registro ordinanze 2012 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 20, prima serie speciale, dell'anno 2012.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 10 ottobre 2012 il Giudice relatore Giorgio Lattanzi.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che con ordinanza depositata il 17 gennaio 2012 (r.o. n. 82 del 2012) il Tribunale di Alessandria  ha sollevato, in riferimento all'articolo 3 della Costituzione, questione di legittimità costituzionale dell'articolo 649, terzo comma, del codice penale, nella parte in cui non annovera tra le fattispecie escluse dalla causa di non punibilità il delitto di usura previsto dall'art. 644 cod. pen.; 
 che il giudice a quo premette di essere investito, nell'ambito di un procedimento riguardante un delitto di usura, della richiesta di riesame di misure cautelari reali, e in particolare di un decreto di sequestro preventivo emesso ex art. 321, comma 3-ter, del codice di procedura penale dal Procuratore della Repubblica presso il Tribunale di Tortona, convalidato dal giudice per le indagini preliminari di quel tribunale, e di un decreto di convalida del sequestro probatorio eseguito dalla polizia giudiziaria, dei quali la difesa dell'indagato ha chiesto la revoca, invocando l'applicabilità a suo favore della causa di non punibilità di cui all'art. 649 cod. pen., in quanto l'indagato è legato alla persona offesa da un rapporto di affinità di primo grado; 
 che ad avviso del tribunale il reato di usura è astrattamente configurabile e sussistono tutti gli altri presupposti di legittimità richiesti ai fini del mantenimento del vincolo;
 che il tribunale ha dichiarato di non condividere l'opinione del giudice per le indagini preliminari, il quale aveva escluso l'applicabilità della causa di non punibilità in considerazione del fatto che la persona offesa e la figlia dell'imputato erano divorziati e nella convinzione che il divorzio avesse fatto venire meno il vincolo di affinità, e di ritenere invece che il vincolo permanga anche in caso di scioglimento o di cessazione degli effetti civili del matrimonio dal quale era stato originato, come si desumerebbe dell'art. 78, terzo comma, del codice civile, il quale prevede che l'affinità non cessa neanche con la morte del coniuge da cui deriva e individua, quale unica causa di cessazione, la dichiarazione di nullità del vincolo coniugale; 
 che gli artt. 87 e 434 cod. civ. confermerebbero la permanenza del rapporto di affinità di primo grado nel caso di divorzio, mentre l'art. 307, ultimo comma, cod. pen., nel definire la categoria dei prossimi congiunti, ricomprende espressamente gli affini, escludendoli nel solo caso di morte, senza prole, del coniuge; 
 che, ad avviso del giudice a quo, la ratio dell'istituto di cui all'art. 649, primo comma, cod. pen. risiede «nella necessità di evitare di turbare le relazioni familiari anche in considerazione del fatto che nell'ambito dello stesso nucleo familiare, vi è comunque una comunanza di interessi economici»; 
 che, a sua volta, la ratio dell'ultimo comma dell'art. 649 cod. pen., il quale esclude dall'ambito di operatività della causa di non punibilità i delitti previsti dagli artt. 628, 629 e 630 cod. pen. e ogni altro delitto contro il patrimonio che sia commesso con violenza alle persone, sarebbe ravvisabile nella plurioffensività delle fattispecie, le quali, oltre al patrimonio, offendono altri beni costituzionalmente protetti, la cui lesione viene considerata di rilievo prevalente sull'interesse tutelato dall'art. 649 cod. pen.; 
 che anche il reato di usura, così come modificato dall'art. 1 della legge 7 marzo 1996, n. 108 (Disposizioni in materia di usura), si caratterizzerebbe come «fattispecie plurioffensiva», perché tutelerebbe, «oltre al patrimonio, la libertà morale del soggetto passivo e l'interesse pubblico alla correttezza dei rapporti economici, beni questi ultimi che trovano riconoscimento negli artt. 2 e 41 Cost.»; 
 che pertanto, secondo il rimettente, la mancata inclusione del delitto di cui all'art. 644 cod. pen., nel novero delle fattispecie escluse dall'applicazione della causa di non punibilità, si appalesa «irragionevole, perché l'art. 649 c.p. tratta in modo diverso reati che sottendono, invece, situazioni uguali ed in forza delle quali si giustifica l'eccezione»;
 che non potrebbero «trarsi, del resto, elementi di contrario avviso dalla circostanza che la causa di non punibilità appare essere applicabile prima facie alle fattispecie di cui agli artt. 648-bis e 648-ter c.p., e cioè a reati che sono volti alla salvaguardia di interessi a rilevanza pubblica, quali quelli dell'ordine pubblico, dell'ordine economico oltre che del patrimonio individuale», considerato che la sentenza della Corte costituzionale n. 302 del 2000, nell'escludere l'illegittimità dell'art. 649 cod. pen., nella parte in cui non comprende tra i reati non punibili, ove commessi in danno dei congiunti, quelli previsti dall'art. 12 del decreto-legge 3 maggio 1991, n. 143 (Provvedimenti urgenti per limitare l'uso del contante e dei titoli al portatore nelle transazioni e prevenire l'utilizzazione del sistema finanziario a scopo di riciclaggio), convertito, con modificazioni, nella legge 5 luglio 1991, n. 197, «ha già implicitamente ritenuto la non applicabilità della causa di non punibilità ai reati di riciclaggio e reimpiego, argomentando proprio sulla plurioffensività degli stessi e sulla tutela meta-individuale che offrono»;
 che, sempre secondo il giudice a quo, «la formulazione letterale della norma con particolare riferimento all'incipit "non è punibile chi ha commesso alcuno dei fatti previsti da questo titolo, in danno ...", non lascia alcun margine di dubbio circa l'estensione della stessa a tutte le fattispecie comprese nel titolo XIII del libro secondo (delitti contro il patrimonio), ad eccezione di quelle espressamente escluse»; 
 che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata non fondata; 
 che, secondo l'Avvocatura, «la scelta legislativa di escludere l'applicazione dell'esimente di cui all'art. 649 cod. pen. ai reati contro il patrimonio commessi con violenza sulle persone è giustificata dal maggior allarme sociale» riscontrabile in queste ultime fattispecie; 
 che «l'esposizione della persona al pericolo per la propria incolumità fisica» differenzierebbe «in modo significativo i delitti per cui non opera l'esimente speciale dagli altri reati contro il patrimonio, anche nel caso in cui le fattispecie presentino profili di plurioffensività»;
 che «l'oggettività giuridica del reato di usura ed in concreto la situazione di fatto vissuta dalla vittima» non sarebbero «assimilabili a quelle proprie dei reati presi in esame dal comma 3 dell'art. 649»; 
 che in seguito alla riformulazione avvenuta con la legge n. 108 del 1996 il reato di usura non tutelerebbe più la libertà morale della persona, dato che lo stato di bisogno altrui non è più un elemento costitutivo della fattispecie ma un'aggravante, mentre la qualificazione dell'interesse usurario è basata su un dato oggettivo;
 che ciò legittimerebbe la convinzione che le situazioni poste a confronto dal rimettente non siano omogenee, con conseguente esclusione della lamentata violazione del principio di ragionevolezza. 
 Considerato che il Tribunale di Alessandria ha sollevato, in riferimento all'articolo 3 della Costituzione, questione di legittimità costituzionale dell'articolo 649, terzo comma, del codice penale, nella parte in cui non annovera, tra le fattispecie escluse dalla operatività della causa di non punibilità, il delitto di usura previsto dall'art. 644 cod. pen.;
 che, secondo il rimettente, la mancata inclusione del delitto di usura tra quelli esclusi dall'ambito di applicazione della causa di non punibilità per fatti commessi a danno di congiunti si appalesa «irragionevole, perché l'art. 649 c.p. tratta in modo diverso reati che sottendono, invece, situazioni uguali ed in forza delle quali si giustifica l'eccezione»;
 che non è implausibile e si sottrae quindi al sindacato della Corte il presupposto, pure controverso, dal quale muove il giudice rimettente che lo scioglimento degli effetti civili del matrimonio non fa venire meno il rapporto di affinità tra i congiunti dei coniugi divorziati;
 che la questione sollevata è diretta ad ottenere l'introduzione di una nuova ipotesi di esclusione della causa di non punibilità prevista dall'art. 649 cod. pen., con un effetto peggiorativo del trattamento penale nel caso di commissione di un delitto di usura a danno di congiunti;
 che, come ha più volte rilevato questa Corte, la possibilità di una dichiarazione di illegittimità costituzionale in materia penale con effetti in malam partem incontra un limite nel principio della riserva di legge, che governa tale materia in forza dell'art. 25, secondo comma, Cost.;
 che perciò non sono ammissibili pronunce con effetti in malam partem che derivino dall'introduzione di nuove norme penali o dalla manipolazione di quelle esistenti (sentenza n. 394 del 2006), perché il principio sancito dall'art. 25, secondo comma, Cost. demanda in via esclusiva al legislatore la scelta dei fatti da sottoporre a pena e delle sanzioni loro applicabili, impedendo alla Corte di creare nuove fattispecie criminose o estendere quelle esistenti a casi non previsti, ovvero anche di incidere in peius sulla risposta punitiva o su aspetti comunque inerenti alla punibilità (ex plurimis, sentenza n. 394 del 2006; ordinanze n. 204, n. 66 e n. 5 del 2009); 
 che, pertanto, essendo diretta ad ottenere una pronuncia additiva in malam partem, la questione sollevata dal Tribunale di Alessandria è manifestamente inammissibile.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 649, terzo comma, del codice penale, sollevata, in riferimento all'articolo 3 della Costituzione, dal Tribunale di Alessandria, con l'ordinanza indicata in epigrafe. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 dicembre 2012.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Giorgio LATTANZI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 12 dicembre 2012.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
