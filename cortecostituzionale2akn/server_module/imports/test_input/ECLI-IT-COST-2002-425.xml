<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/425/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/425/"/>
          <FRBRalias value="ECLI:IT:COST:2002:425" name="ECLI"/>
          <FRBRdate date="07/10/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="425"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/425/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/425/ita@/!main"/>
          <FRBRdate date="07/10/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/425/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/425/ita@.xml"/>
          <FRBRdate date="07/10/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="18/10/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Franco Bile</cc:relatore_pronuncia>
        <cc:data_decisione>07/10/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 1, commi 70 e 71, della legge 28 dicembre 1995, n. 549 (Misure di razionalizzazione della finanza pubblica), e del decreto legislativo 29 giugno 1996, n. 414 (Attuazione della delega conferita dall'art. 1, commi 70 e 71, della legge 28 dicembre 1995, n. 549, in materia di soppressione del Fondo di previdenza per il personale addetto ai pubblici servizi di trasporto), promosso con ordinanza emessa il 17 ottobre 2001 dal Tribunale di Brescia nel procedimento civile vertente tra Carlo Montecucco ed altri e l'Istituto nazionale della previdenza sociale (INPS), iscritta al n. 67 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 8, prima serie speciale, dell'anno 2002. 
      Visti gli atti di costituzione di Lorenzo Micari ed altri, dell'INPS, nonché l'atto d'intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 3 luglio 2002 il Giudice relatore Franco Bile. 
    Ritenuto che con ordinanza del 17 ottobre 2001 il Tribunale di Brescia - nel corso di un giudizio promosso da alcuni dipendenti ed ex-dipendenti di un'azienda pubblica di trasporto nei confronti dell'Istituto nazionale della previdenza sociale (INPS), per l'accertamento del diritto alle prestazioni del Fondo di previdenza per il personale addetto ai pubblici servizi di trasporto anche dopo la sua soppressione ex lege (ossia dopo il 31 dicembre 1995), sul presupposto della ritenuta illegittimità costituzionale della soppressione stessa, nonché la conseguente condanna dell'INPS alla restituzione dei maggiori contributi versati - ha sollevato questione incidentale di legittimità costituzionale dell'art. 1, commi 70 e 71, della legge 28 dicembre 1995, n. 549 (Misure di razionalizzazione della finanza pubblica), e del decreto legislativo 29 giugno 1996, n. 414 (Attuazione della delega conferita dall'art. 1, commi 70 e 71, della legge 28 dicembre 1995, n. 549, in materia di soppressione del Fondo di previdenza per il personale addetto ai pubblici servizi di trasporto), in relazione agli artt. 3, 38, 76 e 97 (rectius: 87) della Costituzione, nella parte in cui prevedono la soppressione del suddetto Fondo di previdenza e pongono a carico dei lavoratori in servizio alla data del 31 dicembre 1995 il contributo al Fondo medesimo;  
    che il rimettente denuncia, con una prima censura, la violazione del principio di eguaglianza (art. 3 Cost.) e di necessaria adeguatezza della tutela previdenziale (art. 38 Cost.), per l'ingiustificatezza del deteriore trattamento riservato ai dipendenti già in servizio prima della soppressione del Fondo, tenuti a corrispondere contributi in misura superiore rispetto a quella degli altri lavoratori ai quali sono sotto ogni altro profilo equiparati, e, con una seconda censura, il vizio di eccesso di delega (art. 76 Cost.) del decreto legislativo n. 414 del 1996, sotto plurimi profili (superamento del termine, disparità di trattamento rispetto ai dipendenti degli enti locali, mantenimento dell'obbligo contributivo nonostante la soppressione del Fondo, mancata salvaguardia delle situazioni previdenziali più favorevoli già maturate, disparità di trattamento dei lavoratori in ragione della data di assunzione, violazione del criterio del c.d. pro rata); 
    che, in particolare, sarebbe stato violato il termine di validità della delega poiché - secondo il rimettente - occorre che il provvedimento delegato sia non solo emanato, ma anche pubblicato nel termine previsto per l'esercizio della delega; 
    che i lavoratori ricorrenti si sono costituiti ed hanno aderito alle prospettazioni dell'ordinanza di rimessione; 
    che si è costituito anche l'INPS, chiedendo che la questione sia dichiarata inammissibile o infondata; 
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la manifesta infondatezza della questione di costituzionalità. 
    Considerato che - quanto alla prima censura mossa dal Tribunale rimettente - questa Corte, con ordinanza n. 164 del 2002, ha già dichiarato la manifesta infondatezza della questione di legittimità costituzionale dell'art. 2, comma 1, del decreto legislativo 29 giugno 1996, n. 414 (Attuazione della delega conferita dall'art. 1, commi 70 e 71, della legge 28 dicembre 1995, n. 549, in materia di soppressione del Fondo di previdenza per il personale addetto ai pubblici servizi di trasporto); 
    che, secondo tale pronuncia, la maggiore ritenuta previdenziale sulla retribuzione negli anni 1996, 1997 e 1998 non riguarda tutti i dipendenti dell'azienda datrice di lavoro, ma solo quelli (già iscritti al Fondo speciale alla data della sua soppressione) che, pur transitati nel regime ordinario dell'assicurazione generale, continuano a beneficiare, seppur pro rata, del più favorevole trattamento previdenziale assicurato dal Fondo speciale; 
    che tale applicabilità residuale della previgente regolamentazione del Fondo, prevista dalla legge di delega  (art. 1, comma 70, lettera c), della legge n. 549 del 1995) e disciplinata dal successivo decreto legislativo (art. 3 del d.lgs. n. 414 del 1996) quale beneficio ad esaurimento per i soli dipendenti già iscritti al Fondo stesso,  rappresenta un elemento differenziale sufficiente a giustificare - sotto il profilo del rispetto del principio di eguaglianza (art. 3 Cost.) - una disciplina particolare, peraltro a carattere transitorio, la quale - deve ora aggiungersi - non compromette affatto  l'adeguatezza della tutela previdenziale dei dipendenti (art. 38 Cost.); 
    che pertanto la prima censura è manifestamente infondata; 
    che l'ulteriore censura di eccesso di delega risulta essere specifica solo con riferimento all'allegato superamento del termine per l'esercizio della delega, previsto dall'art. 1, comma 70, della legge n. 549 del 1995, in sei mesi dalla data di entrata in vigore della legge; 
    che però, nella fattispecie, non rileva la circostanza, dedotta dal rimettente, che il citato d.lgs. n. 414 del 29 giugno 1996, ancorché emanato prima della scadenza del termine, sia stato pubblicato successivamente (nella Gazzetta Ufficiale n. 186 dell'8 agosto 1996); 
    che a tal proposito va ribadito il principio secondo cui l'esercizio della funzione legislativa delegata, consentito dall'art. 76 Cost., si esaurisce con l'emanazione del decreto presidenziale (ex art. 87, quinto comma, Cost.) entro il termine previsto dalla legge di delega, mentre la sua pubblicazione, pur indispensabile per l'entrata in vigore dell'atto legislativo, costituisce un fatto esterno e successivo all'esercizio della funzione stessa e pertanto non necessariamente deve avvenire nel termine suddetto (sentenze nn. 34 del 1960 e 91 del 1962; cfr. anche, più recentemente, sentenza n. 425 del 2000); 
    che le altre censure mosse dal rimettente non sono conferenti al parametro, trattandosi di critiche dirette al contenuto dei criteri direttivi posti dalla legge di delega, mentre il vizio di eccesso di delega concerne unicamente l'esistenza del potere di legiferare esercitato dal Governo con l'emanazione del decreto delegato; 
    che pertanto anche la seconda censura è manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 1, commi 70 e 71, della legge 28 dicembre 1995, n.549 (Misure di razionalizzazione della finanza pubblica), e del decreto legislativo 29 giugno 1996 n.414 (Attuazione della delega conferita dall'art. 1, commi 70 e 71, della legge 28 dicembre 1995, n. 549, in materia di soppressione del Fondo di previdenza per il personale addetto ai pubblici servizi di trasporto), sollevata, in riferimento agli artt. 3, 38, 76 e 87 della Costituzione, dal Tribunale di Brescia con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 ottobre 2002. &#13;
    F.to: &#13;
    Cesare RUPERTO, Presidente &#13;
    Franco BILE, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 18 ottobre 2002. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
