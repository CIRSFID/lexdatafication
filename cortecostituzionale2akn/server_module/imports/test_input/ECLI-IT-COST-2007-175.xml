<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/175/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/175/"/>
          <FRBRalias value="ECLI:IT:COST:2007:175" name="ECLI"/>
          <FRBRdate date="22/05/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="175"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/175/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/175/ita@/!main"/>
          <FRBRdate date="22/05/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/175/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/175/ita@.xml"/>
          <FRBRdate date="22/05/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="01/06/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Franco Bile</cc:relatore_pronuncia>
        <cc:data_decisione>22/05/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente 
O R D I N A N Z A 
nel giudizio di legittimità costituzionale dell'art. 45, comma 4, della legge della Regione Sardegna 23 dicembre 2005, n. 23 (Sistema integrato dei servizi alla persona. Abrogazione della legge regionale n. 4 del 1998 - Riordino delle funzioni socio-assistenziali), promosso con ricorso del Presidente del Consiglio dei ministri notificato il 2 marzo 2006, depositato in cancelleria il successivo 7 marzo ed iscritto al n. 44 del registro ricorsi 2006. 
    Visto l'atto di costituzione della Regione Sardegna; 
    udito nell'udienza pubblica dell'8 maggio 2007 il Giudice relatore Franco Bile; 
    udito l'avvocato Laura Rainaldi per la Regione Sardegna. 
    Ritenuto che, con ricorso notificato il 2 marzo 2006 e depositato il successivo 7 marzo, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato, in riferimento agli artt. 117, secondo comma, lettera l), e 118, terzo comma, della Costituzione, l'art. 45, comma 4, della legge della Regione Sardegna 23 dicembre 2005, n. 23 (Sistema integrato dei servizi alla persona. Abrogazione della legge regionale n. 4 del 1998 - Riordino delle funzioni socio-assistenziali), nella parte in cui stabilisce che il Comitato di gestione del fondo - costituito, ai sensi dell'art. 15 della legge 11 agosto 1991, n. 266 (Legge-quadro sul volontariato), «presso le regioni al fine di istituire, per il tramite degli enti locali, centri di servizio a disposizione delle organizzazioni di volontariato, e da queste gestiti, con la funzione di sostenerne e qualificarne l'attività» - «provvede alla suddivisione dei finanziamenti del fondo speciale su base provinciale, prevedendo l'istituzione di centri di servizi per il volontariato provinciali e distrettuali a partire dall'anno 2006»; 
    che, secondo il ricorrente, la norma impugnata eccede le competenze legislative attribuite alla Regione autonoma della Sardegna dallo statuto speciale, adottato con legge costituzionale 26 febbraio 1948, n. 3, e invade la materia «ordinamento civile», riservata alla legislazione esclusiva dello Stato dall'art. 117, secondo comma, lettera l), Cost., poiché la previsione della suddivisione dei finanziamenti «su base provinciale», anche attraverso l'istituzione di centri di servizi per il volontariato «provinciali e distrettuali», incide illegittimamente sulla libertà di scelta del Comitato medesimo, che ha natura giuridica di ente privato, quale organismo di indirizzo delle fondazioni bancarie; 
    che, ad avviso del ricorrente, tale «intollerabile limitazione dell'autonomia negoziale» del Comitato e dei privati che lo compongono contrasta altresì con le stesse disposizioni dell'art. 15, commi 1 e 3, della legge n. 266 del 1991, in attuazione delle quali è stato emanato il decreto del Ministro del tesoro 8 ottobre 1997 (Modalità per la costituzione dei fondi speciali per il volontariato presso le regioni), il cui art. 2, comma 6, stabilisce, alla lettera a), che il comitato di gestione «provvede ad individuare e a rendere pubblici i criteri per l'istituzione di uno o più centri di servizio nella regione» e, alla lettera e), che esso «ripartisce annualmente, fra i centri di servizio istituiti presso la regione, le somme scritturate nel fondo speciale di cui al presente articolo»; 
      che la Regione autonoma della Sardegna, in persona del suo Presidente pro tempore, costituitasi in giudizio, ha chiesto alla Corte di dichiarare la questione inammissibile e, comunque, infondata nel merito;  
    che - preso atto della sopravvenuta modifica della norma impugnata ad opera dell'art. 27, comma 3, della legge della Regione Sardegna 11 maggio 2006, n. 4 (Disposizioni varie in materia di entrate, riqualificazione della spesa, politiche sociali e di sviluppo) e del conseguente venir meno delle motivazioni del ricorso - il Consiglio dei ministri, con delibera del 21 luglio 2006, ha deciso di rinunciare all'impugnativa; 
    che l'Avvocatura generale dello Stato ha quindi depositato, in data 3 aprile 2007, atto di rinuncia al ricorso, notificato alla Regione, e che questa, con delibera della Giunta regionale del 19 aprile 2007, depositata il successivo 2 maggio, ha accettato tale rinuncia. 
    Considerato che, ai sensi dell'art. 25 delle norme integrative per i giudizi dinanzi a questa Corte, la rinuncia al ricorso, seguita dall'accettazione della controparte, comporta l'estinzione del processo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara estinto il processo. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 maggio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente e Redattore  &#13;
Gabriella MELATTI, Cancelliere  &#13;
Depositata in Cancelleria l'1 giugno 2007.  &#13;
Il Cancelliere  &#13;
F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
