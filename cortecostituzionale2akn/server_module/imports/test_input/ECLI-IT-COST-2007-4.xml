<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/4/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/4/"/>
          <FRBRalias value="ECLI:IT:COST:2007:4" name="ECLI"/>
          <FRBRdate date="08/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="4"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/4/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/4/ita@/!main"/>
          <FRBRdate date="08/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/4/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/4/ita@.xml"/>
          <FRBRdate date="08/01/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/01/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>08/01/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 407 e 410 del codice civile, promosso con ordinanza del 3 gennaio 2006 dal giudice tutelare del Tribunale di Venezia - sezione distaccata di Chioggia, sul ricorso proposto da F.M. ed altri, iscritta al n. 143 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 21, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 25 ottobre 2006 il Giudice relatore Alfio Finocchiaro. 
    Ritenuto che, nel corso del procedimento civile promosso da F.M., F.S., F.M. e F.P. – i quali, con ricorso del 3 marzo 2005, avevano chiesto la nomina di un amministratore di sostegno a favore del fratello F.M., deducendo le sue precarie condizioni di salute, tali da precludergli la possibilità di curare i propri interessi – il giudice tutelare del Tribunale di Venezia - sezione distaccata di Chioggia, con ordinanza del 3 gennaio 2006, ha sollevato, in riferimento agli artt. 2 e 3 della Costituzione, questione di legittimità costituzionale degli artt. 407 e 410 del codice civile, nel testo introdotto dalla legge 9 gennaio 2004, n. 6 (Introduzione nel libro I, titolo XII, del codice civile del capo I, relativo all'istituzione dell'amministrazione di sostegno e modifica degli articoli 388, 414, 417, 418, 424, 426, 427 e 429 del codice civile in materia di interdizione e di inabilitazione, nonché relative norme di attuazione, di coordinamento e finali), nella parte in cui non subordinano al consenso dell'interessato l'attivazione della predetta misura ed il compimento dei singoli atti gestionali, o comunque non attribuiscono efficacia paralizzante al suo dissenso in ordine a tale attivazione e al compimento di tali atti; 
    che il giudice rimettente premette che, dalla disposta perizia medico-legale, è emerso che il beneficiario aveva subìto un profondo danno cerebrale a causa della rottura di un'arteria temporale, con conseguente formazione di un vasto ematoma, cerebrale che aveva causato gravissimi deficit motori nonché a carico del linguaggio, con perdita temporanea della capacità di parlare e scrivere, ma con una residua capacità cognitiva, tale da potersi desumere la possibilità per lo stesso di comprendere il significato di alcuni messaggi ed informazioni, anche di ordine economico, e di comunicare, sia pure in modo non verbale, con le persone a lui più vicine, e che la medesima perizia non ha escluso la possibilità di miglioramenti futuri per effetto di interventi riabilitativi; 
    che – aggiunge il rimettente – l'esame dell'interessato, pur mostrando la ridotta capacità di F.M. di esprimere un eventuale consenso o dissenso rispetto ad atti giuridici da compiere nel suo interesse da parte di un amministratore di sostegno, ha peraltro manifestato una consistente vivacità reattiva dello stesso alle sollecitazioni e domande del giudice stesso con riguardo alle sue attuali condizioni, al livello di solidarietà, materiale e non, di cui poteva godere da parte della moglie, dei figli e dei fratelli, e, soprattutto, ha fatto emergere la contrarietà del medesimo alla nomina di un soggetto che lo rappresentasse ufficialmente nei rapporti con l'ambiente sociale, ed il suo desiderio che fosse sua moglie, con lui convivente nella casa di proprietà comune e già  delegata alla riscossione della pensione del coniuge, a continuare ad occuparsi di lui; 
    che – premette ancora il rimettente – i fratelli di F.M., sentiti in separata sede, si sono detti contrari alla nomina della moglie quale amministratore di sostegno, in considerazione del carattere e dello stile di vita della stessa, e paventano la possibilità che il loro congiunto si trasferisca con la famiglia in Thailandia, terra di origine della donna; 
    che, ciò premesso, il giudice a quo osserva che le infermità o menomazioni, fisiche o psichiche, che incidono sulla possibilità della persona di curare autonomamente i propri interessi, deprimendo, talora fino ad escluderla del tutto, la propria capacità di intendere o di volere, possono richiedere un intervento protettivo di natura anche limitativa, sinanco ablatoria, della capacità di agire; 
    che, in tali ipotesi, il giudice è chiamato a decidere quale misura, tra tutela, curatela o amministrazione di sostegno, sia più idonea alle esigenze di protezione del soggetto in difficoltà e di cura ed amministrazione dei suoi interessi; 
    che, nell'operare tale scelta, il giudice deve attenersi al principio generale secondo il quale in nessun caso all'amministratore di sostegno possono essere affidati integralmente i poteri tipici del tutore (di rappresentanza legale dell'interdetto) o quelli tipici del curatore (di assistenza, in funzione integratrice della volontà dell'inabilitato), con la conseguenza che, ove la gravità del quadro clinico incidente sulla capacità di intendere e di volere del soggetto suggerisca di conferire ad un amministratore di nomina pubblica un potere di rappresentanza o di assistenza generalizzato, esteso, cioè, a tutti gli atti di straordinaria amministrazione, tale amministratore deve assumere la veste di tutore o di curatore, e non già quella di amministratore di sostegno, figura meno invasiva, che si attaglia alla diversa ipotesi in cui il soggetto conservi un consistente grado di autonomia psichica nella cura dei propri interessi, tale da consentirgli in ogni caso di percepire l'eventuale pregiudizio che si annidi negli atti compiuti per suo conto; 
    che, in tali ipotesi, non dovrebbe potersi imporre la misura dell'amministrazione di sostegno in presenza di un dissenso dell'interessato, in quanto essa ne conculcherebbe la libertà e l'autodeterminazione; 
    che, eccettuate le ipotesi in cui le funzioni psichiche del soggetto siano effettivamente compromesse in misura rilevante, sì da escludere la capacità di intendere e di volere il legislatore non può sostituire arbitrariamente le proprie valutazioni e scelte sulla cura degli interessi delle persone a quelle operate dagli stessi interessati sulla base della propria personale ed insindacabile scala di valori senza violare la dignità della persona e la relativa sfera di libertà giuridica riconosciuta e tutelata dagli artt. 2 e 3 della Costituzione, non potendo il sacrificio di tale libertà essere legittimato semplicemente dal rischio di un «pessimo uso» della stessa; 
    che, se è pur vero, conclude il rimettente, che, alla stregua della normativa censurata, il giudice deve tener conto delle richieste dell'interessato, tale doverosa considerazione riguarda, peraltro, il solo caso in cui dette richieste siano compatibili con l'interesse del beneficiario, quale ritenuto dal giudice, e tale compatibilità costituisce altresì il criterio guida al quale deve attenersi il giudice tutelare nel risolvere ogni contrasto tra l'amministratore di sostegno ed il beneficiario medesimo, in ordine ad un singolo atto gestionale da compiere; 
    che in tal modo, ad avviso del giudice a quo, si trasformerebbe il nuovo strumento di protezione dei soggetti in difficoltà in una sorta di interdizione camuffata, nella quale la volontà della persona verrebbe annullata senza una ragionevole giustificazione e senza le relative garanzie e cautele (patrocinio obbligatorio, presenza obbligatoria del pubblico ministero., audizione dell'interessato, decisione collegiale); 
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura Generale dello Stato, che ha concluso per la inammissibilità e, comunque, per la manifesta infondatezza della questione. 
    Considerato che il Giudice tutelare del Tribunale di Venezia, sezione distaccata di Chioggia, dubita della legittimità costituzionale degli artt. 407 e 410 del codice civile, nel testo introdotto dalla legge 9 gennaio 2004, n. 6 (Introduzione nel libro I, titolo XII, del codice civile del capo I, relativo all'istituzione dell'amministrazione di sostegno e modifica degli articoli 388, 414, 417, 418, 424, 426, 427 e 429 del codice civile in materia di interdizione e di inabilitazione, nonché relative norme di attuazione, di coordinamento e finali), nella parte in cui non subordinano al consenso dell'interessato l'attivazione della misura dell'amministrazione di sostegno ed il compimento dei singoli atti gestionali, o comunque non attribuiscono efficacia paralizzante al suo dissenso in ordine a tale attivazione e al compimento di tali atti, per violazione degli artt. 2 e 3 della Costituzione; 
    che, secondo il rimettente, le norme censurate si porrebbero in contrasto con gli evocati parametri perché violerebbero la dignità della persona e la relativa sfera di libertà giuridica, e trasformerebbero il nuovo strumento di protezione dei soggetti in difficoltà in una sorta di interdizione camuffata, nella quale la volontà della persona viene annullata senza una ragionevole giustificazione e senza le relative garanzie e cautele; 
    che il giudice tutelare – secondo la costante giurisprudenza di questa Corte (v. da ultimo, sentenza n. 440 del 2005) – è legittimato a sollevare questioni di legittimità costituzionale; 
    che l'art. 407 del codice civile, nel disciplinare il procedimento per l'istituzione dell'amministrazione di sostegno, prevede espressamente che il giudice tutelare deve sentire personalmente la persona cui il procedimento si riferisce e deve tenere conto «compatibilmente con gli interessi e le esigenze di protezione della persona, dei bisogni e delle richieste di questa» (comma 2); 
    che tale dato normativo, contrariamente all'assunto del rimettente, non esclude, ma anzi chiaramente attribuisce al giudice, anche il potere di non procedere alla nomina dell'amministratore di sostegno in presenza del dissenso dell'interessato, ove l'autorità giudiziaria, nell'ambito della discrezionalità riconosciutale dalla norma censurata, ritenga detto dissenso – nel contesto della fattispecie sottoposta al suo giudizio – giustificato e prevalente su ogni altra diversa considerazione, senza che la sottoposizione del rilievo del dissenso alla condizione della sua compatibilità con gli interessi e con le esigenze di protezione della persona integri violazione dei parametri costituzionali denunciati (artt. 2 e 3 della Costituzione), i quali, invece, sono in questo modo realizzati; 
    che, alla stregua delle suesposte argomentazioni, la questione deve essere dichiarata manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 407 e 410 del codice civile, nel testo introdotto dalla legge 9 gennaio 2004, n. 6 (Introduzione nel libro I, titolo XII, del codice civile del capo I, relativo all'istituzione dell'amministrazione di sostegno e modifica degli articoli 388, 414, 417, 418, 424, 426, 427 e 429 del codice civile in materia di interdizione e di inabilitazione, nonché relative norme di attuazione, di coordinamento e finali), sollevata, in riferimento agli artt. 2 e 3 della Costituzione, dal Giudice tutelare presso il Tribunale di Venezia, sezione distaccata di Chioggia, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8 gennaio 2007. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Alfio FINOCCHIARO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 19 gennaio 2007. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
