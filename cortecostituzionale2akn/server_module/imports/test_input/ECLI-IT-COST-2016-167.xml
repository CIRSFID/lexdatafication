<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/167/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/167/"/>
          <FRBRalias value="ECLI:IT:COST:2016:167" name="ECLI"/>
          <FRBRdate date="22/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="167"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/167/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/167/ita@/!main"/>
          <FRBRdate date="22/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/167/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/167/ita@.xml"/>
          <FRBRdate date="22/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/07/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>22/06/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giuseppe FRIGO, Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Nicolò ZANON, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione della Camera dei deputati del 28 novembre 2012, relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dall'on. Lucio Barani nei confronti del Sistema Integrato Ospedali Regionali (SIOR) ed altri, promosso dal Giudice monocratico del Tribunale ordinario di Prato, con ricorso notificato il 19-23 gennaio 2015, depositato in cancelleria il 20 febbraio 2015 ed iscritto al n. 4 del registro conflitti tra poteri dello Stato 2014, fase di merito. 
 Visti l'atto di costituzione della Camera dei deputati, nonchè gli atti di intervento del Sistema Integrato Ospedali Regionali (S.I.O.R.) e quello, fuori termine, di Barani Lucio;
 udito nella camera di consiglio del 22 giugno il Giudice relatore Giuseppe Frigo.
 Ritenuto che, con ricorso depositato l'11 agosto 2014, il Tribunale ordinario di Prato, in composizione monocratica, ha proposto conflitto di attribuzione tra poteri dello Stato nei confronti della Camera dei deputati, in relazione alla deliberazione del 28 novembre 2012 (Doc. IV-quater, n. 23), con la quale l'Assemblea ha dichiarato che i fatti per i quali è in corso, davanti al Tribunale ricorrente, il giudizio civile promosso nei confronti dell'on. Lucio Barani dal Sistema Integrato Ospedali Regionali (SIOR) e dalle Aziende USL n. 1 di Massa e Carrara, n. 2 di Lucca, n. 3 di Pistoia e n. 4 di Prato, concernono opinioni espresse dal predetto deputato nell'esercizio delle sue funzioni di parlamentare e, dunque, insindacabili ai sensi dell'art. 68, primo comma, della Costituzione;
 che, secondo quanto riferito nell'atto introduttivo, il SIOR e le citate aziende sanitarie hanno convenuto in giudizio l'on. Barani con domanda di risarcimento del danno non patrimoniale e di pagamento della riparazione pecuniaria prevista dalla legge sulla stampa, in relazione ad una serie di dichiarazioni rilasciate dal convenuto ad organi di stampa e ad emittenti televisive tra il mese di marzo e quello di maggio 2011, ritenute dalle parti attrici gravemente lesive del proprio prestigio e della propria reputazione;
 che tali dichiarazioni concernono, in particolare, presunte infiltrazioni mafiose nelle aziende sanitarie della Regione Toscana, asseriti aggiramenti dei sistemi di controllo sugli appalti pubblici, aumenti ingiustificati dei costi e sprechi di risorse economiche verificatisi nella costruzione di quattro ospedali;
 che, ad avviso del Tribunale ricorrente, non sarebbe ravvisabile alcun nesso funzionale - nei sensi precisati dalla giurisprudenza costituzionale - tra le dichiarazioni extra moenia oggetto di giudizio e gli atti, anche «atipici», costituenti esercizio delle funzioni parlamentari indicati nella delibera impugnata: di qui la richiesta di dichiarare che non spettava alla Camera dei deputati affermare l'insindacabilità delle opinioni espresse all'on. Barani e di conseguente annullamento della delibera stessa;
 che il conflitto è stato dichiarato ammissibile con ordinanza n. 286 del 2014, notificata, unitamente al ricorso introduttivo, il 19-23 gennaio 2015 e pubblicata nella Gazzetta Ufficiale n. 9 del 4 marzo 2015;
 che, con atto depositato il 13 marzo 2015, si è costituita la Camera dei deputati, la quale ha eccepito, in via preliminare, l'inammissibilità del conflitto nella parte relativa alle dichiarazioni che non risultano riprodotte esattamente nel ricorso introduttivo, chiedendone, nel merito, il rigetto quanto alle restanti parti;
 che, con atto depositato il 13 gennaio 2015, sono interventi il SIOR e le Aziende USL n. 1 di Massa e Carrara, n. 2 di Lucca, n. 3 di Pistoia e n. 4 di Prato, instando per l'accoglimento del ricorso;
 che è intervenuto, altresì, l'on. Barani con atto depositato il 14 giugno 2016 e, dunque, oltre il termine previsto dagli artt. 4, comma 4, e 24, comma 4, delle norme integrative per i giudizi davanti alla Corte costituzionale;
 che, con atto depositato il 15 giugno 2016, il Tribunale ordinario di Prato - rilevato che nel procedimento civile che ha originato il conflitto le parti attrici hanno presentato dichiarazione di rinuncia agli atti del giudizio, accettata dal convenuto, e che ciò comporta il venir meno dell'interesse all'annullamento della delibera impugnata - ha dichiarato di rinunciare al conflitto;
 che la rinuncia è stata accettata dalla Camera dei deputati, oltre che dai soggetti intervenuti.
 Considerato che, a norma dell'art. 24, comma 6, delle norme integrative per i giudizi davanti alla Corte costituzionale, la rinuncia al ricorso per conflitto di attribuzione tra poteri dello Stato, «qualora sia accettata da tutte le parti costituite, estingue il processo»;
 che, essendosi verificata la predetta condizione, il presente giudizio va, dunque, dichiarato estinto.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara estinto il processo.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 giugno 2016.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 7 luglio 2016.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
