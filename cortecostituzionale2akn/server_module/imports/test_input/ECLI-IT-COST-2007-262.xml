<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/262/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/262/"/>
          <FRBRalias value="ECLI:IT:COST:2007:262" name="ECLI"/>
          <FRBRdate date="20/06/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="262"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/262/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/262/ita@/!main"/>
          <FRBRdate date="20/06/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/262/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/262/ita@.xml"/>
          <FRBRdate date="20/06/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="06/07/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>20/06/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), e dell'art. 10, commi 1 e 2, della stessa legge, promosso con ordinanza del 17 marzo 2006 dalla Corte militare d'appello di Napoli nel procedimento penale a carico di C. V., iscritta al n. 126 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 13, prima serie speciale, dell'anno 2007. 
    Udito nella camera di consiglio del 6 giugno 2007 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che la Corte militare d'appello di Napoli ha sollevato, in riferimento agli artt. 3, 111 e 112 della Costituzione, questione di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui non prevede per il pubblico ministero la possibilità di appellare le sentenze di proscioglimento al di fuori dei casi di cui al comma 2 dello stesso art. 593 cod. proc. pen.», e dell'art. 10, commi 1 e 2, della medesima legge; 
    che il giudice a quo – chiamato a celebrare, a seguito di impugnazione del pubblico ministero, il giudizio d'appello nei confronti di un imputato assolto in primo grado per insussistenza del fatto – precisa che, sopravvenuta medio tempore la legge n. 46 del 2006 e sottratto al pubblico ministero il potere di appellare le sentenze di proscioglimento con la modifica dell'art. 593 cod. proc. pen., le parti hanno chiesto che l'appello sia dichiarato inammissibile ai sensi dell'art. 10, comma 2, della medesima legge; 
    che, quanto alla non manifesta infondatezza della questione, la disciplina censurata si porrebbe in contrasto con plurimi parametri costituzionali; 
    che sarebbe violato, in primo luogo, l'art. 3 Cost., poiché, negando al pubblico ministero la facoltà di proporre appello avverso le sentenze di proscioglimento, la disciplina censurata preclude irragionevolmente all'organo della pubblica accusa di dare «concreta attuazione al principio dell'obbligatorietà dell'azione penale», fornendo «il suo doveroso contributo all'accertamento dei reati»; 
    che, inoltre, sarebbe irragionevole e quindi lesivo dell'art. 3 Cost. privare la parte civile, che conserva la possibilità di proporre appello avverso le sentenze di proscioglimento, del «sostegno della parte pubblica nell'accertamento dei profili civilistici» della responsabilità penale; 
    che sarebbero altresì violati i principi della parità fra le parti e della ragionevole durata del processo, sanciti nel secondo comma dell'art. 111 Cost.; 
    che, sotto il primo profilo, il rimettente – sul presupposto che il principio della parità riguardi anche «gli strumenti di impulso funzionali al raggiungimento degli scopi che un sistema processuale [...] informato ai principi costituzionali deve garantire» e fra questi «l'attuazione della pretesa punitiva dello Stato a tutela dei primari interessi della collettività» – osserva che al pubblico ministero viene negata la possibilità di ottenere un completo riesame nel merito delle risultanze processuali poste a base della sentenza di proscioglimento;  
    che, sotto il secondo profilo, il rimettente sottolinea gli effetti negativi che la nuova disciplina determina sui tempi di definizione dei processi, in conseguenza della natura esclusivamente rescindente del giudizio per cassazione; 
    che, ancora, risulterebbe violato l'art. 112 Cost., poiché la normativa censurata, sottraendo al pubblico ministero il potere di impugnare le sentenze di proscioglimento (potere che è espressione del principio dell'obbligatorietà dell'azione penale), vanifica il complessivo assolvimento delle funzioni d'accusa; 
    che, infine, a giudizio del rimettente, la disciplina transitoria introdotta dall'art. 10 della legge n. 46 del 2006, nel prevedere l'immediata applicabilità della nuova normativa ai procedimenti in corso alla data di entrata in vigore della legge e la declaratoria di inammissibilità degli appelli proposti a quella data, determinerebbe «una ingiustificata disparità di trattamento» dei processi pendenti nei quali il pubblico ministero ha già chiesto l'ammissione di nuove prove decisive rispetto ai procedimenti ai quali si applica la disciplina a regime e per i quali soltanto opera la deroga all'inappellabilità nei casi di cui all'art. 603, comma 2, cod. proc. pen. 
    Considerato che il dubbio di costituzionalità sottoposto a questa Corte ha per oggetto la preclusione, conseguente alla modifica dell'art. 593 del codice di procedura penale ad opera dell'art. 1 della legge 20 febbraio 2006, n. 46, dell'appello delle sentenze dibattimentali di proscioglimento da parte del pubblico ministero e l'immediata applicabilità di tale regime, in forza dell'art. 10 della legge, ai procedimenti in corso alla data di entrata in vigore della medesima; 
    che, successivamente all'ordinanza di rimessione, questa Corte con sentenza n. 26 del 2007 ha dichiarato l'illegittimità costituzionale dell'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui, sostituendo l'art. 593 del codice di procedura penale, esclude che il pubblico ministero possa appellare contro le sentenze di proscioglimento, fatta eccezione per le ipotesi previste dall'art. 603, comma 2, del medesimo codice, se la nuova prova è decisiva», e dell'art. 10, comma 2, della citata legge n. 46 del 2006, «nella parte in cui prevede che l'appello proposto contro una sentenza di proscioglimento dal pubblico ministero prima della data di entrata in vigore della medesima legge è dichiarato inammissibile»; 
    che, alla stregua della richiamata pronuncia di questa Corte, gli atti devono essere pertanto restituiti al giudice rimettente per un nuovo esame della rilevanza della questione.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    ordina la restituzione degli atti alla Corte militare d'appello di Napoli.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 giugno 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 6 luglio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
