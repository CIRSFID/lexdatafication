<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/139/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/139/"/>
          <FRBRalias value="ECLI:IT:COST:2003:139" name="ECLI"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="139"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/139/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/139/ita@/!main"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/139/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/139/ita@.xml"/>
          <FRBRdate date="09/04/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/04/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>09/04/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 51-bis della legge 26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle misure privative e limitative della libertà), promosso, nell'ambito di un procedimento di sorveglianza, dal Tribunale di sorveglianza di Messina con ordinanza in data 11 maggio 2002, iscritta al n. 362 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 34, prima serie speciale, dell'anno 2002. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 26 febbraio 2003 il Giudice relatore Guido Neppi Modona. 
    Ritenuto che il Tribunale di sorveglianza di Messina ha sollevato, in riferimento all'art. 27, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 51-bis della legge 26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle misure privative e limitative della libertà), nella parte in cui in caso di «sopravvenienza di un titolo di esecuzione di altra pena detentiva subordina la prosecuzione della misura dell'affidamento in prova al servizio sociale al mero computo della pena e non anche alla valutazione di ogni altro elemento di meritevolezza»; 
    che il rimettente espone che il condannato era stato ammesso dal Tribunale di sorveglianza di Firenze alla misura alternativa dell'affidamento in prova al servizio sociale in relazione al segmento conclusivo di una esecuzione protrattasi per quasi dodici anni, sulla base della valutazione positiva del percorso rieducativo seguito; 
    che successivamente il Magistrato di sorveglianza di Messina, a seguito di ordine di carcerazione emesso per un nuovo titolo di esecuzione di altra pena detentiva, aveva disposto ai sensi dell'art. 51-bis dell'ordinamento penitenziario la provvisoria sospensione della misura alternativa in corso e ordinato l'accompagnamento in carcere del condannato, in quanto il residuo pena da scontare superava il limite stabilito dall'art. 47, comma 1, del medesimo ordinamento; 
    che il rimettente, chiamato a pronunciarsi in via definitiva sulla prosecuzione o cessazione della misura, osserva che ai sensi dell'art. 51-bis dell'ordinamento penitenziario il giudice deve «tenere conto esclusivamente del cumulo delle pene» e sulla base di questo solo elemento disporre la cessazione della misura alternativa (nella specie, l'affidamento in prova), anche quando la valutazione positiva circa l'adeguatezza della misura già concessa «sia suscettibile di essere confermata sulla base dei complessivi elementi di osservazione rilevati in sede di esecuzione e acquisiti al giudizio»;   
    che l'impossibilità di valutare ai fini della prosecuzione della misura «se permanga la prospettiva reale della rieducazione e della risocializzazione già avviata in sede di concessione del beneficio» determinerebbe l'illegittimità costituzionale della disciplina censurata per contrasto con il principio della finalità rieducativa della pena sancito dall'art. 27, terzo comma, Cost., fermo restando che «il legislatore è certamente sovrano nella identificazione dei presupposti e delle condizioni d'ordine edittale» di accesso alle misure alternative;    
    che la valutazione circa la prosecuzione della misura dovrebbe invece basarsi su «tutti i criteri di giudizio fissati dalla legge» e sui risultati dell'osservazione della personalità del condannato sia in ambito intramurario che extramurario;  
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la Corte dichiari la questione manifestamente inammissibile o infondata; 
    che secondo l'Avvocatura la questione sarebbe inammissibile, tra l'altro, per difetto di motivazione sulla rilevanza, in quanto il giudice a quo si limita a prospettare genericamente il superamento del limite edittale dei tre anni previsto per l'ammissione alla misura, senza indicare quale sia il residuo periodo di affidamento in prova, quale l'entità della nuova condanna e se questa sia divenuta irrevocabile; 
    che, nel merito, l'Avvocatura rileva che dall'accoglimento della questione deriverebbe l'«inaccettabile corollario» di sottoporre la concessione della misura alternativa nei confronti del soggetto condannato per la prima volta a condizioni più rigorose rispetto alla situazione di chi, essendo già in esecuzione della misura alternativa, potrebbe beneficiarne a diverse e più ampie condizioni, con il rischio di ulteriori «sperequazioni consequenziali». 
    Considerato che il rimettente dubita, in riferimento all'art. 27, terzo comma, della Costituzione, della legittimità costituzionale  dell'art. 51-bis della legge 26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle misure privative e limitative della libertà), nella parte in cui, in caso di sopravvenienza di un titolo di esecuzione di altra pena detentiva, impone al Tribunale di sorveglianza, ai fini della decisione sulla prosecuzione o sulla cessazione dell'affidamento in prova al servizio sociale, di tenere conto esclusivamente del limite di pena stabilito dall'art. 47, comma 1, del medesimo ordinamento per l'ammissione al beneficio, e non gli permette di valutare se permangano le condizioni per proseguire il percorso di rieducazione e di recupero sociale già avviato con la concessione della misura; 
    che, in sostanza, il rimettente chiede alla Corte una pronuncia che gli consenta di disporre la prosecuzione della misura malgrado il nuovo titolo esecutivo comporti il superamento del limite di pena previsto in via generale dall'art. 47, comma 1, dell'ordinamento penitenziario; 
    che, ai fini della rilevanza della questione, l'eccezione di inammissibilità prospettata dall'Avvocatura dello Stato è priva di fondamento, in quanto il rimettente ha dato atto che il nuovo titolo di esecuzione comporta il superamento del limite di pena previsto per l'ammissione al beneficio; 
    che la norma censurata è stata  introdotta dalla legge 10 ottobre 1986, n. 663, al fine di colmare una lacuna della precedente disciplina, che non conteneva alcuna disposizione in caso di sopravvenienza di un nuovo ordine di esecuzione di pena detentiva a carico del condannato ammesso ad una misura alternativa; 
    che, in ossequio al principio della funzione rieducativa della pena, la nuova disciplina tiene conto dell'esigenza di non interrompere automaticamente, quale che sia l'entità della pena da espiare a seguito della sopravvenienza di un nuovo titolo esecutivo, la misura alternativa in corso, ferma restando la condizione generale di ammissibilità dell'affidamento in prova rappresentata dal limite di pena ancora da espiare stabilito nella misura di tre anni dall'art. 47, comma 1, dell'ordinamento penitenziario;  
    che la soluzione prospettata dal rimettente porrebbe nel nulla tale condizione di ammissibilità in ogni caso in cui il condannato affidato in prova venga raggiunto da un nuovo titolo di esecuzione, vanificando una precisa scelta operata dal legislatore nell'esercizio della sua discrezionalità; 
    che il limite di pena previsto per l'ammissione all'affidamento in prova sarebbe quindi operante nei confronti di chi si trovi ad espiare una pena detentiva per la prima volta e non anche nei confronti di chi sia già affidato in prova quando interviene un nuovo titolo esecutivo, sì che solo a tale soggetto verrebbe paradossalmente riservato un trattamento più favorevole; 
    che, inoltre, tra soggetti raggiunti da più di un titolo esecutivo risulterebbe irragionevolmente discriminato chi si trovi ad espiare una nuova condanna sopravvenuta in un momento successivo all'esaurimento del primo rapporto esecutivo rispetto al condannato affidato in prova che sia ancora in esecuzione della pena al sopravvenire del nuovo titolo; 
    che la questione deve pertanto essere dichiarata manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 51-bis della legge 26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle misure privative e limitative della libertà personale), sollevata, in riferimento all'art. 27, terzo comma, della Costituzione, dal Tribunale di sorveglianza di Messina, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 aprile 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 24 aprile 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
