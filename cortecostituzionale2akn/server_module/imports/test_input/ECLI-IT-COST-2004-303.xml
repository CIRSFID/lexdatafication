<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/303/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/303/"/>
          <FRBRalias value="ECLI:IT:COST:2004:303" name="ECLI"/>
          <FRBRdate date="27/09/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="303"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/303/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/303/ita@/!main"/>
          <FRBRdate date="27/09/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/303/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/303/ita@.xml"/>
          <FRBRdate date="27/09/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="29/09/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ONIDA</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>27/09/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfonso QUARANTA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito della deliberazione della Camera dei deputati del 18 dicembre 2002 relativa alla insindacabilità, ai sensi dell'articolo 68, primo comma, della Costituzione, delle opinioni espresse dal deputato Cesare Previti nei confronti di Stefania Ariosto, promosso dal Tribunale di Roma – IV Sez. penale, con ricorso depositato il 13 maggio 2003 ed iscritto al n. 244 del registro ammissibilità conflitti. 
      Udito nella camera di consiglio del 7 luglio 2004 il Giudice relatore Giovanni Maria Flick. 
      Ritenuto che con ricorso del 1° aprile 2003, depositato nella cancelleria della Corte il 13 maggio 2003, il Tribunale di Roma — nel corso di un procedimento penale a carico del deputato Cesare Previti per il reato di diffamazione a mezzo stampa — ha sollevato conflitto di attribuzione tra poteri dello Stato nei confronti della Camera dei deputati, in relazione alla deliberazione adottata il 18 dicembre 2002 (doc. IV-quater, n. 38), che ha dichiarato che il fatto per il quale è in corso l'indicato procedimento penale concerne opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, con conseguente insindacabilità ai sensi dell'art. 68, primo comma, della Costituzione; 
      che il Tribunale ricorrente riferisce che il procedimento penale in questione, promosso a seguito di querela sporta da Stefania Ariosto, aveva ad oggetto le dichiarazioni rese dal deputato Cesare Previti nel corso di un'intervista pubblicata dal quotidiano «La Repubblica» del 30 gennaio 1997 e relative alla pretesa falsità di dichiarazioni accusatorie effettuate nei propri confronti dalla prima; 
      che, ad avviso del ricorrente, la Camera dei deputati, con l'affermazione di insindacabilità, avrebbe «arbitrariamente valutato» il collegamento delle dichiarazioni incriminate con la funzione parlamentare; 
      che, trattandosi di dichiarazioni rese fuori dell'esercizio delle attività parlamentari tipiche, avrebbe dovuto infatti esservi — ai fini della sussistenza del «nesso funzionale», presupposto dall'art. 68, primo comma, Cost. — quantomeno una sostanziale corrispondenza tra le dichiarazioni stesse e le opinioni già espresse nell'ambito delle predette attività; 
    che tale condizione non sarebbe per contro ravvisabile nella specie, giacché — se pure l'asserita falsità delle dichiarazioni dell'Ariosto era stata oggetto di dibattito parlamentare in rapporto ad una precedente richiesta di applicazione di misura cautelare e con riferimento a procedimenti penali con imputazioni similari a carico del medesimo deputato, così come rilevato dalla Camera nella deliberazione di insindacabilità — non vi sarebbe però prova che egli abbia reso, prima dell'intervista in questione, dichiarazioni corrispondenti a quelle oggetto di imputazione; 
    che il Tribunale ritiene, pertanto, che detta deliberazione abbia illegittimamente interferito sulla sfera di attribuzioni, costituzionalmente garantita, dell'autorità giudiziaria, chiedendone conseguentemente l'annullamento. 
      Considerato che, in questa fase, la Corte è chiamata, ai sensi dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, a deliberare esclusivamente se il ricorso sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivo ed oggettivo di un conflitto di attribuzioni tra poteri dello Stato, impregiudicata ogni decisione definitiva anche in ordine all'ammissibilità; 
      che, quanto al requisito soggettivo, il Tribunale di Roma è legittimato a sollevare il conflitto, in quanto competente a dichiarare definitivamente, per il procedimento del quale è investito, la volontà del potere cui appartiene, in ragione della posizione di indipendenza, costituzionalmente garantita, di cui godono i singoli organi giurisdizionali; 
      che anche la Camera dei deputati, che ha deliberato nel senso della insindacabilità delle opinioni espresse da un proprio membro, è legittimata ad essere parte del conflitto, in quanto organo competente a dichiarare definitivamente la volontà del potere che rappresenta; 
      che, per quanto attiene al profilo oggettivo del conflitto, il Tribunale ricorrente lamenta la lesione della propria sfera di attribuzioni, garantita da norme costituzionali, in conseguenza della deliberazione — ritenuta illegittima — con la quale la Camera dei deputati ha qualificato come insindacabili, ai sensi dell'art. 68, primo comma, Cost., le dichiarazioni rese dal parlamentare, cui si riferisce il procedimento penale in corso; 
      che, pertanto, esiste la materia di un conflitto, la cui risoluzione è affidata alla competenza della Corte.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzioni proposto dal Tribunale di Roma nei confronti della Camera dei deputati con il ricorso in epigrafe; &#13;
      dispone: &#13;
      a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al Tribunale di Roma ricorrente; &#13;
      b) che il ricorso e la presente ordinanza siano, a cura del ricorrente, notificati alla Camera dei deputati, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere poi depositati nella cancelleria di questa Corte entro il termine di venti giorni dalla notificazione, a norma dell'art. 26, terzo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 27 settembre 2004. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 29 settembre 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
