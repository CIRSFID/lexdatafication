<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/265/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/265/"/>
          <FRBRalias value="ECLI:IT:COST:2004:265" name="ECLI"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="265"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/265/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/265/ita@/!main"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/265/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/265/ita@.xml"/>
          <FRBRdate date="08/07/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="22/07/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>08/07/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfonso QUARANTA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 197-bis, comma 6, del codice di procedura penale, promosso con ordinanza del 4 luglio 2003 dal Tribunale di Novara  nel procedimento penale a carico di B.D. ed altro, iscritta al n. 746 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 38, prima serie speciale, dell'anno 2003. 
    Visto l'atto di  intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 9 giugno 2004 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che con l'ordinanza in epigrafe il Tribunale di Novara ha sollevato, in riferimento all'art. 3, primo comma, della Costituzione, questione di legittimità costituzionale dell'art. 197-bis, comma 6, del codice di procedura penale, nella parte in cui prevede che alle dichiarazioni rese dalle persone che assumono l'ufficio di testimone ai sensi del comma 1 dello stesso articolo si applica la disposizione di cui all'art. 192, comma 3, del codice di procedura penale, in forza della quale dette dichiarazioni sono valutate «unitamente agli altri elementi di prova che ne confermano l'attendibilità»; 
    che il giudice a quo premette che nel corso del dibattimento di un processo penale nei confronti di due imputati si era proceduto all'esame di un soggetto quale «testimone assistito» ai sensi dell'art. 197-bis, comma 1, cod. proc. pen., trattandosi di persona originariamente coimputata del medesimo reato, nei cui confronti era stata pronunciata sentenza irrevocabile di applicazione della pena ex art. 444 cod. proc. pen.; 
    che le dichiarazioni accusatorie rese da tale persona rappresentavano l'unico elemento di prova emerso a carico degli imputati, non sufficiente, peraltro, come tale, a fondare un giudizio di responsabilità; 
    che le dichiarazioni stesse non risultavano infatti corroborate da ulteriori elementi di prova atti a confermarne l'attendibilità, così come richiesto dalla regola di valutazione probatoria di cui all'art. 192, comma 3, cod. proc. pen., espressamente richiamata — quanto alle dichiarazioni dei «testimoni assistiti» — dall'art. 197-bis, comma 6, del medesimo codice; 
    che ad avviso del rimettente, tuttavia, la norma impugnata, nell'estendere ai «testimoni assistiti» la regola in parola, si porrebbe in contrasto con l'art. 3, primo comma, Cost. sotto un duplice profilo; 
    che da un lato, infatti, la disposizione censurata equiparerebbe ingiustificatamente — sul piano della valenza probatoria delle relative dichiarazioni — i «testimoni assistiti» agli imputati in un procedimento connesso o di reato collegato sentiti ai sensi dell'art. 210 cod. proc. pen.; e, dall'altro lato, li differenzierebbe altrettanto ingiustificatamente dai testimoni ordinari; 
    che, quanto al primo profilo, il giudice a quo rileva come la legge 1° marzo 2001, n. 63 (Modifiche al codice penale e al codice di procedura penale in materia di formazione e valutazione della prova in attuazione della legge costituzionale di riforma dell'articolo 111 della Costituzione) — modificando l'art. 197 cod. proc. pen. ed introducendo nel codice di rito il nuovo art. 197-bis — abbia enucleato, nell'ambito della categoria degli imputati in un procedimento connesso o di reato collegato ai sensi dell'art. 371, comma 2, lettera b), cod. proc. pen., i soggetti la cui posizione sia stata definita con sentenza irrevocabile — sia essa di condanna, di proscioglimento o di applicazione della pena ai sensi dell'art. 444 cod. proc. pen. — prevedendo per essi la possibilità di essere sentiti come testimoni; 
    che l'assunzione della qualità di testimone renderebbe peraltro la posizione dei soggetti in parola nettamente differenziata rispetto a quella degli altri imputati in un procedimento connesso o di reato collegato, che vengano sentiti ai sensi dell'art. 210 cod. proc. pen.; 
    che questi ultimi, infatti — a differenza dei primi — non hanno l'obbligo di dire la verità e, conseguentemente, nel caso di dichiarazioni false o reticenti, non commettono il reato di falsa testimonianza (art. 372 cod. pen.); 
    che, inoltre, gli imputati in procedimento connesso o di reato collegato, di cui all'art. 210 cod. proc. pen., sono ancora in attesa di giudizio definitivo: circostanza che, per un verso, permette di utilizzare a loro carico le dichiarazioni rese e, per un altro verso, può generare il sospetto che le loro dichiarazioni accusatorie siano suggerite dall'intento di scaricare su altri le proprie responsabilità o di fruire di benefici collegati alla collaborazione prestata; i «testimoni assistiti», invece, hanno ormai definito la propria posizione processuale, senza alcuna possibilità di modifiche peggiorative a seguito della deposizione, stante la garanzia della completa inutilizzabilità in loro danno delle dichiarazioni rese, prevista dal comma 5 del medesimo art. 197-bis; 
    che, pertanto, le ragioni processuali e sostanziali poste alla base della limitata efficacia probatoria — prevista dall'art. 192, commi 3 e 4, cod. proc. pen. — delle dichiarazioni rese dai soggetti ivi considerati, non sarebbero affatto ravvisabili rispetto ai «testimoni assistiti» di cui all'art. 197-bis, comma 1, cod. proc. pen.; 
    che, quanto al secondo profilo, la posizione dei testimoni in questione non differirebbe da quella dei testimoni ordinari, assumendo entrambi l'obbligo di dire la verità, ex artt. 198, comma 1, e 497, comma 2, cod. proc. pen., e le conseguenti responsabilità penali in caso di sua violazione; 
    che le discrepanze di regime riscontrabili, per il resto, tra l'una e l'altra categoria sarebbero soltanto apparenti o non significative: e così, in particolare, l'esenzione dall'obbligo di deporre su fatti per i quali è stata pronunciata sentenza di condanna (nel caso in cui non vi sia stata ammissione di responsabilità), prevista per il «testimone assistito» dall'art. 197-bis, comma 4, cod. proc. pen., corrisponderebbe alla garanzia contro l'autoincriminazione stabilita per i testimoni ordinari dall'art. 198, comma 2, cod. proc. pen.; l'inutilizzabilità delle dichiarazioni a proprio carico, sancita per i testi de quibus dall'art. 197-bis, comma 4, sarebbe «parallela» a quella prevista in via generale dall'art. 63 cod. proc. pen.; ed ancora, l'assistenza del difensore, imposta per i testi stessi dal comma 3 dell'art. 197-bis, si risolverebbe in una semplice «garanzia anticipata» rispetto a situazioni quali quelle contemplate dal comma 4 del medesimo articolo, garanzia peraltro accordata, a posteriori, dal citato art. 63 cod. proc. pen. anche al testimone comune, nel caso in cui renda «dichiarazioni autoindizianti»; 
    che la questione sarebbe infine rilevante nel giudizio a quo, in quanto, in caso di suo accoglimento, le dichiarazioni accusatorie rese dal «testimone assistito» potrebbero essere valutate alla stregua di qualsiasi altro elemento di prova e, dunque, ritenute idonee — «una volta superato il vaglio di credibilità intrinseca» — a fondare l'affermazione di responsabilità degli imputati pur in assenza di altri elementi di prova; 
    che nel giudizio di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata inammissibile o infondata. 
    Considerato che il giudice rimettente dubita della legittimità costituzionale, in riferimento all'art. 3, primo comma, Cost., dell'art. 197-bis, comma 6, cod. proc. pen., nella parte in cui rende applicabile ai c.d. «testimoni assistiti», di cui al comma 1 dello stesso articolo, la regola di valutazione probatoria sancita dall'art. 192, comma 3, cod. proc. pen., per effetto della quale le dichiarazioni rese da detti soggetti sono valutate «unitamente agli altri elementi di prova che ne confermano l'attendibilità»; 
    che il dubbio di costituzionalità poggia sull'assunto che la norma impugnata avrebbe, in tal modo, per un verso, ingiustificatamente equiparato i soggetti in parola agli imputati in un procedimento connesso o di reato collegato, sentiti ai sensi dell'art. 210 cod. proc. pen.; e, per un altro verso, ingiustificatamente differenziato i soggetti medesimi rispetto ai testimoni ordinari; 
    che, al riguardo, va peraltro rilevato come l'assetto normativo censurato rappresenti espressione della strategia di fondo che ha ispirato il legislatore della legge 1° marzo 2001, n. 63: strategia consistente nell'enucleare una serie di figure di 'dichiaranti' nel processo penale in base ai diversi 'stati di relazione' rispetto ai fatti oggetto del procedimento, secondo una graduazione che, partendo dalla situazione di assoluta indifferenza propria del teste ordinario, giunge fino alla forma 'estrema' di coinvolgimento, rappresentata dal concorso del dichiarante nel medesimo reato; 
    che ai vari 'stati di relazione' corrisponde quindi una articolata scansione normativa di figure soggettive, di modalità di dichiarazione e di effetti del dichiarato; 
    che, in tale ottica, e per quanto attiene specificamente all'odierna questione — concernente le dichiarazioni rese da persona già imputata del medesimo reato per il quale si procede, nei cui confronti è stata pronunciata sentenza irrevocabile di applicazione della pena ai sensi dell'art. 444 cod. proc. pen. — è sufficiente osservare che la totale equivalenza delle figure del teste ordinario e del teste 'assistito', postulata dal giudice a quo, non è, in realtà, affatto ravvisabile; 
    che la circostanza, infatti, che nei confronti dell'imputato in un procedimento connesso o di reato collegato ex art. 371, comma 1, lettera b), cod. proc. pen. sia stata pronunciata sentenza irrevocabile di “patteggiamento”, vale a differenziare la posizione del soggetto considerato rispetto a quella degli imputati in un procedimento connesso o di reato collegato ancora in attesa di giudizio definitivo: giustificando, così, la scelta legislativa di permettere l'audizione del soggetto stesso in veste di testimone, con correlata restrizione (nei limiti normativamente previsti) del 'diritto al silenzio'; ma tale circostanza non basta ancora a 'ripristinaré, alla stregua di una ragionevole valutazione del legislatore, la condizione di assoluta indifferenza rispetto alla vicenda oggetto di giudizio che è propria del teste ordinario; 
    che la norma censurata trova, in altre parole, la sua ratio fondante nella considerazione che chi è stato imputato in un procedimento connesso o di reato collegato ex art. 371, comma 2, lettera b), cod. proc. pen., anche dopo che è divenuta definitiva la sentenza di cui all'art. 444 cod. proc. pen., non è mai completamente 'terzo' rispetto alla imputazione cui la pena applicata si riferisce; l'originario coinvolgimento nel fatto lascia infatti residuare un margine di 'contiguità' rispetto al procedimento, che si riflette sulla valenza probatoria della dichiarazione; 
    che, in questa prospettiva, l'assoggettamento delle dichiarazioni del «teste assistito» alla regola della necessaria 'corroborazione' con riscontri esterni, di cui all'art. 192, comma 3, cod. proc. pen., lungi dal determinare un vulnus del principio di uguaglianza, si risolve in un esercizio — non irragionevole — della discrezionalità che al legislatore compete nella conformazione degli istituti processuali: e ciò tanto più a fronte del fatto che la regola censurata si inserisce in un più ampio 'corpo' di garanzie — quali quelle delineate dallo stesso art. 197-bis cod. proc. pen. — che, ad onta del contrario avviso del giudice a quo, riflettendo anch'esse la particolare relazione che lega il dichiarante alla regiudicanda, fanno in via generale del «testimone assistito» una figura significativamente differenziata, sul piano del trattamento normativo, rispetto al teste ordinario; 
    che le considerazioni che precedono escludono, altresì, che la sottoposizione delle dichiarazioni rese dai «testimoni assistiti» alla medesima regola di valutazione probatoria operante in rapporto alle dichiarazioni dei soggetti sentiti ai sensi dell'art. 210 cod. proc. pen. — quantunque soltanto i primi, e non i secondi, abbiano l'obbligo, penalmente sanzionato, di dire la verità — determini, sotto altro profilo, una compromissione del parametro costituzionale evocato: fermo restando che la sussistenza o meno di un obbligo di verità del dichiarante potrà essere comunque opportunamente valorizzata dal giudice, in sede di determinazione dell'entità del riscontro esterno idoneo a confermare l'attendibilità della dichiarazione di cui si tratta; 
    che la questione va dichiarata, pertanto, manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 197-bis, comma 6, del codice di procedura penale, sollevata, in riferimento all'art. 3, primo comma, della Costituzione, dal Tribunale di Novara con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della  Consulta, l'8  luglio 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Giovanni Maria FLICK, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 22 luglio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
