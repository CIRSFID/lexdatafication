<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/43/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/43/"/>
          <FRBRalias value="ECLI:IT:COST:2008:43" name="ECLI"/>
          <FRBRdate date="25/02/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="43"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/43/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/43/ita@/!main"/>
          <FRBRdate date="25/02/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/43/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/43/ita@.xml"/>
          <FRBRdate date="25/02/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="27/02/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfonso Quaranta</cc:relatore_pronuncia>
        <cc:data_decisione>25/02/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 10, comma 3, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione), promosso con ordinanza del 24 febbraio 2006 dal Tribunale ordinario di Perugia, sezione distaccata di Todi, nel procedimento penale a carico di R.G., iscritta al n. 290 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2007. 
    Udito  nella camera di consiglio del 13 febbraio 2008 il Giudice relatore Alfonso Quaranta.  
    Ritenuto che il Tribunale ordinario di Perugia, sezione distaccata di Todi, ha sollevato questione di legittimità costituzionale – in riferimento all'art. 3 della Costituzione – dell'art. 10, comma 3, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione); 
    che il rimettente premette, in punto di fatto, di essere chiamato a giudicare del reato previsto dagli artt. 624 e 625, primo comma, n. 4, del codice penale, da ritenere ormai estinto per prescrizione ove fosse possibile applicare, anche al giudizio principale, la nuova disciplina sui termini di prescrizione del reato introdotta dall'art. 6 della citata legge n. 251 del 2005; 
    che tuttavia, nel caso di specie, la già avvenuta dichiarazione di apertura del dibattimento (risalente al 21 ottobre 2004) impedisce, ai sensi di quanto previsto dal comma 3 dell'art. 10 della legge n. 251 del 2005, l'applicazione della lex mitior alla fattispecie oggetto del giudizio principale; 
    che reputa, tuttavia, il rimettente che la norma suddetta sia costituzionalmente illegittima per contrasto con l'art. 3 Cost., giacché, sebbene il legislatore sia libero di derogare al principio di retroattività della norma penale più favorevole, esso «non può eludere il principio di eguaglianza»; 
    che tale evenienza si sarebbe verificata, invece, nel caso di specie, essendo stato introdotto un regime transitorio che ha l'effetto «di far dipendere la retroattività della disciplina favorevole sopravvenuta da fattori estranei alla logica del trattamento sanzionatorio», ricollegandola «alla evoluzione del processo penale ed allo stato in cui esso sia pervenuto»; 
    che, inoltre, il legislatore, nell'attribuire rilievo – come condizione ostativa all'applicazione retroattiva dell'intervento in mitius – all'avvenuta dichiarazione di apertura del dibattimento,  risulta «avere individuato come sintomatico un certo momento processuale» che è, invece, «privo di qualsiasi rilievo» nella disciplina delle cause di interruzione della prescrizione. 
    Considerato che il Tribunale ordinario di Perugia, sezione distaccata di Todi, ha sollevato questione di legittimità costituzionale – in riferimento all'art. 3 della Costituzione – dell'art. 10, comma 3, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione); 
    che il rimettente censura tale norma nella parte in cui prevede che l'applicazione delle più favorevoli disposizioni per il reo in ordine al termine di prescrizione del reato, contenute nell'art. 6 della medesima legge n. 251 del 2005, sia limitata, quanto ai processi di primo grado, unicamente a quelli per i quali non «sia stata dichiarata l'apertura del dibattimento»; 
    che, successivamente alle ordinanze di rimessione, questa Corte, chiamata a pronunciarsi su questione analoga a quella in esame, con la sentenza n. 393 del 2006, ha dichiarato l'illegittimità costituzionale del predetto art. 10, comma 3, della legge n. 251 del 2005, limitatamente alle parole «dei processi già pendenti in primo grado ove vi sia stata la dichiarazione di apertura del dibattimento, nonché»; 
    che, secondo la citata sentenza, la scelta «compiuta dal legislatore – in relazione ai processi di primo grado già in corso – di subordinare l'efficacia, ratione temporis, della nuova disciplina sui termini di prescrizione dei reati (quando più favorevole per il reo) all'espletamento dell'incombente ex art. 492 cod. proc. pen.» non si conforma «al canone della necessaria ragionevolezza»; 
    che, difatti, tale incombente processuale non è idoneo «a correlarsi significativamente ad un istituto di carattere generale come la prescrizione, e al complesso delle ragioni che ne costituiscono il fondamento», in quanto esso «non connota indefettibilmente tutti i processi penali di primo grado (in particolare i riti alternativi – e, tra essi, il giudizio abbreviato – che hanno la funzione di “deflazionare” il dibattimento)», né risulta «incluso tra quelli ai quali il legislatore attribuisce rilevanza ai fini dell'interruzione del decorso della prescrizione ex art. 160 cod. pen., il quale richiama una serie di atti, tra cui la sentenza di condanna e il decreto di condanna, oltre altri atti processuali anteriori»; 
    che, alla luce di tale sopravvenuta decisione, vanno restituiti gli atti al giudice rimettente, ai fini di una rinnovata valutazione sulla rilevanza e non manifesta infondatezza della questione dallo stesso sollevata.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    ordina la restituzione degli atti al Tribunale ordinario di Perugia, sezione distaccata di Todi. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 25 febbraio 2008. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Alfonso QUARANTA, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 27 febbraio 2008. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
