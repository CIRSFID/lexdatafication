<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/208/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/208/"/>
          <FRBRalias value="ECLI:IT:COST:2016:208" name="ECLI"/>
          <FRBRdate date="22/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="208"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/208/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/208/ita@/!main"/>
          <FRBRdate date="22/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2016/208/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2016/208/ita@.xml"/>
          <FRBRdate date="22/06/2016" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="08/09/2016" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2016</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Giorgio Lattanzi</cc:relatore_pronuncia>
        <cc:data_decisione>22/06/2016</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giuseppe FRIGO, Alessandro CRISCUOLO, Giorgio LATTANZI, Aldo CAROSI, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Nicolò ZANON, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 2, commi 2-bis e 2-ter, della legge 24 marzo 2001, n. 89 (Previsione di equa riparazione in caso di violazione del termine ragionevole del processo e modifica dell'articolo 375 del codice di procedura civile), come aggiunti dall'art. 55, comma 1, lettera a), numero 2), del decreto-legge 22 giugno 2012, n. 83 (Misure urgenti per la crescita del Paese), convertito, con modificazioni, dall'art. 1, comma 1, della legge 7 agosto 2012, n. 134, promossi dalla Corte d'appello di Firenze, con ordinanze del 6 febbraio 2015 e 4 dicembre 2014, rispettivamente iscritte ai nn. 121, 122 e 123 del registro ordinanze 2015 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 26, prima serie speciale, dell'anno 2015.
          Visti gli atti di intervento del Presidente del Consiglio dei ministri;
          udito nella camera di consiglio del 22 giugno 2016 il Giudice relatore Giorgio Lattanzi.
 Ritenuto che, con ordinanza del 6 febbraio 2015 (r.o. n. 121 del 2015), la Corte  d'appello di Firenze ha sollevato questioni di legittimità costituzionale dell'art. 2, commi 2-bis e 2-ter, della legge 24 marzo 2001, n. 89 (Previsione di equa riparazione in caso di violazione del termine ragionevole del processo e modifica dell'articolo 375 del codice di procedura civile), in riferimento agli artt. 3, 111, secondo comma, e 117, primo comma, della Costituzione, quest'ultimo in relazione agli artt. 6 e 13 della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (d'ora in avanti «CEDU»), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con la legge 4 agosto 1955, n. 848;
 che il rimettente premette di dover decidere un ricorso, depositato nel 2014, con cui le parti hanno proposto una domanda di equa riparazione per l'eccessiva durata di un procedimento avviato proprio sulla base della legge n. 89 del 2001, a causa della eccessiva protrazione di altro giudizio. Il procedimento finalizzato al ristoro del pregiudizio subìto, a sua volta, ha avuto una durata complessiva di tre anni, sette mesi e diciotto giorni per due parti, e di tre anni, sette mesi e ventinove giorni per le altre due, e si è svolto in due gradi;
 che il giudice a quo rileva che l'art. 2, comma 2-ter, della legge n. 89 del 2001, introdotto dall'art. 55, comma 1, lettera a), numero 2), del decreto-legge 22 giugno 2012, n. 83 (Misure urgenti per la crescita del Paese), convertito, con modificazioni, dall'art. 1, comma 1, della legge 7 agosto 2012, n. 134, stabilisce che «[si] considera comunque rispettato il termine ragionevole se il giudizio viene definito in modo irrevocabile in un tempo non superiore a sei anni»;
 che in base a questa norma la domanda andrebbe rigettata;
 che la giurisprudenza di legittimità formatasi anteriormente alla novella del 2012 e la giurisprudenza della Corte europea dei diritti dell'uomo avrebbero indicato in due anni il limite di ragionevole durata del procedimento previsto dalla legge n. 89 del 2001;
 che analoga conclusione si imporrebbe oggi, in base al dettato costituzionale, considerato che questo procedimento ha carattere semplificato, si svolge in un unico grado di merito, accerta fatti di immediata evidenza e persegue finalità acceleratorie;
 che il legislatore, prescrivendo anche per questo procedimento un termine di durata ragionevole pari a sei anni, avrebbe violato gli artt. 3, 111, secondo comma, e 117, primo comma, Cost., quest'ultimo in relazione agli artt. 6 e 13 della CEDU;
 che il giudice rimettente censura, sulla base dei medesimi parametri e per analoghe ragioni, anche l'art. 2, comma 2-bis, della legge n. 89 del 2001, nella parte in cui determina la durata ragionevole del primo grado del giudizio in tre anni, e precisa che, nel caso di specie, il giudizio di primo grado ha avuto la durata di due anni e sette mesi;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni siano dichiarate inammissibili;
 che secondo l'Avvocatura generale il rimettente avrebbe potuto adottare un'interpretazione costituzionalmente orientata, in base alla quale ritenere che il limite di sei anni di durata complessiva non sia vincolante quando il procedimento ha carattere semplificato;
 che la questione sarebbe inammissibile anche perché non consentirebbe un intervento a "rime obbligate";
 che la questione relativa all'art. 2, comma 2-ter, sarebbe infondata, perché questa disposizione concerne i soli procedimenti articolati in tre gradi di giudizio;
 che, con ordinanza del 4 dicembre 2014 (r.o. n. 123 del 2015), la Corte d'appello di Firenze ha sollevato questioni di legittimità costituzionale dell'art. 2, commi 2-bis e 2-ter, della legge n. 89 del 2001, in riferimento agli artt. 3, 111, secondo comma, e 117, primo comma, Cost., quest'ultimo in relazione agli artt. 6 e 13 della CEDU;
 che il giudice a quo deve decidere su una domanda proposta ai sensi della legge n. 89 del 2001, e relativa ad un primo procedimento in base alla stessa legge, concluso in tre anni, tre mesi e venti giorni, in un unico grado;
 che il giudice rimettente dubita della legittimità costituzionale dell'art. 2, commi 2-bis e 2-ter, della legge n. 89 del 2001 e motiva la non manifesta infondatezza delle questioni sollevate con argomenti analoghi a quelli sviluppati dalla precedente ordinanza;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni siano dichiarate inammissibili, e, nel merito, non fondate;
 che, dopo avere ribadito le eccezioni di inammissibilità svolte nel precedente giudizio con riguardo alla questione vertente sull'art. 2, comma 2-ter, l'Avvocatura generale aggiunge che la Corte di cassazione, con la sentenza della sesta sezione civile, 6 novembre 2014, n. 23745, ha statuito che il termine di sei anni si applica ai soli processi articolati in tre gradi di giudizio. Il procedimento previsto dalla legge n. 89 del 2001 non sarebbe perciò disciplinato da tale disposizione, e spetterebbe al giudice decidere quale ne sia la ragionevole durata;
 che perciò la questione sarebbe non fondata;
 che, con ordinanza del 6 febbraio 2015 (r.o. n. 122 del 2015), la Corte d'appello di Firenze ha sollevato questioni di legittimità costituzionale dell'art. 2, comma 2-bis, della legge n. 89 del 2001, in riferimento agli artt. 3, 111, secondo comma, e 117, primo comma, Cost., quest'ultimo in relazione agli artt. 6 e 13 della CEDU;
 che il giudice a quo deve decidere su una domanda proposta ai sensi della legge n. 89 del 2001 e relativa ad un primo procedimento in base alla stessa legge concluso in tre anni, tre mesi e dodici giorni, in un unico grado;
 che, con argomenti analoghi a quelli già esposti, la Corte rimettente dubita della legittimità costituzionale dell'art. 2, comma 2-bis, della legge impugnata, nella parte in cui determina in tre anni la durata ragionevole del procedimento ex legge n. 89 del 2001;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni siano dichiarate inammissibili e, nel merito, non fondate, con argomenti analoghi a quelli sviluppati nei precedenti giudizi.
 Considerato che la Corte d'appello di Firenze, con tre ordinanze di analogo tenore, solleva questioni di legittimità costituzionale dell'art. 2, comma 2-bis (r.o. nn. 121, 122 e 123 del 2015) e comma 2-ter (r.o. nn. 121 e 123 del 2015), della legge 24 marzo 2001, n. 89 (Previsione di equa riparazione in caso di violazione del termine ragionevole del processo e modifica dell'articolo 375 del codice di procedura civile), in riferimento agli artt. 3, 111, secondo comma, e 117, primo comma, della Costituzione, quest'ultimo in relazione agli artt. 6 e 13 della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (d'ora in avanti «CEDU»), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con la legge 4 agosto 1955, n. 848;
 che i giudizi, vertendo sulle stesse disposizioni, meritano di essere riuniti per una decisione congiunta;
 che l'art. 2, comma 2-bis, determina la durata ragionevole del primo grado di un processo in tre anni;
 che i rimettenti dubitano della legittimità costituzionale di questa previsione, nella parte in cui si applica anche ai procedimenti regolati dalla legge n. 89 del 2001;
 che con la sentenza di questa Corte n. 36 del 2016, successiva alle ordinanze di rimessione, la disposizione impugnata è stata dichiarata costituzionalmente illegittima nella parte in cui si applica alla durata del processo di primo grado previsto dalla legge n. 89 del 2001;
 che, conseguentemente, le relative questioni di legittimità costituzionale sono manifestamente inammissibili, per carenza di oggetto (ex plurimis, ordinanza n. 26 del 2016);
 che l'art. 2, comma 2-ter, stabilisce che si considera rispettato il termine ragionevole se il giudizio viene definito in modo irrevocabile in un tempo non superiore a sei anni;
 che i rimettenti ritengono che tale norma, nella parte in cui si applica ai procedimenti previsti dalla legge n. 89 del 2001, sia costituzionalmente illegittima, in riferimento agli artt. 3, 111, secondo comma, e 117, primo comma, Cost., quest'ultimo in relazione agli artt. 6 e 13 della CEDU;
 che analoga questione è già stata decisa dalla sentenza n. 36 del 2016, anche con riferimento alle eccezioni di inammissibilità sollevate dall'Avvocatura generale dello Stato;
 che, quanto a queste ultime, va escluso che i rimettenti potessero reputare non vincolante il termine di sei anni, posto che ciò sarebbe in contrasto con la lettera della disposizione censurata;
 che, inoltre, non determina l'inammissibilità delle questioni l'omessa indicazione del termine di durata complessiva del procedimento, che sarebbe congruo, né la necessità che la sua determinazione sia rimessa alla discrezionalità legislativa, posto che i rimettenti denunciano il contrasto con la Costituzione del termine in questione, nella parte in cui sarebbe applicabile ai procedimenti previsti dalla legge n. 89 del 2001, e quindi per tale parte intendono eliminarlo e non sostituirlo;
 che la sentenza n. 36 del 2016 ha già chiarito che il termine di sei anni indicato dalla norma impugnata si applica ai soli procedimenti che in concreto si siano svolti in tre gradi di giudizio, al fine di compensare l'eccessiva protrazione di una fase con la maggiore celerità di un'altra;
 che, quindi, l'art. 2, comma 2-ter, impugnato non è mai applicabile ai procedimenti previsti dalla legge n. 89 del 2001, articolati dal legislatore in due soli gradi;
 che di conseguenza la norma impugnata non è applicabile nei procedimenti a quibus;
 che le relative questioni di legittimità costituzionale sono perciò inammissibili per difetto di rilevanza.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, commi 1 e 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 riuniti i giudizi,&#13;
 1) dichiara manifestamente inammissibili le questioni di legittimità costituzionale dell'art. 2, comma 2-bis, della legge 24 marzo 2001, n. 89 (Previsione di equa riparazione in caso di violazione del termine ragionevole del processo e modifica dell'articolo 375 del codice di procedura civile), sollevate, in riferimento agli artt. 3, 111, secondo comma, e 117, primo comma, della Costituzione, quest'ultimo in relazione agli artt. 6 e 13 della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con la legge 4 agosto 1955, n. 848, dalla Corte d'appello di Firenze, con le ordinanze indicate in epigrafe;&#13;
 2) dichiara inammissibili le questioni di legittimità costituzionale dell'art. 2, comma 2-ter, della legge n. 89 del 2001, sollevate, in riferimento agli artt. 3, 111, secondo comma, e 117, primo comma, Cost., quest'ultimo in relazione agli artt. 6 e 13 della CEDU, dalla Corte d'appello di Firenze, con le ordinanze iscritte ai nn. 121 e 123 del registro ordinanze 2015.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 giugno 2016.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Giorgio LATTANZI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria l'8 settembre 2016.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
