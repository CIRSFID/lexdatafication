<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/106/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/106/"/>
          <FRBRalias value="ECLI:IT:COST:2002:106" name="ECLI"/>
          <FRBRdate date="10/04/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="106"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/106/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/106/ita@/!main"/>
          <FRBRdate date="10/04/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/106/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/106/ita@.xml"/>
          <FRBRdate date="10/04/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/04/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Carlo Mezzanotte</cc:relatore_pronuncia>
        <cc:data_decisione>10/04/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: 
 Presidente: Cesare RUPERTO; 
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, 
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, 
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria 
FLICK, Francesco AMIRANTE;</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>Sentenza</docType>nel  giudizio  per  conflitto  di  attribuzione sorto a seguito della 
delibera  del Consiglio regionale della Liguria n. 62 del 15 dicembre 
2000 recante "Istituzione del Parlamento della Liguria", promosso con 
ricorso  del  Presidente  del  Consiglio  dei ministri, notificato il 
5 marzo  2001, depositato in cancelleria il 15 successivo ed iscritto 
al n. 11 del registro conflitti 2001. 
    Visto l'atto di costituzione della Regione Liguria; 
    Udito  nell'udienza  pubblica  del  12 febbraio  2002  il giudice 
relatore Carlo Mezzanotte; 
    Uditi   l'avvocato  dello  Stato  Ignazio  F.  Caramazza  per  il 
Presidente  del  Consiglio dei ministri e l'avvocato Luigi Cocchi per 
la Regione Liguria.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. - Il Presidente del Consiglio dei ministri ha proposto ricorso 
per conflitto di attribuzione, in riferimento agli articoli 1, 5, 55, 
115   (articolo   abrogato   dall'art. 9,   comma   2,   della  legge 
costituzionale  18 ottobre  2001,  n. 3  "Modifiche al titolo V della 
parte  seconda della Costituzione") e 121 della Costituzione, avverso 
la  delibera  n. 62  del  15 dicembre  2000 con la quale il Consiglio 
regionale  della  Liguria ha approvato la proposta di istituzione del 
Parlamento della Liguria. Tale delibera prevede che in tutti gli atti 
dell'assemblea  regionale,  alla  dizione costituzionalmente prevista 
"Consiglio   regionale  della  Liguria"  sia  affiancata  la  dizione 
"Parlamento della Liguria". 
    Secondo il ricorrente il cambiamento di denominazione dell'organo 
rappresentativo regionale, sia pure solo in via aggiuntiva, lederebbe 
la  sfera  di  attribuzioni  statali.  Si osserva in proposito che il 
nomen iuris degli organi connota tipicamente le funzioni che a quegli 
organi   sono  attribuite,  e  tale  generale  principio  assumerebbe 
particolare pregnanza in riferimento al nome "Parlamento", che, nella 
storia  costituzionale  moderna, identificherebbe l'organo attraverso 
il  quale  il  popolo  esprime  la  propria  sovranità, partecipando 
all'esercizio  del  potere  politico. Sebbene dunque sia teoricamente 
scorretto attribuire al Parlamento la qualifica di organo del popolo, 
aggiunge   l'Avvocatura,  non  potrebbe  dubitarsi  che  nel  sistema 
costituzionale  italiano, che esalta la "centralità" delle assemblee 
parlamentari, le due Camere siano gli organi costituzionali nei quali 
la volontà popolare più immediatamente ed efficacemente si esprime. 
La  posizione  eminente  che esse occupano nella struttura dei poteri 
statali   rifletterebbe   appunto   la  sovranità  popolare  che  il 
Parlamento  incarna  e  rappresenta e precluderebbe l'impiego di tale 
denominazione  con  riferimento  a  organi  della  regione,  che sono 
comunque  rappresentativi  di  poteri  di  autonomia  e non di poteri 
sovrani. 
    Lesivo delle attribuzioni statali pare alla difesa erariale anche 
il  secondo comma del provvedimento impugnato. In esso si delibera di 
assumere  i  principi  contenuti nelle premesse (principi comprensivi 
della  denominazione di cui si è detto) "quali linee di indirizzo da 
trasmettere  alla  Commissione speciale per lo Statuto e per la legge 
elettorale,  affinché  quest'ultima possa procedere agli adempimenti 
necessari  a  consentire  che gli stessi possano essere compiutamente 
attuati  in  sede  di  elaborazione del nuovo Statuto regionale". Una 
simile   previsione,   secondo  il  ricorrente,  pur  avendo  valenza 
meramente  ottativa,  lederebbe  le  prerogative  statali, intendendo 
preannunciare  l'approvazione  di  un  nuovo  statuto  regionale  che 
sarebbe  diretto a rivendicare alla regione ambiti di potere sovrano. 
Su  simili  premesse  il Presidente del Consiglio dei ministri chiede 
alla  Corte  di  dichiarare  che  non  spetta  al Consiglio regionale 
adottare  la  delibera  oggetto  del  ricorso,  e conseguentemente di 
annullarla. 
    2.  -  Si  è  costituito,  per la Regione Liguria, il Presidente 
della   Giunta  regionale,  chiedendo  che  il  ricorso  statale  sia 
dichiarato inammissibile o infondato. 
    Quanto  ai profili di inammissibilità, si denuncia il difetto di 
lesività   dell'atto   impugnato.   La  determinazione  assunta  dal 
Consiglio  regionale,  osserva  la  difesa  della regione, avrebbe un 
elevato  valore  simbolico,  ma,  in  termini  di  puro  diritto,  si 
risolverebbe   in  una  semplice  addizione  lessicale  alla  formula 
impiegata  in  Costituzione,  senza  che  ciò determini una modifica 
delle  competenze  e  delle  prerogative  dell'organo rappresentativo 
regionale.  Non  vi sarebbe, dunque, nell'atto oggetto del conflitto, 
alcuna  capacità  invasiva  delle  attribuzioni costituzionali dello 
Stato. 
    Nel  merito,  la difesa regionale contesta l'affermazione secondo 
la  quale  l'espressione  Parlamento  "sia sintomatica e coessenziale 
della  sovranità  dello  Stato", replicando che la sovranità è una 
caratteristica  dello  Stato  complessivamente considerato, mentre la 
denominazione   di   Parlamento   si   attaglierebbe   ad   assemblee 
rappresentative,   espressive   di   potere  popolare,  con  funzione 
legislativa  e  di  controllo  politico  sul Governo. Ad avviso della 
resistente   dovrebbe   considerarsi  infondata  anche  la  questione 
relativa  al secondo comma della deliberazione impugnata, che formula 
indirizzi  ai  fini  della  redazione del nuovo statuto, poiché tale 
previsione  non  presenterebbe  un contenuto lesivo, essendo priva di 
valore  giuridico  vincolante  nei  confronti  della commissione alla 
quale è diretta. 
    3.  -  Nella  pubblica  udienza del 12 febbraio 2002 l'Avvocatura 
dello  Stato, oltre a riprendere le argomentazioni spese nel ricorso, 
ha  soggiunto che le attribuzioni del Consiglio regionale, per quanto 
siano state fortemente potenziate dalla revisione del Titolo V, Parte 
II,  della Costituzione (legge costituzionale 18 ottobre 2001, n. 3), 
sarebbero   comunque   espressione  di  poteri  di  autonomia  e  non 
potrebbero  mai  attingere il livello della sovranità. In tal senso, 
secondo  la  difesa del Presidente del Consiglio dei ministri, con la 
delibera  impugnata la Regione Liguria si arrogherebbe la titolarità 
di una sovranità che in nessun modo le spetta. 
    Dal canto suo, la difesa della regione ha sostenuto che l'impiego 
del  nomen  Parlamento  nella  delibera  oggetto  del conflitto - che 
peraltro  esplicitamente riconosce la spettanza della sovranità allo 
Stato  nella  sua  unitarietà  -  troverebbe giustificazione proprio 
nella  marcata  assimilazione  funzionale  tra  assemblea legislativa 
statale  e  assemblea legislativa regionale alla quale hanno condotto 
le riforme costituzionali più recenti, tutte intese al rafforzamento 
delle  istituzioni  regionali  nella complessiva organizzazione dello 
Stato.  Particolare  significato  assumerebbe,  in  tale prospettiva, 
l'attribuzione  di  una amplissima potestà legislativa alle Regioni, 
per  effetto  del  superamento  del criterio della enumerazione delle 
materie  di  competenza regionale, cui era originariamente improntato 
l'art. 117   della  Costituzione,  e  l'accoglimento  del  principio, 
concettualmente   opposto,   della   residualità   della  competenza 
legislativa regionale (art. 117, quarto comma, della Costituzione).Considerato in diritto1. - Il Presidente del Consiglio dei ministri ha proposto ricorso 
per conflitto di attribuzione, in riferimento agli articoli 1, 5, 55, 
115   (articolo   abrogato   dall'art. 9,   comma   2,   della  legge 
costituzionale  18 ottobre  2001,  n. 3)  e  121  della  Costituzione 
avverso  la  delibera  n. 62  del  15 dicembre  2000, con la quale il 
Consiglio  regionale  della  Liguria,  da  un lato ha disposto che in 
tutti  i  propri atti la dizione "Consiglio regionale" sia affiancata 
da  quella  di  "Parlamento della Liguria"; dall'altro ha indirizzato 
alla  Commissione  statuto  la  direttiva  di  tenere  conto  di tale 
denominazione in sede di elaborazione del nuovo statuto regionale. 
    2. - Il ricorso deve essere accolto. 
    Già   un  approccio  puramente  testuale  al  tema  oggetto  del 
conflitto   induce   a   nutrire  forti  dubbi  sulla  conformità  a 
Costituzione  della deliberazione impugnata. Il termine "Parlamento", 
che  apre il Titolo I, Parte II, della Costituzione, si riferisce, ai 
sensi  dell'art. 55,  ai  due organi che lo compongono: la Camera dei 
deputati  e il Senato della Repubblica. L'art. 121 della Costituzione 
denomina invece Consiglio regionale l'organo che esercita le potestà 
legislative  attribuite  alla  regione  e  le  altre  funzioni che la 
Costituzione e le leggi gli conferiscono. 
    L'argomento  letterale,  seppure  non  privo  di valore, non può 
tuttavia  essere considerato decisivo se non viene saggiato alla luce 
degli  altri  canoni  della interpretazione costituzionale. Le stesse 
parti,  del  resto,  hanno avvertito la necessità di spingersi al di 
là  del  dato  testuale  allorché,  con opposti intendimenti, hanno 
addotto   elementi   storico-sistematici   per   corroborarlo  ovvero 
consentirne  il  superamento.  L'Avvocatura dello Stato insiste sulla 
distinzione-contrapposizione  tra sovranità popolare, della quale il 
solo  Parlamento  sarebbe  espressione,  e autonomia; la difesa della 
regione,  richiamandosi  alla  posizione  di perfetta equiordinazione 
che,  dopo  le  recenti  riforme  costituzionali,  si  sarebbe  ormai 
realizzata  tra  Parlamento  e  Consigli regionali, ritiene che anche 
questi  ultimi,  da  annoverare  a  pieno  titolo  tra  le  assemblee 
rappresentative,   possano,   per   analogia,   fregiarsi   del  nome 
Parlamento. 
    È  su  tali  antagonistiche prospettazioni che questa Corte deve 
portare il proprio esame. 
    3. - La  difesa  erariale, dunque, nel tentativo di rinvenire, al 
di  là  del  dato testuale, una più profonda ragione costituzionale 
del  carattere  esclusivo della denominazione "Parlamento" attribuita 
alle  assemblee  legislative  nazionali, pone l'accento sul fatto che 
siano  queste  la sede esclusiva, o anche soltanto preminente, in cui 
prende forma la sovranità del popolo. 
    Si    deve    in    proposito    osservare    che    il    legame 
Parlamento-sovranità   popolare   costituisce  inconfutabilmente  un 
portato  dei  principi democratico-rappresentativi, ma non descrive i 
termini  di una relazione di identità, sicché la tesi per la quale, 
secondo  la  nostra  Costituzione, nel Parlamento si risolverebbe, in 
sostanza, la sovranità popolare, senza che le autonomie territoriali 
concorrano a plasmarne l'essenza, non può essere condivisa nella sua 
assolutezza. 
    Sebbene   il   nuovo  orizzonte  dell'Europa  e  il  processo  di 
integrazione  sovranazionale  nel quale l'Italia è impegnata abbiano 
agito   in   profondità  sul  principio  di  sovranità,  nuovamente 
orientandolo  ed  immettendovi  virtualità  interpretative non tutte 
interamente   predicibili,   un   apparato   concettuale   largamente 
consolidato  nel nostro diritto costituzionale consente di procedere, 
proprio  sui  temi  connessi  alla sovranità, da alcuni punti fermi. 
L'articolo  1  della  Costituzione, nello stabilire, con formulazione 
netta  e  definitiva,  che  la  sovranità  "appartiene"  al  popolo, 
impedisce  di ritenere che vi siano luoghi o sedi dell'organizzazione 
costituzionale  nella quale essa si possa insediare esaurendovisi. Le 
forme  e  i  modi  nei quali la sovranità del popolo può svolgersi, 
infatti,  non si risolvono nella rappresentanza, ma permeano l'intera 
intelaiatura  costituzionale:  si  rifrangono in una molteplicità di 
situazioni  e  di  istituti  ed  assumono una configurazione talmente 
ampia  da  ricomprendere  certamente  il riconoscimento e la garanzia 
delle  autonomie  territoriali.  Per  quanto  riguarda queste ultime, 
risale  alla  Costituente  la  visione  per la quale esse sono a loro 
volta  partecipi dei percorsi di articolazione e diversificazione del 
potere  politico  strettamente legati, sul piano storico non meno che 
su  quello  ideale,  all'affermarsi del principio democratico e della 
sovranità popolare. 
    Il  nuovo  Titolo  V  -  con  l'attribuzione  alle  regioni della 
potestà  di determinare la propria forma di governo, l'elevazione al 
rango  costituzionale  del  diritto degli enti territoriali minori di 
darsi  un proprio statuto, la clausola di residualità a favore delle 
regioni,  che ne ha potenziato la funzione di produzione legislativa, 
il  rafforzamento della autonomia finanziaria regionale, l'abolizione 
dei  controlli statali - ha disegnato di certo un nuovo modo d'essere 
del  sistema  delle  autonomie.  Tuttavia i significativi elementi di 
discontinuità  nelle relazioni tra Stato e regioni che sono stati in 
tal  modo  introdotti  non  hanno intaccato le idee sulla democrazia, 
sulla  sovranità  popolare  e  sul principio autonomistico che erano 
presenti  e  attive  sin  dall'inizio  dell'esperienza  repubblicana. 
Semmai  potrebbe  dirsi  che il nucleo centrale attorno al quale esse 
ruotavano  abbia trovato oggi una positiva eco nella formulazione del 
nuovo  art. 114  della  Costituzione, nel quale gli enti territoriali 
autonomi   sono   collocati  al  fianco  dello  Stato  come  elementi 
costitutivi  della  Repubblica  quasi a svelarne, in una formulazione 
sintetica,  la  comune  derivazione dal principio democratico e dalla 
sovranità popolare. 
    In  conclusione,  se non lo si vuole racchiudere entro uno schema 
troppo   angusto  e  ormai  storicamente  inattendibile,  non  è  il 
principio  di  sovranità  popolare  a  poter fondare un'attribuzione 
costituzionale all'uso esclusivo della denominazione "Parlamento". 
    4. - D'altro   canto,   non   può   essere  accolta  neppure  la 
prospettiva  ricostruttiva  in  cui  si  pone  la Regione Liguria per 
superare  l'ostacolo  recato  dalla  lettera  della  Costituzione. La 
difesa regionale assume che la sostanziale parificazione di funzioni, 
nei  rispettivi  ambiti  di  competenza,  tra  Consiglio  regionale e 
Parlamento  renderebbe  legittima  l'estensione  anche al primo della 
denominazione  propria  del  secondo.  Questa  ricostruzione potrebbe 
avere  una  qualche  plausibilità  se  la denominazione degli organi 
direttivi della regione fosse collocata in uno spazio di indifferenza 
giuridica; solo allora sarebbe infatti possibile muovere alla ricerca 
di  una  nozione  "sostanziale"  di  Parlamento,  e, confortati dalla 
indagine   storica,   annettere   una  qualificazione  siffatta  alle 
assemblee  legislative titolari di una funzione rappresentativa delle 
popolazioni governate, dunque anche ai Consigli regionali. 
    È   tuttavia   di  ostacolo  alla  utilizzazione  dell'argomento 
analogico  la  circostanza che la Costituzione ha inteso pregiudicare 
questo  spazio  giuridico.  Essa  nel Titolo I, Parte II, attribuisce 
alle sole Camere il nome Parlamento, e definisce Consiglio regionale, 
nell'articolo  121, il titolare della funzione legislativa regionale. 
Gli organi direttivi della regione non sono dunque entità nuove nate 
negli  ordinamenti regionali in virtù delle modifiche introdotte nel 
Titolo  V  della Costituzione e prive di denominazioni proprie. Ed è 
vano  richiamare  profili  di  analogia  tra  Consiglio  regionale  e 
Parlamento, che erano evidenti al Costituente del 1948 - il quale con 
l'art. 121  della  Costituzione  (e con le corrispondenti norme degli 
statuti  speciali)  aveva  nondimeno  espresso chiaramente la propria 
scelta diversificatrice - così come si deve presumere lo siano stati 
al  legislatore costituzionale del 1999 e del 2001, che pure, proprio 
nel  momento  in  cui  si accingeva ad un rilevante potenziamento del 
ruolo  delle  autonomie, non ha ritenuto di mutare in "Parlamento" la 
denominazione dell'organo legislativo delle regioni. 
    Conviene  piuttosto individuare gli elementi che giustifichino la 
diversa  denominazione  costituzionale,  ed è fin troppo agevole, in 
questa  prospettiva,  rilevare  che  il termine Parlamento rifiuta di 
essere  impiegato  all'interno di ordinamenti regionali. Ciò non per 
il  fatto  che  l'organo  al  quale  esso  si  riferisce ha carattere 
rappresentativo  ed  è  titolare  di  competenze  legislative, ma in 
quanto  solo  il  Parlamento  è  sede  della rappresentanza politica 
nazionale  (art. 67  Cost.),  la  quale imprime alle sue funzioni una 
caratterizzazione  tipica  ed  infungibile.  In  tal  senso  il nomen 
Parlamento  non  ha  un valore puramente lessicale, ma possiede anche 
una  valenza  qualificativa,  connotando,  con l'organo, la posizione 
esclusiva  che  esso occupa nell'organizzazione costituzionale. Ed è 
proprio  la peculiare forza connotativa della parola ad impedire ogni 
sua  declinazione  intesa  a circoscrivere in ambiti territorialmente 
più  ristretti  quella funzione di rappresentanza nazionale che solo 
il  Parlamento  può  esprimere  e  che  è  ineluttabilmente evocata 
dall'impiego del relativo nomen. 
    5. - Le  considerazioni  fin  qui svolte consentono di apprezzare 
nella  pienezza del suo significato il valore deontico degli articoli 
55  e  121  della  Costituzione,  che si traduce in un vero e proprio 
divieto per i Consigli regionali di appropriarsi del nome Parlamento. 
Ne  consegue  che  la  dizione lessicale integrativa introdotta dalla 
Regione  Liguria,  intesa  ad  estendere anche al Consiglio regionale 
ligure  il  nomen  Parlamento, deve ritenersi illegittima, sicché il 
ricorso  per  conflitto  deve  essere accolto e la delibera impugnata 
annullata  anche  in riferimento alla sua seconda parte, con la quale 
il  Consiglio  regionale,  esorbitando  dalle  proprie attribuzioni e 
ledendo  quelle  statali,  invita la apposita commissione ad inserire 
nello  statuto  regionale  in corso di elaborazione una denominazione 
costituzionalmente non consentita per l'organo consiliare.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
                        LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p/>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso </block>
    </conclusions>
  </judgment>
</akomaNtoso>
