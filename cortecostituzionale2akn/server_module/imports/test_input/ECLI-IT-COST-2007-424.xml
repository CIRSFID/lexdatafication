<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/424/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/424/"/>
          <FRBRalias value="ECLI:IT:COST:2007:424" name="ECLI"/>
          <FRBRdate date="10/12/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="424"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/424/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/424/ita@/!main"/>
          <FRBRdate date="10/12/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/424/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/424/ita@.xml"/>
          <FRBRdate date="10/12/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/12/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Francesco Amirante</cc:relatore_pronuncia>
        <cc:data_decisione>10/12/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 13 del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'articolo 12 della legge 3 ottobre 2001, n. 366), promosso dal Tribunale di Bolzano nel procedimento civile vertente tra A. Z. ed altro e la Cassa di risparmio di Bolzano s.p.a. ed altra, con ordinanza del 14 aprile 2006 iscritta al n. 381 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 21, prima serie speciale, dell'anno 2007. 
    Visto l'atto di intervento del Presidente dl Consiglio dei ministri; 
    udito nella camera di consiglio del 7 novembre 2007 il Giudice relatore Francesco Amirante. 
    Ritenuto che, nel corso di un giudizio promosso da due privati nei confronti della Cassa di risparmio di Bolzano s.p.a. e di una dipendente dell'istituto bancario stesso al fine di ottenere il risarcimento del danno derivato da una operazione di investimento finanziario, il Tribunale di Bolzano, con ordinanza del 14 aprile 2006, ha sollevato, in riferimento agli artt. 3, 24 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 13 del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'articolo 12 della legge 3 ottobre 2001, n. 366); 
    che il giudice remittente chiarisce come, nel caso di specie, la dipendente della cui condotta l'istituto bancario è chiamato a rispondere in solido è rimasta contumace, mentre la Cassa di risparmio di Bolzano si è regolarmente costituita proponendo difese atte a contestare le deduzioni degli attori; 
    che il giudice a quo – dopo aver ricordato come la disposizione censurata stabilisca che, in caso di contumacia del convenuto, i fatti affermati dall'attore si intendono non contestati e il tribunale decide sulla domanda in base alla concludenza di questa – rileva, quanto al merito della questione, che, non avendo l'istituto bancario convenuto contestato di dover rispondere della condotta della propria dipendente come dedotta in causa, per effetto della disposizione censurata i fatti costitutivi della domanda dovrebbero ritenersi provati nei confronti della contumace e non ancora provati nei confronti dell'altra convenuta; 
    che, in tale situazione, ad avviso del remittente, all'interprete si porrebbero tre diverse alternative: quella di considerare comunque prevalente la prova dei fatti costitutivi maturatasi nei confronti della convenuta contumace, quella di considerare prevalenti le risultanze processuali conseguenti all'attività istruttoria richiesta dalla convenuta costituita, ovvero quella di considerare i fatti provati soltanto nei confronti della convenuta contumace e non ancora provati, e quindi suscettibili di verifica istruttoria, nei confronti dell'altra convenuta; 
    che, secondo il giudice a quo, la prima soluzione comporterebbe la violazione dell'art. 24 Cost., in quanto la convenuta costituita non potrebbe esercitare il proprio diritto di difesa, la seconda via sarebbe impraticabile risolvendosi in una palese disapplicazione della legge, mentre la terza soluzione potrebbe dare luogo a risultati del tutto contraddittori e contrastanti con il principio del giusto processo qualora l'attività istruttoria portasse ad escludere la fondatezza dei fatti costitutivi della domanda; 
    che, per tali ragioni, il Tribunale di Bolzano ritiene censurabile la disposizione di cui si tratta, con riguardo all'ipotesi in cui uno dei convenuti della cui condotta siano chiamati a rispondere in solido altri convenuti sia rimasto contumace; 
    che, quanto alla rilevanza, il remittente afferma che essa «è in re ipsa»; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'inammissibilità o l'infondatezza della questione. 
    Considerato che il Tribunale di Bolzano dubita, in riferimento agli artt. 3, 24 e 111 della Costituzione, della legittimità costituzionale dell'art. 13 del decreto legislativo 17 gennaio 2003, n. 5 (Definizione dei procedimenti in materia di diritto societario e di intermediazione finanziaria, nonché in materia bancaria e creditizia, in attuazione dell'articolo 12 della legge 3 ottobre 2001, n. 366); 
    che, ad avviso del remittente, qualora la disposizione censurata fosse letta nel senso di considerare prevalente la prova dei fatti costitutivi maturatasi nei confronti del convenuto contumace sarebbe leso il diritto di difesa del convenuto costituito in giudizio; se, invece, si considerasse prevalente la necessità di verifica istruttoria richiesta da quest'ultimo verrebbe palesemente disapplicata la legge; se, infine, si considerassero i fatti stessi provati nei confronti della parte contumace e da accertare nei confronti del convenuto costituito potrebbe pervenirsi a risultati contraddittori e lesivi del principio del giusto processo qualora l'attività istruttoria porti ad escludere la fondatezza dei fatti stessi; 
    che il giudice a quo non fa propria alcuna delle suddette tre interpretazioni della norma stessa; 
    che, comunque, questa Corte, investita medio tempore di analoga questione avente ad oggetto il comma 2 dell'art. 13 attualmente censurato, ha concluso nel senso dell'illegittimità costituzionale, per violazione dell'art. 76 Cost., di tale ultima disposizione «nella parte in cui stabilisce: “in quest'ultimo caso i fatti affermati dall'attore, anche quando il convenuto abbia tardivamente notificato la comparsa di costituzione, si intendono non contestati e il tribunale decide sulla domanda in base alla concludenza di questa”» (sentenza n. 340 del 2007); 
    che, dunque, alla stregua di tale decisione sopravvenuta, gli atti vanno restituiti al giudice remittente per una nuova valutazione della questione.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    ordina la restituzione degli atti al Tribunale di Bolzano. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 dicembre 2007. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Francesco AMIRANTE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 12 dicembre 2007. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
