<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/460/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/460/"/>
          <FRBRalias value="ECLI:IT:COST:2002:460" name="ECLI"/>
          <FRBRdate date="04/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="460"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/460/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/460/ita@/!main"/>
          <FRBRdate date="04/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2002/460/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2002/460/ita@.xml"/>
          <FRBRdate date="04/11/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/11/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>04/11/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale del combinato disposto degli artt. 409, comma 5, 415-bis e 552, comma 2, del codice di procedura penale promosso con ordinanza emessa il 10 dicembre 2001 dal Giudice per le indagini preliminari del Tribunale di Cuneo nel procedimento penale a carico di T.F. ed altra, iscritta al n. 96 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 11, prima serie speciale, dell'anno 2002. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 25 settembre 2002 il Giudice relatore Giovanni Maria Flick;  
    Ritenuto che, con ordinanza del 10 dicembre 2001, il Giudice per le indagini preliminari del Tribunale di Cuneo ha sollevato, in relazione agli articoli 24, secondo comma, 101, secondo comma, e 112 della Costituzione, questione di legittimità costituzionale del combinato disposto degli artt. 409, comma 5, 552, comma 2, e 415-bis del codice di procedura penale, nella parte in cui prevede che, nei reati a citazione diretta - in esito a richiesta di archiviazione, avanzata dal pubblico ministero oltre la scadenza dei termini di indagine e non accolta dal giudice delle indagini preliminari -, il pubblico ministero, richiesto di formulare dal giudice l'imputazione, debba provvedere a tale adempimento ed alla successiva emissione del decreto che dispone il giudizio senza il previo invio, all'indagato, dell'avviso di conclusione delle indagini preliminari di cui all'art. 415-bis cod.proc.pen., per l'avvenuta scadenza del termine delle stesse;  
    che il giudice rimettente - premesso di essere stato investito dal pubblico ministero di una richiesta di archiviazione, avanzata dopo la scadenza del termine delle indagini preliminari; di avere fissato, in esito all'opposizione proposta dal querelante, la relativa udienza camerale; e di dover comunque respingere, senza la necessità di ulteriori indagini, la richiesta medesima - muove dal rilievo che l'art. 415-bis cod.proc.pen. «pare, di fatto, precludere al giudice per le indagini preliminari il concreto esercizio dell'obbligo conferitogli dalla disposizione di cui al quinto comma dell'art. 409 cod.proc.pen.» e cioè l'ordine, al pubblico ministero, di formulare l'imputazione, nel caso di richiesta di archiviazione non accolta; 
    che infatti, secondo il giudice a quo, il mancato invio dell'avviso di conclusione delle indagini prima della relativa scadenza - adempimento la cui omissione provoca, a norma dell'art. 552, comma 2, cod.proc.pen., la nullità del successivo decreto di citazione a giudizio - determinerebbe l'esercizio di un'azione penale «geneticamente viziata», poiché il pubblico ministero, richiesto di formulare dal giudice l'imputazione, sarebbe tenuto ad emettere il decreto di citazione a giudizio senza aver dato tempestivamente all'imputato l'obbligatorio avviso di cui all'art. 415-bis cod.proc.pen.; 
    che risulterebbe pertanto violato l'art. 112 della Costituzione, in quanto il pubblico ministero verrebbe determinato «ad un esercizio dell'azione penale radicalmente nullo»; 
    che, inoltre, la normativa denunciata si porrebbe in contrasto con l'art. 24, secondo comma, della Costituzione, in quanto l'omissione dell'avviso previsto dall'art. 415-bis cod.proc.pen. priverebbe l'indagato «di un momento difensivo di assoluta rilevanza»;  e, infine, risulterebbe leso anche l'art. 101, secondo comma, della medesima Carta, in quanto il giudice - di fronte ad una richiesta di archiviazione  formulata dal pubblico ministero dopo la scadenza dei termini di indagine - «non potrebbe adempiere al dovere di sollecitare l'esercizio dell'azione penale validamente esperibile»;  
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata non fondata. 
    Considerato che la questione fonda la propria consistenza sulla premessa interpretativa secondo cui l'invio all'indagato dell'avviso di conclusione delle indagini preliminari, previsto dall'art. 415-bis cod.proc.pen., configuri un obbligo in capo al pubblico ministero che prescinde dalle determinazioni che il suddetto organo ritiene di dover assumere in ordine all'esercizio dell'azione penale; 
    che, viceversa, la lettera della legge è chiara nell'affermare che l'avviso in questione deve essere notificato soltanto nell'ipotesi in cui il pubblico ministero non debba «formulare richiesta di archiviazione ai sensi degli artt. 408 e 411» del codice di rito; 
    che, d'altra parte - posto che la funzione dell'avviso di cui al richiamato articolo 415-bis appare essere chiaramente quella di assicurare una fase di “contraddittorio” tra indagato e pubblico ministero, in ordine alla completezza delle indagini - consegue che l'espletamento di quella fase e la garanzia di uno specifico ius ad loquendum dell'indagato in tanto si giustificano, in quanto il pubblico ministero intenda coltivare una prospettiva di esercizio dell'azione penale; altrimenti, infatti, si determinerebbe un anomalo “controllo” dell'indagato in vista di un'eventuale richiesta di archiviazione, non soltanto del tutto superfluo nel quadro delle garanzie che il sistema deve approntare, ma addirittura “anticipato” rispetto allo specifico scrutinio riservato al giudice per le indagini preliminari; 
    che quando - come nel procedimento a quo - ricorre una ipotesi di esercizio dell'azione penale conseguente all'ordine di formulare l'imputazione a seguito di richiesta di archiviazione non accolta, il contraddittorio sulla eventuale incompletezza delle indagini trova necessariamente sede nella udienza in camera di consiglio, che il giudice è tenuto a fissare ove la domanda di “inazione” del pubblico ministero non possa trovare accoglimento; 
     che, pertanto, appare insussistente il preteso contrasto con l'art. 112  della Costituzione, in quanto nessuna nullità - per il mancato avviso di conclusione delle indagini ex art. 415-bis - può conseguire ove la citazione diretta sia imposta dal giudice; e, parimenti, nessuna lesione al diritto di difesa può prospettarsi in tale situazione, in quanto tale diritto è, nella specie, congruamente assicurato nella sede camerale che precede l'ordine di formulare l'imputazione; 
    che neppure è violato, infine, l'art. 101, secondo comma, della Costituzione, poiché tale censura muove dall'erroneo presupposto di ritenere che la citazione diretta del pubblico ministero, emanata a seguito dell'ordine del giudice, sia nulla ove non preceduta dall'avviso di cui all'art. 415-bis cod.proc.pen.; sicché, venendo meno tale premessa, si dissolve alla radice la fondatezza della tesi del rimettente, secondo la quale il giudice sarebbe indotto all'accoglimento della richiesta di archiviazione, per non imporre al pubblico ministero l'esercizio di un'azione penale radicalmente nulla; 
    che la questione deve, pertanto, ritenersi manifestamente infondata. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi  &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale del combinato disposto degli articoli 409, comma 5, 415-bis e 552, comma 2, del codice di procedura penale, sollevata, in riferimento agli artt. 24, 101 e 112 della Costituzione, dal Giudice per le indagini preliminari del Tribunale di Cuneo con l'ordinanza indicata in epigrafe. &#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 4 novembre 2002. &#13;
    F.to: &#13;
    Cesare RUPERTO, Presidente &#13;
    Giovanni Maria FLICK, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 19 novembre 2002. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
