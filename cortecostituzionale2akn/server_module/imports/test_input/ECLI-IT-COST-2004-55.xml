<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/55/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/55/"/>
          <FRBRalias value="ECLI:IT:COST:2004:55" name="ECLI"/>
          <FRBRdate date="20/01/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="55"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/55/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/55/ita@/!main"/>
          <FRBRdate date="20/01/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/55/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/55/ita@.xml"/>
          <FRBRdate date="20/01/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="29/01/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>20/01/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 20 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), promossi, nell'ambito di diversi procedimenti penali, dal Giudice di pace di Fano con ordinanze del 4 marzo 2003 e del 31 ottobre 2002, iscritte al n. 536, al n. 608 e al n. 667 del registro ordinanze 2003 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 33, 35 e 36, prima serie speciale, dell'anno 2003. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 17 dicembre 2003 il Giudice relatore Guido Neppi Modona. 
    Ritenuto che con tre ordinanze il Giudice di pace di Fano ha sollevato, in riferimento agli artt. 3, 24, secondo comma, e 97, primo comma, della Costituzione, questioni di legittimità costituzionale dell'art. 20 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui non prevede che nell'atto di citazione a giudizio davanti al giudice di pace siano indicate la facoltà dell'imputato di ricorrere a riti alternativi e le sanzioni conseguenti a tale carenza;  
    che il giudice a quo osserva che l'art. 52 del citato decreto legislativo ha mutato il quadro sanzionatorio per i reati attribuiti alla competenza del giudice di pace, consentendo l'applicazione sia dell'oblazione "volontaria" ex art. 162 cod. pen., sia di quella "discrezionale" prevista dall'art. 162-bis del medesimo codice, con particolare riferimento alle contravvenzioni già punite con pena congiunta dell'arresto e dell'ammenda e oggi punite con la pena alternativa dell'ammenda, della permanenza domiciliare o del lavoro di pubblica utilità; 
    che, a fronte di tale situazione, la disciplina censurata, nella parte in cui non prevede che la citazione a giudizio contenga, a pena di nullità, l'avviso che l'imputato può presentare domanda di oblazione, appare in contrasto con:  
    - l'art. 3 Cost., perché pone in essere una irragionevole e ingiustificata disparità di trattamento rispetto a quanto disposto in relazione al procedimento davanti al tribunale in composizione monocratica dall'art. 552, comma 1, lettera f), e comma 2, cod. proc. pen., ove è previsto non solo l'avviso, ma anche la nullità in caso di omissione; 
    - l'art. 24, secondo comma, Cost., perché incide sulla facoltà dell'imputato di chiedere tempestivamente di essere ammesso all'oblazione, che è espressione del diritto di difesa; 
    - l'art. 97, primo comma, Cost., perché l'imputato, stante l'assenza dell'informazione, «non viene incentivato ad accedere al rito alternativo»; 
    che il rimettente ricorda infine che la stessa Corte costituzionale ha dichiarato l'illegittimità costituzionale dell'art. 555, comma 2, cod. proc. pen., nel testo precedente la legge 16 dicembre 1999, n. 479 (che ha sostanzialmente trasfuso tale disposizione nell'attuale art. 552, comma 2, cod. proc. pen.), nella parte in cui non prevedeva la nullità del decreto di citazione a giudizio in caso di mancanza dell'avviso concernente la facoltà di chiedere i riti alternativi; 
    che nel giudizio promosso con ordinanza iscritta al n. 667 del registro ordinanze del 2003 è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente infondata. 
    Considerato che le ordinanze di rimessione, aventi uguale tenore testuale, sollevano la medesima questione e deve perciò essere disposta la riunione dei relativi giudizi; 
    che analoghe questioni sono già state dichiarate manifestamente infondate con ordinanza n. 231 del 2003 e successivamente con ordinanze n. 11 e n. 10 del 2004; 
    che in particolare nell'ordinanza n. 231 del 2003 questa Corte ha affermato che nell'udienza di comparizione l'imputato è obbligatoriamente assistito, a norma dell'art. 20, comma 2, lettera e), del decreto legislativo 28 agosto 2000, n. 274, «da un difensore, di fiducia o d'ufficio, sì che risultano pienamente garantite la difesa tecnica e l'informazione circa le varie forme di definizione del procedimento, anche alternative al giudizio di merito (conciliazione tra le parti, oblazione, risarcimento del danno, condotte riparatorie)» e che «l'udienza di comparizione, ove avviene il primo contatto tra le parti e il giudice, risulta sede idonea per sollecitare e verificare la praticabilità di possibili soluzioni alternative, tra cui, evidentemente, l'estinzione del reato per oblazione prevista dagli artt. 162 e 162-bis cod. pen.»; 
    che nell'occasione è stato ribadito che il principio di buon andamento dei pubblici uffici non si riferisce all'attività giurisdizionale in senso stretto, bensì all'organizzazione e al funzionamento dell'amministrazione della giustizia (cfr., ex plurimis, sentenza n. 115 del 2001); 
    che, non risultando profili diversi o aspetti ulteriori rispetto a quelli già valutati con le pronunce richiamate, le questioni devono essere dichiarate manifestamente infondate. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi,  &#13;
    dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'art. 20 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), sollevate, in riferimento agli artt. 3, 24, secondo comma, e 97, primo comma, della Costituzione, dal Giudice di pace di Fano, con le ordinanze in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 gennaio 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 29 gennaio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
