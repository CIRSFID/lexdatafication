<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/283/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/283/"/>
          <FRBRalias value="ECLI:IT:COST:2006:283" name="ECLI"/>
          <FRBRdate date="03/07/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="283"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/283/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/283/ita@/!main"/>
          <FRBRdate date="03/07/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/283/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/283/ita@.xml"/>
          <FRBRdate date="03/07/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/07/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Maria Rita Saulle</cc:relatore_pronuncia>
        <cc:data_decisione>03/07/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale degli artt. 13, commi 2, lettere a) e b), 3 e 7, 13-bis e 14, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), promossi con ordinanze del 16 aprile 2005 e del 18 luglio 2005 dai Giudici di pace di Milano e di Catanzaro, iscritte ai nn. 354 e 526 del registro ordinanze 2005 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 29 e n. 44, prima serie speciale, dell'anno 2005. 
      Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 7 giugno 2006 il Giudice relatore Maria Rita Saulle. 
    Ritenuto che il Giudice di pace di Milano, nel corso di un procedimento avente ad oggetto la opposizione al decreto di espulsione emesso nei confronti di un cittadino extracomunitario, con ordinanza del 16 aprile 2005, ha sollevato, in relazione all'art. 24 della Costituzione, questione di legittimità costituzionale dell'art. 13, comma 3, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui prevede che il provvedimento prefettizio di espulsione è immediatamente esecutivo, anche se sottoposto a impugnativa da parte dell'interessato; 
    che il rimettente, in punto di rilevanza, evidenzia che, non avendo lo straniero ricorrente presenziato all'udienza fissata per la comparizione delle parti, la sua assenza è «presumibilmente determinata dalla già avvenuta esecuzione dell'espulsione o dal timore di incorrere nella sanzione penale prevista dall'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998»; 
    che, quanto alla non manifesta infondatezza, il giudice a quo osserva che la disciplina relativa al decreto di espulsione nel prevedere, da un lato, l'immediata esecutività del citato decreto, al quale può seguire l'ordine di allontanamento dal territorio dello Stato e, dall'altro, il termine fino ad ottanta giorni per la decisione sull'opposizione avverso di esso, non garantisce  il diritto di difesa dello straniero, al quale viene di fatto preclusa la possibilità di presenziare all'udienza di convalida del decreto di espulsione, se non assumendosi il rischio di incorrere nelle conseguenze dell'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998; 
    che il contrasto della norma impugnata con i parametri costituzionali evocati sarebbe, secondo il giudice a quo, ancor più evidente in considerazione della previsione contenuta nell'art. 17 del d.lgs. n. 286 del 1998, che consente allo straniero di rientrare in Italia «per il tempo strettamente necessario per l'esercizio del diritto di difesa, al solo fine di partecipare al giudizio o al compimento di atti per i quali è necessaria la sua presenza», qualora egli sia parte offesa ovvero persona sottoposta ad indagini nell'ambito di un procedimento penale; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente inammissibile ovvero infondata; 
    che il Giudice di pace di Catanzaro, nel corso di un procedimento avente ad oggetto l'opposizione al decreto di espulsione di un cittadino extracomunitario e al contestuale ordine impartitogli dal questore di lasciare il territorio dello Stato, con ordinanza del 18 luglio 2005, ha sollevato, in relazione agli  artt. 3, 13, commi primo, secondo e terzo, e 24 della Costituzione, questione di legittimità costituzionale degli artt. 13, commi 2, lettere a) e b), e 7, 13-bis e 14, comma 5-bis, del d.lgs. n. 286 del 1998, nella parte in cui prevedono che il decreto di espulsione è «immediatamente esecutivo anche se sottoposto a gravame o impugnativa», che il consequenziale ordine del questore di lasciare il territorio dello Stato entro il termine di cinque giorni non è soggetto a convalida e che gli atti del procedimento di espulsione debbono essere tradotti in una lingua conosciuta allo straniero ovvero, ove non sia possibile, in francese, inglese e spagnolo; 
    che, secondo il rimettente, la mancata previsione del potere, in capo al giudice, di sospendere il decreto di espulsione sarebbe in contrasto con l'art 13 della Costituzione, in quanto tale atto inciderebbe sulla libertà personale dello straniero senza alcun controllo preventivo di legittimità; 
    che, a parere del giudice a quo, l'art. 14, comma 5-bis, del d.lgs. n. 286 del 1998 sarebbe in contrasto con i parametri costituzionali evocati, non prevedendo, diversamente da quanto stabilito per l'accompagnamento alla frontiera, alcuna convalida per il provvedimento di allontanamento dal territorio dello Stato; 
    che il rimettente ritiene le norme sopra indicate in contrasto, altresì, con l'art. 24 della Costituzione, in quanto precludono allo straniero la possibilità di «essere ascoltato dal giudice»; 
    che, secondo il giudice a quo, l'art. 13, comma 7, del d.lgs. n. 286 del 1998 nel prevedere la traduzione degli atti relativi al procedimento di espulsione in una lingua conosciuta al destinatario ovvero in francese, inglese e spagnolo, violerebbe il principio della conoscibilità degli atti giuridici; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile e/o infondata; 
    che la difesa erariale osserva che l'ordinanza di rimessione, del tutto priva della descrizione della fattispecie oggetto del giudizio a quo, non contiene alcuna motivazione in ordine alla rilevanza della questione, pur avendo il rimettente dato atto di aver sospeso l'efficacia del decreto di espulsione; 
    che, a parere dell'Avvocatura, la questione sarebbe, comunque, infondata avendo la Corte, con la sentenza n. 161 del 2000, dichiarato non fondata analoga questione riferita all'art. 11 della legge 6 marzo 1998, n. 40 (Disciplina dell'immigrazione e norme sulla condizione dello straniero), in quanto la mancata previsione di una tutela cautelare, nel caso di impugnativa del decreto prefettizio di espulsione dello straniero, si giustificava con la previsione normativa di un termine breve per la decisione del ricorso; 
    che, secondo l'Avvocatura, il richiamo all'art. 13 della Costituzione sarebbe inconferente, poiché le norme impugnate non incidono sulla libertà personale dei destinatari; 
    che, quanto alla questione relativa all'art. 13, comma 7, la difesa erariale osserva che la Corte, con la sentenza n. 257 del 2004, ne ha dichiarato l'infondatezza, evidenziando come la norma risponda a criteri di ragionevolezza. 
    Considerato che le ordinanze di rimessione propongono analoghe questioni, onde i relativi giudizi vanno riuniti per essere unitamente definiti; 
    che entrambe le ordinanze difettano della descrizione delle fattispecie concrete oggetto dei giudizi a quibus, con l'ulteriore limite che la questione sollevata dal Giudice di pace di Milano è, in ordine al requisito della rilevanza, del tutto ipotetica, essendosi il rimettente limitato ad affermare che la mancata comparizione in udienza dello straniero è «presumibilmente determinata dalla già avvenuta esecuzione dell'espulsione o dal timore di incorrere nella sanzione penale prevista dall'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998»; 
    che le questioni così sollevate devono essere, pertanto, dichiarate manifestamente inammissibili. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale degli artt. 13, commi 2, lettere a) e b), 3 e 7,  13-bis e 14, comma 5-bis, del decreto legislativo 25 luglio 1998 n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), sollevate, in riferimento agli artt. 3, 13, commi primo, secondo e terzo, e 24 della Costituzione, dal Giudici di pace di Milano e di Catanzaro con le ordinanze indicate in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 luglio 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 7 luglio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
