<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/120/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/120/"/>
          <FRBRalias value="ECLI:IT:COST:2007:120" name="ECLI"/>
          <FRBRdate date="21/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="120"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/120/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/120/ita@/!main"/>
          <FRBRdate date="21/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/120/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/120/ita@.xml"/>
          <FRBRdate date="21/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="05/04/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Sabino Cassese</cc:relatore_pronuncia>
        <cc:data_decisione>21/03/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 8-bis del decreto-legge 28 maggio 2004, n. 136 (Disposizioni urgenti per garantire la funzionalità di taluni settori della pubblica amministrazione), promosso con ordinanza del 20 marzo 2006 dal Tribunale amministrativo regionale della Puglia – sezione staccata di Lecce, sul ricorso proposto da Campa Marino ed altri nei confronti del Ministero dell'istruzione, dell'università e della ricerca ed altri , iscritta al n. 452 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 44, prima serie speciale, dell'anno 2006. 
    Udito nella camera di consiglio del 21 marzo 2007 il Giudice relatore Sabino Cassese.  
    Ritenuto che il Tribunale amministrativo regionale della Puglia – sezione staccata di Lecce, ha sollevato – in riferimento agli artt. 3, 4, 38 e 97 della Costituzione – questione di legittimità costituzionale dell'art. 8-bis del decreto-legge 28 maggio 2004, n. 136 (Disposizioni urgenti per garantire la funzionalità di taluni settori della pubblica amministrazione), convertito, con modificazioni, dalla legge 27 luglio 2004, n. 186; 
    che, secondo la disposizione impugnata, le riserve di posti previste dalla legge 12 marzo 1999, n. 68 (Norme per il diritto al lavoro dei disabili), si applicano alle procedure concorsuali relative al reclutamento dei dirigenti scolastici (art. 29 del decreto legislativo 30 marzo 2001, n. 165, recante «Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche»), incluse quelle per il conferimento degli incarichi di presidenza, di durata annuale, negli istituti e nelle scuole di istruzione secondaria, nei licei artistici e negli istituti d'arte; 
    che dinanzi al Tribunale pende il giudizio promosso da alcuni docenti – inseriti nella graduatoria provinciale «B» di Lecce relativa agli incarichi annuali di dirigenza scolastica per il settore formativo della scuola primaria e secondaria di primo grado per l'anno scolastico 2005/2006 – per l'annullamento, previa sospensiva, della graduatoria nella parte in cui è riconosciuto ai concorrenti, mediante l'annotazione accanto al loro nominativo delle sigle «N» (Invalido civile) ed «M» (Orfano o vedovo di guerra, per servizio o per lavoro), il diritto alla riserva dei posti in applicazione dell'art. 8-bis suddetto; 
    che il giudice rimettente, nel richiamare una precedente ordinanza di rimessione alla Corte su identica questione (poi decisa con la sentenza n. 190 del 2006), ne ripercorre le argomentazioni in ordine alla rilevanza e alla non manifesta infondatezza; 
    che, in particolare, si sofferma sull'interpretazione ritenuta più plausibile sia della legge n. 68 del 1999, sia della norma impugnata, ritenendo che presupposto della prima è lo stato di disoccupazione e presupposto della seconda lo stato di precedente occupazione, con ciò introducendosi una deroga al corretto principio per cui le quote di riserva nelle assunzioni presso le pubbliche amministrazioni postulano necessariamente lo stato di disoccupazione, anche dopo la legge n. 68 del 1999; 
    che, quanto alla non manifesta infondatezza, il Collegio richiama la giurisprudenza costituzionale in tema di assunzioni privilegiate nell'impiego pubblico, secondo la quale la tutela delle categorie protette, accordata consentendo il più agevole reperimento di un'occupazione, andrebbe ragionevolmente contemperata con l'interesse, di pari rango costituzionale, a che la pubblica amministrazione possa disporre di strumenti di selezione volti alla provvista di impiegati idonei allo svolgimento delle funzioni; 
    che, d'altra parte, aggiunge il Tribunale amministrativo regionale, nella Costituzione non è rinvenibile una tutela delle categorie protette estesa dal momento dell'ingresso nel mondo del lavoro al successivo sviluppo della carriera, prescindendo dall'interesse di altri soggetti, già presenti o aspiranti a farvi ingresso, oltre che dall'interesse dell'amministrazione; 
    che, pertanto, secondo il giudice a quo, l'art. 8-bis, contrasta: con l'art. 3 Cost., atteso che il riconoscimento della tutela incondizionata alle categorie protette comprimerebbe posizioni giuridiche professionali consolidate in capo ad altri soggetti, come nel caso di specie i controinteressati del giudizio principale; con l'art. 4 Cost., per gli stessi motivi, essendo il parametro evocato volto a promuovere le condizioni idonee a rendere effettivo l'esercizio del diritto al lavoro, mentre la norma censurata violerebbe il diritto al lavoro di coloro che perdono il posto; con l'art. 38, terzo comma, Cost., atteso che la norma impugnata, non limitandosi a favorire l'avviamento professionale, ma promuovendo indiscriminatamente lo sviluppo di carriera, supererebbe gli adeguati livelli di tutela imposti dal rispetto dei canoni di solidarietà che devono ispirare la legislazione sociale, specialmente in materia di impiego pubblico; con l'art. 97 Cost., violando i canoni di buon andamento e imparzialità, mediante la compressione dell'esigenza della pubblica amministrazione alla selezione dei soggetti maggiormente idonei a ricoprire le posizioni di responsabilità, in tal modo travalicando l'àmbito di tutela riconoscibile ai soggetti svantaggiati dagli artt. 3, 4, e 38 Cost. 
    Considerato che è all'esame della Corte la questione di legittimità costituzionale dell'art. 8-bis del decreto-legge 28 maggio 2004, n. 136 (Disposizioni urgenti per garantire la funzionalità di taluni settori della pubblica amministrazione), convertito, con modificazioni, dalla legge 27 luglio 2004, n. 186, sollevata in riferimento agli artt. 3, 4, 38 e 97 della Costituzione; 
    che, successivamente alla proposizione della questione, questa Corte, con la sentenza n. 190 del 2006, ha dichiarato l'illegittimità costituzionale dell'art. 8-bis del decreto-legge n. 136 del 2004, nella parte in cui si riferisce alle procedure per il conferimento degli incarichi di presidenza e, data l'evidente connessione tra le predette procedure e quelle concorsuali per il reclutamento dei dirigenti scolastici previste dall'art. 29 del d.lgs. n. 165 del 2001, cui rinvia la norma impugnata, ha dichiarato l'illegittimità costituzionale consequenziale della parte residua dello stesso articolo 8-bis;  
    che, pertanto, va ordinata la restituzione degli atti al giudice rimettente, al fine di una nuova valutazione della rilevanza della questione proposta, alla luce della predetta sopravvenuta sentenza di questa Corte n. 190 del 2006.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>  &#13;
    ordina la restituzione degli atti al giudice rimettente.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 21 marzo 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Sabino CASSESE, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 5 aprile 2007.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
