<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/324/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/324/"/>
          <FRBRalias value="ECLI:IT:COST:2007:324" name="ECLI"/>
          <FRBRdate date="11/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="324"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/324/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/324/ita@/!main"/>
          <FRBRdate date="11/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/324/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/324/ita@.xml"/>
          <FRBRdate date="11/07/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/07/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>11/07/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006 n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), e dell'art. 10 della stessa legge, promosso con ordinanza del 28 settembre 2006 dalla Corte d'appello di Palermo nel procedimento penale a carico di S.G., iscritta al n. 212 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 16, prima serie speciale, dell'anno 2007. 
      Udito nella camera di consiglio del 4 luglio 2007 il Giudice relatore Giovanni Maria Flick. 
    Ritenuto che la Corte d'appello di Palermo ha sollevato questioni di legittimità costituzionale: dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), in riferimento agli artt. 3, 111, secondo, sesto e settimo comma, e 112 della Costituzione; nonché dell'art. 10 della medesima legge, in riferimento agli artt. 3, 97 e 111, settimo comma, Cost.; 
    che il giudizio a quo trae origine dall'appello proposto dal pubblico ministero avverso una sentenza di assoluzione, prima dell'entrata in vigore della legge n. 46 del 2006 - il cui art. 1, sostituendo l'art. 593 cod. proc. pen., ha sottratto al pubblico ministero il potere di appellare le sentenze di proscioglimento - e che, in forza dell'art. 10 della medesima legge, dovrebbe essere dichiarato inammissibile; 
     che il rimettente dubita, in primo luogo, della legittimità costituzionale dell'art. 593 cod. proc. pen., nel testo novellato dalla legge n. 46 del 2006, nella parte in cui non consente al pubblico ministero di proporre appello avverso le sentenze di proscioglimento, se non nel caso previsto dall'art. 603, comma 2, dello stesso codice, ossia quando sopravvengano o si scoprano nuove prove dopo il giudizio di primo grado e sempre che tali prove risultino decisive; 
    che la disciplina censurata violerebbe innanzitutto gli artt. 3, 111, secondo comma, e 112 Cost., perché, privando il pubblico ministero - «chiamato ad esercitare la propria pretesa punitiva in ossequio al principio di obbligatorietà dell'azione penale» - del potere di proporre appello avverso le sentenze di proscioglimento, accorda all'organo della pubblica accusa un trattamento palesemente e irragionevolmente deteriore rispetto all'imputato, con sacrificio anche del principio della parità tra le parti; 
    che il giudice a quo - richiamata la giurisprudenza costituzionale in tema di giudizio abbreviato (ordinanza n. 421 del 2001) - osserva che, sebbene la previsione di limiti al potere di impugnazione del pubblico ministero non si ponga di per sé in contrasto con la Costituzione, la disparità di trattamento tra le parti deve pur sempre essere assistita da  una giustificazione che risponda a criteri di ragionevolezza;  
    che, invece, nella preclusione all'appello del pubblico ministero avverso le sentenze di proscioglimento difetterebbe «qualsiasi ragione giustificativa»; né la residua possibilità di appello in caso di prova nuova decisiva (ex art. 603, comma 2, cod. proc. pen.) consentirebbe di superare il dedotto profilo di incostituzionalità, trattandosi di ipotesi del tutto marginale; 
    che la disciplina censurata sarebbe inoltre irragionevole sotto due distinti profili: sia perché, essendo l'appello «una forma di garanzia contro gli errori contenuti nel giudizio di primo grado», la limitazione di esso ad una sola delle parti «impedisce di pervenire al risultato della decisione giusta cui mira qualsiasi processo»; sia in quanto viene mantenuto in capo al pubblico ministero il potere di proporre appello avverso le sentenze di condanna; 
    che il rimettente denuncia, altresì, la violazione dell'art. 111, commi primo (parametro non riprodotto in dispositivo e non assistito da specifica motivazione), sesto e settimo, Cost., assumendo che, per effetto delle modifiche recate dalla legge n. 46 del 2006 all'art. 606 cod. proc. pen. relativa alla disciplina dei motivi di ricorso per cassazione, «risulta notevolmente ed irragionevolmente estesa l'area del giudizio di merito della Cassazione, trasformata [… ] da giudice di legittimità, (anche) a giudice di merito», con conseguente possibile allungamento dei tempi di definizione dei processi; 
    che, quanto al regime transitorio introdotto dalla legge n. 46 del 2006, il rimettente dubita della legittimità costituzionale dell'art. 10 della medesima legge, nella parte in cui stabilisce l'immediata applicabilità del nuovo regime ai procedimenti in corso alla data di entrata in vigore della medesima legge, prevedendo, in particolare, al comma 2, che l'appello proposto contro una sentenza di proscioglimento prima della data di entrata in vigore della legge è dichiarato inammissibile con ordinanza non impugnabile; 
    che sarebbero violati l'art. 3 Cost., per l'«effetto retroattivo» che la disciplina censurata determina sui processi in corso, derogando senza alcuna plausibile giustificazione alla «regola della tutela dell'affidamento», ed il «principio di buon andamento dell'attività giudiziaria» (art. 97 Cost.); nonché l'art. 111, settimo comma, Cost., secondo cui contro le sentenze è sempre ammesso ricorso per Cassazione per violazione di legge (tale dovendosi ritenere, per il «suo contenuto definitorio», l'ordinanza con cui è dichiarato inammissibile l'appello), ed, ulteriormente, l'art. 3 Cost. sotto il profilo della ragionevolezza, atteso che la norma censurata «sconvolgerebbe l'intero sistema delle impugnazioni». 
    Considerato che il dubbio di costituzionalità sottoposto a questa Corte ha per oggetto la preclusione, conseguente alla modifica dell'art. 593 del codice di procedura penale ad opera dell'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), dell'appello delle sentenze dibattimentali di proscioglimento da parte del pubblico ministero e l'immediata applicabilità di tale regime, in forza dell'art. 10 della legge, ai procedimenti in corso alla data di entrata in vigore della medesima; 
    che, successivamente all'ordinanza di rimessione, questa Corte, con sentenza n. 26 del 2007, ha dichiarato l'illegittimità costituzionale dell'art. 1 della citata legge n. 46 del 2006 «nella parte in cui, sostituendo l'art. 593 del codice di procedura penale, esclude che il pubblico ministero possa appellare contro le sentenze di proscioglimento, fatta eccezione per le ipotesi previste dall'art. 603, comma 2, del medesimo codice, se la nuova prova è decisiva», e dell'art. 10, comma 2, della medesima legge, «nella parte in cui prevede che l'appello proposto contro una sentenza di proscioglimento dal pubblico ministero prima della data di entrata in vigore della medesima legge è dichiarato inammissibile»; 
    che, alla stregua della richiamata pronuncia di questa Corte, gli atti devono essere pertanto restituiti al giudice rimettente per un nuovo esame della rilevanza delle questioni.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi  &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>  &#13;
    ordina la restituzione degli atti alla Corte d'appello di Palermo. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il l'11 luglio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 24 luglio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
