<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/279/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/279/"/>
          <FRBRalias value="ECLI:IT:COST:2011:279" name="ECLI"/>
          <FRBRdate date="17/10/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="279"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/279/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/279/ita@/!main"/>
          <FRBRdate date="17/10/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/279/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/279/ita@.xml"/>
          <FRBRdate date="17/10/2011" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="21/10/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>QUARANTA</cc:presidente>
        <cc:relatore_pronuncia>Paolo Grossi</cc:relatore_pronuncia>
        <cc:data_decisione>17/10/2011</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alfonso QUARANTA; Giudici : Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Consiglio superiore della magistratura del 13 aprile 2011, relativa alla pratica n. 1R 1/VA/2011, promosso da Matteo Brigandì, nella qualità di consigliere, dichiarato decaduto, del Consiglio superiore della magistratura, con ricorso depositato in cancelleria il 2 maggio 2011 ed iscritto al n. 3 del registro conflitti tra poteri dello Stato 2011, fase di ammissibilità. 
 Udito nella camera di consiglio del 21 settembre 2011 il Giudice relatore Paolo Grossi.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che l'avv. Matteo Brigandì ha proposto conflitto di attribuzione tra poteri dello Stato nei confronti del Consiglio superiore della magistratura in riferimento alla deliberazione ed a taluni atti del relativo procedimento, con cui è stata dichiarata la sua decadenza da componente del Consiglio medesimo per la sussistenza di una causa di incompatibilità, deducendo che con il provvedimento di decadenza - del quale, in riferimento agli articoli 51 e 3 della Costituzione, si assume la illegittimità, in quanto la causa di incompatibilità era già stata rimossa «prima di qualsiasi contestazione» - verrebbero ad essere vulnerate attribuzioni di rango costituzionale proprie dei componenti del Consiglio superiore della magistratura derivanti dall'art. 104 Cost., con conseguente pregiudizio del corretto funzionamento dell'organo costituzionale, stante la modifica strutturale della sua composizione;
 che nella specie  - deduce il ricorrente - sussisterebbe la propria legittimazione a promuovere conflitto, in quanto si sarebbe determinata una interferenza rispetto alle proprie attribuzioni, che ha avuto come epilogo la estromissione dall'ufficio cui era stato eletto; né potrebbe opporsi che il venir meno della carica precluda la legittimazione dell'escluso, «dal momento che è proprio la illegittimità degli atti e della spoliazione subita a costituire la ragione della doglianza e, quindi, il fondamento della causa petendi»;
 che infatti - sottolinea il ricorrente - avuto riguardo alla assenza di norme espresse circa le conseguenze che possono scaturire dalla intervenuta cessazione di una causa di incompatibilità, doveva ritenersi escluso in radice il potere di dedurre nei confronti di chiunque una situazione di fatto non più sussistente, giacché, specie laddove siano in gioco interessi che attengano all'assetto costituzionale, la mancanza di presupposti normativamente previsti «non può avere le stesse conseguenze di quando si contrappongono interessi pubblici e interessi disponibili dei privati».
 Considerato che in questa fase del giudizio - a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87 - la Corte è chiamata a deliberare, con ordinanza in camera di consiglio e senza contraddittorio, circa l'ammissibilità del ricorso in relazione all'esistenza o meno della «materia di un conflitto la cui risoluzione spetti alla sua competenza», nonché alla sussistenza, oltre che dei requisiti oggettivi, anche di quelli soggettivi di cui al primo comma dello stesso articolo 37; 
 che, nella specie, difettano palesemente i presupposti di ammissibilità del ricorso, tanto sul piano della legittimazione soggettiva che su quello dei requisiti oggettivi, inerenti alla «delimitazione della sfera di attribuzioni determinata per i vari poteri da norme costituzionali»;
 che, infatti, quanto al requisito soggettivo, la deliberazione di decadenza avverso la quale il ricorrente insorge non vale a "trasformare" il medesimo, uti singulus, in organo competente «a dichiarare definitivamente la volontà del potere» costituzionalmente delineato, posto che il venir meno della sua qualità di componente dell'organo di rilevanza costituzionale non si è determinato in virtù di un atto o comportamento "esterno" all'organo medesimo, ma a seguito ed in dipendenza di una deliberazione "interna" ad esso, nell'esercizio di una prerogativa tipicamente attribuita, secondo il principio dell'autoriconoscimento della regolare composizione dell'organo;
 che, dunque, a seguire la tesi del ricorrente, si perverrebbe alla conseguenza - davvero singolare - per la quale il componente dell'organismo di rilevanza costituzionale, sicuramente privo, a livello individuale, della fisionomia e del carattere di "potere dello Stato" fintanto che faccia parte dell'organo stesso, possa, invece, divenire tale, ipso facto - e soltanto nei confronti di quell'organo - proprio quando, eventualmente, venga "estromesso" dal medesimo; pervenendo, tra l'altro, a generare, in capo a quel medesimo organo, una "legittimazione passiva" nel contraddittorio, in dipendenza di un atto da esso compiuto nei confronti di un proprio componente, così da dar vita ad una sorta di conflitto "soggettivamente impossibile", proprio perché di carattere esclusivamente e necessariamente "endo-organico";
 che del pari manifestamente carenti risultano anche i requisiti oggettivi del conflitto, posto che la materia trattata nel ricorso, e lo stesso petitum che ne costituisce l'epilogo, si pongono palesemente al di fuori del rigoroso perimetro entro il quale può svolgersi il giudizio per conflitto;
 che, al riguardo, è infatti noto - come questa Corte ha costantemente avuto modo di sottolineare - che il conflitto tra poteri dello Stato si configura quale strumento destinato a garantire la sfera delle attribuzioni costituzionalmente presidiate da ingerenze usurpative o menomative poste in essere da organi le cui funzioni sono anch'esse presidiate dalla Costituzione, restando invece esclusa la possibilità di utilizzare il giudizio per conflitto quale strumento generale di tutela dei diritti costituzionali, ulteriore rispetto a quelli offerti dal sistema della giurisdizione;
 che, pertanto, ove il contrasto non attenga direttamente al tema dei "poteri", o attenga solo indirettamente alla questione della loro "spettanza", riguardando, invece, propriamente quello dei relativi "diritti" che, in ipotesi, si assumano vulnerati da atti o comportamenti adottati nell'esercizio del potere e dei quali si deduca - come nella specie - la illegittimità, il rimedio specificamente azionabile è quello del reclamo in sede giurisdizionale comune, proprio perché non vi sarebbe materia per un intervento "regolatore" da parte di questa Corte;
 che, d'altra parte, ove così non fosse, all'odierno ricorrente sarebbe offerto, a fronte del medesimo vulnus, un duplice veicolo di reclamo (il ricorso per conflitto costituzionale, esercitato in quanto "potere" nei confronti dell'organo che ne ha deliberato la decadenza, e l'impugnativa rivolta quale comune cittadino davanti al giudice competente), con la concreta possibilità, tuttavia, di utilizzare il primo rimedio (tendente al ripristino dell'ordine costituzionale delle competenze) per obiettivi eventualmente perseguibili, in via esclusiva, con il secondo (diretto alla garanzia delle posizioni individuali asseritamente compromesse);
 che, per altro verso, e per concludere, una volta che si riconoscesse pregio all'argomento secondo cui la deliberazione di decadenza per incompatibilità - pronunciata da un organismo di rilevanza costituzionale nei confronti di un suo componente - necessariamente interferendo sulla composizione dell'organo, può precluderne l'esercizio delle funzioni e pregiudicarne il corretto funzionamento, irrimediabilmente si finirebbe per vanificare l'esercizio del potere di verifica delle compatibilità, che, al contrario, costituisce essenziale  attribuzione di "autotutela", specie per gli organi di rango costituzionale;
 che, di conseguenza, non sussistendo i requisiti soggettivi ed oggettivi per l'instaurazione del conflitto, il ricorso deve essere dichiarato inammissibile.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara inammissibile il ricorso per conflitto di attribuzione tra poteri dello Stato di cui in epigrafe, proposto dall'avv. Matteo Brigandì  nei confronti del Consiglio superiore della magistratura.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 17 ottobre 2011.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Paolo GROSSI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 21 ottobre 2011.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
