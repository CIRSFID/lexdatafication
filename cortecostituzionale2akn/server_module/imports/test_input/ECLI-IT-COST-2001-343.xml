<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2001/343/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2001/343/"/>
          <FRBRalias value="ECLI:IT:COST:2001:343" name="ECLI"/>
          <FRBRdate date="08/10/2001" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="343"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2001/343/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2001/343/ita@/!main"/>
          <FRBRdate date="08/10/2001" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2001/343/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2001/343/ita@.xml"/>
          <FRBRdate date="08/10/2001" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="24/10/2001" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2001</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>RUPERTO</cc:presidente>
        <cc:relatore_pronuncia>Riccardo Chieppa</cc:relatore_pronuncia>
        <cc:data_decisione>08/10/2001</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Cesare RUPERTO; Giudici : Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Giovanni Maria FLICK,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 5, comma 1, ultima parte, della legge 21 luglio 2000, n. 205 (Disposizioni in materia di giustizia amministrativa), promossi con ordinanze emesse il 25 ottobre 2000 dalla Corte dei conti, sezione giurisdizionale per la Regione Emilia-Romagna e l'8 novembre 2000 (n. 3 ordinanze) dalla Corte dei conti, sezione giurisdizionale per la Regione Siciliana, rispettivamente iscritte ai nn. 52, 219, 220 e 242 del registro ordinanze 2001 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 5 e 13, prima serie speciale, dell'anno 2001.
 Visti gli atti di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 20 giugno 2001 il Giudice relatore Riccardo Chieppa.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che la Corte dei conti, sezione giurisdizionale per l'Emilia-Romagna, con ordinanza del 25 ottobre 2000 (r.o. n. 52 del 2001), e la Corte dei conti, sezione giurisdizionale per la Regione Siciliana, con tre ordinanze, di analogo contenuto, dell'8 novembre 2000, depositate rispettivamente il 2 gennaio 2001 (r.o. n. 219 del 2001), il 21 dicembre 2000 (r.o. n. 220 del 2001) e il 2 gennaio 2001 (r.o. n. 242 del 2001), nel corso di separati giudizi cautelari in materia di pensioni, hanno sollevato questione di legittimità costituzionale, dell'art. 5, comma 1, ultimo periodo, della legge 21 luglio 2000, n. 205 (Disposizioni in materia di giustizia amministrativa) nella parte in cui prevede la competenza della Corte dei conti in composizione collegiale a provvedere per l'emanazione di provvedimenti cautelari nel giudizio pensionistico, nonostante la stessa disposizione istituisca la competenza a decidere il merito della controversia della stessa Corte dei conti in composizione monocratica, nella persona del magistrato designato, appartenente alla sezione giurisdizionale regionale competente per territorio, in funzione di giudice unico;
 che nella prima ordinanza (Corte dei conti sezione giurisdizionale per l'Emilia-Romagna) i sospetti di illegittimità costituzionale della richiamata disposizione sono fondati sulla violazione degli artt. 3 e 97 della Costituzione;
 che in particolare, al fine di sottolineare l'anomalia del nuovo sistema, che devolve, da una parte, la competenza a decidere il merito ad un organo monocratico e, dall'altra, prevede la competenza a pronunciare sulla domanda cautelare in capo ad un organo collegiale, il giudice a quo assume quali parametri di riferimento gli altri modelli processuali desunti: a) dal processo civile in materia cautelare; b) dal procedimento relativo al sequestro conservativo ante causam nel processo contabile che si svolge davanti alla stessa Corte dei conti per l'accertamento di responsabilità per danno erariale; c) dal nuovo processo amministrativo in sede cautelare regolato dall'art. 3 della predetta legge n. 205 del 2000;
 che nei primi due casi é competente ad emanare il provvedimento cautelare un organo giurisdizionale monocratico ed é fatta salva la facoltà di proporre reclamo davanti ad un organo collegiale;
 che nel terzo caso sia la prima delibazione, che la seconda, emessa in sede di appello, appartengono alla competenza di un organo collegiale;
 che per il processo pensionistico, invece, non sono previste norme particolari, limitandosi il dettato di legge a sovrapporre la competenza collegiale in sede cautelare a quella monocratica in sede di merito;
 che il giudice a quo, atteso che i richiamati modelli processuali alternativi non potrebbero trovare applicazione, poichè incompatibili col processo in materia di pensioni, si pone il problema di ricostruire questa figura di processo cautelare, ritenendo, in specie, che la soluzione apparentemente più corretta sarebbe quella già proposta in alcuni precedenti della stessa Corte dei conti (sezione I centrale, ord. n. 68/A del 19 settembre 1995), secondo i quali le ordinanze di sospensione dell'atto impugnato pronunciate dalle sezioni giurisdizionali regionali possono essere oggetto di richiesta di riesame; ciò, si precisa, con l'intento di replicare in sede cautelare il doppio grado di giudizio previsto per la giurisdizione di merito;
 che tuttavia, se questa affermazione fosse corretta - sempre secondo il giudice rimettente - si dovrebbe ammettere che il provvedimento cautelare pronunciato dall'organo collegiale competente in primo grado possa essere impugnato davanti ad una sezione d'appello, e che la decisione di questa, conclusiva del giudizio interinale ed anch'essa collegiale, possa essere posta nel nulla da una sentenza di merito contraria pronunciata dal giudice monocratico; la quale ultima, oltretutto, potrebbe a sua volta essere appellata con istanza cautelare di sospensione immediata dei suoi effetti;
 che tale complessivo sistema sarebbe, a parere del rimettente, farraginoso, palesemente contrastante con gli obiettivi di semplificazione ed accelerazione processuale che la stessa legge n. 205 del 2000 intende perseguire ed, in definitiva, contrastante con i valori di buon andamento dell'azione amministrativa e con il parametro generale di ragionevolezza;
 che qualora, poi, si ritenesse inapplicabile un rimedio di impugnazione dell'ordinanza cautelare nel silenzio normativo, il disegno legislativo non sfuggirebbe comunque alle censure di incostituzionalità, restando caratterizzato dall'anomala sovrapposizione della competenza collegiale a quella del giudice unico; il quale ultimo, oltretutto, sarebbe fatalmente in posizione di "sudditanza" rispetto alla decisione del collegio, con evidente squilibrio della tutela interinale e d'urgenza rispetto alla cognizione di merito;
 che, invece, secondo la Corte dei conti, se fosse dichiarata l'illegittimità costituzionale della norma suindicata, sarebbe possibile, perdurando il silenzio del legislatore, applicare analogicamente gli artt. 669-bis e seguenti cod.proc.civ., adeguando così il processo cautelare in materia pensionistica sia al processo cautelare civile sia al processo cautelare previsto in materia di sequestro conservativo nel giudizio contabile, con immediati riflessi sul giudizio a quo;
 che nelle altre tre successive ordinanze la questione di costituzionalità é stata sollevata dalla Corte dei conti, sezione giurisdizionale per la Regione Siciliana con riferimento agli artt. 3 e 25 della Costituzione;
 che in linea con quanto già osservato dalla prima ordinanza, la Corte dei conti denuncia l'irragionevolezza di una scelta che, in assenza di qualsivoglia giustificazione concreta o sistematica, valicherebbe i margini della discrezionalità legislativa: un sistema che ripartisce la competenza collegiale e quella monocratica nei termini descritti non avrebbe precedenti e sarebbe gravemente difforme rispetto ai modelli più vicini al giudizio pensionistico, il processo cautelare nel processo di responsabilità contabile ed il processo cautelare nel rito del lavoro, pure richiamato, quest'ultimo, nel giudizio della Corte dei conti in tema di pensioni;
 che le tre ultime ordinanze segnalano, poi, che i sospetti di irragionevolezza della disciplina si accentuerebbero alla luce degli effetti che può determinare l'art. 9 della legge n. 205 del 2000, che affiderebbe al collegio chiamato a valutare la domanda cautelare il potere di decidere il merito della controversia con sentenza succintamente motivata, quando il ricorso appaia manifestamente fondato, o manifestamente infondato, irricevibile, inammissibile o improcedibile;
 che secondo la Corte dei conti, sezione giurisdizionale per la Sicilia, la trasformazione del rito avrebbe come effetto automatico anche la trasformazione dell'organo decidente; ciò comporterebbe la modificazione del giudice naturale precostituito per legge, dipendente non già da una chiara e verificabile scelta del legislatore, ma da circostanze occasionali e variabili, consistenti nel fatto che le parti abbiano posto il collegio in condizioni di provvedere nel merito e nella stessa determinazione facoltativa del collegio di decidere con sentenza, chiudendo la fase processuale; quest'evenienza, se da una parte sarebbe coerente con le finalità di accelerazione processuale perseguite dalla legge, dall'altra parte realizzerebbe - proprio a causa dell'anomalia di partenza che contraddistingue il processo cautelare - un arbitrario sovvertimento di competenze;
 che ne seguirebbe, secondo la Corte dei conti, sezione giurisdizionale per la Sicilia, la violazione dell'art. 25 della Costituzione;
 che in tutti i giudizi é intervenuta, innanzi alla Corte, la Presidenza del Consiglio dei ministri, difesa dall'Avvocatura generale dello Stato, che ha chiesto che la questione di legittimità costituzionale sia dichiarata inammissibile e comunque manifestamente infondata, illustrandone le relative ragioni.
 Considerato che, stante la identità dell'oggetto, i giudizi introdotti con le sopra indicate ordinanze vanno riuniti per essere decisi con un'unica pronuncia; 
 che la norma denunciata, contenuta nell'art. 5, comma 1, della legge 21 luglio 2000, n. 205, riguarda esclusivamente la composizione monocratica della Corte dei conti per giudicare "in primo grado" in materia di ricorsi pensionistici, civili, militari e di guerra, e la composizione collegiale della stessa Corte dei conti in ogni caso ("sempre") di pronuncia "in sede cautelare";
 che pertanto la innovazione legislativa si riduce alla assegnazione della competenza a decidere i ricorsi pensionistici ad un magistrato assegnato alla sezione giurisdizionale regionale competente per territorio, con la qualificazione "in funzione di giudice unico", mentre si mantiene la composizione collegiale per la Corte dei conti in sede cautelare secondo la preesistente normativa;
 che, di conseguenza, devono rimanere estranei alla questione sollevata tutti gli altri profili attinenti alla possibilità di richiesta di riesame o di impugnazione delle ordinanze cautelari e alle modalità di applicazione del rinvio operato dall'art. 9, comma 3, della legge n. 205 del 2000 per i giudizi innanzi alla Corte dei conti in materia pensionistica in ordine alla determinazione del giudice (unico o in composizione collegiale) per le decisioni in forma semplificata, in quanto al di fuori del contenuto della norma denunciata, di cui il giudice rimettente doveva fare applicazione nella fase della rimessione;
 che detti ultimi profili dovranno essere risolti, quando se ne presenterà la concreta applicazione, dall'organo giurisdizionale a quo sulla base dell'interpretazione del complesso delle norme processuali relative ai giudizi pensionistici avanti alla Corte dei conti;
 che la sollevata questione di legittimità costituzionale dell'art. 5, comma 1, della legge 21 luglio 2000, n. 205, é manifestamente infondata in quanto, da un canto, rientra nella discrezionalità del legislatore la valutazione della opportunità di differenziare in una determinata materia (nella specie pensionistica) la composizione dell'organo giurisdizionale (monocratico o collegiale) destinato a pronunciare in sede cautelare, o a giudicare sui ricorsi della stessa materia, con il generale limite della non palese arbitrarietà e irragionevolezza;
 che tale limite, nella specie, non risulta violato attesa la ratio della norma denunciata di accelerare e semplificare la definizione dei ricorsi "in primo grado" (v. per riferimenti a misure di accelerazione dei processi sentenza n. 427 del 1999), mantenendo, invece, indistintamente, la composizione collegiale per le pronunce "in sede cautelare" sempre per la materia pensionistica, senza che sia compromessa la precostituzione del giudice, in quanto la determinazione della composizione dell'organo giudicante avviene sulla base di una regola generale fissata dalla legge con criteri obiettivi e predeterminati (monocratico per la decisione dei ricorsi in primo grado, collegiale per le pronunce "in sede cautelare");
 che, infatti, la precostituzione del giudice é rispettata tutte le volte che l'organo giudicante risulti istituito sulla base di criteri generali prefissati per legge (ordinanza n. 159 del 2000), essendo sufficiente che la legge determini criteri oggettivi e generali, capaci di costituire un discrimen della competenza o della giurisdizione di ogni giudice (ordinanza n. 176 del 1998; v. anche sentenza n. 419 del 1998, n. 217 del 1993 e n. 269 del 1992; ordinanza n. 257 del 1995);
 che non esiste nel sistema di "indipendenza" di alcun organo giurisdizionale (degno di tale status) una posizione di sudditanza tra giudice monocratico e giudice collegiale (per riferimenti v. sentenza n. 272 del 1998), allo stesso modo come non esiste condizionamento della pronuncia cautelare sulla definizione del ricorso, se non nel senso che la decisione di rigetto del ricorso in primo grado supera la fase cautelare, sovrapponendosi alla precedente pronuncia cautelare di sospensione ed eliminandone gli effetti, salvi gli eventuali ulteriori provvedimenti cautelari del giudice di secondo grado;
 che é inappropriato il parametro di riferimento dell'art. 97 della Costituzione, poichè il principio di buon andamento e di imparzialità della pubblica amministrazione, pur potendo interessare anche gli organi dell'amministrazione della giustizia per quanto attiene esclusivamente alle leggi concernenti l'ordinamento degli uffici giudiziari e il loro funzionamento sotto l'aspetto amministrativo, é estraneo all'esercizio della funzione giurisdizionale nel suo complesso e in relazione ai diversi provvedimenti che costituiscono espressione del suo esercizio o alla ripartizione delle competenze (ordinanze n. 490 e 30 del 2000; n. 11 del 1999; sentenza n. 272 del 1998; ordinanze n. 189, n.103 e n. 7 del 1997; sentenze n. 122 del 1997; n. 281 del 1995 e n. 376 del 1993);
 che ciò non esclude che anche la funzione giurisdizionale debba essere regolata da leggi che assicurino il buon andamento e l'imparzialità, in quanto connaturale all'esercizio di ogni giurisdizione, senza la quale non avrebbe significato la soggezione dei giudici solo alla legge (v. sentenza n. 387 del 1999), che deve essere conforme ai principi costituzionali, ivi compresi quelli contenuti negli articoli 3, 24, 25, primo comma, e nella Parte II, Titolo IV, della Costituzione;
 che il legislatore nella sua discrezionalità può adottare norme processuali differenziate tra i diversi tipi di giurisdizioni e di riti procedimentali anche nell'ambito della stessa giurisdizione, non essendo tenuto, sul piano costituzionale, ad osservare una uniformità rispetto al processo civile o amministrativo, proprio per le ragioni che possono giustificare la pluralità di giurisdizioni, le diversità processuali e le differenze delle tipologie dei riti speciali (v. per l'autonomia e particolarità dei diversi sistemi processuali ordinanze n. 30 del 2000 e n. 359 del 1998; sentenza n. 53 del 1998; sulla rilevanza delle speciali esigenze dei singoli procedimenti, a proposito del diritto di difesa, purchè non siano pregiudicati lo scopo e le funzioni, sentenza n. 119 del 1995 e n. 220 del 1994);
 che, pertanto, tutti i profili denunciati, relativi agli artt. 3, 97 e 25 della Costituzione, sono manifestamente infondati.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>
 riuniti i giudizi,
 dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'art. 5, comma 1, ultimo periodo, della legge 21 luglio 2000, n. 205 (Disposizioni in materia di giustizia amministrativa), sollevate, in riferimento rispettivamente agli artt. 3 e 97 della Costituzione e agli artt. 3 e 25 della Costituzione, dalla Corte dei conti, sezione giurisdizionale per l'Emilia-Romagna, e sezione giurisdizionale per la Regione Siciliana, con le ordinanze indicate in epigrafe.
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8 ottobre 2001.
 F.to:
 Cesare RUPERTO, Presidente
 Riccardo CHIEPPA, Redattore
 Giuseppe DI PAOLA, Cancelliere</block>
    </conclusions>
  </judgment>
</akomaNtoso>
