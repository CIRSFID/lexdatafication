<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/424/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/424/"/>
          <FRBRalias value="ECLI:IT:COST:2008:424" name="ECLI"/>
          <FRBRdate date="03/12/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="424"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/424/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/424/ita@/!main"/>
          <FRBRdate date="03/12/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2008/424/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2008/424/ita@.xml"/>
          <FRBRdate date="03/12/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="17/12/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>FLICK</cc:presidente>
        <cc:relatore_pronuncia>Alfonso Quaranta</cc:relatore_pronuncia>
        <cc:data_decisione>03/12/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 126-bis, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), come modificato dall'art. 2, comma 164, lettera b), del decreto-legge 3 ottobre 2006, n. 262 (Disposizioni urgenti in materia tributaria e finanziaria), convertito, con modificazioni, dall'art. 1, comma 1, della legge 24 novembre 2006, n. 286, promosso con ordinanza del 31 marzo 2008 dal Giudice di pace di Montefiascone nel procedimento civile vertente tra B.A. e il Comune di Montefiascone, iscritta al n. 248 del registro ordinanze 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 36, prima serie speciale, dell'anno 2008.  
    Udito nella camera di consiglio del 3 dicembre 2008 il giudice relatore Alfonso Quaranta.  
    Ritenuto che il Giudice di pace di Montefiascone ha sollevato – in riferimento agli articoli 3 e 24 della Costituzione – questione di legittimità costituzionale dell'articolo 126-bis, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), come modificato dall'art. 2, comma 164, lettera b), del decreto-legge 3 ottobre 2006, n. 262 (Disposizioni urgenti in materia tributaria e finanziaria), convertito, con modificazioni, dall'art. 1, comma 1, della legge 24 novembre 2006, n. 286; 
    che il remittente premette di essere investito – ai sensi dell'art. 22 della legge 24 novembre 1981, n. 689 (Modifiche al sistema penale) – della richiesta di annullamento di un verbale di contravvenzione amministrativa elevato dalla locale polizia municipale, con il quale si è contestata, al ricorrente nel giudizio a quo, la violazione dell'art. 126-bis, comma 2, del codice della strada, avendo costui omesso di comunicare, in assenza di «giustificato e documentato motivo», i dati personali e della patente di guida del soggetto resosi in precedenza responsabile, a bordo della vettura di proprietà di esso ricorrente, della violazione dell'art. 142 dello stesso codice; 
    che, sempre in punto di fatto, il remittente deduce che la sanzione pecuniaria prevista dal censurato comma 2 dell'art. 126-bis risulta essere stata irrogata, nel caso di specie, sebbene il proprietario del veicolo avesse tempestivamente dichiarato «di non essere responsabile dell'infrazione e di non essere nel contempo in grado di fornire il nominativo del conducente, giacché il veicolo suddetto era in uso a tutti i componenti della sua famiglia (moglie e due figli), con conseguente impossibilità di risalire all'effettivo trasgressore»; 
    che, ciò premesso, il giudice a quo rammenta che in base all'attuale formulazione della norma censurata il proprietario del veicolo (ovvero altro obbligato in solido, ai sensi dell'articolo 196 del medesimo codice della strada) «deve fornire all'organo di polizia che procede, entro sessanta giorni dalla data di notifica del verbale di contestazione, i dati personali e della patente del conducente al momento della commessa violazione», allorché quest'ultimo non sia stato identificato in occasione dell'accertamento di taluna di quelle infrazioni stradali per le quali è prevista l'applicazione, a titolo di sanzione accessoria, anche della decurtazione del punteggio dalla patente di guida; 
    che, inoltre, a carico del proprietario del veicolo (o dell'obbligato in solido per il pagamento della sanzione pecuniaria), il quale ometta, «senza giustificato e documentato motivo», di fornire i dati richiesti, è prevista l'applicazione della sanzione amministrativa costituita dal pagamento di una somma da euro 250 a euro 1.000; 
    che nell'ordinanza di rimessione si evidenzia che il vigente testo del comma 2 dell'art. 126-bis costituisce il risultato di una modifica introdotta dal legislatore (con il già ricordato art. 2, comma 164, lettera b, del decreto-legge n. 262 del 2006, nel testo risultante dalla relativa legge di conversione, n. 286 del 2006) per adeguarsi alle indicazioni fornite dalla Corte costituzionale con la sentenza n. 27 del 2005;  
    che prima, infatti, dell'intervento della Corte la norma in contestazione prevedeva che, in caso di mancata identificazione del conducente responsabile di un'infrazione stradale, «la segnalazione della perdita dei punti di patente dovesse essere effettuata a carico del proprietario del veicolo, salvo comunicazione da parte di quest'ultimo, da effettuarsi entro trenta giorni dalla richiesta dell'organo di polizia che procede, dei dati personali e del conducente al momento della commessa violazione», prevedendosi, inoltre, che l'omessa comunicazione fosse anche sanzionata sul piano pecuniario con l'irrogazione della medesima sanzione prevista per la violazione dell'art. 180, comma 8, del codice della strada; 
    che, tuttavia, la riconosciuta «natura personale della sanzione della decurtazione dei punti di patente» – osserva ancora il remittente – ha indotto la Corte a ritenere irragionevole la originaria scelta legislativa di addossare la stessa ad un soggetto diverso dall'effettivo responsabile dell'infrazione stradale;  
    che alla parziale declaratoria di illegittimità costituzionale della norma suddetta ha fatto seguito l'intervento del legislatore, il quale ha limitato sul piano patrimoniale gli effetti del mancato adempimento dell'obbligo di comunicazione e li ha anche subordinati alla condizione che l'omissione non dipenda da un «documentato e giustificato motivo»; 
    che il remittente reputa, però, che la scelta compiuta dal novellato testo del comma 2 dell'art. 126-bis, consistente nell'avere «subordinato l'esonero dalla sanzione alla ricorrenza di un giustificato e documentato motivo», abbia comunque determinato «un'eccessiva compressione del diritto di difesa spettante ai soggetti su cui grava l'obbligo di trasmissione dei dati», con conseguente violazione dell'art. 24 Cost., giacché costoro «si trovano nella difficile situazione di dover documentare a posteriori la sussistenza di una circostanza giustificatrice in relazione alla commissione di un illecito amministrativo di cui, prima della notifica del verbale, possono senza colpa non aver avuto notizia»; 
    che il proprietario del veicolo, dunque, «si trova costretto» – secondo il giudice a quo – «a doversi procurare ex post e per iscritto la prova dell'esimente in relazione ad un evento fino ad allora legittimamente ignorato»; 
    che è dedotta, inoltre, la violazione dell'art. 3 Cost., atteso che la disposizione censurata, irragionevolmente, riserverebbe «ai destinatari dell'obbligo di trasmissione dati un trattamento ingiustificatamente più severo rispetto a quello previsto dall'art. 180, comma 8», del medesimo codice della strada; 
    che, difatti, quest'ultima disposizione (richiamata, oltretutto, dal testo originario dall'art. 126-bis, comma 2, proprio al fine di individuare la sanzione pecuniaria applicabile all'infrazione da esso prevista) stabilisce «l'esonero dalla sanzione» – da irrogare nel caso della «mancata ottemperanza» all'invito dell'autorità a presentarsi, entro il termine stabilito nell'invito medesimo, «ad uffici di polizia per fornire informazioni o esibire documenti ai fini dell'accertamento delle violazioni amministrative previste dal codice» – in presenza «di un giustificato motivo, senza alcun dovere di allegazione documentale»;  
    che tale differente trattamento appare al remittente, oltre che contrario al principio di eguaglianza, anche «illogico», specie in considerazione del contenuto – a suo dire «identico» – delle due disposizioni, come confermerebbe la circostanza che, prima della riforma operata nel 2006, l'art. 180, comma 8, del codice della strada era espressamente richiamato dall'art. 126-bis, comma 2, del medesimo codice nel fissare l'entità della sanzione pecuniaria irrogabile in caso di omessa comunicazione dei dati personali e della patente del conducente; 
    che, d'altra parte, la scelta del legislatore neppure potrebbe ritenersi giustificata ipotizzando che essa abbia inteso «compensare la maggiore gravosità dell'onere giustificativo introdotta a seguito della modifica con una sanzione pecuniaria d'importo più lieve rispetto a quella inflitta in precedenza» (per effetto proprio del riferimento all'art. 180, comma 8), giacché il piano relativo «alle modalità di esercizio del diritto di difesa» e quello concernente «il quantum della pretesa sanzionatoria» non dovrebbero essere «confusi»; 
    che su tali basi, quindi, il Giudice di pace di Montefiascone – ritenuta la questione sollevata «rilevante ai fini del decidere», giacché l'applicazione dell'art. 126-bis, comma 2, del codice della strada, nel caso di specie, «comporterebbe il rigetto dell'opposizione», dal momento che il ricorrente si è limitato a dichiarare, «sia pure per iscritto e nel termine previsto dalla norma», di «non essere in grado di ricordare» chi fosse alla guida della propria autovettura, «senza documentare in alcun modo tale circostanza» – ha chiesto che sia dichiarata l'illegittimità costituzionale della norma censurata.  
    Considerato che il Giudice di pace di Montefiascone ha sollevato – in riferimento agli articoli 3 e 24 della Costituzione – questione di legittimità costituzionale dell'articolo 126-bis, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), come modificato dall'art. 2, comma 164, lettera b), del decreto-legge 3 ottobre 2006, n. 262 (Disposizioni urgenti in materia tributaria e finanziaria), convertito, con modificazioni, dall'art. 1, comma 1, della legge 24 novembre 2006, n. 286; 
    che, secondo il remittente, la disposizione censurata – subordinando alla ricorrenza di un «giustificato e documentato motivo» l'esonero dalla sanzione pecuniaria, prevista a carico di chi non comunichi «i dati personali e della patente del conducente» (responsabile di taluna di quelle infrazioni stradali assoggettate anche alla sanzione della decurtazione del punteggio dalla patente di guida) – determinerebbe «un'eccessiva compressione del diritto di difesa spettante ai soggetti su cui grava l'obbligo di trasmissione dei dati», con conseguente violazione dell'art. 24 Cost., giacché costoro «si trovano nella difficile situazione di dover documentare a posteriori la sussistenza di una circostanza giustificatrice in relazione alla commissione di un illecito amministrativo di cui, prima della notifica del verbale, possono senza colpa non aver avuto notizia»; 
    che essa, inoltre, riserverebbe, irragionevolmente, «ai destinatari dell'obbligo di trasmissione dati un trattamento ingiustificatamente più severo rispetto a quello previsto dall'art. 180, comma 8», del medesimo codice, giacché quest'ultima disposizione – richiamata, oltretutto, dal testo originario dall'art. 126-bis, comma 2, proprio al fine di individuare la sanzione pecuniaria applicabile all'infrazione da esso prevista – stabilisce «l'esonero dalla sanzione», da irrogare in caso di «mancata ottemperanza all'invito dell'autorità di presentarsi entro il termine stabilito nell'invito medesimo ad uffici di polizia per fornire informazioni o esibire documenti ai fini dell'accertamento delle violazioni amministrative previste dal codice», in presenza «di un giustificato motivo, senza alcun dovere di allegazione documentale»; 
    che questa Corte – nel vagliare la legittimità costituzionale della norma censurata, innanzitutto in relazione al secondo dei profili appena indicati, presentando lo stesso carattere chiaramente pregiudiziale – deve ribadire che, anche con riferimento alla disciplina della circolazione stradale, «l'individuazione delle condotte punibili e la scelta e la quantificazione delle relative sanzioni rientrano nella discrezionalità del legislatore» (così, da ultimo, e con specifico riferimento proprio alla sanzione della decurtazione del punteggio della patente di guida, l'ordinanza n. 204 del 2008); 
    che, pertanto, «tale discrezionalità può essere oggetto di censura, in sede di scrutinio di costituzionalità, soltanto ove il suo esercizio ne rappresenti un uso distorto o arbitrario, così da confliggere in modo manifesto con il canone della ragionevolezza» (ex multis, ordinanza n. 169 del 2006); 
    che tale evenienza, però, deve escludersi nel caso di specie, solo che si abbia riguardo alla centralità che il censurato comma 2 dell'art. 126-bis riveste nel sistema della cd. “patente a punti”; 
    che, difatti, questa Corte ha già sottolineato come «l'obbligo di comunicazione posto a carico del proprietario del veicolo tende ad assicurare l'irrogazione di una sanzione (la decurtazione del punteggio dalla patente di guida) nei confronti del conducente resosi responsabile di un'infrazione stradale», presentando, pertanto, «carattere strumentale» alla soddisfazione di un interesse – la repressione, appunto, delle infrazioni stradali – «il cui collegamento con la tutela dell'ordine e della sicurezza pubblica» è stato a più riprese ribadito dalla giurisprudenza costituzionale (sentenza n. 165 del 2008); 
    che quest'ultima, difatti, ha ripetutamente evidenziato come «la disciplina contenuta nel codice della strada, specie quella di natura sanzionatoria», sia diretta «a soddisfare «l'esigenza, connessa alla strutturale pericolosità dei veicoli a motore, di assicurare l'incolumità personale dei soggetti coinvolti nella loro circolazione (conducenti, trasportati, pedoni)» (sentenze n. 165 del 2008 e n. 428 del 2004; ordinanza n. 247 del 2005); 
    che è di tutta evidenza come un ruolo preminente, in questa prospettiva, il legislatore abbia inteso assegnare proprio alla sanzione delle decurtazione del punteggio dalla patente di guida, atteso che la sua applicazione tende a garantire che – a fronte di ripetute (e gravi) violazioni del codice della strada – possa giungersi all'adozione di una misura destinata ad incidere «sul profilo della legittimazione soggettiva alla conduzione di ogni veicolo, gravando sul relativo atto amministrativo di abilitazione, a seguito dell'accertata trasgressione di regole di comportamento afferenti alla sicurezza della circolazione» (sentenza n. 27 del 2005); 
    che tali rilievi, pertanto, danno conto del particolare rigore con il quale il legislatore ha disciplinato la prova idonea ad esonerare da responsabilità il proprietario (ovvero, l'obbligato in solido ai sensi dell'art. 196 del medesimo codice della strada), il quale ometta di comunicare «i dati personali e della patente del conducente al momento della commessa violazione», giacché tale contegno impedisce l'identificazione proprio del soggetto a carico del quale dovrebbe essere comminata la sanzione della decurtazione del punteggio dalla patente di guida; 
    che, d'altra parte, la scelta legislativa di esigere l'allegazione di un «giustificato e documentato motivo», idoneo a dare conto dell'omessa comunicazione, oltre a non essere irragionevole, per i motivi già indicati, non presenta neppure il paventato profilo di contrasto con l'art. 24 Cost.; 
    che, invero, il remittente propone una lettura della norma censurata viziata da un evidente meccanicismo; 
    che, difatti, non è corretto affermare che la disposizione in contestazione costringe i soggetti tenuti alla comunicazione «a doversi procurare ex post e per iscritto la prova dell'esimente», giacché l'onere di documentazione, su di essi gravante, non investe l'impossibilità di comunicare, bensì semplicemente – come ritenuto anche dalla giurisprudenza di merito formatasi in ordine all'applicazione della norma de qua – quelle circostanze fattuali idonee a rivelare la non esigibilità, nel singolo caso di specie, dell'obbligo di trasmissione dei dati; 
    che, del resto, questa Corte – già con riferimento all'interpretazione del testo originario dell'art. 126-bis, comma 2, del codice della strada – non solo ha affermato «la necessità di distinguere il comportamento di chi si disinteressi della richiesta di comunicare i dati personali e della patente del conducente, non ottemperando, così, in alcun modo all'invito rivoltogli (contegno per ciò solo meritevole di sanzione) e la condotta di chi abbia fornito una dichiarazione di contenuto negativo», ma anche posto in luce come la ricorrenza di tale seconda ipotesi debba essere accertata «sulla base di giustificazioni, la idoneità delle quali ad escludere la presunzione relativa di responsabilità a carico del dichiarante dovrà essere vagliata dal giudice comune, di volta in volta, anche alla luce delle caratteristiche delle singole fattispecie concrete sottoposte al suo giudizio» (così la sentenza n. 165 del 2008; ma si vedano anche, in termini analoghi, le ordinanze n. 282 del 2008, n. 434 del 2007 e n. 244 del 2006); 
    che, pertanto, la questione sollevata dal Giudice di pace di Montefiascone è manifestamente infondata in relazione ad entrambi i parametri evocati.  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'articolo 126-bis, comma 2, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), come modificato dall'art. 2, comma 164, lettera b), del decreto-legge 3 ottobre 2006, n. 262 (Disposizioni urgenti in materia tributaria e finanziaria), convertito, con modificazioni, dall'art. 1, comma 1, della legge 24 novembre 2006, n. 286, sollevata, in riferimento agli articoli 3 e 24 della Costituzione, dal Giudice di pace di Montefiascone, con l'ordinanza indicata in epigrafe.  &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 dicembre 2008.  &#13;
F.to:  &#13;
Giovanni Maria FLICK, Presidente  &#13;
Alfonso QUARANTA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 17 dicembre 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
