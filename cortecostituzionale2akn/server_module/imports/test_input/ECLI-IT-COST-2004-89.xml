<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/89/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/89/"/>
          <FRBRalias value="ECLI:IT:COST:2004:89" name="ECLI"/>
          <FRBRdate date="26/02/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="89"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/89/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/89/ita@/!main"/>
          <FRBRdate date="26/02/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/89/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/89/ita@.xml"/>
          <FRBRdate date="26/02/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="09/03/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>26/02/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 21, comma 1, del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413) promosso con ordinanza del 23 maggio 2002 dalla Commissione tributaria regionale di Roma sull'appello proposto da Raffaele Lamenza contro l'Ufficio IVA di Roma, iscritta al n. 290 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 21, prima serie speciale, dell'anno 2003. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 21 gennaio 2004 il Giudice relatore Alfio Finocchiaro. 
    Ritenuto che, con ordinanza del 23 maggio 2002, la Commissione tributaria regionale di Roma - nel giudicare sull'appello proposto avverso la decisione della Commissione tributaria provinciale di Roma che aveva dichiarato inammissibile il ricorso proposto da Raffaele Lamenza oltre il termine di sessanta giorni dalla notifica degli avvisi in rettifica delle dichiarazioni IVA - ha sollevato d'ufficio, in relazione all'art. 24, primo e secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 21, comma 1, del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), nella parte in cui non consente l'impugnazione tardiva dell'atto impositivo da parte del contribuente che non abbia potuto per caso fortuito o forza maggiore proporre ricorso entro il termine di sessanta giorni dalla data della notificazione dello stesso; 
    che, nella specie, la presentazione del ricorso era avvenuta il 29 dicembre 1997, avverso gli avvisi notificati il 19 settembre 1997, per essere stato il ricorrente “trattenuto” presso il carcere di Francoforte sul Meno dal giorno 3 novembre 1997 al giorno 19 dicembre 1997; 
    che, ad avviso del rimettente, il problema cruciale dell'accesso alla giustizia va individuato nella necessaria effettività della tutela giudiziaria da intendersi quale concreta esercitabilità del diritto di difesa; 
    che “la rilevanza impeditiva del limite temporale inderogabile è apprezzabile unicamente laddove si coltivi una nozione concreta di azione, che, in proiezione soggettiva, appare più correttamente come il diritto ad un'attività giudiziale che si sviluppi in una serie di situazioni le quali sintetizzino, nel processo, il minimo necessario e sufficiente di poteri idonei ad ottenere un provvedimento decisorio”; 
    che “appare insostenibile che mere esigenze di tecnica endoprocessuale volte alla ricerca di un punto di equilibrio tra celerità ed economia dei giudizi possano essere sufficienti a giustificare l'indiscriminato condizionamento temporale (o modale) delle garanzie processuali costituzionalmente previste a tutela dell'individuo”; 
    che nel giudizio innanzi a questa Corte è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha concluso per l'inammissibilità - non costituendo la carcerazione preventiva una causa di forza maggiore, non impedendo la stessa l'esercizio dei diritti - e, comunque, per l'infondatezza della questione, dal momento che nella valutazione della congruità del termine vanno contemperati motivi di certezza ed uniformità con l'interesse del soggetto ad ottenere tutela per i propri diritti. 
    Considerato che il giudice rimettente afferma in modo del tutto apodittico la rilevanza della questione nel giudizio a quo; 
    che in particolare il rimettente, andando di contrario avviso rispetto all'orientamento della giurisprudenza della Corte di Cassazione, non indica le ragioni per le quali, invece, la detenzione all'estero dovrebbe integrare una causa di forza maggiore; 
    che, in difetto di plausibile motivazione relativamente al profilo anzidetto, l'invocato intervento additivo della Corte non risolverebbe la fattispecie oggetto di giudizio per il giudice rimettente, in quanto essa non rientrerebbe nella previsione della norma come auspicabilmente modificata; 
    che l'ordinanza non è quindi idonea a dare valido ingresso al giudizio di legittimità costituzionale (ex plurimis: ordinanze n. 207, n. 50 e n. 1 del 2003; n. 280, n. 205 e n. 43 del 2002) e che la relativa questione deve essere dichiarata manifestamente inammissibile. 
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 21, comma 1, del d. lgs. 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), sollevata, in riferimento all'art. 24, primo e secondo comma, della Costituzione, dalla Commissione tributaria regionale di Roma, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 26 febbraio 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Alfio FINOCCHIARO, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 9 marzo 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
