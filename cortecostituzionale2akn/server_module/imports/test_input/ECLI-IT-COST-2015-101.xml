<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2015/101/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2015/101/"/>
          <FRBRalias value="ECLI:IT:COST:2015:101" name="ECLI"/>
          <FRBRdate date="25/02/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="101"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2015/101/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2015/101/ita@/!main"/>
          <FRBRdate date="25/02/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2015/101/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2015/101/ita@.xml"/>
          <FRBRdate date="25/02/2015" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="05/06/2015" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2015</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CRISCUOLO</cc:presidente>
        <cc:relatore_pronuncia>Giuseppe Frigo</cc:relatore_pronuncia>
        <cc:data_decisione>25/02/2015</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Alessandro CRISCUOLO; Giudici : Paolo Maria NAPOLITANO, Giuseppe FRIGO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'intera disciplina di cui ai Capi I e II del Titolo IV del Libro I del decreto legislativo 6 settembre 2011, n. 159 (Codice delle leggi antimafia e delle misure di prevenzione, nonché nuove disposizioni in materia di documentazione antimafia, a norma degli articoli 1 e 2 della legge 13 agosto 2010, n. 136) e dell'art. 52, comma 1, del medesimo decreto legislativo, promossi dal Tribunale ordinario di Trapani con due ordinanze del 9 aprile 2014, iscritte ai nn. 178 e 179 del registro ordinanze 2014 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 44, prima serie speciale, dell'anno 2014.
 Visti l'atto di costituzione di P.F., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 24 febbraio 2015 il Giudice relatore Giuseppe Frigo; 
 uditi l'avvocato Marco Siragusa per P.F. e l'avvocato dello Stato Mario Antonio Scino per il Presidente del Consiglio dei ministri.
 Ritenuto che, con due ordinanze di analogo tenore, depositate il 9 aprile 2014, il Tribunale ordinario di Trapani, sezione misure di prevenzione, ha sollevato questioni di legittimità costituzionale:
 a) in via principale, «dell'intera disciplina prevista dal I e II capo del titolo IV del I libro» del decreto legislativo 6 settembre 2011, n. 159 (Codice delle leggi antimafia e delle misure di prevenzione, nonché nuove disposizioni in materia di documentazione antimafia, a norma degli articoli 1 e 2 della legge 13 agosto 2010, n. 136), in riferimento agli artt. 3 e 41 della Costituzione;
 b) in subordine, dell'art. 52, comma 1, del medesimo decreto legislativo, nella parte in cui restringe la tutela dei terzi creditori, nel caso di confisca disposta all'esito di un procedimento di prevenzione, ai soli titolari di diritti di credito risultanti da atti aventi data certa anteriore al sequestro, «così escludendo i crediti dimostrabili con criteri meno rigidi di quelli previsti dall'art. 2704 [del codice civile], ma comunque idonei a fornire adeguata certezza della sussistenza del credito e della sua anteriorità al sequestro», per contrasto con agli artt. 3, 24 e 41 Cost.; 
 che il giudice a quo premette di essere investito delle opposizioni proposte da alcuni creditori avverso i provvedimenti di esclusione dallo stato passivo, emessi nell'ambito del procedimento di verifica dei crediti nei confronti di due società a responsabilità limitata, le cui quote e il cui intero patrimonio aziendale erano stati oggetto di sequestro e di successiva confisca per finalità di prevenzione: procedimento disciplinato dagli artt. 57 e seguenti del d.lgs. n. 159 del 2011;
 che, in larga parte dei casi, il rigetto dell'istanza di ammissione al passivo era stato motivato dal giudice delegato con il rilievo che i diritti di credito degli istanti, quasi tutti appartenenti alla categoria dei «fornitori», non risultavano da atti aventi data certa anteriore al sequestro, così come richiesto dall'art. 52, comma 1, del d.lgs. n. 159 del 2011: requisito, questo, da apprezzare alla stregua delle generali previsioni dell'art. 2704 cod. civ.;
 che vari creditori opponenti avevano chiesto di provare il loro credito con documenti o prove testimoniali, nonché con le scritture contabili proprie e della società colpita dal provvedimento ablativo;
 che, ciò premesso, il Tribunale osserva come, nel vigore della previgente disciplina dettata dagli artt. 2-ter e seguenti della legge 31 maggio 1965, n. 575 (Disposizioni contro le organizzazioni criminali di tipo mafioso, anche straniere), fosse «ius receptum» che la tutela prefigurata da tale legge a favore dei terzi aventi un diritto reale sul bene oggetto di confisca non si estendeva ai titolari di diritti di credito, salvo che si trattasse di creditori ipotecari;
 che «nella prassi», peraltro, nel caso di confisca di un'azienda, venivano tutelati anche i creditori aziendali di buona fede, sul presupposto che l'azienda costituisse una «universalità» acquisita al patrimonio dello Stato in tutte le sue componenti, attive e passive, tra loro inscindibilmente collegate;
 che, al di fuori delle ipotesi indicate, la confisca pregiudicava i creditori della persona colpita dalla misura, i quali rischiavano di vedersi privati di colpo della garanzia patrimoniale: assetto, questo, contro il quale si erano levate «diverse voci» critiche;
 che con le disposizioni contenute nei Capi I e II del Titolo IV del Libro I del d.lgs. n. 159 del 2011, il legislatore avrebbe «sovvertit[o]» la precedente impostazione, «concedendo ai terzi una tutela avanzata»;
 che la soluzione adottata a tal fine sarebbe, peraltro, consistita nel recepimento pressoché integrale della disciplina in tema di verifica dei crediti e di liquidazione dell'attivo prevista dalla legge fallimentare (regio decreto 16 marzo 1942, n. 267, recante «Disciplina del fallimento, del concordato preventivo e della liquidazione coatta amministrativa»);
 che, in questo modo, non si sarebbe tenuto conto della profonda differenza esistente tra i due istituti: il fallimento trae origine, infatti, dall'insolvenza, mentre le misure di prevenzione patrimoniali colpiscono spesso imprese vitali, che dovrebbero poter proseguire la loro attività, viceversa seriamente ostacolata dalla nuova disciplina;
 che in base alle disposizioni censurate, infatti, i crediti per titolo anteriore al sequestro debbano essere accertati con lo speciale procedimento di verifica regolato dagli artt. 57, 58 e 59 del d.lgs. n. 159 del 2011, procedimento affidato allo stesso giudice della prevenzione;
 che il soddisfacimento dei creditori ammessi è, inoltre, posticipato ad un momento successivo, presupponendo la formazione dello stato passivo, la liquidazione di parte dei beni e il riparto delle somme ricavate fra i creditori, secondo lo schema tipico della procedura fallimentare;
 che il rinvio dell'adempimento delle obbligazioni ad una data lontana nel tempo provocherebbe, peraltro, inevitabilmente l'interruzione dei rapporti con categorie essenziali di partner dell'impresa interessata dalla misura, quali i fornitori e le banche, determinando così la crisi immediata dell'impresa stessa;
 che la nuova disciplina imprimerebbe, in ogni caso, alla procedura una finalità spiccatamente liquidatoria, sostanzialmente incompatibile con la prosecuzione dell'attività imprenditoriale: situazione, questa, ancora più contraddittoria ove si consideri che l'amministrazione dei beni in una prospettiva di continuità del ciclo produttivo assolverebbe «alla funzione - oltre che di garantire la permanenza dei livelli occupazionali - di preservare i beni stessi in vista del loro riutilizzo a fini sociali, riutilizzo cui si annette uno specifico valore simbolico ai fini della disgregazione del consenso di cui godono le organizzazioni criminali»;
 che la normativa in questione violerebbe, di conseguenza, l'art. 41 Cost., provocando l'ingiustificata «dissipazione» di floride attività economiche: risultato che non si aveva, di contro, nel regime precedente, nel quale - come detto - l'azienda confiscata era considerata, nella prassi, come una universalità, comprensiva anche delle componenti negative, costituite dai debiti, che venivano quindi adempiuti dall'autorità giudiziaria «secondo un prudente apprezzamento e sotto le direttive del giudice delegato»;
 che risulterebbe violato, inoltre, l'art. 3 Cost., giacché, alla luce di quanto dianzi rilevato, l'adozione degli schemi della legge fallimentare per l'accertamento e il successivo adempimento dei debiti dell'impresa confiscata equiparerebbe situazioni del tutto diverse;
 che la questione sarebbe, altresì, rilevante nei giudizi a quibus: la dichiarazione della illegittimità costituzionale dell'intero «sistema di tutela dei terzi» di fronte alle misure di prevenzione travolgerebbe, infatti, anche il procedimento di opposizione allo stato passivo, che costituisce una fase della verifica dei crediti, facendo «rivivere» il sistema precedente, nel quale - ad avviso del rimettente - «i diritti dei creditori delle imprese erano accertati secondo uno schema più agile e meno artificioso e contrario agli interessi economici dell'impresa in sequestro»;
 che, in via subordinata, recependo l'eccezione formulata da alcuni dei creditori opponenti, il giudice a quo ritiene di dover sottoporre a scrutinio di legittimità costituzionale la specifica previsione dell'art. 52, comma 1, del d.lgs. n. 159 del 2011;
 che la disposizione censurata richiede, ai fini dell'ammissione al passivo e della conseguente tutela del creditore, una serie di requisiti: in particolare, che il credito risulti da atto avente data certa anteriore al sequestro; che il restante patrimonio del proposto risulti insufficiente a soddisfarlo; che il credito non sia strumentale all'attività illecita, salvo che il creditore dimostri la propria «buona fede»; infine, la prova del rapporto fondamentale, nel caso di credito con titolo genetico o rappresentativo astratto (promessa di pagamento, ricognizione di debito, titoli di credito);
 che il dubbio di costituzionalità investe segnatamente il primo di detti requisiti, ossia quello della «data certa»;
 che la locuzione normativa implicherebbe un evidente rinvio al generale disposto dell'art. 2704 cod. civ., in forza del quale «La data della scrittura privata della quale non è autenticata la sottoscrizione non è certa e computabile riguardo ai terzi, se non dal giorno in cui la scrittura è stata registrata o dal giorno della morte o della sopravvenuta impossibilità fisica di colui o di uno coloro che l'hanno sottoscritta o dal giorno in cui il contenuto della scrittura è riprodotto in atti pubblici o, infine, dal giorno in cui si verifica un altro fatto che stabilisca in modo egualmente certo l'anteriorità della formazione del documento»;
 che, alla luce della giurisprudenza formatasi in rapporto alla verifica dei crediti nel fallimento, il citato art. 2704 cod. civ. non reca una elencazione tassativa dei fatti che conferiscono certezza alla data della scrittura privata, ma lascia al giudice di merito la valutazione, caso per caso, della sussistenza di un fatto idoneo a dimostrare la data certa, fatto che può essere oggetto di prove per testi o per presunzioni: ma ciò, solo a condizione che esse evidenzino un fatto munito di tale attitudine, e non anche quando tali prove siano rivolte, in via indiziaria ed induttiva, a provocare un giudizio di mera verosimiglianza della data apposta al documento;
 che, sempre alla luce della giurisprudenza in materia fallimentare, non sarebbero inoltre utilizzabili ai fini dell'accertamento del credito le scritture contabili dell'impresa sequestrata, non essendo applicabile, nei confronti degli organi della procedura, l'art. 2710 cod. civ., che conferisce efficacia probatoria tra imprenditori, per i rapporti inerenti all'esercizio dell'impresa, ai libri regolarmente tenuti;
 che il requisito in questione - volto ad impedire collusioni tra il proposto e terzi compiacenti, evitando l'ammissione al passivo di crediti portati da documenti formati dopo il sequestro - penalizzerebbe, peraltro, ingiustificatamente la categoria dei «fornitori», i quali si trovano a dover operare «nella convulsa e dinamica realtà dell'impresa che non si presta ad essere ingessata negli schemi della precostituzione di una data certa del rapporto»;
 che, nella specie, plurimi creditori hanno dedotto, in sede di verifica, l'esistenza di prassi commerciali incompatibili con i suddetti schemi, quale, ad esempio, quella di ordinare per telefono le materie prime necessarie alla produzione;
 che l'art. 52, comma 1, del d.lgs. n. 159 del 2011 violerebbe, per questo verso, gli artt. 24 e 41 Cost., «venendo ad incidere ingiustificatamente, ingessandola in adempimenti incompatibili con il suo ordinario svolgimento, sulla attività delle imprese; e imponendo per l'accertamento dei diritti di credito percorsi probatori aggravati»;
  che la norma censurata violerebbe, altresì, l'art. 3 Cost., equiparando situazioni tra loro diverse, quali quelle dei fornitori e dei creditori «meno dinamici», come le banche, che, per la loro «posizione di supremazia» sarebbero sempre in grado di munirsi di una scrittura con data certa ai fini della dimostrazione dei propri crediti;
 che, sulla base di tali rilievi, il rimettente chiede, quindi, a questa Corte di estendere, «con pronuncia additiva», l'ambito della tutela «anche ai crediti dimostrabili con criteri meno rigidi di quelli previsti dall'art. 2704 c.c., ma comunque idonei a fornire adeguata certezza della sussistenza del credito e della sua anteriorità al sequestro»;
 che anche tale questione sarebbe rilevante, giacché solo nel caso di «modifica della disciplina» posta dalla norma impugnata sarebbe possibile accogliere le istanze dei fornitori, opponenti nei giudizi a quibus, altrimenti «irrimediabilmente pretermessi»;
 che in entrambi i giudizi di legittimità costituzionale è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che le questioni siano dichiarate non fondate;
 che si è costituito, altresì, P.F., creditore opponente nei giudizi a quibus, concludendo per l'accoglimento delle questioni.
 Considerato che le ordinanze di rimessione sollevano identiche questioni, sicché i relativi giudizi vanno riuniti per essere definiti con unica decisione;
 che entrambe le questioni sollevate sono manifestamente inammissibili;
 che, quanto alla questione sollevata in via principale, il rimettente censura l'intero complesso normativo riguardante la tutela dei terzi nei confronti delle misure di prevenzione patrimoniali (Capi I e II del Titolo IV del Libro I del decreto legislativo 6 settembre 2011, n. 159, recante il «Codice delle leggi antimafia e delle misure di prevenzione, nonché nuove disposizioni in materia di documentazione antimafia, a norma degli articoli 1 e 2 della legge 13 agosto 2010, n. 136»), omettendo di individuare - come sarebbe stato suo onere, alla luce della costante giurisprudenza di questa Corte (ex plurimis, sentenza n. 218 del 2014, ordinanze n. 21 del 2003, n. 337 del 2002 e n. 97 del 2000) - le singole norme, o parti di esse, la cui presenza nell'ordinamento determinerebbe la lamentata violazione dei parametri costituzionali evocati (artt. 3 e 41 della Costituzione);
 che il corpus normativo censurato comprende, infatti, disposizioni eterogenee, parte delle quali prive di attinenza con le doglianze formulate, focalizzate sulla denuncia degli effetti negativi conseguenti all'adozione, nella materia considerata, di soluzioni modellate sulle cadenze tipiche della procedura fallimentare: basti pensare, ad esempio, alla disposizione di esordio di cui all'art. 52, comma 1, del d.lgs. n. 159 del 2011, che enuncia le condizioni cui è subordinata la tutela dei creditori (recependo, in buona parte, indirizzi interpretativi già emersi nel vigore della disciplina anteriore), ovvero ai commi 7 e seguenti del medesimo articolo, che stabiliscono i diritti dei partecipanti nel caso di confisca di beni in comunione, ovvero all'art. 53, che fissa il limite entro il quale i crediti per titolo anteriore al sequestro sono soddisfatti dallo Stato; disposizioni tutte prive di riscontro nella legge fallimentare e rispondenti alle finalità proprie dei procedimenti di prevenzione;
 che, al di là di ciò, con la denuncia della violazione degli indicati parametri costituzionali, il rimettente muove, in realtà, critiche di opportunità alle scelte di politica legislativa sottese al nuovo regime introdotto dal d.lgs. n. 159 del 2011, riguardo ai meccanismi di tutela dei diritti dei terzi: e ciò nella prospettiva di far «rivivere» la situazione anteriore, per communis opinio ampiamente lacunosa sul piano della regolamentazione normativa del profilo che interessa, ma nella quale - ad avviso del giudice a quo - si sarebbe instaurata una «prassi» operativa (concernente, peraltro, la sola confisca di azienda) in assunto maggiormente consentanea alla protezione degli interessi in gioco;
 che - a prescindere da ogni altro rilievo - il rimettente invoca, in questo modo, un intervento "di sistema" esorbitante, per sua natura, dai limiti del giudizio di legittimità costituzionale (tra le altre, ordinanza n. 182 del 2009);
 che manifestamente inammissibile è, del pari, la questione sollevata in via subordinata, concernente la disposizione di cui all'art. 52, comma 1, del d.lgs. n. 159 del 2011, nella parte in cui limita la tutela ai diritti di credito che risultano da atti aventi data certa anteriore al sequestro: requisito, questo, finalizzato precipuamente ad evitare che la persona sottoposta al procedimento di prevenzione possa eludere gli effetti della confisca tramite collusioni con creditori di comodo;
 che il rimettente - lamentando che il requisito in questione penalizzi oltre misura la categoria dei «fornitori» dell'impresa, in contrasto con gli artt. 3, 24 e 41 Cost. - chiede, in specie, a questa Corte una «pronuncia additiva» che estenda la protezione ai crediti che possono essere provati «con criteri meno rigidi di quelli previsti dall'art. 2704 [del codice civile], ma comunque idonei a fornire adeguata certezza della sussistenza del credito e della sua anteriorità al sequestro»;
 che - indipendentemente da ogni considerazione riguardo al merito delle censure - il petitum così formulato risulta oscuro e indeterminato;
 che, in assenza di precisazioni sul punto, non si comprende, infatti, quali siano i «criteri» che, nella prospettiva del giudice a quo, dovrebbero surrogare quelli previsti dalla richiamata disposizione del codice civile, temperandone il rigore, ma rimanendo comunque idonei ad assicurare una «adeguata certezza» in ordine all'esistenza del credito e alla sua anteriorità rispetto alla misura di prevenzione patrimoniale: sicché gli esatti contenuti dell'auspicata sentenza additiva restano affatto indefiniti (sulla manifesta inammissibilità della questione per oscurità o indeterminatezza del petitum, tra le molte, sentenza n. 218 del 2014, ordinanze n. 96 del 2014 e n. 195 del 2013);
 che le questioni vanno dichiarate, pertanto, manifestamente inammissibili.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 riuniti i giudizi,&#13;
 1) dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'«intera disciplina prevista dal I e II capo del titolo IV del I libro» del decreto legislativo 6 settembre 2011, n. 159 (Codice delle leggi antimafia e delle misure di prevenzione, nonché nuove disposizioni in materia di documentazione antimafia, a norma degli articoli 1 e 2 della legge 13 agosto 2010, n. 136), sollevate, in riferimento agli artt. 3 e 41 della Costituzione, dal Tribunale ordinario di Trapani con le ordinanze indicate in epigrafe;&#13;
 2) dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 52, comma 1, del d.lgs. n. 159 del 2011, sollevate, in riferimento agli artt. 3, 24 e 41 della Costituzione, dal Tribunale ordinario di Trapani con le medesime ordinanze.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 25 febbraio 2015.&#13;
 F.to:&#13;
 Alessandro CRISCUOLO, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 5 giugno 2015.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
