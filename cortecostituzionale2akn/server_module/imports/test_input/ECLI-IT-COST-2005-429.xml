<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/429/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/429/"/>
          <FRBRalias value="ECLI:IT:COST:2005:429" name="ECLI"/>
          <FRBRdate date="16/11/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="429"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/429/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/429/ita@/!main"/>
          <FRBRdate date="16/11/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2005/429/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2005/429/ita@.xml"/>
          <FRBRdate date="16/11/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="25/11/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Romano Vaccarella</cc:relatore_pronuncia>
        <cc:data_decisione>16/11/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Gaetano SILVESTRI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli articoli 180, secondo comma, terzo periodo, e 183 del codice di procedura civile promosso con ordinanza del 30 aprile 2004 dal Tribunale di Viterbo, sezione distaccata di Civita Castellana, nel procedimento civile vertente tra Pensione “Il Colle” di Sorichetti Giorgio e P. M., iscritta al n. 998 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 50, prima serie speciale, dell'anno 2004. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 26 ottobre 2005 il Giudice relatore Romano Vaccarella. 
    Ritenuto che, con ordinanza del 30 aprile 2004, il Tribunale di Viterbo, sezione distaccata di Civita Castellana, ha sollevato questione di legittimità costituzionale dell'art. 180, secondo comma, «cpv. 3» (recte: terzo periodo), del codice di procedura civile, nella parte in cui prevede che (il giudice istruttore) «in ogni caso fissa a data successiva la prima udienza di trattazione», nonché dell'art. 183 cod. proc. civ., in riferimento agli artt. 3, primo comma, 24, secondo comma e 111, secondo comma, della Costituzione; 
    che, espone il rimettente, in sede di udienza di prima comparizione – rilevata dal giudice la ritualità della notifica dell'atto di citazione e, conseguentemente, dichiarata la contumacia del convenuto – l'attore aveva chiesto il rinvio della causa per la compiuta articolazione dei mezzi istruttori; 
    che, osserva il rimettente, in punto di rilevanza, il combinato disposto degli artt. 180 e 183 cod. proc. civ. comporta la doverosità del differimento della prima udienza di trattazione, così di fatto precludendo al giudice la possibilità di consentire all'attore che ne faccia richiesta di procedere all'articolazione dei mezzi istruttori nella successiva udienza (o addirittura nella stessa udienza di prima comparizione); 
    che, secondo il rimettente, già prima della riforma dell'art. 111 della Costituzione, era stata evidenziata l'esistenza di una fase processuale di trattazione, solo eventualmente frantumata in più udienze successive, «ontologicamente omogenee e tali da potersi dilatare o contrarre a seconda delle necessità dialettiche (…) delle parti», secondo un «vaglio di necessaria consequenzialità», rimesso al prudente apprezzamento del giudice; 
    che il nuovo testo dell'art. 111 della Costituzione avrebbe reso più attuale la possibilità – in vista del contemperamento del «predicato della ragionevole durata» del processo, «con il naturale fluire dei vari momenti processuali» – di una non coincidenza delle varie scansioni, logicamente ineliminabili, del giudizio, con «momenti cronologicamente distanti»;  
    che la Corte di cassazione avrebbe condiviso tale approccio, progressivamente erodendo l'assunto della necessaria coincidenza tra fasi processuali e udienze, fino a sancire – ma limitatamente ai soli giudizi in cui le parti si siano costituite, e dunque con esclusione di quelli contumaciali – la possibilità di una deroga al disposto dell'art. 180 cod. proc. civ., nella parte in cui prevede che «in ogni caso» il giudice istruttore fissa a data successiva la prima udienza di trattazione; mentre «interpretazioni estensive» sarebbero state rese dai giudici di merito che, nella prospettiva accolta dal rimettente, avrebbero operato una «completa rilettura della rigida concatenazione degli artt. 180, 183 e 184, alla luce della nuova normativa costituzionale», in applicazione del criterio per cui tra più esegesi ugualmente possibili, deve essere preferita quella che assegna alla norma un contenuto conforme alla Costituzione;  
    che, osserva ancora il giudice a quo, se è vero che con ordinanza n. 3 del 2002, la Corte costituzionale ha dichiarato manifestamente inammissibile la questione della legittimità dell'art. 180, secondo comma, cod. proc. civ., ribadendo che è improprio sollevare dubbi di costituzionalità al solo fine di sollecitare il Giudice delle leggi a compiere un'attività meramente interpretativa, è anche vero che tale decisione ha avuto riguardo ai soli parametri costituzionali degli artt. 3 e 24, e non già anche a quello dell'art. 111, come rivisitato a seguito della legge costituzionale 23 novembre 1999, n. 2;  
    che, secondo il giudice a quo, nel nuovo assetto normativo, l'inutile protrazione del processo costituirebbe un fatto illegittimo, in quanto in contrasto con le esigenze di economia e celerità dell'attività processuale, «che si reputano ora prevalenti», atteso che la definizione sollecita del contenzioso costituisce valore autonomo dell'ordinamento, «suscettibile di comparazione con gli altri principî», alla stregua di una generale evoluzione del sistema, che ha trovato espressione, sul piano normativo, nella riforma del giudice unico – e nella connessa, tendenziale coincidenza nella medesima persona, del giudice istruttore e del «giudice-decisore» – e, a livello dogmatico, nella ricostruzione del diritto alla ragionevole durata del processo come diritto assoluto e incomprimibile, cui si correla l'obbligo per parti, giudici e organizzazione giudiziaria in genere, di svolgere ciascuno il proprio ruolo nel rispetto dell'art. 6 della Convenzione europea dei diritti dell'uomo; 
    che, conseguentemente, sarebbe «accentuato» il potere/dovere del giudice di passare direttamente all'istruttoria o addirittura alle conclusioni, tutte le volte in cui ritenga di poter saltare «le fasi prodromiche», laddove la formula «in ogni caso», che compare nel testo dell'art. 180 cod. proc. civ., sarebbe espressiva di una rigidità incompatibile col nuovo sistema costituzionale e tale dunque da dover essere elisa;  
    che, come rilevato dai commentatori della sentenza della Corte di cassazione, 24 maggio 2000, n. 6808, contrasta con il nuovo dettato dell'art. 111 della Costituzione la regola per cui, nei giudizi contumaciali, il rinvio all'udienza di trattazione ex art. 180 cod. proc. civ. debba essere sempre disposto, anche quando l'attore abbia dichiarato espressamente di non avervi interesse – e anzi di avere un interesse opposto alla dilazione del processo – e il convenuto, con l'omessa costituzione, abbia dimostrato di non volere accettare il contraddittorio; 
    che, a rendere costituzionalmente legittima la norma, non potrebbe valere la presumibile ratio ad essa sottesa, di salvaguardare uno spazio per la comparizione personale e per l'interrogatorio libero delle parti, posto che trattasi di incombenti che il giudice può ordinare in qualunque stato e grado del giudizio ex art. 117 cod. proc. civ., né la considerazione che l'eccessiva e ingiustificata dilazione del processo non sarebbe determinata dall'astratta possibilità di scansione dell'attività processuale in udienze successive, ma piuttosto, quale mero inconveniente di fatto, dal modo in cui viene in concreto gestito il giudizio, e cioè con rinvii che, di fatto, è impossibile disporre a distanza di pochi giorni; 
    che, osserva il rimettente, se è vero che l'iter delineato negli artt. 180, 183 e 184 cod. proc. civ. è stato costruito dal legislatore della legge 26 novembre 1990, n. 353 (Provvedimenti urgenti per il processo civile), e poi della legge 20 dicembre 1995, n. 534 (Conversione in legge, con modificazioni, del decreto-legge 18 ottobre 1995, n. 432, recante interventi urgenti sul processo civile e sulla disciplina transitoria della legge 26 novembre 1990, n. 353, relativa al medesimo processo) per definire fasi ontologicamente distinte, quali la verifica della corretta introduzione della causa nonché la precisazione del thema decidendum e del thema probandum, con connesso regime di preclusioni, destinato a scattare alla chiusura di ciascuna fase, solo con l'accoglimento della prospettata questione «si potrebbe avere (…) un'applicazione del predicato della ragionevole durata, prevalente sui principi del contraddittorio e della parità processuale», senza sovvertire i consolidati orientamenti che negano ogni valenza sostanziale alla contumacia, laddove essa l'ha solo sul piano delle regole processuali;  
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri,  rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto alla Corte di dichiarare inammissibile o manifestamente infondata la prospettata questione, sottolineando che lo stesso rimettente si mostra consapevole della possibilità che la norma censurata sia interpretata nel senso che del rinvio si può fare a meno, quando nessuna delle parti vi abbia interesse;  
    che rientra certamente nella libera e discrezionale scelta del legislatore disporre un differimento della trattazione della causa di qualche giorno, nell'interesse a una «conferma meditata» della scelta processuale operata dalle parti: rinvio che, oltre a non compromettere il principio della ragionevole durata del processo, non appare irragionevole, né in altro modo lesivo del diritto di difesa. 
 Considerato che il Tribunale di Viterbo, sezione distaccata di Civita Castellana, dubita della legittimità costituzionale, in riferimento agli articoli 3, comma primo, 24, comma secondo, e 111, comma secondo, della Costituzione, dell'articolo 180, secondo comma, terzo periodo, del codice di procedura civile nella parte in cui prevede che (il giudice istruttore) «in ogni caso fissa a data successiva la prima udienza di trattazione» e dell'art. 183 cod. proc. civ.; 
      che la questione è manifestamente inammissibile; 
      che, infatti, il Tribunale rimettente lamenta di dovere, in base al consolidato orientamento della Corte di cassazione, necessariamente fissare l'udienza di trattazione per ciò solo che il convenuto è stato ritualmente dichiarato contumace, ma riferisce che, nel giudizio davanti a lui pendente, l'attore non aveva chiesto l'immediata assunzione, nell'udienza di prima comparizione di cui all'art. 180 cod. proc. civ., o anche solo l'ammissione dei mezzi istruttori, bensì la fissazione di un'udienza successiva per poter articolare compiutamente le sue istanze istruttorie; 
      che, pertanto, l'esigenza di fissare l'udienza di trattazione – «ontologicamente omogenea», ricorda lo stesso rimettente, a quella di cui all'art. 184 cod. proc. civ., e quindi potenzialmente idonea a renderla superflua – non deriva, nel caso di specie, dal censurato consolidato orientamento della Suprema Corte, ma dalla legittima richiesta dell'attore di uno spatium deliberandi  per definire le sue istanze istruttorie; 
      che, quindi, la questione di legittimità costituzionale sollevata non è rilevante nel giudizio a quo. 
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      dichiara la manifesta inammissibilità della questione di legittimità costituzionale degli artt. 180, secondo comma, terzo periodo e 183 del codice di procedura civile, sollevata, in riferimento agli artt. 3, comma primo, 24, comma secondo, e 111, comma secondo, della Costituzione, dal Tribunale di Viterbo, sezione distaccata di Civita Castellana, con l'ordinanza in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 16 novembre 2005.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Romano VACCARELLA, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 25 novembre 2005.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
