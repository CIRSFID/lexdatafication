<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/349/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/349/"/>
          <FRBRalias value="ECLI:IT:COST:2004:349" name="ECLI"/>
          <FRBRdate date="15/11/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="349"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/349/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/349/ita@/!main"/>
          <FRBRdate date="15/11/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/349/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/349/ita@.xml"/>
          <FRBRdate date="15/11/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/11/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ONIDA</cc:presidente>
        <cc:relatore_pronuncia>Guido Neppi Modona</cc:relatore_pronuncia>
        <cc:data_decisione>15/11/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfonso QUARANTA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale degli artt. 11, 14 e 15 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468) e dell'art. 415-bis del codice di procedura penale, promossi, nell'ambito di diversi procedimenti penali, dal Giudice di pace di Bari con ordinanza del 3 novembre 2003, dal Giudice di pace di Chieti con ordinanza del 26 marzo 2003, dal Giudice di pace di Montagnana con ordinanza del 14 gennaio 2004 e dal Giudice di pace di Otranto con ordinanza del 15 gennaio 2004, rispettivamente iscritte al n. 1193 del registro ordinanze 2003, al n. 270, al n. 305 e al n. 325 del registro ordinanze 2004 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 4, prima serie speciale, dell'anno 2003, n. 15 e n. 17, prima serie speciale, dell'anno 2004. 
Visti gli atti di intervento del Presidente del Consiglio dei ministri; 
udito nella camera di consiglio del 7 luglio 2004 il Giudice relatore Guido Neppi Modona. 
Ritenuto che il Giudice di pace di Bari (r.o. n. 1193 del 2003) ha sollevato questione di legittimità costituzionale degli artt. 11, 14 e 15 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468);  
che, in particolare, l'art. 15 del predetto decreto legislativo si porrebbe in contrasto con gli artt. 3, 24 e 111 della Costituzione, nella parte in cui non prevede che venga dato avviso all'indagato della conclusione delle indagini preliminari, così come stabilito nel procedimento ordinario dall'art. 415-bis del codice di procedura penale; 
che il combinato disposto degli artt. 11, 14 e 15 dello stesso decreto violerebbe gli artt. 3, 76 e 109 Cost., nella parte in cui prevede che le indagini preliminari siano svolte dalla polizia giudiziaria, che non solo è svincolata «dal controllo dell'autorità giudiziaria […] contrariamente al dettato dell'art. 109 Cost.», ma non ha l'onere di svolgere indagini anche a favore dell'indagato, così come disposto per il pubblico ministero nel procedimento ordinario; 
che l'art. 14 dello stesso decreto contrasterebbe con gli artt. 3 e 24 Cost., in quanto «la mancata previsione dell'obbligo di iscrizione dell'autore del reato nel registro contenente la notitia criminis […] priva il soggetto indagato della possibilità di sapere se vi sono indagini investigative a suo carico» e, conseguentemente, determina una violazione del suo diritto di difesa;  
che, quanto alla non manifesta infondatezza, il rimettente osserva che se «è vero che il giudizio davanti al giudice di pace riveste caratteri di peculiarità, atteso che lo stesso inerisce a reati di minore gravità», sembra peraltro «ineludibile il diritto del prevenuto di conoscere l'esistenza di indagini sul suo conto anche prima dell'imputazione» ed è «essenziale, altresì, che le indagini della polizia giudiziaria siano condotte e dirette dall'autorità giudiziaria»;  
che i Giudici di pace di Otranto (r.o. n. 325 del 2004) e di Chieti (r.o. n. 270 del 2004) hanno sollevato questione di legittimità costituzionale dell'art. 15 del decreto legislativo n. 274 del 2000, nella parte in cui non prevede che anche nel procedimento dinanzi al giudice di pace sia dato avviso all'indagato della conclusione delle indagini preliminari, come disposto dall'art. 415-bis cod. proc. pen. in relazione al procedimento ordinario; 
che il Giudice di pace di Montagnana (r.o. n. 305 del 2004) ha sollevato analoga questione incentrando le censure non solo sull'art. 15 del decreto legislativo n. 274 del 2000, ma anche sull'art. 415-bis cod. proc. pen., nella parte in cui dette disposizioni non prevedono che anche nel procedimento dinanzi al giudice di pace sia dato avviso all'indagato della conclusione delle indagini preliminari; 
che i giudici rimettenti ritengono che la disciplina censurata sia in contrasto con gli artt. 3, 24 e 111 Cost., perché renderebbe «impossibile la difesa dell'imputato»,  determinerebbe una evidente disparità di trattamento a seconda del giudice (onorario o togato) competente per il giudizio e violerebbe il principio secondo cui la persona accusata di un reato deve essere informata riservatamente nel più breve tempo possibile della natura e dei motivi dell'accusa elevata a suo carico; 
che in tutti i giudizi è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che siano dichiarate inammissibili le questioni sollevate con le ordinanze iscritte al numero 1193 del registro ordinanze del 2003 (per l'omessa indicazione della fase nella quale si trova il procedimento a quo e per difetto di motivazione in ordine alla non manifesta infondatezza) e ai numeri 270 e 325 del registro ordinanze del 2004 (per assoluta carenza di motivazione in ordine alla rilevanza e alla non manifesta infondatezza), e manifestamente infondata la questione sollevata con l'ordinanza iscritta al numero 305 del registro ordinanze del 2004, avuto riguardo alla particolare struttura del procedimento davanti al giudice di pace. 
Considerato che tutti i rimettenti dubitano, in riferimento agli artt. 3, 24 e 111 della Costituzione (l'art. 24 è richiamato solo dai Giudici di pace di Bari, Otranto e Montagnana), della legittimità costituzionale dell'art. 15 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui non prevede che nel procedimento dinanzi al giudice di pace sia dato avviso all'indagato della conclusione delle indagini preliminari, come previsto dall'art. 415-bis del codice di procedura penale nel procedimento davanti al tribunale; 
che il Giudice di pace di Montagnana dubita altresì della legittimità costituzionale dell'art. 415-bis cod. proc. pen., che prevede l'avviso all'indagato della conclusione delle indagini preliminari, sul presupposto che tale disposizione non trovi applicazione nel procedimento davanti al giudice di pace; 
che secondo i rimettenti le disposizioni censurate determinano una irragionevole disparità di trattamento tra il soggetto indagato per un reato di competenza del giudice di pace, che si trova nell'impossibilità di svolgere adeguatamente la sua difesa, e il soggetto sottoposto a indagini per reati di competenza del giudice ordinario, e si pongono altresì in contrasto con l'art. 111 Cost., nella parte in cui prevede che la persona accusata di un reato deve essere informata nel più breve tempo possibile dell'accusa  elevata a suo carico e deve disporre del tempo e delle condizioni necessarie per preparare la difesa; 
che il Giudice di pace di Bari solleva inoltre questione di legittimità costituzionale, in riferimento agli artt. 3 (parametro indicato solo in motivazione), 76 e 109 Cost., del combinato disposto degli artt. 11, 14 e 15 dello stesso decreto, nella parte in cui prevede che le indagini preliminari siano svolte dalla polizia giudiziaria, che non solo sarebbe svincolata dal controllo dell'autorità giudiziaria, ma non avrebbe neppure l'onere di svolgere accertamenti anche a favore dell'indagato, nonché dell'art. 14 dello stesso decreto, in riferimento agli artt. 3 e 24 Cost., nella parte in cui, non prevedendo l'obbligo della polizia giudiziaria di iscrivere la notizia di reato, non consente all'indagato di venire a conoscenza delle indagini svolte a suo carico e lo priva così del diritto di svolgere attività difensiva; 
che, essendo censurati da tutti i rimettenti vari aspetti della medesima disciplina, deve essere disposta la riunione dei relativi giudizi; 
che la questione sollevata dal Giudice di pace di Chieti va dichiarata manifestamente inammissibile per assoluta carenza di motivazione in ordine alla rilevanza e alla non manifesta infondatezza;  
che non possono essere accolte le eccezioni di inammissibilità dell'Avvocatura dello Stato circa le questioni sollevate dai Giudici di pace di Bari, di Otranto e di Montagnana, in quanto le relative motivazioni, sia pure succinte, appaiono sufficienti a dar conto della rilevanza e dei profili di incostituzionalità; 
che, quanto alle censure mosse all'art. 15 del decreto legislativo n. 274 del 2000, questa Corte ha affermato che il procedimento davanti al giudice di pace configura un modello di giustizia non comparabile con il procedimento per i reati di competenza del tribunale, che verrebbe ad essere snaturato dall'innesto dell'avviso di conclusione delle indagini preliminari, posto che tale procedura incidentale appare incompatibile con le finalità di snellezza, semplificazione e rapidità che connotano questa particolare forma di giurisdizione penale (v. ordinanza n. 201 del 2004); 
che con riferimento, più in generale, alle indagini che precedono il dibattimento, questa Corte ha già avuto occasione di mettere in rilievo che il procedimento penale davanti al giudice di pace è caratterizzato dal «ruolo marginale assegnato alle indagini preliminari, che si sostanziano in una fase investigativa affidata in via principale alla polizia giudiziaria» (v. la Relazione allo schema di decreto legislativo sul giudice di pace), in coerenza con le esigenze di massima semplificazione del procedimento e con la «finalità conciliativa» che costituisce il principale obiettivo della giurisdizione penale del giudice di pace (v. ordinanze numeri 231 del 2003; 10, 11, 55, 56, 57 e 201 del 2004); 
che in particolare, quanto alle specifiche censure formulate nei confronti del combinato disposto degli artt. 11, 14 e 15 del decreto legislativo in esame, è sufficiente rilevare che, pur essendo riconosciuto alla polizia giudiziaria (art. 11) il potere di compiere di propria iniziativa tutti gli atti di indagine necessari per la ricostruzione del fatto e per l'individuazione del colpevole, trasmettendo solo al termine delle indagini la relazione al pubblico ministero, questi può esercitare le sue prerogative di direzione e di controllo anche prima e indipendentemente dalla trasmissione della relazione, come risulta dai numerosi strumenti di intervento nel corso delle indagini (v., in particolare, l'art. 12 in relazione alle direttive impartite dal pubblico ministero che abbia acquisito direttamente la notizia di reato; l'art. 13 circa l'autorizzazione alla polizia giudiziaria per il compimento di determinati atti, ove il pubblico ministero non ritenga di svolgere personalmente le indagini; l'art. 15, comma 2, circa la facoltà del pubblico ministero di svolgere personalmente ulteriori indagini ovvero di delegarle alla polizia giudiziaria; l'art. 16, comma 2, in ordine al potere di disporre la prosecuzione delle indagini oltre il termine ordinario di quattro mesi; l'art. 5 del decreto ministeriale di esecuzione 6 aprile 2001, n. 204, relativo alla facoltà di richiedere la trasmissione della documentazione degli atti compiuti dalla polizia giudiziaria anche prima dell'invio della relazione);  
che anche la disciplina dettata per l'iscrizione della notizia di reato - iscrizione prevista obbligatoriamente a seguito della trasmissione della relazione da parte della polizia giudiziaria, ovvero fin dal primo atto di indagine svolto personalmente dal pubblico ministero - appare adeguata alla peculiare struttura delle indagini preliminari nel procedimento davanti al giudice di pace; 
che, al riguardo, questa Corte ha già affermato che «le esigenze di informazione dell'imputato prima dell'udienza di comparizione sono comunque assicurate dall'avviso, contenuto nella citazione a giudizio disposta dalla polizia giudiziaria, che il fascicolo relativo alle indagini preliminari è depositato presso la segreteria del pubblico ministero e che le parti e i loro difensori hanno facoltà di prenderne visione e di estrarne copia, nonché dall'indicazione, contenuta sempre nel medesimo atto, delle fonti di prova di cui il pubblico ministero chiede l'ammissione e, ove venga chiesto l'esame dei testimoni, delle circostanze su cui deve vertere l'esame» (v., ancora, ordinanza n. 201 del 2004); 
che, infine, in relazione alla mancata previsione a carico della polizia giudiziaria dell'onere di svolgere accertamenti anche a favore dell'indagato, la Corte ha già rilevato che l'analogo onere a carico del pubblico ministero non mira a dare attuazione al diritto di difesa, ma si innesta sulla natura di parte pubblica dell'organo dell'accusa e sui compiti che il pubblico ministero è chiamato ad assolvere nell'ambito delle proprie determinazioni al termine delle indagini (v. ordinanza n. 96 del 1997), che continuano a sostanziarsi, anche nel procedimento davanti al giudice di pace, nell'alternativa tra la richiesta dell'archiviazione e l'esercizio dell'azione penale; 
che le questioni sollevate dai Giudici di pace di Bari, Otranto e Montagnana devono pertanto essere dichiarate manifestamente infondate in relazione a tutti i parametri costituzionali evocati dai rimettenti. 
Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
riuniti i giudizi, &#13;
dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 15 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), sollevata, in riferimento agli artt. 3 e 111 della Costituzione, dal Giudice di pace di Chieti, con l'ordinanza in epigrafe; &#13;
dichiara la manifesta infondatezza delle questioni di legittimità costituzionale degli artt. 11, 14 e 15 del medesimo decreto legislativo 28 agosto 2000, n. 274, nonché dell'art. 415-bis del codice di procedura penale, sollevate, in riferimento agli artt. 3, 24, 76, 109 e 111 della Costituzione, dai Giudici di pace di Bari, di Otranto e di Montagnana, con le ordinanze in epigrafe. &#13;
</p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 15 novembre 2004. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Guido NEPPI MODONA, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 19 novembre 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
