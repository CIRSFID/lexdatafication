<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/37/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/37/"/>
          <FRBRalias value="ECLI:IT:COST:2018:37" name="ECLI"/>
          <FRBRdate date="06/02/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="37"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/37/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/37/ita@/!main"/>
          <FRBRdate date="06/02/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/37/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/37/ita@.xml"/>
          <FRBRdate date="06/02/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="23/02/2018" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2018</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Nicolò Zanon</cc:relatore_pronuncia>
        <cc:data_decisione>06/02/2018</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 7-ter del decreto legislativo 21 novembre 2005, n. 286 (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore), come aggiunto dall'art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103 (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), inserito dalla legge di conversione 4 agosto 2010, n. 127, promossi dal Tribunale ordinario di Grosseto, con due ordinanze del 3 giugno 2016, rispettivamente iscritte ai numeri 271 e 272 del registro ordinanze 2016 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 3, prima serie speciale, dell'anno 2017.
 Visti gli atti di costituzione della Danone spa, della Logitrans srl e della Boncioli srl, nonché gli atti di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 6 febbraio 2018 il Giudice relatore Nicolò Zanon;
 uditi gli avvocati Nicola Scopsi e Felice Laudadio per la Danone spa, Andrea De Cesaris per la Logitrans srl e la Boncioli srl e l'avvocato dello Stato Andrea Fedeli per il Presidente del Consiglio dei ministri.
 Ritenuto che il Tribunale ordinario di Grosseto, con due distinte ordinanze di analogo tenore, emesse in altrettanti giudizi, rispettivamente iscritte ai numeri 271 e 272 del registro ordinanze 2016, ha sollevato, in riferimento all'art. 77, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 7-ter del decreto legislativo 21 novembre 2005, n. 286 (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore); 
 che, espone il rimettente, nei giudizi a quibus la società Danone spa «ha sollevato la questione di costituzionalità dell'art. 7 ter» del d.lgs. n. 286 del 2005, aggiunto dall'art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103 (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), a sua volta inserito dalla legge di conversione 4 agosto 2010, n. 127;
 che, secondo il giudice a quo, la questione di legittimità costituzionale «è sicuramente rilevante ai fini della decisione delle cause riunite, in quanto il diritto di credito azionato con i decreti ingiuntivi opposti dalla società convenuta opposta si fonda sulla disposizione sopracitata»;
 che, in punto di non manifesta infondatezza, il rimettente ricorda che la Corte costituzionale, con le sentenze n. 32 del 2014 e n. 22 del 2012, nonché con l'ordinanza n. 34 del 2013, ha chiarito che la legge di conversione deve avere un contenuto omogeneo a quello del decreto-legge, sicché l'inclusione di emendamenti e articoli aggiuntivi che non siano attinenti alla materia oggetto del decreto-legge, o alle finalità di quest'ultimo, determinerebbe un vizio della legge di conversione in parte qua; 
 che, per il rimettente, dalla giurisprudenza costituzionale si trarrebbe la conclusione che le norme aggiunte in sede di conversione, ove siano «del tutto eterogenee al contenuto o alle ragioni di necessità e urgenza proprie del decreto», devono ritenersi illegittime «perché esorbitano dal potere di conversione attribuito dalla Costituzione al Parlamento»;
 che, osserva il giudice a quo, il d.l. n. 103 del 2010, titolato «Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo», era stato emesso sulla base di presupposti di necessità ed urgenza, così esplicitati nel preambolo: a) «[c]onsiderata la necessità di completare la procedura di dismissione dell'intero capitale sociale di Tirrenia di Navigazione S.p.A. e, nel contempo, di assicurare l'esatto adempimento delle obbligazioni derivanti dalle  convenzioni di pubblico servizio di trasporto marittimo fino al 30 settembre 2010, data della loro scadenza stabilita dalla legge»; b) «[r]itenuta la straordinaria necessità ed urgenza di assicurare la regolarità del servizio pubblico di trasporto marittimo e, nel contempo, la continuità territoriale con le isole, con particolare riguardo al periodo di picco del traffico estivo»;
 che, alla luce di tali finalità, la disposizione di cui all'art. 7-ter del d.lgs. n. 286 del 2005, aggiunta dalla legge n. 127 del 2010 in sede di conversione del d.l. n. 103 del 2010, nell'introdurre «l'azione diretta del vettore che ha svolto un servizio di trasporto su incarico di altro vettore nei confronti di tutto coloro che hanno ordinato il trasporto, con riferimento all'attività di autotrasporto di merci per conto di terzi», sarebbe «completamente scollegata» dai contenuti già disciplinati dal decreto-legge, riguardanti esclusivamente la necessità di assicurare la regolarità del servizio  pubblico di trasporto marittimo;
 che, dunque, verrebbe a mancare del tutto il nesso di interrelazione funzionale tra la disposizione censurata e quelle originarie del decreto-legge, con conseguente violazione dell'art. 77, secondo comma, Cost., «essendo stata immessa nell'ordinamento una disciplina estranea ai contenuti ed alle finalità del decreto legge»;
 che si è costituita in entrambi i giudizi la società Danone spa, parte opponente nei giudizi principali, esponendo che in questi ultimi le società Logitrans srl e Boncioli srl, «lamentando il mancato pagamento dei servizi di trasporto asseritamente resi», hanno promosso procedimenti monitori dinanzi al Tribunale ordinario di Grosseto, «non solo nei confronti della propria controparte contrattuale Logipi, ma anche nei confronti di Danone, invocando la sua responsabilità solidale quale "mittente" della merce», proprio ai sensi della norma censurata dal rimettente e della cui costituzionalità anche la società Danone spa dubita, per l'eterogeneità delle norme aggiunte in sede di conversione rispetto alle disposizioni primigenie del d.l. n. 103 del 2010; 
 che la società Danone spa ha prospettato anche la carenza dei requisiti della straordinarietà ed urgenza di provvedere con decreto-legge, richiamando giurisprudenza costituzionale in ordine all'inidoneità della legge di conversione a sanare il vizio di difetto originario di tali presupposti; 
 che si sono costituite anche le società Logitrans srl e Boncioli srl, parti opposte dei giudizi a quibus, sostenendo la non fondatezza della questione di legittimità costituzionale sollevata dal Tribunale ordinario di Grosseto, in quanto la Corte costituzionale, nelle sentenze n. 32 del 2014 e n. 22 del 2012, avrebbe ravvisato una violazione dell'art. 77, secondo comma, Cost., solo nelle ipotesi in cui le disposizioni aggiunte in sede di conversione del decreto-legge siano totalmente «estranee» o addirittura «intruse», cioè tali da interrompere, in parte qua, ogni correlazione tra il decreto-legge e la legge di conversione, situazione che non ricorrerebbe nel caso di specie, in quanto la disposizione censurata interverrebbe sul medesimo settore - ossia sul «trasporto» - regolato dalle originarie norme del decreto-legge;
 che le società opposte nei giudizi principali, in considerazione del fatto che la norma introdotta in sede di conversione è stata inserita nell'ambito del d.lgs. n. 286 del 2005, hanno esposto diffusamente le ragioni per le quali dovrebbe essere considerata non fondata la questione di legittimità costituzionale «per "eccesso di delega"»;
 che nei giudizi è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la non fondatezza della questione, non potendosi definire la disposizione censurata "totalmente estranea" o addirittura "intrusa";
 che, in prossimità dell'udienza pubblica, tutte le parti private e l'Avvocatura generale dello Stato hanno depositato ulteriori memorie, di identico contenuto per i due giudizi, con le quali hanno riproposto gli argomenti articolati negli atti di costituzione e d'intervento;
 che le società Logitrans srl e Boncioli srl hanno, in aggiunta, eccepito l'inammissibilità della questione di legittimità costituzionale sollevata dal Tribunale ordinario di Grosseto per «difetto di motivazione in punto di rilevanza», per non avere il giudice rimettente, in nessuna delle due ordinanze, adeguatamente descritto le fattispecie concrete oggetto dei giudizi principali, così impedendo alla Corte costituzionale di effettuare la necessaria verifica della rilevanza della questione.
 Considerato che il Tribunale ordinario di Grosseto, con due distinte ordinanze di analogo tenore, solleva, in riferimento all'art. 77, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 7-ter del decreto legislativo 21 novembre 2005, n. 286 (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore), aggiunto dall'art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103 (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), a sua volta inserito dalla legge di conversione 4 agosto 2010, n. 127;
 che, in considerazione dell'identità della disposizione censurata e del parametro evocato, i giudizi vanno riuniti, per essere definiti con unica decisione; 
 che la disposizione denunciata, aggiunta in sede di conversione, a parere del rimettente sarebbe contrastante con l'evocato parametro costituzionale, presentando un contenuto disomogeneo rispetto a quello dell'originario d.l. n. 103 del 2010;
 che, infatti, l'art. 7-ter del d.lgs. n. 286 del 2005, nell'introdurre «l'azione diretta del vettore che ha svolto un servizio di trasporto su incarico di altro vettore nei confronti di tutto coloro che hanno ordinato il trasporto, con riferimento all'attività di autotrasporto di merci per conto di terzi», sarebbe «completamente scollegata» dai contenuti già disciplinati dal decreto-legge, riguardanti esclusivamente la necessità di assicurare la regolarità del servizio  pubblico di trasporto marittimo; 
 che il giudice a quo censura espressamente l'art. 7-ter del d.lgs. n. 286 del 2005, ma in quanto introdotto dall'art. 1-bis, comma 2, lettera e), del d.l. n. 103 del 2010, a sua volta aggiunto in sede di conversione dalla legge n. 127 del 2010, sicché è in relazione a quest'ultima disposizione, nella parte in cui inserisce l'art. 7-ter nel d.lgs. n. 286 del 2005, che deve ritenersi indirizzato il sospetto di contrasto con l'evocato parametro costituzionale;
 che non possono essere presi in considerazione i profili d'illegittimità costituzionale prospettati esclusivamente dalle parti private, ma non fatti propri dal giudice rimettente, in ordine alla carenza dei requisiti della straordinarietà ed urgenza di provvedere con decreto-legge ed all'eccesso di delega, in quanto l'oggetto del giudizio di legittimità costituzionale in via incidentale è limitato alle disposizioni, alle censure ed ai parametri indicati nell'ordinanza di rimessione (ex multis, da ultimo, sentenze n. 14, n. 12, n. 10 e n. 4 del 2018);
 che, così delimitato il thema decidendum, la questione di legittimità costituzionale deve essere dichiarata manifestamente inammissibile;
 che il giudice a quo, infatti, si è limitato ad affermare che la questione «è sicuramente rilevante ai fini della decisione delle cause riunite, in quanto il diritto di credito azionato con i decreti ingiuntivi opposti dalla società convenuta opposta si fonda sulla disposizione sopracitata»; 
 che il rimettente, dunque, non ha indicato le ragioni per cui la norma censurata debba applicarsi nei giudizi a quibus, né ha spiegato adeguatamente perché la decisione sulla questione di legittimità costituzionale sollevata risulti pregiudiziale ai fini della definizione dei processi principali;
 che l'art. 7-ter del d.lgs. n. 286 del 2005 è inserito nell'ambito della disciplina dettata per l'attività consistente nel trasferimento di cose di terzi su strada mediante autoveicoli, dietro il pagamento di un corrispettivo, eseguita in modo professionale e non strumentale ad altre attività (art. 2, comma 1, lettera a, del d.lgs. n. 286 del 2005);
 che la norma censurata attribuisce un'azione diretta al vettore - che abbia svolto un servizio di trasporto su incarico di altro vettore, a sua volta obbligato ad eseguire la prestazione in forza di contratto stipulato con precedente vettore o direttamente con il mittente, inteso come mandante effettivo della consegna - nei confronti di tutti coloro che hanno ordinato il trasporto, ma richiede che il primo sia in possesso dei requisiti di cui all'art. 2, comma 1, lettera b), del medesimo d.lgs. n. 286 del 2005;
 che il giudice a quo nulla riferisce né sul tipo di contratto di trasporto sottoposto alla sua cognizione (in particolare se esso rientri nell'ambito dell'autotrasporto di cose per conto terzi), né in ordine al possesso, da parte delle società opposte - creditrici del corrispettivo - dei requisiti di cui all'art. 2, comma 1, lettera b), del d.lgs. n. 286 del 2005;
  che il rimettente neppure chiarisce se le prestazioni da cui sarebbero derivati i crediti azionati siano state eseguite successivamente alla data indicata dal comma 3 dell'art. 1-bis del d.l. n. 103 del 2010, come convertito, secondo cui «[l]e disposizioni di cui al comma 2, lettera e), si applicano decorso un anno dalla data di entrata in vigore della legge di conversione del presente decreto»; 
 che il carattere pregiudiziale della questione deve emergere con immediatezza ed evidenza dalla descrizione della fattispecie svolta dal rimettente (sentenze n. 218 e n. 42 del 2017), sicché la sua omessa o insufficiente descrizione, risolvendosi in un difetto di motivazione sulla rilevanza (ordinanza n. 129 del 2017), preclude il necessario controllo rimesso, sul punto, a questa Corte e rende la questione manifestamente inammissibile (ordinanze n. 210, n. 187 e n. 79 del 2017);
 che le scarne informazioni fornite dalla società Danone spa negli atti di costituzione in giudizio - peraltro insufficienti a colmare le lacune descrittive segnalate, relative ad aspetti decisivi ai fini dell'applicazione della norma censurata - non possono comunque valere a sanare il difetto delle ordinanze di rimessione, le quali esibiscono un vizio che, per costante giurisprudenza costituzionale, non è emendabile né attraverso la lettura degli atti di causa (sentenza n. 276 del 2016; ordinanze n. 12 del 2017, n. 196 del 2016, n. 148 e n. 122 del 2015) né in forza delle deduzioni delle parti (ordinanza n. 209 del 2015); 
 che, infatti, la motivazione dell'ordinanza di rimessione deve contenere tutte le indicazioni indispensabili per una corretta ricostruzione della fattispecie oggetto del giudizio a quo, richiesta non solo in relazione alle condizioni di ammissibilità della questione di legittimità costituzionale (ordinanza n. 120 del 2015), ma anche al fine di valutare la non manifesta infondatezza di quest'ultima (ex plurimis, sentenze n. 56 del 2015 e n. 128 del 2014);
 che, in definitiva, l'insufficiente descrizione delle fattispecie concrete oggetto dei giudizi a quibus impedisce il necessario controllo in punto di rilevanza e rende la questione manifestamente inammissibile (da ultimo, sentenza n. 251 del 2017).</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 riuniti i giudizi,&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103 (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), convertito in legge 4 agosto 2010, n. 127, nella parte in cui inserisce l'art. 7-ter nel decreto legislativo 21 novembre 2005, n. 286 (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore), sollevata, in riferimento all'art. 77, secondo comma, della Costituzione, dal Tribunale ordinario di Grosseto, con le ordinanze indicate in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 febbraio 2018.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Nicolò ZANON, Redattore&#13;
 Filomena PERRONE, Cancelliere&#13;
 Depositata in Cancelleria il 23 febbraio 2018.&#13;
 Il Cancelliere&#13;
 F.to: Filomena PERRONE</block>
    </conclusions>
  </judgment>
</akomaNtoso>
