<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/130/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/130/"/>
          <FRBRalias value="ECLI:IT:COST:2018:130" name="ECLI"/>
          <FRBRdate date="23/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="130"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/130/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/130/ita@/!main"/>
          <FRBRdate date="23/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2018/130/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2018/130/ita@.xml"/>
          <FRBRdate date="23/05/2018" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="13/06/2018" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2018</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>LATTANZI</cc:presidente>
        <cc:relatore_pronuncia>Franco Modugno</cc:relatore_pronuncia>
        <cc:data_decisione>23/05/2018</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli artt. 3, comma 2; 5, comma 1; 11, comma 2; 13, 14 e 15 della legge della Regione Lazio 10 marzo 2017, n. 2 (Disposizioni per la realizzazione, manutenzione, gestione, promozione e valorizzazione della rete dei cammini della Regione Lazio. Modifiche alla legge regionale 6 agosto 2007, n. 13, concernente l'organizzazione del sistema turistico laziale e successive modifiche), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 13-16 maggio 2017, depositato in cancelleria il 23 maggio 2017, iscritto al n. 40 del registro ricorsi 2017 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 25, prima serie speciale, dell'anno 2017.
 Visto l'atto di costituzione della Regione Lazio; 
 udito nella camera di consiglio del 23 maggio 2018 il Giudice relatore Franco Modugno.
 Ritenuto che con ricorso depositato in cancelleria il 23 maggio 2017 (r.r. n. 40 del 2017), il Presidente del Consiglio dei ministri ha impugnato gli artt. 3, comma 2; 5, comma 1; 11, comma 2; 13, 14 e 15 della legge della Regione Lazio 10 marzo 2017, n. 2 (Disposizioni per la realizzazione, manutenzione, gestione, promozione e valorizzazione della rete dei cammini della Regione Lazio. Modifiche alla legge regionale 6 agosto 2007, n. 13, concernente l'organizzazione del sistema turistico laziale e successive modifiche), per contrasto con gli artt. 117, secondo comma, lettera s), e sesto comma, e 118, primo e secondo comma, della Costituzione;
 che il ricorrente rileva che con l'impugnata legge regionale sono state dettate norme per la realizzazione, manutenzione, gestione, promozione e valorizzazione della rete dei cammini della Regione Lazio; rete la quale interessa tutto il territorio regionale, comprese quelle parti ricadenti nei parchi nazionali e nelle aree protette regionali;
 che, nella parte in cui interessano anche queste ultime porzioni di territorio, le disposizioni censurate sarebbero gravemente lesive delle funzioni che la legge 6 dicembre 1991, n. 394 (Legge quadro sulle aree protette) attribuisce agli Enti parco e ai soggetti gestori delle altre aree protette esistenti nel territorio regionale e, pertanto, risulterebbero costituzionalmente illegittime;
 che la giurisprudenza costituzionale, infatti, avrebbe riconosciuto che la legge quadro sulle aree protette è ascrivibile alla competenza legislativa esclusiva dello Stato in materia di «tutela dell'ambiente e dell'ecosistema» e che, in tale prospettiva, sottopone le aree protette a una disciplina inderogabile da parte delle Regioni;
 che, in particolare, tale disciplina è imperniata sulla regolamentazione delle attività che possono essere svolte al loro interno e sulla predisposizione di strumenti programmatici e gestionali; disciplina con la quale le disposizioni impugnate presenterebbero plurimi profili di contrasto;
 che, con atto depositato in cancelleria il 19 giugno 2017, si è costituita in giudizio la Regione Lazio, chiedendo che sia dichiarata l'inammissibilità o l'infondatezza di tutte le questioni di legittimità costituzionale proposte;
 che, in data 12 dicembre 2017, il Presidente del Consiglio dei ministri, su conforme deliberazione del Consiglio dei ministri del 22 novembre 2017, ha depositato atto di rinuncia al ricorso, in considerazione delle modifiche apportate alle disposizioni impugnate ad opera dell'art. 17, comma 95, della legge della Regione Lazio 14 agosto 2017, n. 9 (Misure integrative, correttive e di coordinamento in materia di finanza pubblica regionale. Disposizioni varie), le quali «hanno determinato il venir meno dei motivi del ricorso»;
 che, in data 6 febbraio 2018, la Regione Lazio, su conforme deliberazione della Giunta regionale del 30 gennaio 2018, ha depositato atto di accettazione della predetta rinuncia al ricorso.
 Considerato che il Presidente del Consiglio dei ministri ha rinunciato al ricorso indicato in epigrafe;
 che la rinuncia è stata accettata dalla Regione Lazio;
 che la rinuncia al ricorso accettata dalla controparte costituita determina, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.
 Visti l'art. 26, secondo comma, della legge 11 marzo 1953, n. 87, e gli artt. 9, comma 2, e 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara estinto il processo.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 maggio 2018.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Franco MODUGNO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 13 giugno 2018.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
