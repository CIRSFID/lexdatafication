<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/141/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/141/"/>
          <FRBRalias value="ECLI:IT:COST:2017:141" name="ECLI"/>
          <FRBRdate date="24/05/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="141"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/141/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/141/ita@/!main"/>
          <FRBRdate date="24/05/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2017/141/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2017/141/ita@.xml"/>
          <FRBRdate date="24/05/2017" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="14/06/2017" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2017</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GROSSI</cc:presidente>
        <cc:relatore_pronuncia>Franco Modugno</cc:relatore_pronuncia>
        <cc:data_decisione>24/05/2017</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 10-bis del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), promosso dal Tribunale ordinario di Trieste, nel procedimento penale a carico di R. R., con ordinanza del 3 giugno 2015, iscritta al n. 72 del registro ordinanze 2016 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 15, prima serie speciale, dell'anno 2016. 
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 24 maggio 2017 il Giudice relatore Franco Modugno. 
 Ritenuto che, con ordinanza del 3 giugno 2015, il Tribunale ordinario di Trieste ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 10-bis del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), nella parte in cui, con riferimento ai fatti commessi sino al 17 settembre 2011, punisce l'omesso versamento delle ritenute risultanti dalla certificazione rilasciata ai sostituiti per un ammontare superiore ad euro 50.000, anziché ad euro 103.291,38, per ciascun periodo d'imposta;
 che il giudice a quo riferisce di essere investito del processo nei confronti di una persona imputata del reato previsto dalla norma censurata, per aver omesso di versare, in qualità di legale rappresentante di un ente in liquidazione, entro il termine per la presentazione della dichiarazione annuale di sostituto d'imposta, ritenute per un importo complessivo di euro 100.385 in relazione all'anno di imposta 2007: donde la rilevanza della questione;
 che quanto, poi, alla non manifesta infondatezza, il rimettente osserva come, con la sentenza n. 80 del 2014, la Corte costituzionale abbia dichiarato costituzionalmente illegittimo, in riferimento all'art. 3 Cost., l'art. 10-ter del d.lgs. n. 74 del 2000 nella parte in cui, con riferimento ai fatti commessi sino al 17 settembre 2011, puniva l'omesso versamento dell'imposta sul valore aggiunto (IVA), dovuta in base alla relativa dichiarazione annuale, per importi non superiori, per ciascun periodo di imposta, ad euro 103.291,38;
 che la Corte ha ritenuto, in specie, lesiva del principio di eguaglianza la previsione, per il delitto di omesso versamento dell'IVA, di una soglia di punibilità (euro 50.000) inferiore a quelle stabilite per la dichiarazione infedele e l'omessa dichiarazione dagli artt. 4 e 5 del medesimo decreto legislativo (rispettivamente, euro 103.291,38 ed euro 77.468,53), prima della loro modifica in diminuzione ad opera dal decreto-legge 13 agosto 2011, n. 138 (Ulteriori misure urgenti per la stabilizzazione finanziaria e per lo sviluppo), convertito, con modificazioni, in legge 14 settembre 2011, n. 148: modifica operante, per espressa previsione normativa, in rapporto ai soli fatti commessi dopo il 17 settembre 2011;
 che in questo modo, infatti, veniva riservato un trattamento deteriore a comportamenti di evasione tributaria meno insidiosi e lesivi degli interessi del fisco, attenendo l'omesso versamento a somme di cui lo stesso contribuente si era riconosciuto debitore nella dichiarazione annuale dell'IVA;
 che, ad avviso del giudice a quo, la medesima conclusione si imporrebbe anche in rapporto al delitto di omesso versamento di ritenute certificate;
 che le figure criminose di cui agli artt. 10-bis e 10-ter del d.lgs. n. 74 del 2000 presenterebbero, infatti, una struttura del tutto simile, essendo volte - secondo il rimettente - entrambe a punire il soggetto che omette di versare somme al fisco nel termine di legge, dopo essersene riconosciuto debitore nella dichiarazione annuale;
 che l'art. 10-ter richiama, d'altra parte, espressamente l'art. 10-bis in relazione sia alla pena, sia alla soglia di punibilità, mutuandone, quindi, «l'impostazione sanzionatoria»;
 che la quantificazione in euro 50.000 della soglia di punibilità del delitto in questione, quanto ai fatti commessi sino al 17 settembre 2011, violerebbe quindi l'art. 3 Cost., sia con riferimento alle soglie di punibilità previste dagli artt. 4 e 5 del d.lgs. n. 74 del 2000, prima della riforma introdotta con il d.l. n. 138 del 2011, sia con riferimento alla soglia stabilita dall'art. 10-ter del medesimo decreto legislativo, quale risultante a seguito della richiamata sentenza n. 80 del 2014;
 che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o, in subordine, infondata.
 Considerato che il Tribunale ordinario di Trieste dubita, in riferimento all'art. 3 della Costituzione, della legittimità costituzionale dell'art. 10-bis del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), nella parte in cui, con riferimento ai fatti commessi sino al 17 settembre 2011, punisce l'omesso versamento delle ritenute risultanti dalla certificazione rilasciata ai sostituiti per un ammontare superiore ad euro 50.000, anziché ad euro 103.291,38, per ciascun periodo d'imposta;
 che, successivamente all'ordinanza di rimessione, è intervenuto il decreto legislativo 24 settembre 2015, n. 158 (Revisione del sistema sanzionatorio, in attuazione dell'articolo 8, comma 1, della legge 11 marzo 2014, n. 23), il cui art. 7 ha modificato la norma censurata; 
 che la novella del 2015 ha previsto che le ritenute, il cui omesso versamento assume rilievo penale, possano risultare, oltre che dalla certificazione rilasciata ai sostituiti, anche dalla dichiarazione di sostituto d'imposta (donde il nuovo nomen iuris del reato, risultante dalla rubrica, di «Omesso versamento di ritenute dovute o certificate»), innalzando, al tempo stesso - per quanto qui più interessa - la soglia di punibilità dell'illecito dai precedenti euro 50.000 a euro 150.000 per ciascun periodo d'imposta: dunque, ad un importo più elevato di quello che il giudice a quo ha chiesto a questa Corte di introdurre, con riguardo ai fatti commessi sino al 17 settembre 2011; 
 che, conformemente a quanto è già avvenuto per analoghe questioni (ordinanze n. 230, n. 229, n. 89 e n. 14 del 2016, n. 256 del 2015), va quindi disposta la restituzione degli atti al giudice a quo per un nuovo esame della rilevanza e della non manifesta infondatezza della questione sollevata alla luce dello ius superveniens.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 1, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 ordina la restituzione degli atti al Tribunale ordinario di Trieste.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 maggio 2017.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Franco MODUGNO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 14 giugno 2017.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
