<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/295/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/295/"/>
          <FRBRalias value="ECLI:IT:COST:2002:295" name="ECLI"/>
          <FRBRdate date="19/06/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="295"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/295/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/295/ita@/!main"/>
          <FRBRdate date="19/06/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2002/295/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2002/295/ita@.xml"/>
          <FRBRdate date="19/06/2002" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="28/06/2002" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2002</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>VARI</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>19/06/2002</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: 
 Presidente: Massimo VARI; 
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, 
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto 
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, 
Francesco AMIRANTE;</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>Sentenza</docType>nel  giudizio di legittimità costituzionale dell'art. 262 del codice 
penale  promosso con ordinanza emessa il 22 febbraio 2001 dal giudice 
per  le indagini preliminari del Tribunale di Genova nel procedimento 
penale  a  carico  di  B.G. ed altri, iscritta al n. 614 del registro 
ordinanze   2001   e   pubblicata   nella  Gazzetta  Ufficiale  della 
Repubblica, 1ª serie speciale, n. 34 dell'anno 2001. 
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei 
ministri; 
    Udito  nella  camera di consiglio del 27 febbraio 2002 il giudice 
relatore Giovanni Maria Flick.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. - Con  ordinanza  emessa il 22 febbraio 2001 - nel corso di un 
processo  penale  nei  confronti di persone imputate del reato di cui 
all'art. 262  del  codice  penale  (rivelazione di notizie di cui sia 
stata vietata la divulgazione) per avere, rispettivamente, consegnato 
a  persona  non legittimata e ottenuto documenti contenenti notizie a 
carattere  "riservato",  relative  alla  costruzione di un carcere di 
massima  sicurezza  -  il  giudice  per  le  indagini preliminari del 
Tribunale   di   Genova   ha   sollevato  questioni  di  legittimità 
costituzionale  del  citato art. 262 del codice penale, per contrasto 
con gli artt. 3 e 25 della Costituzione. 
    L'ordinanza  di rimessione - pronunciata nell'udienza preliminare 
- rileva, in via preliminare, come, contrariamente a quanto sostenuto 
da   una   parte   della  dottrina,  la  norma  impugnata  non  possa 
considerarsi  implicitamente  abrogata  dalla  legge 27 ottobre 1977, 
n. 801,  la quale ha definito bensì il concetto di segreto di Stato, 
ma  senza  nulla  disporre  in  ordine  alla  materia  delle "notizie 
riservate",  che  pure  formano  oggetto  di  tutela  nell'ambito dei 
delitti   contro   la   personalità  dello  Stato.  Le  due  nozioni 
risulterebbero,  d'altro canto, tra loro distinte, dovendo il segreto 
essere  inteso  come notizia che non può essere divulgata in ragione 
dei  superiori interessi dello Stato, e la notizia riservata, invece, 
come un quid minus, ossia come informazione che può essere divulgata 
solo  a  certe  condizioni  e  a determinate categorie di persone per 
ragioni di "alta amministrazione". 
    La  disposizione  denunciata - qualificabile come norma penale in 
bianco - violerebbe peraltro il principio di tassatività della legge 
penale,  sancito  dall'art. 25  Cost.,  in  quanto non delineerebbe i 
tratti   salienti   della   fattispecie   punibile,   lasciandone  la 
determinazione all'autorità amministrativa. 
    Al  riguardo, il giudice a quo ricorda come questa Corte abbia in 
più occasioni affermato che il principio di legalità non è violato 
quando  sia  una  legge  dello  Stato  ad  indicare  con  sufficiente 
specificazione  i  presupposti, i caratteri, il contenuto ed i limiti 
dei  provvedimenti  emessi  dall'autorità  non legislativa, alla cui 
trasgressione  deve  seguire  la  pena;  mentre spetta poi al giudice 
penale  indagare,  volta  per  volta,  se  il provvedimento sia stato 
legittimamente emesso. 
    Tale  condizione  non  risulterebbe  tuttavia  soddisfatta  nella 
specie,  giacché  l'individuazione  concreta  delle  notizie che non 
possono  essere  divulgate,  agli  effetti  dell'art. 262  cod. pen., 
verrebbe  affidata  ad atti amministrativi emessi in virtù di poteri 
non direttamente conferiti dalla legge e senza alcuna precisazione in 
ordine  al  concetto  di notizia riservata, i cui limiti resterebbero 
pertanto - a differenza di quanto avviene per il segreto di Stato, in 
virtù della citata legge n. 801 del 1977 - "assai incerti e labili". 
La  circostanza  che  l'art. 262  cod. pen non indichi i motivi per i 
quali la divulgazione delle notizie può essere vietata, rimettendoli 
così    anch'essi    in    toto   all'apprezzamento   dell'autorità 
amministrativa,  impedirebbe  d'altro canto al giudice di valutare se 
il divieto di divulgazione sia stato legittimamente imposto. 
    Sotto diverso profilo, poi, la norma impugnata, nel comminare una 
pena  identica  nel  massimo  a  quella  stabilita dall'art. 261 cod. 
pen. per  il  reato  di rivelazione di segreti di Stato (ventiquattro 
anni  di reclusione) - essendo il trattamento sanzionatorio delle due 
fattispecie differenziato solo in rapporto al minimo - si porrebbe in 
contrasto  con  l'art. 3  Cost.  L'equiparazione  della  pena massima 
risulterebbe  difatti  priva di giustificazione, stante la diversità 
dei  beni protetti dalle due norme incriminatrici: infatti l'art. 261 
cod.  pen.  tutelerebbe  - alla luce della definizione del segreto di 
Stato data dall'art. 12 della legge n. 801 del 1977 - l'unità fisica 
dello Stato rispetto ad attacchi esterni od interni, ed il continuo e 
corretto funzionamento degli organi costituzionali; mentre l'art. 262 
cod. pen. sarebbe posto a salvaguardia di interessi non individuabili 
a  priori,  ma  comunque  privi  di  rango costituzionale e di minore 
valore. 
    La disposizione denunciata violerebbe, da ultimo, il principio di 
legalità  della  pena, stabilito dall'art. 25, secondo comma, Cost., 
in ragione dell'eccessivo divario tra la pena edittale minima da essa 
comminata  (anni  tre  di  reclusione,  riducibili  a due nel caso di 
riconoscimento  delle  circostanze  attenuanti  generiche,  o  a pena 
ancora  inferiore  ove  si  proceda  con  riti  alternativi) e quella 
massima (anni ventiquattro di reclusione, quale limite desumibile, in 
difetto di specifica indicazione, dall'art. 23 cod. pen.). 
    Richiamando  la  sentenza  di  questa  Corte  n. 299 del 1992, il 
rimettente  rileva  come  il  principio  di  legalità della pena non 
imponga  al  legislatore  di  determinare in misura rigida e fissa la 
pena  per  ciascun tipo di reato, poiché lo strumento più idoneo al 
conseguimento  delle  finalità della pena e più congruo rispetto al 
principio  di  uguaglianza  è quello del conferimento al giudice del 
potere  discrezionale  di  stabilire  in concreto, fra un minimo e un 
massimo,  la  sanzione  da irrogare, al fine di adeguare quest'ultima 
alle  specifiche  caratteristiche  del  singolo  caso.  Tuttavia,  la 
determinazione legislativa del minimo e del massimo edittale non deve 
eccedere   il   margine   di   elasticità  necessario  a  consentire 
l'individualizzazione   della   pena,   secondo   i  criteri  di  cui 
all'art. 133  cod.  pen.,  in  correlazione  alla  variabilità delle 
fattispecie  concrete  e delle tipologie soggettive rapportabili alla 
fattispecie  astratta:  altrimenti, la predeterminazione della misura 
della pena diverrebbe solo apparente e il potere conferito al giudice 
si trasformerebbe da discrezionale in arbitrario, investendo non più 
soltanto  la valutazione delle particolarità del caso singolo, ma la 
stessa  individuazione  del  disvalore  del fatto tipico (compito che 
spetta invece al legislatore). 
    Tale  ultima  situazione  ricorrerebbe  puntualmente nell'ipotesi 
dell'art. 262  cod.  pen.,  in  quanto  -  pur  tenendo  conto  delle 
circostanze  aggravanti  speciali da esso previste, che comportano in 
determinati  casi un aumento della pena minima - l'eccessiva ampiezza 
della  forbice  tra  il  minimo  ed il massimo edittale finirebbe, in 
pratica,  per rimettere al giudice anche la valutazione del disvalore 
oggettivo  del  fatto, tanto più a fronte della rimarcata incertezza 
sulla componente precettiva della norma incriminatrice. 
    2. - Nel   giudizio   di   costituzionalità  è  intervenuto  il 
Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso 
dall'Avvocatura  generale  dello  Stato,  il  quale ha chiesto che la 
questione sia dichiarata non fondata. 
    Ad  avviso  dell'Avvocatura  erariale,  non sarebbe riscontrabile 
alcuna   violazione   del   principio   di   tassatività:  la  norma 
incriminatrice  reprimerebbe,  infatti,  una  condotta i cui contorni 
risultano  sufficientemente definiti, non potendosi trarre argomento, 
in  contrario,  dalla  circostanza  che  sia  la pubblica autorità a 
stabilire  quali notizie debbano considerarsi riservate. La lamentata 
impossibilità  di controllo e di eventuale disapplicazione dell'atto 
amministrativo,  d'altro  canto,  lungi  dal costituire un vulnus del 
principio  di  tassatività,  discenderebbe  proprio dalla previsione 
della  norma  incriminatrice,  la  quale,  a  differenza che in altre 
ipotesi,  non  richiederebbe,  fra  gli elementi del fatto tipico, un 
atto legittimo. 
    Quanto,  poi,  alla  presunta  violazione  dell'art. 25,  secondo 
comma,  Cost.,  il  divario  fra  il  massimo  e il minimo della pena 
edittale   previsto   per   il   delitto   in   questione,  ancorché 
significativo, non comporterebbe comunque l'incostituzionalità della 
norma  impugnata,  spettando pur sempre al legislatore di graduare la 
pena  in  modo  che  il  giudice  di  merito  possa adeguarla al caso 
concreto,  così  da  renderla  funzionale  al perseguimento dei suoi 
scopi. 
    Analoghe  conclusioni  potrebbero  formularsi ove si effettui una 
comparazione  con  la  pena  prevista  dall'art. 261  cod.  pen., che 
risulterebbe  adeguatamente  elevata  nel  minimo, in conseguenza del 
ritenuto maggior   disvalore   insito  nell'aggressione  ad  un  bene 
meritevole di particolare considerazione, quale il segreto di Stato.Considerato in diritto1. - Il  giudice  per  le  indagini  preliminari del Tribunale di 
Genova  dubita della legittimità costituzionale, in riferimento agli 
artt. 3  e  25  della  Costituzione, dell'art. 262 del codice penale, 
che,  con  disposizione  collocata  nell'ambito dei delitti contro la 
personalità dello Stato, punisce chiunque rivela od ottiene "notizie 
delle  quali l'Autorità competente ha vietato la divulgazione" (c.d. 
notizie riservate). 
    Ad   avviso   del  rimettente,  risulterebbe  leso  anzitutto  il 
principio  di  tassatività  della legge penale, sancito dall'art. 25 
Cost.,  essendosi  al cospetto di una norma penale in bianco rispetto 
alla quale mancherebbe una sufficiente specificazione legislativa dei 
presupposti,   dei   caratteri,   del  contenuto  e  dei  limiti  dei 
provvedimenti  amministrativi  alla cui trasgressione deve seguire la 
pena: carenza, questa, che - segnatamente a fronte del silenzio della 
legge  circa  i  fini  per  i  quali  il  divieto  di divulgazione di 
determinate  notizie  può  essere  imposto  - precluderebbe anche il 
sindacato  incidentale  di  legittimità  del  giudice  ordinario sul 
provvedimento, in vista della sua eventuale disapplicazione. 
    La  norma  incriminatrice impugnata - comminando una pena uguale, 
nel massimo, a quella prevista dall'art. 261 cod. pen. per il delitto 
di  rivelazione di segreti di Stato (anni ventiquattro di reclusione) 
-   si   porrebbe   altresì  in  contrasto  con  l'art. 3  Cost.  Si 
tratterebbe,  infatti,  di  equiparazione  irragionevole,  stante  la 
diversità  dei  beni  protetti dalle due disposizioni: beni che, nel 
caso dell'art. 261 cod. pen., si identificherebbero - alla luce della 
definizione  del  segreto  di  Stato offerta dall'art. 12 della legge 
24 ottobre  1977, n. 801 - nell'unità fisica dello Stato rispetto ad 
attacchi  interni  o esterni, e nel continuo e corretto funzionamento 
degli  organi costituzionali; e nell'ipotesi dell'art. 262 cod. pen., 
invece, in interessi non individuabili a priori, ma comunque privi di 
rango costituzionale. 
    Sarebbe  violato,  infine,  il principio di legalità della pena, 
enunciato  dall'art. 25, secondo comma, Cost. L'eccessivo divario tra 
la  pena  edittale  minima  e  massima  comminata  per  il delitto in 
questione  -  rispettivamente  tre anni di reclusione (che potrebbero 
scendere  a due, in caso di concessione delle attenuanti generiche, e 
a  pena ancora inferiore nell'ipotesi del ricorso a riti alternativi) 
e  ventiquattro  anni  di  reclusione  (quale  limite  desumibile, in 
difetto   di   specifica   indicazione,  dall'art. 23  cod.  pen.)  - 
lascerebbe  infatti  al giudice un margine talmente ampio, da rendere 
arbitrario  il  suo  potere di determinazione della pena in concreto; 
tale  potere, pertanto, cesserebbe di essere strumentale all'esigenza 
di  adeguamento  della  risposta  sanzionatoria  al caso singolo, per 
investire  lo  stesso  apprezzamento  del disvalore del fatto tipico: 
compito, questo, riservato per contro al legislatore. 
    2.1. - La  prima  delle  tre  censure di costituzionalità non è 
fondata. 
    Essa   poggia,   infatti,  sulla  premessa  interpretativa  della 
impossibilità  di riferire alla categoria delle "notizie riservate", 
protette  dall'art. 262  cod.  pen., le indicazioni rinvenibili nella 
legge  n. 801  del  1977  a  proposito  del segreto di Stato. Da tale 
premessa    il   giudice   a   quo   trae   il   duplice   corollario 
dell'eterogeneità delle due classi di notizie (segrete e riservate), 
sul   versante   degli  obiettivi  di  tutela;  e  della  sostanziale 
indeterminatezza  delle  condizioni  legittimanti  l'apposizione  del 
divieto   di  divulgazione,  presidiato  dalla  norma  incriminatrice 
impugnata:  norma la cui operatività verrebbe perciò a dipendere da 
valutazioni   dell'autorità   amministrativa,   svincolate  da  ogni 
parametro legale e insindacabili da parte dal giudice penale. 
    L'indicata    premessa   interpretativa   è   stata,   peraltro, 
recentemente  contraddetta  dalla  Corte di cassazione, la quale, con 
decisione  successiva  all'ordinanza  di  rimessione  (cfr.  Sez.  I, 
10 dicembre   2001-29 gennaio   2002,   n. 3348),   si   è  espressa 
nell'opposto  senso  che  le  notizie riservate - intese come notizie 
"delle  quali, pur conosciute o conoscibili in un determinato ambito, 
è   vietata   la   divulgazione   con  provvedimento  dell'autorità 
amministrativa" -  costituiscono  categoria  omogenea,  sul piano dei 
requisiti  oggettivi di pertinenza e di idoneità offensiva, rispetto 
a  quella  delle  notizie sottoposte a segreto di Stato. Facendo leva 
sul   collegamento   storico-sistematico  riscontrabile  tra  le  due 
categorie  di  notizie,  e  traendo  altresì specifico argomento dal 
regime delle esclusioni del diritto di accesso delineato dall'art. 24 
della   legge  7 agosto  1990,  n. 241  e  dalla  relativa  normativa 
regolamentare di attuazione, il giudice di legittimità ha affermato, 
più  in  particolare,  non soltanto che le notizie riservate debbono 
inerire  ai  medesimi interessi che, a mente dell'art. 12 della legge 
n. 801 del 1977, giustificano il segreto di Stato; ma altresì che la 
loro diffusione deve risultare idonea - al pari di quanto avviene per 
le  notizie  sottoposte  a  segreto  di  Stato,  in forza della norma 
definitoria  da  ultimo  citata - a recare un concreto pregiudizio ai 
predetti interessi. Nella medesima decisione si precisa, inoltre, che 
il  divieto  di  divulgazione,  analogamente  a quello impositivo del 
segreto  di Stato - concorrendo ad integrare la componente precettiva 
della   norma   incriminatrice   -  resta  soggetto  a  sindacato  di 
legittimità  da  parte  del giudice penale, segnatamente in rapporto 
agli  accennati  requisiti di inerenza contenutistica e di attitudine 
offensiva della notizia che ne costituisce oggetto. 
    Viene  prospettata, in tal modo, una possibile lettura del quadro 
normativo,  che  si  presta  a sottrarre la disposizione impugnata al 
sospetto   di   violazione   del   principio  di  tassatività  della 
fattispecie  di  reato, nonché del principio di legalità in materia 
penale   sotto   il   profilo   della  riserva  di  legge  (anch'esso 
sostanzialmente  evocato  dalla doglianza del giudice a quo). Al lume 
di   tale  lettura,  risulta  difatti  rinvenibile  nella  legge  una 
sufficiente   specificazione  dei  presupposti,  del  carattere,  del 
contenuto  e dei limiti dell'atto di natura amministrativa che impone 
il  divieto  assistito  da  sanzione  penale,  tale  da permettere un 
efficace  controllo  incidentale  di  legittimità dell'atto medesimo 
(cfr., ex plurimis, sentenze n. 333 del 1991 e n. 282 del 1990). 
    Resta  comunque  auspicabile  che il legislatore si faccia carico 
dell'esigenza  di  una  revisione complessiva della materia in esame: 
esigenza  avvertita,  per  vero, già all'epoca dell'emanazione della 
legge  n. 801  del  1977,  il  cui  art. 18  assegnava  carattere  di 
"transitorietà"  al  regime  delineato dal titolo I del libro II del 
codice  penale, in vista dell'emanazione di una "nuova legge organica 
relativa alla materia del segreto". 
    2.2. - Manifestamente inammissibili risultano, invece, le residue 
censure, che ineriscono in via esclusiva al trattamento sanzionatorio 
della figura criminosa. 
    La  questione è stata sollevata, infatti, dal giudice rimettente 
nella  veste  di  giudice dell'udienza preliminare: veste nella quale 
egli  non  è  chiamato a determinare la pena per il fatto per cui si 
procede,  essendo  il  suo  potere  decisorio,  nel  caso  di specie, 
circoscritto all'alternativa fra la sentenza di non luogo a procedere 
e il decreto che dispone il giudizio. 
    La  dedotta  irragionevolezza  della  pena  massima  e l'asserita 
eccessiva  ampiezza del divario fra il massimo e il minimo della pena 
edittale,  previsto  dall'art. 262 cod. pen., non vengono pertanto in 
alcun  modo in rilievo nel perimetro del thema decidendum del giudice 
a  quo  (cfr.,  sempre  in  riferimento  all'art. 262 cod. pen. e con 
riguardo  a  situazione  processuale  analoga,  ordinanza  n. 156 del 
2000).</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
                        LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p/>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso </block>
    </conclusions>
  </judgment>
</akomaNtoso>
