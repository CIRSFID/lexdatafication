<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/335/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/335/"/>
          <FRBRalias value="ECLI:IT:COST:2009:335" name="ECLI"/>
          <FRBRdate date="14/12/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="335"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/335/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/335/ita@/!main"/>
          <FRBRdate date="14/12/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2009/335/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2009/335/ita@.xml"/>
          <FRBRdate date="14/12/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="18/12/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>AMIRANTE</cc:presidente>
        <cc:relatore_pronuncia>Paolo Grossi</cc:relatore_pronuncia>
        <cc:data_decisione>14/12/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori:
 Presidente: Francesco AMIRANTE; Giudici : Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell’art. 537, terzo comma, del codice civile, promosso dal Tribunale ordinario di Cosenza, nel procedimento vertente tra A. B. e D. T. A. ed altri, con ordinanza del 12 giugno 2008, iscritta al n. 68 del registro ordinanze 2009 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 10, prima serie speciale, dell’anno 2009.
 Visto l’atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 23 settembre 2009 il Giudice relatore Paolo Grossi.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. –  Nel corso di una controversia civile – promossa dall’attrice contro gli eredi del de cuius (deceduto ab intestato), per ottenere il riconoscimento della paternità del medesimo e l’accertamento del proprio diritto alla eredità con la conseguente divisione –, il Tribunale ordinario di Cosenza, con ordinanza emessa il 12 giugno 2008, ha sollevato, in riferimento agli artt. 3 e 30, terzo comma, della Costituzione, questione di legittimità costituzionale dell’art. 537, terzo comma, del codice civile, il quale stabilisce che «I figli legittimi possono soddisfare in denaro o in beni immobili ereditari la porzione spettante ai figli naturali che non vi si oppongano. Nel caso di opposizione decide il giudice, valutate le circostanze personali e patrimoniali».
 Il rimettente premette in fatto che – passata in giudicato la sentenza parziale con la quale è stata accertata la paternità – i convenuti figli legittimi, in ordine alla domanda di divisione dell’eredità, hanno richiesto di liquidare in denaro la porzione spettante alla condividente coerede figlia naturale, la quale tuttavia si è opposta a tale istanza, domandando di sollevare la questione di costituzionalità della norma. In punto di rilevanza, il Tribunale afferma che – in ragione dell’opposizione dell’attrice – «dovrebbe decidere valutando le condizioni patrimoniali e personali», mentre, in assenza di tale norma, «di fronte al dissenso del figlio naturale il giudice non potrebbe in alcun caso consentire la commutazione in denaro della quota, trattandosi di eredità composta da diversi beni immobili».
 Quanto alla non manifesta infondatezza, il rimettente osserva come la norma impugnata sia stata introdotta dalla legge 19 maggio 1975, n. 151 (Riforma del diritto di famiglia), che ha abrogato – per adeguarsi al dettato dell’art. 30, terzo comma, della Costituzione – la vecchia disposizione dell’art. 574 cod. civ., in base alla quale i figli legittimi avevano il diritto potestativo di sciogliere la comunione ereditaria con i figli naturali, commutando la quota ereditaria in una somma di denaro, senza possibilità di opposizione da parte del figlio naturale e di valutazione giudiziale delle circostanze del caso concreto.
 Tuttavia, secondo il giudice a quo, la posizione del figlio naturale è cambiata nel corso degli anni, proprio in seguito alla introduzione della legge sul diritto di famiglia, e successivamente delle leggi sulla separazione e sul divorzio, sicché è divenuta anacronistica la ratio sottesa alla norma in esame, consistente nella necessità di rendere compatibile la tutela dei figli naturali con i diritti dei membri della famiglia legittima, seppure attraverso il correttivo della possibilità di opposizione con deferimento della decisione al giudice. Attualmente, infatti, «la figura del figlio naturale, oltre a non destare alcun tipo di sensazione di “estraneità” dalla famiglia, è alquanto diffusa», in quanto «il numero delle separazioni è molto alto e, nella maggior parte dei casi, uno o entrambi i coniugi iniziano una nuova convivenza, con procreazione di figli naturali (a volte riconosciuti con successivo matrimonio)», che si presentano alla comunità come eredi, per cui non può più sostenersi che il figlio legittimo costituisca il «testimone» dell’identità familiare.
 Consentire che la menzionata valutazione (in cui in base alla norma devono confluire anche gli elementi patrimoniali della sfera dei figli interessati) sia fatta dal giudice è, ad avviso del rimettente, ingiustificatamente discriminatorio nei confronti del figlio naturale, e non ne garantisce i diritti. Inoltre, il trattamento differente riservato in sede di divisione ereditaria ai figli naturali, non giustificandosi con una necessità di tutela dei diritti dei figli legittimi (che in ogni caso, per il giudice a quo, non subirebbero lesioni in caso di abrogazione della norma), contrasta con il divieto di differenziazioni basate su condizioni personali e sociali.
 D’altra parte, conclude il rimettente, l’eliminazione della norma dal nostro sistema giuridico non comporterebbe alcuna incompatibilità con la tutela della famiglia legittima, posto che, in caso di accordo, i figli legittimi potrebbero sempre acconsentire alla liquidazione in denaro o in beni immobili della loro quota (così come oggi avviene nella comunione tra più figli legittimi), e, in caso di disaccordo, non vi sarebbe comunque lesione della quota di alcuno dei discendenti.
 2. – È intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall’Avvocatura generale dello Stato, concludendo per la non fondatezza della sollevata questione.
 La difesa erariale rileva che la norma denunciata trova la sua ratio nel diverso rapporto esistente in via di fatto tra i figli legittimi e naturali e tra questi ed i beni familiari; e, pertanto, appare ragionevole riconoscere ai figli legittimi il potere di commutare la quota ereditaria dei figli naturali in valore, consentendo così di conservare i beni ereditari in capo a coloro che normalmente hanno un particolare rapporto affettivo con detti beni. D’altra parte (prosegue l’Avvocatura), la norma denunciata riconosce la parità giuridica dei figli legittimi e naturali, in quanto garantisce anche a questi ultimi una quota ereditaria di ugual valore, prevedendo una possibile diversificazione della quota ereditaria spettante ai figli naturali solamente sotto il profilo della qualità e non della quantità. D’altronde, il diritto di commutazione non è assoluto, in quanto i figli naturali possono opporsi e rimettere la questione al giudice, che dovrà decidere tenendo conto della situazione personale e patrimoniale degli stessi.Considerato in diritto1. – Il Tribunale ordinario di Cosenza censura l’art. 537, terzo comma, del codice civile, in base al quale «I figli legittimi possono soddisfare in denaro o in beni immobili ereditari la porzione spettante ai figli naturali che non vi si oppongano. Nel caso di opposizione decide il giudice, valutate le circostanze personali e patrimoniali».
 Secondo il rimettente la norma si pone in contrasto: a) con l’art. 30, terzo comma, della Costituzione, in quanto il consentire che la menzionata valutazione (in cui in base alla norma devono confluire anche gli elementi patrimoniali della sfera dei figli interessati) sia fatta dal giudice è ingiustificatamente discriminatorio nei confronti del figlio naturale, e non ne garantisce i diritti; b) con l’art. 3 Cost., poiché il trattamento differente riservato in sede di divisione ereditaria ai figli naturali, non giustificandosi con una necessità di tutela dei diritti dei figli legittimi (che in ogni caso, per il giudice a quo, non subirebbero lesioni in caso di abrogazione della norma), contrasta con il divieto di differenziazioni basate su condizioni personali e sociali.
 	2. – La questione non è fondata.
 	2.1. – L’art. 30, terzo comma, Cost. prevede espressamente che «La legge assicura ai figli nati fuori dal matrimonio ogni tutela giuridica e sociale, compatibile con i diritti dei membri della famiglia legittima».
 Questa Corte ha specificamente enucleato il duplice significato normativo attribuito dalla propria giurisprudenza al precetto costituzionale in esame, che, dal lato dei rapporti tra genitori e figli, si esprime in una regola di equiparazione dello status di figlio naturale (riconosciuto o dichiarato) allo status di figlio legittimo nei limiti di compatibilità con i diritti dei membri della famiglia legittima fondata sul matrimonio; mentre, nei rapporti della prole naturale con i parenti del genitore (ossia con la famiglia di origine del genitore e con altri suoi figli, legittimi o naturali riconosciuti), si pone come norma ispiratrice di una direttiva di sempre più adeguata tutela della condizione di diritto familiare della prole naturale (sentenze n. 377 del 1994 e n. 184 del 1990). Di conseguenza, la Corte ha anche chiarito «come dall’art. 30 della Costituzione non discenda in maniera costituzionalmente necessitata la parificazione di tutti i parenti naturali ai parenti legittimi», in quanto «un ampio concetto di “parentela naturale” non è stato recepito dal legislatore costituente, il quale si è limitato a prevedere la filiazione naturale ed a stabilirne l’equiparazione a quella legittima, peraltro con la clausola di compatibilità» (sentenza n. 532 del 2000).
 2.2. – Nello specifico ambito dei rapporti tra il figlio naturale ed i membri della famiglia legittima (e, in particolare, per quanto qui interessa, i figli legittimi), è proprio il menzionato criterio di compatibilità che rappresenta lo snodo del sistema costituzionale finalizzato alla composizione dei diritti coinvolti, che deve compiersi in un contesto (non già di discriminazione della posizione dell’uno rispetto a quella degli altri, quanto piuttosto) di riconoscimento della diversità delle posizioni in esame. Diversità nella parità di trattamento, quindi, che si coglie immediatamente dalla semplice considerazione che l’art. 30, primo e terzo comma, Cost., «come avviene nella stragrande maggioranza degli ordinamenti oggi vigenti», conosce «solo due categorie di figli: quelli nati entro e quelli nati fuori del matrimonio, senza ulteriore distinzione tra questi ultimi» (sentenza n. 494 del 2002).
 Pertanto, l’approccio alla problematica relativa alla correttezza della scelta delle concrete modalità di realizzazione del menzionato contemperamento con (o la sottordinazione ad) altri princìpi di pari o maggior peso va interamente condotto sul versante della analisi della ragionevolezza del trattamento differenziato, commisurata «alla dinamica evolutiva dei rapporti sociali» (sentenza n. 377 del 1994).
 	2.3. – L’art. 537 cod. civ. (come sostituito dall’art. 173 della legge 19 maggio 1975, n. 151), oltre a prevedere e regolamentare il diritto di commutazione in esame (terzo comma), dispone che, nella ipotesi di concorso all’eredità di figli legittimi e naturali, agli uni e agli altri siano attribuiti in egual misura i medesimi diritti successori (primo e secondo comma). Il legislatore della riforma del diritto di famiglia, quindi – modificando radicalmente quanto in precedenza previsto dall’art. 541 cod. civ. (abrogato dall’art. 177 della stessa legge n. 151 del 1975) – ha equiparato i diritti successori dei figli legittimi e naturali, contestualmente rimodulando il menzionato diritto di commutazione (che riguarda la fase di divisione dell’asse ereditario), trasformato da insindacabile diritto meramente potestativo attribuito ai figli legittimi a diritto ad esercizio puntualmente controllato, in quanto soggetto alla duplice condizione della mancata opposizione del figlio naturale e della decisione del giudice, «valutate le circostanze personali e patrimoniali».
 	Occorre rilevare che la Corte – anche se chiamata a vagliare la costituzionalità dell’abrogato art. 541 cod. civ. – ha già avuto modo di affermare come la norma oggi impugnata (che in quel giudizio era assunta quale tertium comparationis a sostegno della dedotta illegittimità della precedente disciplina) si collochi nella prospettiva del progressivo adeguamento della normativa allo spirito evolutivo promanante dal precetto costituzionale di cui al terzo comma dell’art. 30, che permea la riforma del diritto di famiglia e che caratterizza (ed indirizza) l’ampia discrezionalità lasciata al legislatore in materia; discrezionalità che, tuttavia, oltre a dover rispettare il canone di una ragionevolezza commisurata alla dinamica evolutiva dei rapporti sociali, è soggetta anche al limite, stabilito dalla medesima disposizione costituzionale, della compatibilità con i diritti dei membri della famiglia legittima (sentenza n. 168 del 1984).
 	Orbene, tali considerazioni offrono dei riferimenti significativi ed ancora attuali, anche alla luce della giurisprudenza successiva. In primo luogo, relativamente alla attribuzione della spettanza «al legislatore ordinario – stante la formulazione generica del testo del terzo comma del citato art. 30, frutto del “travaglio che portò, nell’Assemblea costituente, alla sua formulazione definitiva” (sentenza n. 54 del 1960) – di rendersi attento interprete della evoluzione del costume e della coscienza sociale, e, in conseguenza, di apprestare, in ordine alla esigenza, espressamente posta dal precetto costituzionale, della “compatibilità” della tutela dei figli nati fuori del matrimonio con i diritti dei membri della famiglia legittima, soluzioni anche diverse nel tempo, in armonia appunto con la cennata evoluzione». In secondo luogo, con riguardo al riconoscimento del fatto che «il legislatore [del 1975], muovendo dalla consapevolezza che “nessuna parte dell’ordinamento giuridico risente come il diritto familiare delle contemporanee ed opposte sollecitazioni della tradizione da un lato e del costume in evoluzione dall’altro”, ha inteso impegnare “saggezza ed equilibrio” al fine di “modulare le delicate correlazioni e l’intensità degli strumenti di tutela dei diversi interessi in gioco … per la migliore sistemazione normativa dell’istituto e soprattutto per la sua aderenza alla realtà sociale”» (sentenza n. 168 del 1984).
 	2.4. – Se, dunque, la completa equiparazione nel quantum dei diritti successori dei figli legittimi e naturali, stabilita dai primi due commi dell’art. 537 cod. civ. attua (in modo certamente obbligato) il principio della necessaria uguaglianza delle posizioni dei figli nel rapporto con il genitore dante causa (deceduto ab intestato), la scelta del legislatore di conservare in capo ai figli legittimi la possibilità di richiedere la commutazione, condizionata dalla previsione della facoltà di opposizione da parte del figlio naturale e dalla valutazione delle specifiche circostanze posta a base della decisione del giudice, non contraddice la menzionata aspirazione alla tendenziale parificazione della posizione dei figli naturali, giacché non irragionevolmente si pone ancor oggi (quale opzione costituzionalmente non obbligata né vietata) come termine di bilanciamento (compatibilità) dei diritti del figlio naturale in rapporto con i figli membri della famiglia legittima.
 	L’espresso riferimento della Costituzione al criterio di “compatibilità” assume la funzione di autentica clausola generale, aperta al divenire della società e del costume.
 È appunto in questa prospettiva che si è mosso il legislatore del nuovo diritto di famiglia, attribuendo al giudice – cui viene in definitiva demandato il riscontro della sussistenza o meno di quella che sostanzialmente può definirsi come “giusta causa” dell’opposizione del figlio naturale alla richiesta di commutazione avanzata dai figli legittimi – il ruolo di garante della parità di trattamento nella diversità, attraverso il continuo adeguamento della concreta applicazione della norma ai princìpi costituzionali. La naturale concretezza della soluzione giurisdizionale (che, ove le circostanze lo esigano, può ovviamente essere a favore del figlio naturale) permette, infatti, di calibrare la singola decisione alle specifiche circostanze personali (attinenti ai pregressi rapporti tra i figli) e patrimoniali (riguardanti la situazione dei beni lasciati in eredità, in considerazione, sia della loro migliore conservazione e gestione, sia del rapporto che lega l’erede al bene), così da scongiurare eventuali esercizi arbitrari, e quindi non meritevoli in concreto di tutela, del diritto di commutazione o della facoltà di opposizione. 
 D’altronde, la (volutamente) elastica formula linguistica adoperata dal legislatore risulta teleologicamente coerente al sistema, poiché lascia tutto il dovuto spazio all’apprezzamento discrezionale del giudice (le cui decisioni, peraltro, sono soggette, come le altre, ai normali rimedi processuali).
 Lungi, dunque, dal dirsi anacronistica (come deduce il rimettente) la ratio sottesa alla norma in esame, questa – anche per la sua formulazione “aperta” (analoga a quella prevista dall’art. 252 cod. civ. in tema di affidamento del figlio naturale e suo inserimento nella famiglia legittima) – appare viceversa idonea a consentire il recepimento nel suo ambito dispositivo (di volta in volta, e secondo il sentire dei tempi) delle singole fattispecie, commisurate proprio a quella dinamica evolutiva dei rapporti sociali, che attualizza il precetto costituzionale. Ciò, tanto più in quanto – per colmare la già evidenziata ampia latitudine del riferimento normativo alle «circostanze personali e patrimoniali» – il giudice, nella propria opzione ermeneutica, è tenuto a dare una valutazione costituzionalmente orientata, la quale appunto non può ignorare (ma deve necessariamente prendere in considerazione) la naturale evoluzione nel tempo della coscienza sociale e dei costumi.
 2.5. – La norma impugnata, pertanto, è immune dai denunciati vizi di incostituzionalità.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara non fondata la questione di legittimità costituzionale dell’art. 537, terzo comma, del codice civile, sollevata, in riferimento agli artt. 3 e 30, terzo comma, della Costituzione, dal Tribunale ordinario di Cosenza, con l’ordinanza indicata in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 14 dicembre 2009.&#13;
 F.to:&#13;
 Francesco AMIRANTE, Presidente&#13;
 Paolo GROSSI, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 18 dicembre 2009.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
