<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2005/52/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2005/52/"/>
          <FRBRalias value="ECLI:IT:COST:2005:52" name="ECLI"/>
          <FRBRdate date="13/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="52"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2005/52/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2005/52/ita@/!main"/>
          <FRBRdate date="13/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2005/52/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2005/52/ita@.xml"/>
          <FRBRdate date="13/01/2005" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="28/01/2005" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2005</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>ONIDA</cc:presidente>
        <cc:relatore_pronuncia>Alfio Finocchiaro</cc:relatore_pronuncia>
        <cc:data_decisione>13/01/2005</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Valerio ONIDA; Giudici: Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 99, comma 3, del decreto legislativo 30 maggio 2002, n. 113 (Testo unico delle disposizioni legislative in materia di spese di giustizia), promosso con ordinanza del 2 ottobre 2003 dal Tribunale di Gela sull'istanza proposta da Gianluca Convissuto, iscritta al n. 168 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 12, prima serie speciale, dell'anno 2004. 
    Udito nella camera di consiglio del 15 dicembre 2004 il Giudice relatore Alfio Finocchiaro.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1.– Il giudice del Tribunale di Gela designato dal Presidente del tribunale, con ordinanza del 2 ottobre 2003, ha sollevato, in riferimento all'art. 76 della Costituzione, questione di legittimità costituzionale dell'art. 99, comma 3, del decreto legislativo 30 maggio 2002, n. 113 (Testo unico delle disposizioni legislative in materia di spese di giustizia), nella parte in cui dispone che nel processo di opposizione avverso il provvedimento di rigetto dell'istanza di ammissione al patrocinio a spese dello Stato, ovvero di revoca dell'ammissione già accordata, l'ufficio giudiziario procede in composizione monocratica anziché collegiale. In via subordinata, ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dello stesso articolo, nella parte in cui dispone che nel processo di opposizione avverso il provvedimento adottato dal collegio, di rigetto dell'istanza di ammissione al patrocinio a spese dello Stato, ovvero di revoca dell'ammissione già accordata, l'ufficio giudiziario procede in composizione monocratica. 
    Il remittente premette che è stato presentato ricorso al Presidente del tribunale avverso il decreto con cui lo stesso tribunale, in composizione collegiale, aveva revocato l'ammissione al patrocinio a spese dello Stato, nonché di essere stato designato dal Presidente alla trattazione del ricorso in camera di consiglio. Ritenuta l'ammissibilità del ricorso ai sensi dell'art. 99, comma 1, dello stesso decreto legislativo – essendo previsto il ricorso diretto in cassazione solo per l'ipotesi speciale di revoca disposta su richiesta proveniente dall'ufficio finanziario ai sensi del successivo art. 113 – sottolinea, in ordine alla rilevanza, che si tratta di individuare pregiudizialmente la composizione collegiale o monocratica dell'organo giurisdizionale chiamato a pronunciarsi.  
    Il giudice a quo si sofferma, poi, sulla non manifesta infondatezza della questione, prospettandola in via principale e in via subordinata. 
    In via principale sostiene che la norma impugnata viola l'art. 76 della Costituzione, avendo il legislatore delegato ecceduto dalla delega conferita con l'art. 7 della legge 8 marzo 1999, n. 50 (Delegificazione e testi unici di norme concernenti procedimenti amministrativi – legge di semplificazione 1998), come modificato dall'art. 1 della legge 24 novembre del 2000, n. 340 (Disposizioni per la delegificazione di norme e per la semplificazione di procedimenti amministrativi – legge di semplificazione 1999). Dapprima, il giudice a quo sottolinea che la norma impugnata, prescrivendo che il processo che si apre con il ricorso è quello speciale previsto per gli onorari di avvocato e che l'ufficio procede in composizione monocratica, innova la disciplina previgente (art. 6, commi 4 e 5, della legge 30 luglio 1990, n.217 (Istituzione del patrocinio a spese dello Stato per i non abbienti), come modificati dalla legge 29 marzo 2001, n. 134 (Modifica della legge 30 luglio 1990, n. 217, recante istituzione del patrocinio a spese dello Stato per i non abbienti), secondo la quale il giudice del gravame è quello collegiale sulla base del rinvio procedurale all'art. 29 della legge 15 giugno 1942, n. 794 (Onorari di avvocato e di procuratore per prestazioni giudiziali in materia civile), là dove si prevede espressamente la comparizione degli interessati davanti al collegio in camera di consiglio. A sostegno della non manifesta infondatezza, richiama l'art. 7 della legge delega ed, in particolare, i criteri direttivi previsti nel comma 2, deducendo che, all'evidenza, non si rinviene la previsione della facoltà di modificare il riparto della competenza tra giudice monocratico e collegiale nella materia di interesse, diversamente da quanto il legislatore ha disposto in materia di diritto societario e di proprietà industriale e intellettuale. Infine, in riferimento all'esigenza di armonizzazione con la sopravvenuta riforma del giudice unico risultante dalla relazione governativa, il remittente sottolinea che nella legge delega non vi è traccia di tale volontà e contesta, inoltre, la pertinenza del richiamo ad un'altra applicazione già fatta dal legislatore, contenuto nella stessa relazione.  
    In via subordinata, il giudice a quo ritiene che la norma impugnata, nel prevedere la competenza di un organo monocratico in sede di giudizio di opposizione avverso provvedimenti emessi anche dal collegio (tribunale o corte d'appello), violi l'art. 3 della Costituzione, sotto il profilo del difetto di ragionevolezza, ingiustificatamente attribuendo – per la prima volta nell'ordinamento – ad un giudice monocratico, dotato di bagaglio culturale ed esperienza professionale inferiore alla terna che compone il collegio, la potestà di sindacare provvedimenti di un organo collegiale. Aggiunge che nell'ordinamento non esistono fattispecie in cui la decisione sul gravame avverso un provvedimento collegiale sia attribuita ad organo monocratico e richiama, a titolo esemplificativo, casi in cui è espressa la regola contraria: le sentenze del tribunale in composizione collegiale, soggette ad impugnazione innanzi ad un organo indefettibilmente collegiale quale è la corte di appello; i decreti emessi dal collegio nei procedimenti in camera di consiglio, in forza del generale rinvio di cui all'art. 739 cod. proc. civ.; i decreti emessi dal tribunale fallimentare. 
    2.– Nel giudizio non è intervenuto il Presidente del Consiglio dei ministri.Considerato in diritto1.– Il giudice del Tribunale di Gela, designato dal Presidente del tribunale, ha sollevato questione di legittimità costituzionale dell'art. 99, comma 3, del decreto legislativo 30 maggio 2002, n. 113 (Testo unico delle disposizioni legislative in materia di spese di giustizia), nella parte in cui dispone che nel processo di opposizione avverso il provvedimento di rigetto dell'istanza di ammissione al patrocinio a spese dello Stato, ovvero di revoca del decreto di ammissione già accordato, l'ufficio giudiziario procede in composizione monocratica anziché collegiale. In via principale, egli ha dedotto la violazione dell'art. 76 della Costituzione, per avere il legislatore delegato ecceduto dalla delega conferita con l'art. 7 della legge 8 marzo 1999, n. 50 (Delegificazione e testi unici di norme concernenti procedimenti amministrativi – legge di semplificazione 1998), come modificato dall'art. 1 della legge 24 novembre del 2000, n. 340 (Disposizioni per la delegificazione di norme e per la semplificazione di procedimenti amministrativi – legge di semplificazione 1999). In via subordinata, la violazione dell'art. 3 della Costituzione, sotto il profilo del difetto di ragionevolezza, avendo il legislatore ingiustificatamente attribuito – per la prima volta nell'ordinamento – ad un giudice monocratico, dotato di bagaglio culturale ed esperienza professionale inferiore alla terna che compone il collegio, la potestà di sindacare provvedimenti di un organo collegiale.  
    2.– La questione è infondata, con riferimento ad entrambi i profili di censura prospettati. 
    2.1.– L'opposizione avverso il provvedimento di rigetto dell'istanza di ammissione al patrocinio a spese dello Stato, ovvero di revoca del decreto di ammissione – equiparato al primo per costante giurisprudenza di legittimità – era disciplinata dall'art. 6, commi 4 e 5, della legge n. 30 luglio 1990, n.217 (Istituzione del patrocinio a spese dello Stato per i non abbienti), come modificati dalla legge 29 marzo 2001, n. 134 (Modifica della legge 30 luglio 1990, n. 217, recante istituzione del patrocinio a spese dello Stato per i non abbienti). Si prevedeva il ricorso al tribunale o alla corte d'appello ai quali apparteneva il giudice, ovvero al tribunale nel cui circondario aveva sede il giudice per le indagini preliminari presso la pretura o il pretore che aveva emesso il decreto. Si rinviava all'art. 29 della legge 15 giugno 1942, n. 794 (Onorari di avvocato e di procuratore per prestazioni giudiziali in materia civile), che disciplina la procedura speciale in camera di consiglio per la liquidazione degli onorari agli avvocati. Si prevedeva espressamente la ricorribilità per cassazione dell'ordinanza di decisione del ricorso. 
    Il giudizio di opposizione si svolgeva, quindi, in camera di consiglio, secondo la procedura per la liquidazione degli onorari agli avvocati, dinanzi ad un giudice in composizione collegiale.  
    La norma impugnata – che fa parte del testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia, emanato sulla base della delega conferita al Governo dall'art. 7 della legge n. 50 del 1999 – pur conservando il rinvio al procedimento speciale previsto per gli onorari di avvocato, anche se nella forma indiretta, prevede che l'ufficio giudiziario proceda in composizione monocratica. Secondo quanto risulta dalla relazione governativa, il legislatore delegato ha introdotto la composizione monocratica in luogo di quella collegiale al fine di adeguare la disciplina del processo in questione alla riforma, operata dal decreto legislativo 19 febbraio 1998, n. 51 (Norme in materia di istituzione del giudice unico di primo grado), in base alla quale il giudice monocratico è la regola, mentre quello collegiale costituisce un'eccezione. Adeguamento che, sempre secondo l'intenzione del legislatore delegato, appariva idoneo ad evitare che una procedura semplificata in origine, nel contesto in cui la regola generale era la composizione collegiale, andasse successivamente nella direzione opposta a quella seguita dal legislatore della riforma. 
    Ad avviso del remittente sarebbe violato l'art. 76 della Costituzione, non rinvenendosi tra i criteri direttivi della legge di delega la previsione della facoltà di modificare la distribuzione di compiti tra giudice monocratico e collegiale, né la volontà di armonizzazione della materia con la sopravvenuta riforma del giudice unico. 
    La censura è priva di fondamento. Tra i criteri direttivi individuati nella delega assume rilievo quello previsto dalla lettera d), comma 2, dell' art. 7 cit.: «coordinamento formale del testo delle disposizioni vigenti apportando, nei limiti di detto coordinamento, le modifiche necessarie per garantire la coerenza logica e sistematica della normativa anche al fine di adeguare e semplificare il linguaggio normativo». 
    Se l'obiettivo è quello della coerenza logica e sistematica della normativa, il coordinamento non può essere solo formale, come non ha mancato di sottolineare il Consiglio di Stato nel parere espresso nel corso della procedura di approvazione del testo unico. Inoltre, se l'obiettivo è quello di ricondurre a sistema una disciplina stratificata negli anni, con la conseguenza che i principî sono quelli già posti dal legislatore, non è necessario che – come vorrebbe il remittente – sia espressamente enunciato nella delega il principio già presente nell'ordinamento, essendo sufficiente il criterio del riordino di una materia delimitata. Entro questi limiti il testo unico poteva innovare per raggiungere la coerenza logica e sistematica e, come nel caso di specie, prevedere la composizione monocratica, anziché collegiale del giudice, applicando al processo in questione il principio generale affermato con la riforma del 1998, al fine di rendere la disciplina più coerente nel suo complesso e in sintonia con l'evolversi dell'ordinamento.  
    Né a diversa conclusione può indurre l'art. 50-bis cod. proc. civ. (inserito dall'art. 56 del d. lgs. n. 51 del 1998), il quale, nell'elencare in via di eccezione, rispetto al successivo art. 50-ter, le cause in cui il tribunale decide in composizione collegiale, richiama (secondo comma) i procedimenti in camera di consiglio disciplinati dagli articoli 737 e seguenti del codice di rito, salvo che sia altrimenti disposto. Infatti, il procedimento camerale disciplinato dall'art. 29 della legge n. 794 del 1942, al quale rinvia la norma impugnata, non rientra tra quelli di cui agli articoli 737 e seguenti del codice. A tal fine è sufficiente considerare che il provvedimento non è impugnabile, mentre l'art. 739 cod. proc. civ. prevede espressamente il reclamo.  
    2.2.– Parimenti infondata è la censura relativa alla violazione dell'art. 3 della Costituzione, sotto il profilo del difetto di ragionevolezza, per essere stata ingiustificatamente attribuita la potestà di sindacare provvedimenti di un organo collegiale ad un giudice monocratico, che sarebbe, secondo il remittente, dotato di bagaglio culturale ed esperienza professionale inferiore alla terna che compone il collegio. 
    È sufficiente osservare che il provvedimento sul quale si pronuncia il giudice dell'opposizione è un provvedimento amministrativo, anche se adottato da un organo giudiziario, con la conseguenza, da un lato, della non pertinenza degli esempi invocati a confronto dal giudice remittente in quanto relativi ad ipotesi in cui il provvedimento impugnato è di natura giurisdizionale, dall'altro, che nessuna irragionevolezza è ravvisabile nella scelta del legislatore di affidare la cognizione di un provvedimento amministrativo ad un giudice monocratico.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara non fondata la questione di legittimità costituzionale dell'art. 99, comma 3, del decreto legislativo 30 maggio 2002, n. 113 (Testo unico delle disposizioni legislative in materia di spese di giustizia), sollevata, in riferimento agli articoli 3 e 76 della Costituzione, dal giudice del Tribunale di Gela designato dal Presidente dello stesso tribunale, con l'ordinanza in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 gennaio 2005. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Alfio FINOCCHIARO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 28 gennaio 2005. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
