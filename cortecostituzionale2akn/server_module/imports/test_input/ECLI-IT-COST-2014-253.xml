<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/253/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/253/"/>
          <FRBRalias value="ECLI:IT:COST:2014:253" name="ECLI"/>
          <FRBRdate date="03/11/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="253"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/253/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/253/ita@/!main"/>
          <FRBRdate date="03/11/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2014/253/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2014/253/ita@.xml"/>
          <FRBRdate date="03/11/2014" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="07/11/2014" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2014</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>NAPOLITANO</cc:presidente>
        <cc:relatore_pronuncia>Alessandro Criscuolo</cc:relatore_pronuncia>
        <cc:data_decisione>03/11/2014</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Paolo Maria NAPOLITANO; Giudici : Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 2, comma 11, della legge 30 dicembre 2010, n. 240 (Norme in materia di organizzazione delle università, di personale accademico e reclutamento, nonché delega al Governo per incentivare la qualità e l'efficienza del sistema universitario), promosso dal Tribunale amministrativo regionale per l'Umbria nel procedimento vertente tra V.M. e l'Università degli studi di Perugia ed altri, con ordinanza del 6 agosto 2013, iscritta al n. 246 del registro ordinanze 2013 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 47, prima serie speciale, dell'anno 2013.
 Visti l'atto di costituzione di V.M., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 7 ottobre 2014 il Giudice relatore Alessandro Criscuolo;
 uditi l'avvocato Giuseppe Morbidelli per V.M. e l'avvocato dello Stato Gabriella Palmieri per il Presidente del Consiglio dei ministri. 
 Ritenuto che il Tribunale amministrativo regionale per l'Umbria (d'ora in avanti TAR), con ordinanza del 6 agosto 2013 (r.o. n. 246 del 2013), ha sollevato, in riferimento agli artt. 3, 33, sesto comma, e 97 della Costituzione, questione di legittimità costituzionale dell'art. 2, comma 11, della legge 30 dicembre 2010, n. 240 (Norme in materia di organizzazione delle università, di personale accademico e reclutamento, nonché delega al Governo per incentivare la qualità e l'efficienza del sistema universitario), «nella parte in cui riserva l'elettorato passivo alle cariche accademiche ai docenti in grado di assicurare un periodo di servizio almeno pari alla durata del mandato prima della data di collocamento a riposo, senza comprendere il biennio di permanenza in servizio successivo al raggiungimento dell'età pensionabile»;
 che il TAR è chiamato a decidere sul ricorso avverso i provvedimenti con cui l'Università degli Studi di Perugia non ha ammesso la candidatura del prof. V.M., ordinario di diritto costituzionale presso la facoltà di giurisprudenza dello stesso ateneo, alle elezioni per il conferimento della carica di Rettore per il sessennio accademico 2013/2014-2018/2019;
 che, secondo quanto riferisce il giudice rimettente, la Commissione centrale elettorale dell'Università di Perugia, premesso che la data prevista per il collocamento a riposo del prof. V.M. precede di circa un anno quella della scadenza del sessennio accademico, ha ritenuto la candidatura inammissibile per difetto del requisito previsto dall'art. 2, comma 11, della legge n. 240 del 2010, richiamato dagli artt. 54, comma 2, dello Statuto dell'Università di Perugia; 6, comma 4, del regolamento generale di ateneo e 4, comma 1, del decreto 12 aprile 2013 di indizione delle elezioni a Rettore, secondo cui «l'elettorato passivo per le cariche accademiche è riservato ai docenti che assicurano un numero di anni di servizio almeno pari alla durata del mandato prima della data di collocamento a riposo»;
 che il ricorrente nel giudizio principale ha sostenuto che il calcolo degli anni, che il docente può assicurare ai fini di concorrere alla carica di Rettore, va effettuato tenendo conto anche del biennio di trattenimento in servizio oltre i limiti di età per il collocamento a riposo, poiché l'art. 2, comma 11, della legge n. 240 del 2010 deve essere interpretato alla luce della sentenza della Corte costituzionale n. 83 del 2013, che ha dichiarato l'illegittimità costituzionale dell'art. 25 della legge n. 240 del 2010, nella parte in cui esclude i professori e ricercatori universitari dalla facoltà, prevista per i dipendenti civili dello Stato dall'art. 16 del decreto legislativo 30 dicembre 1992, n. 503 (Norme per il riordinamento del sistema previdenziale dei lavoratori privati e pubblici, a norma dell'articolo 3 della legge 23 ottobre 1992 n. 421), di essere trattenuti in servizio per un periodo massimo di un biennio oltre i limiti di età per il collocamento a riposo;
 che, secondo il giudice a quo, la dichiarazione di illegittimità costituzionale dell'art. 25 della legge n. 240 del 2010 non consente di riconoscere al prof. V.M. l'elettorato passivo per la candidatura alla carica di Rettore dell'Università di Perugia in quanto, come evidenziato dalla stessa Corte costituzionale con la citata sentenza n. 83 del 2013, l'accoglimento dell'istanza di trattenimento in servizio non è automatico, ma consegue ad una valutazione discrezionale dell'amministrazione;
 che, tuttavia, a parere del TAR: a) la facoltà dell'amministrazione di accogliere o meno la richiesta del dipendente non può «comprimere il diritto dei professori e ricercatori di concorrere alle cariche rappresentative all'interno dell'università, specie se, come nel caso del prof. V., non sia stata ancora raggiunta l'età per presentare la disponibilità al trattenimento, stabilita dai ventiquattro ai dodici mesi precedenti il collocamento a riposo»; b) il conseguimento della carica di Rettore «integra di per sé» la ricorrenza dei presupposti che l'amministrazione deve valutare per accogliere l'istanza di trattenimento in servizio, posto che, ai sensi dell'art. 10 dello statuto, il Rettore esercita funzioni d'indirizzo, di iniziativa e di coordinamento delle attività scientifica e didattiche e di responsabile del perseguimento delle finalità dell'ateneo, secondo criteri di qualità e nel rispetto dei principi di efficacia, efficienza, trasparenza e promozione del merito; c) «la posizione di mero interesse tutelato alla permanenza in servizio dei professori e ricercatori, assoggettata alle esigenze dell'università si pone in aperto contrasto con la posizione di titolare dell'elettorato passivo alla carica di Rettore, di vero e proprio diritto non suscettibile di essere compresso dalla potestà esclusiva dell'amministrazione di stabilire se trattenere o no in servizio il dipendente»; d) «l'aspettativa giuridicamente consolidata alla permanenza in servizio per il biennio successivo al raggiungimento dei limiti di età per il collocamento a riposo, comporta che degli stessi anni di servizio debba essere tenuto conto per determinare il numero di anni, almeno pari alla durata del mandato prima della data di collocamento a riposo, da assicurare, da docenti e ricercatori, per l'elettorato passivo alle cariche accademiche ai sensi dell'art. 2, comma 11, della legge n. 240 del 2010»;
 che l'applicazione testuale della norma, «che nulla dispone sul diritto a concorrere alla carica di Rettore per i docenti la cui durata del mandato ecceda la data di collocamento a riposo e sia condizionata alla permanenza in servizio per l'ulteriore biennio» sarebbe «non solo irragionevole rispetto all'annullamento, per i professori e ricercatori, del divieto di permanenza in servizio (art. 3 cost.) ma anche lesiva dell'autonomia delle università (art. 33, co. 6, cost.) perché non permette di devolvere al corpo elettorale la candidatura di una parte degli aventi diritto che sono in grado di assolvere ai compiti connessi alla carica di Rettore, ad onta del principio di buon andamento dell'azione amministrativa (art. 97 Cost.)»; 
 che, con atto del 9 dicembre 2013, depositato presso la cancelleria della Corte costituzionale il 10 dicembre 2013 è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che «la questione di costituzionalità proposta sia dichiarata inammissibile e comunque infondata»;
 che, con comparsa del 9 dicembre 2013, depositata presso la cancelleria della Corte costituzionale il 10 dicembre 2013, si è costituito il prof. V.M., riferendo di essere stato riammesso con riserva alla competizione elettorale in forza dell'ordinanza cautelare del 6 agosto 2013, n. 111, con cui il TAR aveva sospeso l'atto di esclusione, senza tuttavia risultare eletto, e chiedendo, pertanto, alla Corte di «pronunciare in conseguenza della sopravvenuta carenza di interesse [...] alla definizione del giudizio a quo»;
 che, con nota del 10 dicembre 2013, pervenuta a mezzo posta presso la cancelleria della Corte costituzionale il 19 dicembre 2013, il TAR ha trasmesso copia della lettera del 15 ottobre 2013 con cui il prof. V. ha comunicato al Decano dell'Università degli Studi di Perugia il ritiro ufficiale della propria candidatura alla carica di Rettore, nonché copia della missiva del 18 novembre 2013 con cui la stessa Università ha informato l'Avvocatura distrettuale dello Stato del ritiro della candidatura da parte del docente esprimendo, altresì, l'opinione secondo cui il ricorso dovrebbe «essere dichiarato improcedibile per sopravvenuta carenza di interesse, con tutte le conseguenze del caso anche in ordine alla disposta sospensione del giudizio di merito in attesa della decisione della questione di legittimità costituzionale».
 Considerato che il Tribunale amministrativo regionale per l'Umbria, con ordinanza del 6 agosto 2013, (r.o. n. 246 del 2013), ha sollevato, in riferimento agli artt. 3, 33, sesto comma, e 97 della Costituzione, questione di legittimità costituzionale dell'art. 2, comma 11, della legge 30 dicembre 2010, n. 240 (Norme in materia di organizzazione delle università, di personale accademico e reclutamento, nonché delega al Governo per incentivare la qualità e l'efficienza del sistema universitario), «nella parte in cui riserva l'elettorato passivo alle cariche accademiche ai docenti in grado di assicurare un periodo di servizio almeno pari alla durata del mandato prima della data di collocamento a riposo, senza comprendere il biennio di permanenza in servizio successivo al raggiungimento dell'età pensionabile»;
 che il giudizio di legittimità costituzionale, una volta iniziato in seguito ad ordinanza di rinvio del giudice rimettente, non è suscettibile di essere influenzato da successive vicende di fatto concernenti il rapporto dedotto nel processo che lo ha occasionato, come previsto dall'art. 18 delle norme integrative per i giudizi davanti alla Corte costituzionale, nel testo approvato il 7 ottobre 2008 (sentenze n. 162 del 2014, n. 120 del 2013, n. 274 del 2011 e n. 227 del 2010);
 che, successivamente all'ordinanza di rimessione, è intervenuto il decreto-legge 24 giugno 2014, n. 90 (Misure urgenti per la semplificazione e la trasparenza amministrativa e per l'efficienza degli uffici giudiziari), convertito, con modificazioni, dall'art. 1, comma 1, della legge 11 agosto 2014, n. 114;
 che l'art. 1, comma 1, del d.l. n. 90 del 2014, ha abrogato l'art. 16 del decreto legislativo 30 dicembre 1992, n. 503 (Norme per il riordinamento del sistema previdenziale dei lavoratori privati e pubblici, a norma dell'articolo 3 della legge 23 ottobre 1992 n. 421);
 che, a fronte di questo ius superveniens, spetta al giudice rimettente la valutazione circa la perdurante rilevanza della questione sollevata;
 che va disposta, pertanto, la restituzione degli atti al giudice a quo, per una nuova valutazione della rilevanza della questione, alla luce del mutato quadro normativo (ex multis ordinanze n. 152 e n. 75 del 2014, n. 35 del 2013, n. 316 del 2012 e n. 296 del 2011).
 Visto l'art. 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 ordina la restituzione degli atti al Tribunale amministrativo regionale per l'Umbria.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 novembre 2014.&#13;
 F.to:&#13;
 Paolo Maria NAPOLITANO, Presidente&#13;
 Alessandro CRISCUOLO, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 7 novembre 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
