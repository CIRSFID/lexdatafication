<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/117/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/117/"/>
          <FRBRalias value="ECLI:IT:COST:2003:117" name="ECLI"/>
          <FRBRdate date="26/03/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="117"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/117/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/117/ita@/!main"/>
          <FRBRdate date="26/03/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2003/117/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2003/117/ita@.xml"/>
          <FRBRdate date="26/03/2003" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="10/04/2003" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2003</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>CHIEPPA</cc:presidente>
        <cc:relatore_pronuncia>Fernanda Contri</cc:relatore_pronuncia>
        <cc:data_decisione>26/03/2003</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 269, secondo comma, del codice di procedura civile, promossi con ordinanze del 14 maggio 2002 dal Tribunale di Sassari nel procedimento civile vertente tra la Curatela fall.to Dis. Com Al s.r.l. e Deligios Vincenzo ed altri e del 16 maggio 2002 dal Tribunale di Grosseto nel procedimento civile vertente tra Fiorenzoni Silvio e Spinelli Roberto ed altri, iscritte ai nn. 353 e 405 del registro ordinanze 2002 e pubblicate nella Gazzetta Ufficiale  della Repubblica nn. 34 e 37, prima serie speciale, dell'anno 2002. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 9 gennaio 2003 il Giudice relatore Fernanda Contri. 
    Ritenuto che il Tribunale di Sassari, con ordinanza emessa il 14 maggio 2002, ha sollevato, in riferimento agli artt. 3 e 24 della Costituzione, questione di legittimità costituzionale dell'art. 269, secondo comma, del codice di procedura civile, nella parte in cui non prevede la fissazione di un termine perentorio per la notifica della citazione al terzo chiamato in causa dal convenuto; 
    che il rimettente espone in fatto che i convenuti, dopo aver ottenuto lo spostamento della prima udienza per chiamare terzi in causa, hanno omesso di notificare l'atto di chiamata a una delle tre parti cui volevano estendere il contraddittorio ed hanno perciò chiesto la concessione di un nuovo termine per notificare al terzo la citazione; 
    che il giudice a quo, dopo aver sottolineato come l'art. 269, secondo comma, cod. proc. civ. non contenga alcuna previsione circa le conseguenze derivanti dalla predetta omissione, dichiara di aderire alla tesi che consente al convenuto la reiterazione della propria istanza, in quanto, in assenza di una espressa previsione legislativa, ex art. 152 cod. proc. civ., sarebbe possibile solo la fissazione di termini di natura ordinatoria, ulteriormente prorogabili ai sensi dell'art. 154, non già di natura perentoria; 
    che tale conclusione, ad avviso del rimettente, darebbe luogo tuttavia a dubbi di legittimità costituzionale, per contrasto con gli artt. 3 e 24 della Costituzione, poiché vi sarebbe una ingiustificata disparità di trattamento tra attore e convenuto in relazione alla chiamata in causa, essendo soltanto il primo tenuto al rispetto di un termine perentorio fissato dal giudice, mentre il secondo potrebbe reiterare indefinitamente la richiesta di differimento dell'udienza, qualora il giudice non abbia fissato alcun termine; 
    che analoga questione di legittimità costituzionale dell'art. 269, secondo comma, del codice di procedura civile, è stata sollevata, in riferimento agli artt. 3, 24 e 111 della Costituzione, dal Tribunale di Grosseto, con ordinanza in data 16 maggio 2002; 
    che anche nel giudizio pendente innanzi al detto Tribunale, come riferisce il giudice a quo, i convenuti hanno omesso di notificare l'atto di chiamata in causa del terzo, chiedendo all'udienza di prima comparizione la concessione di un nuovo termine per la citazione del terzo; 
    che il giudice rimettente, dopo aver sottolineato come non sia possibile sanzionare l'inerzia dei convenuti con la decadenza, in assenza di una esplicita previsione normativa, e come non possa nemmeno ritenersi applicabile l'istituto della rimessione in termini, per difetto del presupposto della non imputabilità della causa che ha determinato la decadenza, afferma che la questione non può essere risolta in base ad una interpretazione adeguatrice, ostandovi sia il sistema processuale, che richiede una espressa previsione di legge per conferire natura perentoria ad un termine, sia la ricostruzione della ratio legis attraverso i lavori preparatori, dai quali si rileva come la posizione delle parti riguardo al termine per la chiamata in causa fosse inizialmente coincidente e sia stata poi differenziata (senza alcuna motivazione espressa) nel corso della discussione alla Camera; 
    che, ad avviso del giudice rimettente, la disposizione denunciata si porrebbe anzitutto in contrasto con il principio di eguaglianza, configurando una disparità di trattamento di situazioni sostanzialmente identiche; 
    che sussisterebbe inoltre un contrasto con gli artt. 24  e 111 della Costituzione, in quanto la reiterabilità da parte del convenuto dell'istanza di concessione di un ulteriore termine per chiamare terzi in causa, in assenza di valutazioni di merito da parte del giudice, potrebbe esporre l'attore al pregiudizio derivante da iniziative dilatorie, in violazione sia del diritto di difesa dell'attore, sia del principio di parità delle parti, sia, infine, del principio di ragionevole durata del processo; 
    che nel giudizio promosso dal Tribunale di Grosseto è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata infondata; 
    che, ad avviso della difesa erariale, la questione potrebbe essere superata in via interpretativa sulla base del disposto testuale e comunque sulla base del generale obbligo di comportamento secondo lealtà e probità processuale, ex art. 88 cod. proc. civ.; 
    che, inoltre, la formulazione della norma impugnata, la quale comunque obbliga al rispetto del termine previsto dall'art. 163-bis, non sembrerebbe consentire la possibilità di avanzare una nuova richiesta di chiamata del terzo al di fuori della comparsa, né la possibilità di ulteriori spostamenti della prima udienza. 
    Considerato che i Tribunali di Sassari e di Grosseto censurano, per ragioni sostanzialmente analoghe, l'art. 269, secondo comma, del codice di procedura civile, nella parte in cui non prevede la fissazione di un termine perentorio per la notifica della citazione al terzo chiamato in causa dal convenuto;  
    che entrambi i rimettenti sostengono che la citata norma si porrebbe in contrasto con gli artt. 3 e 24 della Costituzione, per la ingiustificata disparità di trattamento tra l'attore e il convenuto in relazione alla chiamata in causa, essendo soltanto il primo tenuto al rispetto di un termine perentorio fissato dal giudice; 
    che la norma contrasterebbe anche con gli artt. 24 e 111 della Costituzione, in quanto la reiterabilità da parte del convenuto dell'istanza di concessione di ulteriori termini per chiamare terzi in causa, in assenza di valutazioni di merito da parte del giudice, esporrebbe l'attore al pregiudizio derivante da iniziative dilatorie, in violazione sia del diritto di difesa dell'attore, sia del principio di parità delle parti, sia, infine, del principio di ragionevole durata del processo; 
    che le questioni sono manifestamente inammissibili per difetto di rilevanza, per i seguenti motivi:  
    in entrambi i giudizi a quibus, come risulta dalle rispettive ordinanze di rimessione, la parte convenuta, avendo chiesto lo spostamento della prima udienza per provvedere alla chiamata del terzo ma  avendo omesso di notificargli la citazione, ha formulato alla prima udienza di comparizione istanza di concessione di un nuovo termine per procedere al detto adempimento; 
    in entrambi i casi la richiesta di concessione di un nuovo termine o, più esattamente, di fissazione di una nuova udienza per la citazione del terzo è stata formulata tardivamente, essendo in quel momento già decorso il termine che avrebbe consentito la citazione nel rispetto di quello dilatorio ai sensi dell'art. 163-bis cod. proc. civ., fissato dall'art. 269 cod. proc. civ.;  
    il rispetto del termine a comparire di cui all'art. 163-bis cit. comporta quindi la necessità di eseguire la notifica entro il sessantunesimo giorno anteriore all'udienza di comparizione spostata dal giudice su istanza del convenuto, con la conseguenza che solo prima di tale momento potrebbe utilmente formularsi richiesta di proroga, ai sensi dell'art. 154 cod. proc. civ.; 
    secondo l'orientamento dominante della Corte di cassazione, la proroga di un termine ordinatorio può consentirsi solo se richiesta al giudice prima della sua scadenza; 
    tutto ciò premesso, poiché nei giudizi a quibus il termine per proporre l'istanza di proroga era già scaduto ed i convenuti erano pertanto decaduti dal relativo potere, la questione di costituzionalità concernente la natura, ordinatoria o perentoria, del termine di cui all'art. 269, secondo comma, cod. proc. civ. è del tutto priva di rilevanza e pertanto manifestamente inammissibile.  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 269, secondo comma, del codice di procedura civile, sollevate, in riferimento agli artt. 3 e 24 della Costituzione, dal Tribunale di Sassari e, in riferimento agli artt. 3, 24 e 111 della Costituzione, dal Tribunale di Grosseto, con le ordinanze in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 26 marzo 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Fernanda CONTRI, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 10 aprile 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
