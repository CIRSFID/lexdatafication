<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/110/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/110/"/>
          <FRBRalias value="ECLI:IT:COST:2006:110" name="ECLI"/>
          <FRBRdate date="08/03/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="110"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/110/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/110/ita@/!main"/>
          <FRBRdate date="08/03/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/110/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/110/ita@.xml"/>
          <FRBRdate date="08/03/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="17/03/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>MARINI</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maddalena</cc:relatore_pronuncia>
        <cc:data_decisione>08/03/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai Signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nei giudizi di legittimità costituzionale dell'art. 13, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), come modificato dal decreto-legge 14 settembre 2004, n. 241 (Disposizioni urgenti in materia di immigrazione), convertito, con modificazioni, nella legge 12 novembre 2004, n. 271, promossi dal Giudice di pace di Crotone nei procedimenti relativi a Bilokon Yuliya e Bardziuchenk Tatsiana con due ordinanze del 6 novembre 2004, iscritte ai numeri 345 e 346 del registro ordinanze 2005 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 28 , prima serie speciale, dell'anno 2005. 
    Udito nella camera di consiglio dell'11 gennaio 2006 il Giudice relatore Paolo Maddalena. 
    Ritenuto che, con due ordinanze similmente motivate, emesse entrambe il 6 novembre 2004 (pervenute alla Corte il 28 giugno 2005 e iscritte ai numeri 345 e 346 del registro ordinanze dell'anno 2005), il Giudice di pace di Crotone ha sollevato questione di legittimità costituzionale dell'art. 13, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), «come modificato dalla legge n. 241 del 2004» (recte: dal decreto-legge 14 settembre 2004, n. 241, recante “Disposizioni urgenti in materia di immigrazione”, convertito, con modificazioni, nella legge 12 novembre 2004, n. 271), denunciandone il contrasto con l'art. 3 della Costituzione, «nella parte in cui prevede che sia il giudice di pace a disporre la convalida del provvedimento del questore di allontanamento dal territorio nazionale»; 
      che, nel premettere che «i diritti fondamentali della persona (fra i quali, in primis, il diritto alla libertà personale) spettano in via di principio anche agli stranieri» e che è lo stesso art. 2 del decreto legislativo n. 286 del 1998 a prevedere la «parità di trattamento» tra cittadino e straniero «relativamente alla tutela giurisdizionale dei diritti e degli interessi legittimi con la pubblica amministrazione», il giudice a quo assume che la disciplina della convalida, di cui alla denunciata disposizione, sarebbe «in contrasto con il principio di uguaglianza, in quanto postula una disparità di trattamento tra cittadini e stranieri nel godimento di un fondamentale diritto di libertà, atteso che per i primi vige la competenza del tribunale, mentre per i secondi il provvedimento restrittivo viene ad essere adottato dal giudice di pace, che per definizione non ha competenza in materia di misure cautelari personali»; 
      che, a tal riguardo, il rimettente sostiene che «la legge attributiva delle funzioni penali al giudice di pace non prevede l'assegnazione ad esso del potere di infliggere le pene incidenti sulla libertà personale, quale la pena detentiva (con cui viene sostanzialmente a coincidere il trattenimento presso il centro di permanenza temporanea ed assistenza), né di conoscere dei reati implicanti un maggiore disvalore giuridico e sociale e puniti con pene più severe»; 
      che, infine, il giudice a quo adduce la rilevanza ai fini del presente procedimento e la non manifesta infondatezza della questione di legittimità costituzionale del denunciato art. 13, comma 5-bis, del decreto legislativo n. 286 del 1998, provvedendo a sospendere «il giudizio di convalida sino all'esito del giudizio incidentale di legittimità». 
    Considerato che il Giudice di pace di Crotone, con due ordinanze di identico contenuto, censura l'art. 13, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), come da ultimo modificato dal decreto-legge 14 settembre 2004, n. 241 (Disposizioni urgenti in materia di immigrazione), convertito, con modificazioni, nella legge 12 novembre 2004, n. 271, sostenendo che esso violerebbe l'art. 3 della Costituzione, «nella parte in cui prevede che sia il giudice di pace a disporre la convalida del provvedimento del questore di allontanamento dal territorio nazionale»; 
      che, in ragione dell'identità delle questioni prospettate dal medesimo rimettente, i relativi giudizi vanno riuniti per essere decisi congiuntamente; 
      che il giudice a quo, in entrambe le ordinanze di rimessione, non fornisce alcuna indicazione in ordine alla fattispecie concreta sulla quale è chiamato a pronunciarsi, affermando apoditticamente la rilevanza delle questioni proposte; 
      che, peraltro, nel motivare sulla non manifesta infondatezza del sollevato dubbio di costituzionalità, il rimettente fa riferimento al «trattenimento presso il centro di permanenza temporanea ed assistenza» e, quindi, secondo quanto previsto dall'art. 14 del decreto legislativo n. 286 del 1998, al provvedimento adottabile dall'autorità di pubblica sicurezza nei confronti dello straniero nel caso in cui non sia «possibile eseguire con immediatezza l'espulsione mediante accompagnamento alla frontiera ovvero il respingimento» (comma 1) e che deve essere sottoposto a convalida da parte del giudice di pace territorialmente competente, secondo la disciplina recata dai commi da 3 a 5 dello stesso art. 14; 
      che, dunque, proprio l'assoluta carenza nella descrizione delle fattispecie relative ai giudizi a quibus non consente in alcun modo di valutare la rilevanza delle questioni sollevate avverso l'art. 13, comma 5-bis, del d.lgs. n. 286 del 1998, recante la disciplina concernente la convalida del provvedimento di accompagnamento coattivo alla frontiera, giacché non è dato comprendere su quale dei due differenti provvedimenti adottabili dall'autorità di pubblica sicurezza il rimettente è chiamato a pronunciarsi e, di conseguenza, quali siano le disposizioni che lo stesso giudice a quo è tenuto effettivamente ad applicare;  
      che, pertanto, le questioni devono essere dichiarate manifestamente inammissibili (ex plurimis, ordinanze n. 396 e n. 297 del 2005).  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 13, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), come modificato dal decreto-legge 14 settembre 2004, n. 241 (Disposizioni urgenti in materia di immigrazione), convertito, con modificazioni, nella legge 12 novembre 2004, n. 271, sollevate, in riferimento all'art. 3 della Costituzione, dal Giudice di pace di Crotone con le ordinanze indicate in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  l'8 marzo 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Paolo MADDALENA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 17 marzo 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
