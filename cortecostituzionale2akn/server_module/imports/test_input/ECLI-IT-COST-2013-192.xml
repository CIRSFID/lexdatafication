<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/192/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/192/"/>
          <FRBRalias value="ECLI:IT:COST:2013:192" name="ECLI"/>
          <FRBRdate date="03/07/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="192"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/192/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/192/ita@/!main"/>
          <FRBRdate date="03/07/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/192/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/192/ita@.xml"/>
          <FRBRdate date="03/07/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="12/07/2013" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2013</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>GALLO</cc:presidente>
        <cc:relatore_pronuncia>Paolo Maria Napolitano</cc:relatore_pronuncia>
        <cc:data_decisione>03/07/2013</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Franco GALLO; Giudici : Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 13 (recte: comma 1) del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina del reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), promosso dal Tribunale ordinario di Ferrara nel procedimento penale a carico di B.L. con ordinanza del 20 settembre 2011, iscritta al n. 310 del registro ordinanze 2012 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 4, prima serie speciale, dell'anno 2013.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 3 luglio 2013 il Giudice relatore Paolo Maria Napolitano; 
 Ritenuto che, con ordinanza resa a verbale nel corso dell'udienza del 20 settembre 2011, il Tribunale ordinario di Ferrara, in composizione monocratica, ha sollevato, in riferimento agli articoli 3, 24 e 111 della Costituzione, questione di legittimità costituzionale dell'articolo 13 (recte: comma 1) del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina del reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205);
 che, riferisce il rimettente, la difesa dell'imputato ha eccepito, nel corso del dibattimento, l'illegittimità costituzionale della disposizione legislativa sopra indicata «nella parte in cui non prevede l'applicazione della speciale attenuante dell'estinzione del debito anche nell'ipotesi in cui l'imputato stia eseguendo l'estinzione mediante pagamento rateizzato del debito fiscale determinato dall'Agenzie delle Entrate»;
 che, conclude l'ordinanza di rimessione, pur avendo il pubblico ministero d'udienza espresso la sua opposizione all'accoglimento della eccezione di legittimità costituzionale, il Tribunale ordinario di Ferrara la ha, invece, accolta, disponendo la trasmissione degli atti a questa Corte e la sospensione del giudizio a quo;
 che, riferisce altresì il rimettente, solo a questo punto la difesa dell'imputato ha «deposita(to) istanza contenente l'enunciazione della questione di legittimità costituzionale»;
 che è intervenuto nel giudizio, rappresentato e difeso dall'Avvocatura generale dello Stato, il Presidente del Consiglio dei ministri, che ha concluso per l'inammissibilità della questione, per difetto di motivazione, e, comunque, per la sua infondatezza. 
 Considerato che il Tribunale ordinario di Ferrara, in composizione monocratica, ha sollevato, in riferimento agli articoli 3, 24 e 111 della Costituzione, questione di legittimità costituzionale dell'articolo 13 (recte: comma 1) del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina del reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205);
 che, a quanto è dato capire dalla scarna motivazione dell'ordinanza, la questione è sollevata con riferimento alla inapplicabilità della speciale circostanza attenuante prevista dalla norma censurata nella ipotesi in cui l'imputato, al momento della dichiarazione di apertura del dibattimento, ancora non abbia provveduto all'integrale pagamento dei tributi da lui dovuti in quanto la relativa somma, determinata dall'Agenzia delle Entrate, è stata oggetto di rateizzazione;
 che il rimettente, nel sollevare la questione di legittimità costituzionale, omette del tutto sia di descrivere la fattispecie concreta sottoposta al suo esame - trascurando, persino, di riferire quale sia l'imputazione contestata all'imputato nel giudizio a quo - sia di illustrare, al di là della mera enunciazione della proposizione normativa che si ritiene viziata, le ragioni in forza delle quali questa entrerebbe in conflitto con gli evocati parametri costituzionali;
 che siffatte manchevolezze impediscono in radice l'esame da parte di questa Corte sia della rilevanza della questione nel giudizio a quo che del contenuto delle doglianze del rimettente;
 che nessun rilievo ha, ai fini della ammissibilità della presente questione, l'avvenuto deposito - singolarmente eseguito dopo la dichiarazione di sospensione del procedimento penale a quo - di atti da parte della difesa dell'imputato volti ad "enunciare" la questione, atteso che il contenuto di tali atti non è stato in alcun modo fatto proprio dal rimettente, sicché, in ossequio al consolidato principio dell'autosufficienza della ordinanza di rimessione, essi non sono in alcun modo utilizzabili per integrare le evidenti lacune presenti nell'ordinanza stessa;
 che, pertanto, la questione va dichiarata manifestamente inammissibile.
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 13, comma 1, del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina del reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), sollevata, in riferimento agli articoli 3, 24 e 111 della Costituzione, dal Tribunale ordinario di Ferrara con l'ordinanza in epigrafe.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 luglio 2013.&#13;
 F.to:&#13;
 Franco GALLO, Presidente&#13;
 Paolo Maria NAPOLITANO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 12 luglio 2013.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
