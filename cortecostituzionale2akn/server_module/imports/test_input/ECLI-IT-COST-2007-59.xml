<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/59/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/59/"/>
          <FRBRalias value="ECLI:IT:COST:2007:59" name="ECLI"/>
          <FRBRdate date="19/02/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="59"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/59/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/59/ita@/!main"/>
          <FRBRdate date="19/02/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2007/59/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2007/59/ita@.xml"/>
          <FRBRdate date="19/02/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="02/03/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Sabino Cassese</cc:relatore_pronuncia>
        <cc:data_decisione>19/02/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 30 giugno 2004 (Doc. IV-ter, n. 5) relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal senatore Emiddio Novi, promosso con ricorso del Tribunale di Roma – sezione prima civile, notificato il 18 settembre 2006, depositato in cancelleria il 3 ottobre 2006 ed iscritto al n. 5 del registro conflitti tra poteri dello Stato 2006, fase di merito. 
    Visto l'atto di costituzione del Senato della Repubblica; 
    udito nell'udienza pubblica del 23 gennaio 2007 il Giudice relatore Sabino Cassese; 
    udito l'avvocato Stefano Grassi per il Senato della Repubblica.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fatto1. – Con ricorso dell'8 aprile 2005, il Tribunale di Roma ha promosso conflitto di attribuzione tra poteri dello Stato avverso la delibera adottata il 30 giugno 2004 (Doc. IV-ter, n. 5) con la quale il Senato della Repubblica ha statuito che le dichiarazioni oggetto del processo civile, instaurato da quarantuno magistrati con funzioni di sostituto procuratore della Repubblica presso il Tribunale di Napoli, nei confronti del senatore Emiddio Novi, concernono opinioni espresse da quest'ultimo nell'esercizio delle sue funzioni parlamentari, ai sensi dell'articolo 68, primo comma, della Costituzione. 
    1.1. – Il Tribunale ricorrente riferisce che nel giudizio pendente dinanzi a sé, gli attori hanno chiesto, tra l'altro, il risarcimento dei danni asseritamene subiti in conseguenza di un articolo, a firma del sen. Novi, pubblicato nel quotidiano «Roma» il 7 febbraio 2002, con il titolo «Il palazzo brucia e c'è chi pensa a spargere veleni», nel cui contesto lo stesso formulava accuse di inettitudine e opportunismo politico nei confronti della magistratura napoletana impegnata, secondo le affermazioni del senatore, a difendere i propri interessi corporativi e, in molti casi, a difendere il potere «di una sinistra affarista e prevaricatrice». Inoltre, la Procura di Napoli era definita come un «fortino della legalità», assediato da un disordine che vedeva protagonisti e responsabili magistrati intenti a invocare «la cacciata di Cordova». Denunciava, infine, l'ideazione di un sistema per il controllo e l'azzeramento volto a insabbiare e deviare le inchieste scomode sui rapporti tra sinistra imprenditrice e camorra, sviluppatosi in «un contesto di egemonia post-comunista». 
    Riferisce altresì il Tribunale che, costituitosi in giudizio, il convenuto senatore ha eccepito l'insindacabilità delle proprie dichiarazioni a norma dell'art. 68, primo comma, Cost.  
    Il Tribunale di Roma, ritenendo che le opinioni espresse dal sen. Novi nell'articolo da lui sottoscritto non risultano riferibili agli atti funzionali dallo stesso prodotti in giudizio, ha trasmesso gli atti al Senato della Repubblica, per la verifica della sussistenza della garanzia prevista dall'art. 68, primo comma, Cost.  
    Il Senato della Repubblica, con deliberazione dell'Assemblea, adottata nella seduta del 30 giugno 2004, ha approvato la proposta della Giunta delle elezioni e delle immunità parlamentari, volta a dichiarare che le affermazioni del sen. Novi, oggetto del giudizio, concernono opinioni espresse da un membro del parlamento nell'esercizio delle sue funzioni, a norma dell'art. 68, primo comma, Cost.  
    Secondo il Tribunale, le affermazioni contenute nell'articolo del sen. Novi, secondo cui i magistrati di Napoli erano arrivati a non incriminare ovvero ad assolvere non meglio specificati camorristi per evitare di dover indagare anche certi imprenditori legati ai partiti di sinistra, configurano «gravissime accuse alla magistratura napoletana sia inquirente che giudicante che non trovano alcun riscontro in nessuno dei passi di atti parlamentari che la Giunta delle elezioni e delle immunità parlamentari ha addotto a fondamento del proprio giudizio d'insindacabilità». 
    Pertanto, il Tribunale di Roma, ha proposto conflitto di attribuzione nei confronti del Senato della Repubblica e ha chiesto che questa Corte, previa delibazione di ammissibilità, annulli la deliberazione di insindacabilità adottata dall'Assemblea del Senato della Repubblica nella seduta del 30 giugno 2004 in relazione all'articolo a firma del sen. Novi intitolato «Il Palazzo brucia e c'è chi pensa a spargere veleni», pubblicato il 7 febbraio 2002 nel quotidiano «Roma», «quantomeno in riferimento all'opinione da questi espressa in ordine all'asserito mancato perseguimento penale da parte dei magistrati napoletani di alcuni camorristi asseritamente legati a non meglio precisati imprenditori vicini ai partiti di sinistra» e, conseguentemente, dichiari che non spetta al Senato della Repubblica deliberare che le suddette dichiarazioni concernono opinioni espresse da un membro del Parlamento nell'esercizio delle funzioni parlamentari, ai sensi dell'art. 68, primo comma, Cost.  
    2. – Il presente conflitto è stato dichiarato ammissibile con ordinanza n. 320 del 5 luglio 2006.  
    3. – Si è costituito in giudizio il Senato della Repubblica, eccependo l'inammissibilità e l'improcedibilità del ricorso e concludendo, comunque, per la sua infondatezza. 
    In una prima memoria, la difesa del Senato della Repubblica osserva che la dichiarazione di insindacabilità, proposta dalla Giunta delle elezioni e delle immunità parlamentari ed approvata dall'Assemblea, ha verificato la sostanziale corrispondenza tra le opinioni espresse a mezzo stampa e i numerosi interventi in atti formali svolti dal sen. Novi in Aula, nei quali lo stesso precisava le proprie opinioni critiche nei confronti dei magistrati della Procura di Napoli. Aggiunge che tale corrispondenza è stata valutata in modo ampio e dettagliato anche nel corso della discussione che ha preceduto il voto favorevole alla dichiarazione di insindacabilità da parte dell'Assemblea. Ad avviso della difesa, detta valutazione è stata riferita complessivamente all'articolo oggetto del conflitto e non può venir contestata con riferimento ad una parte specifica di esso.  
    La stessa difesa del Senato osserva che la valutazione compiuta dall'Assemblea ha avuto «chiara considerazione» dell'orientamento giurisprudenziale della Corte costituzionale in tema di «nesso funzionale» fra le opinioni espresse dal senatore fuori dal Parlamento e l'esercizio delle funzioni parlamentari; e che ulteriori atti parlamentari a firma del sen. Novi – alcuni anteriori ai fatti oggetto del conflitto – attestano l'antico interesse dello stesso alle tematiche della lotta alla criminalità in Campania e a quelle dell'amministrazione degli enti locali nella stessa Regione. 
    In prossimità della data fissata per l'udienza, il Senato della Repubblica ha depositato una seconda memoria, allegando ulteriori atti funzionali, a firma del sen. Novi, successivi alla pubblicazione dell'articolo di stampa.Considerato in diritto1. – Il Tribunale di Roma ha promosso conflitto di attribuzione tra poteri dello Stato avverso la delibera adottata il 30 giugno 2004 (Doc. IV-ter, n. 5), con la quale il Senato della Repubblica ha statuito che le dichiarazioni oggetto del processo civile, instaurato da quarantuno magistrati con funzioni di sostituto procuratore della Repubblica presso il Tribunale di Napoli nei confronti del senatore Emiddio Novi, concernono opinioni espresse da quest'ultimo nell'esercizio delle sue funzioni parlamentari, ai sensi dell'articolo 68, primo comma, della Costituzione. 
    Nel giudizio pendente dinanzi al Tribunale, gli attori hanno chiesto, tra l'altro, il risarcimento dei danni asseritamente subiti in conseguenza di un articolo, a firma del convenuto senatore, pubblicato nel quotidiano «Roma» il 7 febbraio 2002, con il titolo «Il palazzo brucia e c'è chi pensa a spargere veleni».  
    Secondo il Tribunale, le affermazioni contenute nell'articolo del sen. Novi – secondo cui i magistrati di Napoli erano arrivati a non incriminare ovvero ad assolvere non meglio specificati camorristi per evitare di dover indagare anche certi imprenditori legati ai partiti di sinistra – configurano «gravissime accuse alla magistratura napoletana sia inquirente che giudicante che non trovano alcun riscontro in nessuno dei passi di atti parlamentari che la Giunta delle elezioni e delle immunità parlamentari ha addotto a fondamento del proprio giudizio d'insindacabilità» e, «quantomeno in riferimento all'opinione […] espressa in ordine all'asserito mancato perseguimento penale da parte dei magistrati napoletani di alcuni camorristi asseritamente legati a non meglio precisati imprenditori vicini ai partiti di sinistra», non sono espressione di funzioni parlamentari e non sono, pertanto, insindacabili.  
    2. – In via preliminare, va confermata l'ammissibilità del conflitto, sussistendone i presupposti soggettivi e oggettivi, come già ritenuto da questa Corte con l'ordinanza n. 320 del 2006. 
    3. – Nel merito, il ricorso è infondato. 
    3.1 – Va ribadita la costante giurisprudenza di questa Corte, secondo cui, per la sussistenza del nesso funzionale tra le dichiarazioni rese da un parlamentare all'esterno e l'esercizio da parte sua di un'attività parlamentare, è necessario che ricorrano contemporaneamente due elementi: il legame temporale fra l'attività parlamentare e l'attività esterna, di modo che questa abbia una finalità divulgativa della prima (sentenze numeri 416, 347, 317 e 260 del 2006); la sostanziale corrispondenza di significato – ancorché non testuale – tra opinioni espresse nell'esercizio di funzioni parlamentari e atti esterni, non essendo sufficiente né una mera comunanza di argomenti o di contesto politico cui esse possano riferirsi (sentenze n. 221 del 2006 e n. 176 del 2005), né una generica omogeneità (sentenza n. 335 del 2006), né tantomeno una mera unità tematica (sentenza n. 164 del 2005). 
    3.2. – Occorre, dunque, verificare l'esistenza del legame temporale ed accertare se l'attività esterna sia caratterizzata da una sostanziale corrispondenza di significato con l'attività parlamentare. 
    Uno stretto legame temporale sussiste tra l'articolo di stampa del 7 febbraio 2002 e due interventi in Aula (del 5 e 21 dicembre 2001), tre interpellanze (n. 2-00104 del 19 dicembre 2001 e nn. 2-00122 e 2-00123 del 25 gennaio 2002) e una interrogazione (n. 4-01264 del 24 gennaio 2002) del parlamentare. 
    Vi è, inoltre, sostanziale corrispondenza di significato tra gli orientamenti manifestati in tali atti e il contenuto di detto articolo di stampa.  
    Con argomentazioni sostanzialmente coincidenti con quelle contenute nell'articolo a sua firma, relative alle accuse di inettitudine e di opportunismo politico rivolte ad alcuni settori della magistratura napoletana, il sen. Novi, nell'interpellanza n. 2-00123 del 25 gennaio 2002, nell'intervento in Aula del 21 dicembre 2001 e nell'interpellanza n. 2-00104 del 19 dicembre 2001, denunciava, da un lato, la grave situazione organizzativa in cui versava la Procura circondariale di Napoli, oppressa da un enorme arretrato dovuto alla lamentata inefficienza di alcuni settori della magistratura inquirente; dall'altro, «l'insofferenza» mostrata dai «sostituti dell'ex Procura circondariale ad adeguarsi alle regole» e strumentalizzata «dalle correnti di MD, dei Verdi e dei Ghibellini», al fine di determinare una condizione di incompatibilità ambientale nei confronti del procuratore capo Agostino Cordova. Inoltre, il parlamentare chiedeva al Ministro della giustizia se fosse a conoscenza di questo «autentico sfascio provocato in alcuni uffici giudiziari del Tribunale di Napoli da gestioni incompetenti e dall'insufficiente impegno dei magistrati che, per coprire precedenti e censurabili comportamenti professionali, ritengono di assicurarsi una sorta di protezione correntizia aderendo a quelle componenti che possono contare sul sostegno della maggioranza del CSM».  
    Sostanzialmente coincidenti con le esternazioni relative al denunciato «assedio» della Procura della Repubblica da parte della «sinistra giudiziaria» appaiono le dichiarazioni del parlamentare (intervento in Aula del 5 dicembre 2001 e interpellanza n. 2-00104 del 19 dicembre 2001), in cui egli riferiva che cinque giudici per le indagini preliminari di Napoli avevano stilato una circolare destinata ai colleghi nell'intento di dissuaderli dall'accogliere le istanze di intercettazione richieste dalla Procura nei confronti di un indagato, leader dei «No global». 
    Anche le affermazioni, secondo cui la magistratura partenopea avrebbe ideato un sistema di controllo e azzeramento delle inchieste, deviando e insabbiando le indagini sui rapporti tra sinistra imprenditrice e camorra, possono ritenersi esternazioni divulgative delle opinioni espresse in sede parlamentare. Infatti, nell'interpellanza n. 2-00122 del 25 gennaio 2002, veniva descritto il funzionamento dell'assegnazione dei fascicoli come condizionato dalla «sinistra», che intendeva tenere sotto controllo, mediante giudici per le indagini preliminari politicizzati, l'intera gestione delle indagini sui reati contro la pubblica amministrazione; e, nell'interpellanza n. 2-00104 del 19 dicembre 2001, si denunciava che le sezioni giurisdizionali competenti per i reati contro la pubblica amministrazione sarebbero state monopolizzate «da magistrati che militano nella corrente di magistratura democratica». 
    Infine, possono ritenersi esternazioni divulgative le critiche rivolte dal parlamentare alla magistratura napoletana, relative alla complessa vicenda – denunciata nella interrogazione a risposta scritta n. 4-01264 del 24 gennaio 2002 – concernente le rivelazioni fatte da un pentito in ordine ad affari conclusi tra una cosca camorristica e la giunta di sinistra nel Comune di Portici e alle non adeguate indagini condotte dalla Procura nazionale antimafia e dalla Direzione investigativa antimafia.  
    In conclusione, le dichiarazioni rese all'esterno dal sen. Novi sono riconducibili all'esercizio della funzione parlamentare e sono coperte dalla prerogativa costituzionale di insindacabilità.  
    Il Senato della Repubblica, ritenendo insindacabili le dichiarazioni rese alla stampa dal sen. Novi il 7 febbraio 2002, in quanto opinioni espresse nell'esercizio di funzioni parlamentari, non avendo ecceduto i limiti delle proprie attribuzioni costituzionali, non ha, quindi, leso le attribuzioni dell'autorità giudiziaria.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara che spettava al Senato della Repubblica deliberare che le dichiarazioni rese dal senatore Emiddio Novi, per le quali pende dinanzi al Tribunale di Roma il giudizio civile di cui al ricorso indicato in epigrafe, costituiscono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, ai sensi dell'art. 68, primo comma, della Costituzione. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 febbraio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Sabino CASSESE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 2 marzo 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
