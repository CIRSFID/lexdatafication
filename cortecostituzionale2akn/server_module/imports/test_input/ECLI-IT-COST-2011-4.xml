<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/4/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/4/"/>
          <FRBRalias value="ECLI:IT:COST:2011:4" name="ECLI"/>
          <FRBRdate date="16/12/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="4"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/4/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/4/ita@/!main"/>
          <FRBRdate date="16/12/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2011/4/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2011/4/ita@.xml"/>
          <FRBRdate date="16/12/2010" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="05/01/2011" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2011</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>DE SIERVO</cc:presidente>
        <cc:relatore_pronuncia>Alessandro Criscuolo</cc:relatore_pronuncia>
        <cc:data_decisione>16/12/2010</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli articoli 93, 96, 98, 107, 108, 143, 143-bis, 156-bis e 231 del codice civile, promosso dal Tribunale di Ferrara nel procedimento vertente tra Z. R. ed altra e il Sindaco del Comune di Ferrara, con ordinanza del 14 dicembre 2009 iscritta al n. 169 del registro ordinanze 2010 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 24, prima serie speciale, dell'anno 2010.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 1° dicembre 2010 il Giudice relatore Alessandro Criscuolo.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto che, con l'ordinanza indicata in epigrafe, il Tribunale di Ferrara ha sollevato questione di legittimità costituzionale degli articoli 93, 96, 98, 107, 108, 143, 143-bis, 156-bis e 231 del codice civile, nella parte in cui non consentono che le persone dello stesso sesso possano contrarre matrimonio, per contrasto con gli articoli 2, 3 e 29, primo comma, della Costituzione;
 che, come il Tribunale riferisce, l'ufficiale dello stato civile del Comune di Ferrara ha rifiutato, con provvedimento in data 25 marzo 2009, di procedere alla pubblicazione di matrimonio richiesta dalle parti private; 
 che, ad avviso del detto organo, l'ordinamento giuridico italiano non consente né disciplina il matrimonio tra persone dello stesso sesso, mentre, sulla base dell'art. 29 Cost., «la diversità di sesso è elemento essenziale nel nostro ordinamento per poter qualificare l'istituto del matrimonio»; 
 che in tal senso si è espresso anche il Ministero dell'interno con circolare del 26 marzo 2001, ritenendo che «non è trascrivibile il matrimonio celebrato all'estero tra omosessuali, di cui uno italiano, in quanto contrario alle norme di ordine pubblico»; 
 che le parti private, nel proporre reclamo avverso il detto provvedimento, hanno chiesto, in via principale, di ordinare all'ufficiale di stato civile del Comune di Ferrara di procedere alla pubblicazione del matrimonio e, in via subordinata, di sollevare la questione di legittimità costituzionale, previa positiva valutazione della rilevanza e non manifesta infondatezza, degli artt. 107, 108, 143, 143-bis e 156-bis cod. civ. in riferimento agli artt. 2, 3, 10, secondo comma, 13, 29 e 117 Cost., rimettendo gli atti alla Corte costituzionale;
 che, in punto di rilevanza, il giudice a quo ritiene che l'applicazione delle norme indicate sia ineliminabile nell'iter logico-giuridico necessario alla decisione: infatti, in caso di dichiarazione di fondatezza della questione così come sollevata, il rifiuto delle pubblicazioni, la cui richiesta dimostra inequivocabilmente la volontà di contrarre matrimonio delle parti ricorrenti, dovrebbe ritenersi illegittima, in assenza di altra causa di rifiuto, mentre, in caso di non accoglimento, l'attuale stato della normativa imporrebbe una pronuncia di rigetto del ricorso;
 che, per completezza, il rimettente rileva come, a fronte del rifiuto di pubblicazione da parte dell'ufficiale dello stato civile, essendo essa una formalità necessaria per poter procedere alla celebrazione del matrimonio, non sarebbe individuabile alcun altro procedimento nell'ambito del quale la questione possa essere valutata; 
 che, ad avviso del Tribunale, in mancanza di modifiche legislative in materia, il nostro attuale ordinamento non ammette il matrimonio tra persone dello stesso sesso ed è indiscutibile che tale istituto, anche senza una definizione espressa, si riferisce soltanto al matrimonio tra persone di sesso diverso, in quanto, benché il codice civile non indichi espressamente la differenza di genere fra i requisiti per contrarre matrimonio, in numerose norme, fra le quali quelle menzionate nel ricorso e sospettate d'illegittimità costituzionale, si fa riferimento al marito e alla moglie come "attori" della celebrazione (artt. 107 e 108), protagonisti del rapporto coniugale e autori della generazione (artt. 231 e seguenti), come pure la distinzione di sesso tra i coniugi è rinvenibile in altre disposizioni (artt. 143, 143-bis, 143-ter, 156-bis e seguenti cod. civ.), e segnatamente in quelle che disciplinano il concreto atteggiarsi dei diritti e doveri dei coniugi tra loro e verso i figli, nonché nel decreto del presidente della Repubblica 3 novembre 2000, n. 396 (Regolamento per la revisione e la semplificazione dell'ordinamento dello stato civile, a norma dell'art. 2 comma 12, della legge 15 maggio 1997, n. 127), nella parte in cui prevede, nell'art. 64, lettera e), che l'atto di matrimonio deve indicare «la dichiarazione degli sposi di volersi prendere rispettivamente in marito e in moglie»;
 che, peraltro, il giudice a quo ritiene, come già posto in evidenza nelle precedenti ordinanze di rimessione pronunciate dal Tribunale di Venezia e dalla Corte d'appello di Trento, di non poter ignorare il rapido trasformarsi della società e dei costumi avvenuto negli ultimi decenni, con il superamento del modello di famiglia tradizionalmente intesa ed il contestuale sorgere di forme diverse, seppur minoritarie, di convivenza, che chiedono protezione, ispirandosi al modello tradizionale e che mirano, come quello, ad essere considerate e disciplinate;
 che, a parere del rimettente, il primo riferimento costituzionale con il quale confrontarsi è l'art. 2 Cost., nella parte in cui riconosce i diritti inviolabili dell'uomo non solo nella sua sfera individuale ma anche, e forse soprattutto, nella sua sfera sociale, ossia «nelle formazioni sociali ove si svolge la sua personalità», delle quali la famiglia deve essere considerata la prima e fondamentale espressione; 
 che, pertanto, il diritto di sposarsi configura un diritto fondamentale della persona, riconosciuto a livello sovranazionale (artt. 12 e 16 della Dichiarazione Universale dei Diritti dell'Uomo del 1948, artt. 8 e 12 della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva in Italia con legge 4 agosto 1955, n. 848, ed ora artt. 7 e 9 della Carta dei diritti fondamentali dell'Unione europea proclamata a Nizza il 7 dicembre 2000), nonché dall'art. 2 Cost.; 
 che tale diritto va inteso sia nella sua accezione positiva di libertà di contrarre matrimonio con la persona prescelta (è citata la sentenza della Corte costituzionale n. 445 del 2002), sia in quella negativa di libertà di non sposarsi e di convivere senza formalizzare l'unione (è citata la sentenza n. 166 del 1998), trattandosi di una scelta sulla quale lo Stato non può interferire, a meno che non vi siano interessi prevalenti incompatibili;
 che, a parere del Tribunale, nell'ipotesi in cui una persona intenda contrarre matrimonio con altra persona dello stesso sesso, non sembra possa verificarsi alcun pericolo di lesione ad interessi pubblici o privati di rilevanza costituzionale, quali potrebbero essere la sicurezza o la salute pubblica, in quanto il diritto dei figli di crescere in un ambiente familiare idoneo, che può venire in rilievo, potrebbe avere incidenza solo in riferimento al riconoscimento del diritto delle coppie omosessuali coniugate di avere figli adottivi, che è un diritto distinto, e non necessariamente  connesso, rispetto a quello di contrarre matrimonio;
 che, in riferimento al parametro di cui all'art. 3 Cost., che vieta ogni discriminazione irragionevole, conferendo a tutti i cittadini «pari dignità sociale, senza distinzione di sesso, di razza, di lingua, di religione, di opinioni politiche, di condizioni personali e sociali», impegnando lo Stato a «rimuovere gli ostacoli di ordine economico e sociale, che, limitando di fatto la  libertà e l'eguaglianza dei cittadini, impediscono il pieno sviluppo della persona umana», il diritto di contrarre matrimonio rappresenta un  momento essenziale di espressione della dignità umana, sicché deve essere garantito a tutti, senza discriminazioni derivanti dal sesso o dalle condizioni personali (quali l'orientamento sessuale), con conseguente obbligo dello Stato d'intervenire in caso di impedimenti all'esercizio;
 che, pertanto, la norma che esclude o comunque non consente alle persone omosessuali di contrarre matrimonio con persone dello stesso sesso, così seguendo il proprio orientamento sessuale, non ha alcuna giustificazione razionale, soprattutto se raffrontata con l'analoga situazione delle persone transessuali, le quali possono contrarre matrimonio con persone del proprio stato di nascita, una volta ottenuta la rettificazione di attribuzione di sesso in applicazione della legge 14 aprile 1982, n. 164 (Norme in materia di rettificazione di attribuzione di sesso), la cui legittimità costituzionale è stata riconosciuta dalla Corte costituzionale con sentenza n. 161 del 1985, non tanto sulla base del fatto che i soggetti abbiano compiuto e portato a termine un trattamento medico chirurgico e che vi sia stata l'attribuzione del sesso opposto con provvedimento del Tribunale, ma sull'assunto che la citata sentenza ha definito l'orientamento del transessuale come «naturale modo di essere» ;
 che la legge n. 164 del 1982 si colloca, dunque, nell'alveo di una civiltà giuridica in evoluzione, sempre più attenta ai valori di libertà e dignità della persona umana, valorizzando l'orientamento psicosessuale della persona; 
 che, di conseguenza, non appare giustificata la discriminazione tra coloro che hanno un naturale orientamento psichico che li spinge ad una unione omosessuale, e non vogliono effettuare alcun intervento chirurgico di adattamento, né ottenere la rettificazione anagrafica per conseguire un'attribuzione di sesso contraria al sesso biologico, ai quali è precluso il matrimonio, e i transessuali, che sono ammessi al matrimonio pur appartenendo allo stesso sesso biologico ed essendo incapaci di procreare; 
 che, quindi, «la parità di diritti per i cittadini omosessuali potrà dirsi realizzata soltanto se sarà loro consentito di scegliere di regolare la propria vita e i propri  rapporti giuridici e patrimoniali optando fra le stesse alternative che sono a disposizione dei cittadini transessuali ed eterosessuali»; 
 che, a parere del Tribunale, il terzo parametro di riferimento è proprio il disposto dell'art. 29, primo comma, Cost., ove si ritenga che la norma, per la dizione utilizzata, non tuteli il solo nucleo legittimo di carattere tradizionale, ossia l'unione di un uomo ed una donna suggellata dal vincolo giuridico del matrimonio;
 che, infatti, i Costituenti non posero l'esigenza della diversità di sesso, perché a quel tempo era naturale che il matrimonio fosse possibile soltanto tra persone di sesso diverso; 
 che, ad avviso del rimettente, oggi si deve interpretare tale disposizione costituzionale, in sé neutra, scritta in anni lontani, in un contesto sociale di riferimento molto diverso dall'attuale, tenendo conto delle rilevanti trasformazioni sociali intervenute;
 che il problema ermeneutico riguarda il termine «naturale», unico limite posto dalla norma al riconoscimento costituzionale della famiglia come società fondata sul matrimonio, che non può essere interpretato alla luce di una particolare concezione ideologica, religiosa o altro, né alla luce del suo significato storico, ma va inteso come riferimento al matrimonio quale dato pregiuridico, ossia come riconoscimento da parte del diritto positivo della  preesistenza ed autonomia della famiglia, in quanto comunità originaria (come emerge dalla lettura dei lavori dell'assemblea costituente);
 che la società contemporanea conosce diversi modelli di convivenza, come quella eterosessuale o quella omosessuale, ed è necessario, quindi, individuare un criterio oggettivo, presente all'interno della Costituzione, per selezionare il modello o i modelli rilevanti ed eventualmente censurare quello che, benché naturale, possa essere avvertito come negativo, avendo sempre come valore principale fondante il rispetto della dignità della persona, in sé e nei rapporti con gli altri, con i quali essa convive e si confronta;
 che, a parere del rimettente, non è conforme alla dignità della persona privare qualcuno della possibilità di fondare una famiglia in ragione di un criterio come quello dell'orientamento sessuale, di un criterio, cioè, che, come quello della razza, della nazionalità, dell'origine etnica, non fa parte delle scelte individuali, ma è dato inerente, connaturato, congenito;
 che, del resto, la tutela della tradizione non rientra nelle finalità dell'art. 29 Cost. e la famiglia ed il matrimonio sono istituti aperti alle trasformazioni che si verificano nella storia, come dimostrato dall'evoluzione che ha interessato la loro disciplina dal 1948 ad oggi;
 che, infatti, dal modello di famiglia previsto dal codice civile del 1942, basato su un matrimonio indissolubile e su una struttura gerarchica a subordinazione femminile (caratteristiche che si traducevano, nell'ambito penalistico, nella repressione del solo adulterio femminile, nella responsabilità penale del marito per abuso dei mezzi di correzione nei confronti della moglie, nella previsione del delitto d'onore, nell'estinzione del reato di violenza carnale a mezzo del matrimonio riparatore), si è giunti, anche mediante gli interventi della Corte costituzionale a tutela dell'eguaglianza morale e giuridica dei coniugi (fra cui la storica sentenza n. 126 del 1968), alla riforma del diritto di famiglia, attuata con la legge 19 maggio 1975, n. 151 (Riforma del diritto di famiglia), in linea con i principi di eguaglianza morale e giuridica dei coniugi;
 che, a parere del giudice a quo, gli esempi connessi alla mancata "istituzionalizzazione" dell'indissolubilità del matrimonio, con la conseguente introduzione legislativa del divorzio, ed alla progressiva attuazione per via legislativa del principio costituzionale di eguaglianza tra figli legittimi e figli naturali, dimostrano come l'accezione costituzionale di famiglia, lungi dall'essere ancorata ad una conformazione tipica ed inalterabile, si sia al contrario dimostrata permeabile ai mutamenti sociali, con le relative ripercussioni sul regime giuridico familiare;
 che, secondo il rimettente, le considerazioni svolte in riferimento al significato dell'espressione "società naturale" ed in relazione alla mancata tutela del "matrimonio tradizionale" nell'ambito dell'art. 29 Cost. conducono a ritenere infondate le tesi che giustificano il divieto di matrimonio tra persone dello stesso sesso, ricorrendo ad argomenti correlati alla capacità procreativa della coppia ed alla tutela della procreazione, perché né la Costituzione né il diritto civile prevedono la capacità di avere figli come condizione per contrarre matrimonio, ovvero l'assenza di tale capacità come condizione di invalidità o causa di scioglimento del matrimonio, essendo matrimonio e filiazione istituti nettamente distinti;
 che, pertanto, il citato art. 29 Cost., attribuendo tutela costituzionale alla famiglia legittima, in quanto contribuisce, grazie alla stabilità del quadro delle relazioni sociali, affettive ed economiche che comporta, alla realizzazione della personalità dei coniugi, non costituisce ostacolo al riconoscimento giuridico del matrimonio tra persone dello stesso sesso, ma assurge indubbiamente ad ulteriore parametro, unitamente agli artt. 2 e 3, sulla base del quale dubitare della costituzionalità del divieto; 
 che, sulla base di tali argomenti, il Tribunale è giunto al convincimento della non manifesta infondatezza della questione di illegittimità costituzionale, pur parzialmente modificando i parametri di riferimento rispetto a quelli indicati dalle ricorrenti, delle norme di cui agli artt. 93, 96, 98, 107, 108, 143, 143-bis, 156-bis e 231 cod. civ., ove siano incompatibili, o non consentono che le persone di orientamento omosessuale possano contrarre matrimonio con persone dello stesso sesso; 
 che il giudice rimettente ha poi affidato alla Corte la valutazione circa l'eventuale estensione della pronuncia di fondatezza anche ad altre disposizioni legislative interessate in via di consequenzialità, ai sensi dell'art. 27 della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale).
 Considerato che il Tribunale di Ferrara, con l'ordinanza indicata in epigrafe, dubita, in riferimento agli articoli 2, 3 e 29, primo comma, della Costituzione, della legittimità costituzionale degli articoli 93, 96, 98, 107, 108, 143, 143-bis, 156-bis, 231 del codice civile, nella parte in cui non consentono che le persone dello stesso sesso possano contrarre matrimonio; 
 che questa Corte, con sentenza n. 138 del 2010, emessa a seguito delle ordinanze del Tribunale di Venezia e della Corte d'appello di Trento menzionate dall'attuale rimettente, ha già esaminato la questione di legittimità costituzionale delle norme in questa sede censurate, in riferimento ai parametri costituzionali qui richiamati, nonché all'art. 117, primo comma, Cost. (che non può ritenersi evocato dal Tribunale ferrarese mediante la generica relatio ai citati provvedimenti del Tribunale di Venezia e della Corte di appello di Trento);
 che, in particolare, con la citata sentenza n. 138 del 2010, la questione sollevata in riferimento all'art. 2 Cost. è stata dichiarata inammissibile, perché diretta ad ottenere una pronunzia additiva non costituzionalmente obbligata;
 che con la medesima sentenza la questione, sollevata in riferimento ai parametri individuati negli artt. 3 e 29 Cost., è stata dichiarata non fondata, sia perché l'art. 29 Cost. si riferisce alla nozione di matrimonio definita dal codice civile come unione tra persone di sesso diverso, e questo significato del precetto costituzionale non può essere superato per via ermeneutica, sia perché (in ordine all'art. 3 Cost.) le unioni omosessuali non possono essere ritenute omogenee al matrimonio;
 che non risultano qui allegati profili diversi o ulteriori, idonei a superare gli argomenti addotti nella precedente pronuncia ed anche ribaditi nella successiva ordinanza n. 276 del 2010; 
 che identiche considerazioni valgono anche con riguardo all'art. 231 cod. civ., censurato dall'attuale rimettente insieme con le altre norme indicate in epigrafe;
 che, pertanto, la questione di legittimità costituzionale, sollevata con riferimento all'art. 2 Cost., deve essere dichiarata manifestamente inammissibile, e la questione sollevata con riferimento agli artt. 3 e 29 Cost. deve essere dichiarata manifestamente infondata (ex plurimis: ordinanze n. 42, n. 34 e n. 16 del 2009).
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 a) dichiara la manifesta inammissibilità della questione di legittimità costituzionale degli articoli 93, 96, 98, 107, 108, 143, 143-bis, 156-bis, 231 del codice civile, sollevata, in riferimento all'articolo 2 della Costituzione, dal Tribunale di Ferrara con l'ordinanza indicata in epigrafe;&#13;
 b) dichiara la manifesta infondatezza della questione di legittimità costituzionale degli articoli sopra indicati del codice civile, sollevata, in riferimento agli articoli 3 e 29 della Costituzione, dal Tribunale di Ferrara con la medesima ordinanza.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 16 dicembre 2010.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Alessandro CRISCUOLO, Redattore&#13;
 Maria Rosaria FRUSCELLA, Cancelliere&#13;
 Depositata in Cancelleria il 5 gennaio 2011.&#13;
 Il Cancelliere&#13;
 F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
