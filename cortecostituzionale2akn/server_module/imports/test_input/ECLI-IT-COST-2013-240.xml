<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/240/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/240/"/>
          <FRBRalias value="ECLI:IT:COST:2013:240" name="ECLI"/>
          <FRBRdate date="07/10/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="240"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/240/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/240/ita@/!main"/>
          <FRBRdate date="07/10/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2013/240/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2013/240/ita@.xml"/>
          <FRBRdate date="07/10/2013" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="11/10/2013" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2013</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>SILVESTRI</cc:presidente>
        <cc:relatore_pronuncia>Gaetano Silvestri</cc:relatore_pronuncia>
        <cc:data_decisione>07/10/2013</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori:
 Presidente: Gaetano SILVESTRI; Giudici : Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 7 della legge della Regione Veneto 3 dicembre 2012, n. 46 (Modifiche di disposizioni regionali in materia di programmazione ed organizzazione socio-sanitaria e di tutela della salute), promosso dal Presidente del Consiglio dei ministri con ricorso notificato a mezzo posta il 31 gennaio - 8 febbraio 2013, depositato in cancelleria il 4 febbraio 2013 ed iscritto al n. 13 del registro ricorsi 2013.
 Visto l'atto di costituzione della Regione Veneto;
 udito nella camera di consiglio del 25 settembre 2013 il Giudice relatore Gaetano Silvestri.
 Ritenuto che, con ricorso notificato a mezzo posta il 31 gennaio - 8 febbraio 2013 e depositato il 4 febbraio 2013, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni di legittimità costituzionale dell'articolo 7 della legge della Regione Veneto 3 dicembre 2012, n. 46 (Modifiche di disposizioni regionali in materia di programmazione ed organizzazione socio-sanitaria e di tutela della salute), per violazione degli artt. 97 e 117, terzo comma, della Costituzione;
 che la disposizione impugnata ha modificato il comma 8-ter dell'art. 13 della legge della Regione Veneto 14 settembre 1994, n. 56 (Norme e principi per il riordino del servizio sanitario regionale in attuazione del decreto legislativo 30 dicembre 1992, n. 502 «Riordino della disciplina in materia sanitaria», così come modificato dal decreto legislativo 7 dicembre 1993, n. 517) - secondo il quale l'incarico di direttore generale delle aziende sanitarie regionali aveva durata pari a tre anni - ed ha previsto che il predetto incarico abbia «di norma [...] una durata pari a quella della legislatura regionale» e che «Il mandato del direttore generale scade centottanta giorni dopo l'insediamento della nuova legislatura»;
 che la norma in esame è impugnata per violazione dell'art. 117, terzo comma, Cost., in quanto, secondo il ricorrente, la stessa si pone in contrasto con l'art. 3-bis, comma 8, del decreto legislativo 30 dicembre 1992, n. 502 (Riordino della disciplina in materia sanitaria, a norma dell'articolo 1 della legge 23 ottobre 1992, n. 421), che esprimerebbe principi fondamentali in materia di tutela della salute;
 che, sempre secondo la difesa statale, l'art. 7 della legge reg. Veneto n. 46 del 2012 viola l'art. 97 Cost., in quanto il collegamento dell'incarico di direttore generale alla durata della legislatura darebbe vita ad «una forma di spoils system nei confronti di una figura manageriale, come appunto è quella del direttore generale delle ASL, che [...] non può seguire le sorti degli organi politici della Regione, perché ciò contrasterebbe con il [...] principio di imparzialità e buon andamento dell'amministrazione»;
 che il ricorrente richiama, a sostegno dell'illegittimità della norma impugnata, le argomentazioni della sentenza n. 104 del 2007 della Corte costituzionale, rilevando come la norma regionale oggetto dell'odierno giudizio presenti molte affinità con quella esaminata e dichiarata incostituzionale con la pronuncia citata;
 che la Regione Veneto si è costituita in giudizio, chiedendo il rigetto del ricorso;
 che, in particolare, la resistente contesta l'asserita riconducibilità della normativa in esame alla materia della «tutela della salute», rinvenendo piuttosto l'ambito materiale di riferimento nell'organizzazione amministrativa della Regione, rientrante nella competenza legislativa residuale di quest'ultima (art. 117, quarto comma, Cost.);
 che, inoltre, se anche si potesse ricondurre la normativa impugnata all'ambito della «tutela della salute», la questione risulterebbe infondata, poiché la disposizione statale interposta, richiamata nel ricorso, non recherebbe principi fondamentali e dunque non sarebbe vincolante per la Regione;
 che l'unico principio desumibile dal quadro normativo statale sarebbe quello della «durata determinata e breve» dell'incarico di direttore generale delle aziende sanitarie regionali; principio, questo, che sarebbe rispettato dalla normativa regionale impugnata;
 che, avuto riguardo alla prospettata violazione dell'art. 97 Cost., la difesa regionale assume che i suddetti direttori generali, sulla base delle modalità di selezione e delle funzioni loro assegnate, rivestano un incarico di carattere apicale, con la conseguenza che la prevista cessazione dell'incarico stesso a seguito della scadenza della legislatura non sarebbe in contrasto con la norma costituzionale richiamata;
 che, con atto depositato il 3 giugno 2013, il Presidente del Consiglio dei ministri ha rinunciato al ricorso in considerazione delle modifiche introdotte con la legge della Regione Veneto 19 marzo 2013, n. 2 (Norme di semplificazione in materia di igiene, medicina del lavoro, sanità pubblica e altre disposizioni per il settore sanitario), con la quale la resistente «si è adeguata ai rilievi governativi e ha fra l'altro abrogato espressamente l'articolo 7 della [...] legge regionale 46/2012»;
 che, con atto depositato il 23 luglio 2013, la Regione Veneto ha accettato la suddetta rinuncia.
 Considerato che il Presidente del Consiglio dei ministri, con atto depositato il 3 giugno 2013, in seguito alle modifiche apportate alla impugnata disposizione dalla legge della Regione Veneto 19 marzo 2013, n. 2 (Norme di semplificazione in materia di igiene, medicina del lavoro, sanità pubblica e altre disposizioni per il settore sanitario), ha rinunciato al ricorso; 
 che la Regione Veneto in data 23 luglio 2013 ha depositato atto di accettazione della rinuncia; 
 che, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, la rinuncia al ricorso, seguita dalla accettazione della controparte, estingue il processo.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi&#13;
  LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p>&#13;
 dichiara estinto il processo.&#13;
 </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 ottobre 2013.&#13;
 F.to:&#13;
 Gaetano SILVESTRI, Presidente e Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria l'11 ottobre 2013.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
