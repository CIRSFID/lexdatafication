<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/437/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/437/"/>
          <FRBRalias value="ECLI:IT:COST:2006:437" name="ECLI"/>
          <FRBRdate date="06/12/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="437"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/437/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/437/ita@/!main"/>
          <FRBRdate date="06/12/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2006/437/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2006/437/ita@.xml"/>
          <FRBRdate date="06/12/2006" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="19/12/2006" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2006</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Franco Gallo</cc:relatore_pronuncia>
        <cc:data_decisione>06/12/2006</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 2634 codice civile, promosso con ordinanza del 10 maggio 2004 dal Giudice per le indagini preliminari del Tribunale di Forlì nel procedimento penale a carico di Pier Paolo Cimatti ed altra, iscritta al n. 744 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 39, prima serie speciale, dell'anno 2004.     
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del 22 novembre 2006 il Giudice relatore Franco Gallo. 
    Ritenuto che il Giudice per le indagini preliminari del Tribunale di Forlì, a séguito dell'opposizione proposta, quale «persona offesa» dal reato, dal socio di minoranza di una società di capitali avverso la richiesta del pubblico ministero di archiviare un procedimento penale promosso nei confronti di due persone indagate per i reati di cui agli artt. 2634 del codice civile (Infedeltà patrimoniale) e 640 del codice penale (Truffa), ha sollevato – in riferimento all'art. 3 della Costituzione – questione di legittimità costituzionale dell'art. 2634 cod. civ., nella parte in cui «esclude, dal novero dei soggetti attivi del reato, i soci»; 
    che, riferisce il rimettente, a séguito di un esposto presentato dal socio di minoranza di una società a responsabilità limitata, erano stati iscritti nel registro degli indagati per i suddetti reati l'amministratore, socio della stessa società, e sua madre, titolare di una quota, che, unita alla partecipazione detenuta dal figlio, raggiungeva la maggioranza del capitale sociale; 
    che, secondo quanto affermato nell'esposto – prosegue il rimettente –, l'amministratore, pur avendo un interesse in conflitto con quello della società, aveva concesso in affitto alla madre un bene sociale, «in spregio alla situazione finanziaria della società e senza alcuna garanzia sulla durata e sulla percezione delle entrate, a causa del tipo di attività e della facoltà di recesso concessa al “conduttore”»; 
    che, sempre secondo il giudice a quo, la notizia di reato era stata iscritta nel registro degli indagati senza l'indicazione del concorso esterno della socia, ai sensi dell'art. 110 cod. pen., nel reato di infedeltà patrimoniale ascritto all'amministratore e che, pertanto, detta socia doveva ritenersi “astrattamente responsabile” del reato «a titolo proprio, e non di concorso»; 
    che tuttavia, osserva il rimettente, il citato art. 2634 cod. civ. esclude dal novero dei soggetti attivi del reato il socio che, pur versando in conflitto di interessi e concorrendo a deliberare un atto di disposizione dei beni sociali, non riveste la carica di amministratore, direttore generale o liquidatore della società; 
    che, per il giudice a quo, tale esclusione della responsabilità penale dei soci sarebbe irragionevole, «in quanto discrimina la condotta di chi “concorre a deliberare” un atto di disposizione di beni sociali rivestendo una carica amministrativa […] rispetto a quella di chi», essendo anch'egli portatore di un interesse in conflitto con quello della società, «egualmente concorre a deliberare un atto di disposizione» di tali beni, «valendosi della titolarità di quote sociali»; 
    che, osserva ancora il giudice a quo, la disposizione che disciplina l'impugnabilità delle delibere sociali per conflitto di interessi (art. 2373 cod. civ.) individua quale ipotesi tipica di tale conflitto la «deliberazione approvata con il voto determinante di soci che abbiano […] un interesse in conflitto con quello della società», sicché non si comprenderebbero «le ragioni per le quali la tutela penale del conflitto di interessi nel diritto societario sia così irrazionalmente disgiunta dai relativi fondamenti posti in campo civile»; 
    che, ad avviso del rimettente, posto che «il fattore scatenante del conflitto di interessi giuridicamente rilevante è l'incidenza dell'interesse personale sul patrimonio sociale e la possibilità di controllare una società», dovrebbe ritenersi che «anche, e soprattutto, i soci (quando hanno o concorrono a formare la maggioranza del capitale nelle società con scopo di lucro) vanno considerati e sanzionati quali soggetti che possono interferire nella gestione sociale (ossia, “concorrere a deliberare” atti sociali)»; 
    che, secondo il giudice a quo, la procedibilità a querela della persona offesa – «ossia […] della società» –, prevista dall'art. 2634 cod. civ., comporterebbe un ulteriore aspetto di irragionevolezza della norma che esclude i soci dai soggetti attivi del reato, in quanto l'amministratore, legale rappresentante della società danneggiata, potrebbe proporre querela solo nei confronti di se stesso, quale soggetto attivo del medesimo reato; 
    che tuttavia, osserva il Giudice per le indagini preliminari, a detta irragionevolezza potrebbe ovviarsi «forzando il tenore letterale dell'art. 2634 cod. civ.» e ritenendo «legittimato a proporre querela anche il singolo socio», alla stregua di una interpretazione di tale articolo prospettata nel giudizio a quo e che ha dato luogo ad una «questione […] pure aperta»; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente inammissibile o manifestamente infondata; 
    che la difesa erariale eccepisce l'inammissibilità della questione, perché il rimettente non ha indicato le ragioni che lo hanno indotto a non trasmettere gli atti al pubblico ministero per la formulazione dell'imputazione a titolo di concorso nel reato, come gli era consentito dall'art. 409, comma 5, del codice di procedura penale; 
    che, secondo l'Avvocatura, la questione sarebbe inammissibile anche perché volta ad introdurre una nuova categoria di soggetti attivi in una fattispecie di reato proprio, in contrasto con il principio di stretta legalità delle figure incriminatici, desumibile dall'art. 25 Cost. 
    che, nel merito, la questione concernente la mancata inclusione del socio tra i soggetti attivi del reato di cui all'art. 2634 cod. civ. sarebbe comunque manifestamente infondata, perché spetta solo al legislatore stabilire quali comportamenti illeciti meritano di essere sanzionati sul piano penale; 
    che in ogni caso, prosegue l'Avvocatura, non risponde al vero che il possesso di quote sociali influisce sulla gestione della società in misura pari o maggiore di quanto influisca la titolarità della carica sociale di amministratore, poiché è l'amministratore ad occuparsi della gestione sociale, con l'unica eccezione delle delibere che la legge o lo statuto riservano all'assemblea dei soci, e pertanto deve ritenersi ragionevole la scelta legislativa di considerare l'infedeltà dell'amministratore come un fatto idoneo a pregiudicare l'interesse sociale in misura maggiore del conflitto di interessi in cui versi un socio in occasione delle delibere assembleari; 
    che, ad avviso della difesa erariale, sarebbe parimenti infondata anche la questione concernente la previsione della procedibilità a querela del suddetto reato e la conseguente impunità dell'amministratore, paventata dal giudice a quo; 
    che infatti, per la difesa erariale, l'amministratore giudiziario nominato ai sensi dell'art. 2409 cod. civ. può proporre querela nei confronti dell'amministratore infedele, il quale può anche essere revocato mediante una delibera dei soci; 
    che inoltre, sempre per l'Avvocatura dello Stato, posto che la norma censurata configura un reato di danno, non può escludersi che i singoli soci siano persone offese dal reato, in ragione della diminuzione del valore della loro quota di partecipazione sociale subíta in conseguenza del danno patrimoniale cagionato alla società dall'amministratore infedele e che, quindi, gli stessi soci siano titolari del diritto di querela.  
    Considerato che il Giudice per le indagini preliminari del Tribunale di Forlì dubita, in riferimento all'art. 3 della Costituzione, della legittimità costituzionale dell'art. 2634 del codice civile, nella parte in cui irragionevolmente esclude dal novero dei soggetti attivi del reato di infedeltà patrimoniale i soci che, in conflitto di interessi con la società, concorrano in modo determinante a deliberare atti di disposizione del patrimonio sociale; 
    che, ad avviso del rimettente, la norma censurata – assoggettando a sanzione penale soltanto «gli amministratori, i direttori generali e i liquidatori, che, avendo un interesse in conflitto con quello della società, al fine di procurare a sé o ad altri un ingiusto profitto o altro vantaggio, compiono o concorrono a deliberare atti di disposizione dei beni sociali, cagionando intenzionalmente alla società un danno patrimoniale» – limita in modo irragionevole i soggetti attivi del reato, perché esclude la punibilità del socio nel caso in cui questo, al pari dell'amministratore, concorra a deliberare un atto di disposizione di beni sociali in conflitto di interessi con la società; 
    che deve preliminarmente rilevarsi che, contrariamente a quanto ritenuto dall'Avvocatura generale dello Stato, il rimettente non ha sollevato alcuna questione di legittimità costituzionale riguardante la procedibilità a querela, anziché d'ufficio, del reato di cui all'art. 2634 cod. civ.; 
    che inducono a tale conclusione i seguenti rilievi: a) nel dispositivo dell'ordinanza di rimessione non vi è traccia di tale questione; b) nella motivazione della medesima ordinanza, la procedibilità del reato di infedeltà patrimoniale a querela dell'amministratore, quale rappresentante della società danneggiata, costituisce solo un argomento addotto a sostegno della necessità di rendere punibile anche la condotta di soci che non rivestano cariche sociali; c) il rimettente non solo osserva che la questione circa la legittimazione del singolo socio a proporre querela è stata già prospettata nel giudizio a quo e deve considerarsi ancora «aperta», ma addirittura qualifica espressamente il socio di minoranza come «persona offesa» dal reato, considerandolo, perciò, legittimato a proporre la querela e l'opposizione alla richiesta di archiviazione; 
    che la questione concernente la mancata inclusione del socio tra i soggetti attivi del reato di cui all'art. 2634 cod. civ. è manifestamente inammissibile sia per difetto di motivazione sulla rilevanza sia perché il rimettente richiede a questa Corte una pronuncia additiva in malam partem  in materia penale; 
    che, quanto al primo motivo di inammissibilità, l'ordinanza è priva di motivazione in ordine alle ragioni per le quali, a fronte della richiesta di archiviazione di una notizia di reato e pur ritenendo possibile – nella specie – la configurazione del concorso esterno nel reato, il rimettente non abbia, come era sua facoltà, restituito gli atti al pubblico ministero ai sensi dell'art. 409 del codice di procedura penale, disponendo la formulazione dell'imputazione nei confronti della socia, a titolo di concorso nel reato proprio dell'amministratore; 
    che, quanto all'altro motivo di inammissibilità, osta all'esame nel merito della questione il secondo comma dell'art. 25 Cost., il quale – per costante giurisprudenza di questa Corte – nell'affermare il principio secondo cui nessuno può essere punito se non in forza di una legge entrata in vigore prima del fatto commesso, esclude che la Corte costituzionale possa introdurre in via additiva nuovi reati o che l'effetto di una sua sentenza possa essere quello di ampliare o aggravare figure di reato già esistenti, trattandosi di interventi riservati in via esclusiva alla discrezionalità del legislatore (ex plurimis, sentenze n. 394 del 2006; n. 161 del 2004; n. 49 del 2002; n. 580, n. 508 e n. 183 del 2000; n. 411 del 1995). 
    Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 2634 del codice civile, sollevata, in riferimento all'art. 3 della Costituzione, dal Giudice per le indagini preliminari del Tribunale di Forlì con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  il 6 dicembre 2006. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Franco GALLO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 19 dicembre 2006. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
