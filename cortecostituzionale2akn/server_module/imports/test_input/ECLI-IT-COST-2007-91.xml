<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/91/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/91/"/>
          <FRBRalias value="ECLI:IT:COST:2007:91" name="ECLI"/>
          <FRBRdate date="05/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="91"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/91/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/91/ita@/!main"/>
          <FRBRdate date="05/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2007/91/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2007/91/ita@.xml"/>
          <FRBRdate date="05/03/2007" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="16/03/2007" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2007</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Alfonso Quaranta</cc:relatore_pronuncia>
        <cc:data_decisione>05/03/2007</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'art. 10, comma 3, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione), promosso con ordinanza del 16 dicembre 2005 dal Tribunale di Patti, sezione distaccata di Sant'Agata di Militello, nel procedimento penale a carico di M. A., iscritta al n. 76 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale  della Repubblica n. 12, prima serie speciale, dell'anno 2006. 
    Udito nella camera di consiglio del 21 febbraio 2007 il Giudice relatore Alfonso Quaranta. 
    Ritenuto che il Tribunale di Patti, sezione distaccata di Sant'Agata di Militello, ha sollevato, con l'ordinanza di cui in epigrafe, questione di legittimità costituzionale – in riferimento all'art. 3 della Costituzione – dell'art. 10, comma 3, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione); 
    che il rimettente censura la predetta norma, nella parte in cui prevede che l'applicazione delle più favorevoli disposizioni per il reo in ordine al termine di prescrizione del reato, contenute nell'art. 6 della medesima legge n. 251 del 2005, sia limitata, quanto ai processi di primo grado, unicamente a quelli per i quali non «sia stata dichiarata l'apertura del dibattimento»; 
    che, secondo il giudice a quo, la censurata disposizione «introduce, di fatto, una diversità di trattamento dei processi già pendenti ed incardinati» (categoria alla quale appartiene – rileva il rimettente – il giudizio principale), rispetto a quelli «per i quali il dibattimento in primo grado non è ancora stato dichiarato aperto», donde la conseguente «violazione dell'art. 3 della Costituzione», atteso che i «benefici relativi alla maggiore brevità del termine prescrizionale» sono circoscritti «ad alcuni processi e, quindi ad alcuni cittadini». 
    Considerato che il Tribunale di Patti, sezione distaccata di Sant'Agata di Militello, ha sollevato, con l'ordinanza di cui in epigrafe, questione di legittimità costituzionale – in riferimento all'art. 3 della Costituzione – dell'art. 10, comma 3, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione); 
    che – a prescindere dal rilievo che questa Corte, con la sopravvenuta sentenza n. 393 del 2006, ha dichiarato l'illegittimità costituzionale del predetto art. 10, comma 3, della legge n. 251 del 2005, limitatamente alle parole «dei processi già pendenti in primo grado ove vi sia stata la dichiarazione di apertura del dibattimento, nonché» – deve rilevarsi che il giudice a quo ha omesso di descrivere la fattispecie concreta devoluta alla sua cognizione, di talché la questione sollevata va dichiarata manifestamente inammissibile per difetto di motivazione sulla rilevanza (in tal senso, si vedano, da ultimo, le ordinanze n. 49 del 2007 e n. 459 del 2006).  
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
     LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 10, comma 3, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione), sollevata – in riferimento all'art. 3 della Costituzione – dal Tribunale di Patti, sezione distaccata di Sant'Agata di Militello, con l'ordinanza di cui in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 marzo 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfonso QUARANTA, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 16 marzo 2007.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
