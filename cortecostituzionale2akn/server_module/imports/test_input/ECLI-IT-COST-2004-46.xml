<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/46/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/46/"/>
          <FRBRalias value="ECLI:IT:COST:2004:46" name="ECLI"/>
          <FRBRdate date="20/01/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="46"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/46/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/46/ita@/!main"/>
          <FRBRdate date="20/01/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2004/46/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2004/46/ita@.xml"/>
          <FRBRdate date="20/01/2004" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="27/01/2004" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2004</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>ZAGREBELSKY</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>20/01/2004</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale degli articoli 443, comma 3, e 595 del codice di procedura penale, promosso con ordinanza dell'11 dicembre 2002 dalla Corte di appello di Venezia nel procedimento penale a carico di A.M.B.B. ed altri, iscritta al n. 231 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 18, prima serie speciale, dell'anno 2003. 
Udito nella camera di consiglio del 26 novembre 2003 il Giudice relatore Giovanni Maria Flick. 
Ritenuto che con l'ordinanza in epigrafe la Corte di appello di Venezia ha sollevato, in riferimento all'art. 111, secondo comma, della Costituzione, questione di legittimità costituzionale degli artt. 443, comma 3, e 595 del codice di procedura penale, nella parte in cui escludono l'appello incidentale del pubblico ministero contro le sentenze di condanna pronunciate a seguito di giudizio abbreviato; 
che l'ordinanza premette, in punto di fatto, che il pubblico ministero aveva proposto appello incidentale avverso la sentenza di condanna emessa a seguito di giudizio abbreviato nei confronti degli imputati nel processo a quo — sentenza sottoposta al vaglio della Corte rimettente per effetto degli appelli principali proposti da tutti gli imputati — censurando l'avvenuta applicazione ad uno di essi delle circostanze attenuanti generiche e la conseguente determinazione della pena da parte del primo giudice; 
che, secondo il giudice a quo, alla luce della giurisprudenza della Corte di cassazione — costante a partire dall'intervento delle sezioni unite con sentenza 18 giugno-23 luglio 1993, n. 7247, e rimasta ferma anche dopo le modifiche apportate alla disciplina del giudizio abbreviato dalla legge 16 dicembre 1999, n. 479 (Modifiche alle disposizioni sul procedimento davanti al tribunale in composizione monocratica e altre modifiche al codice di procedura penale. Modifiche al codice penale e all'ordinamento giudiziario. Disposizioni in tema di contenzioso civile pendente, di indennità spettanti al giudice di pace e di esercizio della professione forense) — il predetto appello incidentale dovrebbe essere dichiarato inammissibile ai sensi dell'art. 443, comma 3, cod. proc. pen.; tale disposizione, infatti — escludendo in modo radicale l'appello del pubblico ministero contro le sentenze di condanna pronunciate a seguito di giudizio abbreviato (fatta eccezione per quelle che modifichino il titolo del reato) — non consentirebbe di operare alcuna distinzione tra l'appello principale e quello incidentale; 
che ad avviso del rimettente, peraltro, le norme impugnate, nella parte in cui precludono al pubblico ministero anche il solo appello incidentale, contrasterebbero con il principio di parità delle parti nel processo penale, sancito dall'art. 111, secondo comma, Cost.; 
che, al riguardo, il rimettente osserva come questa Corte abbia già avuto modo di scrutinare la costituzionalità dell'art. 443, comma 3, cod. proc. pen. nella cornice della nuova disciplina del giudizio abbreviato, escludendo, in particolare, con l'ordinanza n. 421 del 2001, che la disposizione censurata contrasti con l'art. 111 Cost., quanto al limite all'appello principale; 
che la decisione si fonda sul rilievo che - in un sistema nel quale il doppio grado di giurisdizione non forma oggetto di garanzia costituzionale - la preclusione all'appello della parte pubblica continua a trovare giustificazione «nell'obiettivo primario di una rapida e completa definizione dei processi svoltisi in primo grado secondo il rito alternativo di cui si tratta: rito che — sia pure, oggi, per scelta esclusiva dell'imputato — implica una decisione fondata, in primis, sul materiale probatorio raccolto dalla parte che subisce la limitazione censurata, fuori delle garanzie del contraddittorio»; 
che tale motivazione, tuttavia, in quanto riferita al solo appello principale, non soltanto lascerebbe aperta la questione in rapporto all'appello incidentale, ma offrirebbe addirittura «decisivi argomenti» per ritenere fondato il dubbio di costituzionalità oggi sollevato; 
che l'esclusione dell'appello incidentale del pubblico ministero non potrebbe trovare difatti giustificazione «nell'obiettivo … di una rapida e completa definizione dei processi», giacché nel caso di impugnazione incidentale il giudizio di appello deve essere comunque celebrato in conseguenza dell'impugnazione principale dell'imputato, che della prima costituisce «necessario presupposto e costante limite», condizionandone la stessa efficacia (art. 595, comma 4, cod. proc. pen.); 
che l'appello incidentale non comporterebbe, dunque, alcuna attività processuale ulteriore rispetto a quella già richiesta dall'appello principale, cui esso è strettamente collegato: né, d'altra parte, sarebbe possibile attribuire rilievo, nella prospettiva della «rapida e completa definizione dei processi», ad un dato «eventuale … ed imponderabile», quale il maggior dispendio di energie intellettuali da parte del giudice dell'impugnazione; 
che l'assunto troverebbe puntuale riprova nella fattispecie oggetto del giudizio a quo, nella quale l'appello incidentale attiene ad un aspetto — la misura della pena — già investito dall'appello principale: onde esso, senza alcun pregiudizio per la speditezza processuale, si limiterebbe solo a rendere “bidirezionale” la valutazione di congruità della pena già demandata al giudice dell'impugnazione; 
che il giudice a quo si dichiara altresì consapevole del fatto che questa Corte, anteriormente all'entrata in vigore della legge n. 479 del 1999, si era specificamente occupata anche del limite all'appello incidentale, riconoscendo, con la sentenza n. 98 del 1994, la legittimità di una disciplina che — a fronte della previsione, in talune fasi processuali, di posizioni di vantaggio per l'organo dell'accusa — munisca in altre fasi l'imputato di poteri «cui non debbano necessariamente corrispondere simmetrici poteri per il pubblico ministero», onde «ristabilire la parità processuale»; 
che tale affermazione, tuttavia, risulterebbe strettamente correlata al quadro normativo, anche costituzionale, dell'epoca; 
che lo scrutinio di costituzionalità era stato infatti compiuto, dalla sentenza n. 98 citata, con riferimento ai soli artt. 24 e 112 Cost.: parametri in rapporto ai quali si era escluso che il potere di impugnazione del pubblico ministero dovesse configurarsi in modo totalmente simmetrico al potere di impugnazione riconosciuto all'imputato quale esplicazione del diritto di difesa; e ciò per la considerazione che, mentre quest'ultimo non poteva essere sacrificato alle finalità deflattive proprie del giudizio abbreviato, il primo non era assistito da garanzie di pari intensità, essendo la disciplina dei poteri del pubblico ministero censurabile per irragionevolezza solo ove i poteri stessi risultassero inidonei all'assolvimento dei compiti previsti dall'art. 112 Cost.; 
che attualmente, per contro, la verifica della legittimità costituzionale della limitazione censurata andrebbe compiuta alla luce della nuova formulazione dell'art. 111 Cost., la quale avrebbe conferito al principio di «reale» parità delle parti nel processo penale una pregnanza ed una «forza di resistenza» assai maggiori; 
che, in pari tempo, sarebbe venuta meno anche quella situazione di «soverchiante prevalenza» del rappresentante della pubblica accusa nella fase delle indagini preliminari (e dunque nella raccolta delle fonti di prova) cui — secondo le affermazioni della sentenza n. 98 del 1994 — poteva ben corrispondere, in passato, per ragioni di «riequilibrio», il riconoscimento in altre fasi all'imputato di poteri non attribuiti al pubblico ministero; 
che la nuova disciplina delle indagini difensive, introdotta dalla legge 7 dicembre 2000, n. 397 (Disposizioni in materia di indagini difensive) — molto più ampia ed articolata di quella precedentemente racchiusa nell'art. 38 disp. att. cod. proc. pen. — comporterebbe, infatti, che il materiale probatorio, sulla cui base è accordato al solo imputato e senza il consenso del pubblico ministero il potere di scelta del rito alternativo, possa formarsi in modo ben diverso che per il passato: onde la palese disparità delle parti, con riferimento non soltanto all'iniziativa per l'accesso al giudizio abbreviato, ma anche ai poteri di impugnazione, non verrebbe più a «bilanciare» — nei termini «totalizzanti» in cui è configurata — una situazione analoga, ma di segno opposto (quella, cioè, della predisposizione da parte del pubblico ministero del materiale probatorio); 
che, in simile situazione, un diverso trattamento della parte pubblica e dell'imputato, quanto al diritto di impugnazione, potrebbe trovare una giustificazione costituzionalmente accettabile solo nella accennata finalità di rapida definizione dei processi: finalità peraltro non ravvisabile, per le considerazioni dianzi ricordate, in rapporto alla preclusione dell'appello incidentale, che si risolverebbe, così, in un sacrificio affatto irragionevole del principio di parità fra accusa e difesa. 
Considerato che la Corte di appello rimettente dubita della legittimità costituzionale, in riferimento all'art. 111, secondo comma, della Costituzione, degli artt. 443, comma 3, e 595 del codice di procedura penale, nella parte in cui escludono l'appello incidentale del pubblico ministero contro le sentenze di condanna pronunciate a seguito di giudizio abbreviato; 
che — come lo stesso giudice a quo rammenta — questa Corte ha già ripetutamente escluso, in termini generali, che la disposizione di cui all'art. 443, comma 3, cod. proc. pen. contrasti con il parametro costituzionale evocato: infatti, per un verso, il principio di parità delle parti «non comporta necessariamente l'identità tra i poteri processuali del pubblico ministero e quelli dell'imputato», potendo una disparità di trattamento risultare giustificata, «nei limiti della ragionevolezza, sia dalla peculiare posizione istituzionale del pubblico ministero, sia dalla funzione allo stesso affidata, sia da esigenze connesse alla corretta amministrazione della giustizia»; e, per un altro verso, il limite all'appello della parte pubblica, oggetto di censura, «continua a trovare giustificazione, come per il passato, nell'obiettivo primario della rapida e completa definizione dei processi svoltisi in primo grado con il rito abbreviato: rito che — sia pure, oggi, per scelta esclusiva dell'imputato — implica una decisione fondata, in primis, sul materiale probatorio raccolto dalla parte che subisce la limitazione denunciata, fuori delle garanzie del contraddittorio» (cfr. ordinanze n. 165 del 2003, n. 347 del 2002 e n. 421 del 2001); 
che ad avviso del giudice a quo, tale giustificazione potrebbe peraltro valere solo in rapporto all'appello principale, ma non anche con riguardo all'appello incidentale, dato che quest'ultimo non comporterebbe alcuna attività processuale ulteriore rispetto a quella già richiesta dall'impugnazione principale dell'imputato, che ne costituisce «necessario presupposto e costante limite»; 
che, in senso contrario, va tuttavia rilevato come le sezioni unite della Corte di cassazione — nel dirimere, con la sentenza 18 giugno-23 luglio 1993, n. 7247 (citata dallo stesso rimettente), il contrasto di giurisprudenza insorto, dopo l'entrata in vigore del nuovo codice di rito,  circa l'estensione della preclusione posta dalla norma in esame anche all'appello incidentale — abbiano specificamente rimarcato che l'esclusione dell'impugnazione incidentale della parte pubblica non può considerarsi «inutile», sul piano dell'economia processuale: giacché, anche in presenza dell'appello principale dell'imputato, «essa serve comunque a ridurre le questioni di merito deducibili nei confronti della sentenza emessa nel giudizio abbreviato e di riflesso a ridurre anche le occasioni di ricorso per cassazione»; 
che tale asserto appare tanto più valido ove, da un lato, si ritenga — in conformità ad un diffuso (anche se non incontrastato) orientamento interpretativo — che i limiti oggettivi dell'appello incidentale sono segnati non dai punti, ma dai capi della decisione investiti dall'appello principale (onde, nell'ambito del medesimo capo, l'impugnazione incidentale potrebbe avere ad oggetto anche punti diversi da quelli su cui verte l'impugnazione avversaria); ed ove, da un altro lato, si consideri che, specie in tale ottica, la proposizione dell'appello incidentale può fornire ulteriori occasioni per l'esercizio del potere officioso di rinnovazione dell'istruzione dibattimentale, da parte del giudice di appello: potere che la giurisprudenza di legittimità ritiene configurabile anche nel caso di processo celebrato in primo grado nelle forme del rito abbreviato; 
che, peraltro, anche a voler prescindere da quanto precede, resta comunque dirimente il rilievo che — dopo il ricordato intervento della Corte di cassazione a sezioni unite, alla stregua di un indirizzo giurisprudenziale costante, sostanzialmente condiviso dallo stesso giudice a quo — il principio generale, a livello di sistema delle impugnazioni, è quello della non spettanza del potere di appello incidentale alla parte che è priva del potere di proporre l'appello principale: e ciò in quanto l'appello incidentale — per denominazione, collocazione e disciplina specifica — non può essere considerato come un mezzo di impugnazione distinto ed autonomo rispetto all'appello, ma costituisce, al contrario, solo una particolare “espressione” di tale mezzo; 
che, in tale prospettiva, l'intervento invocato dal giudice a quo — lungi dal rappresentare una soluzione costituzionalmente obbligata, in un'ottica di “riequilibrio” della denunciata posizione di “minorità” del pubblico ministero nel giudizio abbreviato — implicherebbe la creazione di una disciplina manifestamente eccentrica rispetto alle linee-guida del sistema; si chiede infatti, in sostanza, a questa Corte di “costruire” un potere di appello incidentale svincolato, una tantum, dal potere di appello principale: un potere a fronte del cui riconoscimento il pubblico ministero che ritenesse “ingiusta”, per ragioni non deducibili con ricorso per cassazione, la sentenza di condanna emessa a seguito di giudizio abbreviato, sarebbe abilitato a dolersene — conseguendo eventualmente una reformatio in peius — unicamente nel caso in cui essa venga appellata dalla controparte e, al più, nei limiti del capo attinto dal gravame di quest'ultima; 
che un simile potere di impugnazione della pubblica accusa, quindi, più che realizzare un sia pur limitato recupero della parità tra i poteri processuali delle parti, si esaurirebbe in un mero meccanismo di “dissuasione” dell'imputato dall'esercizio del contrapposto potere di appello principale, segnatamente in un'ottica di prevenzione di utilizzazioni strumentali e dilatorie dello stesso: funzione che, peraltro, questa Corte ha già affermato concretare un semplice «effetto collaterale e non necessario dell'istituto dell'appello incidentale del pubblico ministero», e non già la sua ratio essendi (cfr. sentenza n. 280 del 1995); 
che la questione va dichiarata, pertanto, manifestamente infondata. 
Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 443, comma 3, e 595 del codice di procedura penale, sollevata, in riferimento all'art. 111, secondo comma, della Costituzione, dalla Corte di appello di Venezia con l'ordinanza in epigrafe. &#13;
</p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte Costituzionale, Palazzo della Consulta, il 20 gennaio 2004. &#13;
F.to: &#13;
Gustavo ZAGREBELSKY, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 27 gennaio 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
