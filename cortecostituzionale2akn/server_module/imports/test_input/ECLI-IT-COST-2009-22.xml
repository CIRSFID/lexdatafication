<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/22/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/22/"/>
          <FRBRalias value="ECLI:IT:COST:2009:22" name="ECLI"/>
          <FRBRdate date="26/01/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="22"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/22/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/22/ita@/!main"/>
          <FRBRdate date="26/01/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/2009/22/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/2009/22/ita@.xml"/>
          <FRBRdate date="26/01/2009" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="30/01/2009" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2009</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>O</cc:tipologia_pronuncia>
        <cc:presidente>FLICK</cc:presidente>
        <cc:relatore_pronuncia>Ugo De Siervo</cc:relatore_pronuncia>
        <cc:data_decisione>26/01/2009</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>ORDINANZA</docType>nel giudizio di legittimità costituzionale dell'articolo 3, comma 3, del decreto-legge 22 febbraio 2002, n. 12 (Disposizioni urgenti per il completamento delle operazioni di emersione di attività detenute all'estero e di lavoro irregolare), convertito con modificazioni in legge 23 aprile 2002, n. 73; dell'articolo 2 e, in via subordinata, dell'articolo 7, comma 4, del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della L. 30 dicembre 1991, n. 413), promosso con ordinanza dell'11 luglio 2006 dalla Commissione tributaria provinciale di Venezia sul ricorso proposto dalla Patrizia s.a.s. di Consalvi Patrizia &amp;amp; C. contro l'Agenzia delle Entrate – Ufficio di Venezia 1, iscritta al n. 201 del registro ordinanze 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 28, prima serie speciale, dell'anno 2008. 
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
      udito nella camera di consiglio del 17 dicembre 2008 il Giudice relatore Ugo De Siervo. 
      Ritenuto che, con ordinanza in data 8 luglio 2006, la Commissione tributaria provinciale di Venezia ha sollevato questione di legittimità costituzionale dell'art. 3, comma 3, del decreto-legge 22 febbraio 2002, n. 12 (Disposizioni urgenti per il completamento delle operazioni di emersione di attività detenute all'estero e di lavoro irregolare), convertito in legge dall'art. 1 della legge 23 aprile 2002, n. 73, e dell'art. 2 del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della L. 30 dicembre 1991, n. 413), in relazione agli artt. 102 e 103 della Costituzione, nonché alla IV [recte: VI] disposizione transitoria e finale della Costituzione medesima; 
      che, in subordine, la Commissione tributaria ha eccepito l'illegittimità costituzionale dell'art. 7, comma 4, d.lgs. n. 546 del 1992 per contrasto con gli artt. 3 e 24 della Costituzione; 
      che il rimettente – chiamato a decidere sul ricorso proposto da una società contro l'Agenzia delle entrate, Ufficio di Venezia, in relazione all'atto di irrogazione della sanzione per l'utilizzo di una lavoratrice non risultante dalle scritture o da altra documentazione obbligatoria – dubita della conformità a Costituzione delle norme che attribuiscono alla sua giurisdizione la controversia in esame; 
      che il giudice a quo ricorda che, secondo il diritto vivente espresso dalla Corte di cassazione, tale giurisdizione si radicherebbe in forza, da un lato, dell'art. 3 del decreto-legge n. 12 del 2002, il quale attribuirebbe agli uffici finanziari il compito di irrogare le sanzioni in materia di lavoro irregolare, e, dall'altro lato, in forza dell'art. 2 del d.lgs. n. 546 del 1992 il quale devolve alla giurisdizione tributaria le controversie concernenti le sanzioni amministrative «comunque irrogate da uffici finanziari»; 
      che la Commissione tributaria, pur condividendo tale esito interpretativo, ritiene che le disposizioni censurate contrasterebbero con i parametri sopra evocati; 
      che, ad avviso del rimettente, l'art. 3, comma 3, del decreto-legge n. 12 del 2002, nella parte in cui richiamando l'art. 18 del d.lgs. 18 dicembre 1997, n. 472 (Disposizioni generali in materia di sanzioni amministrative per le violazioni di norme tributarie, a norma dell'articolo 3, comma 133, della L. 23 dicembre 1996, n. 662), – il quale prevede che contro l'atto di irrogazione è ammesso ricorso alle commissioni tributarie – e l'art. 2 del d.lgs. n. 546 del 1992, nella parte in cui attribuisce al giudice tributario la giurisdizione sulle sanzioni per lavoro irregolare, determinerebbero un indebito ampliamento di una giurisdizione speciale a materie diverse da quelle tributarie, presentando le controversie concernenti le sanzioni per il lavoro irregolare natura sostanzialmente giuslavoristica o previdenziale; 
      che tale estensione della giurisdizione tributaria contrasterebbe con l'art. 102 Cost. e il principio della tendenziale unicità della giurisdizione, nonché con quello, correlato, del divieto di istituzione di giudici speciali; 
      che, in via subordinata, per il caso in cui la Corte ritenga infondata la suddetta eccezione di costituzionalità, il rimettente censura l'art. 7 del d.lgs. n. 546 del 1992 nella parte in cui vieta, nell'ambito del processo tributario, il ricorso alla prova testimoniale; 
      che proprio la natura sostanzialmente non tributaria delle controversie in esame determinerebbe un'evidente disparità di trattamento tra il normale processo del lavoro avanti al giudice ordinario, nel quale tale prova è ammessa e addirittura svincolata dal principio dispositivo, e quello avanti alle commissioni tributarie in cui non trova accesso; 
      che anche il diritto di difesa sarebbe irragionevolmente compresso dal momento che tale mezzo istruttorio sarebbe l'unico in grado di consentire l'accertamento dell'esistenza o meno di un rapporto di lavoro irregolare; 
      che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha chiesto, innanzitutto, che sia disposta la restituzione degli atti al giudice rimettente in considerazione delle sopravvenute modifiche all'art. 3 del decreto-legge n. 12 del 2002 introdotte dal decreto-legge 4 luglio 2006, n. 223 (Disposizioni urgenti per il rilancio economico e sociale, per il contenimento e la razionalizzazione della spesa pubblica, nonché interventi in materia di entrate e di contrasto all'evasione fiscale), convertito in legge, con modificazioni, dall'art. 1, della legge 4 agosto 2006, n. 248, il quale, all'art. 36-bis, comma 7, ha stabilito che alla irrogazione delle sanzioni per l'utilizzo di lavoratori irregolari provveda, non più l'Agenzia delle entrate, ma la Direzione provinciale del lavoro territorialmente competente; 
      che, nel merito, le questioni sarebbero infondate dal momento che le controversie in questione  rivestirebbero natura anche tributaria, in quanto le sanzioni previste dall'art. 3 del decreto-legge n. 12 del 2002 sarebbero volte alla emersione del lavoro sommerso il quale costituirebbe «manifestazione di reddito rilevante dal punto di vista tributario ai fini impositivi»; 
      che analoga conclusione varrebbe in relazione alla censura relativa all'art. 7 del d.lgs. n. 546 del 1992 poiché, per un verso, la natura tributaria di tali controversie determinerebbe inevitabilmente l'esclusione della prova testimoniale, per altro verso la Corte si sarebbe già pronunciata sulla questione dichiarandola infondata. 
      Considerato che la Commissione tributaria provinciale di Venezia ha sollevato questione di legittimità costituzionale dell'art. 3, comma 3, del decreto-legge 22 febbraio 2002, n. 12 (Disposizioni urgenti per il completamento delle operazioni di emersione di attività detenute all'estero e di lavoro irregolare), convertito in legge dall'art. 1 della legge 23 aprile 2002, n. 73, e dell'art. 2 del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della L. 30 dicembre 1991, n. 413), in relazione agli artt. 102 e 103 della Costituzione, nonché alla IV [recte: VI] disposizione transitoria e finale della Costituzione medesima; 
      che, secondo il rimettente, le disposizioni censurate determinerebbero un indebito ampliamento della giurisdizione tributaria, presentando le controversie concernenti le sanzioni per il lavoro irregolare natura sostanzialmente giuslavoristica o previdenziale; 
      che, in via subordinata, il medesimo giudice ha eccepito l'illegittimità costituzionale dell'art. 7, comma 4, del d.lgs. n. 546 del 1992 per contrasto con gli artt. 3 e 24 della Costituzione, nella parte in cui vieta, nel processo tributario, il ricorso alla prova testimoniale; 
      che, successivamente all'ordinanza di rimessione, questa Corte, con la sentenza n. 130 del 2008, ha dichiarato costituzionalmente illegittimo l'art. 2, comma 1, del d.lgs. n. 546 del 1992, nella parte in cui attribuisce alla giurisdizione tributaria le controversie relative alle sanzioni comunque irrogate da uffici finanziari, anche laddove esse conseguano alla violazione di disposizioni non aventi natura tributaria; 
      che tale pronuncia ha investito, dunque, proprio la disposizione censurata in via principale dall'odierno rimettente; 
      che la questione avente ad oggetto l'art. 7, comma 4, del d.lgs. n. 546 del 1992, è stata sollevata dal giudice a quo solo in via subordinata, e precisamente solo per il caso in cui non fosse stata accolta la questione prospettata in via principale, di tal che, alla luce della citata decisione, su di essa la Corte non è più chiamata a pronunciarsi; 
      che, pertanto, le odierne questioni di costituzionalità sono divenute prive di oggetto e devono essere dichiarate manifestamente inammissibili (ordinanza n. 269 del 2008). 
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </division>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
      dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 3, comma 3 del decreto-legge 22 febbraio 2002, n. 12 (Disposizioni urgenti per il completamento delle operazioni di emersione di attività detenute all'estero e di lavoro irregolare), convertito in legge dall'art. 1 della legge 23 aprile 2002, n. 73 e degli artt. 2 e 7, comma 4, del decreto legislativo 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della L. 30 dicembre 1991, n. 413), sollevate, in riferimento agli artt. 3, 24, 102 e 103 della Costituzione, nonché alla VI disposizione transitoria e finale della Costituzione medesima, dalla Commissione tributaria provinciale di Venezia con l'ordinanza in epigrafe. &#13;
      </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 26 gennaio 2009.  &#13;
F.to:  &#13;
Giovanni Maria FLICK, Presidente  &#13;
Ugo DE SIERVO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 30 gennaio 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
