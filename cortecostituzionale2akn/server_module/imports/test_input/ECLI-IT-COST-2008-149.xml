<akomaNtoso xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0" xmlns:fao="http://www.fao.org/ns/akn30" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:cc="http://cortecostituzionale.it/metadata">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfid">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/149/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/149/"/>
          <FRBRalias value="ECLI:IT:COST:2008:149" name="ECLI"/>
          <FRBRdate date="07/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="149"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/149/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/149/ita@/!main"/>
          <FRBRdate date="07/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/2008/149/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/2008/149/ita@.xml"/>
          <FRBRdate date="07/05/2008" name=""/>
          <FRBRauthor href="#corteCostituzionale"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="16/05/2008" by="#corteCostituzionale" refersTo="#dataDeposito"/>
      </workflow>
      <references source="#cirsfid">
        <original href="" showAs=""/>
        <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani" showAs="Monica Palmirani"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of Document"/>
        <TLCOrganization eId="corteCostituzionale" href="/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid" showAs="CIRSFID"/>
      </references>
      <proprietary source="#cirsfid">
        <cc:anno_pronuncia>2008</cc:anno_pronuncia>
        <cc:tipologia_pronuncia>S</cc:tipologia_pronuncia>
        <cc:presidente>BILE</cc:presidente>
        <cc:relatore_pronuncia>Giovanni Maria Flick</cc:relatore_pronuncia>
        <cc:data_decisione>07/05/2008</cc:data_decisione>
      </proprietary>
    </meta>
    <header>
      <block name=""><courtType>LA CORTE COSTITUZIONALE</courtType>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <division>
          <content>
            <p>ha pronunciato la seguente<docType>SENTENZA</docType>nel giudizio di legittimità costituzionale dell'art. 266, comma 2, del codice di procedura penale, promosso con ordinanza del 25 gennaio 2006 dal Tribunale di Varese nel procedimento penale a carico di A. G., ed altri, iscritta al n. 168 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 23, prima serie speciale, dell'anno 2006. 
    Udito nella camera di consiglio del 16 aprile 2008 il Giudice relatore Giovanni Maria Flick.</p>
          </content>
        </division>
      </introduction>
      <motivation>
        <division>
          <content>
            <p>Ritenuto in fattoCon l'ordinanza indicata in epigrafe, il Tribunale di Varese ha sollevato, in riferimento agli artt. 13, primo e secondo comma, 14, primo e secondo comma, e 15 della Costituzione, questione di legittimità costituzionale dell'art. 266, comma 2, del codice di procedura penale, nella parte in cui non estende la disciplina delle intercettazioni di comunicazioni tra presenti «a qualsiasi “captazione di immagini in luoghi di privata dimora”», ancorché «non configurabile in concreto come forma di intercettazione di comunicazioni tra presenti». 
    Il rimettente riferisce di essere investito del processo penale nei confronti di tre persone, sottoposte a giudizio per una pluralità di fatti di illecito acquisto o cessione di sostanze stupefacenti: fatti che – secondo l'ipotesi accusatoria – si erano svolti, in buona parte, nell'abitazione di uno degli imputati, la quale ne era stata luogo di consumazione o nella quale era avvenuto, quantomeno, il prelevamento della sostanza stupefacente o la consegna del relativo prezzo. 
    A sostegno dell'accusa, il pubblico ministero aveva chiesto ed ottenuto l'acquisizione dei risultati di riprese visive, effettuate tramite una videocamera collocata su un edificio adiacente la predetta abitazione e puntata sul davanzale di una finestra della medesima: luogo, quest'ultimo – osserva il rimettente – da ritenere certamente riconducibile alle nozioni di «domicilio» e di «privata dimora». 
    In sede di discussione finale, nel valutare «a scopi decisori l'utilizzabilità degli atti», era peraltro emerso che le riprese in questione erano state eseguite dalla polizia giudiziaria senza alcun provvedimento autorizzativo, né del giudice per le indagini preliminari, né del pubblico ministero. 
    Ciò premesso in punto di fatto, il giudice a quo si dichiara consapevole della circostanza che questa Corte, con sentenza n. 135 del 2002, ha ritenuto non fondata analoga questione di legittimità costituzionale dell'art. 266, comma 2, cod. proc. pen., volta ad ottenere «una pronuncia additiva che allinei la disciplina processuale delle riprese visive in luoghi di privata dimora a quella delle intercettazioni di comunicazioni fra presenti nei medesimi luoghi». Ad avviso del rimettente, nondimeno, il persistente «vuoto normativo» in materia determinerebbe – anche alla luce dei più recenti orientamenti della giurisprudenza di legittimità – la lesione dei parametri costituzionali evocati, giustificando un nuovo scrutinio. 
    Al riguardo, il giudice a quo rileva come la Corte di cassazione abbia richiamato i dicta della citata sentenza n. 135 del 2002, ed abbia perciò affermato che le riprese visive in luoghi di privata dimora sono soggette alla disciplina delle cosiddette intercettazioni ambientali – e quindi alla preventiva autorizzazione del giudice per le indagini preliminari – solo quando mirino alla captazione di comportamenti a carattere comunicativo. Invece, negli altri casi la captazione di immagini configurerebbe una prova documentale non espressamente regolata dalla legge, «fermo […] il limite della tutela della libertà domiciliare di cui all'art. 14 Cost. da valutarsi di volta in volta». 
    Tale limite ulteriore, atto a condizionare «di volta in volta» l'utilizzabilità delle captazioni, non risulterebbe, tuttavia – a parere del rimettente – in alcun modo definito dalla legge; infatti, nel vigente panorama normativo, non v'è alcuna disposizione che vieti o che regoli l'attività investigativa in questione. Divieti e norme regolatrici non potrebbero rinvenirsi, in particolare, nel d.lgs. 30 giugno 2003, n. 196 (Codice in materia di protezione dei dati personali) o nel provvedimento generale sulla videosorveglianza del 29 aprile 2004, emanato dal Garante per la protezione dei dati personali; quest'ultimo, infatti, si limita ad affermare la necessità del rispetto delle norme penali che vietano le intercettazioni di comunicazioni e conversazioni. D'altro canto, non potrebbe ritenersi esistente un «confine definito e rigido» tra le intercettazioni disciplinate dal codice di procedura penale e le altre captazioni investigative penalmente illecite ai sensi dell'art. 615-bis del codice penale. 
    L'evidenziata assenza di disciplina si risolverebbe, quindi, in un vulnus delle norme costituzionali poste a tutela della libertà personale, dell'inviolabilità del domicilio e della libertà di comunicazione. Si determinerebbe, fra l'altro, una inversione della «sequenza di garanzia»: nel senso che – alla luce degli orientamenti giurisprudenziali dianzi ricordati – il pubblico ministero e la stessa polizia giudiziaria potrebbero captare immagini all'interno di luoghi di privata dimora, senza alcuna autorizzazione giurisdizionale; se poi risultassero essere state captate delle «comunicazioni», gli esiti del mezzo investigativo sarrebbero inutilizzabili, altrimenti si sarebbero ottenute prove documentali utilizzabili. 
    Il dubbio di costituzionalità non sarebbe, d'altro canto, superabile in via interpretativa: e ciò perché non si potrebbe ritenere – anche alla luce delle indicazioni della sentenza n. 135 del 2002 – che qualsiasi captazione di immagini in luogo di privata dimora rientri nella previsione dell'art. 266 cod. proc. pen., solo perché potenzialmente acquisitiva di comunicazioni tra presenti. Neppure, però, potrebbe accettarsi che – nella perdurante inerzia del legislatore – l'individuazione dei confini di legittimità delle molteplici forme di intrusione nei luoghi di privata dimora resti affidata «alla mera interpretazione giurisprudenziale»: e ciò soprattutto ove si tenga conto della rapida evoluzione tecnologica, che rende aggredibile il domicilio con strumenti sempre più sofisticati, quali le immagini satellitari ad elevatissimo livello di definizione o la termografia a raggi infrarossi. 
    Emergerebbe, di conseguenza, la necessità «minima» di assoggettare la captazione di immagini in luoghi di privata dimora ad un provvedimento autorizzativo giurisdizionale; anzi, più in particolare – come per le intercettazioni «tradizionali» – ad un provvedimento autorizzativo del giudice per le indagini preliminari. 
    In tale ottica, il rimettente chiede dunque alla Corte una sentenza additiva, che estenda la disciplina dell'art. 266, comma 2, cod. proc. pen. – dettata per le intercettazioni di comunicazioni tra presenti – a qualsiasi captazione di immagini in luoghi di privata dimora, anche se non configurabile in concreto come forma di intercettazione di comunicazioni tra presenti. 
    La questione risulterebbe, infine, rilevante nel giudizio a quo, in quanto dal suo esito dipenderebbe l'utilizzabilità o meno delle videoregistrazioni prodotte dal pubblico ministero a sostegno dell'accusa.Considerato in diritto1. – Il Tribunale di Varese dubita della legittimità costituzionale, in riferimento agli artt. 13, primo e secondo comma, 14, primo e secondo comma, e 15 della Costituzione, dell'art. 266, comma 2, del codice di procedura penale, nella parte in cui non estende la disciplina delle intercettazioni di comunicazioni tra presenti a qualsiasi ripresa visiva effettuata in luoghi di privata dimora, ancorché le immagini captate non abbiano ad oggetto comportamenti di tipo comunicativo. 
    Il giudice rimettente rileva come, anche alla luce di quanto affermato da questa Corte nella sentenza n. 135 del 2002, la disciplina delle cosiddette intercettazioni ambientali, di cui al citato art. 266, comma 2, cod. proc. pen. – che prevede l'autorizzazione del giudice per le indagini preliminari – sia applicabile alle videoregistrazioni solo quando le stesse mirino a documentare comportamenti a carattere comunicativo. Fuori di tale ipotesi, si riscontrerebbe, per contro, un totale difetto di regolamentazione, poiché non è rinvenibile, nel vigente panorama normativo, alcuna disposizione che vieti o che disciplini l'attività investigativa in questione. 
    Siffatto «vuoto normativo» comporterebbe la lesione delle norme costituzionali poste a tutela della libertà personale, dell'inviolabilità del domicilio e della libertà di comunicazione. Esso consentirebbe, difatti, non solo al pubblico ministero, ma alla stessa polizia giudiziaria di effettuare riprese visive in luoghi di privata dimora senza alcuna autorizzazione giurisdizionale, con la riserva di valutarne gli esiti a posteriori: nel senso che, ove si fossero filmati comportamenti di tipo comunicativo, le registrazioni rimarrebbero inutilizzabili; mentre, nell'ipotesi opposta (verificatasi nel giudizio a quo) si sarebbero ottenuti elementi probatori suscettibili di utilizzazione. 
    L'esigenza di sottoporre ad un provvedimento autorizzativo giurisdizionale anche le riprese visive di comportamenti non comunicativi in luoghi di privata dimora – esigenza tanto più avvertibile a fronte del progresso tecnologico, che accresce sempre più le possibilità di inspicere nel domicilio tramite strumenti altamente sofisticati – imporrebbe, dunque, la pronuncia additiva invocata. 
    2. – La questione è inammissibile. 
    2.1. – Nel motivare la rilevanza, il giudice a quo riferisce che, nel caso di specie, le riprese sono state eseguite con una videocamera, collocata su un edificio adiacente l'abitazione dell'indagato e puntata sul davanzale di una finestra dell'abitazione stessa. 
    Tale circostanza pone un problema di effettiva configurabilità della protezione costituzionale del domicilio (sulla cui asserita compromissione si incentrano le doglianze del rimettente, al di là della pluralità dei parametri costituzionali evocati): problema, peraltro, specificamente evidenziato nel giudizio a quo dal pubblico ministero. Quest'ultimo – secondo quanto riferisce l'ordinanza di rimessione – ha dedotto, di fronte alle eccezioni della difesa, che le videoregistrazioni in discussione equivarrebbero ad una osservazione a distanza, svolta da un operatore di polizia giudiziaria; e che nessun «attentato al domicilio» sarebbe comunque ravvisabile, proprio perché le riprese sono state eseguite dall'esterno. 
    A tali considerazioni il rimettente si limita ad opporre che il davanzale della finestra di un'abitazione è un «punto» certamente riconducibile alle nozioni di «domicilio» e di «privata dimora»: constatazione, tuttavia, insufficiente a fondare un giudizio di rilevanza della questione. 
    In proposito, si deve difatti osservare che l'art. 14 Cost. tutela il domicilio sotto due distinti aspetti: come diritto di ammettere o escludere altre persone da determinati luoghi, in cui si svolge la vita intima di ciascun individuo; e come diritto alla riservatezza su quanto si compie nei medesimi luoghi. Nel caso delle riprese visive, il limite costituzionale del rispetto dell'inviolabilità del domicilio viene in rilievo precipuamente sotto il secondo aspetto: ossia non tanto – o non solo – come difesa rispetto ad una intrusione di tipo fisico; quanto piuttosto come presidio di un'intangibile sfera di riservatezza, che può essere lesa – attraverso l'uso di strumenti tecnici – anche senza la necessità di un'intrusione fisica. 
    Ne consegue logicamente che, affinché scatti la protezione dell'art. 14 Cost., non basta che un certo comportamento venga tenuto in luoghi di privata dimora; ma occorre, altresì, che esso avvenga in condizioni tali da renderlo tendenzialmente non visibile ai terzi. Per contro, se l'azione – pur svolgendosi in luoghi di privata dimora – può essere liberamente osservata dagli estranei, senza ricorrere a particolari accorgimenti (paradigmatico il caso di chi si ponga su un balcone prospiciente la pubblica via), il titolare del domicilio non può evidentemente accampare una pretesa alla riservatezza; e le videoregistrazioni a fini investigativi non possono, di conseguenza, che soggiacere al medesimo regime valevole per le riprese visive in luoghi pubblici o aperti al pubblico. In una simile ipotesi, difatti, le videoregistrazioni non differiscono, sostanzialmente, dalla documentazione filmata di un'operazione di osservazione o di appostamento, che ufficiali o agenti di polizia giudiziaria potrebbero compiere collocandosi, di persona, al di fuori dell'abitazione. 
    In sostanza, il limite dell'art. 14 Cost. può venire in considerazione, rispetto alle riprese visive (come nel caso di specie), in quanto, per eseguire i filmati all'interno del domicilio, gli organi investigativi debbano superare – tramite opportune manovre o avvalendosi di speciali strumenti – una barriera che si frappone tra la generalità dei consociati e l'attività filmata. Se quest'ultima è accessibile visivamente da chiunque, si è fuori dall'area di tutela prefigurata dalla norma costituzionale de qua. 
    In tale prospettiva, la descrizione della fattispecie concreta fornita dal giudice rimettente risulta dunque inadeguata. Il giudice a quo non specifica, difatti, né quali immagini siano state concretamente filmate; né come la ripresa sia avvenuta: se con l'impiego o meno, cioè, di particolari accorgimenti tecnici atti a permettere il visus in punti dell'abitazione ordinariamente sottratti agli sguardi dei terzi. E così, ad esempio, una cosa è che la videocamera esterna fosse atta a riprendere solo persone affacciate sul davanzale della finestra dell'abitazione dell'imputato, visibili liberamente dai dirimpettai; altra cosa è che l'apparato di ripresa visiva – per le sue caratteristiche tecniche, o anche solo per la sua particolare collocazione – permettesse di riprendere comportamenti sottratti alla normale osservazione ab externo. 
    2.2. – Sotto diverso profilo, il giudice a quo ritiene che – in assenza di un espresso divieto o di una esplicita regolamentazione, da parte della legge ordinaria, delle riprese visive di comportamenti di tipo non comunicativo all'interno del domicilio – tale attività investigativa sarebbe esperibile anche ad iniziativa della polizia giudiziaria; con connessa utilizzabilità processuale dei relativi risultati: esito, quest'ultimo, che il rimettente intende rimuovere attraverso la sentenza additiva richiesta. 
    Il rimettente non prende, però, affatto in esame – anche solo per escluderne, eventualmente, la praticabilità – la soluzione interpretativa esattamente opposta. Secondo quest'ultima, in mancanza di una norma che consenta e disciplini il compimento dell'attività in parola – soddisfacendo la doppia riserva, di legge (quanto ai «casi» e ai «modi») e di giurisdizione, cui l'art. 14, secondo comma, Cost. subordina l'eseguibilità di atti investigativi nel domicilio – l'attività stessa dovrebbe ritenersi radicalmente vietata, proprio perché lesiva dell'inviolabilità del domicilio, sancita dal primo comma dello stesso art. 14 Cost.; mentre i risultati delle riprese effettuate in violazione del divieto rimarrebbero inutilizzabili. Si tratta, in effetti, di una soluzione interpretativa già sostenuta da una parte della giurisprudenza di legittimità, tanto prima che dopo la sentenza di questa Corte n. 135 del 2002, citata dal giudice a quo; e che, successivamente all'ordinanza di rimessione, è stata altresì accolta dalle sezioni unite della Corte di cassazione (sentenza 26 marzo 2006-28 luglio 2006, n. 26795). 
    Questa Corte ha già avuto modo di affermare, del resto, con particolare riferimento agli atti limitativi della libertà e segretezza delle comunicazioni, che le «attività compiute in dispregio dei fondamentali diritti del cittadino non possono essere assunte di per sé a giustificazione e fondamento di atti processuali a carico di chi quelle attività costituzionalmente illegittime abbia subito» (sentenza n. 34 del 1973; si veda anche la sentenza n. 81 del 1993; nonché, con riferimento al sequestro di scritti predisposti dall'imputato unicamente per facilitare la difesa nel corso dell'interrogatorio, la sentenza n. 229 del 1998). 
    È evidente, d'altro lato, che l'adozione della diversa soluzione interpretativa dianzi indicata renderebbe il quesito di costituzionalità irrilevante nel giudizio a quo, giacché – ove le riprese visive di cui si discute, eseguite ad iniziativa della polizia giudiziaria, risultassero, in concreto, lesive dell'inviolabilità del domicilio – esse sarebbero già ora inutilizzabili, alla stregua di detta interpretazione. 
    3. – La questione va dichiarata pertanto inammissibile, perché il giudice a quo non ha fornito una descrizione sufficiente della fattispecie concreta; ed ha, altresì, omesso di verificare la praticabilità di una diversa interpretazione del quadro normativo, tale da superare i dubbi di costituzionalità o da renderli comunque irrilevanti nel caso di specie (a quest'ultimo riguardo, ex plurimis, sentenza n. 192 del 2007; ordinanza n. 409 del 2007). E ciò a prescindere da ogni rilievo circa la conferenza dei parametri di cui agli artt. 13, primo e secondo comma, e 15 Cost., pure evocati dal rimettente; nonché a prescindere da ogni considerazione in ordine al merito della questione. Rispetto a quest'ultimo, infatti, non potrebbe che valere quanto già affermato da questa Corte con la sentenza n. 135 del 2002: sentenza che il rimettente richiama, ma della quale non esamina l'argomento fondante, rappresentato dalla eterogeneità dei due diritti fondamentali posti a confronto – libertà e segretezza delle comunicazioni; inviolabilità del domicilio – e dalla conseguente impossibilità, per la Corte, di colmare il denunciato «vuoto normativo» tramite l'auspicata estensione della disciplina delle intercettazioni ambientali alle videoriprese domiciliari.</p>
          </content>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi &#13;
 LA CORTE
                COSTITUZIONALE <eol/></block>
        <division>
          <content>
            <p> &#13;
     dichiara l'inammissibilità della questione di legittimità costituzionale dell'art. 266, comma 2, del codice di procedura penale, sollevata, in riferimento agli artt. 13, primo e secondo comma, 14, primo e secondo comma, e 15 della Costituzione, dal Tribunale di Varese con l'ordinanza indicata in epigrafe. &#13;
    </p>
          </content>
        </division>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name=""> Così deciso  in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 maggio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 16 maggio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
