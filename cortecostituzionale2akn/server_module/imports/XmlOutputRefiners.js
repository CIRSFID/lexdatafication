import xpath from 'xpath';
import fs from 'fs';
import xmldom from 'xmldom';
import config from '../config/config.json'
import _ from 'underscore';
import camelCase from 'camelcase';
import { createAkomando } from 'akomando';
import xmlFormatter from 'xml-formatter';
import PersonPaser from '../parser/person/PersonParser';
import RoleParser from '../parser/role/RoleParser';
import LocationParser from '../parser/location/LocationParser';
import DateParser from '../parser/date/DateParser';
import MotivationFormulaParser from '../parser/motivation_formula/MotivationFormulaParser';
import BackgroundFormulaParser from '../parser/background_formula/BackgroundFormulaParser';
import ReferenceParser from '../parser/reference/ReferenceParser';

let selectObj = xpath.useNamespaces({ akn: config.aknNamespace });

const parseHeading = (xmlStringDoc) => {
    let xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');    
    let headerNode = selectObj("//akn:header", xmlDoc);    
    let hederStringNode = new xmldom.XMLSerializer().serializeToString(headerNode[0]);            
    if (!headerNode[0]) { return xmlStringDoc; }
    let pp = new PersonPaser('it_IT');    
    let rp = new RoleParser('it_IT');
    //parse persons
    _.uniq(pp.getMatches(hederStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());
        hederStringNode = hederStringNode
            .split(match.fullString)
            .join('<person refersTo="#' + refAttr + '">' + match.fullString + '</person>');
    });
    //parse roles
    _.uniq(rp.getMatches(hederStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());        
        hederStringNode = hederStringNode
            .split(match.fullString)
            .join('<role refersTo="#' + refAttr + '">' + match.fullString + '</role>');
    });
    //remove namespace
    hederStringNode = hederStringNode.replace(' xmlns="' + config.aknNamespace + '"', '');
    let parsedNode = new xmldom.DOMParser().parseFromString(hederStringNode, 'text/xml');    
    headerNode[0].parentNode.replaceChild(parsedNode, headerNode[0]);        
    return new xmldom.XMLSerializer().serializeToString(xmlDoc);
}

const parseConclusions = (xmlStringDoc) => {
    let xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
    let conclusionsNode = selectObj("//akn:conclusions", xmlDoc);
    if (!conclusionsNode[0]){ return xmlStringDoc; }    
    let conclusionsStringNode = new xmldom.XMLSerializer().serializeToString(conclusionsNode[0]);
    let pp = new PersonPaser('it_IT');
    let rp = new RoleParser('it_IT');
    let lp = new LocationParser('it_IT');
    let dp = new DateParser('it_IT');
    //parse persons
    _.uniq(pp.getMatches(conclusionsStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());
        conclusionsStringNode = conclusionsStringNode
            .split(match.fullString)
            .join('<person refersTo="#' + refAttr + '">' + match.fullString + '</person>');
    });
    //parse roles
    _.uniq(rp.getMatches(conclusionsStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());
        conclusionsStringNode = conclusionsStringNode
            .split(match.fullString)
            .join('<role refersTo="#' + refAttr + '">' + match.fullString + '</role>');
    });
    //parse locations
    _.uniq(lp.getMatches(conclusionsStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());
        conclusionsStringNode = conclusionsStringNode
            .split(match.fullString)
            .join('<location refersTo="#' + refAttr + '">' + match.fullString + '</location>');
    });    
    //parse dates
    _.uniq(dp.getMatches(conclusionsStringNode), 'fullString').forEach(match => {        
        conclusionsStringNode = conclusionsStringNode
            .split(match.fullString)
            .join('<date date="' + match.isoDate + '">' + match.fullString + '</date>');
    });
    conclusionsStringNode = conclusionsStringNode.replace(' xmlns="' + config.aknNamespace + '"', '');
    let parsedNode = new xmldom.DOMParser().parseFromString(conclusionsStringNode, 'text/xml');        
    conclusionsNode[0].parentNode.replaceChild(parsedNode, conclusionsNode[0]);    
    return new xmldom.XMLSerializer().serializeToString(xmlDoc);
}

const evalIDAndMeta = (xmlStringDoc) => {
    let xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
    let roleNodes = selectObj("//akn:role", xmlDoc);
    let locationNodes = selectObj("//akn:location", xmlDoc);
    let personNodes = selectObj("//akn:person", xmlDoc);    
    let metaReferenceNode = selectObj("//akn:meta/akn:references", xmlDoc);    
    let rolesMeta = []; let locationsMeta = []; let personsMeta = [];
    //set roles eId
    _.each(roleNodes, (el, key) => {
        el.setAttribute('eId', 'role_' + key);
        rolesMeta.push({
            showAs: el.textContent,
            refersTo: el.getAttribute('refersTo'),
            href: '/akn/ontology/role/it' + el.getAttribute('refersTo').replace('#','/')
        });
    });
    //set locations eId
    _.each(locationNodes, (el, key) => {
        el.setAttribute('eId', 'location_' + key);
        locationsMeta.push({
            showAs: el.textContent,
            refersTo: el.getAttribute('refersTo'),
            href: '/akn/ontology/location/it' + el.getAttribute('refersTo').replace('#', '/')
        });
    });
    //set persons eId
    _.each(personNodes, (el, key) => {
        el.setAttribute('eId', 'person_' + key);
        personsMeta.push({
            showAs: el.textContent,
            refersTo: el.getAttribute('refersTo'),
            href: '/akn/ontology/person/it' + el.getAttribute('refersTo').replace('#', '/')
        });
    });    
    if(!metaReferenceNode[0]){ return new xmldom.XMLSerializer().serializeToString(xmlDoc); }    
    //set role reference metadata
    
    _.each(_.uniq(rolesMeta, 'showAs'), (el, key) => {
        let role = xmlDoc.createElement('TLCRole');
        role.setAttribute('eId', el.refersTo.replace('#',''))
        role.setAttribute('href', el.href);    
        role.setAttribute('showAs', el.showAs);
        metaReferenceNode[0].appendChild(role)
    });
    _.each(_.uniq(locationsMeta, 'showAs'), (el, key) => {
        let location = xmlDoc.createElement('TLCLocation');
        location.setAttribute('eId', el.refersTo.replace('#', ''))
        location.setAttribute('href', el.href);
        location.setAttribute('showAs', el.showAs);
        metaReferenceNode[0].appendChild(location)
    });
    //refersTo for uppercase name disambiguatoins
    _.each(_.uniq(personsMeta, 'refersTo'), (el, key) => {
        let person = xmlDoc.createElement('TLCPerson');
        person.setAttribute('eId', el.refersTo.replace('#', ''))
        person.setAttribute('href', el.href);
        person.setAttribute('showAs', el.showAs);
        metaReferenceNode[0].appendChild(person)
    });
    return new xmldom.XMLSerializer().serializeToString(xmlDoc);
}

const getMotivationBackgroundSections = (xmlStringDoc) => {
    let xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
    let motivationNodes = selectObj("//akn:motivation", xmlDoc); 
    if (!motivationNodes[0]) { return xmlStringDoc; }
    let textContent = motivationNodes[0].textContent;
    let bfp = new BackgroundFormulaParser('it_IT');        
    let mfp = new MotivationFormulaParser('it_IT');
    let backMatches = bfp.getMatches(textContent);
    let motMatches = mfp.getMatches(textContent);
    let backMatch, motMatch, backgroundText, motivationText;
    //can apply logic only if find both background and motivation
    if (typeof backMatches !== 'undefined' && backMatches.length > 0
        && typeof motMatches !== 'undefined' && motMatches.length > 0) {
        //get the first match if more than one
        backMatch = _.min(backMatches, _.property(['offset', 'start']));
        motMatch = _.min(motMatches, _.property(['offset', 'start']));
        //TO-DO: text outside background tag?
        //Check if the background is the first content in the node
        if (backMatch.offset.start !== 0){
            return new xmldom.XMLSerializer().serializeToString(xmlDoc);    
        }
        backgroundText = textContent.substring(backMatch.offset.start, motMatch.offset.start);
        motivationText = textContent.substring(motMatch.offset.start, textContent.length);
        //create background node
        let backgroundNode = xmlDoc.createElement('background');
        let backgroundDivisionNode = xmlDoc.createElement('division');
        let backgroundContentNode = xmlDoc.createElement('content');
        let backgroundPNode = xmlDoc.createElement('p');
        backgroundPNode.textContent = backgroundText;
        backgroundNode.appendChild(backgroundDivisionNode);
        backgroundDivisionNode.appendChild(backgroundContentNode);
        backgroundContentNode.appendChild(backgroundPNode);        
        //create motivation node
        let motivationNode = xmlDoc.createElement('motivation');
        let motivationDivisionNode = xmlDoc.createElement('division');
        let motivationContentNode = xmlDoc.createElement('content');
        let motivationPNode = xmlDoc.createElement('p');
        motivationPNode.textContent = motivationText;
        motivationNode.appendChild(motivationDivisionNode);
        motivationDivisionNode.appendChild(motivationContentNode);
        motivationContentNode.appendChild(motivationPNode);
        //append both nodes to parent in order backgroundNode first motivationNode after
        motivationNodes[0].parentNode.insertBefore(motivationNode, motivationNodes[0]);
        motivationNodes[0].parentNode.insertBefore(backgroundNode, motivationNode);
        //remove the motivationNodes[0] leaving the 2 nodes splitted by the parsing logic
        motivationNodes[0].parentNode.removeChild(motivationNodes[0]);
    } else {
        return new xmldom.XMLSerializer().serializeToString(xmlDoc);
    }
    return new xmldom.XMLSerializer().serializeToString(xmlDoc);        
}

const evalReference = (xmlStringDoc) => {    
    let xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
    let pNodes = selectObj("//akn:p", xmlDoc); 
    _.each(pNodes, (el) => {
        let pNodeToString = new xmldom.XMLSerializer().serializeToString(el);
        let pNodeToStringRef = createReferenceTagInString(pNodeToString);        
        try {
            const newNode = new xmldom.DOMParser().parseFromString(pNodeToStringRef, 'text/xml');
            newNode.firstChild.removeAttribute('xmlns'); //remove the namespace added by xmldom
            el.parentNode.replaceChild(newNode, el);
        } catch (err) {
            console.log(err);
        }        
    });
    return new xmldom.XMLSerializer().serializeToString(xmlDoc);      
}

const createReferenceTagInString = (stringToParse) => {
    let finalStr = '';
    let rp = new ReferenceParser('it_IT');
    let matches = rp.getWiderMatches(stringToParse);
    let lastOffset;
    //sort offsets    
    _.sortBy(matches, ['offset','start']);   
    matches = matches.reverse();
    //iterate matches to rebuild the string adding text before first match
    lastOffset = 0;
    //iterate for all mathces
    _.each(matches, (match) => {
        finalStr = finalStr + stringToParse.substring(lastOffset, match.offset.start);
        const normalizeRef = (refStr) => {
            refStr = refStr.toLowerCase();
            refStr = refStr.replace(/[^\w]/g, '.');
            return refStr;
        };
        const txtMatch = stringToParse.substring(match.offset.start, match.offset.end);
        let hrefStr = '/akn/it/act';
        let refFinalStr = '';
        //create reference tag
        if (match.docType) {
            hrefStr += `/${normalizeRef(match.docType)}`;
        }
        if (match.isoDate) {
            hrefStr += `/${match.isoDate}`;
        } else {
            hrefStr += '/xxxx-xx-xx';
        }
        if (match.docNum) {
            //TO-DO: Fix reference parser
            let refindedDocNum = normalizeRef(match.docNum);
            refindedDocNum = refindedDocNum.replace('n..', '');
            hrefStr += `/${refindedDocNum}`;
        }
        if (match.authority) {
            hrefStr += `/${normalizeRef(match.authority)}`;
        }
        hrefStr += '/!main';
        refFinalStr = `<ref href="${hrefStr}">${txtMatch}</ref>`;
        finalStr = finalStr + refFinalStr;
        lastOffset = match.offset.end;
    });    
    //add last part of the string
    finalStr = finalStr + stringToParse.substring(lastOffset, stringToParse.length);        
    return finalStr;
}

const refineOutput = (xmlStringDoc) => {
    //parse heading and conclusions evaluate id in xml string
    let refinedXmlString = xmlFormatter(evalIDAndMeta(evalReference(parseConclusions(parseHeading(getMotivationBackgroundSections(xmlStringDoc))))));
    //let refinedXmlString = xmlStringDoc;
    //akomando section
    let akomandoHandler = createAkomando({
        aknString: refinedXmlString,
    });
    let unusedRef = akomandoHandler.getReferencesInfo({ filterBy: 'unused'});  
    //Debug purpose only
    //console.log(unusedRef); 
    return refinedXmlString;
}


fs.readdirSync("test_input").forEach(file => {
    console.log(file);
    const rawString = fs.readFileSync("test_input/" + file, "utf8").toString();
    const converted = refineOutput(rawString);
    fs.writeFileSync("test_output/" + file, converted);
});

exports.refineOutput = refineOutput;