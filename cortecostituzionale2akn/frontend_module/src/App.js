import React, { Component } from 'react';
import xml2js from 'xml2js';
import logo from './logo.svg';
import logoUnibo from './logoUnibo.png';
import cirfidTestata from './cirsfid_testata.gif';
import axios, { post, get } from 'axios';
import { Image, Tab, Col, Row, Nav, NavItem } from 'react-bootstrap';
import FileDownload from 'react-file-download';
import { ToastContainer, toast } from 'react-toastify';
import JSZip from 'jszip';
import UploadContainer from './components/ui/Containers/UploadContainer.js';
import ResultContainer from './components/ui/Containers/ResultContainer.js';
import XMLViewerContainer from './components/ui/Containers/XMLViewerContainer.js';
import HomeTextContainer from './components/ui/Containers/HomeTextContainer.js';
import FooterContainer from './components/ui/Containers/FooterContainer.js';
import SpinnerLoader from './components/ui/SpinnerLoader/SpinnerLoader.js';
import FileSaver from 'file-saver';
import './App.css';

//ES6 Axios polyfill IExploer compatibility
require('es6-promise').polyfill()

const uploadApi = '/nir2akn/api/upload-and-convert';
const downloadApi = '/nir2akn/api/download';
const jsonConvertApi = 'convert';
const jsonUploadApi = 'upload-and-convert';
const bulkApi = 'bulk-act-address';
const docConversion = 'single-doc-convert';
const listFilesByTS = 'read-all-doc-by-ts';
const readDocByPath = 'read-doc-by-path';
const zipDownloadApi = 'get-zip-by-ts';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      resultData: [],
      isFetchingData: false,
      showResult: false,
      showViewer: false,
      xmlContent: '',
      modalContent: '',
      currentTsDir: null
    };

  }

  fileDownload(requestObj) {
    let url = downloadApi;
    this.setState({ isFetchingData: true })
    post(url,
      {
        fileName: requestObj.fileName,
        destDir: requestObj.destDir
      }).then((response) => {
        this.setState({ isFetchingData: false })
        FileDownload(response.data, requestObj.fileName);
      }).catch((err) => {
        this.setState({ isFetchingData: false })
        console.error(err)
      })
  }

  downloadZip() {
    let url = zipDownloadApi + '/' + this.state.currentTsDir;
    this.setState({ isFetchingData: true })
    axios({
      url: url,
      method: 'GET',
      responseType: 'blob', // important
    }).then((response) => {
        this.setState({ isFetchingData: false })         
        FileSaver.saveAs(new Blob([response.data]), "result.zip");
      }).catch((err) => {
        this.setState({ isFetchingData: false })
        console.error(err)
      })
  }

  str2bytes(str) {
    var bytes = new Uint8Array(str.length);
    for (var i = 0; i < str.length; i++) {
      bytes[i] = str.charCodeAt(i);
    }
    return bytes;
  }

  fileDirectDownload(docObj) {
    let url = readDocByPath;
    this.setState({ isFetchingData: true })
    post(url,
      {
        content: {
          path: docObj.xmlPath
        }
      }).then((response) => {
        let content = response.data;
        FileDownload(content, docObj.name + '.xml');    
      }).catch((err) => {
        this.setState({ isFetchingData: false });
      })
  }

  fileView(requestObj) {
    let url = downloadApi;
    this.setState({ isFetchingData: true })
    post(url,
      {
        fileName: requestObj.fileName,
        destDir: requestObj.destDir
      }).then((response) => {
        this.setState({ isFetchingData: false })
        this.setState({ xmlContent: response.data })
        this.setState({ showViewer: true });
      }).catch((err) => {
        this.setState({ isFetchingData: false })
        console.error(err)
      })
  }

  fileViewContent(filePath, mode) {
    let url = readDocByPath;
    this.setState({ isFetchingData: true })
    post(url,
      {
        content: {
          path: filePath
        }
      }).then((response) => {
        let content = response.data;
        this.setState({ isFetchingData: false })
        if (mode === 'xml') {
          this.setState({ xmlContent: content })
          this.setState({ showViewer: true });
        } else if (mode === 'vJson') {
          let customString = 'No validation errors';          
          let validationCnt = content;          
          if (!validationCnt.success) {
            customString = "-----------------------------------------\n";
            if (validationCnt.resError){
              customString += JSON.stringify(validationCnt.resError) + '\n';
              customString += "-----------------------------------------\n";
            }
            validationCnt.errors.forEach(element => {
              element.error.split(',').forEach(element => {
                customString += element + '\n';
              });
              customString += "-----------------------------------------\n";
            });
          }
          this.setState({ xmlContent: customString });
          this.setState({ showViewer: true });
        }
      }).catch((err) => {
        this.setState({ isFetchingData: false });      
      })
  }

  viewModalContent(content) {
    this.setState({ modalContent: content })
    this.setState({ showViewer: true });
  }

  downloadResultZip() {
    let zipFile = new JSZip();
    this.state.resultData.forEach((el) => {
      zipFile.file(el.fileName, el.xmlContent);
    });
    zipFile.generateAsync({ type: "blob" })
      .then(function (content) {
        // see FileSaver.js
        FileDownload(content, "Result.zip");
      });
  }

  handleOpenXMLViewer() {
    this.setState({ showViewer: true });
  }

  handleCloseXMLViewer() {
    this.setState({ showViewer: false });
  }

  fileUpload(file) {
    let url = jsonUploadApi;
    let formData = new FormData();
    formData.append('file', file)
    let config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return post(url, formData, config)
  }

  xmlTextUpload(xmlText) {
    let url = uploadApi;
    let formData = new FormData();
    formData.append('textarea', xmlText)
    let config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return post(url, formData, config)
  }

  jsonContentUpload(jsonContent, parseContent) {
    let url = jsonConvertApi;
    let content;
    if(parseContent){
      content = { content: JSON.parse(jsonContent) };
    } else {
      content = { content: jsonContent };
    }    
    let config = {
      headers: {
        'content-type': 'application/json'
      }
    }
    return post(url, content, config)
  }

  getDateRangeDocs(startDate, endDate) {
    let url = bulkApi;
    let api = url + '/' + startDate + '/' + endDate;
    return get(api)
  }

  getSearchedDocs(num, docDate, vigDate) {
    let api, url = docConversion;
    if (vigDate) {
      api = url + '/' + num + '/' + docDate + '/' + vigDate;
    } else {
      api = url + '/' + num + '/' + docDate;
    }
    return get(api)
  }

  getTimestampApi(tsDir) {
    let url = listFilesByTS;
    let api = url + '/' + tsDir;
    return get(api)
  }

  onFormXMLTextSubmit(xmlText) {
    this.setState({ isFetchingData: true })
    this.xmlTextUpload(xmlText).then((response) => {
      this.setState({ isFetchingData: false })
      this.setState({ xmlContent: response.data })
      this.setState({ showViewer: true });
    }).catch((err) => {
      this.setState({ isFetchingData: false })
      toast.error(err.response.data)
      console.error("->", err.response.data)
    })
  }

  onJSONTextSubmit(jsonText) {
    this.setState({ isFetchingData: true })
    try {
      this.jsonContentUpload(jsonText, false).then((response) => {
        this.setState({ isFetchingData: false })
        this.setState({ xmlContent: response.data })
        this.setState({ showViewer: true });
      }).catch((err) => {
        this.setState({ isFetchingData: false })
        toast.error(err.response.data)
        console.error("->", err.response.data)
      })
    } catch (err) {
      toast.error(err.message)
      this.setState({ isFetchingData: false })
    }

  }

  onFormFileSubmit(file) {
    this.setState({ isFetchingData: true })
    this.fileUpload(file).then((response) => {
      let tsDir = response.data.ts;
      //calls BE to list files on the ts directory    
      this.setState({ currentTsDir: tsDir });  
      this.intId = setInterval(() => {
        this.setState({ isFetchingData: false })
        this.setState({ showResult: true });
        this.getTimestampApi(tsDir).then((response) => {
          this.setState({ resultData: response.data.content });
        }).catch((err) => {
          this.setState({ isFetchingData: false })
          toast.error(err.response.data)
          console.error("->", err.response.data)
        });
      }, 2000);
    }).catch((err) => {
      this.setState({ isFetchingData: false })
      toast.error(err.response.data)
      console.error("->", err.response.data)
    })
  }

  onDateRangeSubmit(startDate, endDate) {
    this.setState({ isFetchingData: true })
    this.getDateRangeDocs(startDate, endDate).then((response) => {
      this.setState({ isFetchingData: false })
      this.setState({ showResult: true })
      this.setState({ resultData: response.data.content })
    }).catch((err) => {
      this.setState({ isFetchingData: false })
      toast.error(err.response.data)
      console.error("->", err.response.data)
    })
  }

  onDocSearchSubmit(num, docDate, vigDate) {
    if (!num || !docDate) {
      toast.warning("Campo Obbligatorio: Num. Redazionale")
      toast.warning("Campo Obbligatorio: Data Doc.")
      return;
    }
    this.setState({ isFetchingData: true })
    this.getSearchedDocs(num, docDate, vigDate).then((response) => {      
      let tsDir = response.data.ts;
      this.setState({currentTsDir: tsDir});
      //calls BE to list files on the ts directory      
      this.intId = setInterval(() => {
        this.setState({ isFetchingData: false })
        this.setState({ showResult: true });
        this.getTimestampApi(tsDir).then((response) => {
          this.setState({ resultData: response.data.content });
        }).catch((err) => {
          this.setState({ isFetchingData: false })
          toast.error(err.response.data)
          console.error("->", err.response.data)
        });
      }, 2000);
      
    }).catch((err) => {
      this.setState({ isFetchingData: false })
      toast.error(err.response.data)
      console.error("->", err.response.data)
    })
  }

  backToUploadPahse() {
    clearInterval(this.intId);
    //reset all state vars to the origial state
    window.location.reload(true);  
  }

  render() {

    return (
      <div className="App">
        <ToastContainer />
        <header className="App-header">
          <Row>
            <Col xs={2} sm={2} md={2}>
              <div className="unibo-logo-container">
                <Image src={logoUnibo} width="65" height="65" />
              </div>
            </Col>
            <Col xs={8} sm={8} md={8}>
              <div class="text-left">
                <Image src={cirfidTestata} />
              </div>
            </Col>
            <Col xs={2} sm={2} md={2} />
          </Row>
        </header>
        <div className="App-sub-header" />

        <div className="main-app-content">
          <SpinnerLoader isLoading={this.state.isFetchingData} />
          <div>
            <HomeTextContainer
              show={!this.state.isFetchingData && !this.state.showResult}
            />

            <UploadContainer
              show={!this.state.isFetchingData && !this.state.showResult}
              onFormFileSubmit={this.onFormFileSubmit.bind(this)}
              onFormXMLTextSubmit={this.onFormXMLTextSubmit.bind(this)}
              onJSONTextSubmit={this.onJSONTextSubmit.bind(this)}
              onDateRangeSubmit={this.onDateRangeSubmit.bind(this)}
              onDocSearchSubmit={this.onDocSearchSubmit.bind(this)}
            />

            <ResultContainer
              show={!this.state.isFetchingData && this.state.showResult}
              backToUploadPahse={this.backToUploadPahse.bind(this)}
              downloadZip={this.downloadZip.bind(this)}
              resultData={this.state.resultData}
              fileDownload={this.fileDownload.bind(this)}
              downloadResultZip={this.downloadResultZip.bind(this)}
              fileDirectDownload={this.fileDirectDownload.bind(this)}
              fileView={this.fileView.bind(this)}
              fileViewContent={this.fileViewContent.bind(this)}
              viewModalContent={this.viewModalContent.bind(this)}
            />

            <XMLViewerContainer
              showViewer={this.state.showViewer}
              xmlContent={this.state.xmlContent}
              handleCloseXMLViewer={this.handleCloseXMLViewer.bind(this)}
              handleOpenXMLViewer={this.handleOpenXMLViewer.bind(this)}
            />
          </div>

        </div>
      </div>
    );
  }
}

export default App;
