import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import CodeMirror from 'react-codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/duotone-dark.css';
import 'codemirror/addon/scroll/simplescrollbars.css';
import 'codemirror/addon/scroll/simplescrollbars.js';
require('codemirror/mode/xml/xml');

class XMLViewer extends Component {  

  render() {
    let options = {
      lineNumbers: true,
      mode:'xml',
      theme:'duotone-dark',      
      scrollbarStyle:'simple'
    };
    return ( 
      <div>
        <CodeMirror
          value={this.props.xmlContent}  options={options}
          />
      </div>     
    );
  }
}

export default XMLViewer;
