import React, { Component } from 'react';
import { Line } from 'rc-progress';
import { truncate } from 'fs';

class LoadingBarContainer extends Component {   
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            percentageString: '',
            endProcess: false           
        };
        this.evaluateProgress = this.evaluateProgress.bind(this);
    }

    componentDidMount() {          
    }

    componentDidUpdate(prevProps) {        
        if (this.props.show === true && prevProps.show === false) {
            this.evaluateProgress();
        } 
    }

    evaluateProgress() {              
        let translatedDocs = this.props.resultData.filter((el) => el.status === "TRANSLATED");
        let percentageString ='Converting: ' +  translatedDocs.length + '/' + this.props.resultData.length;
        let percentage = (translatedDocs.length / this.props.resultData.length) * 100;        
        if (percentage >= 100) {
            this.setState({ progress: 100 });
            this.setState({ percentageString:'Completed: ' + translatedDocs.length + '/' + translatedDocs.length });
            this.setState({ endProcess: true });            
            clearTimeout(this.tm);
            return;
        }
        this.setState({ progress: percentage });
        this.setState({ percentageString: percentageString });        
        this.tm = setTimeout(this.evaluateProgress, 10);    
    }

    handleDownloadZip(e) {
        e.preventDefault();
        this.props.downaloadZip();
    }

    render() {      
        let displayDownload = '', displayProgress = '';
        if (this.state.endProcess) {
            displayDownload = { 'display': 'block' }
            displayProgress = { 'display': 'none' }
        } else {
            displayDownload = { 'display': 'none' }
            displayProgress = { 'display': 'block' }
        }
        return ( 
            <div>     
                <div style={displayDownload}>
                    <div>Click here to <a onClick={this.handleDownloadZip.bind(this)}> download zip</a> </div>                    
                </div>           
                <div style={displayProgress}>    
                    <div>{this.state.percentageString} </div>
                    <Line percent={this.state.progress} strokeWidth="0.5" strokeColor="#337ab7" />
                </div>
            </div>     
        );
    }
}

export default LoadingBarContainer;
