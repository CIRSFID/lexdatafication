import { DOMParser } from 'xmldom';

/**
 * Object schema for the akomando configuration
 * @typedef {Object} dpt.getXmlOrJson.object
 * @property {String} stringType The name of the root of AkomaNtoso documents
 * @property {Object} xmlDom Properties of AkomaNtoso doctypes
 * @property {Object} jsonObject Properties of AkomaNtoso doctypes
 * @example
 * {
 *    stringType: "xml",
 *    xmlDom: DOMObject (xmldom),
 *    jsonObject: null
 * }
 *
 * {
 *    stringType: "json",
 *    xmlDom: null,
 *    jsonObject: JSONobject
 * }
 *
 * {
 *    stringType: null,
 *    xmlDom: null,
 *    jsonObject: null
 * }
 */

/**
 * Checks if the given string is an XML or a JSON object.
 * @param {String} aString The string that must be checked
 * @param {String} xmlStartingCharacter The XML starting character
 * @param {String} jsonStringName The JSON string name that must be used in the output
 * @param {String} xmlStringName The XML string name that must be used in the output
 * @param {String} xmlContentType The XML content type that must be used in the parsing
 * @return {Object} returns an object that contains the information about
 * the input string and structured as the {@link dpt.getXmlOrJson.object} oject
 */
const getXmlOrJson = (
   aString,
   xmlStartingCharacter,
   jsonStringName,
   xmlStringName,
   xmlContentType,
) =>
   new Promise((resolve, reject) => {
      let stringType = null;
      let xmlDom = null;
      let jsonObject = null;

      if (aString.startsWith(xmlStartingCharacter)) {
         try {
            const parser = new DOMParser();
            xmlDom = parser.parseFromString(aString, xmlContentType);
            stringType = xmlStringName;
         } catch (e) {
            stringType = null;
            xmlDom = null;
         }
      } else if (!aString.startsWith(xmlStartingCharacter)) {
         try {
            jsonObject = JSON.parse(aString);
            stringType = jsonStringName;
         } catch (e) {
            stringType = null;
            jsonObject = null;
         }
      } else {
         reject(Error({
            stringType,
            xmlDom,
            jsonObject,
         }));
      }
      resolve({
         stringType,
         xmlDom,
         jsonObject,
      });
   });


export default getXmlOrJson;
