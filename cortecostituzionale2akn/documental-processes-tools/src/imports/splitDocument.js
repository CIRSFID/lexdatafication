import { XMLSerializer } from 'xmldom';

/**
 * Splits the given XML DOM in several files having as root the element specified in the splitOn parameter.
 * @param {*} xmldom
 * @param {*} splitOn
*/
const splitXml = (
   xmlDom,
   splitOn,
   xmlTagNamePrefixSeparator,
) => {
   const {
      documentElement,
   } = xmlDom;
   const {
      namespaceURI,
   } = documentElement;

   const elements = (splitOn.indexOf(xmlTagNamePrefixSeparator) !== -1) ?
      xmlDom.getElementsByTagNameNS(
         namespaceURI,
         splitOn.split(xmlTagNamePrefixSeparator)[1]
      ) :
      xmlDom.getElementsByTagName(splitOn);

   const results = [];
   const serializer = new XMLSerializer();
   for (let i = 0; i < elements.length; i += 1) {
      results.push(serializer.serializeToString(elements.item(i)));
   }
   return results;
};

const splitJson = (
   json,
   splitOn,
) => ({
   json,
   splitOn
});


/**
 * Returns an Array containing the a set of documents
 * @param {*} documentObject
 * @param {*} splitOn
 * @param {*} jsonStringName
 * @param {*} xmlStringName
 * @param {*} xmlTagNamePrefixSeparator
 * @param {*} refObject
 */
const splitDocument = (
   documentObject,
   splitOn,
   jsonStringName,
   xmlStringName,
   xmlTagNamePrefixSeparator,
) => {
   let results;
   const docType = documentObject.stringType;
   if (docType.toLowerCase() === jsonStringName) {
      results = splitJson(documentObject.jsonObject, splitOn);
   } else if (docType.toLowerCase() === xmlStringName) {
      results = splitXml(
         documentObject.xmlDom,
         splitOn,
         xmlTagNamePrefixSeparator
      );
   }
   return new Promise((resolve, reject) => (
      (results.length !== 0) ?
         resolve({
            documents: results
         }) :
         reject(Error({
            documents: []
         }))
   ));
};

export default splitDocument;
