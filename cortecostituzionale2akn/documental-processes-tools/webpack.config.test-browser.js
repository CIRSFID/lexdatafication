/* eslint-disable */

var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: path.resolve(__dirname, './test/browser/test-browser.js'),

	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'test-browser.api.min.js',
		library: 'dpt',
		libraryTarget: 'umd',
	},

	mode: 'production',

	target: 'web',

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}
			}
		]
	}
};
