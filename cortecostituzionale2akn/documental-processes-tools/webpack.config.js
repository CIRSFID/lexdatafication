/* eslint-disable */

var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: path.resolve(__dirname, './src/api/dpt.api.js'),

	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'dpt.api.min.js',
		library: 'dpt',
		libraryTarget: 'umd',
	},

	mode: 'production',

	target: 'node',

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}
			}
		]
	}
};
