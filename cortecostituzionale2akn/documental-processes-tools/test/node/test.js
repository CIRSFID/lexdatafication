import fs from 'fs';
import jsonDocument from '../resources/json-example.json';
import doTests from '../imports/tests-browser-node';

import dpt from '../../dist/dpt.api.min.js';


const xmlDocumentStream = fs.readFileSync('test/resources/xml-example.xml');
const xmlDocumentString = xmlDocumentStream.toString();

const xslDocumentStream = fs.readFileSync('test/resources/xslt-example.xsl');
const xslDocumentString = xslDocumentStream.toString();

const jsonDocumentString = JSON.stringify(jsonDocument);

dpt.splitDocument({
   documentString: xmlDocumentString,
   splitOn: 'articolo',
}).then(d => (
   console.log(d)
));
dpt.splitDocument({
   documentString: xslDocumentString,
   splitOn: 'xsl:template',
}).then(d => (
   console.log(d)
));
/* dpt.splitDocument({
   documentString: jsonDocumentString,
   splitOn: 'itemSurname',
}); */

/*doTests({
   xmlDocumentString,
   xslDocumentString,
   jsonDocumentString,
});*/

