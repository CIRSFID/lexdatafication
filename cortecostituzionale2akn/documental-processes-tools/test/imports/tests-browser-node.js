import { expect } from 'chai';
import dpt from '../../dist/dpt.api.min.js';

const doTests = (files) => {
   describe('#getXmlOrJson - tests on xml-example.xml', () => {
      it('stringType should contain "xml"', (done) => {
         dpt.getXmlOrJson({
            documentString: files.xmlDocumentString,
         }).then((data) => {
            expect(data.stringType).equal('xml');
            done();
         }).catch(err => Error(err));
      });
      it('xmlDom document element name should be "NIR" ...', (done) => {
         dpt.getXmlOrJson({
            documentString: files.xmlDocumentString,
         }).then((data) => {
            expect(data.xmlDom.documentElement.tagName).equal('NIR');
            done();
         }).catch(err => Error(err));
      });
      it('jsonObject should be null', (done) => {
         dpt.getXmlOrJson({
            documentString: files.xmlDocumentString,
         }).then((data) => {
            expect(data.jsonObject).equal(null);
            done();
         }).catch(err => Error(err));
      });
   });

   describe('#getXmlOrJson - tests on xslt-example.xsl', () => {
      it('stringType should contain "xml"', (done) => {
         dpt.getXmlOrJson({
            documentString: files.xslDocumentString,
         }).then((data) => {
            expect(data.stringType).equal('xml');
            done();
         }).catch(err => Error(err));
      });
      it('xmlDom document element name should be "xsl:stylesheet"', (done) => {
         dpt.getXmlOrJson({
            documentString: files.xslDocumentString,
         }).then((data) => {
            expect(data.xmlDom.documentElement.tagName).equal('xsl:stylesheet');
            done();
         }).catch(err => Error(err));
      });
      it('jsonObject should be null', (done) => {
         dpt.getXmlOrJson({
            documentString: files.xslDocumentString,
         }).then((data) => {
            expect(data.jsonObject).equal(null);
            done();
         }).catch(err => Error(err));
      });
   });

   describe('#getXmlOrJson - tests on json-example.json', () => {
      it('stringType should contain "json"', (done) => {
         dpt.getXmlOrJson({
            documentString: files.jsonDocumentString,
         }).then((data) => {
            expect(data.stringType).equal('json');
            done();
         }).catch(err => Error(err));
      });
      it('the root element name of the json should be "list" ...', (done) => {
         dpt.getXmlOrJson({
            documentString: files.jsonDocumentString,
         }).then((data) => {
            expect(Object.entries(data.jsonObject)[0][0]).equal('list');
            done();
         }).catch(err => Error(err));
      });
      it('xmlDom should be null', (done) => {
         dpt.getXmlOrJson({
            documentString: files.jsonDocumentString,
         }).then((data) => {
            expect(data.xmlDom).equal(null);
            done();
         }).catch(err => Error(err));
      });
   });
};

export default doTests;
