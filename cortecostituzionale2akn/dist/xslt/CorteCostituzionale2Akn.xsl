<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fao="http://www.fao.org/ns/akn30"
    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
    xmlns:cc="http://cortecostituzionale.it/metadata" exclude-result-prefixes="xs" version="1.0">


    <xsl:template match="elenco_pronunce">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="pronuncia">
        <akomaNtoso>
            <judgment name="ordinanza">
                <xsl:apply-templates select="./pronuncia_testata" mode="meta"/>
                <xsl:apply-templates select="./pronuncia_testo" mode="header"/>
            </judgment>
        </akomaNtoso>
    </xsl:template>

    <xsl:template match="pronuncia_testata" mode="meta">
        <meta>
            <identification source="#cirsfid">
                <FRBRWork>
                    <xsl:element name="FRBRthis">
                        <xsl:attribute name="value">
                            <xsl:choose>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'S'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/sentenza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/!main')"
                                    />
                                </xsl:when>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'O'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/ordinanza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/!main')"
                                    />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:element name="FRBRuri">
                        <xsl:attribute name="value">
                            <xsl:choose>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'S'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/sentenza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/')"
                                    />
                                </xsl:when>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'O'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/ordinanza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/')"
                                    />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:element>
                    <FRBRalias value="ECLI:IT:COST:{anno_pronuncia}:{numero_pronuncia}" name="ECLI"/>
                    <FRBRdate date="{data_decisione}" name=""/>
                    <FRBRauthor href="#corteCostituzionale"/>
                    <FRBRcountry value="it"/>
                    <FRBRnumber value="{numero_pronuncia}"/>
                </FRBRWork>
                <FRBRExpression>
                    <xsl:element name="FRBRthis">
                        <xsl:attribute name="value">
                            <xsl:choose>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'S'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/sentenza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@/!main')"
                                    />
                                </xsl:when>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'O'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/ordinanza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@/!main')"
                                    />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:element name="FRBRuri">
                        <xsl:attribute name="value">
                            <xsl:choose>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'S'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/sentenza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@/!main')"
                                    />
                                </xsl:when>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'O'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/ordinanza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@/!main')"
                                    />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:element>
                    <FRBRdate date="{data_decisione}" name=""/>
                    <FRBRauthor href="#corteCostituzionale"/>
                    <FRBRlanguage language="ita"/>
                </FRBRExpression>
                <FRBRManifestation>
                    <xsl:element name="FRBRthis">
                        <xsl:attribute name="value">
                            <xsl:choose>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'S'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/sentenza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@/!main.xml')"
                                    />
                                </xsl:when>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'O'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/ordinanza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@/!main.xml')"
                                    />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:element name="FRBRuri">
                        <xsl:attribute name="value">
                            <xsl:choose>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'S'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/sentenza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@.xml')"
                                    />
                                </xsl:when>
                                <xsl:when
                                    test="./tipologia_pronuncia/text() = 'O'">
                                    <xsl:value-of
                                        select="concat('/akn/it/judgment/ordinanza/', ./anno_pronuncia, '/', ./numero_pronuncia, '/ita@.xml')"
                                    />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:element>
                    <FRBRdate date="{data_decisione}" name=""/>
                    <FRBRauthor href="#corteCostituzionale"/>
                </FRBRManifestation>
            </identification>
            <workflow source="#corteCostituzionale">
                <step date="{data_deposito}" by="#corteCostituzionale" refersTo="#dataDeposito"/>
            </workflow>
            <references source="#cirsfid">
                <original href="" showAs=""/>
                <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani"
                    showAs="Monica Palmirani"/>
                <TLCRole eId="editor" href="/akn/ontology/role/it/editor"
                    showAs="Editor of Document"/>
                <TLCRole eId="author" href="/akn/ontology/role/it/author"
                    showAs="Author of Document"/>
                <TLCOrganization eId="corteCostituzionale"
                    href="/ontology/organizations/it/corteCostituzionale"
                    showAs="Corte Costituzionale"/>
                <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid"
                    showAs="CIRSFID"/>
            </references>
            <proprietary source="#cirsfid">
                <cc:anno_pronuncia>
                    <xsl:value-of select="./anno_pronuncia"/>
                </cc:anno_pronuncia>
                <cc:tipologia_pronuncia>
                    <xsl:value-of select="./tipologia_pronuncia"/>
                </cc:tipologia_pronuncia>
                <cc:presidente>
                    <xsl:value-of select="./presidente"/>
                </cc:presidente>
                <cc:relatore_pronuncia>
                    <xsl:value-of select="./relatore_pronuncia"/>
                </cc:relatore_pronuncia>
                <cc:data_decisione>
                    <xsl:value-of select="./data_decisione"/>
                </cc:data_decisione>
            </proprietary>
        </meta>
    </xsl:template>

    <xsl:template match="pronuncia_testo" mode="header">
        <header>
            <block name="">
                <xsl:apply-templates select="./collegio"/>
            </block>
        </header>
        <judgmentBody>
            <xsl:apply-templates select="./epigrafe"/>
            <xsl:apply-templates select="./testo"/>
            <xsl:apply-templates select="./dispositivo" mode="decision"/>
        </judgmentBody>
        <xsl:apply-templates select="./dispositivo" mode="conclusions"/>
    </xsl:template>

    <xsl:template match="collegio">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="titolo">
        <xsl:choose>
            <xsl:when test="parent::collegio">
                <courtType>
                    <xsl:value-of select="."/>
                </courtType>
            </xsl:when>
            <xsl:when test="parent::epigrafe">
                <docType>
                    <xsl:value-of select="."/>
                </docType>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="epigrafe">
        <introduction>
            <division>
                <content>
                    <p>
                        <xsl:apply-templates/>
                    </p>
                </content>
            </division>
        </introduction>
    </xsl:template>

    <xsl:template match="testo">
        <xsl:choose>
            <xsl:when test=". = ''"/>
            <xsl:otherwise>
                <motivation>
                    <division>
                        <content>
                            <p>
                                <xsl:apply-templates/>
                            </p>
                        </content>
                    </division>
                </motivation>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="dispositivo" mode="decision">
        <xsl:variable name="dispositivoText" select="."/>
        <decision>
            <block name="formulaRito">
                <xsl:value-of select="substring-before($dispositivoText, 'LA CORTE')"/> LA CORTE
                COSTITUZIONALE <eol/>
            </block>
            <division>
                <content>
                    <p>
                        <xsl:value-of
                            select="
                                substring-after(
                                substring-before($dispositivoText, 'Così deciso in Roma'),
                                'LA CORTE COSTITUZIONALE'
                                )"
                        />
                    </p>
                </content>
            </division>
        </decision>
    </xsl:template>

    <xsl:template match="dispositivo" mode="conclusions">
        <xsl:variable name="dispositivoText" select="."/>
        <conclusions>
            <block name=""> Così deciso <xsl:value-of
                    select="substring-after($dispositivoText, 'Così deciso')"/>
            </block>
        </conclusions>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:variable name="eol">
            <eol/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test=". = '&#13;'">
                <eol/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="translate(., '&#13;', $eol)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
