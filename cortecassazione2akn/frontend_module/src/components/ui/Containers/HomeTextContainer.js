import React, { Component } from 'react';

class HomeTextContainer extends Component {   

  render() {
      
    let displayAttribute = '';
    if (this.props.show) {
      displayAttribute = { 'display': 'block' }
    } else {
      displayAttribute = { 'display': 'none' }
    }
    return ( 
      <div style={displayAttribute}>
        <div className="container text-left">
            <h1>CorteCassazione2Akn</h1>
            <section>
              <p>
                Servizio di conversione Akoma Ntoso
              </p>
            </section>
        </div>  
      </div>     
    );
  }
}

export default HomeTextContainer;
