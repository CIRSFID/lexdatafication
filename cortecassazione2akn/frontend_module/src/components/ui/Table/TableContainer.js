import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import ActionContainer from './ActionContainer.js';

class TableContainer extends Component {  

  render() {
    return ( 
        <Table responsive striped bordered condensed hover>
            <thead className="table-header">
                <tr>
                    <th>Num</th>
                    <th>File</th>
                    <th>Stato</th>                    
                    <th>Validazione</th>
                    <th>Azioni</th>
                </tr>
            </thead>
            <tbody>
                {this.props.resultData.map((row, i) =>{
                    let validationStr = "NOT VALID";
                    let actionNode = "WAIT..."
                    if (row.isValid) {
                        validationStr ="VALID";
                    }
                    if (row.status === "TRANSLATED"){
                        actionNode = <ActionContainer
                            rowData={row}
                            fileDownload={this.props.fileDownload}
                            fileDirectDownload={this.props.fileDirectDownload}
                            fileView={this.props.fileView}
                            fileViewContent={this.props.fileViewContent}
                            viewModalContent={this.props.viewModalContent}
                        />;
                    }
                    return (<tr key={i}>
                        <td>{i + 1}</td>
                        <td>{row.name}</td>
                        <td>{row.status}</td>
                        <td>{validationStr}</td>
                        <td>{actionNode}</td>
                    </tr>)
                }
                    
                    
                )}              
            </tbody>
        </Table>
    );
  }
}

export default TableContainer;
