import React, { Component } from 'react';

class TextArea extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: ''
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({ value: event.target.value });
	}

	handleSubmit(e) {
		e.preventDefault(e);

		if (this.props.onFormXMLTextSubmit) {
			return this.props.onFormXMLTextSubmit(this.state.value);
		}
		if (this.props.onJSONTextSubmit) {
			return this.props.onJSONTextSubmit(this.state.value);
		}
	}
	handleClear(e) {
		e.preventDefault(e);
		this.setState({ value: '' })
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<h2>{this.props.title}</h2>
				<textarea className="form-control" rows="10" value={this.state.value} onChange={this.handleChange} />
				<input type="submit" className="btn btn-primary pull-left" value="Convert" />
				<button className="btn btn-primary pull-left" onClick={this.handleClear.bind(this)}>Clear</button>
			</form>
		);
	}
}

export default TextArea;
