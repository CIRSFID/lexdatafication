import React, { Component } from 'react';
import { Tab, Col, Row, Nav, NavItem } from 'react-bootstrap';
import TextArea from './TextArea.js';
import FileUploader from './FileUploader.js';
import DateRange from './DateRange.js';
import SimpleSearch from './SimpleSearch.js';

class TabsContainer extends Component {

  render() {
    return (
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <Row className="clearfix">
              <Col sm={4}>
            <br /><br />
                <Nav bsStyle="pills" stacked>                  
                  <NavItem eventKey="first">UPLOAD XML/ZIP</NavItem>                  
                  <NavItem eventKey="second">DOCS</NavItem>
                </Nav>
              </Col>
              <Col sm={8}>
                <Tab.Content animation>									
                  <Tab.Pane eventKey="first">
                    <FileUploader
                      title="XML/ZIP UPLOAD"
                      onFormFileSubmit={this.props.onFormFileSubmit}
                      showToasterError={this.props.showToasterError}
                    />
                  </Tab.Pane>
                  <Tab.Pane eventKey="second">
											<h2>DOCS</h2>
											<div className="text-left">                      
                      <p>Viene esposta l'api in MULTIPART <code>'/upload-and-convert'</code> che converte un file xml o un file zip contentente dei file xml in una lista di file Akoma Ntoso</p>
											</div>
									</Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
    );
  }
}

export default TabsContainer;
