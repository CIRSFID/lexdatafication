import React from 'react'
// Import React FilePond
import { FilePond, File } from 'react-filepond';

// Import FilePond styles
import 'filepond/dist/filepond.min.css';
class FilePondWrapper extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            file: null,
            files: [],
        }  
    }

    handleUpload(fieldName, file, metadata, load, error, progress, abort){
        if (!this.props.selectedOption) {
            this.props.showToasterError("Inserire selezione Civile/Penale");    
            this.setState({ files: []});
            return;
        }    
        this.props.onFormFileSubmit(file, {
            radio_selection: this.props.selectedOption
        });
    }
    
    handleInit() {
        console.log('FilePond instance has initialised', this.pond);
        const _pondRoot = document.querySelector('.filepond--root');
        let self = this;
        _pondRoot.addEventListener('FilePond:addfile', e => {            
            if (!self.props.selectedOption){
                self.props.showToasterError("Inserire selezione Civile/Penale prima del caricamento del file");
                self.setState({ files: [] });
                return;
            }
        });
    }

    render() {
        return (
            <div>
                {<FilePond ref={ref => this.pond = ref}
                    allowMultiple={false}
                    instantUpload={false}
                    oninit={() => this.handleInit()}
                    server={{
                        process: this.handleUpload.bind(this)
                    }}        
                >            
                    {this.state.files.map(file => (
                        <File key={file} source={file} />
                    ))}
                </FilePond> }
            </div>
        )
    }
}

export default FilePondWrapper;
