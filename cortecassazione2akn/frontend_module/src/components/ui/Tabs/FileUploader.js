import React from 'react';
// Import FilePond wrapper
import FilePondWrapper from './FilePondWrapper';

class FileUploader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null,
            file: null
        }  
    }

    handleOptionChange(changeEvent) {
        this.setState({
            selectedOption: changeEvent.target.value                
        });        
    }

    setCurrentFile(currentFile){
        this.setState({
            file: currentFile
        });
    }

    render() {
        return (
            <div>
                <h2>{this.props.title}</h2>
                <div>
                    <form id="radio_selection_form">
                        <div className="radio">
                            <label>
                                <input type="radio" id="radio_civ" value="civ"
                                    checked={this.state.selectedOption === 'civ'}
                                    onChange={this.handleOptionChange.bind(this)} />
                                Civile
                            </label>
                        </div>
                        <div className="radio">
                            <label>
                                <input type="radio" id="radio_pen" value="pen"
                                    checked={this.state.selectedOption === 'pen'}
                                    onChange={this.handleOptionChange.bind(this)} />
                                Penale
                            </label>
                        </div>   
                    </form>
                    </div>
                <div>
                    <FilePondWrapper
                        selectedOption={this.state.selectedOption} 
                        onFormFileSubmit={this.props.onFormFileSubmit}    
                        showToasterError={this.props.showToasterError}                
                    />     
                </div>        
            </div>
        )
    }
}

export default FileUploader;
