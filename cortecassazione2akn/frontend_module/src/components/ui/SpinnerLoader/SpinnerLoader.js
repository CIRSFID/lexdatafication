import React, { Component } from 'react';
import Spinner from 'react-spinkit';
import { Row, Col } from 'react-bootstrap';

class SpinnerLoader extends Component {
  
  render() {
    let displayAttribute = '';
    if(this.props.isLoading){
      displayAttribute = { display: 'block', margin: '0px auto', width: '0px', marginTop: '15%'}
    }else{
      displayAttribute = { display: 'none'}
    }
  
    return (
     
      <div style={displayAttribute}> 
        <Row>
          <Col xs={12} sm={12} md={12}>
          <Spinner name="ball-clip-rotate-multiple" fadeIn="none" color="steelblue" />
          </Col>
        </Row>  
      </div>
    );
  }
}

export default SpinnerLoader;
