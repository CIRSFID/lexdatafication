import async from 'async';
import fs from 'fs';
import path from 'path';
import config from '../config/config.json';
import dpt from '../../documental-processes-tools/dist/dpt.api.min';

const createSplittedArray = (xmlString, splitParam) => {
    return dpt.splitDocument({
        documentString: xmlString,
        splitOn: splitParam
    })
}

const splitFileArray = (fileArray, splitParam) => {
    let promiseArray = []
    fileArray.forEach((el) => {        
        let promise = dpt.splitDocument({
            documentString: el.content,
            splitOn: splitParam,
            refObject: el
        });
        promiseArray.push(new Promise((resolve, reject) => {
            promise
                .then((res) =>
                    resolve(Object.assign(el, res))
                )
                .catch(reject);
        }));                
    });
    return Promise.all(promiseArray);
}


exports.createSplittedArray = createSplittedArray;
exports.splitFileArray = splitFileArray;