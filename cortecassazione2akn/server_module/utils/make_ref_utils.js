
import http from 'http';

const refMarkerApi = {
    "host": "bach.cirsfid.unibo.it",
    "path": "/poldo/markup/",
    "method": "POST",
    "headers": {
        "Content-Type": "application/json"
    }
};

const markRefByRemoteService = (fileObj, cb) => {
    try {
        let refMarkupRes = '';
        let postOpt = refMarkerApi;
        let postData = JSON.stringify({
            text_input: fileObj.xmlContent,
            xml_safe: true,
            dev: true
        });
        postOpt.headers['Content-Length'] = Buffer.byteLength(postData);
        let postReq = http.request(postOpt, function (res) {
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                refMarkupRes += chunk.toString();
            });
            res.on('end', () => {
                try {
                    fileObj.refMarkupRes = JSON.parse(refMarkupRes);
                    cb(null, fileObj);
                } catch (err) {
                    fileObj.refMarkupRes = null;
                    console.log(err);
                    console.log("----------------- ERROR --------------------");
                    console.log("Sended data: ");
                    console.log(fileObj.xmlContent);
                    console.log("Server responce: ");
                    console.log(refMarkupRes);
                    console.log("--------------------------------------------");
                    cb(null, fileObj);
                }
            });
            res.on('error', (err) => {
                console.log(err);
                cb(err);
            })
        });
        postReq.write(postData);
    } catch (err) {
        console.log(err);
        cb(err);
    }
};

exports.markRefByRemoteService = markRefByRemoteService;
