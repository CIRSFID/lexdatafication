import async from 'async';
import fs from 'fs';
import path from 'path';
import validateUtils from './validate_utils';
import xsltUtils from './xslt_utils';
import xmlOutputRefiner from '../imports/XmlOutputRefiners'
import config from '../config/config.json';

const xsltFilePath = process.env.XSLT_PATH || path.join(config.xsltTransformFile.path, config.xsltTransformFile.fileName);

const convertValidateAndStore = (fileObj, doneCb) => {
    async.waterfall([
        (cb) => {
            cb(null, fileObj)
        },
        convertSingleFile,
        validateUtils.validateAknCb,
        (fileObj, cb) => {
            //writes xml content file
            fs.writeFile(fileObj.uploadDir + '/' + fileObj.fileName + '.xml', fileObj.xmlContent, (err) => {
                if (err) {
                    cb(err);
                }
                //writes .validation.json file 
                fs.writeFile(fileObj.uploadDir + '/' + fileObj.fileName + '.validation.json', fileObj.validationRes, (err) => {
                    if (err) {
                        cb(err);
                    }
                    cb(null, fileObj);
                });
            });
        }
    ], doneCb)
};

const convertXSLTValidateAndStore = (fileObj, doneCb) => {
    async.waterfall([
        (cb) => {
            cb(null, fileObj)
        },
        convertSingleFileByXSLT,
        refineOutput,
        validateUtils.validateAknCb,
        (fileObj, cb) => {
            //writes xml content file
            fs.writeFile(fileObj.uploadDir + '/' + fileObj.fileName + '.xml', fileObj.xmlContent, (err) => {
                if (err) {
                    console.log(">", err);
                    cb(err);
                }
                //writes .validation.json file 
                fs.writeFile(fileObj.uploadDir + '/' + fileObj.fileName + '.validation.json', fileObj.validationRes, (err) => {
                    if (err) {
                        console.log(">>", err);
                        cb(err);
                    }
                    cb(null, fileObj);
                });
            });
        }
    ], doneCb)
};

const convertSingleFile = (fileObj, cb) => {
    fileObj.xmlContent = fileObj.content;
    //TO APPLY Translation logic
    cb(null, fileObj);
};

const convertSingleFileByXSLT = (fileObj, cb) => {
    fs.readFile(xsltFilePath, (err, xsltData) => {
        if (err) {
            console.log(err);
            cb(err);
        }
        xsltUtils.transformByXslt(fileObj, xsltData.toString(), (err, fileObj) => {
            if (err) {
                console.log(err);
                cb(err);
            }
            cb(null, fileObj);
        });
    });
};

const refineOutput = (fileObj, cb) => {
    xmlOutputRefiner.refineOutput(fileObj.xmlContent, fileObj.materia, (err, res) => {
        if (err) {
            console.log(err);
            return cb(err);
        }
        fileObj.xmlContent = res;
        cb(null, fileObj);
    });
};

exports.convertValidateAndStore = convertValidateAndStore;
exports.convertXSLTValidateAndStore = convertXSLTValidateAndStore;