
import http from 'http';

const transformApi = {
    "host": "sinatra.cirsfid.unibo.it",
    "path": "/node/akn-validation/XsltTransform",
    "method": "POST",
    "headers": {
        "Content-Type": "application/json"
    }
};

const transformByXslt = (fileObj, xsltCnt, cb) => {
    try {
        let tranlationRes = '';
        let postOpt = transformApi;
        let postData = JSON.stringify({ 
            source: fileObj.content,
            transformFiles: [xsltCnt] 
        });
        postOpt.headers['Content-Length'] = Buffer.byteLength(postData);    
        let postReq = http.request(postOpt, function (res) {
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                tranlationRes += chunk.toString();
            });
            res.on('end', () => {
                try {
                    fileObj.xmlContent = tranlationRes;                              
                    cb(null, fileObj);
                } catch (err) {
                    console.log(err);
                    cb(err);
                }
            });
            res.on('error', (err) => {
                console.log(">>>", err);
                cb(err);
            });
        });
        postReq.write(postData);
    } catch (err) {
        console.log(err);
        cb(err);
    }
};

exports.transformByXslt = transformByXslt;
