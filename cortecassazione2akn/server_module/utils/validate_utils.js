
import http from 'http';

const validationApi = {
    "host": "sinatra.cirsfid.unibo.it",
    "path": "/node/akn-validation/Validate",
    "method": "POST",
    "headers": {
        "Content-Type": "application/json"
    }
};

const validateAkn = (xmlFileContent) => {
    return new Promise((resolve, reject) => {
        try {
            let validationRes = '';
            let postOpt = validationApi;
            let postData = JSON.stringify({ source: xmlFileContent.xmlContent });
            postOpt.headers['Content-Length'] = Buffer.byteLength(postData);
            let postReq = http.request(postOpt, function (res) {
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    validationRes += chunk.toString();
                });
                res.on('end', () => {
                    try {
                        xmlFileContent.validationObj = JSON.parse(validationRes);
                        resolve(xmlFileContent);
                    } catch (err) {
                        xmlFileContent.validationObj = {
                            success: false,
                            resError: err
                        };
                        resolve(xmlFileContent);
                    }
                })
                res.on('error', (err) => {
                    xmlFileContent.validationObj = {
                        success: false,
                        resError: err
                    };
                    resolve(xmlFileContent);
                })
            });
            postReq.write(postData);
        } catch (err) {
            console.log(err);
            reject(err);
        }
    });
}

const validateAknCb = (fileObj, cb) => {
    try {
        let validationRes = '';
        let postOpt = validationApi;
        let postData = JSON.stringify({ source: fileObj.xmlContent });
        postOpt.headers['Content-Length'] = Buffer.byteLength(postData);
        let postReq = http.request(postOpt, function (res) {
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                validationRes += chunk.toString();
            });
            res.on('end', () => {
                try {
                    fileObj.validationRes = validationRes;
                    cb(null, fileObj);
                } catch (err) {
                    console.log(err);
                    cb(err);
                }
            })
            res.on('error', (err) => {
                console.log(err);
                cb(err);
            })
        });
        postReq.write(postData);
    } catch (err) {
        console.log(err);
        cb(err);
    }
}

exports.validateAkn = validateAkn;
exports.validateAknCb = validateAknCb;
