import expect from 'chai';
import crypto from 'crypto';
import nmt from '../../src/api/nmt.api.js';

// nmt.getGoogleGoodAndBad().then((data) => {
//     console.log(data);
// });

// nmt.getGoogleGoodAndBadSync((err, data) => {
//     if (err) console.log(`Errore: ${err}`);
//     else console.log(data);
// });

describe('#hiChecker', () => {
    it('should contain "Hi Law World!"', () => {
        nmt.getGoogleGoodAndBad().then((data) => {
            expect(data).to.deep.include({
                hi: 'Hi Law World!'
            });
        }).catch(err => console.log(err));
    });

    it('should contain a correct hash field', () => {
        nmt.getGoogleGoodAndBad().then((data) => {
            expect(data).to.deep.include({
                hash: crypto.createHash('md5').update(data).digest('hex')
            });
        }).catch(err => console.log(err));
    });
});

describe('#hiCheckerSync', () => {
    it('should contain "Hi Law World!"', () => {
        nmt.getGoogleGoodAndBadSync((err, data) => {
            if (err) return console.log(err);
            expect(data).to.deep.include({
                hi: 'Hi Law World!'
            });
        });
    });

    it('should contain a correct hash field', () => {
        nmt.getGoogleGoodAndBadSync((err, data) => {
            if (err) return console.log(err);
            expect(data).to.deep.include({
                hash: crypto.createHash('md5').update(data).digest('hex')
            });
        });
    });
});
