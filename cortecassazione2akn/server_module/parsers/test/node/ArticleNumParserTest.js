import { expect, should } from 'chai';
import ArticleNumParser from '../../src/imports/article_num/ArticleNumParser';

describe('#Article_Num_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new ArticleNumParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});

describe('#Article_Num_Parser_getMatchesCheck', () => {
	it('Expect all article_num string mathces are correct', () => {
		const anp = new ArticleNumParser('it_IT');
		let testString = "              Art. 6-bis ((  (Indice ";
		console.log('> ', anp.getMatches(testString));
	});
});

describe('#Article_Num_Parser_getMatchesCheck', () => {
	it('Expect all article_num string mathces are correct', () => {
		const anp = new ArticleNumParser('it_IT');
		let testString = "              Art. 6 . (Indice ";
		console.log('> ', anp.getMatches(testString));
	});
});

describe('#Article_Num_Parser_getMatchesCheck', () => {
	it('Expect all article_num string mathces are correct', () => {
		const anp = new ArticleNumParser('it_IT');
		let testString = "              Art. 6. (Indice ";
		console.log('> ', anp.getMatches(testString));
	});
});

describe('#Article_Num_Parser_getMatchesCheck', () => {
	it('Expect all article_num string mathces are correct', () => {
		const anp = new ArticleNumParser('it_IT');
		let testString = "              Art. 6        (Indice ";
		console.log('> ', anp.getMatches(testString));
	});
});

describe('#Article_Num_Parser_getMatchesCheck', () => {
	it('Expect all article_num string mathces are correct', () => {
		const anp = new ArticleNumParser('it_IT');
		let testString = "              Art. 6    Termine";
		console.log('> ', anp.getMatches(testString));
	});
});
