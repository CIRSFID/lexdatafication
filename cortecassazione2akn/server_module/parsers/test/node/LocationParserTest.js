import { expect, should } from 'chai';
import LocationParser from '../../src/imports/location/LocationParser.js';

describe('#Location_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new LocationParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});


describe('#Location_Parser_getMatchesCheck', () => {
	it('Expect all qupte mathces are correct', () => {
		const lp = new LocationParser('it_IT');
		const testStr = "text Roma other text aosta other text";
		console.log(lp.getMatches(testStr));
	});
});
