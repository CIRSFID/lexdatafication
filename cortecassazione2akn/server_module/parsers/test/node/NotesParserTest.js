import { expect, should } from 'chai';
import NotesParser from '../../src/imports/notes/NotesParser.js';

describe('#Notes_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new NotesParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});

describe('#Notes_Parser_getMatchesCheck', () => {
	it('Expect all notes mathces are correct', () => {
		const np = new NotesParser('it_IT');
		const testStr = "AGGIORNAMENTO (2) prima nota convertito con modificazioni dalla L. 26 febbraio 2010, n. 25, ha disposto (con l'art. 7, comma 4) che: \"Il Consiglio nazionale per l'alta formazione artistica e musicale (CNAM) di cui all'articolo 3 della legge 21 dicembre 1999, n. 508, e' prorogato nella composizione esistente alla data di entrata in vigore del presente decreto fino al 31 dicembre 2010\". AGGIORNAMENTO (3) seconda nota 2009, n. 194, convertito con modificazioni dalla L. 26 febbraio 2010, n. 25, ha disposto(con l'art. 7, comma 4) che: \"Il Consiglio nazionale per l'alta formazione artistica e musicale(CNAM) di cui all'articolo 3 della legge 21 dicembre 1999, n. 508, e' prorogato nella composizione esistente alla data di entrata in vigore del presente decreto fino al 31 dicembre 2010\". AGGIORNAMENTO (4) Il D.L. 30 dicembre 2009, n. 194, convertito con modificazioni dalla L. 26 febbraio 2010, n. 25, ha disposto(con l'art. 7, comma 4) che: \"Il Consiglio nazionale per l'alta formazione artistica e musicale(CNAM) di cui all'articolo 3 della legge 21 dicembre 1999, n. 508, e' prorogato nella composizione esistente alla data di entrata in vigore del presente decreto fino al 31 dicembre 2010\".";
		console.log(np.getMatches(testStr));
	});
});
