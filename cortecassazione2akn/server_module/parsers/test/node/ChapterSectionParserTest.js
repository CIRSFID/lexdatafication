import { expect, should } from 'chai';
import ChapterSectionParser from '../../src/imports/chapter_section/ChapterSectionParser';

describe('#Chapter_Section_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new ChapterSectionParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});


describe('#Chapter_Section_Parser_getMatchesCheck', () => {
	it('Expect all date string mathces are correct', () => {
		const dp = new ChapterSectionParser('it_IT');
		let testString = "-*-*-*-*-*-*((Capo I))*PRINCIPI GENERALI*Sezione I*Definizioni, finalita' e ambito di applicazione*";
		console.log('> ', dp.getWiderMatches(testString));
	});
});
