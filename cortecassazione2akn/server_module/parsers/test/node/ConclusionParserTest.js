import { expect, should } from 'chai';
import ConclusionParser from '../../src/imports/conclusion/ConclusionParser';

describe('#Conclusion_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new ConclusionParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});


describe('#Conclusion_Parser_getMatchesCheck', () => {
	it('Expect all conclusion string mathces are correct', () => {
		const cp = new ConclusionParser('it_IT');
		let testString = "Il presente decreto, munito del sigillo dello Stato, sara' inserito nella Raccolta ufficiale degli atti normativi della Repubblica italiana. E' fatto obbligo a chiunque spetti di osservarlo e di farlo osservare. Dato a Roma, addi' 7 marzo 2005 CIAMPI Berlusconi, Presidente del Consiglio dei Ministri Stanca, Ministro per l'innovazione e le tecnologie Baccini, Ministro per la funzione pubblica Siniscalco, Ministro del-l'economia e delle finanze Pisanu, Ministro dell'interno Castelli, Ministro della giustizia Marzano, Ministro delle attivita' produttive Gasparri, Ministro delle comunicazioni Visto, il Guardasigilli: Castelli ";
		console.log('> ', cp.getMatches(testString));
	});
});

describe('#Conclusion_getWiderMatchesCheck', () => {
	it('Expect all conclusion string mathces are wider matches', () => {
		const cp = new ConclusionParser('it_IT');
		let testString = "Il presente decreto, munito del sigillo dello Stato, sara' inserito nella Raccolta ufficiale degli atti normativi della Repubblica italiana. E' fatto obbligo a chiunque spetti di osservarlo e di farlo osservare. Dato a Roma, addi' 7 marzo 2005 CIAMPI Berlusconi, Presidente del Consiglio dei Ministri Stanca, Ministro per l'innovazione e le tecnologie Baccini, Ministro per la funzione pubblica Siniscalco, Ministro del-l'economia e delle finanze Pisanu, Ministro dell'interno Castelli, Ministro della giustizia Marzano, Ministro delle attivita' produttive Gasparri, Ministro delle comunicazioni Visto, il Guardasigilli: Castelli ";
		console.log('> ', cp.getWiderMatches(testString));
	});
});
