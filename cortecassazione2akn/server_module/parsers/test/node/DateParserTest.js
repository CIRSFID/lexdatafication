import { expect, should } from 'chai';
import DateParser from '../../src/imports/date/DateParser.js';


describe('#Date_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new DateParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});


describe('#Date_Parser_getMatchesDatesCheck', () => {
	it('Expect all date string mathces are correct', () => {
		const dp = new DateParser('it_IT');
		const testString = '15-09-2013 then text 31.1.2004 and then date 12 gennaio 1986 other text then 1 Febbraio 2018 and then 03/05/2001 then other text';
		const testMatch = dp.getMatches(testString);
		expect(testMatch.length).equals(5);
		const datesArray = testMatch.map(matchObj => matchObj.dateString);
		expect(datesArray).to.include.members(['15-09-2013', '31.1.2004', '12 gennaio 1986', '1 Febbraio 2018', '03/05/2001']);
	});
});

describe('#Date_Parser_getMatchesOffsetCheck', () => {
	it('Expect all offset contains the searched date', () => {
		const dp = new DateParser('it_IT');
		const testString = '15-09-2013 then text 31.1.2004 and then date 12 gennaio 1986 other text then 1 Febbraio 2018 and then 03/05/2001 then other text';
		const testMatch = dp.getMatches(testString);
		expect(testMatch.length).equals(5);
		const offsetArray = testMatch.map(matchObj => matchObj.offset);
		const datesArray = offsetArray.map(offsetObj => testString.substring(offsetObj.start, offsetObj.end));
		expect(datesArray).to.include.members(['15-09-2013', '31.1.2004', '12 gennaio 1986', '1 Febbraio 2018', '03/05/2001']);
	});
});
describe('#Date_Parser_getMatchesInlineDatesCheck', () => {
	it('Expect all date string mathces are correct', () => {
		const dp = new DateParser('it_IT', 'inline');
		const testString = '15-09-2013 then text 31.1.2004 and then date 12 gennaio 1986 other text then 1 Febbraio 2018 and then 03/05/2001 then other text';
		const testMatch = dp.getMatches(testString);
		expect(testMatch.length).equals(5);
		const datesArray = testMatch.map(matchObj => matchObj.dateString);
		expect(datesArray).to.include.members(['15-09-2013', '31.1.2004', '12 gennaio 1986', '1 Febbraio 2018', '03/05/2001']);
	});
});

describe('#Date_Parser_getMatchesInlineOffsetCheck', () => {
	it('Expect all offset contains the searched date', () => {
		const dp = new DateParser('it_IT', 'inline');
		const testString = '15-09-2013 then text 31.1.2004 and then date 12 gennaio 1986 other text then 1 Febbraio 2018 and then 03/05/2001 then other text';
		const testMatch = dp.getMatches(testString);
		expect(testMatch.length).equals(5);
		const offsetArray = testMatch.map(matchObj => matchObj.offset);
		const datesArray = offsetArray.map(offsetObj => testString.substring(offsetObj.start, offsetObj.end));
		expect(datesArray).to.include.members(['15-09-2013', '31.1.2004', '12 gennaio 1986', '1 Febbraio 2018', '03/05/2001']);
	});
});

