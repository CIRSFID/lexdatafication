import { expect, should } from 'chai';
import PreambleParser from '../../src/imports/preamble/PreambleParser';

describe('#Preamble_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new PreambleParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});

describe('#Preamble_Parser_getMatchesCheck', () => {
	it('Expect all date string mathces are correct', () => {
		const dp = new PreambleParser('it_IT');
		let testString = "IL MINISTRO DELLA SANITA' Vista la legge 30 aprile 1962, n. 283, pubblicata nella Gazzetta Ufficiale n. 139 del 4 giugno 1962, relativa alla disciplina igienica della produzione e della vendita delle sostanze alimentari; Visto il regolamento alla sopracitata legge n. 283/1962 approvato con decreto del Presidente della Repubblica 26 marzo 1980, n. 327, pubblicato nella Gazzetta Ufficiale n. 193 del 16 luglio 1980, ed in particolare l'art. 9; Vista la prima direttiva della commissione CEE del 6 ottobre 1987, concernente la fissazione dei metodi comunitari di prelievo ai fini dell'analisi chimica per il latte conservato (87/524/CEE) pubblicata nella \"Gazzetta Ufficiale\" CEE n. L. 306 del 28 ottobre 1987; Ritenuto di dover recepire nell'ordinamento nazionale le disposizioni che formano oggetto della direttiva sopra citata; Vista la legge 23 agosto 1988, n. 400, pubblicata nel supplemento ordinario alla Gazzetta Ufficiale n. 214 del 12 settembre 1988, riguardante la disciplina dell'attivita' di Governo e l'ordinamento della Presidenza del Consiglio dei Ministri ed in particolare l'art. 17; Udito il parere del Consiglio di Stato, reso nell'adunanza generale del 14 settembre 1989; E M A N A il seguente regolamento: Art. 1. Sono approvati i metodi di prelievo ai fini della analisi chimica per il controllo del latte conservato destinato all'alimentazione umana, riportati in allegato. Essi sostituiscono quelli riportati nell'allegato A al decreto del Presidente della Repubblica 26 marzo 1980, n. 327 relativamente alle voci: \"Latte \" e \"Latte in polvere\". Il presente decreto, munito del sigillo dello Stato, sara' inserito nella Raccolta ufficiale degli atti normativi della Repubblica italiana. E' fatto obbligo a chiunque spetti di osservarlo e di farlo osservare. Roma, addi' 8 novembre 1989 Il Ministro: DE LORENZO Visto, il Guardasigilli: VASSALLI Registrato alla Corte dei conti, addi' 12 dicembre 1989 Registro n. 16 Sanita', foglio n. 123";
		console.log('> ', dp.getMatches(testString));
	});
});
