import { expect, should } from 'chai';
import QuoteParser from '../../src/imports/quote/QuoteParser.js';

describe('#Quote_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new QuoteParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});


describe('#Quote_Parser_getMatchesCheck', () => {
	it('Expect all qupte mathces are correct', () => {
		const qp = new QuoteParser('it_IT');
		const testStr = "text «quoted text1» other text “quoted text2” other text";
		console.log(qp.getMatches(testStr));
	});
});
