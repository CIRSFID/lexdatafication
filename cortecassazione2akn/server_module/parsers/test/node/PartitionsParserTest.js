import { expect, should } from 'chai';
import PartitionsParser from '../../src/imports/partitions/PartitionsParser.js';

function stringEqualOffset(string, partition) {
	return string.substring(partition.offset.start, partition.offset.end) === partition.value;
}

describe('#PartitionsParser check config', () => {
	it('should throw not defined language exception', () => {
		expect(() => new PartitionsParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});

	it('shouldn\'t throw language exception', () => {
		expect(() => new PartitionsParser('it_IT')).not.to.throw('Not defined language configuration for: it_IT');
	});
});

describe('#PartitionsParser parse articles', () => {
	const parser = new PartitionsParser('it_IT');
	it('should parse Art. 1', () => {
		const inputStr = 'Art. 1';
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 1');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(6);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).to.be.undefined;
		expect(partition.content.value).to.equal('Art. 1');
		expect(partition.content.offset.start).to.equal(0);
		expect(partition.content.offset.end).to.equal(6);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).to.be.empty;
	});
	it('should parse "Art. 10', () => {
		const inputStr = '"Art. 10';
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 10');
		expect(partition.num.offset.start).to.equal(1);
		expect(partition.num.offset.end).to.equal(8);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).to.be.undefined;
		expect(partition.content.value).to.equal('Art. 10');
		expect(partition.content.offset.start).to.equal(1);
		expect(partition.content.offset.end).to.equal(8);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).to.be.empty;
	});
	it('should parse Art 199', () => {
		const inputStr = 'Art 199';
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art 199');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(7);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).to.be.undefined;
		expect(partition.content.value).to.equal('Art 199');
		expect(partition.content.offset.start).to.equal(0);
		expect(partition.content.offset.end).to.equal(7);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).to.be.empty;
	});
	it('should parse Art49', () => {
		const inputStr = 'Art49';
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art49');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(5);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).to.be.undefined;
		expect(partition.content.value).to.equal('Art49');
		expect(partition.content.offset.start).to.equal(0);
		expect(partition.content.offset.end).to.equal(5);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).to.be.empty;
	});
	it('should parse Art. 3 Forme ...', () => {
		const inputStr = '           Art. 3 \n \n \n                Forme aggregative delle associazioni';
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 3');
		expect(partition.num.offset.start).to.equal(11);
		expect(partition.num.offset.end).to.equal(17);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).not.to.be.undefined;
		expect(partition.bodyItem.value).to.equal(' \n \n \n                Forme aggregative delle associazioni');
		expect(partition.bodyItem.offset.start).to.equal(17);
		expect(partition.bodyItem.offset.end).to.equal(75);
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(partition.content.value).to.equal('Art. 3 \n \n \n                Forme aggregative delle associazioni');
		expect(partition.content.offset.start).to.equal(11);
		expect(partition.content.offset.end).to.equal(75);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).to.be.empty;
	});

	it('should parse Art. 5 (Modifiche ... including paragraph', () => {
		const inputStr = "Art. 5\n(Modifica alla legge 27 dicembre 1997, n. 449)\n1. All'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo, sono inseriti i seguenti: «Si considerano iniziative di cui al comma 1, nel rispetto dei requisiti di cui al primo periodo del presente comma, anche quelle finalizzate a favorire l'assorbimento delle emissioni di anidride carbonica (CO <sub>2</sub> ) dall'atmosfera tramite l'incremento e la valorizzazione del patrimonio arboreo delle aree urbane, nonche' eventualmente anche quelle dei comuni finalizzate alla creazione e alla manutenzione di una rete di aree naturali ricadenti nel loro territorio, anche nel rispetto delle disposizioni del regolamento di cui al decreto del Presidente della Repubblica 8 settembre 1997, n. 357. Nei casi di cui al secondo periodo, il comune puo' inserire il nome, la ditta, il logo o il marchio dello sponsor all'interno dei documenti recanti comunicazioni istituzionali. La tipologia e le caratteristiche di tali documenti sono definite, entro sessanta giorni dalla data di entrata in vigore della presente disposizione, con decreto del Ministro dell'ambiente e della tutela del territorio e del mare, di concerto con il Ministro dell'interno, sentita la Conferenza unificata di cui all'articolo 8 del decreto legislativo 28 agosto 1997, n. 281, e successive modificazioni. Fermi restando quanto previsto dalla normativa generale in materia di sponsorizzazioni nonche' i vincoli per la tutela dei parchi e giardini storici e le altre misure di tutela delle aree verdi urbane, lo sfruttamento di aree verdi pubbliche da parte dello sponsor ai fini pubblicitari o commerciali, anche se concesso in esclusiva, deve aver luogo con modalita' tali da non compromettere, in ogni caso, la possibilita' di ordinaria fruizione delle stesse da parte del pubblico».";
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 5');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(6);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).not.to.be.undefined;
		expect(partition.heading.value).to.equal('(Modifica alla legge 27 dicembre 1997, n. 449)');
		expect(partition.heading.offset.start).to.equal(7);
		expect(partition.heading.offset.end).to.equal(53);
		expect(stringEqualOffset(inputStr, partition.heading)).to.be.true;
		expect(partition.bodyItem).not.to.be.undefined;
		expect(partition.bodyItem.value).to.equal("\n");
		expect(partition.bodyItem.offset.start).to.equal(53);
		expect(partition.bodyItem.offset.end).to.equal(54);
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(partition.content.value).to.equal(inputStr);
		expect(partition.content.offset.start).to.equal(0);
		expect(partition.content.offset.end).to.equal(1835);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).not.to.be.empty;
		const paragraph = partition.contains[0];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('1.');
		expect(paragraph.num.offset.start).to.equal(54);
		expect(paragraph.num.offset.end).to.equal(56);
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(stringEqualOffset(inputStr, paragraph.content)).to.be.true;
		expect(paragraph.content.value).to.equal("1. All'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo, sono inseriti i seguenti: «Si considerano iniziative di cui al comma 1, nel rispetto dei requisiti di cui al primo periodo del presente comma, anche quelle finalizzate a favorire l'assorbimento delle emissioni di anidride carbonica (CO <sub>2</sub> ) dall'atmosfera tramite l'incremento e la valorizzazione del patrimonio arboreo delle aree urbane, nonche' eventualmente anche quelle dei comuni finalizzate alla creazione e alla manutenzione di una rete di aree naturali ricadenti nel loro territorio, anche nel rispetto delle disposizioni del regolamento di cui al decreto del Presidente della Repubblica 8 settembre 1997, n. 357. Nei casi di cui al secondo periodo, il comune puo' inserire il nome, la ditta, il logo o il marchio dello sponsor all'interno dei documenti recanti comunicazioni istituzionali. La tipologia e le caratteristiche di tali documenti sono definite, entro sessanta giorni dalla data di entrata in vigore della presente disposizione, con decreto del Ministro dell'ambiente e della tutela del territorio e del mare, di concerto con il Ministro dell'interno, sentita la Conferenza unificata di cui all'articolo 8 del decreto legislativo 28 agosto 1997, n. 281, e successive modificazioni. Fermi restando quanto previsto dalla normativa generale in materia di sponsorizzazioni nonche' i vincoli per la tutela dei parchi e giardini storici e le altre misure di tutela delle aree verdi urbane, lo sfruttamento di aree verdi pubbliche da parte dello sponsor ai fini pubblicitari o commerciali, anche se concesso in esclusiva, deve aver luogo con modalita' tali da non compromettere, in ogni caso, la possibilita' di ordinaria fruizione delle stesse da parte del pubblico».");
		expect(paragraph.content.offset.start).to.equal(54);
		expect(paragraph.content.offset.end).to.equal(1835);
		expect(stringEqualOffset(inputStr, paragraph.bodyItem)).to.be.true;
		expect(paragraph.bodyItem.value).to.equal(" All'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo, sono inseriti i seguenti: «Si considerano iniziative di cui al comma 1, nel rispetto dei requisiti di cui al primo periodo del presente comma, anche quelle finalizzate a favorire l'assorbimento delle emissioni di anidride carbonica (CO <sub>2</sub> ) dall'atmosfera tramite l'incremento e la valorizzazione del patrimonio arboreo delle aree urbane, nonche' eventualmente anche quelle dei comuni finalizzate alla creazione e alla manutenzione di una rete di aree naturali ricadenti nel loro territorio, anche nel rispetto delle disposizioni del regolamento di cui al decreto del Presidente della Repubblica 8 settembre 1997, n. 357. Nei casi di cui al secondo periodo, il comune puo' inserire il nome, la ditta, il logo o il marchio dello sponsor all'interno dei documenti recanti comunicazioni istituzionali. La tipologia e le caratteristiche di tali documenti sono definite, entro sessanta giorni dalla data di entrata in vigore della presente disposizione, con decreto del Ministro dell'ambiente e della tutela del territorio e del mare, di concerto con il Ministro dell'interno, sentita la Conferenza unificata di cui all'articolo 8 del decreto legislativo 28 agosto 1997, n. 281, e successive modificazioni. Fermi restando quanto previsto dalla normativa generale in materia di sponsorizzazioni nonche' i vincoli per la tutela dei parchi e giardini storici e le altre misure di tutela delle aree verdi urbane, lo sfruttamento di aree verdi pubbliche da parte dello sponsor ai fini pubblicitari o commerciali, anche se concesso in esclusiva, deve aver luogo con modalita' tali da non compromettere, in ogni caso, la possibilita' di ordinaria fruizione delle stesse da parte del pubblico».");
		expect(paragraph.bodyItem.offset.start).to.equal(56);
		expect(paragraph.bodyItem.offset.end).to.equal(1835);
	});

	it('should parse multiple articles with heading Art. 5 (Modifiche ...', () => {
		const inputStr = "Art. 5\n(Modifica alla legge 27 dicembre 1997, n. 449)\nAll'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo.\n"+
							"Art. 6\n(Modifica alla legge 27 dicembre 1997, n. 449)\nsono inseriti i seguenti.";
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(2);
		let partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 5');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(6);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).not.to.be.undefined;
		expect(partition.heading.value).to.equal('(Modifica alla legge 27 dicembre 1997, n. 449)');
		expect(partition.heading.offset.start).to.equal(7);
		expect(partition.heading.offset.end).to.equal(53);
		expect(stringEqualOffset(inputStr, partition.heading)).to.be.true;
		expect(partition.bodyItem).not.to.be.undefined;
		expect(partition.bodyItem.value).to.equal("\nAll'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo.\n");
		expect(partition.bodyItem.offset.start).to.equal(53);
		expect(partition.bodyItem.offset.end).to.equal(141);
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(partition.content.value).to.equal("Art. 5\n(Modifica alla legge 27 dicembre 1997, n. 449)\nAll'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo.\n");
		expect(partition.content.offset.start).to.equal(0);
		expect(partition.content.offset.end).to.equal(141);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).to.be.empty;
		partition = res[1];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 6');
		expect(partition.num.offset.start).to.equal(141);
		expect(partition.num.offset.end).to.equal(147);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).not.to.be.undefined;
		expect(partition.heading.value).to.equal('(Modifica alla legge 27 dicembre 1997, n. 449)');
		expect(partition.heading.offset.start).to.equal(148);
		expect(partition.heading.offset.end).to.equal(194);
		expect(stringEqualOffset(inputStr, partition.heading)).to.be.true;
		expect(partition.bodyItem).not.to.be.undefined;
		expect(partition.bodyItem.value).to.equal("\nsono inseriti i seguenti.");
		expect(partition.bodyItem.offset.start).to.equal(194);
		expect(partition.bodyItem.offset.end).to.equal(220);
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(partition.content.value).to.equal("Art. 6\n(Modifica alla legge 27 dicembre 1997, n. 449)\nsono inseriti i seguenti.");
		expect(partition.content.offset.start).to.equal(141);
		expect(partition.content.offset.end).to.equal(220);
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).to.be.empty;
	});

	it('should parse article containing paragraphs and item1', () => {
		const inputStr = "Art. 6 \n \n \nPromozione di iniziative locali per lo  sviluppo  degli  spazi  verdi urbani \n \n  1. Ai fini di cui alla presente legge, le regioni, le province e  icomuni, ciascuno nell'ambito delle proprie competenze e delle risorse disponibili, promuovono l'incremento degli  spazi  verdi  urbani,  di «cinture verdi» intorno alle conurbazioni per  delimitare  gli  spazi urbani,  adottando  misure  per  la  formazione   del   personale   e l'elaborazione di capitolati finalizzati alla migliore  utilizzazione e manutenzione delle aree, e adottano  misure  volte  a  favorire  il risparmio e l'efficienza  energetica,  l'assorbimento  delle  polveri sottili e a ridurre l'effetto «isola di calore estiva», favorendo  al contempo una regolare raccolta delle acque piovane,  con  particolare riferimento: \n    a) alle nuove edificazioni,  tramite  la  riduzione  dell'impatto edilizio e il rinverdimento dell'area oggetto di nuova edificazione o di una significativa ristrutturazione edilizia; \n    b) agli edifici esistenti, tramite l'incremento, la conservazione e la tutela del patrimonio arboreo esistente nelle aree  scoperte  di pertinenza di tali edifici; \n    c) alle coperture a verde, di cui all'articolo 2,  comma  5,  del regolamento di cui al  decreto  del  Presidente  della  Repubblica  2 aprile 2009, n. 59, quali strutture dell'involucro  edilizio  atte  a produrre risparmio  energetico,  al  fine  di  favorire,  per  quanto possibile, la trasformazione dei lastrici solari in giardini pensili; \n    d) al rinverdimento delle pareti degli edifici,  sia  tramite  il rinverdimento  verticale  che  tramite  tecniche  di  verde   pensile verticale; \n    e) alla previsione e alla  realizzazione  di  grandi  aree  verdi pubbliche   nell'ambito   della   pianificazione   urbanistica,   con particolare riferimento alle zone a maggior densita' edilizia; \n    f) alla previsione  di  capitolati  per  le  opere  a  verde  che prevedano l'obbligo delle necessarie infrastrutture  di  servizio  di irrigazione e drenaggio e specifiche schede  tecniche  sulle  essenze vegetali; \n    g) alla creazione di percorsi formativi per il personale  addetto alla  manutenzione  del  verde,  anche  in  collaborazione   con   le universita', e alla sensibilizzazione della cittadinanza alla cultura del verde attraverso i canali di comunicazione e di informazione. \n  2. Ai fini del risparmio del suolo e della salvaguardia delle  aree comunali non urbanizzate, i comuni possono: \n    a) prevedere particolari misure di vantaggio volte a favorire  il riuso  e  la  riorganizzazione  degli  insediamenti  residenziali   e produttivi  esistenti,  rispetto  alla  concessione   di   aree   non urbanizzate ai fini dei suddetti insediamenti; \n    b)  prevedere   opportuni   strumenti   e   interventi   per   la conservazione e il ripristino del paesaggio rurale  o  forestale  non urbanizzato di competenza dell'amministrazione comunale. \n  3. Le modalita' di attuazione delle disposizioni di cui al comma  2 sono  definite  d'intesa  con  la   Conferenza   unificata   di   cui all'articolo 8 del decreto legislativo 28  agosto  1997,  n.  281,  e successive modificazioni. \n  4. I comuni e le  province,  in  base  a  sistemi  di  contabilita' ambientale,  da  definire  previe  intese  con  le   regioni,   danno annualmente conto, nei rispettivi siti internet, del  contenimento  o della  riduzione  delle  aree  urbanizzate  e   dell'acquisizione   e sistemazione  delle   aree   destinate   a   verde   pubblico   dalla strumentazione urbanistica vigente. \n";
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		let partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 6');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(6);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).not.to.be.undefined;
		expect(partition.bodyItem.value).to.equal(' \n \n \nPromozione di iniziative locali per lo  sviluppo  degli  spazi  verdi urbani \n \n  ');
		expect(partition.bodyItem.offset.start).to.equal(6);
		expect(partition.bodyItem.offset.end).to.equal(94);
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).not.to.be.empty;
		expect(partition.contains.length).to.equal(4);
		let paragraph = partition.contains[0];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('1.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).not.to.be.empty;
		expect(paragraph.contains.length).to.equal(7);
		paragraph = partition.contains[1];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('2.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).not.to.be.empty;
		expect(paragraph.contains.length).to.equal(2);
		paragraph = partition.contains[2];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('3.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).to.be.empty;
		paragraph = partition.contains[3];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('4.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).to.be.empty;
	});

	it('should parse article containing paragraph, item1 and item2', () => {
		const inputStr = "Art. 2 \n \n                       Ammortizzatori sociali \n \n  43. Ai contributi di cui  ai  commi  da  25  a  39  si  applica  la disposizione di cui all'articolo 26, comma 1, lettera e), della legge 9 marzo 1989, n. 88. \n  45. La durata massima legale,  in  relazione  ai  nuovi  eventi  di disoccupazione verificatisi a decorrere dal 1° gennaio 2013 e fino al 31 dicembre 2015, e' disciplinata nei seguenti termini: \n    a) per le prestazioni relative agli eventi  intercorsi  nell'anno 2013: otto mesi per  i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni e dodici mesi per i soggetti con eta' anagrafica  pari o superiore a cinquanta anni; \n    b) per le prestazioni relative agli eventi  intercorsi  nell'anno 2014: otto mesi per  i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni, dodici mesi per i soggetti con eta' anagrafica pari o superiore a  cinquanta  anni  e  inferiore  a  cinquantacinque  anni, quattordici mesi per i soggetti con eta' anagrafica pari o  superiore a cinquantacinque anni, nei limiti delle settimane  di  contribuzione negli ultimi due anni; \n    c) per le prestazioni relative agli eventi  intercorsi  nell'anno 2015: dieci mesi per i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni, dodici mesi per i soggetti con eta' anagrafica pari o superiore a cinquanta anni e inferiore a cinquantacinque anni, sedici mesi  per  i  soggetti  con  eta'  anagrafica  pari  o  superiore   a cinquantacinque anni, nei limiti  delle  settimane  di  contribuzione negli ultimi due anni. \n  46. Per i lavoratori collocati in  mobilita'  a  decorrere  dal  1° gennaio 2013 e fino al 31 dicembre  2016  ai  sensi  dell'articolo  7 della legge 23 luglio 1991, n. 223, e  successive  modificazioni,  il periodo  massimo  di  diritto  della  relativa  indennita'   di   cui all'articolo 7, commi 1 e 2, della legge 23 luglio 1991, n.  223,  e' ridefinito nei seguenti termini: \n    a) lavoratori collocati in mobilita' nel periodo dal  1°  gennaio 2013 al 31 dicembre 2014: \n      1) lavoratori di cui all'articolo  7,  comma  1:  dodici  mesi, elevato a ventiquattro per i lavoratori che hanno compiuto i quaranta anni e a trentasei per i lavoratori che hanno  compiuto  i  cinquanta anni; \n      2) lavoratori di cui  all'articolo  7,  comma  2:  ventiquattro mesi, elevato a trentasei per  i  lavoratori  che  hanno  compiuto  i quaranta anni e a quarantotto per i lavoratori che hanno  compiuto  i cinquanta anni; \n    b) LETTERA ABROGATA DAL D.L. 22 GIUGNO 2012,  N.  83,  CONVERTITO CON MODIFICAZIONI DALLA L. 7 AGOSTO 2012, N. 134;";
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		let partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Art. 2');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(6);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).not.to.be.undefined;
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).not.to.be.empty;
		expect(partition.contains.length).to.equal(3);
		let paragraph = partition.contains[0];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('43.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).to.be.empty;
		paragraph = partition.contains[1];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('45.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).not.to.be.empty;
		expect(paragraph.contains.length).to.equal(3);
		item = paragraph.contains[1];
		expect(item.name).to.equal('item1');
		expect(item.num.value).to.equal('b)');
		expect(stringEqualOffset(inputStr, item.num)).to.be.true;
		paragraph = partition.contains[2];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('46.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).not.to.be.empty;
		expect(paragraph.contains.length).to.equal(2);
		let item = paragraph.contains[0];
		expect(item.name).to.equal('item1');
		expect(item.num.value).to.equal('a)');
		expect(stringEqualOffset(inputStr, item.num)).to.be.true;
		expect(item.contains).not.to.be.empty;
		expect(item.contains.length).to.equal(2);
		let item2 = item.contains[0];
		expect(item2.name).to.equal('item2');
		expect(item2.num.value).to.equal('1)');
		expect(stringEqualOffset(inputStr, item2.num)).to.be.true;
		expect(item2.contains).to.be.empty;
		item2 = item.contains[1];
		expect(item2.name).to.equal('item2');
		expect(item2.num.value).to.equal('2)');
		expect(stringEqualOffset(inputStr, item2.num)).to.be.true;
		expect(item2.contains).to.be.empty;
	});
});

describe('#PartitionsParser heirarchy of numbers', () => {
	const parser = new PartitionsParser('it_IT');

	it('should parse CAPO I .. Sezione I..', () => {
		const inputStr = "-*-*-*-*-*-*Capo I*PRINCIPI GENERALI*Sezione I*Definizioni, finalita' e ambito di applicazione*";
		const res = parser.getMatches(inputStr);
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('chapter');
		expect(partition.num.value).to.equal('Capo I');
		expect(partition.num.offset.start).to.equal(12);
		expect(partition.num.offset.end).to.equal(18);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(partition.heading).to.be.undefined;
		expect(partition.bodyItem).not.to.be.undefined;
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.contains).not.to.be.empty;
		expect(partition.contains.length).to.equal(1);
		const section = partition.contains[0];
		expect(section.name).to.equal('section');
		expect(section.num.value).to.equal('Sezione I');
		expect(stringEqualOffset(inputStr, section.num)).to.be.true;
		expect(section.contains).to.be.empty;
	});
});

describe('#PartitionsParser heirarchy context', () => {
	const parser = new PartitionsParser('it_IT');

	it('should throw not defined partition', () => {
		expect(() => parser.getMatches('', 'foo')).to.throw('Not defined partition: foo');
	});

	it('should parse from paragraph', () => {
		const inputStr = "Art. 5\n Modifica alla legge 27 dicembre 1997, n. 449\n1. All'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo, sono inseriti i seguenti: «Si considerano iniziative di cui al comma 1, nel rispetto dei requisiti di cui al primo periodo del presente comma, anche quelle finalizzate a favorire l'assorbimento delle emissioni di anidride carbonica (CO <sub>2</sub> ) dall'atmosfera tramite l'incremento e la valorizzazione del patrimonio arboreo delle aree urbane, nonche' eventualmente anche quelle dei comuni finalizzate alla creazione e alla manutenzione di una rete di aree naturali ricadenti nel loro territorio, anche nel rispetto delle disposizioni del regolamento di cui al decreto del Presidente della Repubblica 8 settembre 1997, n. 357. Nei casi di cui al secondo periodo, il comune puo' inserire il nome, la ditta, il logo o il marchio dello sponsor all'interno dei documenti recanti comunicazioni istituzionali. La tipologia e le caratteristiche di tali documenti sono definite, entro sessanta giorni dalla data di entrata in vigore della presente disposizione, con decreto del Ministro dell'ambiente e della tutela del territorio e del mare, di concerto con il Ministro dell'interno, sentita la Conferenza unificata di cui all'articolo 8 del decreto legislativo 28 agosto 1997, n. 281, e successive modificazioni. Fermi restando quanto previsto dalla normativa generale in materia di sponsorizzazioni nonche' i vincoli per la tutela dei parchi e giardini storici e le altre misure di tutela delle aree verdi urbane, lo sfruttamento di aree verdi pubbliche da parte dello sponsor ai fini pubblicitari o commerciali, anche se concesso in esclusiva, deve aver luogo con modalita' tali da non compromettere, in ogni caso, la possibilita' di ordinaria fruizione delle stesse da parte del pubblico».";
		const res = parser.getMatches(inputStr, 'paragraph');
		expect(res.length).to.equal(1);
		const partition = res[0];
		expect(partition.name).to.equal('paragraph');
		expect(partition.num.value).to.equal('1.');
		expect(partition.num.offset.start).to.equal(53);
		expect(partition.num.offset.end).to.equal(55);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.content.value).to.equal("1. All'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo, sono inseriti i seguenti: «Si considerano iniziative di cui al comma 1, nel rispetto dei requisiti di cui al primo periodo del presente comma, anche quelle finalizzate a favorire l'assorbimento delle emissioni di anidride carbonica (CO <sub>2</sub> ) dall'atmosfera tramite l'incremento e la valorizzazione del patrimonio arboreo delle aree urbane, nonche' eventualmente anche quelle dei comuni finalizzate alla creazione e alla manutenzione di una rete di aree naturali ricadenti nel loro territorio, anche nel rispetto delle disposizioni del regolamento di cui al decreto del Presidente della Repubblica 8 settembre 1997, n. 357. Nei casi di cui al secondo periodo, il comune puo' inserire il nome, la ditta, il logo o il marchio dello sponsor all'interno dei documenti recanti comunicazioni istituzionali. La tipologia e le caratteristiche di tali documenti sono definite, entro sessanta giorni dalla data di entrata in vigore della presente disposizione, con decreto del Ministro dell'ambiente e della tutela del territorio e del mare, di concerto con il Ministro dell'interno, sentita la Conferenza unificata di cui all'articolo 8 del decreto legislativo 28 agosto 1997, n. 281, e successive modificazioni. Fermi restando quanto previsto dalla normativa generale in materia di sponsorizzazioni nonche' i vincoli per la tutela dei parchi e giardini storici e le altre misure di tutela delle aree verdi urbane, lo sfruttamento di aree verdi pubbliche da parte dello sponsor ai fini pubblicitari o commerciali, anche se concesso in esclusiva, deve aver luogo con modalita' tali da non compromettere, in ogni caso, la possibilita' di ordinaria fruizione delle stesse da parte del pubblico».");
		expect(partition.content.offset.start).to.equal(53);
		expect(partition.content.offset.end).to.equal(1834);
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(partition.bodyItem.value).to.equal(" All'articolo 43, comma 2, della legge 27 dicembre 1997, n. 449, dopo il primo periodo, sono inseriti i seguenti: «Si considerano iniziative di cui al comma 1, nel rispetto dei requisiti di cui al primo periodo del presente comma, anche quelle finalizzate a favorire l'assorbimento delle emissioni di anidride carbonica (CO <sub>2</sub> ) dall'atmosfera tramite l'incremento e la valorizzazione del patrimonio arboreo delle aree urbane, nonche' eventualmente anche quelle dei comuni finalizzate alla creazione e alla manutenzione di una rete di aree naturali ricadenti nel loro territorio, anche nel rispetto delle disposizioni del regolamento di cui al decreto del Presidente della Repubblica 8 settembre 1997, n. 357. Nei casi di cui al secondo periodo, il comune puo' inserire il nome, la ditta, il logo o il marchio dello sponsor all'interno dei documenti recanti comunicazioni istituzionali. La tipologia e le caratteristiche di tali documenti sono definite, entro sessanta giorni dalla data di entrata in vigore della presente disposizione, con decreto del Ministro dell'ambiente e della tutela del territorio e del mare, di concerto con il Ministro dell'interno, sentita la Conferenza unificata di cui all'articolo 8 del decreto legislativo 28 agosto 1997, n. 281, e successive modificazioni. Fermi restando quanto previsto dalla normativa generale in materia di sponsorizzazioni nonche' i vincoli per la tutela dei parchi e giardini storici e le altre misure di tutela delle aree verdi urbane, lo sfruttamento di aree verdi pubbliche da parte dello sponsor ai fini pubblicitari o commerciali, anche se concesso in esclusiva, deve aver luogo con modalita' tali da non compromettere, in ogni caso, la possibilita' di ordinaria fruizione delle stesse da parte del pubblico».");
		expect(partition.bodyItem.offset.start).to.equal(55);
		expect(partition.bodyItem.offset.end).to.equal(1834);
	});

	it('should parse from paragraph down to item2', () => {
		const inputStr = "Art. 2 \n \n                       Ammortizzatori sociali \n \n  43. Ai contributi di cui  ai  commi  da  25  a  39  si  applica  la disposizione di cui all'articolo 26, comma 1, lettera e), della legge 9 marzo 1989, n. 88. \n  45. La durata massima legale,  in  relazione  ai  nuovi  eventi  di disoccupazione verificatisi a decorrere dal 1° gennaio 2013 e fino al 31 dicembre 2015, e' disciplinata nei seguenti termini: \n    a) per le prestazioni relative agli eventi  intercorsi  nell'anno 2013: otto mesi per  i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni e dodici mesi per i soggetti con eta' anagrafica  pari o superiore a cinquanta anni; \n    b) per le prestazioni relative agli eventi  intercorsi  nell'anno 2014: otto mesi per  i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni, dodici mesi per i soggetti con eta' anagrafica pari o superiore a  cinquanta  anni  e  inferiore  a  cinquantacinque  anni, quattordici mesi per i soggetti con eta' anagrafica pari o  superiore a cinquantacinque anni, nei limiti delle settimane  di  contribuzione negli ultimi due anni; \n    c) per le prestazioni relative agli eventi  intercorsi  nell'anno 2015: dieci mesi per i  soggetti  con  eta'  anagrafica  inferiore  a cinquanta anni, dodici mesi per i soggetti con eta' anagrafica pari o superiore a cinquanta anni e inferiore a cinquantacinque anni, sedici mesi  per  i  soggetti  con  eta'  anagrafica  pari  o  superiore   a cinquantacinque anni, nei limiti  delle  settimane  di  contribuzione negli ultimi due anni. \n  46. Per i lavoratori collocati in  mobilita'  a  decorrere  dal  1° gennaio 2013 e fino al 31 dicembre  2016  ai  sensi  dell'articolo  7 della legge 23 luglio 1991, n. 223, e  successive  modificazioni,  il periodo  massimo  di  diritto  della  relativa  indennita'   di   cui all'articolo 7, commi 1 e 2, della legge 23 luglio 1991, n.  223,  e' ridefinito nei seguenti termini: \n    a) lavoratori collocati in mobilita' nel periodo dal  1°  gennaio 2013 al 31 dicembre 2014: \n      1) lavoratori di cui all'articolo  7,  comma  1:  dodici  mesi, elevato a ventiquattro per i lavoratori che hanno compiuto i quaranta anni e a trentasei per i lavoratori che hanno  compiuto  i  cinquanta anni; \n      2) lavoratori di cui  all'articolo  7,  comma  2:  ventiquattro mesi, elevato a trentasei per  i  lavoratori  che  hanno  compiuto  i quaranta anni e a quarantotto per i lavoratori che hanno  compiuto  i cinquanta anni; \n    b) LETTERA ABROGATA DAL D.L. 22 GIUGNO 2012,  N.  83,  CONVERTITO CON MODIFICAZIONI DALLA L. 7 AGOSTO 2012, N. 134;";
		const res = parser.getMatches(inputStr, 'paragraph');
		expect(res.length).to.equal(3);
		let paragraph = res[0];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('43.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).to.be.empty;
		paragraph = res[1];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('45.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).not.to.be.empty;
		expect(paragraph.contains.length).to.equal(3);
		item = paragraph.contains[1];
		expect(item.name).to.equal('item1');
		expect(item.num.value).to.equal('b)');
		expect(stringEqualOffset(inputStr, item.num)).to.be.true;
		paragraph = res[2];
		expect(paragraph.name).to.equal('paragraph');
		expect(paragraph.num.value).to.equal('46.');
		expect(stringEqualOffset(inputStr, paragraph.num)).to.be.true;
		expect(paragraph.contains).not.to.be.empty;
		expect(paragraph.contains.length).to.equal(2);
		let item = paragraph.contains[0];
		expect(item.name).to.equal('item1');
		expect(item.num.value).to.equal('a)');
		expect(stringEqualOffset(inputStr, item.num)).to.be.true;
		expect(item.contains).not.to.be.empty;
		expect(item.contains.length).to.equal(2);
		let item2 = item.contains[0];
		expect(item2.name).to.equal('item2');
		expect(item2.num.value).to.equal('1)');
		expect(stringEqualOffset(inputStr, item2.num)).to.be.true;
		expect(item2.contains).to.be.empty;
		item2 = item.contains[1];
		expect(item2.name).to.equal('item2');
		expect(item2.num.value).to.equal('2)');
		expect(stringEqualOffset(inputStr, item2.num)).to.be.true;
		expect(item2.contains).to.be.empty;
	});
});


describe('#PartitionsParser heirarchy recognize partitions out of heirarchy order', () => {
	const parser = new PartitionsParser('it_IT');

	it('should throw not defined partition', () => {
		expect(() => parser.getMatches('', 'foo')).to.throw('Not defined partition: foo');
	});

	it('should parse from paragraph', () => {
		const inputStr = "Articolo 1\nFinalità\n1. Il presente regolamento detta le disposizioni di attuazione della legge regionale 24 luglio 2014, n. 22 “Disciplina delle attività di pescaturismo, di ittiturismo e di acquiturismo. Istituzione della Consulta ittica regionale. Modifiche alla legge regionale 21 aprile 1999, n. 3 (Riforma del sistema regionale e locale)”, secondo quanto previsto dall’articolo 12 della legge medesima.\nArticolo 2\nDefinizioni\n1. Ai fini del presente regolamento, fermo restando quanto stabilito all’articolo 2 della legge regionale n. 22 del 2014, si applicano le seguenti definizioni:\na) impresa ittica: l’insieme delle attività di pesca e di acquacoltura esercitate dall’imprenditore ittico in modo professionalmente organizzato, per la produzione e lo scambio dei prodotti pescati o allevati;\nb) materie prime aziendali: i prodotti derivanti dall’esercizio diretto dell’attività di pesca ed acquacoltura che si distinguono dalle materie secondarie, frutto della trasformazione delle materie prime;\nc) ospitalità: offerta di pernottamento o alloggio;\nd) attività ricreative, didattiche, culturali, sociali, di pratica sportiva, escursionistiche: le iniziative finalizzate ad intrattenere gli ospiti ovvero a fornire servizi, organizzate dall’impresa nell’ambito delle proprie attività;\ne) attività commerciali: attività dirette alla produzione di beni o di servizi, attività intermediarie nella circolazione dei beni, di trasporto per terra, per acqua o per aria, o attività ausiliarie alle precedenti;\nf) somministrazione di pasti e bevande: l’attività commerciale di offerta di pasti e bevande con la valorizzazione dei prodotti ittici locali pescati o allevati;\ng) degustazioni di prodotti aziendali trasformati in prodotti gastronomici: attività di servizio svolta attraverso l’offerta di una o più pietanze nelle quali è prevalente la valorizzazione di un prodotto aziendale senza che l’offerta assuma complessivamente la caratteristica di un pasto;\nh) marineria: il complesso degli uomini, dei mezzi e delle attività che concorrono a costituire la potenza peschereccia di un determinato porto.\nTITOLO I\nATTIVITÀ DI PESCATURISMO\nArticolo 3\nOrganizzazione e svolgimento delle attività e delle iniziative di pescaturismo\n1. Nell’ambito dell’attività di pescaturismo, come definita all’articolo 2, comma 2, della legge regionale n. 22 del 2014, rientrano le seguenti iniziative:\na) lo svolgimento di attività turistico-ricreative per la divulgazione della cultura del mare e della pesca finalizzate all’organizzazione e allo svolgimento di escursioni in mare, lungo le coste, le lagune, i laghi ed i fiumi e per la diffusione del patrimonio di conoscenze legate ai mestieri e alle tradizioni della pesca;\nb) lo svolgimento di attività finalizzate alla conoscenza e alla valorizzazione dell’ambiente costiero, delle lagune costiere e delle acque interne, nonché ad avvicinare il pubblico al mondo della pesca professionale, anche per l’osservazione di attività di pesca professionale;\nc) lo svolgimento di attività di pesca sportiva e ricreativa mediante l’impiego degli attrezzi da pesca consentiti;\nd) il trasporto di subacquei.\nArticolo 4\nPescaturismo nelle acque marine\n1. L’esercizio dell’attività di pescaturismo nelle acque marine è subordinato alla specifica autorizzazione all’imbarco di persone estranee all’equipaggio, rilasciata dall’autorità preposta all’iscrizione dell’imbarcazione.\n2. L’autorizzazione di cui al comma 1 deve essere allegata alla segnalazione certificata di inizio attività, di cui all’articolo 18 del presente regolamento.\n3. Per quanto attiene alla determinazione dei sistemi di pesca, degli attrezzi utilizzabili, delle possibili aree, dei periodi e dei tempi di esercizio dell’attività, le imprese ittiche sono tenute al rispetto delle norme previste dal Decreto del Presidente della Repubblica 2 ottobre 1968, n. 1639 (Regolamento per l'esecuzione della legge 14 luglio 1965, n. 963, concernente la disciplina della pesca marittima) e dal Decreto del Ministero delle Politiche agricole, alimentari e forestali 13 aprile 1999, n. 293 (Regolamento recante norme in materia di disciplina dell’attività di pescaturismo, in attuazione dell’art. 27-bis della legge 17 febbraio 1982, n. 41).\nArticolo 5\nPescaturismo nelle acque interne\n1. Le imprese ittiche esercenti la pesca professionale nelle acque interne possono intraprendere l’attività di pescaturismo su imbarcazioni in disponibilità dell’impresa ittica, regolarmente iscritte ai Registri delle unità adibite alla navigazione nelle acque interne.\n2. Per quanto attiene alla determinazione dei sistemi di pesca, degli attrezzi utilizzabili, delle possibili aree, dei periodi e dei tempi di esercizio dell’attività, le imprese di pesca sono tenute al rispetto delle norme previste dalla legge regionale 7 novembre 2012, n. 11 (Norme per la tutela della fauna ittica e dell'ecosistema acquatico e per la disciplina della pesca, dell'acquacoltura e delle attività connesse nelle acque interne) e dal regolamento regionale 2 febbraio 2018, n. 1 (Regolamento regionale di attuazione delle disposizioni in materia di tutela della fauna ittica e dell'ecosistema acquatico e di disciplina della pesca, dell'acquacoltura e delle attività connesse nelle acque interne, a norma dell'articolo 26, legge regionale 7 novembre 2012, n. 11).\nTITOLO II\nATTIVITÀ DI ITTITURISMO\nArticolo 6\nIniziative dell’ittiturismo\n1. Nell’ambito dell’attività di ittiturismo, come definita all’articolo 2, comma 3, della legge regionale n. 22 del 2014, rientrano le seguenti iniziative:\na) attività di ospitalità;\nb) attività ricreative;\nc) attività di tipo didattico;\nd) attività culturali;\ne) attività di fornitura di beni e servizi;\nf) attività volte alla corretta fruizione degli ecosistemi acquatici e delle risorse della pesca;\ng) attività volte alla valorizzazione degli aspetti socioculturali del settore ittico.";
		const res = parser.getMatches(inputStr);
		// console.log(JSON.stringify(res[0], false, 4));
		expect(res.length).to.equal(4);
		const partition = res[0];
		expect(partition.name).to.equal('article');
		expect(partition.num.value).to.equal('Articolo 1');
		expect(partition.num.offset.start).to.equal(0);
		expect(partition.num.offset.end).to.equal(10);
		expect(stringEqualOffset(inputStr, partition.num)).to.be.true;
		expect(stringEqualOffset(inputStr, partition.content)).to.be.true;
		expect(partition.content.value).to.equal("Articolo 1\nFinalità\n1. Il presente regolamento detta le disposizioni di attuazione della legge regionale 24 luglio 2014, n. 22 “Disciplina delle attività di pescaturismo, di ittiturismo e di acquiturismo. Istituzione della Consulta ittica regionale. Modifiche alla legge regionale 21 aprile 1999, n. 3 (Riforma del sistema regionale e locale)”, secondo quanto previsto dall’articolo 12 della legge medesima.\n");
		expect(partition.content.offset.start).to.equal(0);
		expect(partition.content.offset.end).to.equal(408);
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
		expect(stringEqualOffset(inputStr, partition.bodyItem)).to.be.true;
	});
});
