import { expect, should } from 'chai';
import TransitionalParser from '../../src/imports/transitional/TransitionalParser';

describe('#Transitional_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new TransitionalParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});

describe('#Transitional_Parser_getMatchesCheck', () => {
	it('Expect all article_num string mathces are correct', () => {
		const anp = new TransitionalParser('it_IT');
		let testString = "                              Art. 15.\n\n  Per  i  reati  di  attentato alla Costituzione e di alto tradimento\ncommessi dal Presidente della Repubblica la Corte costituzionale, nel\npronunciare  sentenza  di  condanna, determina le sanzioni penali nei\nlimiti  del  massimo  di pena previsto dalle leggi vigenti al momento\ndel  fatto,  nonche'  le  sanzioni  costituzionali,  amministrative e\ncivili adeguate al fatto.\n  Le norme contenute nelle leggi penali relative alla sussistenza del\nreato,  alla punibilita' ed alla perseguibilita' sono applicabili nei\ngiudizi  di  accusa  nei  confronti  del Presidente del Consiglio dei\nMinistri  e  dei Ministri, ma la Corte puo' aumentare la pena fino ad\nun  terzo anche oltre la misura stabilita, in caso di circostanze che\nrivelino  l'eccezionale gravita' del reato. La Corte puo' infliggere,\naltresi',  le  sanzioni  costituzionali  e amministrative adeguate al\nfatto.\n\n                      Disposizione transitoria.\n\n  La  prima  elezione  della Commissione preveduta dall'art. 12 avra'\nluogo entro due mesi dalla entrata in vigore della presente legge.\n\n  La  presente  legge costituzionale, munita del sigillo dello Stato,\nsara'  inserta  nella  Raccolta  ufficiale  delle leggi e dei decreti\ndella  Repubblica  italiana.  E'  fatto  obbligo a chiunque spetti di\nosservarla e di farla osservare come legge dello Stato.\n\n  Data a Roma, addi' 11 marzo 1953\n\n                               EINAUDI\n\n                                                    DE GASPERI - ZOLI\n\nVisto, il Guardasigilli: ZOLI\n";
		console.log('> ', anp.getMatches(testString));
	});
});
