import { expect, should } from 'chai';
import ReferenceParser from '../../src/imports/reference/ReferenceParser.js';

describe('#Reference_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new ReferenceParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});


describe('#Reference_Parser_getMatchesCheck', () => {
	it('Expect all date string mathces are correct', () => {
		const dp = new ReferenceParser('it_IT');
		const testStringArray = [
			'legge 17 febbraio 1982, n. 46',
			'legge n. 46 del 17 febbraio 1982',
			"direttiva del ministro dell'industria, del commercio e dell'artigianato del 16 gennaio 2001",
			"l'art. 1, commi da 354 a 361, della legge n. 311 del 30 dicembre 2004",
			'decreto interministeriale del 1° febbraio 2006'
		];

		testStringArray.forEach((testStr) => {
			console.log('tmWd ', dp.getWiderMatches(testStr));
		});

		console.log('ddd ', dp.getWiderMatches(testStringArray.join(' blah blah , blah')));
	});
});
