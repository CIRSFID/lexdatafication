import { expect, should } from 'chai';
import ConclusionDateParser from '../../src/imports/conclusion_date/ConclusionDateParser';

describe('#Conclusion_Date_Parser_unavaibleLanguageConfig', () => {
	it('should throw not defined language exception', () => {
		expect(() => new ConclusionDateParser('abcd')).to.throw('Not defined language configuration for: abcd');
	});
});


describe('#Conclusion_Date_Parser_getMatchesCheck', () => {
	it('Expect all conclusion_date string mathces are correct', () => {
		const cp = new ConclusionDateParser('it_IT');
		let testString = "text some TEXT  21389u text Dato a Roma, addi' 7 marzo 2005 text text";
		console.log('> ', cp.getMatches(testString));
	});
});

describe('#Conclusion_Date_Parser_getWiderMatchesCheck', () => {
	it('Expect all conclusion_date string mathces are wider matches', () => {
		const cp = new ConclusionDateParser('it_IT');
		let testString = "text some TEXT  21389u text Dato a Roma, addi' 7 marzo 2005 123 text text";
		console.log('> ', cp.getWiderMatches(testString));
	});
});
