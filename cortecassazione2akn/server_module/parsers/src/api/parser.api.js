import DateParser from '../imports/date/DateParser.js';
import ReferenceParser from '../imports/reference/ReferenceParser.js';
import ChapterSectionParser from '../imports/chapter_section/ChapterSectionParser.js';
import PreambleParser from '../imports/preamble/PreambleParser.js';
import ConclusionParser from '../imports/conclusion/ConclusionParser.js';
import ConclusionDateParser from '../imports/conclusion_date/ConclusionDateParser.js';
import ArticleNumParser from '../imports/article_num/ArticleNumParser.js';
import CitationRecitalsParser from '../imports/citation_recitals/CitationRecitalsParser.js';
import QuoteParser from '../imports/quote/QuoteParser.js';
import NotesParser from '../imports/notes/NotesParser.js';
import LocationParser from '../imports/location/LocationParser.js';
import PartitionsParser from '../imports/partitions/PartitionsParser.js';
import TransitionalParser from '../imports/transitional/TransitionalParser';
import PreambleInitParser from '../imports/preamble_init/PreambleInitParser';
import PreambleEndParser from '../imports/preamble_end/PreambleEndParser';
import EpigrafeParser from '../imports/epigrafe/EpigrafeParser';
import SvolgimentoParser from '../imports/svolgimento/SvolgimentoParser';
import FattoParser from '../imports/fatto/FattoParser';
import DirittoParser from '../imports/diritto/DirittoParser';
import DispositivoParser from '../imports/dispositivo/DispositivoParser';
import JudgeParser from '../imports/judges/JudgeParser';
import AutGiudParser from '../imports/autorita_giudiziarie/AutGiudParser';
import AutGiudSezParser from '../imports/autorita_giudiziarie_sezioni/AutGiudSezParser';
import ProvGiudParser from '../imports/provvedimenti_giudiziari/ProvvGiudParser';
import ReferenceGiurParser from '../imports/reference_giur/ReferenceGiurParser';
import ParagraphsParser from '../imports/paragraphs/ParagraphsParser';
import ReferenceLegParser from '../imports/reference_leg/ReferenceLegParser';
import PersonParser from '../imports/person/PersonParser';
import RoleParser from '../imports/role/RoleParser';




const parser = {
	DateParser,
	ReferenceParser,
	ChapterSectionParser,
	PreambleParser,
	ConclusionParser,
	ConclusionDateParser,
	ArticleNumParser,
	CitationRecitalsParser,
	QuoteParser,
	NotesParser,
	LocationParser,
	PartitionsParser,
	TransitionalParser,
	PreambleInitParser,
	PreambleEndParser,
	EpigrafeParser,
	SvolgimentoParser,
	FattoParser,
	DirittoParser,
	DispositivoParser,
	JudgeParser,
	AutGiudParser,
	AutGiudSezParser,
	ProvGiudParser,
	ReferenceGiurParser,
	ParagraphsParser,
	ReferenceLegParser,
	PersonParser,
	RoleParser
};

export default parser;
