import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class JudgeParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				giudice: super.createInlineMatchProp(matchProps.mp, 'giudice'),
                ruolo: super.createInlineMatchProp(matchProps.mp, 'ruolo'),
				fullString: matchProps.mp[0]
			};
			matches.push(matchObj);
		});
		return matches;
	}
}

module.exports = JudgeParser;
