
import XRegExp from 'xregexp';

class Parser {
	constructor(configObj, mode, flags, externalBuilders) {
		this.configObj = configObj;
		this.buildedFields = {};
		this.flags = flags === undefined ? 'i' : flags;
		//adds extra group builder for compositions
		this.externalBuilders = externalBuilders || [];
		this.externalBuilders.forEach((groupBuilder) => {
			this.configObj[groupBuilder.groupName] = groupBuilder.buildedRegExp;
		});
		this.mode = mode || 'standard';
		if (this.mode === 'standard') {
			this.buildedRegExps = this.buildRegex();
		} else if (this.mode === 'inline') {
			this.buildedRegExps = this.buildRegexInline();
		}
	}

	getInlineComposition() {
		return this.buildRegexInline()[0].xregexp.source;
	}

	buildRegex() {
		const buildedRegExpArray = [];
		this.configObj.patterns.forEach((regExPattern) => {
			buildedRegExpArray.push(this.buildRegExpPattern(regExPattern));
		});
		return buildedRegExpArray;
	}

	buildRegexInline() {
		const buildedRegExpArray = [];
		const inlineRegExpArray = [];
		this.configObj.patterns.forEach((regExPattern) => {
			inlineRegExpArray.push(`(${regExPattern})`);
		});
		buildedRegExpArray.push(this.buildRegExpPattern(inlineRegExpArray.join('|')));
		return buildedRegExpArray;
	}

	buildRegExpPattern(regExPattern) {
		let patternFields = XRegExp(/{{\w+}}/g), localObj = {};
		XRegExp.forEach(regExPattern, patternFields, (a1, a2, a3, a4) => {
			const groupName = a1[0].replace('{{', '').replace('}}', '');
			const occurence = (regExPattern.match(new RegExp(a1[0], 'g')) || []).length;
			if (!this.configObj.hasOwnProperty(groupName)) {
				throw new Error(`${'Pattern misconfiguration: Group '}${groupName} not defined`);
			}
			if (!this.buildedFields.hasOwnProperty(groupName)) {
				if (Array.isArray(this.configObj[groupName])) {
					this.buildedFields[groupName] = this.buildRegExpPattern(this.configObj[groupName].join('|'));
				} else {
					this.buildedFields[groupName] = this.buildRegExpPattern(this.configObj[groupName]);
				}
			}
			localObj[groupName] = this.buildedFields[groupName];
			if (occurence > 1) {
				for (let k = 1; k <= occurence; k++) {
					localObj[`${groupName}_${k}`] = localObj[groupName];
					regExPattern = regExPattern.replace(a1[0], a1[0].replace('}}', `_${k}}}`));
				}
			}
		});
		//NB: flags defined here works, not in json (escape or xregexp?)
		return XRegExp.build(regExPattern, localObj, this.flags);
	}

	createInlineMatchProp(matchPropsObj, attrName) {
		const inlineFieldsArray = [];
		Object.keys(matchPropsObj).forEach((key) => {
			if (attrName === key.split('_')[0] && matchPropsObj[key]) {
				inlineFieldsArray.push(matchPropsObj[key]);
			}
		});
		if (inlineFieldsArray.length > 1) {
			return inlineFieldsArray;
		}
		return inlineFieldsArray[0];
	}

	getMatches(stringToParse) {
		let matches = [];
		this.buildedRegExps.forEach((regExp) => {
			matches = matches.concat(this.getRawMatches(stringToParse, regExp));
		});
		return matches;
	}

	getRawMatches(stringToParse, regExp) {
		const matches = [];
		XRegExp.forEach(stringToParse, regExp, (matchProps) => {
			const matchObj = {
				offset: {
					start: matchProps.index,
					end: matchProps.index + matchProps[0].length
				},
				mp: matchProps
			};
			matches.push(matchObj);
		});
		return matches;
	}
}

module.exports = Parser;
