import * as R from 'ramda';
import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class PartitionsParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode, '');
			this.selectedLang = avaiableLangs[lang];
			this.hierarchyRegExps = this.buildHeirRegExps();
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	buildHeirRegExps() {
		return this.selectedLang.hierarchy
				.map((name) => {
					return {
						name,
						regExp: this.buildRegExpPattern(this.selectedLang[name])
					}
				})
				.reduce((prevObj, currObj) => {
					return Object.assign(prevObj, {
						[currObj.name]: currObj.regExp
					})
				}, {})
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse, contextPartition) {
		let hierarchy = this.selectedLang.hierarchy;
		if (contextPartition) {
			const contextIndex = this.selectedLang.hierarchy.indexOf(contextPartition);
			if (contextIndex == -1) {
				throw new Error(`Not defined partition: ${contextPartition}`);
			} else {
				hierarchy = hierarchy.slice(contextIndex)
			}
		}
		return this.normalizeOffsets(
					this.getPartitionsRec(stringToParse, hierarchy)
				);
	}

	getPartitionsRec(stringToParse, hierarchy) {
		let partitions = [];
		hierarchy
			.some((partition, hierarchyIndex) => {
				const descHierarchy = hierarchy.slice(hierarchyIndex+1);
				const patitionMatches = this.getPartitionRec(stringToParse, partition, descHierarchy);
				if(patitionMatches.length > 0) {
					const startOffset = patitionMatches[0].num.offset.start;
					const notRecognizedStr = stringToParse.substring(0, startOffset);
					const outOfHeirMatches = this.getPartitionsRec(notRecognizedStr, descHierarchy);
					partitions = outOfHeirMatches.concat(partitions);
				}
				partitions = partitions.concat(patitionMatches);
				return patitionMatches.length > 0;
			})
		return partitions;
	}

	getPartitionRec(stringToParse, partition, hierarchy) {
		const buildResObj = (matchOffset) => {
			const [match, offset] = matchOffset;
			const partitionContent = stringToParse.substring(offset.start, offset.end);
			const num = {
				value: match.mp.num.trim()
			};

			num.offset = {
				start: match.offset.start,
				end: match.offset.start+num.value.length
			}

			const heading = {
				value: (match.mp.heading || '').trim()
			}

			let lastHeadingElement = num;
			if (heading.value) {
				const startHeading = partitionContent.indexOf(heading.value);
				heading.offset = {
					start: offset.start+startHeading,
					end: offset.start+startHeading+heading.value.length
				}
				lastHeadingElement = heading;
			}

			const bodyItem = {
				value: stringToParse.substring(lastHeadingElement.offset.end, offset.end),
				offset: {
					start: lastHeadingElement.offset.end,
					end: offset.end
				}
			}

			const partitionObj = {
				name: partition,
				num,
				content: {
					value: partitionContent,
					offset: offset
				},
				contains: []
			};

			if (heading.value) {
				partitionObj.heading = heading;
			}

			if (bodyItem.value) {
				const contains = this.getPartitionsRec(bodyItem.value, hierarchy);
				if (contains.length > 0) {
					const startOffset = contains[0].content.offset.start;
					bodyItem.value = bodyItem.value.substring(0, startOffset);
					if (bodyItem.value) {
						bodyItem.offset.end = bodyItem.offset.start + startOffset;
						partitionObj.bodyItem = bodyItem;
					}
				} else {
					partitionObj.bodyItem = bodyItem;
				}
				partitionObj.contains = contains;
			}

			return partitionObj;
		}

		let matches = this.getRawMatches(stringToParse, this.hierarchyRegExps[partition]);
		const offsets = R.aperture(2,
							R.concat(
								R.map(R.path(['offset', 'start']), matches),
								[stringToParse.length]
							)
						).map((offGroup) => {
							return {
								start: offGroup[0],
								end: offGroup[1]
							}
						});
		matches = R.zip(matches, offsets)
					.map(buildResObj);
		return matches;
	}

	normalizeOffsets(partitions) {
		const updateOffset = (field, startOffset) => {
			if (field) {
				field.offset.start += startOffset;
				field.offset.end += startOffset;
			}
			return field;
		}

		const updateOffsets = (partitions, startOffset) => {
			return partitions.map((partition) => {
				updateOffset(partition.num, startOffset);
				updateOffset(partition.heading, startOffset);
				updateOffset(partition.content, startOffset);
				updateOffset(partition.bodyItem, startOffset);
				if (partition.bodyItem) {
					partition.contains = updateOffsets(
											partition.contains,
											partition.bodyItem.offset.start
										)
				}
				return partition;
			});
		}

		return updateOffsets(partitions, 0);
	}
}

module.exports = PartitionsParser;
