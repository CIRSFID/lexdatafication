import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class PreambleParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			/*
				NB: Fields preambleInitList and preambleEndList one or the other is undefined
				for each match. TO-DO: (widerMatches is possible?)
			*/
			const matchObj = {
				offset: matchProps.offset,
				preambleEndList: super.createInlineMatchProp(matchProps.mp, 'preambleEndList'),
				refString: matchProps.mp[0]
			};
			matches.push(matchObj);
		});
		return matches;
	}

}

module.exports = PreambleParser;
