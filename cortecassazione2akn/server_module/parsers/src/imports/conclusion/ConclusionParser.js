import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class ConclusionParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				conclusionEntry: super.createInlineMatchProp(matchProps.mp, 'conclusionList'),
				conclusionString: matchProps.mp[0]
			};
			matches.push(matchObj);
		});
		return matches;
	}

	getWiderMatches(stringToParse) {
		const allMatches = this.getMatches(stringToParse).sort((m1, m2) => m1.offset.start - m2.offset.start).sort((m1, m2) => m2.offset.end - m1.offset.end);
		let matchesCounts = allMatches.length;
		let toRemoveItems = [], widerItems = [];
		while (matchesCounts-- && matchesCounts >= 1) {
			if (allMatches[matchesCounts - 1].offset.start === allMatches[matchesCounts].offset.start &&
				allMatches[matchesCounts - 1].offset.end > allMatches[matchesCounts].offset.end) {
				toRemoveItems.push(allMatches[matchesCounts]);
			}
		}
		let found = false;
		for (let j = 0; j < allMatches.length; j++) {
			found = false;
			for (let f = 0; f < toRemoveItems.length; f++) {
				if (toRemoveItems[f] === allMatches[j]) {
					found = true;
				}
			}
			if (!found) {
				widerItems.push(allMatches[j]);
			}
		}
		return widerItems;
	}
}

module.exports = ConclusionParser;
