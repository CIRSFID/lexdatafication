import Parser from '../Parser.js';
import DateParser from '../date/DateParser';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class ReferenceParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			//create external group builders
			const externalBuilders = [
				{
					groupName: 'date',
					buildedRegExp: new DateParser(lang).getInlineComposition()
				}
			];
			super(avaiableLangs[lang], mode, undefined, externalBuilders);
			this.selectedLang = avaiableLangs[lang];
			this.dateParser = new DateParser(lang);
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				docNum: super.createInlineMatchProp(matchProps.mp, 'docNum'),
				docType: super.createInlineMatchProp(matchProps.mp, 'docType'),
				authority: super.createInlineMatchProp(matchProps.mp, 'authority'),
				number: super.createInlineMatchProp(matchProps.mp, 'number'),
				date: super.createInlineMatchProp(matchProps.mp, 'date'),
				refString: matchProps.mp[0]
			};
			if (matchObj.date) {
				matchObj.isoDate = this.dateParser.getMatches(matchObj.date)[0].isoDate;
			}
			matches.push(matchObj);
		});
		return matches;
	}

	getWiderMatches(stringToParse) {
		const allMatches = this.getMatches(stringToParse).sort((m1, m2) => m1.offset.start - m2.offset.start).sort((m1, m2) => m2.offset.end - m1.offset.end);
		let matchesCounts = allMatches.length;
		let toRemoveItems = [], widerItems = [];
		while (matchesCounts-- && matchesCounts >= 1) {
			if (allMatches[matchesCounts - 1].offset.start === allMatches[matchesCounts].offset.start &&
				allMatches[matchesCounts - 1].offset.end > allMatches[matchesCounts].offset.end) {
				toRemoveItems.push(allMatches[matchesCounts]);
			}
		}
		let found = false;
		for (let j = 0; j < allMatches.length; j++) {
			found = false;
			for (let f = 0; f < toRemoveItems.length; f++) {
				if (toRemoveItems[f] === allMatches[j]) {
					found = true;
				}
			}
			if (!found) {
				widerItems.push(allMatches[j]);
			}
		}
		return widerItems;
	}
}

module.exports = ReferenceParser;
