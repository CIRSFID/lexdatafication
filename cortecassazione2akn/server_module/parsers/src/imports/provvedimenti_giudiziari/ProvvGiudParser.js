import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class ProvvGiudParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				sentenza: super.createInlineMatchProp(matchProps.mp, 'sentenza'),
				ordint: super.createInlineMatchProp(matchProps.mp, 'ordint'),
				ordinanza: super.createInlineMatchProp(matchProps.mp, 'ordinanza'),
				decreto: super.createInlineMatchProp(matchProps.mp, 'decreto'),
				fullString: matchProps.mp[0]
			};
			matches.push(matchObj);
		});
		return matches;
	}

}

module.exports = ProvvGiudParser;
