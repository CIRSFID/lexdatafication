import Parser from '../Parser.js';
import DateParser from '../date/DateParser';
import AutGiudParser from '../autorita_giudiziarie/AutGiudParser';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class ReferenceGiurParser extends Parser {
	constructor(lang, mode, context) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			//create external group builders
			const externalBuilders = [
				{
					groupName: 'date',
					buildedRegExp: new DateParser(lang).getInlineComposition()
				},
			];
			super(avaiableLangs[lang], mode, undefined, externalBuilders);
			this.selectedLang = avaiableLangs[lang];
			this.dateParser = new DateParser(lang);
			this.context = context ? context : {};
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMateria(matToCheck) {
		if (matToCheck){
			const mat = matToCheck.toLowerCase();
			if (mat.indexOf("civ") > -1){
				return "CIV";
			}
			if (mat.indexOf("pen") > -1){
				return "PEN";
			}
		}
		return this.context.materia;
	};

	getCorte(corteToCheck) {
		const corte = corteToCheck.toLowerCase();
		if (corte.indexOf("cass") > -1){
			return "CASS";
		}
		if (corte.indexOf("cost") > -1 || corte.indexOf("consulta")){
			return "COST";
		}
		return this.context.corte;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				materia: this.getMateria(super.createInlineMatchProp(matchProps.mp, 'materia')),
				numero: super.createInlineMatchProp(matchProps.mp, 'numero'),
				corte: this.getCorte(super.createInlineMatchProp(matchProps.mp, 'corte')),
				anno: super.createInlineMatchProp(matchProps.mp, 'year'),
				sezione: super.createInlineMatchProp(matchProps.mp, 'sezNum'),
				refString: matchProps.mp[0],
				tipoProvv: super.createInlineMatchProp(matchProps.mp, 'provvedimento'),
			};
			if (matchObj.date) {
				matchObj.isoDate = this.dateParser.getMatches(matchObj.date)[0].isoDate;
			}
			matchObj.corte = matchObj.corte ? matchObj.corte : this.context.corte;
			matchObj.materia = matchObj.materia ? matchObj.materia : this.context.materia;
			matches.push(matchObj);
		});
		return matches;
	}

	getWiderMatches(stringToParse) {
		const allMatches = this.getMatches(stringToParse).sort((m1, m2) => m1.offset.start - m2.offset.start).sort((m1, m2) => m2.offset.end - m1.offset.end);
		let matchesCounts = allMatches.length;
		let toRemoveItems = [], widerItems = [];
		while (matchesCounts-- && matchesCounts >= 1) {
			if (allMatches[matchesCounts - 1].offset.start === allMatches[matchesCounts].offset.start &&
				allMatches[matchesCounts - 1].offset.end > allMatches[matchesCounts].offset.end) {
				toRemoveItems.push(allMatches[matchesCounts]);
			}
		}
		let found = false;
		for (let j = 0; j < allMatches.length; j++) {
			found = false;
			for (let f = 0; f < toRemoveItems.length; f++) {
				if (toRemoveItems[f] === allMatches[j]) {
					found = true;
				}
			}
			if (!found) {
				widerItems.push(allMatches[j]);
			}
		}
		return widerItems;
	}
}

module.exports = ReferenceGiurParser;
