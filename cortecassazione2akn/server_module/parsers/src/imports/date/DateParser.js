import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class DateParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	evaluateISODate(day, month, year) {
		const newDate = new Date();
		if (month.toString().length > 0 && month.toString().length <= 2) {
			newDate.setMonth(month);
		} else if (month.toString().length > 2) {
			for (let k = 0; k < this.selectedLang.monthNames.length; k++) {
				if (this.selectedLang.monthNames[k].toLowerCase().substring(0, 3) === month.toLowerCase().substring(0, 3)) {
					newDate.setMonth(k);
					break;
				}
			}
		}
		//escape special chars for day ex: °
		const escapedDay = day.replace(/[^\w\s]/gi, '');
		newDate.setDate(escapedDay);
		newDate.setFullYear(+year);
		return newDate.toISOString().substring(0, 10);
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				day: super.createInlineMatchProp(matchProps.mp, 'day'),
				month: super.createInlineMatchProp(matchProps.mp, 'monthNames') || super.createInlineMatchProp(matchProps.mp, 'numMonth'),
				year: super.createInlineMatchProp(matchProps.mp, 'year'),
				dateSep: super.createInlineMatchProp(matchProps.mp, 'dateSep'),
				dateString: matchProps.mp[0]
			};
			matchObj.isoDate = this.evaluateISODate(matchObj.day, matchObj.month, matchObj.year);
			matches.push(matchObj);
		});
		return matches;
	}
}

module.exports = DateParser;
