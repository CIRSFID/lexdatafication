import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class NotesParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		const rawMatches = super.getMatches(stringToParse);
		rawMatches.forEach((matchProps, idx) => {
			const matchObj = {
				offset: matchProps.offset,
				noteIncipit: super.createInlineMatchProp(matchProps.mp, 'noteIncipit'),
				noteNumber: super.createInlineMatchProp(matchProps.mp, 'noteNumber'),
				noteContent: rawMatches[idx + 1] ? stringToParse.substring(matchProps.offset.end, rawMatches[idx + 1].offset.start)
					: stringToParse.substring(matchProps.offset.end, stringToParse.length),
				fullString: matchProps.mp[0]
			};
			matchObj.offset.end += matchObj.noteContent.length;
			matchObj.fullString += matchObj.noteContent;
			matches.push(matchObj);
		});
		return matches;
	}
}

module.exports = NotesParser;
