import Parser from '../Parser.js';
import _it_IT from './it_IT/config.json';

const avaiableLangs = {
	it_IT: _it_IT
};

class NotesParser extends Parser {
	constructor(lang, mode) {
		if (avaiableLangs.hasOwnProperty(lang)) {
			super(avaiableLangs[lang], mode);
			this.selectedLang = avaiableLangs[lang];
		} else {
			throw new Error(`Not defined language configuration for: ${lang}`);
		}
	}

	getLang() {
		return this.selectedLang;
	}

	getMatches(stringToParse) {
		const matches = [];
		super.getMatches(stringToParse).forEach((matchProps) => {
			const matchObj = {
				offset: matchProps.offset,
				transitionalInit: super.createInlineMatchProp(matchProps.mp, 'transitionalInit'),
				conclusionInitList: super.createInlineMatchProp(matchProps.mp, 'conclusionInitList'),
				content: super.createInlineMatchProp(matchProps.mp, 'content'),
				fullString: matchProps.mp[0]
			};
			matchObj.fullString = matchObj.fullString.substring(0, matchObj.fullString.indexOf(matchObj.conclusionInitList))
			matches.push(matchObj);
		});
		return matches;
	}
}

module.exports = NotesParser;
