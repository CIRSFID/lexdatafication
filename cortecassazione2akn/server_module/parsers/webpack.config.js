/* eslint-disable */

var webpack = require('webpack');
var uglifyJsPlugin = require('uglifyjs-webpack-plugin');
var WebpackShellPlugin = require('webpack-shell-plugin');
var path = require('path');

module.exports = {
    entry: path.resolve(__dirname, './src/api/parser.api.js'),

    output: {
        path: path.resolve(__dirname, 'dist'),
		filename: 'parser.api.min.js',
		library: "js-parser",
        libraryTarget: "umd",
    },

    target: 'node',

    plugins: [
		new uglifyJsPlugin(),
		new WebpackShellPlugin({
			onBuildStart: ['echo "Starting build..."'],
			onBuildEnd: ['node postbuild.js']
		})
    ],

    resolve: {
        extensions: ['.js'],
        modules: [
            'node_modules',
        ],
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'es2016', 'es2017'],
                    plugins: ['transform-runtime']
                },
            }, {
                test: /\.json$/,
                use: 'json-loader',
            },
        ],
    }
};
