var fs = require('fs');
var path = require('path');

var distFileName = 'parser.api.min.js';
var srcPath = '/dist/' + distFileName;
var moduleName = 'js_parsers';
var deps = ['document_generator','json_adapter'];

function copyFile(source, target, cb) {
	var cbCalled = false;

	var rd = fs.createReadStream(source);
	rd.on("error", function (err) {
		done(err);
	});
	var wr = fs.createWriteStream(target);
	wr.on("error", function (err) {
		done(err);
	});
	wr.on("close", function (ex) {
		done();
	});
	rd.pipe(wr);

	function done(err) {
		if (!cbCalled) {
			cb(err);
			cbCalled = true;
		}
	}
}
console.log("Coping to dependecies folder...");
deps.forEach(function (currentModule) {
	var distPath = '../' + currentModule + '/lib/' + moduleName + '/' + distFileName;
	copyFile(path.join(__dirname, srcPath), path.join(__dirname, distPath), function () {
		console.log("Updated module: ", currentModule);
	});
})
