<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
                xmlns:cass="http://italgiure.giustizia.it/sncass" exclude-result-prefixes="xs" version="1.0">

    <xsl:variable name="tipoprovvedimento">
        <xsl:choose>
            <xsl:when test="snintegrale/@tipoprov = 'S'">
                <xsl:value-of select="'sentenza'"/>
            </xsl:when>
            <xsl:when test="snintegrale/@tipoprov = 'O'">
                <xsl:value-of select="'ordinanza'"/>
            </xsl:when>
            <xsl:when test="snintegrale/@tipoprov = 'D'">
                <xsl:value-of select="'decreto'"/>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="snintegrale">
        <akomaNtoso>
            <judgment name="{$tipoprovvedimento}">
                <xsl:apply-templates select="./sic" mode="meta"/>
                <header>
                    <xsl:apply-templates select="./col" mode="collegio"/>
                </header>
                <judgmentBody>
                    <xsl:apply-templates select="./tes" mode="body"/>
                    <xsl:apply-templates select="./dis" mode="decision"/>
                </judgmentBody>
            </judgment>
        </akomaNtoso>
    </xsl:template>

    <xsl:template match="sic" mode="meta">
        <meta>
            <identification source="#cirsfid">
                <FRBRWork>
                    <xsl:element name="FRBRthis">
                        <xsl:attribute name="value">
                            <xsl:value-of
                                    select="concat('/akn/it/judgment/', $tipoprovvedimento, '/cass/', ../@anno, '/', ../@numdec, '/!main')"
                            />
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:element name="FRBRuri">
                        <xsl:attribute name="value">
                            <xsl:value-of
                                    select="concat('/akn/it/judgment/', $tipoprovvedimento, '/cass/', ../@anno, '/', ../@numdec, '/')"
                            />
                        </xsl:attribute>
                    </xsl:element>
                    <FRBRalias value="ECLI:IT:CASS:{../@anno}:{../@numdec}" name="ECLI"/>  <!-- manca CIV|PEN -->
                    <FRBRdate date="{./@datdep}" name=""/>  <!-- snintegrale/@datdec || sic/@datdep || sic/data_ud -->
                    <FRBRauthor href="#corteDiCassazione"/>
                    <FRBRcountry value="it"/>
                    <FRBRnumber value="{../@numdec}"/>
                </FRBRWork>
                <FRBRExpression>
                    <xsl:element name="FRBRthis">
                        <xsl:attribute name="value">
                            <xsl:value-of
                                    select="concat('/akn/it/judgment/', $tipoprovvedimento, '/cass/', ../@anno, '/', ../@numdec, '/ita@/!main')"
                            />
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:element name="FRBRuri">
                        <xsl:attribute name="value">
                            <xsl:value-of
                                    select="concat('/akn/it/judgment/', $tipoprovvedimento, '/cass/', ../@anno, '/', ../@numdec, '/ita@/!main')"
                            />
                        </xsl:attribute>
                    </xsl:element>
                    <FRBRdate date="{./@datdep}" name=""/>  <!-- snintegrale/@datdec || sic/@datdep || sic/data_ud -->
                    <FRBRauthor href="#corteDiCassazione"/>
                    <FRBRlanguage language="ita"/>
                </FRBRExpression>
                <FRBRManifestation>
                    <xsl:element name="FRBRthis">
                        <xsl:attribute name="value">
                            <xsl:value-of
                                    select="concat('/akn/it/judgment/', $tipoprovvedimento, '/cass/', ../@anno, '/', ../@numdec, '/ita@/!main.xml')"
                            />

                        </xsl:attribute>
                    </xsl:element>
                    <xsl:element name="FRBRuri">
                        <xsl:attribute name="value">
                            <xsl:value-of
                                    select="concat('/akn/it/judgment/', $tipoprovvedimento, '/cass/', ../@anno, '/', ../@numdec, '/ita@.xml')"
                            />
                        </xsl:attribute>
                    </xsl:element>
                    <FRBRdate date="{./@datdep}" name=""/> <!-- snintegrale/@datdec || sic/@datdep || sic/data_ud -->
                    <FRBRauthor href="#corteDiCassazione"/>
                </FRBRManifestation>
            </identification>
            <workflow source="#corteDiCassazione">
                <step date="{./@data_ud}" by="#corteDiCassazione" refersTo="#dataUdienza"/>
                <step date="{./@datdep}" by="#corteDiCassazione" refersTo="#dataDeposito"/>
            </workflow>
            <references source="#cirsfid">
                <original href="" showAs=""/>
                <TLCOrganization eId="corteDiCassazione"
                                 href="/ontology/organizations/it/corteDiCassazione"
                                 showAs="Corte di Cassazione"/>
                <TLCOrganization eId="cirsfid" href="/akn/ontology/organizations/it/cirsfid"
                                 showAs="CIRSFID"/>
                <TLCPerson eId="palmirani" href="/ontology/person/it/palmirani"
                           showAs="Monica Palmirani"/>

                <!-- QUI PRENDIAMO LE PERSONE [GIUDICI, PM E PARTI] -->
                <xsl:for-each select="../sic/coll">
                    <xsl:choose>
                        <xsl:when test="@PM">
                            <TLCPerson eId="" href="/ontology/person/pubblicoministero/it." role="#pubblicoministero" showAs="{@PM}"/>
                        </xsl:when>
                        <xsl:when test="@REL">
                            <TLCPerson eId="" href="/ontology/person/relatore/it." role="#relatore" showAs="{@REL}"/>
                        </xsl:when>
                        <xsl:when test="@CON">
                            <TLCPerson eId="" href="/ontology/person/giudice/it." role="#giudice" showAs="{@CON}"/>
                        </xsl:when>
                        <xsl:when test="@PRE">
                            <TLCPerson eId="" href="/ontology/person/presidente/it." role="#presidente" showAs="{@PRE}"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>

                <xsl:for-each select="../sic/parte">
                    <xsl:choose>
                        <xsl:when test="@ric">
                            <TLCPerson eId="" href="/ontology/person/it/" role="#ricorrente" showAs="{@ric}"/>
                        </xsl:when>
                        <xsl:when test="@imp">
                            <TLCPerson eId="" href="/ontology/person/it/" role="#imputato" showAs="{@imp}"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <TLCPerson eId="" href="/ontology/person/it/" role="#resistente" showAs="{@contro}"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>

                <!-- QUI PRENDIAMO I RUOLI [GIUDICI, PM E PARTI] -->

                <TLCRole eId="presidente" href="/ontology/role/presidente" showAs="Presidente"/>
                <TLCRole eId="relatore" href="/ontology/role/relatore" showAs="Relatore"/>
                <TLCRole eId="giudice" href="/ontology/role/giudiceofcassation" showAs="Consigliere"/>
                <TLCRole eId="pubblicoministero" href="/ontology/role/pubblicoministero" showAs="Pubblico Ministero"/>
                <!--
                <xsl:for-each select="../sic/coll">
                    <xsl:choose>
                        <xsl:when test="@PM">
                            <TLCRole eId="pubblicoministero" href="/ontology/role/pubblicoministero" showAs="pubblicoministero"/>
                        </xsl:when>

                        WARNING: possono esservi più PM, quindi gli eId andranno enumerati
                        per evitare conflitti

                        Così avremo più consiglieri nel TLCRole però!
                        <xsl:when test="@CON">
                            <TLCRole eId="consigliere" href="/ontology/role/consigliere" showAs="Consigliere"/>
                        </xsl:when>
                        <xsl:when test="@REL">
                            <TLCRole eId="relatore" href="/ontology/role/relatore" showAs="Relatore"/>
                        </xsl:when>
                        <xsl:when test="@PRE">
                            <TLCRole eId="presidente" href="/ontology/role/presidente" showAs="Presidente"/>
                        </xsl:when>
                        Ci sono ontologie per presidente, relatore e consigliere? Vanno distinti in TLCRole?
                    </xsl:choose>
                </xsl:for-each>
                -->

                <xsl:for-each select="../sic/parte">
                    <xsl:choose>
                        <xsl:when test="@ric">
                            <TLCRole eId="ricorrente" href="/ontology/role/ricorrente/it." showAs="ricorrente"/>
                        </xsl:when>
                        <xsl:when test="@imp">
                            <TLCRole eId="imputato" href="/ontology/person/imputato/it." showAs="imputato"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <TLCRole eId="resistente" href="/ontology/role/resistente/it." showAs="resistente"/>
                        </xsl:otherwise>
                        <!--
                        WARNING: possono esservi più ricorrenti e resistenti, quindi gli eId andranno enumerati
                        per evitare conflitti
                        -->
                    </xsl:choose>
                </xsl:for-each>
                <TLCRole eId="editor" href="/akn/ontology/role/it/editor"
                         showAs="Editor of Document"/>
                <TLCRole eId="author" href="/akn/ontology/role/it/author"
                         showAs="Author of Document"/>

                <TLCConcept eId="epigrafe" href="/akn/ontology/concept/it/svolgimento" showAs="epigrafe"/>
                <TLCConcept eId="svolgimento" href="/akn/ontology/concept/it/svolgimento" showAs="svolgimento"/>
                <TLCConcept eId="fatto" href="/akn/ontology/concept/it/fatto" showAs="fatto"/>
                <TLCConcept eId="diritto" href="/akn/ontology/concept/it/fatto" showAs="diritto"/>
                <TLCEvent eId="dataUdienza" href="/akn/ontology/event/it/dataUdienza" showAs="data udienza"/>
                <TLCEvent eId="dataDeposito" href="/akn/ontology/event/it/dataDeposito" showAs="data deposito"/>
                <TLCConcept eId="pqm" href="/akn/ontology/concept/it/pqm" showAs="P. Q. M."/>
            </references>
            <proprietary source="#cirsfid">
                <cass:anno_pronuncia>
                    <xsl:value-of select="../@anno"/>
                </cass:anno_pronuncia>
                <cass:tipologia_pronuncia>
                    <xsl:value-of select="$tipoprovvedimento"/>
                </cass:tipologia_pronuncia>
                <cass:presidente>
                    <xsl:value-of select="./coll[@PRE]/@PRE"/>
                </cass:presidente>
                <cass:relatore_pronuncia>
                    <xsl:value-of select="./coll[@REL]/@REL"/>
                </cass:relatore_pronuncia>
                <cass:pm_pronuncia>
                    <xsl:value-of select="./coll[@PM]/@PM"/>
                </cass:pm_pronuncia>
                <cass:data_pronuncia>
                    <xsl:value-of select="./@datdep"/> <!-- snintegrale/@datdec || sic/@datdep || sic/data_ud -->
                </cass:data_pronuncia>
                <cass:numero_pronuncia>
                    <xsl:value-of select="../@numdec"/>
                </cass:numero_pronuncia>
            </proprietary>
        </meta>
    </xsl:template>

    <xsl:template match="col" mode="collegio">
        <collegio>
            <contentToParse>
                <xsl:apply-templates/>
            </contentToParse>
        </collegio>
    </xsl:template>

    <xsl:template match="tes" mode="body">
        <contentToParse>
            <xsl:apply-templates/>
        </contentToParse>
    </xsl:template>

    <xsl:template match="dis" mode="decision">
        <decision>
            <contentToParse>
                <xsl:apply-templates/>
            </contentToParse>
        </decision>
    </xsl:template>

    <!-- TODO: sembra non funzionare -->
    <xsl:template match="newLine">
        <eol>
            <xsl:apply-templates/>
        </eol>
    </xsl:template>
</xsl:stylesheet>
