import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';
import path from 'path';
import xsltUtils from '../utils/xslt_utils';
import config from '../config/config.json';
import bufferReplace from 'buffer-replace';
import xmlInputRefiners from '../imports/XmlInputRefiners';

const router = express.Router();
const jsonParser = bodyParser.json({ limit: '50mb' });

const xsltFilePath = process.env.XSLT_PATH || path.join(config.xsltTransformFile.path, config.xsltTransformFile.fileName);
//Single convertion directly from json with field "content"
router.post('/', jsonParser, (request, response) => {
    if (!request.body.content) {
        throw new Error('Invalid request, empty fields');
    }
    //here convertion/refine logic
    let fileObj = {};
    //buffer content replacement, not handled by carachter encoding, direct to buffer
    fileObj.content = bufferReplace(request.body.content, '&#13;', '<eol/>').toString('utf8');
    //refining input
    fileObj.content = xmlInputRefiners.refineInput(fileObj.content);
    fs.readFile(xsltFilePath, (err, xsltData) => {
        if (err) {
            console.log(err);
            cb(err);
        }
        xsltUtils.transformByXslt(fileObj, xsltData.toString(), (err, fileObj) => {
            if (err) {
                console.log(err);
                cb(err);
            }            
            response.end(fileObj.xmlContent);
        });        
    }); 
});

exports.router = router;
