import xpath from 'xpath';
import xmldom from 'xmldom';

const refineDates = (xmlStringDoc) => {
    let xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
    let dateNodes = xpath.select("//data_decisione|//data_deposito", xmlDoc);
    dateNodes.forEach((el) => {
        let dateSplits = el.lastChild.data.split('/');        
        let newDate = dateSplits.reduceRight((rightV, leftV) => {
            return rightV + '-' + leftV;
        });
        el.lastChild.data = newDate;        
    });
    return new xmldom.XMLSerializer().serializeToString(xmlDoc);
}

const refineInput = (xmlStringDoc) => {
    let refinedXmlString = xmlStringDoc.replace('<list-of-documents>', '<list-of-documents xmlns:xw="whatever">');
    console.log(refinedXmlString.slice(0,200));
    refinedXmlString = refineDates(xmlStringDoc);
    return refinedXmlString;
}
const replaceFromBuffer = (bufferIn) => {
    let hexBuffer = bufferIn.toString('hex');
    //replace &#13; with <eol/>
    const newline = Buffer.from('&#13;').toString('hex');
    const eol = Buffer.from('<eol/>').toString('hex');
    hexBuffer = hexBuffer.replace(new RegExp(newline, 'g'), eol);
    //replace &amp; with &amp;amp;
    // include space in order to avoid replacing this: &amp;#8562
    const amp = Buffer.from('&amp; ').toString('hex');
    const ampamp = Buffer.from('&amp;amp; ').toString('hex');
    hexBuffer = hexBuffer.replace(new RegExp(amp, 'g'), ampamp);
    return Buffer.from(hexBuffer, 'hex');
}

exports.replaceFromBuffer = replaceFromBuffer;
exports.refineInput = refineInput;