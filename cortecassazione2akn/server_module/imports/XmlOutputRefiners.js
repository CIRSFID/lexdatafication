import _ from 'underscore';
import camelCase from 'camelcase';
import {createAkomando} from 'akomando';
import format from 'xml-formatter';
import fs from 'fs';
import xmldom from 'xmldom';
import xpath from 'xpath';
import XRegExp from 'xregexp';
import request from 'request';
import utf8 from 'utf8';

import config from '../config/config.json'
import parser from '../parsers';


const selectObj = xpath.useNamespaces({akn: config.aknNamespace});

const JudgeParser = new parser.JudgeParser('it_IT');  // parser dei giudici
const EpigrafeParser = new parser.EpigrafeParser('it_IT');
const SvolgimentoParser = new parser.SvolgimentoParser('it_IT');
const FattoParser = new parser.FattoParser('it_IT');
const DirittoParser = new parser.DirittoParser('it_IT');
const ConclusionParser = new parser.ConclusionParser('it_IT');
const ParagraphsParser = new parser.ParagraphsParser('it_IT');

// unused
const parseConclusionsAng = (conclusionsNode) => {
    let conclusionsStringNode = new xmldom.XMLSerializer().serializeToString(conclusionsNode);
    let pp = new parser.PersonParser('it_IT');
    let rp = new parser.RoleParser('it_IT');
    let lp = new parser.LocationParser('it_IT');
    //parse persons
    _.uniq(pp.getMatches(conclusionsStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());
        conclusionsStringNode = conclusionsStringNode
            .split(match.fullString)
            .join('<person refersTo="#' + refAttr + '">' + match.fullString + '</person>');
    });
    //parse roles
    _.uniq(rp.getMatches(conclusionsStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());
        conclusionsStringNode = conclusionsStringNode
            .split(match.fullString)
            .join('<role refersTo="#' + refAttr + '">' + match.fullString + '</role>');
    });
    //parse locations
    _.uniq(lp.getMatches(conclusionsStringNode), 'fullString').forEach(match => {
        let refAttr = camelCase(match.fullString.toLowerCase());
        conclusionsStringNode = conclusionsStringNode
            .split(match.fullString)
            .join('<location refersTo="#' + refAttr + '">' + match.fullString + '</location>');
    });
    conclusionsStringNode = conclusionsStringNode.replace(' xmlns="' + config.aknNamespace + '"', '');
    let parsedNode = new xmldom.DOMParser().parseFromString(conclusionsStringNode, 'text/xml');
    return parsedNode;
};

// unused
const evalIDAndMeta = (xmlStringDoc) => {
    let xmlDoc = new xmldom.DOMParser().parseFromString(xmlStringDoc, 'text/xml');
    let roleNodes = selectObj("//akn:role", xmlDoc);
    let locationNodes = selectObj("//akn:location", xmlDoc);
    let personNodes = selectObj("//akn:person", xmlDoc);
    let metaReferenceNode = selectObj("//akn:meta/akn:references", xmlDoc);
    let rolesMeta = [];
    let locationsMeta = [];
    let personsMeta = [];
    //set roles eId
    _.each(roleNodes, (el, key) => {
        el.setAttribute('eId', 'role_' + key);
        rolesMeta.push({
            showAs: el.textContent,
            refersTo: el.getAttribute('refersTo'),
            href: '/akn/ontology/role/it' + el.getAttribute('refersTo').replace('#', '/')
        });
    });
    //set locations eId
    _.each(locationNodes, (el, key) => {
        el.setAttribute('eId', 'location_' + key);
        locationsMeta.push({
            showAs: el.textContent,
            refersTo: el.getAttribute('refersTo'),
            href: '/akn/ontology/location/it' + el.getAttribute('refersTo').replace('#', '/')
        });
    });
    //set persons eId
    _.each(personNodes, (el, key) => {
        el.setAttribute('eId', 'person_' + key);
        personsMeta.push({
            showAs: el.textContent,
            refersTo: el.getAttribute('refersTo'),
            href: '/akn/ontology/person/it' + el.getAttribute('refersTo').replace('#', '/')
        });
    });
    if (!metaReferenceNode[0]) {
        return new xmldom.XMLSerializer().serializeToString(xmlDoc);
    }
    //set role reference metadata

    _.each(_.uniq(rolesMeta, 'showAs'), (el, key) => {
        let role = xmlDoc.createElement('TLCRole');
        role.setAttribute('eId', el.refersTo.replace('#', ''));
        role.setAttribute('href', el.href);
        role.setAttribute('showAs', el.showAs);
        metaReferenceNode[0].appendChild(role)
    });
    _.each(_.uniq(locationsMeta, 'showAs'), (el, key) => {
        let location = xmlDoc.createElement('TLCLocation');
        location.setAttribute('eId', el.refersTo.replace('#', ''));
        location.setAttribute('href', el.href);
        location.setAttribute('showAs', el.showAs);
        metaReferenceNode[0].appendChild(location)
    });
    _.each(_.uniq(personsMeta, 'showAs'), (el, key) => {
        let person = xmlDoc.createElement('TLCPerson');
        person.setAttribute('eId', el.refersTo.replace('#', ''));
        person.setAttribute('href', el.href);
        person.setAttribute('showAs', el.showAs);
        metaReferenceNode[0].appendChild(person)
    });
    return new xmldom.XMLSerializer().serializeToString(xmlDoc);
}

const compareOffset = (a, b) => {
    // utility per ordinare i riferimenti in base all'offset
    if (a.offset.start < b.offset.start) {
        return -1;
    } else if (a.offset.start > b.offset.start) {
        return 1;
    }
    return 0;
};

const rePersonBuilder = (name) => {
    const sep = '(\\s|<eol\\/>)';
    const nameComponents = name.toLowerCase().split(' ');  // splittiamo i nomi
    const reTemplate = `(?<!#pholder${sep})(#pholder)(?!=${sep}#pholder)`;
    const reComponents = nameComponents.map(x => reTemplate.replace(/#pholder/g, x));
    return `(((${reComponents.join('|')})${sep}*\\b){${nameComponents.length}})\\W+`;
};

const getPersons = (emptyAKN) => {
    // NB: dove lo lanciamo questo? Alcuni metadati vengono estratti dal testo
    const persons = [];
    const personElements = selectObj("//akn:TLCPerson", emptyAKN);
    personElements.forEach(person => {
        if (person.getAttribute("showAs").trim() === "") {
            person.parentNode.removeChild(person);
            return;
        }
        const name = person.getAttribute("showAs");
        //const eId = person.getAttribute("eId").trim() !== "" ? person.getAttribute("eId") : name.split(' ').join('.');
        const eId = camelCase(name.replace(/'/g, ''));
        person.setAttribute("eId", eId);
        const href = person.getAttribute("href");
        person.setAttribute("href", href + camelCase(name));
        persons.push(
            {
                name,
                role: person.getAttribute("role"),
                eId,
                re: rePersonBuilder(name)
            }
        );
    });
    return persons;
};

const createInlinePersonElement = (nameString, refersTo, role) => {
    if (["#presidente", "#relatore", "#giudice"].indexOf(role) >= 0) {
        return `<judge refersTo="#${refersTo}" as="${role}">${nameString}</judge>`;
    }
    return `<party refersTo="#${refersTo}" as="${role}">${nameString}</party>`;
};

const preParse = (xmlString) => {
    // parso l'akn risultato dalla conversione con CorteDiCassazione2Akn.xsl
    const emptyAKN = new xmldom.DOMParser().parseFromString(xmlString, 'text/xml');
    // seleziono gli elementi da parsare
    const heading = selectObj("//akn:header", emptyAKN)[0];
    const judgmentBody = selectObj("//akn:judgmentBody", emptyAKN)[0];
    const decision = selectObj("//akn:decision", emptyAKN)[0];
    // ne copio il contenuto testuale per il parsing
    const headingToParse = selectObj("//akn:header/akn:collegio/akn:contentToParse", emptyAKN)[0].textContent.trim();
    const decisionToParse = selectObj("//akn:decision/akn:contentToParse", emptyAKN)[0].textContent.trim();
    // li svuoto per avere uno scheletro da riempire dinamicamente
    decision.textContent = "";
    const judgmentBodyToParse = selectObj("//akn:judgmentBody/akn:contentToParse", emptyAKN)[0].textContent.trim();
    judgmentBody.textContent = "";
    heading.textContent = "";

    return {
        emptyAKN, heading, headingToParse, judgmentBody,
        judgmentBodyToParse, decisionToParse
    };

};

const isAJudge = (href) => {
    if (href.indexOf("giudice") > -1) {
        return true
    }
    if (href.indexOf("presidente") > -1) {
        return true
    }
    if (href.indexOf("relatore") > -1) {
        return true
    }
};

const parseHeader = (emptyAKN, header, headerToParse) => {
    const targetContentElement = emptyAKN.createElement("p"); // questo è fondamentale [v. parsejudgmentbody]
    const metaPersons = selectObj("//akn:TLCPerson", emptyAKN);  // i metadati originali; ci servono per incrociarli
                                                                 // con i dati estratti dal testo e completarli
    // itero linea per linea
    headerToParse.split('\n').forEach(lineToParse => {
        targetContentElement.appendChild(emptyAKN.createElement("eol")); // todo: fix: non molto preciso
        const line = lineToParse;
        const judges = JudgeParser.getMatches(line);
        if (judges.length) {
            // se trova un match, ne salva l'offset
            const judgeMatch = judges[0];
            const jStart = line.indexOf(judgeMatch.giudice);
            const jEnd = jStart + judgeMatch.giudice.length;
            const jOffset = {
                start: jStart,
                end: jEnd
            };
            // anche del ruolo
            const rStart = line.indexOf(judgeMatch.ruolo);
            const rEnd = rStart + judgeMatch.ruolo.length;
            const rOffset = {
                start: rStart,
                end: rEnd
            };
            // appende il testo precedente all'inizio del riferimento
            targetContentElement.appendChild(emptyAKN.createTextNode(line.slice(0, jOffset.start)));
            const personElement = emptyAKN.createElement("judge");  // crea il nodo person
            personElement.textContent = judgeMatch.giudice;  // mantengo la stringa del giudice
            targetContentElement.appendChild(personElement);  // lo appende

            // idem per il ruolo
            targetContentElement.appendChild(emptyAKN.createTextNode(line.slice(jOffset.end, rOffset.start)));
            const roleElement = emptyAKN.createElement("role");
            roleElement.textContent = judgeMatch.ruolo;
            targetContentElement.appendChild(roleElement);
            targetContentElement.appendChild(emptyAKN.createTextNode(line.slice(rOffset.end)));

            // incrociamo i dati e completiamo i metadati e riferimenti
            let roleOntology = "#giudice";
            if (judgeMatch.ruolo.toLowerCase().indexOf("president") > -1) {
                roleOntology = "#presidente";
            }
            if (judgeMatch.ruolo.toLowerCase().indexOf("rel") > -1 || judgeMatch.ruolo.toLowerCase().indexOf("est") > -1) {
                // i ruoli possono essere cumulati (presidente e relatore) TODO: a cosa dare precedenza?
                roleOntology = "#relatore";
            }
            if (roleOntology === null) {
                roleOntology = "#giudice"
            }
            personElement.setAttribute("as", roleOntology);
            roleElement.setAttribute("refersTo", roleOntology);

            // estraiamo e creaimo gli eId per i giudici. Todo: omonimia;
            let lastName = "";
            let firstName = "";
            const fullname = judgeMatch.giudice.split(/\s+/).join(' ');
            // distinguiamo nome e cognome dal fatto che il cognome è sempre maiuscolo
            judgeMatch.giudice.split(/\s+/).forEach(part => {
                if (part === part.toUpperCase()) {
                    lastName += part;
                } else {
                    firstName += part;
                }
            });
            const eId = "#" + firstName[0].toLowerCase() + lastName;
            personElement.setAttribute("refersTo", eId);
            metaPersons.forEach(person => {
                const href = person.getAttribute("href");
                if (isAJudge(href)) {
                    // TODO: i nomi abbreviati non vengono matchati fra testo e TLC
                    if (person.getAttribute("showAs").trim().toUpperCase() === fullname.trim().toUpperCase()) {
                        person.setAttribute("eId", eId.slice(1));
                    }
                }
            });
        } else {
            // se non troviamo il giudice appendiamo semplicemente la linea
            targetContentElement.appendChild(emptyAKN.createElement("eol"));
            targetContentElement.appendChild(emptyAKN.createTextNode(line));
        }
    });
    header.appendChild(targetContentElement);
};

const buildPersonsRefs = (emptyAKN, personsToFind) => {
    const contentElements = selectObj("//content[not(./parent::akn:header)]", emptyAKN); // il collegio è marcato a parte
    const persons = personsToFind ? personsToFind : [];
    contentElements.forEach(contentElement => {
        const matches = [];
        const text = contentElement.toString();
        let newString = "";
        let lastIndex = 0;
        persons.forEach(person => {
            if (!person.re) {
                return;
            }
            XRegExp.forEach(text, XRegExp.build(person.re, {}, 'i'), (matchProps) => {
                const matchObj = {
                    type: "person",
                    offset: {
                        start: matchProps.index,
                        end: matchProps.index + matchProps[0].length
                    },
                    mp: matchProps,
                    person
                };
                matches.push(matchObj);
            });
        });
        matches.sort(compareOffset);
        matches.forEach(match => {
            const before = text.slice(lastIndex, match.offset.start);
            let ref;
            if (match.type === "person") {
                ref = createInlinePersonElement(match.mp[1], match.person.eId, match.person.role);
                // l'offset del match è di tutto, qui lo correggiamo con riferimento al primo gruppo
                lastIndex = match.offset.end - (match.mp[0].length - match.mp[1].length);
            }
            newString += before;
            newString += ref;
        });
        newString += text.slice(lastIndex);
        const newContentElement = new xmldom.DOMParser().parseFromString(newString, 'text/xml');
        const contentParent = contentElement.parentNode;
        contentParent.removeChild(contentElement);
        contentParent.appendChild(newContentElement);
    });
};

const getParentByHierarchy = (point, paragraphs) => {
    const root = point.getAttribute("eId").split("__")[0];
    const hierarchy = point.getAttribute("eId").split("__")[1];
    const hList = hierarchy.split('.').filter(h => h.trim() !== "");
    if (hList.length === 1) {
        return point.parentNode;
    }
    let parentObj;
    while (parentObj === undefined) {
        hList.pop();
        if (hList.length === 0) {
            return point.parentNode;
        }
        let parentEID = `${root}__${hList.join('.')}`;
        if (hList.length === 1) {
            parentEID += ".";
        }
        parentObj = selectObj(".//point[@eId='" + parentEID + "']", paragraphs)[0];
    }

    return parentObj;
};

const getParRootLevel = (point) => {
    //const root = point.getAttribute("eId").split("__")[0];
    const hierarchy = point.getAttribute("eId").split("__")[1];
    const hList = hierarchy.split('.').filter(h => h.trim() !== "");
    return hList.length;
};

const hierarchize = emptyAKN => {
    let paragraphsList = [].slice.call(emptyAKN.getElementsByTagName("paragraphs"));
    //const paragraphsList = selectObj(".//paragraphs", emptyAKN);
    paragraphsList.forEach(paragraphs => {
        const points = selectObj(".//point", paragraphs);
        points.forEach(point => {
            const parent = point.parentNode;
            const realParent = getParentByHierarchy(point, paragraphs);
            parent.removeChild(point);
            let listElement;
            const listElements = realParent.getElementsByTagName("list");
            if (listElements.length === 0) {
                if (realParent.tagName === "point") {
                    listElement = emptyAKN.createElement("list");
                    realParent.appendChild(listElement);
                } else {
                    listElement = realParent;
                }
            } else {
                listElement = listElements[0];
            }
            if (getParRootLevel(point) === 1) {
                realParent.appendChild(point);
            } else {
                listElement.appendChild(point);
            }
        });
        //console.log("RENAMING!!");
        points.forEach(point => {
            const content = point.getElementsByTagName("content")[0];
            //console.log(point.getAttribute("eId"));
            const list = content.nextSibling;
            if (getParRootLevel(point) === 1) {
                //console.log(getParRootLevel(point));
                point.tagName = "paragraph";
            }
            if (list && list.tagName === "list") {
                content.tagName = "alinea";
            }
        });
    });
    paragraphsList.forEach(paragraphs => {

        const points = [].slice.call(paragraphs.getElementsByTagName("point"));
        points.forEach(point => {

            let eId = point.getAttribute("eId");
            if (eId.endsWith(".")) {
                eId = eId.slice(0, eId.length - 1);
            }
            eId = eId.replace(/\./g, "__point_");
            point.setAttribute("eId", eId);

            const content = point.getElementsByTagName("content")[0];
            if (content.firstChild.tagName !== "p") {
                const newContent = emptyAKN.createElement("content");
                const parent = content.parentNode;
                content.tagName = "p";
                newContent.appendChild(content);
                parent.appendChild(newContent);
            }
        });

        let paragraphList = [].slice.call(paragraphs.getElementsByTagName("paragraph"));
        paragraphList.forEach(paragraph => {
            let eId = paragraph.getAttribute("eId");
            if (eId.endsWith(".")) {
                eId = eId.slice(0, eId.length - 1);
            }
            eId = eId.replace(/\./g, "__point_");
            paragraph.setAttribute("eId", eId);
            const content = paragraph.getElementsByTagName("content")[0];
            if (content && content.firstChild.tagName !== "p") {
                const newContent = emptyAKN.createElement("content");
                const parent = content.parentNode;
                content.tagName = "p";
                newContent.appendChild(content);
                parent.appendChild(newContent);
            }
        });
    });

    paragraphsList.forEach(paragraphs => {
        const targetParent = paragraphs.parentNode;
        while (paragraphs.childNodes.length > 0) {
            targetParent.appendChild(paragraphs.childNodes[0]);
        }
        paragraphs.parentNode.removeChild(paragraphs);
    });

};

const fixThisAKN = emptyAKN => {
    const alineeList = [].slice.call(emptyAKN.getElementsByTagName("alinea"));
    alineeList.forEach(alinea => {
        const alineaParent = alinea.parentNode;
        const newAlinea = emptyAKN.createElement("alinea");
        const newContent = emptyAKN.createElement("content");
        const newP = emptyAKN.createElement("p");
        newAlinea.appendChild(newContent);
        newContent.appendChild(newP);
        while (alinea.childNodes.length > 0) {
            newP.appendChild(alinea.childNodes[0]);
        }
        alineaParent.insertBefore(newAlinea, alinea);
        alineaParent.removeChild(alinea);
    });
    const contentList = [].slice.call(emptyAKN.getElementsByTagName("content"));
    contentList.forEach(content => {
        if (content.firstChild.tagName === "paragraph") {
            const parentBlock = content.parentNode;
            while (content.childNodes.length > 0) {
                parentBlock.appendChild(content.childNodes[0]);
            }
            parentBlock.removeChild(content);
        }
    });
    const contentToFixList = [].slice.call(emptyAKN.getElementsByTagName("content"));
    contentToFixList.forEach(content => {
        if (content.firstChild.tagName !== "p") {
            const pElement = emptyAKN.createElement("p");
            while (content.childNodes.length > 0) {
                pElement.appendChild(content.childNodes[0]);
            }
            content.appendChild(pElement);
        }
    })
};

const parsejudgmentBody = (emptyAKN, judgmentBody, judgmentBodyToParse) => {
    /*
    Funzionamento:
    - settiamo delle flag che indicano l'inizio di marco elementi a false;
    - iteriamo il testo linea per linea;
    - se incontriamo un indicatore ("considerato in diritto" ecc.):
        - creiamo l'elemento vuoto corrispondente e lo appendiamo all'albero;
        - creiamo un nuovo contentElement che verrà puntato da targetContentElement
    - se nella linea non vi sono indicatori, la stessa viene appesa al target contentElement
    dopo essere stata parsata per riferimenti
    list
    point
     */

    let flagEpigrafe = false;
    let flagSvolgimento = false;
    let flagFatto = false;
    let flagDiritto = false;
    let introductionElement = null;
    let targetContentElement = null;
    let lastLine = "whatever.";
    let firstParFound = false;
    let currentStructuralElement;
    judgmentBodyToParse.split('\n').forEach(lineToParse => {
        let line = lineToParse;
        const paragraphs = ParagraphsParser.getMatches(line);
        if (paragraphs.length && (lastLine.endsWith("."))) {
            const parNumMatch = paragraphs[0];
            if (parNumMatch.fullString === "1.") {
                firstParFound = true;
            }
            if (targetContentElement && firstParFound) {
                const parentElement = targetContentElement.parentNode;
                let paragraphsElement;
                //console.log(targetContentElement.parentNode.toString());
                if (parentElement.tagName !== "point") {
                    paragraphsElement = emptyAKN.createElement("paragraphs");
                    targetContentElement.appendChild(paragraphsElement);
                } else {
                    paragraphsElement = parentElement.parentNode;
                }
                //console.log(paragraphsElement.tagName);
                const pointElement = emptyAKN.createElement("point");
                const numElement = emptyAKN.createElement("num");
                pointElement.setAttribute("eId", `${currentStructuralElement}__${parNumMatch.fullString}`);
                numElement.textContent = parNumMatch.fullString;
                pointElement.appendChild(numElement);

                const contentElement = emptyAKN.createElement("content");
                const lineElement = emptyAKN.createTextNode(line.slice(parNumMatch.offset.end));
                contentElement.appendChild(lineElement);
                pointElement.appendChild(contentElement);
                paragraphsElement.appendChild(pointElement);
                targetContentElement = contentElement;
                return;
            }
        }

        if (!flagEpigrafe) {
            const matchesEpigrafe = EpigrafeParser.getMatches(line);
            if (matchesEpigrafe.length) {
                const matchEpigrafe = matchesEpigrafe[0];
                const {docType} = matchEpigrafe;
                introductionElement = emptyAKN.createElement("introduction");
                const headingElement = emptyAKN.createElement("heading");
                const docTypeElement = emptyAKN.createElement("docType");
                const contentElement = emptyAKN.createElement("content");
                const pElement = emptyAKN.createElement("p");
                const blockElement = emptyAKN.createElement("division");
                introductionElement.appendChild(blockElement);
                blockElement.setAttribute("refersTo", "#epigrafe");
                docTypeElement.textContent = docType;
                headingElement.appendChild(docTypeElement);
                blockElement.appendChild(headingElement);
                blockElement.appendChild(contentElement);
                contentElement.appendChild(pElement);
                judgmentBody.appendChild(introductionElement);
                targetContentElement = pElement;
                flagEpigrafe = true;
                lastLine = "cool.";
                currentStructuralElement = "epigrafe";
                //console.log(epigrafeElement.toString());
                return;
            }
        }
        if (!flagSvolgimento) {
            const matchesSvolgimento = SvolgimentoParser.getMatches(line.trim());
            if (matchesSvolgimento.length) {
                const matchSvolgimento = matchesSvolgimento[0];
                const {heading} = matchSvolgimento;
                introductionElement = introductionElement ? introductionElement : emptyAKN.createElement("introduction");
                const headingElement = emptyAKN.createElement("heading");
                const contentElement = emptyAKN.createElement("content");
                const blockElement = emptyAKN.createElement("division");
                introductionElement.appendChild(blockElement);
                blockElement.setAttribute("refersTo", "#svolgimento");
                headingElement.textContent = heading;
                blockElement.appendChild(headingElement);
                blockElement.appendChild(contentElement);
                introductionElement.appendChild(blockElement);
                targetContentElement = contentElement;
                flagSvolgimento = true;
                lastLine = "cool.";
                currentStructuralElement = "svolgimento";
                return;
            }
        }
        if (!flagFatto) {
            const matchesFatto = FattoParser.getMatches(line.trim());
            if (matchesFatto.length) {
                const matchFatto = matchesFatto[0];
                const {heading} = matchFatto;
                /*
                if (!introductionElement) {
                    introductionElement = emptyAKN.createElement("introduction");
                    judgmentBody.appendChild(introductionElement);
                }
                */
                const fattoElement = emptyAKN.createElement("background");
                const headingElement = emptyAKN.createElement("heading");
                const contentElement = emptyAKN.createElement("content");
                const blockElement = emptyAKN.createElement("division");
                //introductionElement.appendChild(blockElement);
                blockElement.setAttribute("refersTo", "#fatto");
                blockElement.appendChild(fattoElement);
                headingElement.textContent = heading;
                blockElement.appendChild(headingElement);
                blockElement.appendChild(contentElement);
                fattoElement.appendChild(blockElement);
                judgmentBody.appendChild(fattoElement);
                targetContentElement = contentElement;
                flagFatto = true;
                lastLine = "cool.";
                currentStructuralElement = "fatto";
                return;
            }
        }
        if (!flagDiritto) {
            const matchesDiritto = DirittoParser.getMatches(line.trim());
            if (matchesDiritto.length) {
                const matchDiritto = matchesDiritto[0];
                const {heading} = matchDiritto;
                const dirittoElement = emptyAKN.createElement("motivation");
                const blockElement = emptyAKN.createElement("division");
                const headingElement = emptyAKN.createElement("heading");
                const contentElement = emptyAKN.createElement("content");
                headingElement.textContent = heading;
                blockElement.setAttribute("refersTo", "#diritto");
                dirittoElement.appendChild(blockElement);
                blockElement.appendChild(headingElement);
                blockElement.appendChild(contentElement);
                judgmentBody.appendChild(dirittoElement);
                targetContentElement = contentElement;
                flagDiritto = true;
                lastLine = "cool.";
                currentStructuralElement = "diritto";
                return;
            }
        }

        if (targetContentElement) {
            targetContentElement.appendChild(emptyAKN.createElement("eol"));
            targetContentElement.appendChild(emptyAKN.createTextNode(line));
        }
        lastLine = line.trim();
    });
    hierarchize(emptyAKN);
    fixThisAKN(emptyAKN);
};

const parseDispositivo = (emptyAKN, judgmentBody, decisionToParse) => {
    // v. parsejudgmentBody
    const dp = new parser.DispositivoParser("it_IT");
    const cp = new parser.ConclusionParser("it_IT");
    let flagDispositivo = false;
    let flagConclusions = false;
    let targetContentElement = null;
    decisionToParse.split('\n').forEach(lineToParse => {
        const line = lineToParse;
        if (!flagDispositivo) {
            const matchesDispositivo = dp.getMatches(line);
            if (matchesDispositivo.length) {
                const matchDispositivo = matchesDispositivo[0];
                const {heading} = matchDispositivo;
                const headingElement = emptyAKN.createElement("heading");
                const divisionElement = emptyAKN.createElement("division");
                const contentElement = emptyAKN.createElement("content");
                const pElement = emptyAKN.createElement("p");
                const decisionElement = emptyAKN.createElement("decision");
                headingElement.textContent = heading;
                decisionElement.appendChild(divisionElement);
                divisionElement.appendChild(headingElement);
                divisionElement.appendChild(contentElement);
                contentElement.appendChild(pElement);
                judgmentBody.appendChild(decisionElement);
                targetContentElement = pElement;
                flagDispositivo = true;
                return;
            }
        } else {
            const matchesConclusions = cp.getMatches(line);
            if (matchesConclusions.length) {
                const conclusionsElement = emptyAKN.createElement("conclusions");
                const pElement = emptyAKN.createElement("p");
                conclusionsElement.appendChild(pElement);
                judgmentBody.parentNode.appendChild(conclusionsElement);
                targetContentElement = pElement;
                targetContentElement.appendChild(emptyAKN.createTextNode(line));
                flagConclusions = true;
                return;
            }
        }
        if (targetContentElement) {
            targetContentElement.appendChild(emptyAKN.createElement("eol"));
            targetContentElement.appendChild(emptyAKN.createTextNode(line));
        }
    });
};

const fixMeta = (emptyAKN, materia) => {
    const ecliElement = selectObj("//akn:FRBRalias", emptyAKN)[0];
    const numberElement = selectObj("//akn:FRBRnumber", emptyAKN)[0];
    //const proprietaryNumberElement = selectObj("//akn:proprietary", emptyAKN)[0];
    let ecli = ecliElement.getAttribute("value").split(":");
    const number = parseInt(ecli[4], 10);
    ecli[4] = number.toString();
    ecli[4] = `${ecli[4]}${materia}`;
    numberElement.setAttribute("value", number);
    ecliElement.setAttribute("value", ecli.join(":"));
    const dateElements = selectObj("//akn:FRBRdate", emptyAKN);
    dateElements.forEach(dateElement => {
        const oldDate = dateElement.getAttribute("date").split("/");
        oldDate.reverse();
        const newDate = oldDate.join('-');
        dateElement.setAttribute("date", newDate)
    });
    const workFlow = selectObj("//akn:workflow", emptyAKN)[0];
    if (workFlow) {
        [].slice.call(workFlow.childNodes).forEach(step => {
            const oldDate = step.getAttribute("date").split("/");
            oldDate.reverse();
            const newDate = oldDate.join('-');
            step.setAttribute("date", newDate)
        })
    }
    const allElementsWithValue = selectObj("//akn:*[@value]", emptyAKN);
    allElementsWithValue.forEach(element => {
        const value = element.getAttribute("value");
        const newValue = value.replace(/0*(\d+)/g, '$1');
        element.setAttribute("value", newValue);
    });
    const allMeta = selectObj("//akn:meta", emptyAKN);
    if (allMeta.length === 2) {
        allMeta[1].parentNode.removeChild(allMeta[1]);
    }
    const tlcPersons = selectObj("//akn:TLCPerson", emptyAKN);
    const organizations = ["spa", "s.p.a.", "srl", "s.r.l.", "snc", "s.n.c.", "sas", "s.a.s.", "sapa", "s.a.p.a.",
    "agenzia", "comune", "provincia", "stato", "regione", "presidenza", "consiglio", "associazione", "onlus", "ong",
        "comitato", "società"
    ];
    tlcPersons.forEach(person => {
        const showAs = person.getAttribute("showAs").toLowerCase();
        for (let i=0; i < organizations.length; i++) {
            const o = organizations[i];
            if (showAs.startsWith(o) || showAs.endsWith(o)) {
                person.tagName = "TLCOrganization";
                break;
            }
        }
    })
};

const externalRefs = (emptyAKN, cb) => {
    const aknString = emptyAKN.toString();
    request.post('http://bach.cirsfid.unibo.it/poldo/markup/', {
        json: {
            text_input: aknString,
            xml_safe: true,
            dev: true,
            filters: ['riferimenti_legislativi', 'riferimenti_giurisprudenziali']
        }
    }, (error, res, body) => {
        if (error) {
            console.error(error);
            return cb(error);
        }
        //console.log(`statusCode: ${res.statusCode}`);
        //console.log(body);
        return cb(null, body);
    });
};

const purgeTLCDuplicates = (emptyAKN) => {
    const references = selectObj("//akn:references", emptyAKN)[0];
    const idList = [];
    [].slice.call(references.childNodes).forEach(ref => {
        const eId = ref.getAttribute("eId");
        if (idList.indexOf(eId) >= 0) {
            ref.parentNode.removeChild(ref);
        }
        idList.push(eId);
        if (ref.tagName === "TLCPerson" || ref.tagName === "TLCOrganization") {
            if (ref.getAttribute("role")) {
                ref.removeAttribute("role");
            }
        }
    });

};

const fixConclusions = (emptyAKN) => {
    const conclusions = selectObj("//conclusions", emptyAKN)[0];
    const dp = new parser.DateParser("it_IT");
    if (conclusions) {
        const origP = conclusions.firstChild;
        const pString = origP.toString();
        let newPString = "";
        let i = 0;
        let lastIndex = 0;
        const refersToMap = {
            0: "#dataUdienza",
            1: "#dataDeposito"
        };
        dp.getMatches(pString).forEach(match => {
            newPString += pString.slice(lastIndex, match.offset.start);
            const refersTo = refersToMap[i];
            newPString += `<date refersTo="${refersTo}" date="${match.isoDate}">${match.dateString}</date>`
            i += 1;
            lastIndex = match.offset.end;
        });
        newPString += pString.slice(lastIndex);
        const newP = new xmldom.DOMParser().parseFromString(newPString, 'text/xml');
        origP.parentNode.appendChild(newP);
        origP.parentNode.removeChild(origP);
        const newConclusions = parseConclusionsAng(conclusions);
        conclusions.parentNode.removeChild(conclusions);
        conclusions.parentNode.appendChild(newConclusions);
    }
};

const parseJudgement = (wholeXMLString, materia, cb) => {
    const {
        emptyAKN, heading, headingToParse, judgmentBody,
        judgmentBodyToParse, decisionToParse
    } = preParse(wholeXMLString);
    console.log('>>>>>', materia);
    fixMeta(emptyAKN, materia.toUpperCase());
    parseHeader(emptyAKN, heading, headingToParse);
    const persons = getPersons(emptyAKN);
    parsejudgmentBody(emptyAKN, judgmentBody, judgmentBodyToParse);
    parseDispositivo(emptyAKN, judgmentBody, decisionToParse);
    buildPersonsRefs(emptyAKN, persons);
    purgeTLCDuplicates(emptyAKN);
    fixConclusions(emptyAKN);
    let newAKN = emptyAKN;
    externalRefs(emptyAKN, (error, res) => {
        if (error) {
            console.log(error);
            return cb(error);
            //return cb(null, format(emptyAKN.toString().replace(/(<eol\/> *)+/g, '<eol/>')))
        } else {
            newAKN = new xmldom.DOMParser().parseFromString(res.markup, 'text/xml');
            const meta = selectObj("//akn:meta", newAKN)[0];
            res.references.forEach(refs => {
                refs.forEach(ref => {
                    if (ref.ecli && ref.url) {
                    //console.log(ref.ecli, source);
                    let analysis = meta.getElementsByTagName("analysis");
                    if (analysis.length === 0) {
                        analysis = newAKN.createElement("analysis");
                        analysis.setAttribute("source", "#cirsfid");
                        const references = selectObj("//akn:references", newAKN)[0];
                        meta.insertBefore(analysis, references);
                        //meta.appendChild(analysis);
                    } else {
                        analysis = [].slice.call(analysis)[0];
                    }
                    let otherReferences = analysis.getElementsByTagName("otherReferences");
                    if (otherReferences.length === 0) {
                        otherReferences = newAKN.createElement("otherReferences");
                        otherReferences.setAttribute("source", "#cirsfid");
                        analysis.appendChild(otherReferences);
                    } else {
                        otherReferences = [].slice.call(otherReferences)[0];
                    }
                    const alternativeReference = newAKN.createElement("alternativeReference");
                    alternativeReference.setAttribute("for", `#${ref.eId}`);
                    alternativeReference.setAttribute("href", `${ref.url}`);
                    otherReferences.appendChild(alternativeReference);
                }
                });
            });
            const licenceComment = `<?xml version="1.0"?>\n<!-- AKN XML files released with license Creative Commons (CC BY SA 4.0) by CIRSFID
        derived by documents and information from Corte di Cassazione released with Creative Commons (CC BY SA 3.0) -->`;
            return cb(null, utf8.encode(format(newAKN.toString().replace(/(<eol\/> *)+/g, '<eol/>'))).replace('<?xml version="1.0"?>', licenceComment));
        }
    });
};

const refineOutput = (xmlStringDoc, materia, cb) => {
    //parse heading and conclusions evaluate id in xml string
    parseJudgement(xmlStringDoc, materia, (err, res) => {
        if (err) {
            console.log(err);
            return cb(err);
        } else {
            cb(null, res);
        }
    });
    //akomando section
    /*
    const akomandoHandler = createAkomando({
        aknString: refinedXmlString,
    });

    const unusedRef = akomandoHandler.getReferencesInfo({filterBy: 'unused'});
    //Debug purpose only
    console.log(unusedRef);
    */
    //return refinedXmlString;
};

/*
fs.readdirSync("test_input").forEach(file => {
    const rawString = fs.readFileSync("test_input/" + file, "utf8").toString();
    let materia = "";
    if (!file.endsWith(".xml")) {
        return
    }

    console.log(file);
    if (file.toLowerCase().indexOf('civ') > -1) {
        materia = "CIV";
        ReferenceGiurParser.context.materia = "CIV";
    } else if (file.toLowerCase().indexOf('pen') > -1) {
        materia = "PEN";
        ReferenceGiurParser.context.materia = "PEN";
    }
    ReferenceGiurParser.context.corte = "CASS";
    parseJudgement(rawString, materia, file, (error, res) => {
        if (error) {
            console.log(error);
        } else {
            fs.writeFileSync("test_output/" + file, res);
        }
    });
});
*/


exports.refineOutput = refineOutput;
