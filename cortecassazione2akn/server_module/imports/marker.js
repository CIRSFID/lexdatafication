import parser from '../parsers';

const AuGiudParser = new parser.AutGiudParser('it_IT');
const AutGiudSezParser = new parser.AutGiudSezParser('it_IT');
const ProvvGiudParser = new parser.ProvGiudParser('it_IT');
const ReferenceGiurParser = new parser.ReferenceGiurParser('it_IT');

const testStrings = [
        'cassazzione penale, sez. 5, sentenza numero 10 del 1990',
        'cass. civ., sez. 3, sentenza numero 10 del 8 settembre 1989',
        'cass. civ., sez. un., sentenza numero 10 del 8 settembre 1989',
        'cass. civ., ss. un., sentenza numero 10 del 8 settembre 1989',
        'cass. civ., ss.uu., sentenza numero 10 del 8 settembre 1989',
        'corte cost., sentenza numero 10 del 1990',
        'cass. n. 17686 del 2012'
    ]
;
testStrings.forEach(testString => {
    const refGiur = ReferenceGiurParser.getMatches(testString);
    console.log(refGiur);
});
