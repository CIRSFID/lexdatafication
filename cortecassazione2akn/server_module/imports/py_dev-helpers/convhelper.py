import os
from lxml import etree as ET
import re


INPUT_FILE = os.path.join(os.pardir, os.pardir, "input_samples", "sentenzecivili.xml")
XSLT_FILE = os.path.join(os.pardir, os.pardir, os.pardir, "dist", "xslt", "CorteDiCassazione2Akn.xsl")
XSLT = ET.parse(XSLT_FILE)
transform = ET.XSLT(XSLT)
INPUT_DIR = os.path.join(os.pardir, "test_input")
OUTPUT_DIR = os.path.join(os.pardir, "test_output")

os.makedirs(INPUT_DIR, exist_ok=True)
os.makedirs(OUTPUT_DIR, exist_ok=True)



def cleanup_file(fname):
    with open(fname, "r", encoding="ISO-8859-1") as f:
        broken_xml = f.read()
    return str(re.sub(r"<xw:alr .+>", "", broken_xml))


def multiple_conversion(fname, splitby="snintegrale"):
    if "civili" in fname:
        mat = "CIV"
    elif "penali":
        mat = "PEN"
    else:
        mat = ""
    tmp = "tmp.xml"
    with open(tmp, "w") as w:
        w.write(cleanup_file(fname))

    root = ET.parse(tmp)
    for i, doc in enumerate(root.findall(splitby)):
        ecli = f"ECLI:IT:CASS:{doc.attrib['anno']}:{int(doc.attrib['numdec'])}{mat}"
        akn = transform(doc)
        input_file = os.path.join(INPUT_DIR, ecli.replace(":", "-") + ".xml")
        with open(input_file, "w") as f:
            f.write(str(ET.tostring(akn, pretty_print=True, encoding="unicode")))


if __name__ == "__main__":
    for filename in ["sentenzecivili.xml", "sentenzepenali.xml"]:
        input_file = os.path.join(os.pardir, os.pardir, "input_samples", filename)
        multiple_conversion(input_file)
