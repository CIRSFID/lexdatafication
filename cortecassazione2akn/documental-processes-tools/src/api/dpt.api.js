import { typeCheck } from 'type-check';
import splitDocument from '../imports/splitDocument';
import getXmlOrJson from '../imports/getXmlOrJson';

import configs from '../configs/configs';
import errors from '../configs/errors';

/**
 * Supplies common functionlities on bla bla bla.
 */
const dpt = {

   /**
    * Splits the given document
    * @param {} param0
    */
   async splitDocument({
      documentString,
      splitOn,
      config = configs,
   } = {}) {
      if (typeCheck('Undefined', documentString)) {
         throw new Error(errors.InputFileStringNotSpecified);
      }
      if (!typeCheck('String', documentString)) {
         throw new Error(errors.InputFileIsNotAString);
      }
      if (!typeCheck('String', splitOn)) {
         throw new Error(errors.SplitOnIsNotAString);
      }
      if (typeCheck('Undefined', splitOn) || splitOn === '') {
         throw new Error(errors.SplitOnUndefined);
      }

      const {
         jsonStringName,
         xmlStringName,
         xmlTagNamePrefixSeparator,
      } = config;

      if (jsonStringName == null) {
         throw new Error(errors.ConfigXmlStartingCharacterUndefined);
      }
      if (xmlStringName == null) {
         throw new Error(errors.ConfigXmlStartingCharacterUndefined);
      }

      const documentObject = await dpt.getXmlOrJson({
         documentString,
         config,
      });

      const results = splitDocument(
         documentObject,
         splitOn,
         jsonStringName,
         xmlStringName,
         xmlTagNamePrefixSeparator,
      );
      return results;
   },

   /**
    * bla bla
    * @param {} param0
    */
   async getXmlOrJson({
      documentString,
      config = configs,
   } = {}) {
      if (typeCheck('Undefined', documentString)) {
         throw new Error(errors.InputFileStringNotSpecified);
      }
      if (!typeCheck('String', documentString)) {
         throw new Error(errors.InputFileIsNotAString);
      }
      const {
         xmlStartingCharacter,
         jsonStringName,
         xmlStringName,
         xmlContentType,
      } = config;
      if (xmlStartingCharacter == null) {
         throw new Error(errors.ConfigXmlStartingCharacterUndefined);
      }
      if (jsonStringName == null) {
         throw new Error(errors.ConfigXmlStartingCharacterUndefined);
      }
      if (xmlStringName == null) {
         throw new Error(errors.ConfigXmlStartingCharacterUndefined);
      }
      if (xmlContentType == null) {
         throw new Error(errors.ConfigXmlStartingCharacterUndefined);
      }
      const results = await getXmlOrJson(
         documentString,
         xmlStartingCharacter,
         jsonStringName,
         xmlStringName,
         xmlContentType,
      );
      if (results.stringType == null) {
         throw new Error(errors.InputFileTypeNotSupported);
      }
      return results;
   },
};

export default dpt;
