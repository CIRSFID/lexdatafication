# General

## Requirements

- node 8.12.0

## clone this repo

- using ssh:

    `git clone git@gitlab.com:biagiodistefano/lexdatafication.git`

- using https:

    `git clone https://gitlab.com/biagiodistefano/lexdatafication.git`

# Normattiva2Akn

## installation

    cd normattiva2akn
    npm install

- on windows:
    
    `npm link-win`

- on *nix systems

    `npm link`

## PHP Parser
*N.B.* It already works out-of-the box calling a remote parser. For local installation:

- copy `php-parser/paragraph` in your favorite application server that supports PHP
- change `lib/document_generator/src/confifgs/configs.json` accordingly

## run

### for GUI

for server_module:

    cd document_generator_server_module
    npm run dev

the server will run on port 8080

for frontend_module:

    cd document_generator_frontend_module
    node scripts/start

it will run on port 3000

### for BULK conversion

**NB**: requres eXistDB (latest version)

**NB**: after installing eXistDB, edit
`bulk_server_module/src/configs/config.json`
according to your specific needs

    cd bulk_server_module/src/pipelines
    node pipeline.js <aaaa-mm-gg> <aaaa-mm-gg>

# Cortecostituzionale2Akn | Cortecassazione2Akn

## installation

    cd [cortecostituzionale2Akn|cortecassazione2Akn]
    cd server_module
    npm install

    cd ../frontend_module
    npm install

## run

    cd [cortecostituzionale2Akn|cortecassazione2Akn]

for server_module:

    cd server_module
    npm run dev

the server will run on port 8080

for frontend_module:

    cd frontend_module
    node scripts/start

it will run on port 3000
